<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Active_Campaign</fullName>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Active Campaign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Active_field_update</fullName>
        <description>Activar el campo &quot;Active&quot; después de la fecha &quot;Start Date&quot;</description>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Active field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivate_Act_field</fullName>
        <description>Deactivate &quot;Active&quot; checkbox.</description>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Deactivate Act field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivate_act</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Deactivate act</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_active_field</fullName>
        <description>Desactivar el campo &quot;Active&quot; de campaña después de &quot;End Date&quot;</description>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Update active field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_campaign_active_field</fullName>
        <description>Activate &quot;Active&quot; checkbox.</description>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Update campaign active field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TRAD_Activate Campaign</fullName>
        <actions>
            <name>Active_Campaign</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project,Event</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TRAD_Activate%2FDesactivate_Campaign_Auxiliar</fullName>
        <actions>
            <name>Update_campaign_active_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Campaign.StartDate</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.StartDate</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>If &quot;Start Date&quot; is null or it is less or equal than today, the Campaign &quot;Active&quot; field is  activated. 
On &quot;End Date&quot; the field will be deactivated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Deactivate_Act_field</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
