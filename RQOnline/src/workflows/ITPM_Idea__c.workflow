<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Idea_Status_Date_Update</fullName>
        <field>ITPM_Idea_DT_State_date__c</field>
        <formula>TODAY()</formula>
        <name>Idea Status Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Idea Status Updated</fullName>
        <actions>
            <name>Idea_Status_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( ITPM_SEL_Progress_Degree__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
