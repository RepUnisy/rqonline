<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>alerta_email_evaluacion_enviada</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email evaluacion enviada</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_evaluacion_enviada</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_evaluacion_enviada_ingles</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email evaluacion enviada ingles</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_evaluacion_enviada</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_fecha_limite_aprobacion</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email fecha limite aprobacion</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_fecha_limite_aprobacion</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_fecha_limite_aprobacion_ingles</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email fecha limite aprobacion ingles</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_fecha_limite_aprobacion</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_modificaciones_contrato_firmado</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email modificaciones contrato firmado</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_modificaciones_en_contrato_firmado</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_modificaciones_contrato_firmado_ingles</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email modificaciones contrato firmado ingles</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_modificaciones_en_contrato_firmado</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_pendiente_firma_contrato</fullName>
        <description>alerta email pendiente firma contrato</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_recordatorio_firmar_contrato</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_pendiente_firma_contrato_ingles</fullName>
        <description>alerta email pendiente firma contrato ingles</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_recordatorio_firmar_contrato_ingles</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_programa_aprobado</fullName>
        <description>alerta email programa aprobado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_programa_aprobado</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_programa_aprobado_ingles</fullName>
        <description>alerta email programa aprobado ingles</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_programa_aprobado_ingles</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_programa_finalizado</fullName>
        <description>alerta email programa finalizado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_programa_finalizado</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_programa_finalizado_ingles</fullName>
        <description>alerta email programa finalizado ingles</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_programa_finalizado_ingles</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_programa_no_aprobado</fullName>
        <description>alerta email programa no aprobado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_programa_no_aprobado</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_programa_no_aprobado_ingles</fullName>
        <description>alerta email programa no aprobado ingles</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_programa_no_aprobado_ingles</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_programa_renovado</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email programa renovado</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_programa_renovado</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_programa_renovado_ingles</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email programa renovado ingles</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_programa_renovado</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_solicitud_aprobacion</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email solicitud aprobacion</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_solicitud_aprobacion</template>
    </alerts>
    <alerts>
        <fullName>alerta_email_solicitud_aprobacion_ingles</fullName>
        <ccEmails>plandirector@repsol.com</ccEmails>
        <description>alerta email solicitud aprobacion ingles</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/plantilla_texto_solicitud_aprobacion</template>
    </alerts>
    <fieldUpdates>
        <fullName>Actualiza_Euro</fullName>
        <field>Moneda__c</field>
        <literalValue>€</literalValue>
        <name>Actualiza Euro</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Actualiza_Libra</fullName>
        <field>Moneda__c</field>
        <literalValue>£</literalValue>
        <name>Actualiza Libra</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Actualiza_duracion_estimada</fullName>
        <description>Cuando se cumplimenten las Fechas de Inicio y Fin, el valor del campo Duración estimada se borrará.</description>
        <field>Duracion__c</field>
        <name>Actualiza duracion estimada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Actualizar_Dolar</fullName>
        <field>Moneda__c</field>
        <literalValue>$</literalValue>
        <name>Actualizar Dolar</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Aprobado</fullName>
        <field>Estado__c</field>
        <literalValue>Programa aprobado</literalValue>
        <name>Aprobado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cancelado</fullName>
        <field>Estado__c</field>
        <literalValue>Cancelado</literalValue>
        <name>Cancelado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contrato_Firmado</fullName>
        <field>Estado__c</field>
        <literalValue>Contrato firmado</literalValue>
        <name>Contrato Firmado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Estado_Inicial</fullName>
        <field>Estado__c</field>
        <literalValue>Inicial</literalValue>
        <name>Estado Inicial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>No_aprobado</fullName>
        <field>Estado__c</field>
        <literalValue>Programa no aprobado</literalValue>
        <name>No aprobado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Programa_en_curso</fullName>
        <description>Ha comenzado la fecha inicial del patrocinio</description>
        <field>Estado__c</field>
        <literalValue>Programa en curso</literalValue>
        <name>Programa en curso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>actualiza_area_patrocinio</fullName>
        <field>Area_Calculada__c</field>
        <formula>Owner:User.UserRole.Name</formula>
        <name>actualiza area patrocinio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>actualiza_fecha_de_envio_para_aprobacion</fullName>
        <description>actualiza la fecha de envío para aprobación</description>
        <field>fecha_de_envio_para_aprobacion__c</field>
        <formula>TODAY()</formula>
        <name>actualiza fecha de envío para aprobación</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>desindica_pendiente_aprobacion</fullName>
        <description>indica que el patrocinio ya no está en un proceso de aprobacion</description>
        <field>en_proceso_de_aprobacion__c</field>
        <literalValue>0</literalValue>
        <name>desindica pendiente aprobacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>desindica_programa_rechazado</fullName>
        <description>por si el patrocinio tuvo un proceso de aprobacion el cual fue rechazado y ahora de nuevo se envía para aprobar, hay que resetear el valor de su atributo &quot;programa no aprobado&quot;</description>
        <field>programa_rechazado__c</field>
        <literalValue>0</literalValue>
        <name>desindica programa rechazado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>indica_pendiente_aprobacion</fullName>
        <description>indica si el patrocinio tiene actualmente en curso un proceso de aprobacion</description>
        <field>en_proceso_de_aprobacion__c</field>
        <literalValue>1</literalValue>
        <name>indica pendiente aprobacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>indica_programa_rechazado</fullName>
        <description>indica que el patrocinio actual que se encontraba bajo un proceso de aprobacion finalmente no ha sido aprobado</description>
        <field>programa_rechazado__c</field>
        <literalValue>1</literalValue>
        <name>indica programa rechazado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>rellena_fecha_de_aprobacion</fullName>
        <field>fecha_de_aprobacion__c</field>
        <formula>TODAY()</formula>
        <name>rellena fecha de aprobacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Actualizar Dolar</fullName>
        <actions>
            <name>Actualizar_Dolar</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(CurrencyIsoCode, &apos;USD&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Actualizar Euro</fullName>
        <actions>
            <name>Actualiza_Euro</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(CurrencyIsoCode, &apos;EUR&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Actualizar Libras</fullName>
        <actions>
            <name>Actualiza_Libra</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(CurrencyIsoCode, &apos;GBP&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Resetea duracion estimada</fullName>
        <actions>
            <name>Actualiza_duracion_estimada</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.Fecha_de_inicio__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Patrocinios__c.Fecha_de_fin__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Cuando se cumplimenten las Fechas de Inicio y Fin, el valor del campo Duración estimada se borrará.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>contrato firmado ingles</fullName>
        <actions>
            <name>alerta_email_modificaciones_contrato_firmado_ingles</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Contrato_Firmado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.Contrato_firmado__c</field>
            <operation>equals</operation>
            <value>Si</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Inglés</value>
        </criteriaItems>
        <description>si se indica contrato firmado = SI, el patrocinio cambia automáticamente a estado &quot;Contrato firmado&quot; y se envía notificacion email en ingles</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>en proceso aprobacion</fullName>
        <actions>
            <name>alerta_email_solicitud_aprobacion</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.en_proceso_de_aprobacion__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Español</value>
        </criteriaItems>
        <description>notificacion email tiene pendiente un proceso de aprobacion</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>en proceso aprobacion ingles</fullName>
        <actions>
            <name>alerta_email_solicitud_aprobacion_ingles</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.en_proceso_de_aprobacion__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Inglés</value>
        </criteriaItems>
        <description>notificacion email ingles tiene pendiente un proceso de aprobacion</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>evaluacion enviada</fullName>
        <actions>
            <name>alerta_email_evaluacion_enviada</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.formula_estado__c</field>
            <operation>equals</operation>
            <value>Programa evaluado</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Español</value>
        </criteriaItems>
        <description>notificacion email la evaluacion asociada al patrocinio a sido enviada</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>evaluacion enviada ingles</fullName>
        <actions>
            <name>alerta_email_evaluacion_enviada_ingles</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.formula_estado__c</field>
            <operation>equals</operation>
            <value>Programa evaluado</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Inglés</value>
        </criteriaItems>
        <description>notificacion email en ingles la evaluacion asociada al patrocinio a sido enviada</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>fecha contrato pendiente firma</fullName>
        <actions>
            <name>alerta_email_pendiente_firma_contrato</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Español</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patrocinios__c.avisado_pendiente_firmar_contrato__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patrocinios__c.formula_estado__c</field>
            <operation>equals</operation>
            <value>Programa aprobado</value>
        </criteriaItems>
        <description>notificacion email el programa ha sido aprobado y todavía no ha firmado el contrato</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>fecha contrato pendiente firma ingles</fullName>
        <actions>
            <name>alerta_email_pendiente_firma_contrato_ingles</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Inglés</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patrocinios__c.avisado_pendiente_firmar_contrato__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patrocinios__c.formula_estado__c</field>
            <operation>equals</operation>
            <value>Programa aprobado</value>
        </criteriaItems>
        <description>notificacion email ingles el programa ha sido aprobado y todavía no ha firmado el contrato</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>fecha limite aprobacion</fullName>
        <actions>
            <name>alerta_email_fecha_limite_aprobacion</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.avisado_fecha_limite_aprobacion__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Español</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patrocinios__c.Fecha_limite_de_aprobacion2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>notificacion email cuando se acerque la fecha límite de aprobación de un Patrocinio</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>fecha limite aprobacion ingles</fullName>
        <actions>
            <name>alerta_email_fecha_limite_aprobacion_ingles</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.avisado_fecha_limite_aprobacion__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Inglés</value>
        </criteriaItems>
        <criteriaItems>
            <field>Patrocinios__c.Fecha_limite_de_aprobacion2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>notificacion email ingles cuando se acerque la fecha límite de aprobación de un Patrocinio</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ha firmado firmado</fullName>
        <actions>
            <name>alerta_email_modificaciones_contrato_firmado</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Contrato_Firmado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.Contrato_firmado__c</field>
            <operation>equals</operation>
            <value>Si</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Español</value>
        </criteriaItems>
        <description>si se indica contrato firmado = SI, el patrocinio cambia automáticamente a estado &quot;Contrato firmado&quot; y se envía notificacion email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>programa aprobado</fullName>
        <actions>
            <name>alerta_email_programa_aprobado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.formula_estado__c</field>
            <operation>equals</operation>
            <value>Programa aprobado</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Español</value>
        </criteriaItems>
        <description>alerta email el programa ha sido aprobado</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>programa aprobado ingles</fullName>
        <actions>
            <name>alerta_email_programa_aprobado_ingles</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.formula_estado__c</field>
            <operation>equals</operation>
            <value>Programa aprobado</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Inglés</value>
        </criteriaItems>
        <description>alerta email en ingles el programa ha sido aprobado</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>programa finalizado</fullName>
        <actions>
            <name>alerta_email_programa_finalizado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.formula_estado__c</field>
            <operation>equals</operation>
            <value>Pendiente evaluacion</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Español</value>
        </criteriaItems>
        <description>notificacion email cuando el programa alcanza su fecha de finalizacion</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>programa finalizado ingles</fullName>
        <actions>
            <name>alerta_email_programa_finalizado_ingles</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.formula_estado__c</field>
            <operation>equals</operation>
            <value>Pendiente evaluacion</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Inglés</value>
        </criteriaItems>
        <description>notificacion email en ingles cuando el programa alcanza su fecha de finalizacion</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>programa no aprobado</fullName>
        <actions>
            <name>alerta_email_programa_no_aprobado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.programa_rechazado__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Español</value>
        </criteriaItems>
        <description>notificacion email que el proceso de aprobacion en el que estaba inmerso el patrocinio ha sido rechazado</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>programa no aprobado ingles</fullName>
        <actions>
            <name>alerta_email_programa_no_aprobado_ingles</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.programa_rechazado__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Inglés</value>
        </criteriaItems>
        <description>notificacion email en ingles que el proceso de aprobacion en el que estaba inmerso el patrocinio ha sido rechazado</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>programa renovado</fullName>
        <actions>
            <name>alerta_email_programa_renovado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.es_renovacion__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Español</value>
        </criteriaItems>
        <description>notificacion email que el pograma de patrocinio ha sido renovado</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>programa renovado ingles</fullName>
        <actions>
            <name>alerta_email_programa_renovado_ingles</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patrocinios__c.es_renovacion__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LanguageLocaleKey</field>
            <operation>equals</operation>
            <value>Inglés</value>
        </criteriaItems>
        <description>notificacion email en inglés que el pograma de patrocinio ha sido renovado</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
