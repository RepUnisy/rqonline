<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ITPM_RISK_ROW_UPDATE_MODIFICATION_DATE</fullName>
        <field>ITPM_Matrix_risk_DT_Modification_Date__c</field>
        <formula>TODAY()</formula>
        <name>ITPM_RISK_ROW_UPDATE_MODIFICATION_DATE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ITPM_Matrix_Risk__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>ITPM_RISK_MATRIX_ROW_UPDATE_MODIFICATION_DATE</fullName>
        <actions>
            <name>ITPM_RISK_ROW_UPDATE_MODIFICATION_DATE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
