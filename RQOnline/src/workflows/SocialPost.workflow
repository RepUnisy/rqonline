<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>rrss_Alerta_Rechazo_solicitud_aprobaci_n_Post</fullName>
        <description>Alerta Rechazo solicitud aprobación Post</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>rrss_Plantillas_de_Correo_Social_Media/rrss_Rechazo_Aprobaci_n_Post</template>
    </alerts>
</Workflow>
