<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>rrss_Alerta_incumplimiento_KPI_solicitud_aprobacion_Post</fullName>
        <ccEmails>socialhub@repsol.com</ccEmails>
        <description>Alerta incumplimiento KPI solicitud aprobación Post</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>rrss_Plantillas_de_Correo_Social_Media/rrss_Solicitud_Aprobaci_n_Post_Incumplimiento_KPI</template>
    </alerts>
    <fieldUpdates>
        <fullName>RRSS_Cierre_de_Segunda_Respuesta</fullName>
        <description>Actualización del campo checkbox &quot;Nueva Respuesta&quot; para que se desactive cuando un caso pase al estado &quot;Resuelto&quot;</description>
        <field>rrss_Nueva_Repuesta__c</field>
        <literalValue>0</literalValue>
        <name>Cierre de Segunda Respuesta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>rrss_Cambio_de_Estado_a_Resuelto</fullName>
        <field>Status</field>
        <literalValue>Resuelto</literalValue>
        <name>Cambio de Estado a Resuelto</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>rrss_Cierre_de_caso</fullName>
        <description>Actualización del estado de caso a &quot;Cerrado&quot;</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Cierre de caso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>rrss_Priority_alta</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Priority_alta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>rrss_Record_Type_a_Cerrado</fullName>
        <description>Cuando el estado del caso sea &quot;Cerrado&quot; el Record Type cambia a rrss_Casos_Cerrados_Social_Media</description>
        <field>RecordTypeId</field>
        <lookupValue>rrss_Casos_Cerrados_Social_Media</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type a Cerrado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>rrss_Reseteo_flag_nueva_respuesta</fullName>
        <description>Acción que desmarca la casilla de Nueva respuesta que permite controlar el completado del evento clave &quot;Tiempo Segunda Respuesta&quot;</description>
        <field>rrss_Nueva_Repuesta__c</field>
        <literalValue>0</literalValue>
        <name>Reseteo flag nueva respuesta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>rrss Caso Cerrado</fullName>
        <actions>
            <name>rrss_Record_Type_a_Cerrado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>RRSS Casos Social Media</value>
        </criteriaItems>
        <description>Cuando el estado de un caso de Social Medie sea &quot;Cerrado&quot;, cambia el Record Type a rrss Casos Cerrados Social Media y bloquea todos los campos</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>rrss Resuelto a Cerrado</fullName>
        <actions>
            <name>RRSS_Cierre_de_Segunda_Respuesta</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resuelto</value>
        </criteriaItems>
        <description>Workflow que modifica el estado del caso para cerrarlo a los 5 días de estar en el estado de Resuelto</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>rrss_Cierre_de_caso</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
