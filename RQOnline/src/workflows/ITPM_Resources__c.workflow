<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>updateResourceName</fullName>
        <description>updateResourceName with Resource Name</description>
        <field>Name</field>
        <formula>ITPM_REL_Resource__r.FirstName + &apos; &apos; +  ITPM_REL_Resource__r.LastName</formula>
        <name>updateResourceName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>updateName</fullName>
        <actions>
            <name>updateResourceName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>updateName withe Resource Name</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
