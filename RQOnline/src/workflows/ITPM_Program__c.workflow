<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ITPM_UPDATE_PROGRAM_NAME_COPY</fullName>
        <field>ITPM_TX_Name_Copy__c</field>
        <formula>Name</formula>
        <name>ITPM_UPDATE_PROGRAM_NAME_COPY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ITPM_PROGRAM_DUPLICATED</fullName>
        <actions>
            <name>ITPM_UPDATE_PROGRAM_NAME_COPY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Evita que se duplique el Name de un Programa</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
