<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ITPM_CI_UPDATE_CARGA</fullName>
        <description>Actualiza el campo no carga a True para todos los registros insertados desde SF. 
Se utilizará este campo posteriormente en el proceso de carga de ficheros.</description>
        <field>ITPM_BLN_Carga__c</field>
        <literalValue>1</literalValue>
        <name>ITPM_CI_UPDATE_CARGA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ITPM_UPDATE_CARGA</fullName>
        <actions>
            <name>ITPM_CI_UPDATE_CARGA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actualiza el campo no carga a True para todos los registros insertados desde SF.
Se utilizará este campo posteriormente en el proceso de carga de ficheros.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
