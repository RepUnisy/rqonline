<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ITPM_UPDATE_PORTFOLIO_NAME_COPY</fullName>
        <description>Evita que se duplique el Name de un Portfolio</description>
        <field>ITPM_TX_Name_Copy__c</field>
        <formula>Name</formula>
        <name>ITPM_UPDATE_PORTFOLIO_NAME_COPY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ITPM_PORTFOLIO_DUPLICATED</fullName>
        <actions>
            <name>ITPM_UPDATE_PORTFOLIO_NAME_COPY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Evita que se duplique el Name de un Portfolio</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
