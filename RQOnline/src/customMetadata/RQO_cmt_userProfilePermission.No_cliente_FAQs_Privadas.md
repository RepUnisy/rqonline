<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>No cliente FAQs Privadas</label>
    <protected>false</protected>
    <values>
        <field>RQO_fld_permission__c</field>
        <value xsi:type="xsd:string">RQO_plv_privateFaqsAccess</value>
    </values>
    <values>
        <field>RQO_fld_url__c</field>
        <value xsi:type="xsd:string">/faqs</value>
    </values>
    <values>
        <field>RQO_fld_userProfile__c</field>
        <value xsi:type="xsd:string">No cliente</value>
    </values>
</CustomMetadata>
