<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AutomatedTest</label>
    <protected>false</protected>
    <values>
        <field>RQO_fld_emailList__c</field>
        <value xsi:type="xsd:string">david.iglesias@unisys.com</value>
    </values>
    <values>
        <field>RQO_fld_environment__c</field>
        <value xsi:type="xsd:string">DES</value>
    </values>
    <values>
        <field>RQO_fld_lessCoverageReportRecords__c</field>
        <value xsi:type="xsd:double">99.0</value>
    </values>
    <values>
        <field>RQO_fld_password__c</field>
        <value xsi:type="xsd:string">eU3jmaF5sML+VzKtRN+nMF7WfRe9ouUgmKsuRoFp3ZI=</value>
    </values>
    <values>
        <field>RQO_fld_securityToken__c</field>
        <value xsi:type="xsd:string">dSG5qYYYBslnByJ0EDXt2Le6t+7oIb/8IHf3GBour8uQw2F0WnxZBK2MiJZMiPAN</value>
    </values>
    <values>
        <field>RQO_fld_username__c</field>
        <value xsi:type="xsd:string">testUser@unisys.com</value>
    </values>
</CustomMetadata>
