<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Commercial Information 1</label>
    <protected>false</protected>
    <values>
        <field>RQO_fld_customerProfile__c</field>
        <value xsi:type="xsd:string">Transformer</value>
    </values>
    <values>
        <field>RQO_fld_email__c</field>
        <value xsi:type="xsd:string">francisco.delnido@unisys.com</value>
    </values>
    <values>
        <field>RQO_fld_informationProduct__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>RQO_fld_informationTopic__c</field>
        <value xsi:type="xsd:string">CommercialInformation</value>
    </values>
</CustomMetadata>
