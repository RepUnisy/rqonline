<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Pedido confirmado</label>
    <protected>false</protected>
    <values>
        <field>RQO_fld_notificationConsent__c</field>
        <value xsi:type="xsd:string">Commercial Information</value>
    </values>
    <values>
        <field>RQO_fld_notificationType__c</field>
        <value xsi:type="xsd:string">CO</value>
    </values>
</CustomMetadata>
