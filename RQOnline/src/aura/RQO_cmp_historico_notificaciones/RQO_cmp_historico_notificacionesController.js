({
    getInfo : function(component, event, helper) {
        var language = event.getParam("language");
        var user = event.getParam("user");
        
        //	Almacenamos la información obtenida
        component.set("v.language", language);
        component.set("v.user", user);
        debugger;
        //	Obtenemos la fecha diaria
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth();
        var yy = today.getFullYear();
        var today = new Date(yy, mm, dd);
       
        component.set("v.today", "2017-12-19");
        
        //	LLamamos al helper para obtener la información inicial
        helper.getInitialInfo(component);
    },
    searchData : function(component, event, helper) {
		helper.searchData(component, event);
	}
})