({
	getInitialInfo : function(component) {		
        
        var idContacto = component.get("v.user.idContacto");
        //	Llamamos a la funcion para inicializar las notificacioens 
        this.obtenerNotificaciones(component, idContacto, null, null, null, false);
        
    },
    searchData : function (component, event){
        
		var idContacto = component.get("v.user.idContacto");
        var categoria = event.getParam("categoria");
        var dateFrom = event.getParam("dateFrom");
        var dateTo = event.getParam("dateTo");
        debugger;
        //	Llamamos a la funcion para inicializar las notificacioens 
        this.obtenerNotificaciones(component, idContacto, categoria, dateFrom, dateTo, true);
     
    },
    
    obtenerNotificaciones : function(component, idContacto, categoria, fechaInicio, fechaFin, visualized) {
     	debugger;
        var action = component.get('c.getNotifications');
        action.setParams({ idContacto : idContacto, categoria : categoria, fechaInicio : fechaInicio, 
                          fechaFin: fechaFin, visualized: visualized });
        debugger;
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                debugger;
                var map = new Map(Object.entries(JSON.parse(response.getReturnValue())));
                                
                var listadoKey = Array.from(map.keys());                
                var myObjectMap = [];
                for(var x=0; x<listadoKey.length; x++) {
                    var temp = { "key": listadoKey[x], "list": map.get(listadoKey[x]) };
                    myObjectMap.push(temp);
                }
                component.set("v.mapaNotificaciones", myObjectMap);
                
                debugger;
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);   
    }
})