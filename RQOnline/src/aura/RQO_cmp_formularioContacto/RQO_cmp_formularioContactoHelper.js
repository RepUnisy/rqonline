({
	reCaptchaEvent : function(component, event, helper) {
		var vfOrigin = "https://" + window.location.hostname;
		// Capturar evento procedente del reCaptcha de la página visual force
        window.addEventListener("message", function(event) {
            if (event.origin !== vfOrigin) {
                // Not the expected origin: Reject the message!
                return;
            }
            
            if(event.data.action == component.get("v.reCaptchaMessage") && event.data.alohaResponseCAPTCHA == 'NOK'){
                component.set('v.textoAlerta', 'Debe validar el captcha');
	            component.set('v.showAlert',true);
            }
            else if(event.data.action == component.get("v.reCaptchaMessage") && event.data.alohaResponseCAPTCHA == 'OK'){
                helper.save(component, event, helper);
            }
        }, false);
	},
	getTitles : function(component, event, helper) {
		//	Obtenemos Listado de titulos
        // hints to ensure labels are preloaded
		// $Label.c.RQO_lbl_personTitleMr
		// $Label.c.RQO_lbl_personTitleMrs
        var action = component.get("c.getPersonTitles");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var titulos = response.getReturnValue();
                var numTitulos = titulos.length;
                var opciones = [];
                for(var i = 0;i < numTitulos;i++)
                {
                	var tituloLabel = $A.get("$Label.c.RQO_lbl_personTitle" + titulos[i]);
                	opciones.push({'label':tituloLabel, 'value':titulos[i], 'selected':false});
                }
                component.set("v.SrValores", opciones);
            }
        });
        
        $A.enqueueAction(action);
    },
	getProfiles : function(component, event, helper) {
		//	Obtenemos Listado de perfiles
        // hints to ensure labels are preloaded
		// $Label.c.RQO_lbl_customerProfileTransformer
		// $Label.c.RQO_lbl_customerProfileDistributor
		// $Label.c.RQO_lbl_customerProfileAgent
		// $Label.c.RQO_lbl_customerProfileMedia
		// $Label.c.RQO_lbl_customerProfileInstitution
		// $Label.c.RQO_lbl_customerProfileAssociation
		// $Label.c.RQO_lbl_customerProfilePrivate
		// $Label.c.RQO_lbl_customerProfileEducationalInstitutions
		// $Label.c.RQO_lbl_customerProfileOther
        var action = component.get("c.getCustomerProfiles");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var perfiles = response.getReturnValue();
                var numPerfiles = perfiles.length;
                var opciones = [];
                for(var i = 0;i < numPerfiles;i++)
                {
                	var perfilLabel = $A.get("$Label.c.RQO_lbl_customerProfile" + perfiles[i]);
                	opciones.push({'label':perfilLabel, 'value':perfiles[i], 'selected':false});
                }
                component.set("v.PerfilValores", opciones);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    getTopics : function(component, event, helper) {
		//	Obtenemos Listado de temas
        // hints to ensure labels are preloaded
		// $Label.c.RQO_lbl_informationTopicCorporateInformation
		// $Label.c.RQO_lbl_informationTopicCommercialInformation
		// $Label.c.RQO_lbl_informationTopicTechnicalInformation
		// $Label.c.RQO_lbl_informationTopicHelpAndSupport
        var action = component.get("c.getInformationTopics");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var topics = response.getReturnValue();
                var numTopics = topics.length;
                var opciones = [];
                for(var i = 0;i < numTopics;i++)
                {
                	var topicLabel = $A.get("$Label.c.RQO_lbl_informationTopic" + topics[i]);
                	opciones.push({'label':topicLabel, 'value':topics[i], 'selected':false});
                }
                component.set("v.TemaContactoValoresPublico", opciones);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    getProducts : function(component, event, helper) {
		//	Obtenemos Listado de productos
        // hints to ensure labels are preloaded
		// $Label.c.RQO_lbl_informationProductPolyolefins
		// $Label.c.RQO_lbl_informationProductPropyleneOXID
		// $Label.c.RQO_lbl_informationProductOlefins
		// $Label.c.RQO_lbl_informationProductAromatics
		// $Label.c.RQO_lbl_informationProductAll
		// $Label.c.RQO_lbl_informationProductNone
        var action = component.get("c.getProducts");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var productos = response.getReturnValue();
                var numProductos = productos.length;
                var opciones = [];
                for(var i = 0;i < numProductos;i++)
                {
                	var productoLabel = $A.get("$Label.c.RQO_lbl_informationProduct" + productos[i]);
                	opciones.push({'label':productoLabel, 'value':productos[i], 'selected':false});
                }
                component.set("v.ProductoValores", opciones);
            }
        });
        
        $A.enqueueAction(action);
    },

    adjuntar : function(component, event, helper) {
        var listaArchivos = component.get('v.FileList');
    	var listaNombres = component.get('v.listaNombres');
        var listaContents = component.get('v.listaContents');
    	
        for(var i=0; i<listaArchivos.length; i++){
            var file = listaArchivos[i];
            listaNombres.push(file.name);
            component.set("v.listaNombres", listaNombres);
            this.getFileContent(listaContents, file);
        }
    },
    
    getFileContent: function(listaContents, file) {
        var fr = new FileReader();
        
       	fr.onload = function() {
            var fileContents = fr.result;
    	    var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
 
            fileContents = fileContents.substring(dataStart);
        
    	    listaContents.push(encodeURIComponent(fileContents));
        };
 
        fr.readAsDataURL(file);
    },
    
    save: function(component, event, helper) {
        
        // Comprobar tamaño de ficheros
        var totalSize = 0;
        var listaArchivos = component.get('v.FileList');
        for(var i=0; i<listaArchivos.length; i++){
            var file = listaArchivos[i];
            totalSize = totalSize + file.size;
        }
        if (totalSize >= 2621440)
        {
        	component.set('v.textoAlerta', $A.get("$Label.c.RQO_lbl_fileSizeExceeded"));
        	component.set('v.showAlert',true);
        }
        else
        {
	    	var listaNombres = component.get('v.listaNombres');
	        var listaContents = component.get('v.listaContents');
	        
	        var titulo = component.find('sltTitulo').get('v.value');
	        var perfil = component.find('sltPerfil').get('v.value');
	        var tema = component.find('sltTema').get('v.value');
	        var producto = '';
	        if (component.find('sltProducto'))
	        	producto = component.find('sltProducto').get('v.value');
	        	
	        component.set("v.showSpinner",true);
	        
	        var action = component.get("c.sendRequest");
	        action.setParams({
	        	title: titulo,
	        	name: component.get('v.Nombre'),
	        	surname: component.get('v.Apellidos'),
	        	profile: perfil,
	        	companyName: component.get('v.NombreEmpresa'),
	        	phoneArea: component.get('v.Prefijo'),
	        	phoneNumber: component.get('v.Telefono'),
	        	countryCode: component.get('v.PaisSelected'),
	        	email: component.get('v.Correo'),
	        	topic: tema,
	        	product: producto,
	        	subject: component.get('v.Asunto'),
	        	message: component.get('v.Mensaje'),
	        	copyEmail: component.find("inputCHCopiaPublico").get("v.checked"),
	            fileNames: listaNombres,
	            base64Datas: listaContents
	        });
	 
	        action.setCallback(this, function(response){
	            var state = response.getState();
	            component.set("v.showSpinner",false);
	            if (state === "SUCCESS") {
	            	var btAccept = component.find("btAccept");
	            	btAccept.set("v.disabled", true);
	                component.set('v.showConfirm',true);
	            }
	            else {
	            	component.set('v.textoAlerta', $A.get("$Label.c.RQO_lbl_errorGenerico"));
	            	component.set('v.showAlert',true);
	            }
	        });
	        
	        $A.enqueueAction(action);
	    }
    }
})