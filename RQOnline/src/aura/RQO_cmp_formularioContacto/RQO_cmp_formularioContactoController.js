({
	doInit : function(component, event, helper) {
		component.set('v.communityUrl', window.location.href.substring(0 ,window.location.href.indexOf('/s/')));
		helper.getProfiles(component, event, helper);
		helper.getTopics(component, event, helper);
		helper.getProducts(component, event, helper);
		helper.getTitles(component, event, helper);
		helper.reCaptchaEvent(component, event, helper);
    },
	onRender : function(component, event, helper) {

	},
    validaciones :function(component, event, helper) {
    
    	var perfil = component.find("sltPerfil").get("v.value");

    	if ($A.get("$Label.c.RQO_lbl_customerProfile" + perfil) === $A.get("$Label.c.RQO_lbl_customerProfileTransformer") || 
    		$A.get("$Label.c.RQO_lbl_customerProfile" + perfil) === $A.get("$Label.c.RQO_lbl_customerProfileDistributor") || 
    		$A.get("$Label.c.RQO_lbl_customerProfile" + perfil) === $A.get("$Label.c.RQO_lbl_customerProfileAgent"))
            component.find('inputNombreEmpresaPublico').set("v.required", true);
        else
        	component.find('inputNombreEmpresaPublico').set("v.required", false);
    
        var tema = component.find("sltTema").get("v.value");
        
    	if ($A.get("$Label.c.RQO_lbl_informationTopic" + tema) === $A.get("$Label.c.RQO_lbl_informationTopicCommercialInformation") || 
    		$A.get("$Label.c.RQO_lbl_informationTopic" + tema) === $A.get("$Label.c.RQO_lbl_informationTopicTechnicalInformation"))
            component.set('v.MostrarProducto',true);
        else
        	component.set('v.MostrarProducto',false);
        	
	},
    toggleArco:function(component, event, helper) {
     	jQuery('.rq-tooltip-arco').slideToggle();
        
	},
    toggleTelefono:function(component, event, helper) {
        console.log('entro');
     	jQuery('.rq-tooltip-telefono.rq-tooltip').slideToggle();
	},
	siguiente : function(component, event, helper) {
        helper.adjuntar(component, event, helper);
	},
    guardar : function(component, event, helper) {
    
		 var validityTitulo = component.find("sltTitulo").get("v.validity");
		 var validityNamePublico = component.find("inputNamePublico").get("v.validity");
		 var validityApellidosPublico = component.find("inputApellidosPublico").get("v.validity");
		 var validityPerfil = component.find("sltPerfil").get("v.validity");
		 var validityPrefijoPublico = component.find("inputPrefijoPublico").get("v.validity");
		 var validityTelefonoPublico = component.find("inputTelefonoPublico").get("v.validity");
		 var validityTema = component.find("sltTema").get("v.validity");
		 var validityMailPublico = component.find("inputMailPublico").get("v.validity");
		 var validityAsuntoPublico = component.find("inputAsuntoPublico").get("v.validity");
		 var validityMensaje = component.find("txtMensaje").get("v.validity");
		 var validityDerechosArco = component.find("cbDerechosArco").get("v.validity");
		 
		 var perfil = component.find("sltPerfil").get("v.value");
		 
    	if (perfil &&
    		($A.get("$Label.c.RQO_lbl_customerProfile" + perfil) === $A.get("$Label.c.RQO_lbl_customerProfileAgent") || 
    		$A.get("$Label.c.RQO_lbl_customerProfile" + perfil) === $A.get("$Label.c.RQO_lbl_customerProfileDistributor") || 
    		$A.get("$Label.c.RQO_lbl_customerProfile" + perfil) === $A.get("$Label.c.RQO_lbl_customerProfileTransformer")))
    	{
			var correo = component.get("v.Correo");
			if (correo.includes("gmail") || correo.includes("hotmail") || correo.includes("yahoo"))
			{
				component.set('v.textoAlerta', $A.get("$Label.c.RQO_lbl_emailTypeError"));
            	component.set('v.showAlert',true);
            	return;
			}
    	}

		if (validityTitulo.valid && validityNamePublico.valid && validityApellidosPublico.valid && 
			validityPerfil.valid && validityPrefijoPublico.valid && validityTelefonoPublico.valid && 
			validityTema.valid && validityMailPublico.valid && validityAsuntoPublico.valid && 
			validityMensaje.valid && validityDerechosArco.valid) {
			
			var message = component.get("v.reCaptchaMessage");
			var vfOrigin = "https://" + window.location.hostname;
	        var vfWindow = component.find("vfFrame").getElement().contentWindow;
			vfWindow.postMessage({ action: message }, vfOrigin);
		}
		else
		{
			component.find("sltTitulo").showHelpMessageIfInvalid();
			component.find("inputNamePublico").showHelpMessageIfInvalid();
			component.find("inputApellidosPublico").showHelpMessageIfInvalid();
			component.find("sltPerfil").showHelpMessageIfInvalid();
			component.find("inputPrefijoPublico").showHelpMessageIfInvalid();
			component.find("inputTelefonoPublico").showHelpMessageIfInvalid();
			component.find("sltTema").showHelpMessageIfInvalid();
			component.find("inputMailPublico").showHelpMessageIfInvalid();
			component.find("inputAsuntoPublico").showHelpMessageIfInvalid();
			component.find("txtMensaje").showHelpMessageIfInvalid();
		}
	},
    quitarArchivo : function(component, event, helper) {
        var nombreArchivo = event.currentTarget.previousElementSibling.innerText;
        var listaNombres = component.get('v.listaNombres');
        var listaContents = component.get('v.listaContents');
        var listaArchivos = component.get('v.FileList');
        var listaNombres2 = [];
        var listaContents2 = [];
        var listaArchivos2 = [];
        
        for(var i=0; i<listaNombres.length; i++){
            if(listaNombres[i] != nombreArchivo){
                listaNombres2.push(listaNombres[i]);
                listaContents2.push(listaContents[i]);
            }
        }
        for(var i=0; i<listaArchivos.length; i++){
            if(listaArchivos[i].name != nombreArchivo){
                listaArchivos2.push(listaArchivos[i]);
            }
        }
        component.set("v.listaNombres", listaNombres2);
        component.set("v.listaContents", listaContents2);
        component.set("v.FileList", listaArchivos2);
	}
})