({
    descargar : function(component, event) {
        
        var idArchivado = event.getSource().get("v.name");
        debugger;
        
        var action = component.get('c.getFileFactura');
        action.setParams({ idArchivado : idArchivado});
        
        action.setCallback(this, function(response){
            debugger;
            var nok = false;
            var state = response.getState();
            console.log('ACL. Get factura' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var storedResponse = response.getReturnValue();
                if (storedResponse && storedResponse.length > 2 && storedResponse[2]){
                      alert('descargado!!!')
                    try {
                        var byteString = atob(storedResponse[2]);
                        var ab = new ArrayBuffer(byteString.length);
                        var ia = new Uint8Array(ab);
                        for (var i = 0; i < byteString.length; i++) {
                            ia[i] = byteString.charCodeAt(i);
                        }                        
                        var docName = ((storedResponse.length > 3 && storedResponse[3]) ?  storedResponse[3] : 'DOC' + new Date());
                        var docExtension = '';
                        var docType = ((storedResponse.length > 4 && storedResponse[4]) ?  storedResponse[4] : '');
                        var contentType = '';
                        if(docType && docType=='pdf'){
                            contentType = 'application/pdf';
                            docExtension = '.pdf';
                        }else if(docType && (docType == 'msw' || docType == 'msw8' || docType == 'docx')){
                            contentType = 'application/msword';
                        }else if(docType && docType=='txt'){
                            contentType = 'text/plain';
                            docExtension = '.txt';
                        }else{
                            contentType = 'application/octet-stream';
                            //docExtension = docType;
                        }						
                        var blob = new Blob([ia], { type: contentType});                        
                        saveAs(window, blob, docName + docExtension);
                        
                    }catch(err) {
                        nok = true;
                        console.log('Error: ' + err.message);
                    }
                }
                else{
					nok = true;
                }
            }
            else if (state === "INCOMPLETE") {
                nok = true;
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    nok = true;
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            if (nok){
                var textAlert = $A.get("$Label.c.RQO_lbl_errorDescargaDocumento");
                var titleAlertValue = $A.get("$Label.c.RQO_lbl_error");
                this.fireAlertEvent(textAlert, titleAlertValue);
            }
        });
        $A.enqueueAction(action);  
        
	}, fireAlertEvent : function(textAlert, titleAlertValue){
        
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : textAlert, "confirmAlert" : false, 
                            "titleAlertValue" : titleAlertValue, "idAlert" : 'downInvoice', "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }
})