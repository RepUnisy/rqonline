({
	search : function(component, event) {
        debugger;
        var solicitud = component.get("v.solicitud");
        var pedido = component.get("v.pedido");
        var refClient = component.get("v.refCliente");
        var grado = component.get("v.grado");
        var dateFrom = component.get("v.dateFrom");
        var dateTo = component.get("v.dateTo");
		var estado = component.find("selectionStatusList").get("v.value");
		var destino = component.find("destinoMercancias").get("v.value");
        
        var appEvent = $A.get("e.c:RQO_evt_SearchOrderEvent");
        appEvent.setParams({"solicitud" : solicitud, "pedido" : pedido, "refCliente" : refClient, "grado" : grado, "estado" : estado, "destino" : destino, 
                           "dateFrom" : dateFrom, "dateTo" : dateTo});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	},
    getInfo : function(component, event) {
        var language = event.getParam("language");
		var user = event.getParam("user");
        var idAccount = user.idCliente;
        component.set("v.userHeredado", user);
		var action = component.get('c.getStatusPicklist');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var statusPicklist = [];
                var storedResponse = response.getReturnValue();
                for (var key in storedResponse) {
                    statusPicklist.push({value:storedResponse[key], key:key});
                }
                component.set('v.statusPicklist',statusPicklist);
            }
        }); 
        // enqueue the Action   
        $A.enqueueAction(action);
	},
    onStatusSelectChange: function(component, event) {
        var selected = component.find("selectionStatusList").get("v.value");

	}, 
    onDestinationSelectChange: function(component, event) {
		var selected = component.find("destinoMercancias").get("v.value");

	},
    limpiarBusqueda : function(component, event){
        component.set("v.solicitud", null);
        component.set("v.pedido", null);
        component.set("v.refCliente", null);
        component.set("v.grado", null);
        component.set("v.dateFrom", null);
        component.set("v.dateTo", null);
		component.find("selectionStatusList").set("v.value", "none");
		component.find("destinoMercancias").set("v.value", "");
    }
})