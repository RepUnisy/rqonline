({
	doInitDataProd4 : function(component, event, helper) {
        helper.doInitDataProd4(component, event);
	},
   toggleDescargables : function(component, event, helper) {
		if(window.matchMedia("(max-width:767px)").matches)
		{
            var element = component.find('title');
            $A.util.toggleClass(element,'plegado');
            jQuery('.rq-descargables-toggle').slideToggle();
        }
	}
})