({
    onRender : function(component, event, helper) {
		helper.onRender(component, event);
	},
    currentSlide : function(component, event, helper) {
		helper.currentSlide(component, event);
	},
   	nextSlides : function(component, event, helper) {
		helper.nextSlides(component, event);
	},
    prevSlides : function(component, event, helper) {
		helper.prevSlides(component, event);
	}
})