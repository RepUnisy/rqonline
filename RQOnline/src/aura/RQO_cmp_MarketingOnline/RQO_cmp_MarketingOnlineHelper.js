({
    onRender : function(component, event){
         var sIndex = component.get("v.slideIndex");
		 this.showSlides(sIndex);
     },   
   	 nextSlides : function(component,event) {
     var sIndex = component.get("v.slideIndex");
     var sliders = document.querySelectorAll(".slide");
     var indice = parseInt(sIndex) + 1;
	 
       if(sliders.length >= indice){
         component.set("v.slideIndex",indice);
         this.showSlides(indice); 
       }

   },
    prevSlides : function(component,event) {
     var sIndex = component.get("v.slideIndex");
 	 var indice = parseInt(sIndex) - 1;
        if(indice >= 1)
        {
             component.set("v.slideIndex",indice);
             this.showSlides(indice);
        }
   },
  currentSlide :function(component,event) {
      
     var sIndex = component.get("v.slideIndex");
     var indice = event.target.id;
     component.set("v.slideIndex",indice);
     this.showSlides(indice);
   },
    showSlides : function(indice){
		 var i;
         var slides = document.getElementsByClassName("slide");
         var dots = document.getElementsByClassName("dot");
     
         for (i = 0; i < slides.length; i++) {
             slides[i].style.display = "none";
         }
         for (i = 0; i < dots.length; i++) {
             dots[i].className = dots[i].className.replace(" active", ""); 
         } 
      
         slides[indice-1].style.display = "block"; 
         dots[indice-1].className += " active";
       
    },
})