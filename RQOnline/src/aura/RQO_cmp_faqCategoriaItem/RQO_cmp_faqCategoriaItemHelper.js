({
    cargarLista : function(component, event) {
        
        var action = component.get('c.getFaqsCategorias');
        component.set('v.palabra', event.getParam("texto"));
        var user = component.get('v.user');
        var tipoUsu = (user == null || user.isPrivate == false) ? 'publico' : (user.esCliente == false) ? 'noCliente' : 'cliente';
        action.setParams({"lenguaje" : component.get('v.language'),
                          "palabra" : component.get('v.palabra'),
                          "tipoUsuario" : tipoUsu});
        action.setCallback(this, function(response) {
            //store response state
           
            var state = response.getState();
            var listaAux = [];
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();              
                component.set('v.listCategorias', respuesta);
                component.set('v.predefinido', true);
               // this.marcarPredefinido(component,event);
                this.enviarEvento(respuesta[0], component.get('v.palabra'));
            }else if (state === "INCOMPLETE") {
                //debugger;
                // do something
            }else if (state === "ERROR") {
                //debugger;
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });  
        // enqueue the Action   
        $A.enqueueAction(action);
    },
    
    enviarEvento : function(topic, palabra){    
        var appEvent = $A.get("e.c:RQO_evt_buscadorFaqsList");
        appEvent.setParams({
            "topic" : topic,
        	"palabra" : palabra});
        appEvent.fire();
    },
    marcarPredefinido : function(component, event) {
       var dispositivo =  $A.get("$Browser.formFactor");
        
    	// SI ESTAMOS EN EL ESCRITORIO MARCAMOS EL BOTON PREDEFINIDO
        if(dispositivo == 'DESKTOP')
        {
            if(component.get('v.predefinido')){
                var botones = component.find('botones');
              //$A.util.removeClass(botones[1], 'btn on');   
                $A.util.addClass(botones[1], 'btn on');
                component.set('v.predefinido', false);
            } 
            
        }

    },
})