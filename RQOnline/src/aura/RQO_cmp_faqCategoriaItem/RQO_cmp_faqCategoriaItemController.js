({
    doInit : function(component, event, helper) {
        helper.cargarLista(component, event);
       
    }, 
    recibir : function(component, event, helper) {
        helper.cargarLista(component, event);
    },
    
    seleccionCategoria : function(component, event, helper) {
        debugger;
         var appEvent = $A.get("e.c:RQO_evt_buscadorFaqsList");

        appEvent.setParams({
            "topic" : event.getSource().get("v.label"),
            "palabra" : component.get('v.palabra')});
        appEvent.fire();
        
        //Estilos Frontend
        var botones = component.find('botones');
        for(var i in botones){
           $A.util.removeClass(botones[i], 'on');
        }
        
        $A.util.addClass(event.getSource(), 'btn on');
       
    },
    seleccionCategoriaSelect : function(component, event, helper) {
        var appEvent = $A.get("e.c:RQO_evt_buscadorFaqsList");
        appEvent.setParams({
            "topic" : component.find('categoriasSelect').get('v.value'),
            "palabra" : component.get('v.palabra')});
        appEvent.fire();
    }
})