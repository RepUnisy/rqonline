({
    menuDown : function(component, event, helper) {
        /*------- FRONTEND -----------*/
        // Despliega el menú
        var element = component.find('hamburguer')
        $A.util.toggleClass(element, 'menu-visible');
        jQuery('.menu-down').siblings().slideToggle();
    },
    enlaceExterno : function(component, event, helper) {
        var source = event.getSource();
        var label = source.get("v.label");
        console.log('ACL. ' + label)
        var url;
        
        switch(label) {
            case $A.get("$Label.c.RQO_lbl_laQuimicadeRepsol"): 
                url = $A.get("$Label.c.RQO_lbl_repsolQuimicaUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_agricultura"): 
                url = $A.get("$Label.c.RQO_lbl_agriculturaUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_automocion"): 
                url = $A.get("$Label.c.RQO_lbl_automocionUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_bienestarConsumo"): 
                url = $A.get("$Label.c.RQO_lbl_bienestarUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_contruccionInfraestructuras"): 
                url = $A.get("$Label.c.RQO_lbl_construccionUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_packaging"): 
                url = $A.get("$Label.c.RQO_lbl_envasesUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_salud"): 
                url = $A.get("$Label.c.RQO_lbl_hogar");
                break;
            case $A.get("$Label.c.RQO_lbl_benceno"): 
                url = $A.get("$Label.c.RQO_lbl_bencenoUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_butadieno"): 
                url = $A.get("$Label.c.RQO_lbl_butadienoUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_caucho"): 
                url = $A.get("$Label.c.RQO_lbl_cauchoUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_etileno"): 
                url = $A.get("$Label.c.RQO_lbl_etilenoUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_evaEba"): 
                url = $A.get("$Label.c.RQO_lbl_evaebaUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_estireno"): 
                url = $A.get("$Label.c.RQO_lbl_estirenoUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_oxidodePropileno"): 
                url = $A.get("$Label.c.RQO_lbl_oxipropilenoUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_Polietileno"): 
                url = $A.get("$Label.c.RQO_lbl_polietilenoUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_Poliol"): 
                url = $A.get("$Label.c.RQO_lbl_poliolUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_Polipropileno"): 
                url = $A.get("$Label.c.RQO_lbl_polipropilenoUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_propilenglicolIndustrial"): 
                url = $A.get("$Label.c.RQO_lbl_propilenglicolIndustrialUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_Propileno"): 
                url = $A.get("$Label.c.RQO_lbl_propilenoUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_innovacionCalidad"): 
                url = $A.get("$Label.c.RQO_lbl_innovacionUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_seguridadMedioAmbiente"): 
                url = $A.get("$Label.c.RQO_lbl_seguridadUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_publicacionesCatalogos"): 
                url = $A.get("$Label.c.RQO_lbl_publicacionesUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_quimicaSociedad"): 
                url = $A.get("$Label.c.RQO_lbl_quimicaSociedadUrlExterna");
                break;
            case $A.get("$Label.c.RQO_lbl_quimicaNews"): 
                url = $A.get("$Label.c.RQO_lbl_enlaceQuimicaNews");
                break;
        }        
        
        if (url != null){
            //	Vamos a la url seleccionada
            window.location.href = url;
        }
    },
    gotoURL : function (component, event, helper) {
        var source = event.getSource();
        var label = source.get("v.label");
        //	Buscamos la url de redirección correspondiente
        var url = "";
        switch(label) {
            case $A.get("$Label.c.RQO_lbl_catalogo"): 
                url = "/catalogo"
                break;
            case $A.get("$Label.c.RQO_lbl_preguntasFrecuentes"): 
                url = "/faqs";
                break;
            case $A.get("$Label.c.RQO_lbl_miCatalogo"): 
                url = "/mi-catalogo";
                break;
            case $A.get("$Label.c.RQO_lbl_historial"): 
                url = "/hp";
                break;
            case $A.get("$Label.c.RQO_lbl_seguimiento"): 
                url = "/segp";
                break;
            case $A.get("$Label.c.RQO_lbl_informeCompras"): 
                url = "/con";
                break;
            case $A.get("$Label.c.RQO_lbl_formularioContacto"):  
                url = "/contacto";
                break;
            case $A.get("$Label.c.RQO_lbl_atencionComercial"): 
                url = $A.get("$Label.c.RQO_lbl_atencionComercialUrl");
                break;
            case $A.get("$Label.c.RQO_lbl_inicio"): 
                url = "/hm";
                break;
            case $A.get("$Label.c.RQO_lbl_miPerfil"):
                url = "/perfil"; 
                break;
            case $A.get("$Label.c.RQO_lbl_misPlantillas"): 
                url = "/pl";
                break;
            case $A.get("$Label.c.RQO_lbl_facturas"): 
                url = "/fact";
                break;
            case $A.get("$Label.c.RQO_lbl_crearSolicitud"):  
                url = "/sp";
                break;
        }
       //PARTE FRONT-ANTES DE REDIRECCIONAR
       	//Pliega el menu
       	if(window.matchMedia("(max-width:920px)").matches)
        {
            var hamburguer = component.find('hamburguer')
            $A.util.removeClass(hamburguer, 'menu-visible');
            jQuery('.menu-down').siblings().slideUp();
            
            //Cierra todos los elementos abiertos
            var menuparent = component.find('menuparent');
           
            for(var i in menuparent)
            {
                $A.util.removeClass(menuparent[i], 'active-child');
            }
            //Cierra todos los elementos abiertos
            var padre = component.find('padre');
            
            for(var i in padre)
            {
                $A.util.removeClass(padre[i], 'active-child');
            }
            //Cierra el resto de submenus
            var menuparent2 = component.find('menuparent2');
            
            for(var i in menuparent2)
            {
                $A.util.removeClass(menuparent2[i], 'active-child');
            }
        }
        //FIN PARTE FRONT-END
		
        //	Si tenemos una url para redireccinar, llamamos al evento de redirección
        if (url != ""){
          
            
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url,
                "isredirect" : true
            });
            urlEvent.fire();    
        }
        
    },
    openChild : function(component, event, helper) {
        /*------- FRONTEND -----------*/
        // Abre los elementos hijos del menú
        if(window.matchMedia("(max-width:920px)").matches)
        {
            var elemento = event.getSource();
            $A.util.addClass(elemento,'active-child');   
        }
    },
    goBack : function(component, event, helper) {
        /*------- FRONTEND -----------*/
        //Cierra los elementos hijos del men
        var element = component.find('menuparent');
        for(var i in element)
        {
            $A.util.removeClass(element[i], 'active-child');
        }
    },
    goBackSecond : function(component, event, helper) {
        /*------- FRONTEND -----------*/
        //Cierra los elementos hijos del menu
        var element = component.find('padre');
        for(var i in element)
        {
            $A.util.removeClass(element[i], 'active-child');
        }
    },
    goBackSub : function(component, event, helper) {
        /*------- FRONTEND -----------*/
        //Cierra los elementos hijos del men
        var element = component.find('menupadre2');
        for(var i in element)
        {
            $A.util.removeClass(element[i], 'active-child');
        }
    },
    getInfo : function(component, event, helper) {
        helper.getInfoHerencia(component, event);
      //  helper.getInfo(component, event);
    }
    
    
})