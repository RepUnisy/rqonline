({	/******* No tocar la versión del componente ********
    * 
    * Se ha bajado para poder usar algunas funciones de javascript necesarias para la obtención y propagación del browser
    * https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/security_ls_disable.htm
	*
	****************************************************/
    doInit : function(component, event, helper) {
        helper.getInfoHerencia(component, event, helper);
    },
	registerEventData : function(component, event, helper) {
		helper.registerEventDataHelper(component, event);
	}
})