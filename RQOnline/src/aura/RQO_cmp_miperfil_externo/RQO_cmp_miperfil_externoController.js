({
	doInit : function(component, event, helper) {

    },
    personal : function(component, event, helper) {
        component.set("v.tab", 1);
        var cmpTarget = component.find('personalTab');
        $A.util.addClass(cmpTarget, 'activo');
        cmpTarget = component.find('notificacionesTab');
        $A.util.removeClass(cmpTarget, 'activo');
        cmpTarget = component.find('permisosTab');
        $A.util.removeClass(cmpTarget, 'activo');
    },
    notificaciones : function(component, event, helper) {
        component.set("v.tab", 2);
        var cmpTarget = component.find('notificacionesTab');
        $A.util.addClass(cmpTarget, 'activo');
        cmpTarget = component.find('personalTab');
        $A.util.removeClass(cmpTarget, 'activo');
        cmpTarget = component.find('permisosTab');
        $A.util.removeClass(cmpTarget, 'activo');
    },
    permisos : function(component, event, helper) {
        component.set("v.tab", 3);
        var cmpTarget = component.find('permisosTab');
        $A.util.addClass(cmpTarget, 'activo');
        cmpTarget = component.find('personalTab');
        $A.util.removeClass(cmpTarget, 'activo');
        cmpTarget = component.find('notificacionesTab');
        $A.util.removeClass(cmpTarget, 'activo');
    },
    getUserInfo:  function(component, event, helper) {
		var language = event.getParam("language");
        var user = event.getParam("user");
        
        component.set("v.language", language);
        component.set("v.user", user);
	},
})