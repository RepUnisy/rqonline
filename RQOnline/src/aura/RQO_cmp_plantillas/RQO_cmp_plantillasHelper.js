({
	  getTemplates : function(component) {
          debugger;
          var idContacto = component.get("v.user.idContacto");  
          var idAccount = component.get("v.user.idCliente");  
          var actionTemplate = component.get('c.getTemplates');    
          actionTemplate.setBackground();
          actionTemplate.setAbortable();
          actionTemplate.setParams({ idContact : idContacto, idAccount: idAccount });
          
        actionTemplate.setCallback(this, function(response){
            debugger;
            var state = response.getState();     
            if (state === "SUCCESS") {
                var listBbdd = JSON.parse(response.getReturnValue()); 
                debugger;
                var listPlantillas = new Array();
                //	Solo mostramos 3 plantillas
                var numPlantillas = 3;
                if (listBbdd.length < 3){
                    numPlantillas = listBbdd.length;
                }
                for (var i=0; i<numPlantillas  ; i++){
                    var objPl = {label:listBbdd[i].nombre, value:listBbdd[i].identificador};
                    listPlantillas.push(listBbdd[i]);
                    
                }           
                component.set("v.listPlantillas", listPlantillas);
                          }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }    
            
              //	Ocultamos el spinner de espera
            component.set("v.mostrarSpinner", false);

        });
        $A.enqueueAction(actionTemplate);         
    }
})