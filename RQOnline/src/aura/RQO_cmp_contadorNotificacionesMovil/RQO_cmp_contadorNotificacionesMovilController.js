({
    getInfo : function(component, event, helper) {
		helper.getInfo(component, event);
	},
    irASp : function(component, event, helper){
        var url = '/ntf';
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();
    },
})