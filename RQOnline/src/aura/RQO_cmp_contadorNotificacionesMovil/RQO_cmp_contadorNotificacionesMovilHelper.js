({
	getInfo : function(component, event) {
        var user = component.get("v.user");
        if (user != null && user.idContacto != null){
            
            var IdContacto = user.idContacto;
            var action = component.get('c.countNotificaciones');
            action.setParams({ "IdContacto" : IdContacto});
            
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    var numNotifi = response.getReturnValue();
                    console.log('notificacion' + numNotifi);
                    component.set("v.numNotificaciones", numNotifi);                                    
                }                
            });
            
            $A.enqueueAction(action);  
        }
	}
})