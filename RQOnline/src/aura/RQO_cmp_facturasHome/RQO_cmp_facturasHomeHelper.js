({
	getFacturas : function(component) {
		 var fechaActual = new Date();
        var dateFrom = (fechaActual.getFullYear() - 5) + "-" + ((fechaActual.getMonth() + 1) > 9 ? "" + (fechaActual.getMonth() + 1): "0" + (fechaActual.getMonth() + 1)) + "-" + fechaActual.getDate();
        var dateTo = fechaActual.getFullYear() + "-" + ((fechaActual.getMonth() + 1) > 9 ? "" + (fechaActual.getMonth() + 1): "0" + (fechaActual.getMonth() + 1)) + "-" + fechaActual.getDate();
        var numFacturas = 3;
       
        var usuario = component.get("v.user");
        var idCliente = usuario.idExternoCliente;
       
        var action = component.get('c.getFacturas');
        action.setParams({ idCliente : idCliente, numFactura : null, tipoFactura : null, estado: null, dateFrom : dateFrom, dateTo : dateTo, grado : null, numAlbaran : null, numPedido : null, dateEmisionFrom : null, dateEmisionTo : null, ivNumReg : numFacturas, isHome : true});
        action.setBackground();
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();           
            
            console.log('ACL. Estado getFactura ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var listFacturas = JSON.parse(response.getReturnValue());   
                if (listFacturas.length != 0){
                    if(listFacturas[0].error){
                        component.set("v.error", true);
                    }
                    else{
                         component.set("v.listFactura", listFacturas);                
                    }
                }
                else{
                    component.set("v.vacio", true);
                }
                debugger;
                
               
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            
             //	Ocultamos el spinner de espera
            component.set("v.mostrarSpinner", false);    
        });
        $A.enqueueAction(action);
	}
})