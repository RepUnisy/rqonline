({
    goTo :function(component, event, helper){
        var url = "/fact";
          var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        try{
        	urlEvent.fire();
        }catch(ex){
            console.log(ex);
        }
    },
     getInfo : function(component, event, helper){
        helper.getFacturas(component);
    }
})