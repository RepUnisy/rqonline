({
    doInit : function(component, event, helper) {
		helper.getInfoHerencia(component, event, helper);
	},
    
    getRelacion : function(component, event, helper) {
        
        //	Obtenemos el mapa del usuario
        var objectSuEquipo = component.get("v.user.mapSuEquipo");
        var mapSuEquipo = new Map(Object.entries(objectSuEquipo));
        //	Asignamos el mapa en la variable
        component.set("v.mapRelaciones", mapSuEquipo);
        
        var listKeys = Array.from( mapSuEquipo.keys()) ;
        
        if (listKeys.length > 0){
            
            component.set("v.privado", true);
            component.set("v.seleccionadoAreaVenta", listKeys[0]);

            helper.traducirListadoLabel(component, listKeys);            
            
            helper.asignarListaRelaciones(component, listKeys[0]);
        }

	},
    
    changeSeleccionAreaVenta : function(component, event, helper) {
        debugger;
        var seleccionado = component.get("v.seleccionadoAreaVenta");
        helper.asignarListaRelaciones(component, seleccionado);
    },
    
    toggle: function(component, event, helper) {
        /*PARTE FRONTEND*/
        //	var btn = component.find('btn-toggle');
       // $A.util.toggleClass(btn, 'fa-chevron-up'); 
     	//$A.util.toggleClass(btn, 'fa-chevron-down');
     	debugger;
        jQuery('.rq-banner-lateral-cuerpo').addClass('adsfasfasfd');
        jQuery('.rq-banner-lateral-cuerpo').slideToggle();
       
	}
})