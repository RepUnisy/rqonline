({
     asignarListaRelaciones : function (component, key){
        debugger;
         var mapSuEquipo = component.get("v.mapRelaciones");
         
         var list = mapSuEquipo.get(key);                
         component.set("v.listRelaciones", list);
     },
    
    traducirListadoLabel : function (component, listKeys){
       
        var listTraducido = Array();
        for (var i = 0; i<listKeys.length; i++){
            try{
                var area = $A.get("$Label.c.RQO_lbl_areaVenta" + listKeys[i]) != '' ? $A.get("$Label.c.RQO_lbl_areaVenta" + listKeys[i]) : listKeys[i];
            }
            catch(exp){
                var area = listKeys[i];
            }
           
            
            var selector = new Object();
            selector.label = listKeys[i];
            selector.value =  listKeys[i];    
            
             listTraducido.push(selector);
        }
        component.set("v.listAreasVenta", listTraducido);
        
    }
    
})