({
    init : function(component, event, helper) {
        
        var mapaObt = component.get("v.mapaResumenHeredada");
        var keyObt = component.get("v.idResumenHeredada");
        //	Obtenemos y asignamos la tabla
        var lista = mapaObt[keyObt];
        var destinoMercancias = '';
        var dirFacturacion = '';
        if (lista.length != 0){
            for (var i = 0; i<1; i++){
                destinoMercancias = lista[i].dirEnvio;
                dirFacturacion = lista[i].direccionFacturacion;
            }
        }

        //	Obtenemos
        var usuario = component.get("v.userHeredado");
        var destino;
        var facturacion;
        
        if (usuario.idCliente == destinoMercancias){
            //	Llamamos a la funcion del helper para obtener la estructura del destino
            destino =  helper.getInfoUser(usuario);            
        }
        else{
            //	Llamamos a la funcion del helper para buscar el destino buscado
            destino = helper.getInfoObject(usuario.listDirMercancias, destinoMercancias);            
        }
        
        //	Obtenemos la direccion de facturacion
        if (usuario.idCliente == dirFacturacion){
            //	Llamamos a la funcion del helpr para obtener la estructura del destino
            facturacion = helper.getInfoUser(usuario);
            
        }
        else{
            //	Llamamos a la funcion del helper para buscar el destino buscado
            destino = helper.getInfoObject(usuario.listDirFacturacion, dirFacturacion);            
        }
        
        //	Almacenamos la información obtenida
        component.set("v.dirFacturacion", facturacion);
        component.set("v.destinoMercancias", destino);
	}
})