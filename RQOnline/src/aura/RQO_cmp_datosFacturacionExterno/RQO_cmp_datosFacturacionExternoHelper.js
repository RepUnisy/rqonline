({
    getInfoUser : function(usuario) {
        var nuevoObjeto = {name: usuario.cliente.Name, 
                           nameOrg3: usuario.cliente.RQO_fld_name3__c, 
                           calle: usuario.cliente.RQO_fld_calleNumero__c,
                           city: usuario.cliente.RQO_fld_poblacion__c, 
                           country: usuario.cliente.RQO_fld_pais__c
                          };
        return nuevoObjeto;
    },
    getInfoObject : function(listaDestinoMercancias, idComparar) {
        
        for(var i = 0; i< listaDestinoMercancias.length; i++ ){
            if (listaDestinoMercancias[i].RQO_fld_cliente__c == idComparar){
                var nuevoObjeto = {name: listaDestinoMercancias[i].RQO_fld_cliente__r.Name, 
                                   nameOrg3: listaDestinoMercancias[i].RQO_fld_cliente__r.RQO_fld_name3__c, 
                                   calle: listaDestinoMercancias[i].RQO_fld_calleNumero__c,
                                   city: listaDestinoMercancias[i].RQO_fld_city__c, 
                                   country: listaDestinoMercancias[i].RQO_fld_country__c
                                  };
                return nuevoObjeto;
            }
        }
        
    }
})