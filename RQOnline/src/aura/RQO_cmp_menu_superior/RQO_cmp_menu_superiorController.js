({ 
    getInfo : function(component, event, helper) {
        helper.getInfo(component, event);
    },
    getInfoController : function(cmp, event, helper) {
        helper.getInfoController(cmp, event);
    },
    almacenarPeticion : function(cmp, event, helper) {
        helper.almacenarPeticion(cmp, event);
    },
    irASp : function(component, event, helper){
        var url = '/sp';
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();
    },
    login : function(component, event, helper) {
        helper.login(component);
    }
})