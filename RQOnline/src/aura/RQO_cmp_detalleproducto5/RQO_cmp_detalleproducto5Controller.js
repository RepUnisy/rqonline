({
    doInit : function(component, event, helper) {
        var language = component.get("v.languageHeredado");
        helper.getTranslateEnvaseUnidad(component, language);
    },
    getInfoEnvases : function(component, event, helper) {
        debugger;
        
        if (component.get("v.id") != null && component.get("v.user.isPrivate")){
            if (component.get("v.mostrarDetalleProductoHeredado")){
                helper.inicializarFechaSolicitud(component);
            }           
        }
        else{
             component.set("v.mostrarComponente", true);
        }
         helper.getInfo(component, event);
    },
    changeMapTraduccion : function(component, event, helper) {
      
    },
    dependant : function(component, event, helper) {
        
        debugger; 
        
        var envase = component.get("v.envase");
        var mapModoEnvio = component.get("v.mapModoEnvio");
        var mapaIdEnvasesTP = component.get("v.mapaIdEnvasesTP");
        var mapaIdModosEnvio = component.get("v.mapaIdModosEnvio");
        var traduccionCode =  component.get("v.mapaTraduccionCofigo");
        var selectModoEnvio = [];
        
        
        var listModoEnvio = mapModoEnvio[mapaIdEnvasesTP[envase]];
        debugger;
        for(var i in listModoEnvio){
            var code = traduccionCode[listModoEnvio[i]] != null ? traduccionCode[listModoEnvio[i]] : listModoEnvio[i];
            selectModoEnvio.push({value : mapaIdModosEnvio[code], label : code});
        }
        
          debugger;    
        component.set("v.modoEnvio", selectModoEnvio.pop().value)
        component.set("v.selectModoEnvio", selectModoEnvio);
    },
    changeEnvase : function(component, event, helper) {
        var envase = component.get("v.envase");
        helper.setListUnidadMedida(component, envase);
    },
    
    
    introducir : function(component, event, helper) {
			helper.introducir(component, event);
	},
    mostrarPopup : function(component, event, helper) {
			helper.mostrarPopup(component, event);
	},
    handleResponseAlert : function(component, event, helper){
        var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");
      // this.showSpinner(component, 'slds-hide', 'slds-show');
    }, 
    showSpinner : function(component, classToAdd, classToremove){
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, classToremove);
        $A.util.addClass(spinner, classToAdd);
	},
    solicitar : function(component, classToAdd, classToremove){
        debugger;
                var url = "/contacto";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();     
    },
    toggle : function(component, event, helper) {
        if(window.matchMedia("(max-width:767px)").matches)
        {
            var element = component.find('title');
            $A.util.toggleClass(element,'plegado');
            jQuery('.wrapper-producto5').slideToggle();
        }
	},
})