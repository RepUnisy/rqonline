({
    getInfo : function(component, event) { 
        var id = component.get("v.id");
        var action = component.get('c.getToCreateRequest');
        var language = component.get("v.languageHeredado");
        action.setParams({ "idGrado" : id, "language" : language });
        
        action.setCallback(this, function(response) { 
            var state = response.getState();
            if (state === "SUCCESS") {
            
                var pedido = JSON.parse(response.getReturnValue());
                
                var crearPedido = pedido.crearPedido;                
                var modoEnvio = pedido.modoEnvio;
                var selectEnvases = pedido.selectEnvases;
                var selectModoEnvio = pedido.selectModoEnvio;
                var mapModoEnvio = pedido.mapModoEnvio;
                var mapaIdEnvases = pedido.mapaIdEnvases;
                var mapaIdEnvasesTP = pedido.mapaIdEnvasesTP;
                var mapaIdModosEnvio = pedido.mapaIdModosEnvio;                
                

                
                component.set("v.crearPedido", crearPedido);	
                component.set("v.mapModoEnvio", mapModoEnvio);
               
                component.set("v.isCargado", true);
                component.set("v.mapaIdEnvases", pedido.mapaIdEnvases);
                component.set("v.mapaIdEnvasesTP", pedido.mapaIdEnvasesTP);
                component.set("v.mapaIdModosEnvio", pedido.mapaIdModosEnvio);
                component.set("v.mapaTraduccionCofigo", pedido.mapTradEnvaseUidadMedida);
                
                component.set("v.selectEnvases", selectEnvases);
                component.set("v.selectModoEnvio", selectModoEnvio);   
                
                var listEnvases = this.setListEnvases(component, pedido.listIdsEnvases);
                
                var envase = pedido.listIdsEnvases.length > 0 ? pedido.listIdsEnvases[0] : null;
               // component.set("v.envase", envase);
                debugger;
                this.setListUnidadMedida(component, null);                              
                
            }
             component.set("v.mostrarComponente", true);
        }); 
        $A.enqueueAction(action);
        
    },
     mostrarPopup : function(component, event){
         component.set('v.visible',true);
     },
    introducir : function(component, event){
        var user = component.get("v.user");
        var idAccount = user.idCliente;
        var idContact = user.idContacto;
        var incoterm =  user.listIncoterm[0] != null ? user.listIncoterm[0] : '';
        
        var idGrado = component.get("v.id");
        
        var mapaIdEnvases = component.get("v.mapaIdEnvases");
        var mapaIdModosEnvio = component.get("v.mapaIdModosEnvio");
        
        var listEnvases = component.get("v.listEnvases");
        var envaseEnviar = component.get("v.envase");
        
        var mapModoEnvio = component.get("v.mapModoEnvio");
        var unidadMedida = component.get("v.unidadMedida");
               
        var fechaEnvio = component.get("v.fechaEnvio");
      
        var intCantidad = component.get("v.intCantidad");
        var action = component.get('c.insertAsset');
        action.setParams({ "idGrado" : idGrado, "AccountId" : idAccount, "ContactId" : idContact, "envase" : envaseEnviar, "modoEnvio" : unidadMedida,  "fechaEnvio" : fechaEnvio, "cantidad" : intCantidad, "incoterm": incoterm });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var eventoNotificaciones = $A.get("e.c:RQO_evt_sumarUnoANotificaciones");
                eventoNotificaciones.setParams({"numSumar": 1});
                eventoNotificaciones.fire();
                var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
                appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_mensajeCorrectamente"), "confirmAlert" : false, 
                                    "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), "idAlert" : 'gradoInsertado', "parentLE" : false});
                try{
                    appEvent.fire();
                }catch(err){
                   // this.showSpinner(component);
                    console.log(err.message);
                }
            }else{
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        }); 
        $A.enqueueAction(action);
        
    },
    
    inicializarFechaSolicitud : function(component){
        
        var today = new Date();
        //	Comprobamos y asignamos para que el mes tenga 2 digitos (01...12)
        var mesActual = (today.getMonth() + 1).toString();
        if (mesActual.length != 2){
            mesActual = '0' + mesActual;
        }
        //	Comprobamos y asignamos para que el dia tenga 2 digitos (01...31)
        var diaActual =  today.getDate().toString()
        if (diaActual.length != 2){
            diaActual = '0' + diaActual;
        }
        component.set('v.fechaEnvio', today.getFullYear() + "-" + mesActual + "-" + diaActual);
    }
    
})