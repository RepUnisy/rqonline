({
    getCatPropuesto : function(component, event) {
        var IdCliente = component.get("v.user.idCliente");
        var IdContacto = component.get("v.user.idContacto");
        var action = component.get('c.volverProponer');
        action.setParams({ "IdContacto" : IdContacto , idAccoun : IdCliente});
        console.log('ACL. proponer pedido para: ' + IdContacto)
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                if (response.getReturnValue() == true){
                    console.log('ACL. Catálogo propuesto')
                    component.set("v.catalogoAsignado", true);
                }
                else{
                    console.log('ACL. No se ha encontrado nigun assert: Cliente: ' + IdCliente  )
                }                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                } 
            var spinner = component.find("spinner");        
            $A.util.toggleClass(spinner, "slds-show"); 
        });
        $A.enqueueAction(action);  
    }
})