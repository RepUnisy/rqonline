({	/******* No tocar la versión del componente ********
    * 
    * Se ha bajado para poder usar algunas funciones de javascript necesarias para la obtención y propagación del browser
    * https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/security_ls_disable.htm
	*
	****************************************************/
	registerEventDataHelper : function(component, event) {
        var pathName = event.getParam("rePathActual");
        //El path que se manda en el evento tiene que coincidir con el path del ambito actual del componente. Para no duplicar 
        //eventos 
        if (window.location.pathname === pathName){
            var dataComponentName = event.getParam("reComponentName");
            var dataComponentDescription = event.getParam("reComponentDescription");
            var dataActionType = event.getParam("actionType");
            var dataActionDescription = event.getParam("actionDescription");
            var dataGrade = event.getParam("reGradeId");
            var dataDocument = event.getParam("reDocumentId");
            var dataPedido = event.getParam("rePedidoId");
            var dataComunicacion = event.getParam("reComunicacionId");
            var dataFaq = event.getParam("reFaqId");
            var dataNumFactura = event.getParam("reNumFactura");
            var action = component.get('c.createEvent');
            var idCategoria = event.getParam("reIdCategoria");
            var searchData = event.getParam("reSearchData");
            var idioma = component.get("v.language");
            var requestId = event.getParam("reRequestId");
            var browser = this.browserDetection();
            var device = this.deviceDetection();
            
            action.setParams({"componentName" : dataComponentName,  "componentDescription" : dataComponentDescription, 
                              "actionType" : dataActionType, "actionDescription" : dataActionDescription, 
                              "gradeId" : dataGrade, "documentId" : dataDocument,
                              "pedidoId" : dataPedido, "comunicacionId" : dataComunicacion,
                              "faqId" : dataFaq, "numFactura" : dataNumFactura, "idCategoria" : idCategoria,
                              "searchData" : searchData, "idioma" : idioma, "requestId" : requestId, "browser" : browser,
                              "device" : device});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storedResponse = response.getReturnValue();
                    if (storedResponse == 'OK'){
                        console.log('Registro creado');
                    }else{
                        console.log('Error en la creación de registro: ' + storedResponse);
                    }
                }else{
                    console.log('Error en la creación de registro: ' + state);
                }
            });
            $A.enqueueAction(action);
		}
	}, browserDetection : function () {
        var browser = '';
        // Opera 8.0+
        var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';
        // Safari 3.0+ "[object HTMLElementConstructor]" 
        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
        // Internet Explorer 6-11
       	var isIE = /*@cc_on!@*/false || !!document.documentMode;
        // Chrome 1+
        var isChrome = !!window.chrome && !!window.chrome.webstore;
        // Edge 20+
        //var isEdge = !isIE && !!window.StyleMedia;
        // Blink engine detection
        //var isBlink = (isChrome || isOpera) && !!window.CSS;
        if (isOpera){
            browser = 'Opera';
        }else if(isFirefox){
            browser = 'Firefox';
        }else if(isSafari){
            browser = 'Safari';
        }else if(isIE){
            browser = 'IE';
        }else if(isChrome){
            browser = 'Chrome';
        }
        return browser;
    }, deviceDetection : function(){
        var device = '';
        //Obtenemos el formato donde estamos visualizando y formateamos los elementos visibles - DESKTOP, PHONE, TABLET
        var formFactor = $A.get("$Browser.formFactor");
        switch(formFactor) {
            case 'PHONE':
                device = '1'; //Teléfono
                break;
            case 'TABLET':
                device = '2'; //Tablet
                break;
            default:
                device = '3'; // PC
        }
        return device;
	}
})