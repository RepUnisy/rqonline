({
    changeUser : function(component, event) {
        var user = component.get("v.user");
        var eventoLanzado = component.get("v.eventProduct");
        if(user && user.isPrivate && !eventoLanzado){
            this.registrarEvento(component, '3', '1', null);
            component.set("v.eventProduct", true);
        }
    },
    registrarEvento: function(component, componentDescription, actionDescription, idCategoria){
        /*
         * Registro de los eventos de visualización de categorías y visualización de los grados de una categoría
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         * 	Valor 3 = List of products (Visualización de las categorías)
         * 	Valor 4 = List of Grades (Visualización de los grados de una categoría)
         * actionType - Grupo al que pertenece, 1 = Product Catalogue
         * actionDescription - descripción de la acción que lanza el evento. En este caso visualización de categorías o de
         * lista de grados.Valores para este componente:
         * 	Valor 1 = Query products page
         * 	Valor 2 = Query grade list
         * reIdCategoria - id de la categoría a la que pertenecen los grados cuando se está visualizando el listado de grados
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_listaCategoriasTablaGrados', "reComponentDescription" : componentDescription, 
                            "actionType" : '1', "actionDescription" : actionDescription, "reIdCategoria" : idCategoria,
                            "rePathActual" : window.location.pathname});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})