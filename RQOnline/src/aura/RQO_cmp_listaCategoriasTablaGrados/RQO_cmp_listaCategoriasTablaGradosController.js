({
	doInit : function(component, event, helper) {
        helper.getInfoHerencia(component, event);
        component.set("v.catalogo", true);
        component.set("v.varEsconderCatalogo", true);
	},
    cargaTabla : function(component, event, helper) {
        
        console.log('Entra a cargaTabla.');
        var mapatabla = event.getParam("mapaTabla");
        component.set("v.mapaTabla", event.getParam("mapaTabla"));
        console.log('Este es el resultado actual: ' + component.get("v.mapaTabla"));
        var user = component.get("v.user");
        var eventoLanzado = component.get("v.eventGrade");
        if(user && user.isPrivate && !eventoLanzado){
            helper.registrarEvento(component, '4', '2', mapatabla.categoria);
            component.set("v.eventGrade", true);
        }
	},
    esconderCatalogo : function(component, event, helper){
		component.set("v.varEsconderCatalogo", event.getParam("boolEsconderCatalogo"));
    },
    changeUser : function(component, event, helper) {
        helper.changeUser(component, event);
    }
})