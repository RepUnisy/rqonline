({
	getInfo : function(component, event) {
		var language = event.getParam("language");
		var user = event.getParam("user");
        
		// Establecer permisos
		if (user.permissionUrls.includes("/sp"))
			component.set("v.hasAccessSolicitudPedido", true);
		if (user.permissionUrls.includes("/fact"))
			component.set("v.hasAccessFacturas", true);
	},       
})