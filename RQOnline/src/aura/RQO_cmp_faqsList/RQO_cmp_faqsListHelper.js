({
    cargarLista : function(component, event) {
        debugger;
        var action = component.get('c.getFaqsPreguntas');
        var topic = event.getParam("topic");
        var palabra = event.getParam("palabra");
        var user = component.get('v.user');
        component.set('v.palabra', palabra);
        var tipoUsu = (user == null || user.isPrivate == false) ? 'publico' : (user.esCliente == false) ? 'noCliente' : 'cliente';
        
        
        action.setParams({"lenguaje" : component.get("v.language"),
                          "topic" : event.getParam("topic"),
                          "palabra" : component.get('v.palabra'),
                          "tipoUsuario" : tipoUsu});
        action.setCallback(this, function(response, topic) {
            //store response state 
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                var listaAux = [];
                
                for(var i in respuesta){
                    listaAux.push({"RQO_fld_question__c" : String(respuesta[i].RQO_fld_question__c).replace(component.get('v.palabra'), '<b>'+component.get('v.palabra')+'</b>'), 
                        		   "RQO_fld_answer__c" : String(respuesta[i].RQO_fld_answer__c).replace(component.get('v.palabra'), '<b>'+component.get('v.palabra')+'</b>')});
                }
                component.set('v.listPreguntas', listaAux);
            }else if (state === "INCOMPLETE") {
                // do something
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });  
        // enqueue the Action   
        $A.enqueueAction(action);
    }
})