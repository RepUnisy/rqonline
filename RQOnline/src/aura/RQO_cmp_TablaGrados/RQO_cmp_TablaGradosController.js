({
	cargaTabla : function(component, event, helper) {
        debugger;
        helper.cargaTabla(component, event);
	},
    doInit : function(component, event, helper) {
        helper.doInit(component, event);
	},
    ordenarCatalogoo : function(component, event, helper){
        
        var nombre = event.getParam("nombre");
        var idCategoriaTabla = event.getParam("idEvtCategoriaTabla");
        var idAnteriorOrdenacion = component.get("v.idAnteriorOrdenacion");
        var contadorOrdenacion = component.get("v.contadorOrdenacion");
        
        if(idAnteriorOrdenacion == idCategoriaTabla){
            contadorOrdenacion++;
            component.set("v.contadorOrdenacion", contadorOrdenacion);
            if(nombre){
                /*Front*/
                jQuery('th span').removeClass('fa fa-caret-down fa-caret-up');
                /*Front*/
                if(contadorOrdenacion % 2 == 0){
                   /*Front*/
                    jQuery('th:first-child span').removeClass('fa fa-caret-up').addClass('fa fa-caret-down');
                    /*Front*/
                    helper.ordenarCatalogoNombreDescendente(component, event);
                }
                else{
                    /*Front*/
                    jQuery('th:first-child span').removeClass('fa fa-caret-down').addClass('fa fa-caret-up');
                    /*Front*/
                    helper.ordenarCatalogoNombre(component, event);
                }
            }
            else{
                if(contadorOrdenacion % 2 == 0){
    				
                    helper.ordenarCatalogoEspecificacionesDescendente(component, event);
                }
                else{
                    helper.ordenarCatalogoEspecificaciones(component, event);
                }
            }
        }
        else{
            contadorOrdenacion = 0;
            component.set("v.contadorOrdenacion", contadorOrdenacion);
            idAnteriorOrdenacion = idCategoriaTabla;
            component.set("v.idAnteriorOrdenacion", idAnteriorOrdenacion);
            if(nombre){
                helper.ordenarCatalogoNombre(component, event);
            }
            else{
                helper.ordenarCatalogoEspecificaciones(component, event);
            }
        }
    },
    ordenacionEspecificacionesNombre : function(component, event, helper) {   
        var eventoOrdenarTabla = $A.get("e.c:RQO_evt_ordenarEspecificacionTabla");
        eventoOrdenarTabla.setParams({"nombre" : true,
                                      "idEvtCategoriaTabla" : component.get("v.idCategoriaTabla")});
        eventoOrdenarTabla.fire();
        
         /*PARTE FRONT-END*/
		 	var element = event.currentTarget;
        	jQuery(element).addClass('active-order');
        	jQuery(element).closest('table').addClass('active-table');
        	jQuery('.active-table td').removeClass('sortable-active');
        	jQuery('.active-table th').removeClass('sortable-active');
        	var indice = jQuery('.active-order').index();
        	component.set('v.OrdenacionActual',indice);
     
			
       	 setTimeout(function(){
             jQuery('.active-table tbody td').removeClass('sortable-active');
             jQuery('.active-order').addClass('sortable-active');
             
                jQuery('.active-table tbody td').each(function()
				{
                    
              	 if(jQuery(this).index() == indice)
				   {
               		 jQuery(this).addClass('sortable-active');
            	   }
				});  
                
        	 jQuery('.active-table td').removeClass('active-order');
       		 jQuery(element).closest('table').removeClass('active-table');
              }, 100);

        /* FIN FRONT-END */
        
	}
})