({

   advanceSearch : function(component, event, helper) {
       var btn = component.find('advanceSearch');
       $A.util.toggleClass(btn, 'open');
       $A.util.toggleClass(btn, 'fa-angle-down');
       $A.util.toggleClass(btn, 'fa-angle-up');
		
        //jQuery('.rq-search-advance').toggleClass('open fa-angle-down fa-angle-up');
        jQuery('.rq-input-inf').slideToggle();
	}, search : function(component, event, helper) {
		helper.search(component, event);
	}, doInit : function(component, event, helper) {
		helper.getInfoHerencia(component, event, helper);
	}, getInfo : function(component, event, helper) {
		helper.getInfo(component, event);
	}, onStatusSelectChange: function(component, event, helper) {
		helper.onStatusSelectChange(component, event);
	}, onDestinationSelectChange: function(component, event, helper) {
		helper.onDestinationSelectChange(component, event);
    }, limpiarBusqueda : function(component, event, helper) {
        helper.limpiarBusqueda(component, event);
    },vaciarLista : function(component, event, helper) {
        //setTimeout(function(){ component.set("v.listaGradosCompleta", []); }, 150);
        setTimeout(function(){ component.set("v.predictiveGradeList", []); }, 150);
    }, getPredictiveData : function(component, event, helper){
        helper.getPredictiveData(component, event);
    }, rellenarBuscador : function(component, event, helper) {
        debugger;
        var texto = event.getParam("texto");
        //component.find('v.grado').set("v.value", texto);
        component.set("v.grado", texto);
        component.set("v.predictiveGradeList", []);
	},
})