({
	search : function(component, event) {
        component.find("fechaPreferenteFromValue").set("v.errors", null);
        component.find("fechaPreferenteToValue").set("v.errors", null);
        var solicitud = component.get("v.solicitud");
        var pedido = component.get("v.pedido");
		var refClient = component.get("v.refCliente");
        var grado = component.get("v.grado");
        var fechaFrom = component.get("v.dateFrom");
        var fechaTo = component.get("v.dateTo");
        var fechaPreferenteFrom = component.get("v.fechaPreferenteFrom");
        var fechaPreferenteTo = component.get("v.fechaPreferenteTo");
        var estado = component.find("selectionStatusList").get("v.value");
        var destino = component.find("destinoMercancias").get("v.value");
        //Validamos que rellene al menos un campo de búsqueda
        if (!solicitud && !pedido && !refClient && !grado && !fechaFrom && !fechaTo && (!estado || estado == 'none') && !destino
           && !fechaPreferenteFrom && !fechaPreferenteTo){
            this.fireAlertEvent($A.get("$Label.c.RQO_lbl_seleccionFiltroBusqueda"), $A.get("$Label.c.RQO_lbl_informacion"));
            return false;
        }
        
        var validado = this.validaciones(component);
        if (!validado){return;}
        
        if (solicitud){solicitud = solicitud.padStart(10, "0");}
        if (pedido){pedido = pedido.padStart(10, "0");}
        //Formateo de las fechas de búsqueda
        var datePreferentFrom = null;
        if (fechaPreferenteFrom){
            var datePreferentFromAux = new Date(component.get("v.fechaPreferenteFrom"));
            var monthPreferentFrom = datePreferentFromAux.getMonth() + 1;
            datePreferentFrom = datePreferentFromAux.getFullYear() + "-" + monthPreferentFrom.toString().padStart(2, "0") + "-" + datePreferentFromAux.getDate();
        }
        var datePreferentTo = null;
        if (fechaPreferenteTo){
            var datePreferentToAux = new Date(component.get("v.fechaPreferenteTo"));
            var monthPreferentTo = datePreferentToAux.getMonth() + 1;
            datePreferentTo = datePreferentToAux.getFullYear() + "-" + monthPreferentTo.toString().padStart(2, "0") + "-" + datePreferentToAux.getDate();
        }
        var dateFrom = null;
        if (fechaFrom){
        	var dateFromAux = new Date(component.get("v.dateFrom"));
            var monthFrom = dateFromAux.getMonth() + 1;
            dateFrom = dateFromAux.getFullYear() + "-" + monthFrom.toString().padStart(2, "0") + "-" + dateFromAux.getDate();
        }
        var dateTo = null;
        if (fechaTo){
            var dateToAux = new Date(component.get("v.dateTo"));
            var monthTo = dateToAux.getMonth() + 1;
            dateTo = dateToAux.getFullYear() + "-" + monthTo.toString().padStart(2, "0") + "-" + dateToAux.getDate();
        }
        
        var appEvent = $A.get("e.c:RQO_evt_searchOrder");
        appEvent.setParams({"solicitud" : solicitud, "pedido" : pedido, "refCliente" : refClient, "grado" : grado, "estado" : estado, "destino" : destino, 
                            "dateFrom" : dateFrom, "dateTo" : dateTo, "datePreferentDeliveryFrom" : datePreferentFrom, 
                            "datePreferentDeliveryTo" : datePreferentTo});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	},
    getInfo : function(component, event) {
        var fechasFiltro = false;
        //Comprobamos con una expresion regular si se han pasado parámetros por la url y en ese caso se establecen en el filtro
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
				function(m,key,value) {
					console.log(key + ":"+value);
                    if(key=="solicitud"){
                        component.set("v.solicitud", value);
                    }
                    if(key=="pedido"){
                        component.set("v.pedido", value);
                    }
                    if(key=="fecha"){
                        component.set("v.fechaPreferenteFrom", value);
                        component.set("v.fechaPreferenteTo", value);
                       	fechasFiltro = true;
                    }
			});          
        /*var language = event.getParam("language");
		var user = event.getParam("user");*/
        var language = component.get("v.language");
		var user = component.get("v.user");
        var idAccount = user.idCliente;
        //component.set("v.userHeredado", user);
        var origen = component.get("v.origen");
		var action = component.get('c.getStatusPicklist');
        action.setParams({"origin" : origen});  
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var statusPicklist = [];
                var storedResponse = response.getReturnValue();
                for (var key in storedResponse) {
                    statusPicklist.push({value:storedResponse[key], key:key});
                }
                component.set('v.statusPicklist',statusPicklist);
            }
        }); 
        // enqueue the Action   
        $A.enqueueAction(action);
        //Si no han llegado fechas por el filtro vamos a buscarlas
        if (!fechasFiltro){
            var origen = component.get("v.origen");
            //Vamos a buscar el rango de meses parametrizado para el histórico/seguimiento
            var actionParametrized = component.get('c.getDateParametrizedData');
            actionParametrized.setParams({"origen" : origen});
            actionParametrized.setBackground();
            actionParametrized.setAbortable();
            actionParametrized.setCallback(this, function(responseParametrized) {
                var stateParametrized = responseParametrized.getState();
                var addMonths = 3; 
                if (stateParametrized === "SUCCESS") {
                    addMonths = responseParametrized.getReturnValue();
                }
                component.set("v.addMonths", addMonths);
                this.setFechasPreferentes(component, addMonths);
            }); 
            // enqueue the Action   
            $A.enqueueAction(actionParametrized); 
		}
	},
    onStatusSelectChange: function(component, event) {
        var selected = component.find("selectionStatusList").get("v.value");

	}, 
    onDestinationSelectChange: function(component, event) {
		var selected = component.find("destinoMercancias").get("v.value");

	}, limpiarBusqueda : function(component, event){
        component.set("v.solicitud", null);
        component.set("v.pedido", null);
        component.set("v.refCliente", null);
        component.set("v.grado", null);
        component.set("v.dateFrom", null);
        component.set("v.dateTo", null);
        var addMonths = component.get("v.addMonths");
        this.setFechasPreferentes(component, addMonths);
        
		component.find("selectionStatusList").set("v.value", "none");
		component.find("destinoMercancias").set("v.value", "");
    }, getPredictiveData : function(component, event) {
		var action = component.get("c.getAllGrades");
		//var user = component.get("v.userHeredado");
		var user = component.get("v.user");
		var searchKey = component.find('filtroGrado').get('v.value');
        action.setParams({"accountId" : user.idCliente, "searchKey" : searchKey});  
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var grados = response.getReturnValue();
                if (grados){
                 	component.set("v.predictiveGradeList", grados);   
                }else{
                    component.set("v.predictiveGradeList", []);
                }
            }else{
             	component.set("v.predictiveGradeList", []);
            }
        });
        $A.enqueueAction(action);
    }, fireAlertEvent : function(textAlert, titleAlertValue){
        
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : textAlert, "confirmAlert" : false, 
                            "titleAlertValue" : titleAlertValue, "idAlert" : 'searchConsumption', "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }, validaciones : function(component){
        var validado = true;
        //Vamos a validar las fechas
        //Fecha preferente de entrega      
        var inputFechaPreferenteFrom = component.find("fechaPreferenteFromValue");
        var valueFechaPreferenteFrom = inputFechaPreferenteFrom.get("v.value");
        var inputFechaPreferenteTo= component.find("fechaPreferenteToValue");
        var valueFechaPreferenteTo = inputFechaPreferenteTo.get("v.value");

        
        if (!valueFechaPreferenteFrom){
            inputFechaPreferenteFrom.set("v.errors", [{message: $A.get("$Label.c.RQO_lbl_introducirFechaDesde")}]);
            return false;
        }
        if (!valueFechaPreferenteTo){
            inputFechaPreferenteTo.set("v.errors", [{message: $A.get("$Label.c.RQO_lbl_introducirFechaHasta")}]);
            return false;
        }
        var addMonths = component.get("v.addMonths");
        validado = this.validarFechas(valueFechaPreferenteFrom, valueFechaPreferenteTo, addMonths);
        if (!validado){return false;}
        
        //Fecha de entrega
        var inputDateFrom = component.find("dateFromValue");
        var value = inputDateFrom.get("v.value");
        var inputDateTo= component.find("dateToValue");
        var valueTo = inputDateTo.get("v.value");
        
        //Si tengo al menos una fecha rellenada voy a por sus validaciones
        if (value || valueTo){
			validado = this.validarFechas(value, valueTo, 3);
        }
        return validado;
    }, validarFechas : function(value, valueTo, addMonths){
        
        //Si alguna de las dos fechas está vacía sacamos el error correspondiente
        if ((value && !valueTo) || (!value && valueTo)) {
            var message = $A.get("$Label.c.RQO_lbl_introducirFechaDesde");
            if (!valueTo){message = $A.get("$Label.c.RQO_lbl_introducirFechaHasta");}
            this.fireAlertEvent(message, $A.get("$Label.c.RQO_lbl_informacion"));
            return false;
        }
        
        var fechaDate = new Date(value);
        fechaDate.setHours(0);
        fechaDate.setMinutes(0);
        fechaDate.setSeconds(0);        
        var fechaDateTo = new Date(valueTo);
        fechaDateTo.setHours(0);
        fechaDateTo.setMinutes(0);
        fechaDateTo.setSeconds(0); 
        if (fechaDate > fechaDateTo){
            this.fireAlertEvent($A.get("$Label.c.RQO_lbl_mensajeValidacionFecha"), $A.get("$Label.c.RQO_lbl_informacion"));
			return false;
        }
       	var mes = fechaDateTo.getMonth()-addMonths;
        fechaDateTo.setMonth(mes);
        if (fechaDate < fechaDateTo){
            var mensajeRango = $A.get("$Label.c.RQO_lbl_mensajeFechaHistorico");
            if (mensajeRango){mensajeRango = mensajeRango.replace('<PARAM>', addMonths);}
            this.fireAlertEvent(mensajeRango,  $A.get("$Label.c.RQO_lbl_informacion"));
            return false;
        }
        
        return true;
    }, setFechasPreferentes : function(component, addMonths){
        var date = new Date();
        //Añadimos uno por delante y lo seteamos
        date.setMonth(date.getMonth() +1);
		component.set("v.fechaPreferenteTo", date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? "" + (date.getMonth() + 1): "0" + (date.getMonth() + 1)) + "-" + date.getDate());
		date.setMonth(date.getMonth() - addMonths);
		component.set("v.fechaPreferenteFrom", date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? "" + (date.getMonth() + 1): "0" + (date.getMonth() + 1)) + "-" + date.getDate());
	}
})