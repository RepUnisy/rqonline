({
    hacerBusqueda : function(component, event) {
  		var busqueda = event.getParam("busqueda");
        component.set("v.busqueda", busqueda); 
        var busquedaGrados = component.get("c.getBusquedaGrados");
        var user = component.get("v.user");
        debugger;
        if(user.isPrivate){
			busquedaGrados.setParams({ busqueda : component.get("v.busqueda"),
                                  producto : component.get("v.producto"),
                                  segmento : component.get("v.segmento"),
                                  aplicacion: component.get("v.aplicacion"),
                                  language : component.get("v.language"),
                                  privado : user.isPrivate,
                                  RelatedAccount : user.cliente.Id});
        }
        else{
            busquedaGrados.setParams({ busqueda : component.get("v.busqueda"),
                                  producto : component.get("v.producto"),
                                  segmento : component.get("v.segmento"),
                                  aplicacion: component.get("v.aplicacion"),
                                  language : component.get("v.language"),
                                  privado : user.isPrivate,
                                  RelatedAccount : null});
        }
        busquedaGrados.setCallback(this, function(response) {
            var spinnerTop = component.find("spinnerPadre");        
      		$A.util.toggleClass(spinnerTop, "slds-show"); 
            var state = response.getState();
            if (state === "SUCCESS") {
                var busquedaDevuelta = JSON.parse(response.getReturnValue());
                if(busquedaDevuelta.length != 0){
                    component.set("v.mostrarTablas", true);
                }
                else{
                    component.set("v.mostrarTablas", false);
                }
                //component.set("v.listaTablas", busquedaDevuelta);
                component.set("v.listaTablas", busquedaDevuelta);
                var muestraTablasBusqueda = $A.get("e.c:RQO_evt_escondeCatalogo");
                muestraTablasBusqueda.setParams({ "boolEsconderCatalogo" : false });
                muestraTablasBusqueda.fire();
                if(user && user.isPrivate){
                    this.registrarEvento(component, component.get("v.busqueda"));
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                debugger;
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(busquedaGrados);
	},
    registrarEvento: function(component, searchData){
        /*
         * Registro del evento de búsqueda de grados
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         * 	Valor 7 = Search of degrees
         * actionType - Grupo al que pertenece, 1 = Product Catalogue
         * actionDescription - descripción de la acción que lanza el evento. En este caso búsqueda de grados.
         * Valores para este componente:
         * 	Valor 8 = Grade search
         * reSearchData - Búsqueda realizada por el usuario
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_componenteBusquedaGrados', "reComponentDescription" : '7', 
                            "actionType" : '1', "actionDescription" : '8', "reSearchData" : searchData,
                            "rePathActual" : window.location.pathname});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})