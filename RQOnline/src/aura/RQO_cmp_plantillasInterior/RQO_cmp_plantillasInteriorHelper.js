({   
    getTranslateEnvaseUnidad : function(component, helper) {
        var language = component.get("v.language");
        var action = component.get('c.getEnvaseUnidadMedida');   
       
        action.setBackground();
        action.setAbortable();
        action.setParams({"language" : language});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var obj = JSON.parse(response.getReturnValue());
                var map = new Map(Object.entries(obj));
                //	Almacenamos las traducciones obtenidas
                component.set("v.mapTraduccion",  map);
                
                //	LLamamos a la funcion "getTemplates" para obtener las plantillas de pedido
                helper.getTemplates(component);           
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }              
        });
        $A.enqueueAction(action); 
        
    },
    
    getTemplates : function(component) {
        var idContacto = component.get("v.user.idContacto");
        debugger;
        var idAccount = component.get("v.user.idCliente");
        var actionTemplate = component.get('c.getTemplates'); 
        actionTemplate.setBackground();
        actionTemplate.setAbortable();
        actionTemplate.setParams({ idContact : idContacto , idAccount : idAccount });
        
        actionTemplate.setCallback(this, function(response){
            
            var state = response.getState();     
            
            if (state === "SUCCESS") {
                var listPosicionPedido = JSON.parse(response.getReturnValue());   
                
                var mapaTrad =  component.get("v.mapTraduccion");
                
                //	Introducimos las traducciones
                for (var i = 0; i< listPosicionPedido.length; i++){
                    for (var j = 0; j < listPosicionPedido[i].posicionPlantillaList.length; j++){
                        
                        //	Obtenemos la key del envase de la plantilla
                        var envaseKey = Array.from(mapaTrad.keys()).find(function (obj) { return JSON.parse(obj).value === listPosicionPedido[i].posicionPlantillaList[j].envase; });
                        listPosicionPedido[i].posicionPlantillaList[j].envaseNombre = (envaseKey != null ? JSON.parse(envaseKey).label : listPosicionPedido[i].posicionPlantillaList[j].envase);
                       	
                        //	Obtenemos las traducciones de los envases
                        var envListId = listPosicionPedido[i].posicionPlantillaList[j].envasesList;
                       	
                        try{
                            var listEnvases = new Array();
                            if (envListId != null){
                                for (var k = 0; k< envListId.length; k++){
                                    var env = Array.from(mapaTrad.keys()).find(function (obj) { return JSON.parse(obj).value === envListId[k]; });
                                    if (env != null){
                                        listEnvases.push(JSON.parse(env));
                                    }
                                }
                                listPosicionPedido[i].posicionPlantillaList[j].listEnvases = listEnvases;
                            }
                            
                        }catch (e) {        
                            console.log(e);
                        }
                        //	Obtenemos el listado de unidades de medida
                        var listaUnidadMedida = this.getListUnidadMedida(component, envaseKey);
                        //	Asignamos el listado de unidades de medida
                        listPosicionPedido[i].posicionPlantillaList[j].listaUnidadMedida = listaUnidadMedida;
                        
                        var objUnidadMedida = listaUnidadMedida.find(function (obj) { return obj.value === listPosicionPedido[i].posicionPlantillaList[j].unidadMedida; });
                        listPosicionPedido[i].posicionPlantillaList[j].unidadMedidaNombre = (objUnidadMedida != null ? objUnidadMedida.label : listPosicionPedido[i].posicionPlantillaList[j].unidadMedida);
                        
                        //	Obtenemos el nombre de la dirección de mercancias
                        var objDestino = component.get("v.user.listDirMercanciasSelect").find(function (obj) { return obj.value === listPosicionPedido[i].posicionPlantillaList[j].destino; })
                        listPosicionPedido[i].posicionPlantillaList[j].destinoNombre = (objDestino != null ? objDestino.label : listPosicionPedido[i].posicionPlantillaList[j].destino);
                        
                        //	Obtenemos el nombre de la dirección de facturación
                        var objFacturacion = component.get("v.user.listDirFacturacionSelect").find(function (obj) { return obj.value === listPosicionPedido[i].posicionPlantillaList[j].facturacion; })
                        listPosicionPedido[i].posicionPlantillaList[j].facturacionNombre = (objFacturacion != null ? objFacturacion.label : listPosicionPedido[i].posicionPlantillaList[j].facturacion);
                    }                    
                }                           
                component.set("v.listPlantillas", listPosicionPedido);             
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                        
        });
        $A.enqueueAction(actionTemplate); 
        
    },
    
    getListUnidadMedida : function (component, envaseKey){
        var mapaTrad =  component.get("v.mapTraduccion");

        //	Obtenemos la lista de unidades de medida
        var listaUnidadMedida = mapaTrad.get(envaseKey);
        
        return listaUnidadMedida;        
    }
})