({
    init : function(component, event, helper) {
        if (component.get("v.anoActual") == null){
            var today = new Date();
            component.set("v.anoActual", today.getFullYear());
        }	        
    },
    goContact : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/contacto",
            "isredirect" : true
        });
        urlEvent.fire();
    },
    goFaqs : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/faqs",
            "isredirect" : true
        });
        urlEvent.fire();
    },

})