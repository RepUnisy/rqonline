({
    spinner: function(component){
        var spinnerTop = component.find("spinner");        
        $A.util.toggleClass(spinnerTop, "slds-show");   
    },
	resumeT : function(component) {
		var IdCliente = component.get("v.userHeredado.idCliente");
        var language = component.get("v.languageHeredado");

        var action = component.get('c.getPedidosResumen');
        
        action.setParams({ idAccoun : IdCliente, language : language});
        action.setCallback(this, function(response){
           // this.spinner(component);
            var state = response.getState();
            console.log('ACL. Pedidos Resumen: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var mapResumenFactura = JSON.parse(response.getReturnValue());     
                var listKeyObt = [];
                var idDestinoMercancias;
                 debugger;
                for(var key in mapResumenFactura) {                    
                    idDestinoMercancias = mapResumenFactura[key][0].dirEnvio;
                    listKeyObt.push(key);
                }
               
                
                debugger;
                //Insertamos la lista de keys
                component.set("v.mapaDireccion", mapResumenFactura);  
                component.set("v.listKey", listKeyObt);
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }       
        });
        $A.enqueueAction(action);  
	}
})