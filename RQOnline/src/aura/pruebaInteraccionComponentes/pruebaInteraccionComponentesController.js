({
	incrementar : function(component, event, helper) {
		var numero = component.get("v.numero");
        component.set("v.numero", numero+1);
	},
    pasarValor : function(component, event, helper) { 
        var appEvent = $A.get("e.c:eventoPruebaBoton");
        appEvent.setParams({ "message" : component.get("v.numero") });
        appEvent.fire();
    }
})