({
    validate : function(component, event, helper) {
        var valNombre = component.find("inNombre").get("v.validity");
        var valPrimerApellido = component.find("inPrimerApellido").get("v.validity");
        var valSegundoApellido = component.find("inSegundoApellido").get("v.validity");
        var valEmail = component.find("inEmail").get("v.validity");
        var valTelefono = component.find("inTelefono").get("v.validity");
        if (valNombre.valid && valPrimerApellido.valid && valSegundoApellido.valid && valEmail.valid && valTelefono.valid) {
        	var btAccept = component.find("btAccept");
        	btAccept.set("v.disabled", false);
        }
        else {
        	var btAccept = component.find("btAccept");
        	btAccept.set("v.disabled", true);
        }
        
    },
    close : function(component, event, helper) {
        helper.close(component, event);
	},
    register : function(component, event, helper) {
        helper.register(component, event);
    },
    profileChange : function(component, event, helper) {
    	var valorSeleccionado = event.getParam("valorSeleccionado");
        component.set('v.perfilSeleccionado',valorSeleccionado);
    }
})