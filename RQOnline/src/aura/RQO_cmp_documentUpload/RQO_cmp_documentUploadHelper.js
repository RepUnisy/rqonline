({
    doInitLoadData : function(component, event){
        //En la carga del componente vamos al controlador apex a por el combo de tipo de documento ya que hay que filtrar
        //los datos seleccionables.
        var actionLoadPicklistDocType = component.get('c.loadPicklistDocType');
        actionLoadPicklistDocType.setCallback(this, function(response) {
            var state = actionLoadPicklistDocType.getState();
            if (state === "SUCCESS") {
                var customPicklistDocType = [];
                var storedResponse = response.getReturnValue();
                for (var key in storedResponse) {
                    customPicklistDocType.push({value:storedResponse[key], key:key});
                }
                component.set('v.listDocTypeData',customPicklistDocType);
                //Si tenemos datos en la lista de tipos de documento establecemos el primer valor de esta como seleccionado.
                if (customPicklistDocType && customPicklistDocType.length>0){
                	component.find("docTypeSelectionList").set("v.value", customPicklistDocType[0]);
                }
            }
        });
        $A.enqueueAction(actionLoadPicklistDocType);
        
       //En la carga del componente vamos al controlador apex a por el combo idioma de documento
        var actionLoadPicklistDocLocale = component.get('c.loadPicklistDocLocale');
        actionLoadPicklistDocLocale.setCallback(this, function(responseLocale) {
            var state = actionLoadPicklistDocLocale.getState();
            if (state === "SUCCESS") {
                var customPicklistDocLocale = [];
                var storedResponseLocale = responseLocale.getReturnValue();
                for (var key in storedResponseLocale) {
                    customPicklistDocLocale.push({value:storedResponseLocale[key], key:key});
                }
                component.set('v.listDocLocaleData',customPicklistDocLocale);
                //Si tenemos datos en la lista de tipos de documento establecemos el primer valor de esta como seleccionado.
                if (customPicklistDocLocale && customPicklistDocLocale.length>0){
                	component.find("localePicklist").set("v.value", customPicklistDocLocale[0]);
                }
            }
        });
        $A.enqueueAction(actionLoadPicklistDocLocale);
        
    }, uploadDocum : function(component, event) {
        this.showSpinner(component, 'slds-show', 'slds-hide');
        //Recuperamos los datos seleccionados en los combos
        var selectedDocType = component.find("docTypeSelectionList").get("v.value");
        var selectedLocale = component.find("localePicklist").get("v.value");
        
        //Verificamos que ambos combos estén informados. De no ser así lanzamos un evento para el componente de alertas
        var titleAlertValue = $A.get("$Label.c.RQO_lbl_informacion");
        if(!selectedDocType || !selectedLocale){
            this.showSpinner(component, 'slds-hide','slds-show');
            this.fireAlertEvent($A.get("$Label.c.RQO_lbl_UploadDocNotSelectPick"), false, titleAlertValue, 'updValidData');
            return false;
        }
        //Verificamos que se haya cargado un documento para enviar a documentum. De no ser así lanzamos un evento para 
        //el componente de alertas en modo alerta
		var fileUpload = component.get('v.FileList');
        if(!fileUpload || fileUpload.length<1){
            this.showSpinner(component, 'slds-hide','slds-show');
			this.fireAlertEvent($A.get("$Label.c.RQO_lbl_UploadDocNotSelectDoc"), false, titleAlertValue, 'updValidData');
            return false;
        }
    	//Lanzamos validación en el controlador apex para verificar que el tipo de documento en el idioma seleccionado no exista.
    	//Si existe lanzamos una alerta de confirmación para que el usuario decida si quiere pisar el registro en salesforce. Si no 
    	//existe lo cargamos
        var idGrade = component.get("v.recordId");
        var actionValidateDoc = component.get('c.validateDocExist');
        actionValidateDoc.setParams({ "idGrade" : idGrade, "docType" : selectedDocType, "locale" : selectedLocale});
        actionValidateDoc.setCallback(this, function(response) {
            var state = actionValidateDoc.getState();
            if (state === "SUCCESS") {
                var documentSFId = response.getReturnValue();
                component.set("v.documentSFId", documentSFId);
                //Si nos devuelve un true es que el documento en ese idioma ya existe. Debemos preguntar al usuario si desea reemplazarlo 
                //en salesforce 
                if (documentSFId){
                    this.fireAlertEvent($A.get("$Label.c.RQO_lbl_UploadDocConfirmReplace"), true, titleAlertValue, 'updLoadDoc');
                    return false;
                }else{
                    //Si no existe el documento lanzamos la llamada a la función que lee el doc y llama al controlador Apex para cargar el 
                    //documento
                    this.callUploadDocument(component, idGrade, fileUpload, selectedLocale, selectedDocType, documentSFId);
                }
            }
        });
        $A.enqueueAction(actionValidateDoc);
        
    },visibility : function(component, event) {
        //Controla la visibilidad tras el onclick en la cabecera
		var visibilityControl = component.get("v.visibilityControl");
        var cmpTarget = component.find('divData');
        if (visibilityControl == false){
        	$A.util.removeClass(cmpTarget, 'rq-upload-toggle-hide');
        	$A.util.addClass(cmpTarget,'rq-upload-toggle');
            component.set("v.visibilityControl", true);
        }else{
            //Al ocultar limpiamos el documento
            component.set("v.FileList", null);
            $A.util.removeClass(cmpTarget, 'rq-upload-toggle');
        	$A.util.addClass(cmpTarget,'rq-upload-toggle-hide');
            component.set("v.visibilityControl", false);
        }
    }, deleteFile : function(component, event){
        //Función para eliminar documentos pantalla del componente file
        component.set("v.FileList", null);
    }, fireAlertEvent : function(textValue, confirmAlert, titleAlertValue, idAlert, documentSFId){
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : textValue, "confirmAlert" : confirmAlert, 
                            "titleAlertValue" : titleAlertValue, "idAlert" : idAlert, "parentLE" : true});
        try{
        	appEvent.fire();
        }catch(err){
            this.showSpinner(component);
            console.log(err.message);
        }
    }, handleResponseAlert : function(component, event){
        //Capturamos las respuestas del componente de alertas
		var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");        
        //Si recibimos un true como respuesta (ha pulsado Aceptar) y el id que nos devuelve es el de carga de documento 
        //lanzamos la función de carga contra el controlador apex
        if (responseAlert && responseAlert == true && idAlert && idAlert == 'updLoadDoc'){
            var idGrade = component.get("v.recordId");
            var fileUpload = component.get('v.FileList');
            var selectedDocType = component.find("docTypeSelectionList").get("v.value");
            var selectedLocale = component.find("localePicklist").get("v.value");
            var documentSFId = component.get("v.documentSFId");
            console.log('Vamos a por el documento');
            this.callUploadDocument(component, idGrade, fileUpload, selectedLocale, selectedDocType, documentSFId);            
        }else{
            this.showSpinner(component, 'slds-hide', 'slds-show');
		}
    },callUploadDocument : function(component, idGrade, fileUpload, locale, docType, documentSFId){

        var fr = new FileReader();
        var fileContents;
        //Ejecutamos las acciones para llamar al controlador en el onload del FileReader
       	fr.onload = function(file) {
            //Recuperamos el cuerpo de documento 
            var fileContents = fr.result;
    	    var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            var docBody = encodeURIComponent(fileContents);
            //Lanzamos el método del controlador apex que carga el documento
			var actionUpload = component.get('c.uploadDocToRepository');            
        	actionUpload.setParams({ "idGrade" : idGrade, "docName" : file.target.fileName, "tipoDoc" : docType,
                                    "docBody" : docBody, "locale" : locale, "documentSFId" : documentSFId});
            actionUpload.setCallback(this, function(response) {
				var spinner = component.find("mySpinner");
        		$A.util.removeClass(spinner, 'slds-show');
        		$A.util.addClass(spinner, 'slds-hide');
                var state = actionUpload.getState();
                if (state === "SUCCESS") {
                    var storedResponse = response.getReturnValue();
                    component.set("v.FileList", null);
                    $A.get('e.force:refreshView').fire();
                }
            });
            $A.enqueueAction(actionUpload);
        }
        fr.fileName = fileUpload[0].name;
        fr.readAsDataURL(fileUpload[0]);
    }, showSpinner : function(component, classToAdd, classToremove){
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, classToremove);
        $A.util.addClass(spinner, classToAdd);
	}
})