({    
     doInit : function(component, event, helper) {      
         helper.getInfoHerencia(component, event, helper);
     },
    
    getSolicitud : function(component, event, helper) {      
        helper.getSolicitud(component);
    },
    editar : function(component, event, helper) {
        var bloqueoSiguiente = component.get("v.bloqueoSiguiente");
       
        if (bloqueoSiguiente){            
       //     component.find("siguiente").set("v.disabled",true);
        }
        else{
         //   component.find("siguiente").set("v.disabled",false);
        }
    },
    siguente : function(component, event, helper) {
        //	Marcamos para almacenar
        var paso = component.get("v.paso"); 
        component.set("v.guardar", true);
        component.set("v.guardar", false);
        debugger;
        var bloqueo = component.get("v.bloqueoSiguiente");
        if (!bloqueo){
            component.set("v.paso", paso + 1);     
        }  
        debugger;
    }, 
    volver : function(component, event, helper) {
        var paso = component.get("v.paso");
        component.set("v.paso", paso - 1);
    }, 
    enviar : function(component, event, helper) {
        var condicion = component.get("v.condicionesLegales");
        debugger;
        if (condicion){
             helper.enviar(component);
        }
        else{
            var mensaje = $A.get("$Label.c.RQO_lbl_aceptarCondicionesLegales");
            var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
            appEvent.setParams({"textAlertValue" : mensaje, 
                                "confirmAlert" : false, 
                                "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"),                                     
                                "parentLE" : false});
            try{
                appEvent.fire();
            }catch(err){
                console.log(err.message);
            }
        }
        
    }, 

    
    irCatalogo: function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/catalogo",
            "isredirect" : true
        });
        urlEvent.fire();       
    }
})