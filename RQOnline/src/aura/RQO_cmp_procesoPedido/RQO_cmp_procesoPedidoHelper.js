({
    
     getSolicitud: function(component){
         var idContacto = component.get("v.user.idContacto"); 
         var action = component.get('c.getInfoRequest');
        
         action.setBackground();
         action.setParams({ idContact : idContacto, language:  component.get("v.language") });
         action.setCallback(this, function(response){
             var state = response.getState();
          
             if (state === "SUCCESS") {
                 
                 var listAsset = JSON.parse(response.getReturnValue());
                
                 var idSolicitud;
                 var nameSolicitud;
                 var variasDirFacturacion;

                 for(var i = 0; i < listAsset.length; i++) {
                     idSolicitud = listAsset[i].RQO_fld_requestManagement__c;
                     nameSolicitud = listAsset[i].RQO_fld_requestManagement__r.Name;                     
                 }
                 
                 //	almacenamos la información
                 component.set("v.nameSolicitud", nameSolicitud);                 
                 component.set("v.idSolicitud", idSolicitud);
                
             }
             else if (state === "INCOMPLETE") {
                 console.log('Incomplete');
             }
                 else if (state === "ERROR") {
                     var errors = response.getError()
                     if (errors) {
                         if (errors[0] && errors[0].message) {
                             console.log("Error message: " + 
                                         errors[0].message);
                         }
                    } else {
                        console.log("Unknown error");
                    }
                }                        
        });
        $A.enqueueAction(action);  
    },
        
    
    enviar: function(component){
        var idSolicitud = component.get("v.idSolicitud");
        var action = component.get('c.sentRequest');
        action.setParams({ idSolicitud : idSolicitud});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") { 
                var storedResponse = response.getReturnValue();
                if (storedResponse && storedResponse == 'OK'){
                    component.set("v.sentOk", true);
					//this.registrarEvento(component);
                }else{
                    component.set("v.sentOk", false);
                }
                var eventoNotificaciones = $A.get("e.c:RQO_evt_sumarUnoANotificaciones");
                eventoNotificaciones.setParams({"numSumar": 0});
                eventoNotificaciones.fire();
                
                component.set("v.paso", 4)
            }else if (state === "INCOMPLETE") {
                console.log('Incomplete');
                component.set("v.sentOk", false);
            }else if (state === "ERROR") {
                var errors = response.getError()
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.sentOk", false);
			}  
        });
        $A.enqueueAction(action);  
    },
    registrarEvento: function(component){
        /*
         * Registro del evento de alta de solicitud
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         * 	Valor 10 = Sent Request (Solicitud enviada)
         * actionType - Grupo al que pertenece, 2 = Order
         * actionDescription - descripción de la acción que lanza el evento. En este caso alta de solicitud.Valores para este componente:
         * 	Valor 11 = High request
         * reRequestId - Id de la solicitud que se ha enviado
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var idSolicitud = component.get("v.idSolicitud");
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_listaCategoriasTablaGrados', "reComponentDescription" : '10', 
                            "actionType" : '2', "actionDescription" : '11', "reRequestId" : idSolicitud,
                            "rePathActual" : window.location.pathname});
        
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})