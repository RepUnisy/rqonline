({
	doInit : function(component, event) {
		var inicializarFiltros = component.get("c.inicializarFiltros");
		inicializarFiltros.setParams({});
        inicializarFiltros.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var filtroSegmento = $A.get("$Label.c.RQO_lbl_Segmentos");
                var filtroAplicaciones = $A.get("$Label.c.RQO_lbl_Aplicaciones");
                var filtroProductos = $A.get("$Label.c.RQO_lbl_productKey");
                var mapaFiltros = JSON.parse(response.getReturnValue());
                component.set("v.listaProductosCompleta", mapaFiltros[filtroProductos]);
                component.set("v.listaSegmentosCompleta", mapaFiltros[filtroSegmento]);
                component.set("v.listaAplicacionesCompleta", mapaFiltros[filtroAplicaciones]);
                component.set("v.listaProductos", mapaFiltros[filtroProductos]);
                component.set("v.listaSegmentos", mapaFiltros[filtroSegmento]);
                component.set("v.listaAplicaciones", mapaFiltros[filtroAplicaciones]);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                debugger;
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(inicializarFiltros);
	},
    cambiarPicklistsProducto : function(component, event){
        console.log('cambiamos picklist');
        var lanzarCambioPicklist = component.get("c.lanzarCambioPicklist");
        var inputSelectProduct = component.find("inputSelectProduct");
		lanzarCambioPicklist.setParams({ product : inputSelectProduct.get("v.value"),
                                        aplicacion : 'null'});
        lanzarCambioPicklist.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var mapaListas = JSON.parse(response.getReturnValue());
                var filtroSegmento = $A.get("$Label.c.RQO_lbl_Segmentos");
                var filtroAplicaciones = $A.get("$Label.c.RQO_lbl_Aplicaciones");
                if(mapaListas[filtroSegmento] && mapaListas[filtroAplicaciones]){
                    component.set("v.listaSegmentos", mapaListas[filtroSegmento]);
                    component.set("v.listaSegmentosParcial", mapaListas[filtroSegmento]);
                    component.set("v.listaAplicaciones", mapaListas[filtroAplicaciones]);
                }
                else{
                  	component.set("v.listaSegmentos", component.get("v.listaSegmentosCompleta"));
                    component.set("v.listaAplicaciones", component.get("v.listaAplicacionesCompleta"));
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                debugger;
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(lanzarCambioPicklist);
    },
    cambiarPicklistsAplicacion : function(component, event){
        console.log('cambiamos picklist');
        var lanzarCambioPicklist = component.get("c.lanzarCambioPicklist");
        var inputSelectSegmento = component.find("inputSelectSegmento");
        var inputSelectAplicacion = component.find("inputSelectAplicacion");
		lanzarCambioPicklist.setParams({ aplicacion : inputSelectAplicacion.get("v.value"),
                                       product : 'null'});
        lanzarCambioPicklist.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var mapaListas = JSON.parse(response.getReturnValue());
                var filtroSegmento = $A.get("$Label.c.RQO_lbl_Segmentos");
                if(mapaListas[filtroSegmento]){
                    component.set("v.listaSegmentos", mapaListas[filtroSegmento]);
                }
                else{
                  	component.set("v.listaSegmentos", component.get("v.listaSegmentosParcial"));
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                debugger;
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(lanzarCambioPicklist);
    },
    cambiarPicklistsSegmento : function(component, event){
        
    }
})