({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},
    cambiarPicklistsProduct : function(component, event, helper){
        console.log('NGM. CambiarPicklistsProduct');
        var inputSelectProduct = component.find("inputSelectProduct");
        var inputSelectSegmento = component.find("inputSelectSegmento");
        var inputSelectAplicacion = component.find("inputSelectAplicacion");
        component.set("v.producto", inputSelectProduct.get("v.value"));
        inputSelectSegmento.set("v.value", component.find("inputOptionTodosSegmento"));
        component.set("v.segmento",  inputSelectSegmento.get("v.value"));
        inputSelectAplicacion.set("v.value", component.find("inputOptionTodosAplicacion"));
        component.set("v.aplicacion",  inputSelectAplicacion.get("v.value"));
        helper.cambiarPicklistsProducto(component, event);
    },
    cambiarPicklistsAplicacion : function(component, event, helper){
        var inputSelectSegmento = component.find("inputSelectSegmento");
        var inputSelectAplicacion = component.find("inputSelectAplicacion");
        component.set("v.aplicacion", inputSelectAplicacion.get("v.value"));
        inputSelectSegmento.set("v.value", component.find("inputOptionTodosSegmento"));
        component.set("v.segmento",  inputSelectSegmento.get("v.value"));
        helper.cambiarPicklistsAplicacion(component, event);
    },
    cambiarPicklistsSegmento : function(component, event, helper){
        var inputSelectSegmento = component.find("inputSelectSegmento");
        component.set("v.segmento", inputSelectSegmento.get("v.value"));
    	helper.cambiarPicklistsSegmento(component, event);
    },
    limpiarBusqueda : function(component, event, helper){
        component.set("v.txtWikiTextHeredada", null);
    	var inputSelectProduct = component.find("inputSelectProduct");
        var inputSelectSegmento = component.find("inputSelectSegmento");
        var inputSelectAplicacion = component.find("inputSelectAplicacion"); 
        inputSelectProduct.set("v.value", component.find("inputOptionTodosProduct"));
        component.set("v.producto", inputSelectProduct.get("v.value"));
        inputSelectSegmento.set("v.value", component.find("inputOptionTodosSegmento"));
        component.set("v.segmento",  inputSelectSegmento.get("v.value"));
        inputSelectAplicacion.set("v.value", component.find("inputOptionTodosAplicacion"));
        component.set("v.aplicacion",  inputSelectAplicacion.get("v.value"));
    }
})