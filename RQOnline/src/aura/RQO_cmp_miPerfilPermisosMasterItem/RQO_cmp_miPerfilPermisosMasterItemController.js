({
	doInit : function(component, event, helper) {
	
		var perfilContacto = component.get("v.user.Contact.RQO_fld_perfilUsuario__c");
		var perfiles = component.get("v.perfiles");
		
		var numPerfiles = 0;
		if (perfiles)
			numPerfiles = perfiles.length;
		var perfilSeleccionado = '';
		var i = 0;
		for (i = 0; i < numPerfiles; i++) {
		    if (perfiles[i].value == perfilContacto) {
		    	perfilSeleccionado = perfiles[i].value;
		    }
		}
		
		component.set("v.perfilSeleccionado", perfilSeleccionado);
	},
	showInfo : function(component, event, helper) {
    	var help = component.find("divInfo");        
        $A.util.toggleClass(help, "slds-show");
    },
    profileChange : function(component, event, helper) {
    	var perfilSeleccionado = component.find("perfilContacto").get("v.value");
    	component.set("v.perfilSeleccionado", perfilSeleccionado);
    	component.set("v.user.Contact.RQO_fld_perfilUsuario__c", perfilSeleccionado);
    },
    solicitarBaja : function(component, event, helper) {
        component.set('v.showAlertBaja',true);
    },
    realizarBaja : function(component, event, helper) {
    	var responseAlert = event.getParam("responseAlert");
    	var idAlert = event.getParam("idAlert");
    	if (idAlert == component.get("v.user.Id") && responseAlert)
    	{
    		// Actualizar perfil
    		var user = component.get("v.user");
    		user.Contact.RQO_fld_perfilUsuario__c = null;
    		var users = [user];
	    	var action = component.get("c.updateProfile");
	        action.setParams({
	            users : users
	        });
	        
	        action.setCallback(this, function(response){
	            var state = response.getState();
	            if (state === "SUCCESS") {
	            	//	Realizar baja del usuario
			    	var action2 = component.get("c.unregisterUser");
			        action2.setParams({
			            userId : component.get("v.user.Id")
			        });
			        action2.setCallback(this, function(response){
			            var state = response.getState();
			            if (state === "SUCCESS") {
			            	component.set('v.showAlertBaja',false);
			            	component.set('v.showAlertBajaConfirm',true);
			            	setTimeout( function()
			            				{ 
			            					component.set('v.showItem',false);
			            				}, 3000);
			            }
			            else {
			            	component.set('v.showAlertBaja',false);
			            	component.set('v.showAlertBajaConfirm', $A.get("$Label.c.RQO_lbl_errorGenerico"));
			            	component.set('v.showAlertBajaConfirm',true);
			            }
			        });
			        
			        $A.enqueueAction(action2);
	            }
	            else {
	            	component.set('v.showAlertBaja',false);
	            	component.set('v.showAlertBajaConfirm', $A.get("$Label.c.RQO_lbl_errorGenerico"));
	            	component.set('v.showAlertBajaConfirm',true);
	            }
	        });

	        $A.enqueueAction(action);
    	}
    },
})