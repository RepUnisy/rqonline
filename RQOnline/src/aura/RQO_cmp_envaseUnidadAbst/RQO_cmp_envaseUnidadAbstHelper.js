({
    
    getTranslateEnvaseUnidad : function(component, language) {
       
        var action = component.get('c.getEnvaseUnidadMedida');               
        action.setBackground();
        action.setAbortable();
        action.setParams({ "language" : language });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var obj = JSON.parse(response.getReturnValue());
                var map = new Map(Object.entries(obj));
                var mapTrans = new Map();
                
                //	Almacenamos las traducciones obtenidas
                var listKeys = Array.from(map.keys());
                for (var i = 0; i< listKeys.length ; i++){
                    
                    mapTrans.set(JSON.parse(listKeys[i]).value, map.get(listKeys[i]));
                    
                }
                component.set("v.mapOrigen", map);
                component.set("v.mapTraduccion",  mapTrans);
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action); 
    },
    
    inicialiteShippingJuntion : function (component){
        
        var action = component.get('c.getAllShippingJuntion');               
        action.setBackground();
        action.setAbortable();
        action.setCallback(this, function(response){
            
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var obj = JSON.parse(response.getReturnValue());
                var map = new Map(Object.entries(obj));
                
                component.set("v.mapShippingJuntion", map);
                debugger;
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action); 
    },
    
    getDetailShippingJunction : function(component, envase, unidadMedida){
        
        var mapShippingJuntion = component.get("v.mapShippingJuntion");
        var emvaseMedida =  envase + unidadMedida;
        var info = mapShippingJuntion.get(emvaseMedida) ;
        
        return info;
    },
    
    setListEnvases : function (component, envListId){
    var mapaTrad = component.get("v.mapOrigen");
    try{
    var listEnvases = new Array()
    if (envListId != null){
    for (var k = 0; k< envListId.length; k++){
    
    var env = Array.from(mapaTrad.keys()).find(function (obj) { return JSON.parse(obj).value === envListId[k]; });
if (env != null){
    listEnvases.push(JSON.parse(env));
    
}                            
}
component.set("v.listEnvases", listEnvases);

if (listEnvases.length > 0){
    component.set("v.envase", listEnvases[0].value);
}                
}
return listEnvases;

}catch (e) {        
    console.log(e);
}

},
    
    setListUnidadMedida : function (component, envaseKey){
        var mapaTrad =  component.get("v.mapTraduccion");
        debugger;
        //	Si pasamos como parámetros la key la utilizamos, sino 
        var envase = envaseKey != null ? envaseKey : component.get("v.envase") ;

        
        //	Obtenemos la lista de unidades de medida
        var listaUnidadMedida = mapaTrad.get(envase);
        component.set("v.listUnidadMedida", listaUnidadMedida);   
        
        var unidad = component.get("v.unidadMedida");
        var existente = null;
        if (unidad != null){
            existente = listaUnidadMedida.find(function(obj) { return obj.value === unidad;});
        }
        
        
        if (existente == null && listaUnidadMedida != null && listaUnidadMedida.length > 0){
             component.set("v.unidadMedida", listaUnidadMedida[0].value);   
        }
        
        return listaUnidadMedida;
        
    },
    
})