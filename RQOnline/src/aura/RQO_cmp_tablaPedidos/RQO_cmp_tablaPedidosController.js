({
    getIncoterm : function(component, event, helper){                      
        var user = component.get("v.userHeredado");
        //	Los incoterm se encuntran relaccionados con el usuario. Obtenemos el listado y lo asignamos a nuestro atributo
        var lista = user.listIncoterm;
        component.set("v.listIncoterm",lista);         
    },
    inicialiteShippingJuntion  :  function(component, event, helper){
        
        helper.inicialiteShippingJuntion(component);
    },
    
    toggleInfo: function(component, event, helper){
        /*FRONTEND*/
        jQuery('.rq-msg-info').slideToggle();
    },
    changeDestino : function(component, event, helper) {
        
        helper.mostrarGuardar(component);
        var listadoPosiciones = component.get("v.listPosicionPedidoTable");
        var listDestinos = component.get("v.userHeredado.listDirMercancias");
        var idDireccion = event.getSource().get("v.value");
        var idLinea = event.getSource().get("v.name");
        var nombreDir = '';
        
        for (var i = 0; i<listDestinos.length; i++){
            if (listDestinos[i].RQO_fld_cliente__r.Id == idDireccion){
                nombreDir = listDestinos[i].RQO_fld_cliente__r.Name + ' ' + listDestinos[i].RQO_fld_country__c;
                break;
            }            
        }
        if (nombreDir != ''){
            listadoPosiciones[idLinea].destinoMercanciasNombre = nombreDir;
        }
        
    },
    actualizarTabla : function(component, event, helper) {
        helper.validacionEnvase(component, parametro);
    },
    mostrarGuardar : function(component, event, helper) {
        helper.mostrarGuardar(component);
    },
    controlFecha : function(component, event, helper) {
        /*   debugger;
         var elem = event.getSource();
        var fecha = new Date ();
        var fechaIntroducida = new Date (elem.get("v.value"));       
       
        if ( fechaIntroducida <= fecha){
         	elem.set("v.errors", [{message:"Introduce una fecha mayor"}]);
        }
        else{
            elem.set("v.errors", null);
        }
        */
        helper.mostrarGuardar(component);
    },
    openDuplicar : function(component, event, helper) {
        
        var posicion = event.getSource().get("v.name");
        var lista = component.get("v.listPosicionPedidoTable");
        var wrapper = lista[posicion];
        
        component.set("v.posicionDuplicar", wrapper);
        component.set("v.clickDuplicar", true);
    },
    closeDuplicar: function(component, event, helper) {
        component.set("v.clickDuplicar", false);
    },
    aceptDuplicar: function(component, event, helper) {
        helper.guardar(component);
        
        var elemDuplicar = component.get("v.posicionDuplicar");
        var cantidadDuplicar = component.get("v.cantidadDuplicar");        
        helper.duplicar(component.get('c.cantidadDuplicar'), elemDuplicar, cantidadDuplicar);
        component.set("v.clickDuplicar", false);
    },
    toggleTabla : function(component, event, helper) {
        helper.toggleTabla(component,event)
    },    
    openEliminar : function(component, event, helper) {
        var listado = component.get("v.listPosicionPedidoTable");       
        var id = event.getSource().get("v.name");
        
        var listadoEliminar = component.get("v.listEliminadoHeredado");        
        
        var elementoEncontrado =  listado[id];
        
        listadoEliminar.push(elementoEncontrado);
        var index = listado.indexOf(elementoEncontrado);
        listado.splice(index, 1);
        
        
        //	Actualizamos los listados
        component.set("v.listEliminadoHeredado", listadoEliminar);
        component.set("v.listPosicionPedidoTable", listado);
        
    }, 
    changeEnvase : function(component, event, helper){
        
        var listPosicionPedido = component.get("v.listPosicionPedidoTable");
        var parametro = event.getSource().get("v.name");
        debugger;
        //	Buscamos la label del envase seleccionado
        for (var i = 0; i< listPosicionPedido[parametro].listEnvases.length ; i++){
            
            //	Comprobamos si se trata del valor seleccionado
            if (listPosicionPedido[parametro].listEnvases[i].value == listPosicionPedido[parametro].envase){
                listPosicionPedido[parametro].envaseName = listPosicionPedido[parametro].listEnvases[i].label;
                break;
            }
        }
        
        component.set("v.unidadMedida", listPosicionPedido[parametro].modoEnvio);
        //	Llamamos a la funcion del componente abstracto "RQO_cmp_envaseUnidadAbst" para obtener el listado de unidades de medida correspondiente al envase
        listPosicionPedido[parametro].listUnidadMedida = helper.setListUnidadMedida(component, listPosicionPedido[parametro].envase);        
        listPosicionPedido[parametro].modoEnvio = component.get("v.unidadMedida");        
        component.set("v.listPosicionPedidoTable", listPosicionPedido); 
        
    },
    
    changeUnidadMedida : function(component, event, helper){
        
        var listPosicionPedido = component.get("v.listPosicionPedidoTable");
        var parametro = event.getSource().get("v.name");
        debugger;
        //	Buscamos la label del envase seleccionado
        for (var i = 0; i< listPosicionPedido[parametro].listUnidadMedida.length ; i++){
            
            //	Comprobamos si se trata del valor seleccionado
            if (listPosicionPedido[parametro].listUnidadMedida[i].value == listPosicionPedido[parametro].modoEnvio){
                listPosicionPedido[parametro].modoEnvioName = listPosicionPedido[parametro].listUnidadMedida[i].label;
                break;
            }
        }
    },
    
    comprobar: function(component, event, helper) {
        helper.mostrarGuardar(component);
        var parametro = event.getSource().get("v.name");
        var envaseSeleccionado = event.getSource().get("v.value");
        debugger;        
        helper.validacionEnvase(component, parametro);
        
    },
    
    guardar: function(component, event, helper) {
        debugger;
        //	Obtenemos la petición de almacenar, si es true procedemos a ello
        var guardar = component.get("v.guardarHeredado");        
        if (guardar){
            var listPosicionPedidoTable = component.get("v.listPosicionPedidoTable");
            var bloqueo = false;
            
            
            for(var i = 0; i<listPosicionPedidoTable.length; i++){
                
                //	Realizamos las validaciones de envases
                var blockObt = helper.validacionEnvase(listPosicionPedidoTable[i], component);
                if(blockObt && !bloqueo){
                    bloqueo = blockObt;
                }
                var idFecha = 'fecha_' + listPosicionPedidoTable[i].idAsset;
                
                //	Realizamos las validaciones de fechas
                var blockFech =  helper.validacionFecha(listPosicionPedidoTable[i], component.get("v.userHeredado"), component.find("fechapreferente")[i]);
                if(blockFech && !bloqueo){
                    bloqueo = blockFech;
                }
                
                //	Realizamos las valicadiones de cantidades
                var blockCant = helper.validacionCantidad(listPosicionPedidoTable[i]);
                if(blockCant && !bloqueo){
                    bloqueo = blockCant;
                }
                var blockEliminar = helper.validacionListaEliminados(component);
                if(blockEliminar && !bloqueo){
                    bloqueo = blockEliminar;
                }
            }
            
            component.set("v.bloqueoSiguienteHeredado", bloqueo);
            
            if (!bloqueo){
                
                component.set("v.listPosicionPedidoTable", listPosicionPedidoTable);
                console.log('ACL. Almacenamos la tabla');
                helper.guardar(component);
            }        
            else{
                component.set("v.listPosicionPedidoTable", listPosicionPedidoTable);
                
                var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
                appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_revisionCambios"), 
                                    "confirmAlert" : false, 
                                    "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"),                                     
                                    "parentLE" : false});
                try{
                    appEvent.fire();
                }catch(err){
                    this.showSpinner(component);
                    console.log(err.message);
                }
            }            
        }                
    }
})