({
    validacionEnvase: function(item, component){
        debugger;
        var envase = item.envase;
        var unidadMedida = item.modoEnvio; 
        if (envase == null || unidadMedida == null){            
            item.error = $A.get("$Label.c.RQO_lbl_errorValidacionCantidad" ) ;   
            return true;
        }        

        var info = this.getDetailShippingJunction(component, envase, unidadMedida);
        
        var cantidadUnidad = item.cantidadUnidad;
        
        //	La cantidad en kg = cantidad introducida por a cantidad por unidad
        var cantidadKg = cantidadUnidad * info.multiploKgUnidad;
        item.cantidad = cantidadKg;
        
        if (info == null){
            item.error = $A.get("$Label.c.RQO_lbl_errorValidacionCantidad" ) ;   
            return true;
        }
        
        //	Comprobamos que sea necesario comprobar validaciones      
        if (info != null && !info.excluirValidacion){
                                  
            //	Comprobamos si excede el peso máximo
            if (info.pesoMaximo != null && cantidadKg > info.pesoMaximo){
                var unidadesMaxima = info.pesoMaximo/info.pesoMinimo;
                item.error = $A.get("$Label.c.RQO_lbl_errorValidacionCantidad" ) ;   
                 return true;
            }
            else{   
                //	Comprobamos si es menor al peso mínimo
                if (info.pesoMinimo != null && cantidadKg < info.pesoMinimo){
                    var unidadesMaxima = info.pesoMaximo/info.pesoMinimo;
                    item.error = $A.get("$Label.c.RQO_lbl_errorValidacionCantidad" ) ;   
                    return true;
                }
                else{
                    //	Comprbamos que los kg sean multiplos a los kg permitidos
                    if (cantidadKg % info.multiploKgUnidad == 0){
                        item.error = '';
                        return false;                 
                    }
                    else{
                        item.error = $A.get("$Label.c.RQO_lbl_errorValidacionCantidad" ) ;           
                        return true;   
                    }
                }                
            }
        }
        else{
            return false;
        }   
    },
    
    validacionFecha: function(item, user, inputCmp){
        var incoterm = item.incoterm;
        var envase = item.envase;
        var fechaPreferente = item.fechaPreferenteEntrega;
        var bloqueo = false;
        
        var f=new Date();
        var horas = f.getHours();
        var numDias = 0;
        var material;        
        var pais = item.destinoMercanciasNombre.substr(-2);
        
        if (fechaPreferente == null || fechaPreferente == ''){
          //  inputCmp.set("v.errors", [{message:($A.get("$Label.c.RQO_lbl_esObligatorio" ))}]);
            item.errorFecha = $A.get("$Label.c.RQO_lbl_validacionFechaPedidos");
            bloqueo = true;
        }
        else{
         //    inputCmp.set("v.errors", [{message: ''}]);
            //	Comprobamos el incoterm
            item.errorFecha = '';
            if (incoterm == $A.get("$Label.c.RQO_lbl_incotermFCA") ){ 
                //	Comprobamos si es antes de las 12
                if (horas < 12 ){
                    //	Comprobamos el material
                    if ($A.get("$Label.c.RQO_lbl_gradosEspecial").includes(item.sku)){
                        numDias = 1;
                        bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                    }
                    else{
                        numDias = 1;
                        bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);                     
                    }
                }
                else{
                    //	Comprobamos el material
                    if ($A.get("$Label.c.RQO_lbl_gradosEspecial").includes(item.sku)){
                        numDias = 1;                      
                        bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                    }
                    else{
                        numDias = 1;
                        bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                    }
                }
            }
            else{
                //	Comprobamos el destino
                if (pais == 'ES'){
                    //	Comprobamos si es antes de las 12
                    if (horas < 12 ){
                        //	Comprobamos el material
                        if ($A.get("$Label.c.RQO_lbl_gradosEspecial").includes(item.sku)){
                            numDias = 3;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                        else{
                            numDias = 3;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                    }
                    else{
                        //	Comprobamos el material
                        if ($A.get("$Label.c.RQO_lbl_gradosEspecial").includes(item.sku)){
                             numDias = 4;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                        else{
                             numDias = 4;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                    }
                }
                else{
                    //	Comprobamos si es antes de las 12
                    if (horas < 12 ){
                        //	Comprobamos el material
                        if ($A.get("$Label.c.RQO_lbl_gradosEspecial").includes(item.sku)){
                            numDias = 4;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                        else{
                            numDias = 5;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                    }
                    else{
                        //	Comprobamos el material
                        if ($A.get("$Label.c.RQO_lbl_gradosEspecial").includes(item.sku)){
                            numDias = 5;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                        else{
                            numDias = 6;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                    }
                }
            }
        }                
        return bloqueo;
    },
    
    validacionCantidad: function(item){
        var cantidadUnidad = item.cantidadUnidad;
        if (cantidadUnidad <= 0){
            item.errorCantidad = $A.get("$Label.c.RQO_lbl_cantidadNegativa" ) ;
            return true;
        }
        else{
            item.errorCantidad = '';
             return false;
        }
       
    },
    
    validacionListaEliminados: function(component){
        var listaEliminados = component.get("v.listEliminadoHeredado");
        
        if (listaEliminados.length > 0){
            component.set("v.errorEliminarHeredado", $A.get("$Label.c.RQO_lbl_mensajeSeleccionGrado" ));
            return true;
        }
    },
    
    comprobacionFecha : function(numDias, fechaPreferente, item){

        var fecha = new Date ();
        
        var fechaIntroducida = new Date (fechaPreferente);
        var fechaValidar =  new Date (fecha.setDate(fecha.getDate() + numDias));
        
        if( fechaValidar > fechaIntroducida) {            
            item.errorFecha = $A.get("$Label.c.RQO_lbl_errorValidacionFechas1") + ' ' + numDias + ' ' + $A.get("$Label.c.RQO_lbl_errorValidacionFechas2");
            return true;
        }   
        else{
            item.errorFecha = '';
            return false;
        }
    },
    
    duplicar : function(action, elemDuplicar, cantidadDuplicar){

        action.setParams({ idPosicion : elemDuplicar.idAsset, cantidadDuplicar: cantidadDuplicar});
        action.setCallback(this, function(response){
            var state = response.getState();

            console.log('ACL. Actualizar Posiciones ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {                             
                
                $A.get('e.force:refreshView').fire();
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }    
        });
        $A.enqueueAction(action);
    },
       
    mostrarGuardar : function(component){
        var editado = component.get("v.editado");
        if (!editado){
            component.set("v.editado", true);
            component.set("v.bloqueoSiguienteHeredado", true);
        }
    },
    
    guardar : function(component) {
        
        var listTabla = component.get("v.listPosicionPedidoTable");
        for(var i = 0; i<listTabla.length; i++){
            listTabla[i].listEnvases = null;
            listTabla[i].listUnidadMedida = null;
        }
        
        var action = component.get('c.actualizarPosiciones');
        action.setParams({ listAlmacenar : JSON.stringify(listTabla)});
        debugger;
        action.setCallback(this, function(response){
            debugger;
            var state = response.getState();
            console.log('ACL. Actualizar Posiciones ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var listVacia = []                
                component.set("v.editado", false);
                component.set("v.bloqueoSiguienteHeredado", false);                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }    
        });
        $A.enqueueAction(action);
     
    },
    toggleTabla : function(component, event, helper) {
        var element = event.currentTarget;
        
        jQuery(element).addClass('this-line');
        $A.util.toggleClass(element,'fa-chevron-down');
         $A.util.toggleClass(element,'fa-chevron-up');
        //Si ya estaba desplegado
        if(jQuery('.this-line').closest('tr').children('td:not(:nth-child(-n+3))').hasClass('rq-table-block'))
        {
            
            jQuery('.this-line').closest('tr').children('td.rq-table-block').slideUp(0, function() {
                jQuery('.this-line').closest('tr').children('td:not(:nth-child(-n+3))').removeClass('rq-table-block');
            });
        }
        else{
            //si esta oculto
            jQuery('.this-line').closest('tr').children('td:not(:nth-child(-n+3))').addClass('rq-table-block');
            jQuery('td:last-child').removeClass('rq-table-block');
            jQuery('.rq-table-block').slideDown();
        }
        jQuery(element).removeClass('this-line');
    }
})