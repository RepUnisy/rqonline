({

   close : function(component, event, helper) {
      component.set("v.visible", false);
	},
	accept : function(component, event, helper) {
       component.set("v.visible", true);
    }
})