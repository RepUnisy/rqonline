({
    guardar : function(component) {
        var dirParaAlmacenar = component.get("v.dirSeleccionado").Id;
        
        var action = component.get('c.assignDireccionFacturacionGlobal');
        action.setParams({ idDirFacturacion : dirParaAlmacenar});
       
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('ACL. Almacenar ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {   
             	component.set("v.isEditadoUnica", false);
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);  
        
        
    }
})