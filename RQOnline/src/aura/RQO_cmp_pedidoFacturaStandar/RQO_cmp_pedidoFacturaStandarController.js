({
    
    init : function (component, event) {
        
        //	Obtenemos la opcion seleccionada (todos tendrán la misma opcion)
        var idOpcionAlmacenada = component.get("v.listPosicionPedidoHeredado")[0].direccionFacturacion;
        
        var mapFactu = component.get("v.mapaDirFacturacionHeredado");
        var listOpFact = [];
        var seleccionado ;
        var posicion = 0;
        debugger;
        if (mapFactu != null){
            debugger;
            for (var [key, value] of mapFactu) {
                listOpFact.push(value.Name);
               
                if (seleccionado == null || (value.Id != null && value.Id == idOpcionAlmacenada)){
                    seleccionado = value;
                    posicion = key;
                }                        
            }
            debugger;
            component.set("v.posicionSeleccionado", posicion);
            debugger;
            component.set("v.dirSeleccionado", seleccionado);  
            debugger;
            component.set("v.opcionesDirFacturacion", listOpFact);
             debugger;
        }                
    },
    
    changeSelect : function (component, event) {
        
        var idComponente = event.getParam("idComponente");
        var idTexto =  component.get("v.dirFactTxt");
        
        if (idComponente == idTexto){
            var valorSeleccionado = event.getParam("valorSeleccionado");        
            var mapF = component.get("v.mapaDirFacturacionHeredado");
            component.set("v.dirSeleccionado" , mapF.get(parseInt(valorSeleccionado)));
            component.set("v.isEditadoUnica", true);
        }
    },
    
    guardar : function (component, event, helper) {
        var guardamos = component.get("v.guardarHeredado");   
        if (guardamos){
            var isEditadoUnica = component.get("v.isEditadoUnica");
            if (isEditadoUnica){
                helper.guardar(component);
            }   
        }    
    }
})