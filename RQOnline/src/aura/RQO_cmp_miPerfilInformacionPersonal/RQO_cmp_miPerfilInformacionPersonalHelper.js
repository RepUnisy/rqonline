({
	updateLanguage : function(component, language) {
		//	Actualizar idioma
        var action = component.get("c.updateLanguage");
        action.setParams({
            language : language
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	// Refrescar la página
            	//$A.get('e.force:refreshView').fire();
            }
            else
            {
            }
        });
        
        $A.enqueueAction(action);
	}
})