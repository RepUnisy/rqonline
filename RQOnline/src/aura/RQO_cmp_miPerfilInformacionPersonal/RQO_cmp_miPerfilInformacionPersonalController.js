({
	doInit : function(component, event, helper) {
		//	Obtenemos información del contcto
        var action = component.get("c.selectContactById");
        action.setParams({
            idContact : component.get("v.user.idContacto")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var contact = response.getReturnValue();
                component.set("v.contact", contact);
            }
        });
        
        $A.enqueueAction(action);
        
        //	Obtenemos Listado de idiomas
        // hints to ensure labels are preloaded
		// $Label.c.RQO_lbl_idioma_en_US
		// $Label.c.RQO_lbl_idioma_es
		// $Label.c.RQO_lbl_idioma_fr
		// $Label.c.RQO_lbl_idioma_de
		// $Label.c.RQO_lbl_idioma_it
		// $Label.c.RQO_lbl_idioma_pt
        var action = component.get("c.getLanguages");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var idiomas = response.getReturnValue();
                var numIdiomas = idiomas.length;
                var opciones = [];
                for(var i = 0;i < numIdiomas;i++)
                {
                	var idiomaLabel = $A.get("$Label.c.RQO_lbl_idioma_" + idiomas[i]);
                	var selected = (component.get("v.user.language") == idiomas[i])
                	opciones.push({'label':idiomaLabel, 'value':idiomas[i], 'selected':selected});
                }
                component.set("v.languages", opciones);
                component.set("v.currentLanguage", $A.get("$Label.c.RQO_lbl_idioma_" + component.get("v.user.language")));
            }
        });
        
        $A.enqueueAction(action);
    },
	enableEditMode : function(component, event, helper) {
		var editMode = component.get("v.editMode");
                   
            /******* F R O N T E N D *******/
           		jQuery('.rq-btn').toggleClass('rq-edit rq-save');
        	/*** F I N   F R O N T E N D ***/
        
        
		if (editMode)
		{
			helper.updateLanguage(component, component.find("sltLanguage").get("v.value"));
			component.set("v.currentLanguage", $A.get("$Label.c.RQO_lbl_idioma_" + component.find("sltLanguage").get("v.value")));
			component.set("v.user.language", component.find("sltLanguage").get("v.value"));
			component.set("v.editMode", false);
			location.reload();
		}
		else
			component.set("v.editMode", true);
        
	}
})