({
	doInit : function(component, event) {
        var numeroPaginas = component.get("v.pagina");
	},
    cambiarPagina : function(component, event) {
        component.set("v.finalPaginacion", (component.get("v.pagina") * $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla")));
        component.set("v.inicioPaginacion", (component.get("v.pagina") * $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla")) - $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla"));
        var finalPaginacion = component.get("v.finalPaginacion");
        var listaKeys = component.get("v.listaKeys");
        component.set("v.listaKeysPaginacion", listaKeys.slice(component.get("v.inicioPaginacion"), component.get("v.finalPaginacion")));
        var listaOrdenada = component.get("v.listaOrdenar");
		var indiceOrdenacion = component.get("v.OrdenacionActual");
        component.set("v.listaOrdenarPaginacion", listaOrdenada.slice(component.get("v.inicioPaginacion"), component.get("v.finalPaginacion")));
		
        

        
        /* PARTE FRONT-END ORDEN*/
      
          /*ESTILOS PARA LA PAGINACION ACTIVA*/
          jQuery('.activePage').removeClass('activePage');
           var elemento = event.currentTarget;  
          jQuery(elemento).addClass('activePage');
        	/*FIN*/

       setTimeout(function(){

           jQuery('tbody td').each(function()
				{
              	if(jQuery(this).index() == indiceOrdenacion)
				   {
               		 jQuery(this).addClass('sortable-active');
            	   }
				});
            },  100);
        /* FIN FRONT-END */

    }
})