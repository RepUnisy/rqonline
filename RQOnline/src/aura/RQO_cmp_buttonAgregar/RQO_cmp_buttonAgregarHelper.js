({
    getInfo : function(component, event) {
        
        var idGrado = component.get("v.id");
        var idContacto = component.get("v.user.idContacto");
        var action = component.get('c.isMiCatalogoGrado');
        action.setParams({ "grado" : idGrado, "contacto": idContacto });
        action.setCallback(this, function(response){            
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var isMiCatalogo = response.getReturnValue();
                component.set("v.isMiCatalogo", isMiCatalogo);
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }            
        });
        $A.enqueueAction(action);
    },
    insertMiCatalogoFireEvent : function(component, event){
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_quiereInsertar"), "confirmAlert" : true, 
                            "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), "idAlert" : $A.get("$Label.c.RQO_lbl_insertGradeAlert_const"), "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
           // this.showSpinner(component);
            console.log(err.message);
        }
    },
    deleteMiCatalogoFireEvent : function(component, event){
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_quiereBorrar"), "confirmAlert" : true, 
                            "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), "idAlert" : $A.get("$Label.c.RQO_lbl_deleteGradeAlert_const"), "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
         //   this.showSpinner(component);
            console.log(err.message);
        }
    },
    insertMiCatalogo : function(component, event) {
        var idGrado = component.get("v.id");
        var idContacto = component.get("v.user.idContacto");
        var action = component.get('c.insertMiCatalogoGrado');
        action.setParams({ "grado" : idGrado, "contacto": idContacto });
        action.setCallback(this, function(response){            
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var spinner = component.find("mySpinner");
        		$A.util.removeClass(spinner, 'slds-show');
        		$A.util.addClass(spinner, 'slds-hide');
                var insertado = response.getReturnValue();
                if (insertado == true){
                    component.set("v.isMiCatalogo", true);
                    debugger;
                    var user = component.get("v.user");
                    //Verificamos si el usuario esta logado y en caso de ser así lanzamos el registro de evento
                    if(user && user.isPrivate){
                        var idGrado = component.get("v.id");
                        this.registrarEvento(component, '7', idGrado);
                    }
                }
                
            }else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }else if (state === "ERROR") {
                    
                var errors = response.getError()
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
           }            
        });
        $A.enqueueAction(action);
    },
    deleteMiCatalogo : function(component, event) {
        var idGrado = component.get("v.id");
        var idContacto = component.get("v.user.idContacto");
        var action = component.get('c.deleteMiCatalogoGrado');
        action.setParams({ "grado" : idGrado, "contacto": idContacto });
        action.setCallback(this, function(response){
			var spinner = component.find("mySpinner");
        	$A.util.removeClass(spinner, 'slds-show');
        	$A.util.addClass(spinner, 'slds-hide');            
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var insertado = response.getReturnValue();
                if (insertado == true){
                    component.set("v.isMiCatalogo", false);
                    
                    debugger;
                    var user = component.get("v.user");
                    //Verificamos si el usuario esta logado y en caso de ser así lanzamos el registro de evento
                    if(user && user.isPrivate){
                        var idGrado = component.get("v.id");
                        this.registrarEvento(component, '10', idGrado);
                    }
                    
                }
                
            }else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }else if (state === "ERROR") {
                    
                var errors = response.getError()
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
			}        
        });
        $A.enqueueAction(action);
    }, registrarEvento: function(component, actionDescription, idGrado){
        /*
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist,
         * el valor 6 = Grade Assignment / Deassignment to my catalog.
         * actionType - Grupo al que pertenece, 1 = Product Catalogue
         * actionDescription - descripción de la acción que lanza el evento, 7 = Assignment of grade to my catalog, 
         * 10 - De-allocation to my catalog
         * reGradeId - id del grado que lanza el evento
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_buttonAgregar', "reComponentDescription" : '6', 
                            "actionType" : '1', "actionDescription" : actionDescription, "reGradeId" : idGrado,
                            "rePathActual" : window.location.pathname});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})