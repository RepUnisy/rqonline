({
	volerSolicitar : function(component, event) {
        component.set("v.mostrarSpinner", true);
		var numPosicion = event.getSource().get("v.name");
        
        var user = component.get("v.userHeredado");
        var wp = component.get("v.objDataHeredado");
        
        var idAccount = user.idCliente;
        var idContact = user.idContacto;
        var numRefCliente = wp.numRefCliente;
        var idGrado = wp.idGrado;
        var idQP0Grado = wp.qp0gradeId;
        var envaseEnviar = wp.envase;
        var modoEnvioEnviar = wp.modoEnvio;
        var intCantidad = wp.cantidad;
        var unidadMedida = wp.modoEnvio;
        var fechaEnvio = null;
        
        debugger;
        var action = component.get('c.insertAsset');
        action.setParams({ "idGrado" : idGrado, "idQP0Grade" : idQP0Grado, "AccountId" : idAccount, "ContactId" : idContact, "envase" : envaseEnviar, "modoEnvio" : modoEnvioEnviar,  "fechaEnvio" : fechaEnvio, "cantidad" : intCantidad });
        action.setCallback(this, function(response){
            component.set("v.mostrarSpinner", false);
            var state = response.getState();
            debugger;
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                this.fireAlertEvent($A.get("$Label.c.RQO_lbl_mensajeAnadirGrado"), $A.get("$Label.c.RQO_lbl_informacion"));
            }
            else if (state === "INCOMPLETE") {
                this.fireAlertEvent($A.get("$Label.c.RQO_lbl_noSeHaAnadido"), $A.get("$Label.c.RQO_lbl_error"));
            }
			else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                this.fireAlertEvent($A.get("$Label.c.RQO_lbl_noSeHaAnadido"), $A.get("$Label.c.RQO_lbl_error"));
			}  
        });
        $A.enqueueAction(action);
        
	},
    toggleTabla : function(component,event) {
        /*:::::::::::::::::::*/
        /* F R O N T - E N D */
        /*:::::::::::::::::::*/
		var element = event.currentTarget;
            jQuery(element).addClass('this');
             
            //Si ya estaba desplegado
            if(jQuery('.this').closest('tr').children('td:not(:nth-child(-n+2))').hasClass('rq-table-block'))
            {
                
                jQuery('.this').closest('tr').children('td.rq-table-block').slideUp(0, function() {
                    jQuery('.this').closest('tr').children('td:not(:nth-child(-n+2))').removeClass('rq-table-block');
               		jQuery('.this').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                    
                });
            }
            else{
            //si esta oculto
                jQuery('.this').closest('tr').children('td:not(:nth-child(-n+2))').addClass('rq-table-block');
                jQuery('.this').addClass('fa-chevron-up').removeClass('fa-chevron-down');
                jQuery('td:last-child').removeClass('rq-table-block');
                jQuery('.rq-table-block').slideDown(0,'rq-table-block');
            }
            jQuery(element).removeClass('this');
    }, 
    openDownloadDocTable : function(component, event){
        var item = event.getSource().get("v.value");
        var appEvent = $A.get("e.c:RQO_evt_OpenTableDoc");
        appEvent.setParams({"numDocumento" : item.numEntrega, "showAlbaranCMR" : item.showAlbaranCMR,"showCertificadoAnalisis" : item.showCertificadoAnalisis});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }, fireAlertEvent : function(textAlert, titleAlertValue){
        
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : textAlert, "confirmAlert" : false, 
                            "titleAlertValue" : titleAlertValue, "idAlert" : 'downInvoice', "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }
})