({
	getData : function(component, event, helper) {
        if(!$A.util.isEmpty(component.get("v.idiomaAux")) && !$A.util.isEmpty(component.get("v.idAux"))){
			helper.getData(component, event);
        }
	},
    toggleCaracteristicas : function(component, event, helper) {
		if(window.matchMedia("(max-width:767px)").matches)
        {
            jQuery('.rq-title-caracteristicas').toggleClass('plegado');
            jQuery('.rq-toggle-caracteristicas').slideToggle();
        }
	}
})