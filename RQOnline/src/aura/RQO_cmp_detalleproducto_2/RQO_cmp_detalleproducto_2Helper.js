({
	getData : function(component, event) {
		
    	var id_str = component.get("v.idAux");
        console.log('AA envio id: ' + id_str);
        var idioma = component.get("v.idiomaAux");
        console.log('AA idioma: ' + idioma);

        if(id_str){
            var action = component.get('c.getDetalleProducto');
            console.log('AA idioma: ' + idioma);
            action.setParams({ "lenguaje" : idioma, "idProducto" : id_str });
            action.setCallback(this, function(response) {
                //store response state 
                var state = response.getState();
                if (state === "SUCCESS") {
                    // store the response of apex controller (return map)     
                    var storedResponse = response.getReturnValue();
                    if (!storedResponse){
                        console.log('No hay datos de caracteristicas. Ocultamos el div');
                        component.set('v.lengthListaCaracteristicas', 0);
                        this.hideData(component, event);
                    }
                    console.log(storedResponse);
                    // set the store response[map] to component attribute, which name is map and type is map.   
                    component.set('v.listaCaracteristicas', storedResponse);
                }
            });  
            // enqueue the Action   
            $A.enqueueAction(action);
        }else{
            console.log('AA no tengo id, voy a ocultar. ');
            this.hideData(component, event);
        }
	},    
    hideData : function(component, event) {
        console.log('AA no tengo id, voy a ocultar 2. ');
        var cmpTarget = component.find('divIteration');
        console.log(cmpTarget);
        $A.util.removeClass(cmpTarget, 'rq-caracteristicas-tecnicas');
        $A.util.addClass(cmpTarget, 'rq-caracteristicas-tecnicas-hide');
    }   
})