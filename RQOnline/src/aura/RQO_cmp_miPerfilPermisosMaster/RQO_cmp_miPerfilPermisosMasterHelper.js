({
	filter : function(component, filter) {
    	// Filtrar listado de usuarios
    	var action;
    	
    	if (filter.length == 0) {
	    	action = component.get("c.selectUsersByAccountId");
	        action.setParams({
	            idAccount : component.get("v.user.cliente.Id")
	        });
    	}
    	else {
	    	action = component.get("c.selectUsersByName");
	        action.setParams({
	        	idAccount : component.get("v.user.cliente.Id"),
	            name : filter
	        });
    	}

        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var users = response.getReturnValue();
                component.set("v.users", users);
            }
            else {
            	component.set('v.visible',false);
            	component.set('v.textoAlerta',response.getError()[0].message);
            	component.set('v.showAlert',true);
            }
        });
        
        $A.enqueueAction(action);
    },
	save : function(component, users) {
    	// Actualizar listado de perfiles
    	var action = component.get("c.updateProfile");
        action.setParams({
            users : users
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	component.set('v.visible',false);
            	component.set('v.textoAlerta',component.get("v.textoAlertaDefault"));
                component.set('v.showAlert',true);
            }
            else {
            	component.set('v.visible',false);
            	component.set('v.textoAlerta',response.getError()[0].message);
            	component.set('v.showAlert',true);
            }
        });
        
        $A.enqueueAction(action);
    },
})