({
    doInit : function(component, event) {
		component.set("v.catalogo", true);
        component.set("v.varEsconderCatalogo", true);
    }, getInfo : function(component, event) {
		var language = event.getParam("language");
		var user = event.getParam("user");
        
        component.set("v.language", language);
        component.set("v.user", user);
		console.log('ACL. Tu equipo language ' + user);
        //var usuario = component.get("v.user");
        if(user && user.isPrivate){
            this.registrarEvento(component);
        }
	}, registrarEvento: function(component){    	
        var appEvent = $A.get("e.c:RQO_evt_RegisterEvent");
    	appEvent.setParams({"reComponentName" : 'RQO_cmp_ListCategoriasTablaGrados', "reComponentDescription" : '3', 
    						"actionType" : '1', "actionDescription" : '1'});
		try{
    		appEvent.fire();
		}catch(err){
    		console.log(err.message);
		}
	}
})