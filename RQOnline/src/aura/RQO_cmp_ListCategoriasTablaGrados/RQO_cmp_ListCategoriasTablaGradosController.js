({
	doInit : function(component, event, helper) {
        helper.doInit(component, event);
	},
    cargaTabla : function(component, event, helper) {
        console.log('Entra a cargaTabla.');
        var mapatabla = event.getParam("mapaTabla");
        component.set("v.mapaTabla", event.getParam("mapaTabla"));
        console.log('Este es el resultado actual: ' + component.get("v.mapaTabla"));
	},
    esconderCatalogo : function(component, event, helper){
		component.set("v.varEsconderCatalogo", event.getParam("boolEsconderCatalogo"));        
    },
	getInfo : function(component, event, helper) {
		helper.getInfo(component, event);
	}
})