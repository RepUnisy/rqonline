({
	getData : function(component, event) {
        /*********NO BORRAR**********/
        // No borrar estos comentario bajo ninguna circunstancia. Precargan los labels. 
        // https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/labels_dynamic.htm
        // $Label.c.RQO_lbl_aromaticosSolventes
        // $Label.c.RQO_lbl_olefinasProducto
        // $Label.c.RQO_lbl_fertilizantes
        // $Label.c.RQO_lbl_pib
        // $Label.c.RQO_lbl_oxidoPropileno
        // $Label.c.RQO_lbl_polietilenoAltaDensidad
        // $Label.c.RQO_lbl_acnMmaPmma
        // $Label.c.RQO_lbl_polioles
        // $Label.c.RQO_lbl_metanol
        // $Label.c.RQO_lbl_lab
        // $Label.c.RQO_lbl_polipropilenoProducto
        // $Label.c.RQO_lbl_estirenoProducto
        // $Label.c.RQO_lbl_anhidridoMaleico
        // $Label.c.RQO_lbl_glicolesPropilenicos
        // $Label.c.RQO_lbl_polietilenoBajaDensidad
        // $Label.c.RQO_lbl_copolimerosEba
        /***************************/
        
       	var dateActual = new Date();
        var dateFrom = new Date(dateActual.getFullYear()-1, 0, 1);
        
        component.set("v.monthFrom", String(dateFrom.getMonth()+1));
        component.set("v.yearFrom", String(dateFrom.getFullYear()));
        component.set("v.monthTo", String(dateActual.getMonth()+1));
        component.set("v.yearTo", String(dateActual.getFullYear()));
		component.find("selectionYearFrom").set("v.value", String(dateFrom.getFullYear()));
        component.find("selectionMonthFrom").set("v.value",  String(dateFrom.getMonth()+1));
        component.find("selectionYearTo").set("v.value", String(dateActual.getFullYear()));
        component.find("selectionMonthTo").set("v.value", String(dateActual.getMonth()+1));
        
		var actionMonths = component.get('c.getMonths');
        actionMonths.setCallback(this, function(responseMonths) {
            var stateMonths = responseMonths.getState();
            if (stateMonths === "SUCCESS"){
                var storedResponseMonths = responseMonths.getReturnValue();
                if (storedResponseMonths){
					var customMap = [];
                    for (var key in storedResponseMonths) {
                        customMap.push({value:storedResponseMonths[key], key:key});
                    }
                    component.set("v.months", customMap);
                }
            }
        }); 
        $A.enqueueAction(actionMonths);
        
        var actionYears = component.get('c.getYears');
        actionYears.setCallback(this, function(responseYears) {
            var stateYears = responseYears.getState();
            if (stateYears === "SUCCESS"){
                var storedResponseYears = responseYears.getReturnValue();
                if (storedResponseYears){
                    component.set("v.years", storedResponseYears);
                }
            }
        });
        $A.enqueueAction(actionYears);
        
		var actionFamily = component.get('c.getFamilies');
        actionFamily.setCallback(this, function(responseFamily){
            var stateFamily = responseFamily.getState();
            if (stateFamily === "SUCCESS"){
                var storedResponseFamily = responseFamily.getReturnValue();
                if (storedResponseFamily){
                    var listData = [];
                    var trasnslate = '';
                    for (var key in storedResponseFamily) {
                        if (storedResponseFamily[key]){
                            trasnslate = $A.get("$Label.c." + storedResponseFamily[key]);
                            if (trasnslate){
                            	listData.push({value:trasnslate, key:key});
							}
                        }
                    }
                    component.set("v.families", listData);
                }
            }
        }); 
        $A.enqueueAction(actionFamily);
        
	}, search : function(component, event) {
        debugger;
        var family = component.find("familyName").get("v.value");
        var yearFrom = component.find("selectionYearFrom").get("v.value");
        var monthFrom = component.find("selectionMonthFrom").get("v.value");
        var yearTo = component.find("selectionYearTo").get("v.value");
        var monthTo = component.find("selectionMonthTo").get("v.value");
        var dateFrom = null;
        if (yearFrom && monthFrom){
            dateFrom = new Date(yearFrom,monthFrom,0);
        }
        var dateTo = null;
		if (yearTo && monthTo){
            dateTo = new Date(yearTo,monthTo,0);
        }
        if (dateFrom > dateTo){
            this.fireAlertEvent($A.get("$Label.c.RQO_lbl_mensajeValidacionFecha"), $A.get("$Label.c.RQO_lbl_informacion"));
			return;
        }
        if (dateTo.getYear()-2 >= dateFrom.getYear()){
            this.fireAlertEvent('No puede visualizar más de dos años',  $A.get("$Label.c.RQO_lbl_informacion"));
            return;
        }
        var appEvent = $A.get("e.c:RQO_evt_searchOrder");
        appEvent.setParams({"grado" : null, "family" : family, "yearFrom" : yearFrom, "monthFrom" : monthFrom, "yearTo" : yearTo, 
                            "monthTo" : monthTo});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}, fireAlertEvent : function(textAlert, titleAlertValue){
        
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : textAlert, "confirmAlert" : false, 
                            "titleAlertValue" : titleAlertValue, "idAlert" : 'searchConsumption', "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }
})