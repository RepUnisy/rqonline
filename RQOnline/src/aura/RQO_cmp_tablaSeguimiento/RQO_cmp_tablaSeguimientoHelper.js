({
	getInfo : function(component, event) {
        /*var language = event.getParam("language");
		var user = event.getParam("user");*/
        var language = component.get("v.language");
        var user = component.get("v.user");
        var idAccount = user.idCliente;
        //component.set("v.language", language);
        //component.set("v.user", user);
		this.getData(component, idAccount, language, null, null, null, null, null, null, null, null, null, null, true);
	},
    getData : function(component, idAccount, idioma, solNumber, orderNumber, refCliente, grade, fechaEntregaDesde, fechaEntregaHasta,
                      searchStatus, searchDestination, fechaPreferenteEntregaDesde, fechaPreferenteEntregaHasta, isInitialSearch){
        component.set("v.mostrarSpinner", true);
        //Vamos a por los datos de seguimiento
        var action = component.get('c.getSeguimiento');
        action.setParams({"idAccount" : idAccount, "idioma" : idioma, "solNumber" : solNumber, "orderNumber" : orderNumber, "refCliente" : refCliente, 
                          "grade" : grade, "fechaEntregaDesdeDt" : fechaEntregaDesde, "fechaEntregaHastaDt" : fechaEntregaHasta,
                         "searchStatus" : searchStatus, "searchDestination" : searchDestination, 
                          "fechaPreferenteDesde" : fechaPreferenteEntregaDesde, "fechaPreferenteHasta" : fechaPreferenteEntregaHasta, 
                          "isInitialSearch" : isInitialSearch});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var storedResponse = response.getReturnValue();
                if (storedResponse){
                    var listData = JSON.parse(storedResponse);
                    //Lista inicial sin ordenación
                    component.set("v.resulsetData", listData);
                    //Ahora vamos a ordenar los datos para mostrar por pantalla
                    this.getOrderedData(component, listData, false);
                    this.registrarEvento();
                }else{
                    component.set("v.resulsetData", null);
                    component.set("v.orderResultSetData", null);
                    component.set("v.vacio", true);
                }
            }else{
                component.set("v.error", true);
            }
            component.set("v.mostrarSpinner", false);
        }); 

        $A.enqueueAction(action);
    }, 
    getOrderedData : function (component, listData, showSpinner){
		/*********NO BORRAR**********/
        // No borrar estos comentario bajo ninguna circunstancia. Precargan los labels. 
        // https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/labels_dynamic.htm
		// $Label.c.RQO_lbl_pedidoSeguimientoPreferentDate
        // $Label.c.RQO_lbl_pedidoSeguimientoGrade
        // $Label.c.RQO_lbl_pedidoSeguimientoReferenciaCliente
        // $Label.c.RQO_lbl_pedidoSeguimientoDestination
        // $Label.c.RQO_lbl_Status
        // $Label.c.RQO_lbl_pedidoSeguimientoGrade
        
		/***************************/
        
        //En este método se agrupan los datos que llegan del controlador apex en un mapa para mostrar por pantalla
        //y después se establece la paginación en la página 1
        if (showSpinner){
            component.set("v.mostrarSpinner", true);
        }
        component.set("v.vacio", false);
        component.set("v.error", false);
        var  groupField = component.get("v.groupByField");
        this.getOrderedLabel(component, groupField);
        var myMap = new Map();
        var objAux = null;
        var keyAux = null;
        var listAux = null;
        for(var i = 0;i<listData.length;i++){
            objAux = listData[i];
            if (!objAux.canceladoView){
                keyAux = objAux[groupField];
                if (myMap.has(keyAux)){
                    listAux = myMap.get(keyAux);
                }else{
                    listAux = [];
                    myMap.set(keyAux, listAux);
                }
                listAux.push(objAux);
			}
        }
        
        var customMap = [];
        for (var key of myMap.keys()) {
            customMap.push({value:myMap.get(key), key:key});
        }
        component.set("v.orderResultSetData", customMap);
        //Cálculo datos paginación inicial
        var numeroPaginas = parseInt((customMap.length / $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla")) + 1);
        if(customMap.length % $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla") == 0){
            numeroPaginas -= 1;
        }
        var listaPaginas = [];
        for (var i = 1; i <= numeroPaginas; i++) {
            listaPaginas.push(i);
        }
        component.set("v.numeroPaginas", listaPaginas);
        if(listaPaginas.length <= 1){
            component.set("v.mostrarPaginacion", false);
        }else{
            component.set("v.mostrarPaginacion", true);
        }
        var inicioPaginacion = $A.get("$Label.c.RQO_lbl_cero");
        var finPaginacion = $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla");
        component.set("v.inicioPaginacion", inicioPaginacion);
        component.set("v.finalPaginacion", finPaginacion);
        this.paginacion(component, customMap, inicioPaginacion, finPaginacion, 1);
        if (showSpinner){
            component.set("v.mostrarSpinner", false);
        }
    }, 
    searchData : function (component, event){
        //Búsqueda que se realiza desde el filtro
        component.set("v.vacio", false);
        component.set("v.error", false);
        component.set("v.resulsetData", null);
        component.set("v.orderResultSetData", null);
        var user = component.get("v.user");
		var idAccount = user.idCliente;
        this.getData(component, idAccount, component.get("v.language"), event.getParam("solicitud"), event.getParam("pedido"), event.getParam("refCliente"),
                     event.getParam("grado"), event.getParam("dateFrom"), event.getParam("dateTo"), event.getParam("estado"), event.getParam("destino"),
                     event.getParam("datePreferentDeliveryFrom"),event.getParam("datePreferentDeliveryTo"), false);
        
    }, 
    getOrderedLabel : function (component, groupByCode){
		var translatedLabel = '';
        switch (groupByCode) {
            case 'srcFechaPreferente':
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoPreferentDate");
                break;
            case 'qp0gradeName':
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoGrade");
                break;
            case 'numRefCliente':
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoReferenciaCliente");
                break;
            case 'destinoMercanciasNombre':
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoDestination");
                break;
            case 'statusDescription':
                translatedLabel = $A.get("$Label.c.RQO_lbl_Status");
                break;
            default: 
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoGrade");
        }
        component.set("v.groupByDescription", translatedLabel);
    },
    paginacion : function(component, lista, inicioPaginacion, finPaginacion, pagina){
        component.set("v.listaPaginada", lista.slice(inicioPaginacion, finPaginacion));
    },
    cambiarPagina : function(component, event) {
       	component.set("v.mostrarSpinner", true);
        var pagina = parseInt(event.getSource().get("v.value"));
        component.set('v.paginaActiva',pagina);
        var inicioPaginacion = (pagina * $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla")) - $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla");
        var finPaginacion = (pagina * $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla"));
        
        component.set("v.inicioPaginacion", inicioPaginacion);
        component.set("v.finalPaginacion", finPaginacion);
		var customMap = component.get("v.orderResultSetData");
         /*:::: PARTE FRONT PAGINADOR :::::*/
            var paginadores = component.find('paginadores');
            
            for(var i in paginadores){
                $A.util.removeClass(paginadores[i], 'page-active');
            }
            
            $A.util.addClass(paginadores[pagina-1], 'page-active');
        /*:::: PARTE FRONT PAGINADOR :::::*/
        
        this.paginacion(component, customMap, inicioPaginacion, finPaginacion, pagina);
        component.set("v.mostrarSpinner", false);
        
    }, 
    handleCancelValueChange : function(component, event){
        //Este método se ejecuta cuando en un componente hijo se cambia la variable cancelledId o cuando se setea a null
        //si el cliente no quiere cancelar la posición
        var cancel = component.get('v.cancelledId');
        //Si tiene dato preguntamos por la cancelación
        if (cancel){
            component.set("v.mostrarSpinner", true);
            this.fireAlertEvent($A.get("$Label.c.RQO_lbl_cancelacionSolicitud"), $A.get("$Label.c.RQO_lbl_informacion"), true);
            component.set("v.mostrarSpinner", false);
        }else{
            //Si está nulo es porque se ha rechazado el envío de la cancelación
            return;
        }
    }, enviarCancelacion : function(component, event) {
		component.set("v.mostrarSpinner", true);
        //Envíamos la cancelación a SAP
        var selectedAsset = component.get('v.cancelledId');
        var cancelledAsset = [];
		cancelledAsset.push(selectedAsset);
        var action = component.get('c.sendCancelledPositions');
        action.setParams({"canceledAssets" : cancelledAsset});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var messagge =  $A.get("$Label.c.RQO_lbl_mensajeSolicitudEliminada2");
            var tittle = $A.get("$Label.c.RQO_lbl_error");
            if (state === "SUCCESS"){
                var storedResponse = response.getReturnValue();
                if (storedResponse){
                    var dataReturn =  JSON.parse(storedResponse);
                    if (dataReturn.errorCode == 'OK'){
						messagge = $A.get("$Label.c.RQO_lbl_mensajeSolicitudEliminada1");
                        tittle = $A.get("$Label.c.RQO_lbl_informacion");
                        component.set('v.changeCancelStyle', component.get('v.cancelledId'));
                    }else if(storedResponse && storedResponse.errorCode == 'NV'){
                        messagge = $A.get("$Label.c.RQO_lbl_mensajeSolicitudEliminada3");
                        tittle = $A.get("$Label.c.RQO_lbl_GenericAlertTitle");
                    }         
                }
            }
            component.set("v.mostrarSpinner", false);
            this.fireAlertEvent(messagge, tittle, false);
        }); 

        $A.enqueueAction(action);
    }, 
    fireAlertEvent : function(textAlert, titleAlertValue, confirmAlert){
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : textAlert, "confirmAlert" : confirmAlert, 
                            "titleAlertValue" : titleAlertValue, "idAlert" : 'seguimientoCancel', "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    },
    handleResponseAlert : function(component, event){
        //Capturamos las respuestas del componente de alertas
		var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");        
        //Si recibimos un true como respuesta (ha pulsado Aceptar) y el id que nos devuelve es el de cancelación de posición de solicitud 
        //lanzamos la función de envío de cancelación a SAP
        if (responseAlert && responseAlert == true && idAlert && idAlert == 'seguimientoCancel'){
			this.enviarCancelacion(component, event);
        }else{
            component.set('v.cancelledId', null);
        }
    }, registrarEvento: function(){
        /*
         * Registro de los eventos de consulta de listado de pedidos desde el seguimiento
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         * 	Valor 14 = Query order list from tracking
         * actionType - Grupo al que pertenece, 2 = Order
         * actionDescription - descripción de la acción que lanza el evento. En este caso consulta de listado de pedidos desde el 
         * seguimiento. Valores para este componente:
         * 	Valor 16 = Query order list
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_tablaSeguimiento', "reComponentDescription" : '14', 
                            "actionType" : '2', "actionDescription" : '16', "rePathActual" : window.location.pathname});
        
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})