({
	onRender : function(component, event, helper) {
		var perfiles = component.get("v.perfiles");
		//helper.profileChange(component, perfiles[0]);
	},
    close : function(component, event, helper) {
        helper.close(component, event);
	},
    profileChange : function(component, event, helper) {
    	var valorSeleccionado = event.getParam("valorSeleccionado");
    	var perfiles = component.get("v.perfiles");
    	helper.profileChange(component, perfiles[valorSeleccionado]);
    },
})