({
    
    getInfoMenu : function(component, event, helper) {   
        helper.getInfoMenu(component, event);
    },
    getCatPropuesto : function(component, event, helper) {    
        component.set("v.mostrarSpinner", true);
        helper.spinner(component);
        helper.proponerCatalogo(component, event);        
    }
    
})