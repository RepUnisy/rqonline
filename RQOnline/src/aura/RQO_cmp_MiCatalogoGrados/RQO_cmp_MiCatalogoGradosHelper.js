({
        /*	Obtenemos la información del menú	*/
    getInfoMenu : function(component, event) {
        var language = event.getParam("language");
        var user = event.getParam("user");
        component.set("v.language", language);
        component.set("v.user", user);
        console.log('ACL. Detalle producto language ' + language)
        //	Obtenemos la información de mi catalogo
        this.getMiCatalogo(component, event);
    },
    
    /*	Obtenemos los grados que tiene el usuario en mi catálogo	*/
    getMiCatalogo : function(component, event) {
        var IdContacto = component.get("v.user.idContacto");
        var language = component.get("v.language");
        console.log('ACL. id: ' + IdContacto)
        var action = component.get('c.getMiCatalogo');
        console.log('ACL. Id contacto ' + IdContacto)	
        action.setParams({ "IdContacto" : IdContacto, "language" : language });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                
                var respuesta = JSON.parse(response.getReturnValue());
                console.log('ACL. Respuesta: ' + respuesta)               
                
                if (respuesta == null){
                    component.set("v.hasCatalogo", false);
                }
                else{
                    component.set("v.listTabla" , respuesta);
                    component.set("v.hasCatalogo", true);
                    console.log('ACL. Introducida la tabla: ' + respuesta)                    
                }                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }  
            component.set("v.mostrarSpinner", false);
        });
        $A.enqueueAction(action);
    },
    
    /*	Propone de nuevo mi catálogo	*/
    proponerCatalogo : function(component, event) {
        var IdCliente = component.get("v.user.idCliente");
        var IdContacto = component.get("v.user.idContacto");
        var action = component.get('c.volverProponer');
        action.setParams({ "IdContacto" : IdContacto , idAccoun : IdCliente});
        console.log('ACL. proponer pedido para: ' + IdContacto)
        action.setCallback(this, function(response){
            this.spinner(component);
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                
                 var respuesta = JSON.parse(response.getReturnValue());
                console.log('ACL. Respuesta: ' + respuesta)               
                
                if (respuesta == null){
                    component.set("v.hasCatalogo", false);
                }
                else{
                    component.set("v.listTabla" , respuesta);
                    component.set("v.hasCatalogo", true);
                    console.log('ACL. Introducida la tabla: ' + respuesta)                    
                }
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                        
           component.set("v.mostrarSpinner", false);
        });
        $A.enqueueAction(action);  
    },
    
    spinner: function(component){
    	var spinnerTop = component.find("spinnerPadre");        
        $A.util.toggleClass(spinnerTop, "slds-show");   
	}
    
})