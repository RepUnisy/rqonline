({
    doInit : function(component, event, helper) {
        helper.getInfoHerencia(component, event, helper);
        
    },

    getInfo : function(component, event, helper) {
        
        helper.getInfoHerencia(component, event, helper);
        //	Obtenemos los parámetros de la url
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); 
        var sURLVariables = sPageURL.split('&'); 
        var sParameterName;
        var idGrado = '';
        
        for (var i = 0; i < sURLVariables.length; i++) {
            console.log('todo '+ sURLVariables[i]);
            sParameterName = sURLVariables[i].split('='); 
            
            if (sParameterName[0] === 'id') {
                idGrado = sParameterName[1];
            }
        }
        component.set("v.id", idGrado);
        
        helper.lanzaComponenteTabla(component, event);
    },
    
})