({
        doInitDataProd4 : function(component, event) {
            debugger;
            var actiontranslate = component.get('c.getDocTypeTittleTranslate');
            actiontranslate.setCallback(this, function(responseTranslate) {
                var stateTranslate = responseTranslate.getState();
                if (stateTranslate === "SUCCESS") {
                    var customTranslateList = [];
                    var storedResponseTranslate = responseTranslate.getReturnValue();
                    for (var key in storedResponseTranslate) {
                        customTranslateList.push({value:storedResponseTranslate[key], key:key});
                    }
                    component.set('v.docTypeTittleTranslate',customTranslateList);
                }
            }); 
            $A.enqueueAction(actiontranslate);
            var idGrade = component.get("v.idAux");
            //var user = component.get('v.userHeredado');
            var action = component.get('c.doInitDataProd4Apex');
            action.setParams({ "idGrade" : idGrade});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var customList = [];
                    var storedResponse = response.getReturnValue();
                    for (var key in storedResponse) {
                        customList.push({value:storedResponse[key], key:key});
                    }
                    component.set("v.resulsetData", customList);
                }
            });
            // enqueue the Action   
            $A.enqueueAction(action);
        }
    })