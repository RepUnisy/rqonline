({
	getInfo : function(component, event, helper) {
		helper.getInfo(component, event);
	}, searchConsumption : function(component, event, helper){
        helper.searchConsumption(component, event);
    }, cambiaVista: function(component, event, helper){
        helper.cambiaVista(component, event);
    },cambiaVistaEuros: function(component, event, helper){
        helper.cambiaVistaEuros(component, event);
    }, exportConsumption: function(component, event, helper){
        helper.exportConsumption(component, event);
    }
})