({
	getInfo : function(component, event) {
        var language = event.getParam("language");
		var user = event.getParam("user");
        //var idAccount = user.idCliente;
        var idAccount = user.idCliente;
        component.set("v.language", language);
        component.set("v.user", user);
        this.getData(component, idAccount, null, new Date(new Date().getFullYear()-1, 0, 1), new Date());
	}, getData : function(component, idAccount, skuGrade, fechaInicio, fechaFin){
        /*********NO BORRAR**********/
        // No borrar estos comentario bajo ninguna circunstancia. Precargan los labels. 
        // https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/labels_dynamic.htm
		// $Label.c.RQO_lbl_aromaticosSolventes
        // $Label.c.RQO_lbl_olefinasProducto
        // $Label.c.RQO_lbl_fertilizantes
        // $Label.c.RQO_lbl_pib
        // $Label.c.RQO_lbl_oxidoPropileno
        // $Label.c.RQO_lbl_polietilenoAltaDensidad
        // $Label.c.RQO_lbl_acnMmaPmma
        // $Label.c.RQO_lbl_polioles
        // $Label.c.RQO_lbl_metanol
        // $Label.c.RQO_lbl_lab
        // $Label.c.RQO_lbl_polipropilenoProducto
        // $Label.c.RQO_lbl_estirenoProd
        // $Label.c.RQO_lbl_anhidridoMaleico
        // $Label.c.RQO_lbl_glicolesPropilenicos
        // $Label.c.RQO_lbl_polietilenoBajaDensidad
        // $Label.c.RQO_lbl_copolimerosEba
		/***************************/
        component.set("v.mostrarSpinner", true);
        debugger;
        var action = component.get('c.getSapConsumption');
        action.setParams({"clientId" : idAccount, "gradeSKU" : skuGrade, "fechaInicio" : fechaInicio, "fechaFin" : fechaFin});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var error = false;
            var noData = false;
            if (state === "SUCCESS"){
                var storedResponse = response.getReturnValue();
                if (storedResponse){
                    var dataObject = JSON.parse(storedResponse);
                    if (dataObject && dataObject.errorCode != 'NOK' && dataObject.errorCode != 'E'){
						this.setViewData(component, dataObject);
                    }else if (dataObject && dataObject.noDataError){
						noData = true;
                    }else{
                        error = true;
                    }
                }else{
					error = true;
            	}
            }else{
                error = true;
            }
            if (error || noData){
                component.set("v.headerListData", null);
                component.set("v.bodyListData", null);
                component.set("v.totalYearsCount", null);
                if (noData){
                	component.set("v.vacio", true);   
                }else{
                	component.set("v.error", true);    
                }
            }
            component.set("v.mostrarSpinner", false);
        }); 
        $A.enqueueAction(action);
    }, searchConsumption : function(component, event){
        component.set("v.error", false);
        component.set("v.vacio", false);
        debugger;
        var user = component.get("v.user");
        var idAccount = user.idCliente;
        var grade = event.getParam("grado");
        var dateFrom = null;
        var yearFrom = event.getParam("yearFrom");
        var monthFrom = event.getParam("monthFrom");
        if (yearFrom && monthFrom){
            dateFrom = new Date(yearFrom,monthFrom-1,1);
        }
        
		var dateTo= null;
        var yearTo = event.getParam("yearTo");
        var monthTo = event.getParam("monthTo");
		if (yearTo && monthTo){
            dateTo = new Date(yearTo,monthTo,0);
        }
        this.getData(component, idAccount, grade, dateFrom, dateTo);
    }, setViewData : function(component, dataObject){
        debugger;
        var customMap = [];
        if (dataObject.headerDataList && dataObject.headerDataList.length > 0){
            var description = ''
            for (var i = 0; i< dataObject.sapConsumptionsList.length; i++){
                if (dataObject.sapConsumptionsList[i].label){
                    //var yy = "$Label.c." + dataObject.sapConsumptionsList[i].label;
                    description = $A.get("$Label.c." + dataObject.sapConsumptionsList[i].label);
                     //description = $A.get(yy);
                    if (description){
                        dataObject.sapConsumptionsList[i].translatedFamily = description;
                    }else{                        
                        dataObject.sapConsumptionsList[i].translatedFamily = dataObject.sapConsumptionsList[i].family;
                    }
                }else{
                    dataObject.sapConsumptionsList[i].translatedFamily = dataObject.sapConsumptionsList[i].family;
                }
            }
            component.set("v.headerListData", dataObject.headerDataList);
            component.set("v.totalYearsCount", dataObject.totalYearsCount);
            component.set("v.bodyListData", dataObject.sapConsumptionsList);
        }else{
            component.set("v.vacio", true);
            component.set("v.headerListData", null);
            component.set("v.bodyListData", null);
            component.set("v.totalYearsCount", null);
        }
    }, cambiaVista: function(component, event){
        component.set("v.error", false);
        component.set("v.vacio", false);
        debugger;
        var tons = event.getParam("viewTableTons");
        component.set("v.viewTableTons", tons);
    }, cambiaVistaEuros: function(component, event){
        component.set("v.error", false);
        component.set("v.vacio", false);
        debugger;
		var euros = event.getParam("viewTableEuros");
		component.set("v.viewTableEuros", euros);
    }, exportConsumption : function (component,event){
        var headerListData = component.get("v.headerListData");
        var bodyListData = component.get("v.bodyListData");
        var exportType = event.getParam("exportType");
        
        // call the helper function which "return" the CSV data as a String   
        var csv = this.convertArrayOfObjectsToCSV(headerListData, bodyListData, exportType);   
        if (csv == null){return;}
        
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; // 
        hiddenElement.download = 'ExportData.csv';  // CSV file Name* you can change it.[only name not .csv] 
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click(); // using click() js function to download csv file
    }, convertArrayOfObjectsToCSV : function(headerListData, bodyListData, exportType) {
        debugger;
        var csvStringResult, counter, keysBody, columnDivider, lineDivider;
        if (headerListData == null || !headerListData.length || bodyListData == null || !bodyListData.length) {
            return null;
        }
        
		//Definimos los separadores de columna y linea
        columnDivider = ',';
        lineDivider =  '\n';

        csvStringResult = '';
        var csvHeaderYears = '';
        //Vamos a por la cabecera
		csvStringResult = this.createCsvHeader(headerListData, lineDivider, columnDivider);
        csvStringResult += this.createCsvBody(bodyListData, lineDivider, columnDivider, exportType);
        
        return csvStringResult;  
    }, createCsvHeader : function (headerListData, lineDivider, columnDivider){        
		var objHeader = null;
        var counter = 0;
        var countYears = 0;
        var countActualYears = 0;
        var csvStringResult = '';
        var csvStringHeaderResult = '';
        //Vamos a recorrer los datos de cabecera
        for (var i = 0; i<headerListData.length;i++){
            //El primer cuadro va vacío
            if (i==0){
                csvStringResult += '""';
                csvStringResult += columnDivider;
                csvStringHeaderResult += '""';
                csvStringHeaderResult += columnDivider; 
			}
            objHeader = headerListData[i];
			countActualYears = 0;
			//Vamos rellanando un string con formato correcto con los años asociados a cada mes. Además con esto sabemos cuantos años tiene asociados por si 
			//es necesario repetir el mes en la cabecera
			//Si tenemos lista de año para el mes la recorremos y añadimos cada año al string del csv 
            if (objHeader.years && objHeader.years.length > 0){
                for (var z = 0; z < objHeader.years.length; z++){
                    //Añadimos el separador de campos
                    if(countYears > 0){ 
                        csvStringHeaderResult += columnDivider; 
                    }   
                    csvStringHeaderResult += '"'+ objHeader.years[z]+'"';
                    countYears++;
                    countActualYears++;
                }
            }else{
                //Todos deberían tener tener años asociados, de no ser así pintamos espacio vacío.
                if(countYears > 0){ 
                    csvStringHeaderResult += columnDivider; 
                }   
                csvStringHeaderResult += '""';
                countYears++;
            }
            
			//Vamos a pintar el mes. Si tiene varios años repetiremos el mes hasta el mismo número de años
            for (var x = 0; x < countActualYears; x++){
				//Añadimos el separador de campos
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }
                csvStringResult += '"'+ objHeader.month+'"';
            }
            counter++;
        }
        csvStringResult += lineDivider;
        csvStringResult += csvStringHeaderResult;
        csvStringResult += lineDivider;
		return csvStringResult;
    }
    ,createCsvBody : function (bodyListData, lineDivider, columnDivider, exportType){
        var csvStringResult = '';
        var objBody = null;
        var counter = 0;
        //Vamos a por el cuerpo
        for (var i = 0; i<bodyListData.length;i++){
            objBody = bodyListData[i];
            counter = 0;
            //El primer dato es la familia
            csvStringResult += '"'+ objBody.translatedFamily+'"';
            csvStringResult += columnDivider;
            
            //Vamos a por los datos de la sublista
            for (var z = 0; z<objBody.dataList.length;z++){
                //Añadimos el separador de campos
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }
                if (exportType == 'tons'){
                	csvStringResult += '"'+ objBody.dataList[z].data+'"';    
                }else if(exportType == 'euros'){
                    csvStringResult += '"'+ objBody.dataList[z].importe+'"'; 
                }else{
                    csvStringResult += '""'; 
                }
                
                counter++;
            }
            csvStringResult += lineDivider;
        }
        return csvStringResult;
    }
})