({
    confirm : function(component, event, helper) {
        debugger;
        
        var idTemplate =  component.get("v.idTemplate");
        if (idTemplate != null){
            var popup = component.find('rqPopupPlantillas');
            $A.util.addClass(popup, 'ocultarPopup');
            helper.confirm(component,event)
        }
        else{
            var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
            appEvent.setParams({"textAlertValue" : 'Seleccione una plantilla', "confirmAlert" : false, 
                                "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), 
                                "idAlert" : $A.get("$Label.c.RQO_lbl_idInsertarPlantilla"), 
                                "parentLE" : false});
            try{ 
                appEvent.fire();
            }catch(err){
                this.showSpinner(component);
                console.log(err.message);
            }
        }
        
    },
    getTemplatesInfo : function(component, event, helper) {
        helper.getTemplates(component);
    },
    changeNewTemplate : function(component, event, helper) {
        debugger;
        if(component.get("v.changeNewTemplate")){
            helper.getTemplates(component);
        }
        component.set("v.changeNewTemplate", false);
    },
    handleResponseAlert : function(component, event, helper) {
        helper.handleResponseAlert(component, event);
    },
    check : function(component, event, helper) {
        //	En la versión winter 2017 la función event.getParam("value") retorna un string, y en la spring 2018 la función retorna un objeto
        var idTemp = typeof event.getParam("value") == typeof new Object ? event.getParam("value")[0] : event.getParam("value");
        component.set("v.idTemplate", idTemp);
    },
    close : function(component, event, helper) {
        component.set("v.visible", false);             
    },
    nuevaPlantilla : function(component, event, helper) {
        component.set("v.selectPlantilla", false)               
    },
    volverListPedido : function(component, event, helper) {
        component.set("v.selectPlantilla", true);             
    },
    crearNuevaPlantilla: function(component, event, helper) {
        
        var templateName = component.get("v.templateName");
        if (templateName == null || templateName == ''){
            var elemento = component.find("templateNameText");
            elemento.set("v.errors", [{message:"Introduce el nombre de la plantilla" }]);
        }
        else{
            helper.crearNuevaPlantilla(component);
        }
        
    },
    
    
})