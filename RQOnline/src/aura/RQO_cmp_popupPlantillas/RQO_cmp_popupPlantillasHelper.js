({
    close : function(component, event){
        component.set('v.visible',false);
    },
    
    crearNuevaPlantilla : function(component, event){
        debugger;
         var idAccount = component.get("v.userHeredado.idCliente");
        var idContacto = component.get("v.userHeredado.idContacto");
        var templateName = component.get("v.templateName");
        var action = component.get('c.createTemplate');               
        action.setParams({ templateName : templateName, idContact : idContacto, idAccount : idAccount});
        action.setCallback(this, function(response){
            var state = response.getState();       
            debugger;
            if (state === "SUCCESS") {   
                component.set("v.changeNewTemplate", true);
                component.set("v.templateName", '');
                debugger;             
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            component.set("v.selectPlantilla", true);
        });
        $A.enqueueAction(action); 
        
    },
    confirm : function(component,event){
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" :  $A.get("$Label.c.RQO_lbl_mensajeCorrectamente"), "confirmAlert" : false, 
                            "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), 
                            "idAlert" : $A.get("$Label.c.RQO_lbl_idInsertarPlantilla"), 
                            "parentLE" : false});
        try{ 
            appEvent.fire();
        }catch(err){
            this.showSpinner(component);
            console.log(err.message);
        }
    },
    getTemplates : function(component) {
        
        var idAccount = component.get("v.userHeredado.idCliente");
        var idContacto = component.get("v.userHeredado.idContacto");
        var actionTemplate = component.get('c.getTemplates');
        actionTemplate.setParams({  idContact : idContacto , idAccount : idAccount });
        debugger;
        actionTemplate.setCallback(this, function(response){
            
            var state = response.getState();     
            if (state === "SUCCESS") {
                var listBbdd = JSON.parse(response.getReturnValue()); 
                debugger;
                var listPlantillas = new Array();
                for (var i=0; i<listBbdd.length ; i++){
                    var objPl = {label:listBbdd[i].nombre, value:listBbdd[i].identificador};
                    listPlantillas.push(objPl);
                }           
                component.set("v.listPlantillas", listPlantillas);
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                        
        });
        $A.enqueueAction(actionTemplate);         
    },
    
    handleResponseAlert : function(component, event){ 
        debugger;
        var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");
        if ( $A.get("$Label.c.RQO_lbl_idInsertarPlantilla") == idAlert){
            this.addToTemplate(component, event);
            this.close(component, event);
        }
    },
    addToTemplate : function(component, event){
        debugger;
        var listadoPosicionesPedido = component.get("v.listPosicionPedido");
        
        if (listadoPosicionesPedido.length == 0){
            this.addToTemplateList(component);
        }
        else{
            debugger;
            //	Obtenemos los parámetros necesarios
            var idTemplate =  component.get("v.idTemplate");
            debugger;
            var action = component.get('c.addPositionTemplateList');
            action.setParams({ idTemplate : idTemplate, listWrapperPedido: JSON.stringify(listadoPosicionesPedido)  });        
            action.setCallback(this, function(response){
                
                var state = response.getState();         
                if (state === "SUCCESS") {
                    console.log('**********************')
                }
                else if (state === "INCOMPLETE") {
                    console.log('Incomplete');
                }
                    else if (state === "ERROR") {
                        var errors = response.getError()
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }                        
            });
            $A.enqueueAction(action);
        } 
    },
    
    addToTemplateList : function(component){
        debugger;
        //	Obtenemos los parámetros necesarios
        var idTemplate =  component.get("v.idTemplate");
        var modoEnvio = component.get("v.modoEnvioHeredado");
        var cantidad = component.get("v.intCantidadHeredado");
        var envase = component.get("v.envaseHeredado");
        var idGrado = component.get("v.idGradoHeredado");
        var idCliente = component.get("v.userHeredado.idCliente");
        var listIncoterm = component.get("v.userHeredado.listIncoterm");
        var incoterm = listIncoterm[0] != null ? listIncoterm[0] : '';
        //	Generamos manualmente el Json de plantilla con los parámetros obtenidos
        var positionTemplate = {cantidad: cantidad, grado: idGrado, incoterm: incoterm, unidadMedida : modoEnvio, envase : envase, destino : idCliente, facturacion: idCliente};
        
        var action = component.get('c.addPositionTemplate');
        action.setParams({ idTemplate : idTemplate, positionTemplateBean: JSON.stringify(positionTemplate) });        
        action.setCallback(this, function(response){
            
            var state = response.getState();         
            if (state === "SUCCESS") {
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                        
        });
        $A.enqueueAction(action);
    }
})