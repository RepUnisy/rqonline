({
	hacerBusqueda : function(component, event, helper) {
        var spinnerTop = component.find("spinnerPadre");        
        $A.util.toggleClass(spinnerTop, "slds-show");
        helper.hacerBusqueda(component, event);
	},doInit : function(component, event, helper){
        helper.getInfoHerencia(component, event, helper);
    }
})