({
	getInfo : function(component, event, helper) {
		helper.getInfo(component, event);
	},
    sumarUnoANumProduct : function(component, event, helper){
        var numProduct = component.get("v.numProduct");
        var numSumar = event.getParam("numSumar");
        numProduct = numProduct + numSumar;
        component.set("v.numProduct", numProduct);
    },
    irASp : function(component, event, helper){
        var url = '/sp';
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();
    }
})