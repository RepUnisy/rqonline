({
	myAction : function(component, event, helper) {
		
	},
    toggleSubItem: function(component, event, helper) {
        /*FRONTEND*/
        var btnSubItem = event.currentTarget;
        jQuery(btnSubItem).addClass('thisItem').toggleClass('fa-chevron-down fa-chevron-up');
        jQuery('.thisItem.fa-chevron-up').closest('tr').find('.movil-hide').slideDown().addClass('rq-db');
        jQuery('.thisItem.fa-chevron-down').closest('tr').find('.movil-hide').slideUp(0).removeClass('rq-db');
        jQuery(btnSubItem).removeClass('thisItem');
        
    },
})