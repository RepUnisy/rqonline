({
	doInit : function(component, event, helper) {
    	// Recuperar opciones de perioricidad
		helper.getNotificationPeriodicity(component);
		// Recuperar consentimientos de notificaciones
		helper.getNotificationConsents(component);
		
		var user = component.get("v.user");
        
		// Establecer permisos
		if (user.permissionUrls.includes("/sp"))
			component.set("v.hasAccessSolicitudPedido", true);
		if (user.permissionUrls.includes("/fact"))
			component.set("v.hasAccessFacturas", true);
    },
    save: function(component, event, helper) {
    	// Actualizar consentimientos de notificaciones
		helper.updateNotificationConsents(component);
    },
})