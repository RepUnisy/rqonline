({
	getNotificationPeriodicity : function(component) {
    	// Obtener listado de periodicidades
    	var action = component.get("c.getNotificationPeriodicities");

        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
				var options = [];
				var responseValue = response.getReturnValue();
				for (var key in responseValue) {
				    if (responseValue.hasOwnProperty(key)) {
				        options.push({value: key, label: responseValue[key]});
				    }
				};
                component.set("v.emailSendingPeriodicity", options);
            }
            else {
            	component.set('v.visible',false);
            	component.set('v.textoAlerta',response.getError()[0].message);
            	component.set('v.showAlert',true);
            }
        });
        
        $A.enqueueAction(action);
    },
    getNotificationConsents : function(component) {
    	// Obtener listado de consentimientos
    	var action = component.get("c.getNotificationConsents");
        action.setParams({
            idContact : component.get("v.user.idContacto")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var consents = response.getReturnValue();
                component.set("v.notificationConsents", consents);
            }
            else {
            	component.set('v.visible',false);
            	component.set('v.textoAlerta',response.getError()[0].message);
            	component.set('v.showAlert',true);
            }
        });
        
        $A.enqueueAction(action);
    },
    updateNotificationConsents : function(component) {

    	var action = component.get("c.updateNotificationConsents");
    	var consents = component.get("v.notificationConsents");
        action.setParams({
            jsonConsents : JSON.stringify(consents)
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	var texto = component.get("v.textoAlertaDefault");
            	component.set('v.textoAlerta',texto);
            	component.set('v.showAlert',true);
            }
            else {
            	component.set('v.textoAlerta',response.getError()[0].message);
            	component.set('v.showAlert',true);
            }
        });
        
        $A.enqueueAction(action);
    },
})