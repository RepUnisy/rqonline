({
    getInfoHerencia : function(component, event) {
       
        //	Obtenemos la información del usuario
        var action = component.get('c.getUserInfo');
        var relativeUrl = window.location.pathname.substring(window.location.pathname.lastIndexOf('/'));
        if (relativeUrl == '/catalogo')
        	relativeUrl='';
        
        action.setParams({
	        	relativeUrl: relativeUrl
	        });

        //  action.setStorable();
        action.setBackground();
        
        action.setCallback(this, function(response) {                         
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                
                //	Obtenemos el usuario y el idioma
                var user = JSON.parse(respuesta);
               
                
                var language =  $A.get("$Locale.langLocale") == 'es' ? 'es_ES' :  $A.get("$Locale.langLocale");     
                console.log('getInfoHerencia ' + language + ' ' + user)
                                 //	Almacenamos el usuario y el idioma                              
                component.set("v.language", language);
                component.set("v.user", user);
               
                                
                // Establecer permisos
                if (user.permissionUrls.includes("/catalogo"))
                    component.set("v.hasAccessCatalogoPrivado", true);
                if (user.permissionUrls.includes("/catalogo"))
                    component.set("v.hasAccessMiCatalogo", true);
                if (user.permissionUrls.includes("/sp"))
                    component.set("v.hasAccessSolicitudPedido", true);                
                if (user.permissionUrls.includes("/sp"))
                    component.set("v.hasAccessMisPlantillas", true);                
                if (user.permissionUrls.includes("/hp"))
                    component.set("v.hasAccessHistoricoPedidos", true);
                if (user.permissionUrls.includes("/hp"))
                    component.set("v.hasAccessSeguimientoPedidos", true);
                if (user.permissionUrls.includes("/fact"))
                    component.set("v.hasAccessFacturas", true);
                if (user.permissionUrls.includes("/fact"))
                    component.set("v.hasAccessMisConsumos", true);
                if (user.permissionUrls.includes("/perfil"))
                    component.set("v.hasAccessPerfil", true);     
                

            }
            else {
	            var urlEvent = $A.get("e.force:navigateToURL");
		        urlEvent.setParams({
		            "url": "/",
		            "isredirect" : true
		        });
		        urlEvent.fire();
            }
        });  
        $A.enqueueAction(action);        
    }
})