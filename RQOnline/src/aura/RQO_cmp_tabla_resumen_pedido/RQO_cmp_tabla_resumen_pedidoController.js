({
    createTable : function(component, event, helper) {
        var mapaObt = component.get("v.mapaResumenHeredada");
        var keyObt = component.get("v.idResumenHeredada");
        //	Obtenemos y asignamos la tabla
        var lista = mapaObt[keyObt];
        component.set("v.listResumen", lista);
        
        var incoterm = '';
        var destinoMercancias = '';
        var cliente = '';
        var cantidadTotal = 0;
        for (var i = 0; i< lista.length; i++){            
            if (incoterm == ''){
                incoterm = lista[i].incoterm;
                destinoMercancias = lista[i].dirEnvio;
            }
            cantidadTotal = cantidadTotal + lista[i].cantidad;
        }
        
        var usuario = component.get("v.userHeredado");
        if (usuario.idCliente == destinoMercancias){
            cliente = usuario.cliente.Name ;            
        }
        else{
            var listaDestinoMercancias = usuario.listDirMercancias;
            for(var i = 0; i< listaDestinoMercancias.length; i++ ){
                if (listaDestinoMercancias[i].RQO_fld_cliente__c == destinoMercancias){
                    cliente = listaDestinoMercancias[i].RQO_fld_cliente__r.Name;
                }
            }
            debugger;            
        }
        
        debugger;
        component.set("v.incoterm", incoterm);
        component.set("v.cantidadTotal", cantidadTotal/1000);
        
        component.set("v.nombreDestinoMercancias", cliente);
        
        debugger;
        
    },
    toggleTabla : function(component, event, helper) {
        var element = event.currentTarget;
        jQuery(element).addClass('this');
        
        //Si ya estaba desplegado
        if(jQuery('.this').closest('tr').children('td:not(:nth-child(-n+4))').hasClass('rq-table-block'))
        {
            
            jQuery('.this').closest('tr').children('td.rq-table-block').slideUp(0, function() {
                jQuery('.this').closest('tr').children('td:not(:nth-child(-n+4))').removeClass('rq-table-block');
            });
        }
        else{
            //si esta oculto
            jQuery('.this').closest('tr').children('td:not(:nth-child(-n+4))').addClass('rq-table-block');
            jQuery('td:last-child').removeClass('rq-table-block');
            jQuery('.rq-table-block').slideDown();
        }
        jQuery(element).removeClass('this');
    }
})