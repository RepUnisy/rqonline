({
	doInit : function(component, event) {
        var id = component.get("v.idGrado");
        var mapaTraducciones = component.get("v.mapaTraducciones");

        if (mapaTraducciones != null ){
        	component.set("v.textoTraduccion", mapaTraducciones[id].RQO_fld_detailTranslation__c);    
        }
	}
})