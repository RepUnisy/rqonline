({
    descargar : function(component, event) {
        debugger;
        component.set("v.mostrarSpinner", true);
        var idArchivado = component.get("v.idArchivadoHeredado");
        var cliente = component.get("v.idClienteHeredado");
        var fechaFactura = component.get("v.fechaFacturaHeredado");
        var numFactura = component.get("v.numeroFacturaHeredado");
        
        var action = component.get('c.getFileFactura');
        action.setParams({ "cliente" : cliente, "fechaFactura" : fechaFactura, "numFactura" : numFactura, "idArchivado" : idArchivado});        
        
        action.setCallback(this, function(response){
            var nok = false;
            var noData = false;
            var customMessagge = '';
            var state = response.getState();
            console.log('ACL. Get factura' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var storedResponse = response.getReturnValue();
                if (storedResponse && storedResponse.length > 2 && (storedResponse[0] == 'OK' || storedResponse[0] == '200') && storedResponse[2]){
                    try {
                        var byteString = atob(storedResponse[2]);
                        var ab = new ArrayBuffer(byteString.length);
                        var ia = new Uint8Array(ab);
                        for (var i = 0; i < byteString.length; i++) {
                            ia[i] = byteString.charCodeAt(i);
                        }                        
                        var docName = ((storedResponse.length > 3 && storedResponse[3]) ?  storedResponse[3] : 'DOC' + new Date());
                        var docExtension = '';
                        var docType = ((storedResponse.length > 4 && storedResponse[4]) ?  storedResponse[4] : '');
                        var contentType = '';
                        if(docType && docType=='pdf'){
                            contentType = 'application/pdf';
                            docExtension = '.pdf';
                        }else if(docType && (docType == 'msw' || docType == 'msw8' || docType == 'docx')){
                            contentType = 'application/msword';
                        }else if(docType && docType=='txt'){
                            contentType = 'text/plain';
                            docExtension = '.txt';
                        }else{
                            contentType = 'application/octet-stream';
                            //docExtension = docType;
                        }						
                        var blob = new Blob([ia], { type: contentType});                        
                        saveAs(window, blob, docName + docExtension);
                        this.registrarEvento(component);
                    }catch(err) {
                        nok = true;
                        console.log('Error: ' + err.message);
                    }
                }
                else if (storedResponse && storedResponse.length > 0){
                    if (storedResponse[0] == 'ND'){
                        noData = true;
                    }else if (storedResponse[0] == 'E' && storedResponse.length > 1 && storedResponse[2] == '001'){
                        customMessagge = $A.get("$Label.c.RQO_lbl_errorFacturaSapRellenar");
                    }else if (storedResponse[0] == 'E' && storedResponse.length > 1 && storedResponse[2] == '002'){
                        customMessagge = $A.get("$Label.c.RQO_lbl_errorFacturaSapCliente");
                    }else if (storedResponse[0] == 'E' && storedResponse.length > 1 && storedResponse[2] == '003'){
                        customMessagge = $A.get("$Label.c.RQO_lbl_errorFacturaSapElectronica");
                    }else if (storedResponse[0] == 'E' && storedResponse.length > 1 && storedResponse[2] == '004'){
                        customMessagge = $A.get("$Label.c.RQO_lbl_errorFacturaSapProblemas");
                    }else if (storedResponse[0] == 'E' && storedResponse.length > 1 && storedResponse[2] == '005'){
                        customMessagge = $A.get("$Label.c.RQO_lbl_errorFacturaSapTransitorio");
                    }else{
                        nok = true;
                    }
                }else{
                    nok = true;
                }
            }
            else if (state === "INCOMPLETE") {
                nok = true;
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    nok = true;
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }     
            if (nok || noData || customMessagge != ''){
                var textAlert = '';
                var titleAlertValue = '';
                if (noData){
                    textAlert = $A.get("$Label.c.RQO_lbl_sinDocumentos");
                    titleAlertValue = $A.get("$Label.c.RQO_lbl_informacion");
                }else if(customMessagge != ''){
                    textAlert = customMessagge;
                    titleAlertValue = $A.get("$Label.c.RQO_lbl_informacion");
				}else{
                    textAlert = $A.get("$Label.c.RQO_lbl_errorDescargaDocumento");
                    titleAlertValue = $A.get("$Label.c.RQO_lbl_error");
                }
                this.fireAlertEvent(textAlert, titleAlertValue);
            }
            component.set("v.mostrarSpinner", false);
        });
        $A.enqueueAction(action);  
        
    },
    fireAlertEvent : function(textAlert, titleAlertValue){
        
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : textAlert, "confirmAlert" : false, 
                            "titleAlertValue" : titleAlertValue, "idAlert" : 'downInvoice', "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }, registrarEvento: function(component){
        /*
         * Registro de los eventos de descarga de factura
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         * 	Valor 19 = Download invoice
         * actionType - Grupo al que pertenece, 5 = Invoices
         * actionDescription - descripción de la acción que lanza el evento. En este caso descarga de facturas. Valores para este componente:
         * 	Valor 30 = Download invoice
         *  reNumFactura - Número de la factura descargada
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var invoiceNumber = component.get("v.numeroFacturaHeredado");
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_descargaFactura', "reComponentDescription" : '19', 
                            "actionType" : '2', "actionDescription" : '30', "reNumFactura" : invoiceNumber,
                            "rePathActual" : window.location.pathname});
        
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})