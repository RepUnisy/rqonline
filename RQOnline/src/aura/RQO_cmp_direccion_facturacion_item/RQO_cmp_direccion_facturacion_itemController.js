({
    init : function(component, event, helper) {
        var mapOpciones = component.get("v.mapOpcionesHeredado");
        var listOpciones = [];
        for (var [key, value] of mapOpciones) {                
            listOpciones.push(value);
        }
        component.set("v.listOpciones", listOpciones);
        
        //	Obtenemos la clave y el mapa de los elementos que conciernen el componente
        var key = component.get("v.keyHeredada");
        var mapMercancias = component.get("v.mapDestinosMercanciaHeredada");
        
        //	Obtenemos todos los elementos agrupados
        var listMercancias = mapMercancias.get(key);
        
        //	Obtenemos los valores genéricos
        if (listMercancias.length != 0){
            var itemMercancia = listMercancias[0];
            component.set("v.nombreDestinoMercancias", itemMercancia.destinoMercanciasNombre);
            component.set("v.incoterm", itemMercancia.incoterm);
            debugger;
            component.set("v.posicion", Array.from( mapOpciones.keys()).indexOf(itemMercancia.direccionFacturacion));
            //var listKeysOpciones = mapOpciones.keys().indexOf;
        }
    },
    
    changeSelect : function (component, event) {
        //	Obtenemos la información del servicio
        var idComponente = event.getParam("idComponente");
        var valorSeleccionado = event.getParam("valorSeleccionado");           
        debugger;
        
        var mapElementos = component.get("v.mapElementosAlmacenarHeredado");
        var keyHeredada = component.get("v.keyHeredada");
        debugger;
        if (mapElementos == null){
            debugger;
            var nuevoMap = new Map();
            nuevoMap.set(keyHeredada, valorSeleccionado);
            component.set("v.mapElementosAlmacenarHeredado", nuevoMap);
        }
        else{
            debugger;
            if (mapElementos.has(keyHeredada)){
                debugger;
                var posicionInicial = component.get("v.posicionHeredada");
                debugger;
                if (posicionInicial == valorSeleccionado){
                    mapElementos.delete(keyHeredada);
                    component.set("v.mapElementosAlmacenarHeredado", mapElementos);                    
                }
            }
            else{
                mapElementos.set(keyHeredada, valorSeleccionado);
                component.set("v.mapElementosAlmacenarHeredado", mapElementos);
            }
        }
        
        debugger;        
    }
})