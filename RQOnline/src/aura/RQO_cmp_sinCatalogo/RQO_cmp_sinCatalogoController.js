({
    getCatPropuesto : function(component, event, helper) {         
        var spinner = component.find("spinner");        
        $A.util.toggleClass(spinner, "slds-show");        
       
        helper.getCatPropuesto(component, event);
    }
})