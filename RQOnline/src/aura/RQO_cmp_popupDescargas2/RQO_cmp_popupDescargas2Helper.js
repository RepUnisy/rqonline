({
	close : function(component, event) {
        component.set("v.visible", false);
        component.set("v.vacio", false);
        component.set("v.error", false);
    }, handleGetDocData : function(component, event){
        component.set("v.visible", true);
        var numDocumento = event.getParam("numDocumento");
        var showAlbaranCMR = event.getParam("showAlbaranCMR");
        var showCertificadoAnalisis = event.getParam("showCertificadoAnalisis");
        var pedidoId = event.getParam("pedidoId");
        var listData = [];
        if (showAlbaranCMR || showCertificadoAnalisis){
			var listData = [];
            if (showAlbaranCMR){
                listData.push({"label" : $A.get("$Label.c.RQO_lbl_cmrAlbaran"), "value" : "DN" + numDocumento});
            }
            if (showCertificadoAnalisis){
                listData.push({"label" : $A.get("$Label.c.RQO_lbl_certificadoDeAnalisis"), "value" : "AC" + numDocumento});
            }
            component.set("v.picklistData", listData);
        }else{
            component.set("v.vacio", true);
        }
        component.set("v.picklistData", listData);
        component.set("v.pedidoId", pedidoId);
		
    }, getDocData : function(component, event){
        this.showSpinner(component, 'slds-show', 'slds-hide');
        var user = component.get("v.user");
        var idAccount = user.idCliente;
        var selectedDoc = component.find("selectionDocument").get("v.value");
        var type = selectedDoc.substring(0, 2);
        var numDocumento = selectedDoc.substring(2, selectedDoc.length);

        var action = component.get('c.getDocumentData');
        action.setParams({"idCliente" : idAccount, "numDocumento" : numDocumento, "tipo" : type});
        action.setCallback(this, function(response) {
            this.showSpinner(component, 'slds-hide','slds-show');
            var state = response.getState();
            if (state === "SUCCESS") {
                var storedResponse = response.getReturnValue();
                if (storedResponse && storedResponse.length > 1 && storedResponse[0] != 'E' && storedResponse[0] != 'NOK' && storedResponse[2]){
                    try {
                        var byteString = atob(storedResponse[2]);
                        var ab = new ArrayBuffer(byteString.length);
                        var ia = new Uint8Array(ab);
                        for (var i = 0; i < byteString.length; i++) {
                            ia[i] = byteString.charCodeAt(i);
                        }	
                        var contentType = 'application/pdf';
                        var docName = 'document';
                        if (storedResponse.length > 2 && storedResponse[3]){
                            docName = storedResponse[3];
                        }
                        var blob = new Blob([ia], { type: contentType});                        
                        saveAs(window, blob,  docName + '.pdf');
                        //Registro evento
						var selectedDoc = component.find("selectionDocument").get("v.value");
                        var pedidoId = component.get("v.pedidoId");
                        var type = selectedDoc.substring(0, 2);
                        var numDocumento = selectedDoc.substring(2, selectedDoc.length);
                        
                        this.registrarEvento(component, type, pedidoId, numDocumento);
                    }catch(err) {
                        console.log('Error: ' + err.message);
                        component.set("v.error", true);
                    }
                }else{
                    component.set("v.error", true);
                }
            }else{
                component.set("v.error", true);    
            }
        });
        $A.enqueueAction(action);
    }, getInfo : function(component, event) {
        var language = event.getParam("language");
		var user = event.getParam("user");
        component.set("v.language", language);
        component.set("v.user", user);
	},showSpinner : function(component, classToAdd, classToremove){
        $A.util.removeClass(component.find('spinner'), classToremove);
        $A.util.addClass(component.find('spinner'), classToAdd);
    }, registrarEvento: function(component, type, pedidoId, numDocumento){
        /*
         * Registro del evento de descarga de albarán o CMR
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         * 	Valor 11 = Download delivery note (Descarga Albarán)
         *  Valor 12 = Download CMR
         * actionType - Grupo al que pertenece, 2 = Order
         * actionDescription - descripción de la acción que lanza el evento. En este caso descarga de albarán o CMR. Lo calculamos en función del type que nos llega
         * por parámetro. Valores para este componente:
         * 	Valor 12 = Download delivery note
         *  Valor 15 = Download CMR
         * reSearchData - numDocumento - número de albarán / CMR que se descarga (Se utiliza este campo como auxiliar)
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var componentDescription = '';
        var actionDescription = '';
        //Recuperamos la descripción del componente a partir del type
        if (type === 'DN'){//Albarán
            componentDescription = '11';
            actionDescription = '12';
        }else if (type === 'AC'){
			componentDescription = '12';
            actionDescription = '15';
        }
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_popupDescargas2', "reComponentDescription" : componentDescription, 
                            "actionType" : '2', "actionDescription" : actionDescription, "rePedidoId" : pedidoId, "reSearchData" : numDocumento,
                            "rePathActual" : window.location.pathname});
        
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})