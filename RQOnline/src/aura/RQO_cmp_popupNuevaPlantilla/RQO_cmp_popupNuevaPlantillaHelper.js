({
    close : function(component, event){
        component.set("v.templateName", null);
        component.set("v.visible", false);
    },
    
    accept : function(component, event){

        var templateName = component.get("v.templateName");
        if (templateName == null || templateName == ''){
            var elemento = component.find("templateNameText");
            elemento.set("v.errors", [{message:"Introduce el nombre de la plantilla" }]);
        }
        else{
            var idContacto = component.get("v.user.idContacto");
            var idAccount = component.get("v.user.idCliente");
            var action = component.get('c.createTemplate');
            action.setParams({ templateName : templateName, idContact : idContacto, idAccount : idAccount});
            action.setCallback(this, function(response){
                var state = response.getState();          
                if (state === "SUCCESS") {                    
                    component.set("v.visible", false);
                    this.registrarEvento(component);
                    $A.get('e.force:refreshView').fire();   
                }
                else if (state === "INCOMPLETE") {
                    console.log('Incomplete');
                }
                    else if (state === "ERROR") {
                        var errors = response.getError()
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                
                //$A.util.toggleClass(spinner, "slds-show");        
            });
            $A.enqueueAction(action); 
        }
        
    }, 
    fireResponseAlertEvent : function(component, confirmAlertResponse){
        component.set("v.visible", false);
        var identificativo = component.get("v.idAlert");     
        var appEvent = $A.get("e.c:RQO_evt_ResponseAlert");
        appEvent.setParams({"responseAlert" : confirmAlertResponse, "idAlert" : identificativo});
        try{
            appEvent.fire();
            console.log('Ha lanzado el evento: ' + appEvent);
        }catch(err){
            console.log(err.message);
        }
    }, registrarEvento : function(component){
        /*
         * Registro del evento de generación de nueva plantilla
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         *  Valor 16 = New template
         * actionType - Grupo al que pertenece, 2 = Order
         * actionDescription - descripción de la acción que lanza el evento. En este caso creación de nueva plantilla. 
         * Valores para este componente:
         *  Valor 13 = Creation of new template
         * reSearchData - Nombre de la template
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
		var templateName = component.get("v.templateName");
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_popupNuevaPlantilla', "reComponentDescription" : '16', "reSearchData" : templateName,
                            "actionType" : '2', "actionDescription" : '13', "rePathActual" : window.location.pathname});
        
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})