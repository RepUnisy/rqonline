({
    close : function(component, event, helper) {
        helper.close(component, event);
	},
    accept : function(component, event, helper) {
        helper.accept(component, event);
    }
})