({
    handleComponentEvent : function(component, event) {
        var data = event.getParam("dataLocaleDoc");
        component.set("v.docIdData", data.idDoc);
        component.set("v.docRecoveryTypeData", data.docRecoveryType);
        component.set("v.docRepository", data.docRepository);
        component.set("v.docDescriptionType", data.docDescriptionType);
        component.set("v.sfDocId", data.sfDocId);
    }
})