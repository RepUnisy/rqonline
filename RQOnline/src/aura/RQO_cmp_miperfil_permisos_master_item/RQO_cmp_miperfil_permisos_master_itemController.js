({
	onRender : function(component, event, helper) {
	
		var perfilContacto = component.get("v.user.Contact.RQO_fld_perfilUsuario__c");
		var perfiles = component.get("v.perfiles");
		
		var numPerfiles = 0;
		if (perfiles)
			numPerfiles = perfiles.length;
		var perfilSeleccionado = 0;
		var i = 0;
		for (i = 0; i < numPerfiles; i++) {
		    if (perfiles[i] == perfilContacto)
		    	perfilSeleccionado = i;
		}
		
		component.set("v.perfilSeleccionado", perfilSeleccionado);
		
	},
	showInfo : function(component, event, helper) {
    	var help = component.find("divInfo");        
        $A.util.toggleClass(help, "slds-show");
    },
    profileChange : function(component, event, helper) {
    	var valorSeleccionado = event.getParam("valorSeleccionado");
    	var perfiles = component.get("v.perfiles");
    	console.log(perfiles[valorSeleccionado]);
    	component.set("v.user.Contact.RQO_fld_perfilUsuario__c", perfiles[valorSeleccionado]);
    },
    solicitarBaja : function(component, event, helper) {
        component.set('v.showAlertBaja',true);
    },
    realizarBaja : function(component, event, helper) {
    	var responseAlert = event.getParam("responseAlert");
    	var idAlert = event.getParam("idAlert");
    	if (idAlert == component.get("v.user.Id") && responseAlert)
    	{
	    	//	Realizar baja del usuario
	    	var action = component.get("c.unregisterUser");
	        action.setParams({
	            userId : component.get("v.user.Id")
	        });
	        action.setCallback(this, function(response){
	            var state = response.getState();
	            if (state === "SUCCESS") {
	            	component.set('v.showAlertBaja',false);
	            }
	            else {
	            	// Mostrar mensaje
	            }
	        });
        
	        $A.enqueueAction(action);
    	}
    },
})