({
    doInit : function (component, event, helper){
        if(!$A.util.isEmpty(component.get("v.idGrado")) && !$A.util.isEmpty(component.get("v.language"))){
       		helper.doInit(component, event);
        }
    },
    toggleSegmentos : function (component, event, helper){
       if(window.matchMedia("(max-width:767px)").matches)
       {
           var element = component.find('titleSeg');
           $A.util.toggleClass(element,'plegado');
           jQuery('.rq-segmentos-toggle').slideToggle();
       }
    },
    toggleRecomendacion : function (component, event, helper){
       if(window.matchMedia("(max-width:767px)").matches)
       {
           var element = component.find('titleRecom');
           $A.util.toggleClass(element,'plegado');
           jQuery('.rq-recomendacion-toggle').slideToggle();
       }
    },
    
})