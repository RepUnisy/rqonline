({
    getInfo : function(component, event) {
        
        var idGrado = component.get("v.id");
        var idContacto = component.get("v.user.idContacto");
        var action = component.get('c.isMiCatalogoGrado');
        action.setParams({ "grado" : idGrado, "contacto": idContacto });
        action.setCallback(this, function(response){            
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var isMiCatalogo = response.getReturnValue();
                component.set("v.isMiCatalogo", isMiCatalogo);
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }            
        });
        $A.enqueueAction(action);
    },
    insertMiCatalogoFireEvent : function(component, event){
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_quiereInsertar"), "confirmAlert" : true, 
                            "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), "idAlert" : $A.get("$Label.c.RQO_lbl_insertGradeAlert_const"), "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            this.showSpinner(component);
            console.log(err.message);
        }
    },
    deleteMiCatalogoFireEvent : function(component, event){
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_quiereBorrar"), "confirmAlert" : true, 
                            "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), "idAlert" : $A.get("$Label.c.RQO_lbl_deleteGradeAlert_const"), "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            this.showSpinner(component);
            console.log(err.message);
        }
    },
    insertMiCatalogo : function(component, event) {
        var idGrado = component.get("v.id");
        var idContacto = component.get("v.user.idContacto");
        var action = component.get('c.insertMiCatalogoGrado');
        action.setParams({ "grado" : idGrado, "contacto": idContacto });
        action.setCallback(this, function(response){            
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var spinner = component.find("mySpinner");
        		$A.util.removeClass(spinner, 'slds-show');
        		$A.util.addClass(spinner, 'slds-hide');
                var insertado = response.getReturnValue();
                if (insertado == true){
                    component.set("v.isMiCatalogo", true);
                }
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }            
        });
        $A.enqueueAction(action);
    },
    deleteMiCatalogo : function(component, event) {
        var idGrado = component.get("v.id");
        var idContacto = component.get("v.user.idContacto");
        var action = component.get('c.deleteMiCatalogoGrado');
        action.setParams({ "grado" : idGrado, "contacto": idContacto });
        action.setCallback(this, function(response){
			var spinner = component.find("mySpinner");
        	$A.util.removeClass(spinner, 'slds-show');
        	$A.util.addClass(spinner, 'slds-hide');            
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var insertado = response.getReturnValue();
                if (insertado == true){
                    component.set("v.isMiCatalogo", false);
                }
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }            
        });
        $A.enqueueAction(action);
    }
})