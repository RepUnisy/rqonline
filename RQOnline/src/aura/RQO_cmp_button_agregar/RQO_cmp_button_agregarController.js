({
    getInfo : function(component, event, helper) {         
        helper.getInfo(component, event);
    },
    insertMiCatalogoFireEvent : function(component, event, helper) {         
        helper.insertMiCatalogoFireEvent(component, event);
    },
    deleteMiCatalogoFireEvent : function(component, event, helper) {         
        helper.deleteMiCatalogoFireEvent(component, event);
    },
    toggleAgregar : function(component, event, helper) { 
        if(window.matchMedia("(max-width:767px)").matches)
        {        
                jQuery('.rq-title-agregar').toggleClass('plegado').siblings('.rq-pedido').slideToggle();
        }
    },
    handleResponseAlert : function(component, event, helper){
        var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");
        if(responseAlert && responseAlert == true && idAlert && idAlert == 'deleteGradeAlert'){
            helper.deleteMiCatalogo(component, event);
        }
        else{
            if(responseAlert && responseAlert == true && idAlert && idAlert == 'insertGradeAlert'){
                helper.insertMiCatalogo(component, event);
            }
            else{
                this.showSpinner(component, 'slds-hide', 'slds-show');
            }
        }
    }, 
    showSpinner : function(component, classToAdd, classToremove){
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, classToremove);
        $A.util.addClass(spinner, classToAdd);
	},
    irAMiCatalogo : function(component, event, helper){
        var url = '/mi-catalogo';
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();
    }
})