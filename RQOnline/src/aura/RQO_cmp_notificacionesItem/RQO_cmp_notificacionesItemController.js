({
	goTo : function(component, event, helper) {
        debugger;
		var codeCategoria = component.get("v.category");
		var id = component.get("v.identificadorRedirect");
        var fecha = component.get("v.dateRedirect");
        var url= ''; 
        switch(codeCategoria) {
            case $A.get("$Label.c.RQO_lbl_notifCodeProducto"):
                 url = "/catalogo";
                if (id){
                    url='/grado?id=' + id;
                }
                break;
            case $A.get("$Label.c.RQO_lbl_notifCodeEntrega"):
                 url = "/hp";
                 if (id){
                     url+='?pedido=' + id;
                     if (fecha){url += '&fecha=' + fecha;}
				 }
                break;
            case $A.get("$Label.c.RQO_lbl_notifCodeFactura"):
                url = "/fact";
                if (id){
                    url+='?numfact=' + id;
                }
                break;
            /*case $A.get("$Label.c.RQO_lbl_notifCodeCliente"):
               
                break;*/
            case $A.get("$Label.c.RQO_lbl_notifCodeSolicitud"):
                 url = "/hp";
                 if (id){
                     url+='?solicitud=' + id;
                     if (fecha){url += '&fecha=' + fecha;}
                 }
                break;
            case $A.get("$Label.c.RQO_lbl_notifCodePedido"):
                url = "/hp";
                if (id){
                    url+='?pedido=' + id;
					if (fecha){url += '&fecha=' + fecha;}
				}
                break;
            case $A.get("$Label.c.RQO_lbl_notifCodeDocumentacion"):
                 url = "/catalogo";
                if (id){
                    url='/grado?id=' + id;
                }
                break;
            /*case $A.get("$Label.c.RQO_lbl_notifCodeOtros"):
                url = "/fact";
                break;*/
            case $A.get("$Label.c.RQO_lbl_notifCodeComunicacion"):
                url = "/visor-comunicaciones-multimedia?id=" + id;
                break;
            default:                
        }
        
        if (url != ''){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url,
                "isredirect" : true
            });
            urlEvent.fire();     
        }
    }
})