({
	doInit : function(component, event, helper) {
        helper.doInit(component, event);
	},
    lanzaFichaGrado : function(component, event, helper) {
        helper.lanzaFichaGrado(component, event);
    },
    toggleTecnica : function(component, event, helper) {
        helper.toggleTecnica(component, event);
    },
    toggleInfo : function(component, event, helper) {
        helper.toggleInfo(component, event);
    },
	toggleTable : function(component, event, helper) {
        helper.toggleTable(component, event);
    },
    onRender : function(component, event, helper) {
		helper.onRender(component, event);
    }
})