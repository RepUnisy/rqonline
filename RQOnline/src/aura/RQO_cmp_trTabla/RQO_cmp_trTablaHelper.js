({
	doInit : function(component, event) {
        var clave = component.get("v.valorMapa");
        var map = component.get("v.mapaTabla");
        var mapaInfoGrados = component.get("v.mapaInfoGrados");
        var id = mapaInfoGrados[clave].Id;
        
        component.set("v.idGrado", id);
        var mapaTraducciones = component.get("v.mapaTraducciones");
        if(mapaTraducciones[id] != null){
            component.set("v.nombreGrado", mapaTraducciones[id].RQO_fld_translation__c);
        }
        else{
            component.set("v.nombreGrado", mapaInfoGrados[clave].Name);
        }
        var nombre = mapaInfoGrados[clave].Name;
        component.set("v.valorLista", map[clave]);
        component.set("v.esNuevo", mapaInfoGrados[clave].RQO_fld_new__c);
        component.set("v.esRecomendado", mapaInfoGrados[clave].RQO_fld_recommendation__c);
        if(mapaInfoGrados[clave].RQO_fld_sku__r != null){
            if(mapaInfoGrados[clave].RQO_fld_sku__r.RQO_fld_numContent__c != 0){
                component.set("v.rollUpSummaryNoEsCero", true);
            }
            else{
                component.set("v.rollUpSummaryNoEsCero", false);
            }
        }
        else{
            component.set("v.rollUpSummaryNoEsCero", false);
        }
        var listaKeys = [];
        var listaEspecificaciones = component.get("v.listaEspecificaciones");
        if(listaEspecificaciones.length > 0){
            for(var i = 0; i < listaEspecificaciones.length; i++){
                listaKeys.push(listaEspecificaciones[i].Id);
            }
            component.set("v.listaKeys", listaKeys);
        }
        debugger;
        debugger;
	},
    lanzaFichaGrado : function(component, event) {
        debugger;
        var id = component.get("v.idGrado");
        var url = "/grado?id=" + id;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
           "url": url,
           "isredirect" : true
        });
        urlEvent.fire();
    },
    toggleTecnica : function(component, event) {
        var idGrado = component.get("v.idGrado");
        var language = component.get("v.language");
        var user = component.get("v.user");
        var appEvent = $A.get("e.c:RQO_evt_openDocTable");
        appEvent.setParams({"idGradoEvt" : idGrado, "idioma" : language, "user" : user});
        try{
        	appEvent.fire();
            console.log('Ha lanzado el evento: ' + appEvent);
        }catch(err){
            console.log(err.message);
        }
    },
    toggleInfo : function(component, event) { 
    debugger;
		component.set("v.mostrarDetalleProducto", true);
        var element = event.currentTarget;
        jQuery(element).addClass('elemento-activo');
		if(jQuery('.elemento-activo').siblings('.rq-tooltip-pedido').hasClass('visible'))
		{
			jQuery('.elemento-activo').siblings('.rq-tooltip-pedido').removeClass('visible').css('display','none');
		}
		else if(jQuery('.rq-tooltip-pedido').hasClass('visible'))
		{
			jQuery('.rq-tooltip-pedido').removeClass('visible').css('display','none');
			jQuery('.elemento-activo').siblings(".rq-tooltip").slideToggle().addClass('visible');
		}
		else{
			jQuery('.elemento-activo').siblings(".rq-tooltip").slideToggle().addClass('visible');
		}
		jQuery(element).removeClass('elemento-activo');
		
    },
	toggleTable : function(component, event) {
 	 if(window.matchMedia("(max-width:766px)").matches)
         {
            var element = event.currentTarget;
            jQuery(element).addClass('this');
             
            //Si ya estaba desplegado
            if(jQuery('.this').closest('tr').children('td:not(:nth-child(-n+2))').hasClass('rq-table-block'))
            {
                
                jQuery('.this').closest('tr').children('td.rq-table-block').slideUp(0, function() {
                    jQuery('.this').closest('tr').children('td:not(:nth-child(-n+2))').removeClass('rq-table-block');
               		jQuery('.this').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                    
                });
            }
            else{
            //si esta oculto
                jQuery('.this').closest('tr').children('td:not(:nth-child(-n+2))').addClass('rq-table-block');
                jQuery('.this').addClass('fa-chevron-up').removeClass('fa-chevron-down');
                jQuery('td:last-child').removeClass('rq-table-block');
                jQuery('.rq-table-block').slideDown('rq-table-block');
            }
            jQuery(element).removeClass('this');
        }
    },
    onRender : function(component, event) {
        
       /* FRONT-END */
        //jQuery('.rq-grado-recomendado').closest('tr').addClass('rq-fila-recomendada');
    },
})