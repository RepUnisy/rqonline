({
	autocompletar : function(component, event) {
        console.log('entramos');
        var escrito = component.find('idTxtWikiText').get('v.value');
        var user = component.get("v.user");
        var obtenerGrados = component.get("c.getAllGrades");
        if(user.isPrivate){
            obtenerGrados.setParams({busqueda : escrito,
                                     privado : user.isPrivate,
                                     relatedAccount : user.cliente.Id});  
        }
        else{
            obtenerGrados.setParams({busqueda : escrito,
                                     privado : user.isPrivate,
                                    relatedAccount : null});  
        }
        obtenerGrados.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var todosGrados = JSON.parse(response.getReturnValue());
                component.set("v.listaGradosCompleta", todosGrados);
                if(todosGrados.length == 0){
                    component.set("v.listaGradosCompleta", []);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(obtenerGrados);
    }
})