({
	doInit : function(component, event) {
        var clave = component.get("v.clave");
        var valorLista = component.get("v.valorLista");
        component.set("v.valor", valorLista[clave]);
	}
})