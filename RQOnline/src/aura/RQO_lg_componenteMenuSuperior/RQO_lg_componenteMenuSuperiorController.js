({ 
    getInfo : function(component, event, helper) {
        helper.getInfo(component, event);
    },
    getInfoController : function(cmp, event, helper) {
        helper.getInfoController(cmp, event);
    },
    almacenarPeticion : function(cmp, event, helper) {
        helper.almacenarPeticion(cmp, event);
    },
    login : function(component, event, helper) {
        helper.login(component);
    }
})