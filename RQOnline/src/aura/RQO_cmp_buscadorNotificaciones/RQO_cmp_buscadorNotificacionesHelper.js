({
    doInit : function(component, event){
        debugger;
        var action = component.get('c.getNotificationsCategories');
        action.setBackground();
        action.setAbortable();
        action.setCallback(this, function(response) {
            var storedResponse = response.getReturnValue();
            if (storedResponse){
                var customMap = [];
                for (var key in storedResponse) {
                    customMap.push({value:storedResponse[key], key:key});
                }
                component.set("v.categories", customMap);
            }
        }); 
        // enqueue the Action   
        $A.enqueueAction(action);
	}, buscar : function(component) {
        
        var categoria = component.get("v.categoria");
        var dateFrom = component.get("v.dateFrom");
        var dateTo = component.get("v.dateTo");               
        debugger;
        
        var appEvent = $A.get("e.c:RQO_evt_searchOrder");
        appEvent.setParams({"categoria": categoria ,"dateFrom" : dateFrom, "dateTo" : dateTo});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})