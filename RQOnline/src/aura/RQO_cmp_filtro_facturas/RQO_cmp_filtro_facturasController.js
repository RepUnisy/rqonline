({
        
    advanceSearch : function(component, event, helper) {
        jQuery('.rq-search-advance').toggleClass('open fa-angle-down fa-angle-up');
        
        jQuery('.rq-input-inf').slideToggle();
    },
    getInfo : function(component, event, helper) {
        var language = event.getParam("language");
        var user = event.getParam("user");

        component.set("v.language", language);
        component.set("v.user", user);
        var fechaActual = new Date();
       
        debugger;
        component.set('v.fechaEmisionHasta', fechaActual.getFullYear() + "-" + (fechaActual.getMonth() + 1) + "-" + fechaActual.getDate());
        component.set('v.fechaEmisionDesde', fechaActual.getFullYear() + "-" + (fechaActual.getMonth() - 1) + "-" + fechaActual.getDate());
        

        helper.getTipoFacturas(component, event);
    },
    buscar : function(component, event, helper) {
        helper.buscar(component, event);
    },
	limpiarBusqueda : function(component, event, helper) {
	    component.set("v.nFactura", null);
	    component.set("v.tipoFactura", null);
	    component.set("v.estado", null);
	    component.set("v.grado", null);
        component.set("v.nAlbaran", null);
	    component.set("v.nPedido", null);
	    component.set("v.fechaVencimientoDesde", null);
	    component.set("v.fechaVencimientoHasta", null);
        
        var fechaActual = new Date();
        
        debugger;
        //component.set('v.fechaEmisionHasta', fechaActual.getFullYear() + "-" + (fechaActual.getMonth() + 1) + "-" + fechaActual.getDate());
        //component.set('v.fechaEmisionDesde', fechaActual.getFullYear() + "-" + (fechaActual.getMonth() - 1) + "-" + fechaActual.getDate());
        component.set('v.fechaEmisionHasta', null);
        component.set('v.fechaEmisionDesde', null);
        
        component.find("selectionDestMercancias").set("v.value", "");
        
    }
})