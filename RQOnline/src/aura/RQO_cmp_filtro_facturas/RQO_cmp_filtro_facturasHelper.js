({
    getTipoFacturas : function(component, event) {


        var action = component.get('c.getTipoFacturas');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                 var tiposFactura = JSON.parse(response.getReturnValue());   
                component.set("v.listTiposFacturas", tiposFactura);
            
               debugger;
            }
        }); 
        // enqueue the Action   
        $A.enqueueAction(action);
    },
    buscar : function(component, event){
        debugger;
        var validado = true;
        var inputDateFrom = component.find("dateFromValue");
        var value = inputDateFrom.get("v.value");
        if (!value) {
            inputDateFrom.set("v.errors", [{message: $A.get("$Label.c.RQO_lbl_required")}]);
            validado = false;
        } else {
            validado = true;
            inputDateFrom.set("v.errors", null);
        }

        
        var inputDateTo= component.find("dateToValue");
        var valueTo = inputDateTo.get("v.value");
        if (!valueTo) {
            inputDateTo.set("v.errors", [{message: $A.get("$Label.c.RQO_lbl_required")}]);
            validado = false;
        } else {
            if (validado){
            	validado = true;
			}
            inputDateTo.set("v.errors", null);
        }
        
        
        if(validado){
            var nFactura = component.get("v.nFactura");
            if (!nFactura){nFactura = null;}
            var tipoFactura = component.get("v.tipoFactura");
            if (!tipoFactura){tipoFactura = null;}
            var estado = component.get("v.estado");
            var fechaVencimientoDesde = component.get("v.fechaVencimientoDesde");
            var fechaVencimientoHasta = component.get("v.fechaVencimientoHasta");
            var grado = component.get("v.grado");
            var nAlbaran = component.get("v.nAlbaran");
            var nPedido = component.get("v.nPedido");
            var fechaEmisionDesde = component.get("v.fechaEmisionDesde");
            var fechaEmisionHasta = component.get("v.fechaEmisionHasta");
            var destinoMercancias = component.find("selectionDestMercancias").get("v.value");
            debugger;
            
            /*if (nFactura == ''){
                nFactura = null;
            }
            if (tipoFactura == ''){
                tipoFactura = null;
            }*/
            if (grado == ''){
                grado = null;
            }
            /*if (estado == 'null'){
                estado = null;
            }*/
            if (tipoFactura == 'null'){
                tipoFactura = null;
            }
            if (!destinoMercancias){
                destinoMercancias = null;
            }
                
                           
            
            var appEvent = $A.get("e.c:RQO_evt_SearchOrderEvent");
            appEvent.setParams({"numFactura" : nFactura, "tipoFactura" : tipoFactura, "estado" : estado, 
            "dateFrom" : fechaEmisionDesde, "dateTo" : fechaEmisionHasta, "grado" : grado, "numAlbaran" : nAlbaran, "pedido" : nPedido, 
                                "dateEmisionFrom" : fechaVencimientoDesde, "dateEmisionTo" : fechaVencimientoHasta, "destino" : destinoMercancias});
            try{
                appEvent.fire();
            }catch(err){
                console.log(err.message);
            }
		}
    }
})