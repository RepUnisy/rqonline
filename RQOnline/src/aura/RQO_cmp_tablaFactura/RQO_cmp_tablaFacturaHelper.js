({
    
    initFacts : function(component) {
        debugger;
        //var dateFrom = (fechaActual.getFullYear() - 5) + "-" + ((fechaActual.getMonth() + 1) > 9 ? "" + (fechaActual.getMonth() + 1): "0" + (fechaActual.getMonth() + 1)) + "-" + fechaActual.getDate();
        //var dateTo = fechaActual.getFullYear() + "-" + ((fechaActual.getMonth() + 1) > 9 ? "" + (fechaActual.getMonth() + 1): "0" + (fechaActual.getMonth() + 1)) + "-" + fechaActual.getDate();
        var action = component.get('c.getDateParametrizedData');
        action.setBackground();
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            var addMonths = 3; 
            if (state === "SUCCESS") {
                addMonths = response.getReturnValue();
            }
            var busquedaFactura = false;
            var numeroFactura = null;
            //Comprobamos con una expresion regular si se han pasado parámetros por la url y en ese caso se establecen en el filtro
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                                                     function(m,key,value) {
                                                         console.log(key + ":"+value);
                                                         if(key=="numfact"){
                                                             busquedaFactura = true;
                                                             numeroFactura = value;
                                                         }
                                                     });
            //Si se está pasando como parametro el número de factura establecemos un filtro amplio sin tener en cuenta
            //el rango parametrizado de meses para encuentre la factura concreta que se requiere
            if (busquedaFactura){
                addMonths = 60;
            }
            
            var fechaActual = new Date();
            var dateTo = fechaActual.getFullYear() + "-" + ((fechaActual.getMonth() + 1) > 9 ? "" + (fechaActual.getMonth() + 1): "0" + (fechaActual.getMonth() + 1)) + "-" + fechaActual.getDate();
            fechaActual.setMonth(fechaActual.getMonth() - addMonths);
            var dateFrom = fechaActual.getFullYear() + "-" + ((fechaActual.getMonth() + 1) > 9 ? "" + (fechaActual.getMonth() + 1): "0" + (fechaActual.getMonth() + 1)) + "-" + fechaActual.getDate();
            
            //$Label.c.RQO_lbl_numeroFacturasInicial
            var numFacturas = $A.get("$Label.c.RQO_lbl_numeroFacturasInicial");
            if (numFacturas === '0'){
                numFacturas = null;
            }        
            this.getData(component, numeroFactura, null, null, dateFrom, dateTo, null, null, null, null, null, null, numFacturas);
            
        }); 
        // enqueue the Action   
        $A.enqueueAction(action);
    },
    busqueda : function (component, event){
        component.set("v.mostrarSpinner", true);
        component.set("v.error", false);
        component.set("v.vacio", false);
        var numFactura = event.getParam("numFactura");
        var tipoFactura = event.getParam("tipoFactura");
        var estado = event.getParam("estado");
        var dateFrom = event.getParam("dateFrom");
        var fechaActual = new Date();
		var month = fechaActual.getMonth() + 1;
        if (!dateFrom){dateFrom = (fechaActual.getFullYear()-5) + "-" + month.toString().padStart(2, "0") + "-" + fechaActual.getDate();}
        var dateTo = event.getParam("dateTo");
        if (!dateTo){dateTo = fechaActual.getFullYear() + "-" + month.toString().padStart(2, "0") + "-" + fechaActual.getDate();}
        var grado = event.getParam("grado");
        var numAlbaran = event.getParam("numAlbaran");
        var numPedido = event.getParam("pedido");
        var dateEmisionFrom = event.getParam("dateEmisionFrom");
        var dateEmisionTo = event.getParam("dateEmisionTo");
        var destino = event.getParam("destino");

        if (numFactura){component.set("v.numFacturaSearch", numFactura);}else{component.set("v.numFacturaSearch", null);}
        this.getData(component, numFactura, tipoFactura, estado, dateFrom, dateTo, grado, numAlbaran, numPedido, dateEmisionFrom, dateEmisionTo, destino, null);
        
    },
    getData : function (component, numFactura, tipoFactura, estado, dateFrom, dateTo, grado, numAlbaran, numPedido, dateEmisionFrom, dateEmisionTo, destino, numeroDeFacturas){
        var usuario = component.get("v.user");
        var idCliente = usuario.idExternoCliente;
        var action = component.get('c.getFacturas');
        action.setParams({ idCliente : idCliente, numFactura : numFactura, tipoFactura : tipoFactura, estado: estado, dateFrom : dateFrom, dateTo : dateTo, grado : grado, numAlbaran : numAlbaran, 
                          numPedido : numPedido, dateEmisionFrom : dateEmisionFrom, dateEmisionTo : dateEmisionTo, ivNumReg : numeroDeFacturas, destinoMercancias : destino, isHome : false});
        action.setBackground();
        action.setAbortable();
        action.setCallback(this, function(response){
            var state = response.getState();
            var numFacturas = 0;
            var invoiceNumber = component.get("v.numFacturaSearch");
            console.log('ACL. Estado getFactura ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                if (response && response.getReturnValue()){
					var listFacturas = JSON.parse(response.getReturnValue());
                    if (listFacturas.length != 0){
                        numFacturas = listFacturas.length;
                        if(listFacturas[0].error){
                            component.set("v.error", true);
                        }else{
                            component.set("v.listFactura", listFacturas);
                            //Cálculo datos paginación inicial
                            var numeroPaginas = parseInt((listFacturas.length / $A.get("$Label.c.RQO_lbl_facturasPorPagina")) + 1);
                            if(listFacturas.length % $A.get("$Label.c.RQO_lbl_facturasPorPagina") == 0){
                                numeroPaginas -= 1;
                            }
                            var listaPaginas = [];
                            for (var i = 1; i <= numeroPaginas; i++) {
                                listaPaginas.push(i);
                            }
                            component.set("v.numeroPaginas", listaPaginas);
                            if(listaPaginas.length <= 1){
                                component.set("v.mostrarPaginacion", false);
                            }else{
                                component.set("v.mostrarPaginacion", true);
                            }
                            var inicioPaginacion = $A.get("$Label.c.RQO_lbl_cero");
                            var finPaginacion = $A.get("$Label.c.RQO_lbl_facturasPorPagina");
                            component.set("v.inicioPaginacion", inicioPaginacion);
                            component.set("v.finalPaginacion", finPaginacion);
                            component.set('v.paginaActiva',1);
                            this.paginacion(component, listFacturas, inicioPaginacion, finPaginacion, 1);
                            this.registrarEvento(component, '18', '31', invoiceNumber);
                        }
                    }else{
                        component.set("v.vacio", true);
                        numFacturas = 0;
                        this.registrarEvento(component, '18', '31', invoiceNumber);
                    }
                }else{
                    component.set("v.vacio", true);
                	numFacturas = 0;
                    this.registrarEvento(component, '18', '31', invoiceNumber);
                }
            }else if (state === "INCOMPLETE") {
                component.set("v.error", true);
                console.log('Incomplete');
                numFacturas = 0;
            }else if (state === "ERROR") {
                component.set("v.error", true);
                var errors = response.getError()
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                numFacturas = 0;
			}
            component.set("v.numeroFacturas", numFacturas);
            component.set("v.mostrarSpinner", false);
        });
        $A.enqueueAction(action);  
    },
    convertArrayOfObjectsToCSV : function(component){
		
		//Se recorre la lista de wrappers y la sublista de wrappers de cada objeto principal para pintar los datos básicos y los desglosados (si los tiene)
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        //Obtenemos la lista de registros y verificamos que tiene datos
        var objectRecords = component.get("v.listFactura");
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        
		//Definimos los separadores de columna y linea
        columnDivider = ',';
        lineDivider =  '\n';
        
        //Definimos las keys sobre las que luego buscaremos los datos. Tenemos keys para la lista de facturas y para el desglose de cada una de ellas.
		//Cada key debe coincidir con su atributo del wrapper de pedidos
        keys = ['numFactura','fechaEmision','fechaVencimiento','importeFormateado','tipo','estado' ];
        var keysBrokenDown = ['numRefCliente', 'numPedido', 'numAlbaran', 'grado', 'cantidad', 'tipoCantidad', 'precioToneladaFormateado', 'importeNetoFormateado', 'destino'];
		//Cabecera que se pinta en la excel. 
        //var labelizedHeader = [$A.get("$Label.c.RQO_lbl_invoiceNumber"),'fechaEmision','fechaVencimiento',
        //'importe','tipo','estado', 'numRefCliente', 'numPedido', 
        //'numAlbaran', 'grado', 'cantidad', 'tipoCantidad', 'precioTonelada', 'importe', 'destino'];
        
        var labelizedHeader = [$A.get("$Label.c.RQO_lbl_numFacturaExport"),$A.get("$Label.c.RQO_lbl_fechaEmision2"),
                               $A.get("$Label.c.RQO_lbl_fechaVencimiento"),
                               $A.get("$Label.c.RQO_lbl_importe"),
                               $A.get("$Label.c.RQO_lbl_tipo"),
                               $A.get("$Label.c.RQO_lbl_Status"),
                               $A.get("$Label.c.RQO_lbl_numRefExport"),
                               $A.get("$Label.c.RQO_lbl_numerodePedido"),
                               $A.get("$Label.c.RQO_lbl_numAlbaran"),
                               $A.get("$Label.c.RQO_lbl_pedidoSeguimientoGrade"),
                               $A.get("$Label.c.RQO_lbl_pedidoSeguimientoQuantity"),
                               $A.get("$Label.c.RQO_lbl_tipoCantidad"),
                               $A.get("$Label.c.RQO_lbl_precioTonelada"),
                               $A.get("$Label.c.RQO_lbl_importe"),
                               $A.get("$Label.c.RQO_lbl_pedidoSeguimientoDestination")];
        
        csvStringResult = '';
        csvStringResult += labelizedHeader.join(columnDivider);
        csvStringResult += lineDivider;
        var listaDetalle = null;
        var obj = null;
        var tipoMoneda = '';
		//Vamos a recorrer los datos
        for(var i=0; i < objectRecords.length; i++){
			//Recuperamos cada factura y su desglose. 
            obj =objectRecords[i];
            tipoMoneda = obj.tipoMoneda;
            listaDetalle = obj.detallesList;
			//Si tiene desglose pintaremos para cada registro del desglose los datos básicos y los datos detallados. Si no tiene pintamos únicamente los datos básicos
            if (listaDetalle && listaDetalle.length > 0){
                csvStringResult += this.createBrokenDownCsv(obj, listaDetalle, keysBrokenDown, keys, columnDivider, lineDivider, tipoMoneda);
            }else{
                csvStringResult += this.createBasicCsv(obj, keys, columnDivider, tipoMoneda);
                csvStringResult += lineDivider;
            }
        }
        
        // return the CSV formate String 
        return csvStringResult;        
    }, 
    createBasicCsv: function (obj, keys, columnDivider, tipoMoneda){
        var csvStringResult = '';
        var counter = 0;
		var skey = null;
		var data = '';
        //Pintamos los datos básicos de factura (sin desglose). Se busca cada key dentro del objeto actual y se añaden los separadores de campo.
        for(var sTempkey in keys) {            
            skey = keys[sTempkey] ;  
			//Añadimos el separador de campos
            if(counter > 0){ 
                csvStringResult += columnDivider; 
            }   
			data = '';
			if (obj[skey]){
				data = obj[skey];
                if (skey === 'importeFormateado'){data += ' ' + tipoMoneda;}
			}
			//Añadimos el datos al string que devolvemos al método principal
            csvStringResult += '"'+ data+'"'; 
            
            counter++;
            
        }
    	return csvStringResult;
	},
    createBrokenDownCsv: function (obj, listObjDetail, keysBrokenDown, keys, columnDivider, lineDivider, tipoMoneda){
        var csvStringResult = '';
        var counter = 0;
		var objList = null;
		var skey = null;
		var data = '';
		debugger;
		//Vamos a pintar los datos de factura con desglose. Recorremos la lista detallada del objeto actual
		for(var i=0; i < listObjDetail.length; i++){
            counter = 0;
			//Pintamos los datos básicos del objeto. Cada linea desglosada lleva los datos básicos.
			csvStringResult += this.createBasicCsv(obj, keys, columnDivider, tipoMoneda);
			csvStringResult += columnDivider;
			objList = listObjDetail[i];
			//Vamos a recorrer todas las key dede desglose para pintar los datos del objeto desglosado que tenemos en la sublist del objeto actual
			for(var sTempkey in keysBrokenDown) {
				skey = keysBrokenDown[sTempkey];  
				//Añadimos el separador de campos
				if(counter > 0){ 
					csvStringResult += columnDivider; 
				}   
				data = '';
				if (objList[skey]){
					data = objList[skey];
                    if (skey === 'importeNetoFormateado' || skey === 'precioToneladaFormateado'){data += ' ' + tipoMoneda;}
				}
				//Añadimos el datos al string que devolvemos al método principal
				csvStringResult += '"'+ data +'"'; 
				
				counter++;
            
			}
			//Añadimos el separador de lineas
            csvStringResult += lineDivider;
        }
		
    	return csvStringResult;
	},
    toggleTabla: function (component, event){
        debugger;
        var tabla = event.currentTarget;
        jQuery(tabla).closest("tr").toggleClass('active-row').addClass('this');
        jQuery(tabla).toggleClass('fa-chevron-down fa-chevron-up');
        if (window.matchMedia("(max-width: 819px)").matches) {
            //Tabla exterior
            jQuery('.this td.movil-hide').toggleClass('show');
            jQuery('tr').removeClass('this');   
        }
        else{
            //Ha partir de escritorio muestra la tabla interior
            
            jQuery('.this + tr .wrapper-tabla-interior').toggleClass('show');
            jQuery('tr').removeClass('this');
        }
        this.registrarEvento(component, '20', '30', tabla.dataset.id);
        
    },
    toggleTablaInterior: function (component, event){
        
        /*::::::: T A B L A  I N T E R I O R :::::::*/
        
        var tablaInterior = event.currentTarget;
        jQuery(tablaInterior).closest('tr').addClass('this-active');
        jQuery(tablaInterior).toggleClass('fa-chevron-down fa-chevron-up');
        
        //Si el elemento actual se clicka pero su padre no esta activo se borra
        //jQuery('.this-active:not(active-row) + tr .wrapper-tabla-interior').removeClass('show');
        
        //Si el elemento actual se clicka y su padre esta desplegado se muestra
        jQuery('.this-active.active-row + tr .wrapper-tabla-interior').toggleClass('show');
        
        jQuery('tr').removeClass('this-active');
        
        this.registrarEvento(component, '20', '30', tabla.dataset.id);
    },
    cambiarPagina : function(component, event) {
       	component.set("v.mostrarSpinner", true);
        var pagina = parseInt(event.getSource().get("v.value"));
        component.set('v.paginaActiva',pagina);
        var inicioPaginacion = (pagina * $A.get("$Label.c.RQO_lbl_facturasPorPagina")) - $A.get("$Label.c.RQO_lbl_facturasPorPagina");
        var finPaginacion = (pagina * $A.get("$Label.c.RQO_lbl_facturasPorPagina"));
        
        component.set("v.inicioPaginacion", inicioPaginacion);
        component.set("v.finalPaginacion", finPaginacion);
		var customMap = component.get("v.listFactura");
         /*:::: PARTE FRONT PAGINADOR :::::*/
            var paginadores = component.find('paginadores');
            
            for(var i in paginadores){
                $A.util.removeClass(paginadores[i], 'page-active');
            }
            
            $A.util.addClass(paginadores[pagina-1], 'page-active');
        /*:::: PARTE FRONT PAGINADOR :::::*/
        
        this.paginacion(component, customMap, inicioPaginacion, finPaginacion, pagina);
        component.set("v.mostrarSpinner", false);
        
    },paginacion : function(component, lista, inicioPaginacion, finPaginacion, pagina){
        component.set("v.listFacturaPaginada", lista.slice(inicioPaginacion, finPaginacion));
    }, registrarEvento : function(component, componentDescription, actionDescription, invoiceNumber){
        /*
         * Registro del evento de consulta de factura
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         *  Valor 18 = Search of invoices
         *  Valor 20 = Invoice detail query
         * actionType - Grupo al que pertenece, 5 = Inovices
         * actionDescription - descripción de la acción que lanza el evento. En este caso consulta de factura. 
         * Valores para este componente:
         *  Valor 30 = Invoice detail query
         *  Valor 31 = Search of invoices
         * reNumFactura - Número de la factura si se ha filtrado por ese campo
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_tablaFactura', "reComponentDescription" : componentDescription, "actionType" : '5', 
                            "actionDescription" : actionDescription, "reNumFactura" : invoiceNumber, 
                            "rePathActual" : window.location.pathname});
        
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})