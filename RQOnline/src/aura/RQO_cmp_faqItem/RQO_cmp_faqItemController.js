({
    toggleFaq : function(component, event, helper) {
        var activo = event.currentTarget;
        jQuery(activo).toggleClass('visible').next().addClass('sliderfaq');
        jQuery('.sliderfaq').slideToggle().removeClass('sliderfaq');
	},
	onRender : function(component, event, helper) {
		if (component.get("v.desplegada") == true)
		{
			jQuery('.visible').next().addClass('sliderfaq');
	        jQuery('.sliderfaq').slideToggle().removeClass('sliderfaq');
		}
	},
})