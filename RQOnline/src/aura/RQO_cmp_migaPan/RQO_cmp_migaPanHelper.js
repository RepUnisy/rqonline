({
	cambiaPagina : function(component, event) {
     //  var url = '/' + component.get("v.pagina");
     	 var url = "/";
        console.log('URL de destino:' + url);
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();
	},
    cambiaPaginaCategoria : function(component, event) {
        var url = '/catalogo';
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();
	},
    cambiaPaginaGrado : function(component, event) {
        var id = component.get("v.idGrado");
        var url = "/grado?id=" + id;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
           "url": url,
           "isredirect" : true
        });
        urlEvent.fire();
	},
    doInit : function(component, event){
        var user = component.get("v.user");
        if(user == null){
            component.set("v.userHasValue", false);
        }
        else{
            component.set("v.userHasValue", true);
            if(user.isPrivate){
                component.set("v.userIsPrivate", true);
            }
            else{
                component.set("v.userIsPrivate", false);
            }
        }
    }
})