({
	cambiaPagina : function(component, event, helper) {
		helper.cambiaPagina(component, event);
	},
    cambiaPaginaGrado : function(component, event, helper) {
		helper.cambiaPaginaGrado(component, event);
	},
    cambiaPaginaCategoria : function(component, event, helper) {
		helper.cambiaPaginaCategoria(component, event);
	},
    doInit : function(component, event, helper){
        helper.doInit(component, event);
    }
})