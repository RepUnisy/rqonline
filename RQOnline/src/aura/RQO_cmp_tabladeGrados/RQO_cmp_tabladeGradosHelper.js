({

    cargaTabla : function(component, event) {
        component.set("v.catalogo", true);
        var mapatabla = event.getParam("mapaTabla");
        component.set("v.mapaTabla", mapatabla);
        
	}, 
    doInit : function(component, event){
                    debugger;
        debugger;
        debugger;
        var mapaTabla = component.get("v.mapaTabla");
        if (mapaTabla.listaTraduccionesEspeci != null){
             mapaTabla.listaTraduccionesEspeci.reverse();
        }
       
  
        component.set("v.mapaInfoGrados", component.get("v.mapaTabla.mapaInfoGrados"));
        component.set("v.idCategoriaTabla", component.get("v.mapaTabla.categoria"));
        var listaEspecificaciones = component.get("v.mapaTabla.listaEspecificaciones");
        var tamanioListaEspecificaciones = listaEspecificaciones.length;
        component.set("v.tamanioListaEspecificaciones", tamanioListaEspecificaciones);
        var listaKeys = [];
        var nombreCategoria = $A.get("$Label.c.RQO_lbl_nombreCategoria");
        listaKeys = component.get("v.mapaTabla.listOrderedGrades");
        component.set("v.listaKeys", listaKeys);
        component.set("v.inicioPaginacion", $A.get("$Label.c.RQO_lbl_cero"));
        component.set("v.finalPaginacion", $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla"));
        component.set("v.listaKeysPaginacion", listaKeys.slice(component.get("v.inicioPaginacion"), component.get("v.finalPaginacion")));
        var numeroPaginas = parseInt((listaKeys.length / $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla")) + 1);
        if(listaKeys.length % $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla") == 0){
            numeroPaginas -= 1;
        }
        var listaPaginas = [];
        for (var i = 1; i <= numeroPaginas; i++) {
            listaPaginas.push(i);
        }
        component.set("v.numeroPaginas", listaPaginas);
        if(listaPaginas.length <= 1){
            component.set("v.mostrarPaginacion", false);
        }

       
        /* FRONT-END */
        var indiceOrdenacion = component.get("v.OrdenacionActual");
         setTimeout(function(){
           jQuery('tbody td').each(function()
				{         
                    if(jQuery(this).index() == indiceOrdenacion)
                    {
                       jQuery(this).addClass('sortable-active');
                    }
				});
              jQuery('thead th').each(function()
				{
                  
                    if(jQuery(this).index() == indiceOrdenacion)
                    {
                        jQuery(this).addClass('sortable-active');
                        jQuery(this).find('span').addClass('fa fa-caret-up');
                    }
				});
            },  100);
        
        /*FIN FRONT-END*/
        
        component.set("v.visible", true);
    },
    insertionSort : function(listaOrdenar, tipoDato){
        var len = listaOrdenar.length;
        if(tipoDato == $A.get("$Label.c.RQO_lbl_texto")){
           for (var i = 1; i < len; i++) {
                var tmp = listaOrdenar[i]; 
                for (var j = i - 1; j >= 0 && (listaOrdenar[j][2] > tmp[2]); j--) {
                    listaOrdenar[j + 1] = listaOrdenar[j];
                }
                listaOrdenar[j + 1] = tmp;
           } 
        }
        else{
            if(tipoDato == $A.get("$Label.c.RQO_lbl_numero")){
                for (var i = 1; i < len; i++) {
                    var tmp = listaOrdenar[i]; 
                    for (var j = i - 1; j >= 0 && (Number(listaOrdenar[j][2]) > Number(tmp[2])); j--) {
                        listaOrdenar[j + 1] = listaOrdenar[j];
                    }
                    listaOrdenar[j + 1] = tmp;
               }
            }
        }  
    },
    insertionSortNombre : function(listaOrdenar){
        var len = listaOrdenar.length;
        for (var i = 1; i < len; i++) {
            var tmp = listaOrdenar[i]; 
            for (var j = i - 1; j >= 0 && (listaOrdenar[j] > tmp); j--) {
                listaOrdenar[j + 1] = listaOrdenar[j];
            }
            listaOrdenar[j + 1] = tmp;
        } 
    },
    ordenarCatalogoEspecificaciones : function(component, event){
        
        var caracteristicaAOrdenar = event.getParam("especificacion");
		var mapaTabla = component.get("v.mapaTabla.valoresTabla");
        var tipoDatos = component.get("v.mapaTabla.tipoDatosCategorias");
        var listaOrdenar = [];
        var listaGuiones = [];
        component.set("v.listaOrdenarPaginacion", listaOrdenar);
        
        for (var key in mapaTabla) {
            if (mapaTabla[key] != null) {
                var elemento = [];
                elemento.push(key); elemento.push(caracteristicaAOrdenar); elemento.push(mapaTabla[key][caracteristicaAOrdenar]);
                if(elemento[2] == $A.get("$Label.c.RQO_lbl_guion")){
                    listaGuiones.push(elemento);
                }
                else{
                    listaOrdenar.push(elemento);
                }
            }
        }
        this.insertionSort(listaOrdenar, tipoDatos[caracteristicaAOrdenar]);
        listaOrdenar.push.apply(listaOrdenar, listaGuiones);
        var listaOrdenada = [];
        for(var i = 0; i < listaOrdenar.length ; i++){
            listaOrdenada.push(listaOrdenar[i][0]);
        }
        component.set("v.listaOrdenar", listaOrdenada);
        component.set("v.listaOrdenarPaginacion", listaOrdenada.slice(component.get("v.inicioPaginacion"), component.get("v.finalPaginacion")));
        component.set("v.ordenacion", true);

    },
    ordenarCatalogoEspecificacionesDescendente : function(component, event){
        
        var caracteristicaAOrdenar = event.getParam("especificacion");
		var mapaTabla = component.get("v.mapaTabla.valoresTabla");
        var tipoDatos = component.get("v.mapaTabla.tipoDatosCategorias");
        var listaOrdenar = [];
        var listaGuiones = [];
        component.set("v.listaOrdenarPaginacion", listaOrdenar);
        
        for (var key in mapaTabla) {
            if (mapaTabla[key] != null) {
                var elemento = [];
                elemento.push(key); elemento.push(caracteristicaAOrdenar); elemento.push(mapaTabla[key][caracteristicaAOrdenar]);
                if(elemento[2] == $A.get("$Label.c.RQO_lbl_guion")){
                    listaGuiones.push(elemento);
                }
                else{
                    listaOrdenar.push(elemento);
                }
            }
        }
        this.insertionSort(listaOrdenar, tipoDatos[caracteristicaAOrdenar]);
        listaOrdenar.push.apply(listaOrdenar, listaGuiones);
        var listaOrdenada = [];
        for(var i = listaOrdenar.length - 1; i >= 0 ; i--){
            listaOrdenada.push(listaOrdenar[i][0]);
        }
        component.set("v.listaOrdenar", listaOrdenada);
        component.set("v.listaOrdenarPaginacion", listaOrdenada.slice(component.get("v.inicioPaginacion"), component.get("v.finalPaginacion")));
        component.set("v.ordenacion", true);
    },
    ordenarCatalogoNombre : function (component, event){
        
        var mapaTabla = component.get("v.mapaTabla.valoresTabla");
		var categoria = event.getParam("categoriaOrdenarNombre");
        var listaOrdenar = [];
        component.set("v.listaOrdenarPaginacion", listaOrdenar);
        
        listaOrdenar = component.get("v.mapaTabla.listOrderedGrades");
        component.set("v.listaOrdenar", listaOrdenar);
        component.set("v.listaOrdenarPaginacion", listaOrdenar.slice(component.get("v.inicioPaginacion"), component.get("v.finalPaginacion")));
        component.set("v.ordenacion", true);
    },
    ordenarCatalogoNombreDescendente : function (component, event){
        
        var mapaTabla = component.get("v.mapaTabla.valoresTabla");
		var categoria = event.getParam("categoriaOrdenarNombre");
        var listaOrdenar = [];
        var listaOrdenada = [];
        component.set("v.listaOrdenarPaginacion", listaOrdenar);
        listaOrdenar = component.get("v.mapaTabla.listOrderedGrades");
        
        for(var i = listaOrdenar.length - 1; i >= 0 ; i--){
            listaOrdenada.push(listaOrdenar[i]);
        }
        
        component.set("v.listaOrdenar", listaOrdenada);
        component.set("v.listaOrdenarPaginacion", listaOrdenada.slice(component.get("v.inicioPaginacion"), component.get("v.finalPaginacion")));
        component.set("v.ordenacion", true);

        
    },
     
})