({
	cargaTabla : function(component, event, helper) {
        helper.cargaTabla(component, event);
	},
    doInit : function(component, event, helper) {
        helper.doInit(component, event);
         jQuery('html').scrollTop(0);
	}, 
    ordenarCatalogo : function(component, event, helper){
        var nombre = event.getParam("nombre");
        var idCategoriaTabla = event.getParam("idEvtCategoriaTabla");
        var idAnteriorOrdenacion = component.get("v.idAnteriorOrdenacion");
        var contadorOrdenacion = component.get("v.contadorOrdenacion");
        if(idAnteriorOrdenacion == idCategoriaTabla){
            contadorOrdenacion++;
            console.log('Contador de Ordenacion: '+ contadorOrdenacion);
            component.set("v.contadorOrdenacion", contadorOrdenacion);
            if(nombre){
                /*Front*/
                console.log('1');
                jQuery('th span').removeClass('fa fa-caret-down fa-caret-up');
                /*Front*/
                if(contadorOrdenacion % 2 == 0){
                    console.log('1.5');
                    /*Front*/
                    jQuery('th:first-child span').removeClass('fa fa-caret-up').addClass('fa fa-caret-down');
                    /*Front*/
                    helper.ordenarCatalogoNombreDescendente(component, event);
                }
                else{
                    console.log('1.6');
                    /*Front*/
                    jQuery('th:first-child span').removeClass('fa fa-caret-down').addClass('fa fa-caret-up');
                    /*Front*/
                    helper.ordenarCatalogoNombre(component, event);
                }
            }
            else{
                console.log('2');
                if(contadorOrdenacion % 2 == 0){
                    console.log('2.5');
                    helper.ordenarCatalogoEspecificacionesDescendente(component, event);
                }
                else{
                    console.log('2.6');
                    helper.ordenarCatalogoEspecificaciones(component, event);
                }
            }
        }
        else{
            contadorOrdenacion = 0;
            component.set("v.contadorOrdenacion", contadorOrdenacion);
            idAnteriorOrdenacion = idCategoriaTabla;
            component.set("v.idAnteriorOrdenacion", idAnteriorOrdenacion);
            if(nombre){
                helper.ordenarCatalogoNombre(component, event);
            }
            else{
                helper.ordenarCatalogoEspecificaciones(component, event);
            }
        }
    },
    ordenacionEspecificacionesNombre : function(component, event, helper) {   
        var eventoOrdenarTabla = $A.get("e.c:RQO_evt_ordenarEspecificacionTabla");
        eventoOrdenarTabla.setParams({"nombre" : true,
                                      "idEvtCategoriaTabla" : component.get("v.idCategoriaTabla")});
        eventoOrdenarTabla.fire();
        
        
         /*PARTE FRONT-END*/
		 	var element = event.currentTarget;
        	jQuery(element).addClass('active-order');
        	jQuery(element).closest('table').addClass('active-table');

        	var indice = jQuery('.active-order').index();
        	component.set('v.OrdenacionActual',indice);
			
       	 setTimeout(function(){
             jQuery('.active-table tbody td').removeClass('sortable-active');
             jQuery('.active-table th').removeClass('sortable-active');
             jQuery('.active-order').addClass('sortable-active');
             
                jQuery('.active-table tbody td').each(function()
				{
                    
              	 if(jQuery(this).index() == indice)
				   {
               		 jQuery(this).addClass('sortable-active');
            	   }
				});  
                
        	 jQuery(element).removeClass('active-order');
       		 jQuery(element).closest('table').removeClass('active-table');
              }, 500);

        /* FIN FRONT-END */
        
	},
    cambiarPagina : function(component, event, helper) {
        
        component.set("v.finalPaginacion", (component.get("v.pagina") * $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla")));
        component.set("v.inicioPaginacion", (component.get("v.pagina") * $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla")) - $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla"));
        var finalPaginacion = component.get("v.finalPaginacion");
        var listaKeys = component.get("v.listaKeys");
        component.set("v.listaKeysPaginacion", listaKeys.slice(component.get("v.inicioPaginacion"), component.get("v.finalPaginacion")));
        var listaOrdenada = component.get("v.listaOrdenar");
		var indiceOrdenacion = component.get("v.OrdenacionActual");
        component.set("v.listaOrdenarPaginacion", listaOrdenada.slice(component.get("v.inicioPaginacion"), component.get("v.finalPaginacion")));
		
    }
        
})