({
    init : function(component, event, helper){
              
        
        var user = component.get("v.userHeredado");
        debugger;
        var lista = user.listIncoterm;
        component.set("v.listIncoterm",lista);  
    },
    toggleInfo: function(component, event, helper){
      /*FRONTEND*/
        jQuery('.rq-msg-info').slideToggle();
    },
    changeDestino : function(component, event, helper) {
       
        helper.mostrarGuardar(component);
        var listadoPosiciones = component.get("v.listPosicionPedidoTable");
        var listDestinos = component.get("v.userHeredado.listDirMercancias");
        var idDireccion = event.getSource().get("v.value");
        var idLinea = event.getSource().get("v.name");
        var nombreDir = '';
        debugger;
        for (var i = 0; i<listDestinos.length; i++){
            if (listDestinos[i].RQO_fld_cliente__r.Id == idDireccion){
                debugger
                nombreDir = listDestinos[i].RQO_fld_cliente__r.Name;
            }            
        }
        debugger;
        listadoPosiciones[idLinea].destinoMercanciasNombre = nombreDir;
        debugger;
        
    },
    actualizarTabla : function(component, event, helper) {
        helper.validacionEnvase(component, parametro);
    },
    mostrarGuardar : function(component, event, helper) {
        helper.mostrarGuardar(component);
    },
    controlFecha : function(component, event, helper) {
      /*   debugger;
         var elem = event.getSource();
        var fecha = new Date ();
        var fechaIntroducida = new Date (elem.get("v.value"));       
       
        if ( fechaIntroducida <= fecha){
         	elem.set("v.errors", [{message:"Introduce una fecha mayor"}]);
        }
        else{
            elem.set("v.errors", null);
        }
        */
        helper.mostrarGuardar(component);
    },
    openDuplicar : function(component, event, helper) {
        debugger;
        var posicion = event.getSource().get("v.name");
        var lista = component.get("v.listPosicionPedidoTable");
        var wrapper = lista[posicion];
        
        component.set("v.posicionDuplicar", wrapper);
        component.set("v.clickDuplicar", true);
    },
    closeDuplicar: function(component, event, helper) {
        component.set("v.clickDuplicar", false);
    },
    aceptDuplicar: function(component, event, helper) {
        debugger;
        var elemDuplicar = component.get("v.posicionDuplicar");
        var cantidadDuplicar = component.get("v.cantidadDuplicar");
        helper.duplicar(component.get('c.cantidadDuplicar'), elemDuplicar, cantidadDuplicar);
        component.set("v.clickDuplicar", false);
    },
    toggleTabla : function(component, event, helper) {
        helper.toggleTabla(component,event)
    },    
    openEliminar : function(component, event, helper) {
        var listado = component.get("v.listPosicionPedidoTable");       
        var id = event.getSource().get("v.name");
        debugger;
        var listadoEliminar = component.get("v.listEliminadoHeredado");        
        
        var elementoEncontrado =  listado[id];
        
        listadoEliminar.push(elementoEncontrado);
        var index = listado.indexOf(elementoEncontrado);
        listado.splice(index, 1);
        debugger;
        
        //	Actualizamos los listados
        component.set("v.listEliminadoHeredado", listadoEliminar);
        component.set("v.listPosicionPedidoTable", listado);
        
        debugger;
    }, 
    picklist : function(component, event, helper){
        helper.mostrarGuardar(component);
        debugger;
        var listPosicionPedidoTable = component.get("v.listPosicionPedidoTable");
        var envaseSeleccionado = event.getSource().get("v.value");
        var parametro = event.getSource().get("v.name");
      
        listPosicionPedidoTable[parametro].listModoEnvio = listPosicionPedidoTable[parametro].mapModoEnvio[listPosicionPedidoTable[parametro].mapaIdEnvasesTP[envaseSeleccionado]];
        listPosicionPedidoTable[parametro].selectModoEnvio = [];
              
        var map = new Map(Object.entries(listPosicionPedidoTable[parametro].mapTradEnvaseUidadMedida));
        for(var i in listPosicionPedidoTable[parametro].listModoEnvio){
           
           
            var labelGenerada =  (map.get(listPosicionPedidoTable[parametro].listModoEnvio[i]) != null) ? map.get(listPosicionPedidoTable[parametro].listModoEnvio[i]) : listPosicionPedidoTable[parametro].listModoEnvio[i] ;
            listPosicionPedidoTable[parametro].selectModoEnvio.push({value : listPosicionPedidoTable[parametro].mapaIdModosEnvio[labelGenerada], label : labelGenerada});
        }   
        
        component.set("v.listPosicionPedidoTable", listPosicionPedidoTable);
        
        var envase = listPosicionPedidoTable[parametro].mapaIdEnvasesTP[listPosicionPedidoTable[parametro].envase];
        var unidaMedida = listPosicionPedidoTable[parametro].modoEnvio;
        
        var infoShipping = listPosicionPedidoTable[parametro].mapEnvaseUndMedida[envase + unidaMedida];
        //listPosicionPedidoTable[parametro].pesoMaximo
        //listPosicionPedidoTable[parametro].pesoMaximo
        
        debugger;
        helper.validacionEnvase(component, parametro);
    },
      
    comprobar: function(component, event, helper) {
        helper.mostrarGuardar(component);
        var parametro = event.getSource().get("v.name");
        var envaseSeleccionado = event.getSource().get("v.value");
		debugger;        
        helper.validacionEnvase(component, parametro);
        debugger;
    },
    
    guardar: function(component, event, helper) {
        
        //	Obtenemos la petición de almacenar, si es true procedemos a ello
        var guardar = component.get("v.guardarHeredado");        
        if (guardar){
            var listPosicionPedidoTable = component.get("v.listPosicionPedidoTable");
            var bloqueo = false;
            debugger;
            for(var i = 0; i<listPosicionPedidoTable.length; i++){
                //	Realizamos las validaciones de envases
                var blockObt = helper.validacionEnvase(listPosicionPedidoTable[i]);
                if(blockObt && !bloqueo){
                    bloqueo = blockObt;
                }
                 var idFecha = 'fecha_' + listPosicionPedidoTable[i].idAsset;
                debugger;
                //	Realizamos las validaciones de fechas
                var blockFech =  helper.validacionFecha(listPosicionPedidoTable[i], component.get("v.userHeredado"), component.find("fechapreferente")[i]);
                if(blockFech && !bloqueo){
                    bloqueo = blockFech;
                }
            }
            component.set("v.bloqueoSiguienteHeredado", bloqueo);
            if (!bloqueo){
                console.log('ACL. Almacenamos la tabla');
                helper.guardar(component);
            }        
            else{
                component.set("v.listPosicionPedidoTable", listPosicionPedidoTable);
                
                var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
                appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_revisionCambios"), 
                                    "confirmAlert" : false, 
                                    "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"),                                     
                                    "parentLE" : false});
                try{
                    appEvent.fire();
                }catch(err){
                    this.showSpinner(component);
                    console.log(err.message);
                }
            }            
        }                
    }
})