({
    validacionEnvase: function(item){
        debugger;
        var envase = item.envase;
        var unidaMedida = item.modoEnvio; 
        if (envase == null || unidaMedida == null){            
            item.error = $A.get("$Label.c.RQO_lbl_errorValidacionCantidad" ) ;   
            return true;
        }
        var emvaseMedida =  envase + unidaMedida;
        var info = item.mapEnvaseUndMedida[emvaseMedida] ;
        
        var cantidadUnidad = item.cantidadUnidad;
        var cantidadKg = cantidadUnidad * info.pesoMinimo;
        item.cantidad = cantidadKg;
        
        if (cantidadUnidad == null){
            item.error = $A.get("$Label.c.RQO_lbl_errorValidacionCantidad" ) ;   
            return true;
        }
        
        //	Comprobamos que sea necesario comprobar validaciones      
        if (info != null && !info.excluirValidacion){
                                  
            //	Comprobamos si excede el peso máximo
            if (info.pesoMaximo != null && cantidadKg > info.pesoMaximo){
                var unidadesMaxima = info.pesoMaximo/info.pesoMinimo;
                item.error = $A.get("$Label.c.RQO_lbl_errorValidacionCantidad" ) ;   
                 return true;
            }
            else{                                
                //	Comprbamos que los kg sean multiplos a los kg permitidos
                if (cantidadKg % info.multiploKgUnidad == 0){
                    item.error = '';
                     return false;                 
                }
                else{
                    item.error = $A.get("$Label.c.RQO_lbl_errorValidacionCantidad" ) ;           
                    return true;   
                }
            }
        }
        else{
            return false;
        }   
    },
    
    validacionFecha: function(item, user, inputCmp){        
        var incoterm = item.incoterm;
        var envase = item.envase;
        var fechaPreferente = item.fechaPreferenteEntrega;
        var bloqueo = false;
        
        var f=new Date();
        var horas = f.getHours();
        var numDias = 0;
        var material;
        var pais = 'ES';
        	
        debugger;
        if (fechaPreferente == null){
            inputCmp.set("v.errors", [{message:"Es obligatorio" }]);
            bloqueo = true;
        }
        else{
            //	Comprobamos el incoterm
            if (incoterm == 'FCA'){
                //	Comprobamos si es antes de las 12
                if (horas < 12 ){
                    //	Comprobamos el material
                    if ($A.get("$Label.c.RQO_lbl_gradosEspecial").includes(item.sku)){
                        numDias = 1;
                        bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                    }
                    else{
                        numDias = 1;
                        bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);                     
                    }
                }
                else{
                    //	Comprobamos el material
                    if (true){
                        numDias = 1;                      
                        bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                    }
                    else{
                        numDias = 1;
                        bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                    }
                }
            }
            else{
                //	Comprobamos el destino
                if (pais == 'ES'){
                    //	Comprobamos si es antes de las 12
                    if (horas < 12 ){
                        //	Comprobamos el material
                        if (true){
                            numDias = 3;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                        else{
                            numDias = 3;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                    }
                    else{
                        //	Comprobamos el material
                        if (true){
                             numDias = 4;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                        else{
                             numDias = 4;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                    }
                }
                else{
                    //	Comprobamos si es antes de las 12
                    if (horas < 12 ){
                        //	Comprobamos el material
                        if (true){
                            numDias = 5;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                        else{
                            numDias = 4;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                    }
                    else{
                        //	Comprobamos el material
                        if (true){
                            numDias = 6;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                        else{
                            numDias = 5;
                            bloqueo = this.comprobacionFecha(numDias, fechaPreferente, item);
                        }
                    }
                }
            }
        }                
        return bloqueo;
    },
    
    comprobacionFecha : function(numDias, fechaPreferente, item){
        debugger;
        var fecha = new Date ();
        
        var fechaIntroducida = new Date (fechaPreferente);
        var fechaValidar =  new Date (fecha.setDate(fecha.getDate() + numDias));
        
        if( fechaValidar > fechaIntroducida) {            
            item.errorFecha = 'La fecha introducida tiene que ser ' + numDias + ' dias mayor a la fecha actual';
            return true;
        }   
        else{
            item.errorFecha = '';
            return false;
        }
    },
    
    duplicar : function(action, elemDuplicar, cantidadDuplicar){
        debugger;
        
        action.setParams({ idPosicion : elemDuplicar.idAsset, cantidadDuplicar: cantidadDuplicar});
        action.setCallback(this, function(response){
            var state = response.getState();
            debugger;
            console.log('ACL. Actualizar Posiciones ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {                             
                
                $A.get('e.force:refreshView').fire();
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }    
        });
        $A.enqueueAction(action);
    },
       
    mostrarGuardar : function(component){
        var editado = component.get("v.editado");
        if (!editado){
            component.set("v.editado", true);
            component.set("v.bloqueoSiguienteHeredado", true);
        }
    },
    guardar : function(component) {
        var listTabla = component.get("v.listPosicionPedidoTable");
        
        var action = component.get('c.actualizarPosiciones');
        action.setParams({ listAlmacenar : JSON.stringify(listTabla)});
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('ACL. Actualizar Posiciones ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                var listVacia = []                
                component.set("v.editado", false);
                component.set("v.bloqueoSiguienteHeredado", false);
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }    
        });
        $A.enqueueAction(action);
        debugger;
    },
    toggleTabla : function(component, event, helper) {
        var element = event.currentTarget;
        jQuery(element).addClass('this-line');
        
        //Si ya estaba desplegado
        if(jQuery('.this-line').closest('tr').children('td:not(:nth-child(-n+4))').hasClass('rq-table-block'))
        {
            
            jQuery('.this-line').closest('tr').children('td.rq-table-block').slideUp(0, function() {
                jQuery('.this-line').closest('tr').children('td:not(:nth-child(-n+4))').removeClass('rq-table-block');
            });
        }
        else{
            //si esta oculto
            jQuery('.this-line').closest('tr').children('td:not(:nth-child(-n+4))').addClass('rq-table-block');
            jQuery('td:last-child').removeClass('rq-table-block');
            jQuery('.rq-table-block').slideDown();
        }
        jQuery(element).removeClass('this-line');
    }
})