({
    onRender : function(component, event, helper) {
		helper.createGraphic(component, event);
    }, setViewGraphic : function(component, event, helper) {
		helper.setViewGraphic(component, event);
	}, createGroupedGraphic : function(component, event, helper) {
		helper.createGroupedGraphic(component, event);
    }
})