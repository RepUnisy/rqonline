({
    adjuntar : function(component, event, helper) {
        debugger;
        var listaArchivos = component.get('v.FileList');
    	var listaNombres = component.get('v.listaNombres');
        var listaContents = component.get('v.listaContents');
    	
        for(var i=0; i<listaArchivos.length; i++){
            var file = listaArchivos[i];
            listaNombres.push(file.name);
            component.set("v.listaNombres", listaNombres);
            debugger;
            this.getFileContent(listaContents, file);
        }
    },
    
    getFileContent: function(listaContents, file) {
        var fr = new FileReader();
        
        var self = this;
       	fr.onload = function() {
            var fileContents = fr.result;
    	    var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
 
            fileContents = fileContents.substring(dataStart);
        
    	    listaContents.push(encodeURIComponent(fileContents));
        };
 
        fr.readAsDataURL(file);
    },
    
    save: function(component, event, helper, asunto, body, correos) {
        debugger;
        
        var listaArchivos = component.get('v.FileList');
    	var listaNombres = component.get('v.listaNombres');
        var listaContents = component.get('v.listaContents');
        var cadena = ['prueba'];
        
        debugger;
        var action = component.get("c.saveTheFile");
        action.setParams({
            fileNames: listaNombres,
            base64Datas: listaContents,
            asunto: asunto,
            body: body,
            direcciones: correos
        });
 
        action.setCallback(this, function(a) {
            debugger;
            var valor = a.getReturnValue();
            console.log(valor);
        });
        
        $A.enqueueAction(action);
    }
})