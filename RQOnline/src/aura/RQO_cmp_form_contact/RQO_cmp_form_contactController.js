({
    validaciones :function(component, event, helper) {
        debugger;
     	var idComponente = event.getParam("idComponente");
        var valorSeleccionado = event.getParam("valorSeleccionado");
        
        if(idComponente === 'PerfilNamePublico' && (valorSeleccionado === "0"  || valorSeleccionado === "1"  ||valorSeleccionado === "2")){
            component.find('inputNombreEmpresaPublico').set("v.required", true);
            //añadir pattern 
        }else if(idComponente === 'PerfilNamePublico' && valorSeleccionado !== "0"  && valorSeleccionado !==  "1"   && valorSeleccionado !==  "2"){
            component.find('inputNombreEmpresaPublico').set("v.required", false);
        }
        
       /*if(idComponente === 'TemaNamePublico' && (valorSeleccionado === "1"  || valorSeleccionado === "2"){
            component.find('prueba').set("v.required", true);
            //añadir pattern 
        }else if(idComponente === 'TemaNamePublico' && valorSeleccionado !== "1"  && valorSeleccionado !==  "2"){
            component.find('prueba').set("v.required", false);
        }*/
        
	},
    getInfo : function(component, event, helper) {        
        /*component.set("v.language", event.getParam("language"));
        component.set("v.isPrivate", event.getParam("user").isPrivate);
        debugger;*/
	},
    toggleArco:function(component, event, helper) {
     	jQuery('.rq-tooltip-arco').slideToggle();
	},
    toggleTelefono:function(component, event, helper) {
     	jQuery('.rq-tooltip-telefono').slideToggle();
	},
	siguiente : function(component, event, helper) {
        helper.adjuntar(component, event, helper);
	},
    guardar : function(component, event, helper) {
        debugger;
        var correos = ['Francisco.DelNido@unisys.com'];
        var body = '<html><body>Estimado '+ component.get('v.SrValores')[component.get('v.SrSelected')] +' '+String(component.get('v.Nombre'))+' '+String(component.get('v.Apellidos'))+' de la empresa: '+String(component.get('v.NombreEmpresa'))+'&nbsp;&nbsp;&nbsp;'+
            'Su mensaje es el siguiente:</br>'+
            String(component.get('v.Mensaje'))+'&nbsp;&nbsp;&nbsp;'+
            'Un saludo.</body></html>';
        	debugger;
        helper.save(component, event, helper, component.get('v.Asunto'), body, correos);
	},
    quitarArchivo : function(component, event, helper) {
        var nombreArchivo = event.currentTarget.dataset.value;
        var indice;
        var lista = component.get('v.FileList');
        var lista2 = [];
        
        for(var i=0; i<lista.length; i++){
            if(lista[i].name != nombreArchivo){
                lista2.push(lista[i]);
            }
        }
        component.set("v.FileList", lista2);
	}
})