({
	incrementar : function(component, event, helper) {
		var numero = component.get("v.numero");
        component.set("v.numero", numero+1);
	},
    handleApplicationEvent : function(component, event, helper) {
        var message = event.getParam("message");
        component.set("v.numero", message);
    }
})