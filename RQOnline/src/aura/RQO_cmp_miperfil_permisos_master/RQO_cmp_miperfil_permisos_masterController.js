({
	doInit : function(component, event, helper) {
		//	Obtenemos listado de usuario
        var action = component.get("c.selectUsersByAccountId");
        action.setParams({
            idAccount : component.get("v.user.cliente.Id")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var users = response.getReturnValue();
                component.set("v.users", users);
            }
        });
        
        $A.enqueueAction(action);
        
        // Obtener listado de perfiles
        var action = component.get("c.selectAllProfiles");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var profiles = response.getReturnValue();
                component.set("v.perfiles", profiles);
            }
        });
        
        $A.enqueueAction(action);
        
        // Obtener listado de permisos
        var action = component.get("c.selectAllPermissions");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var permissions = response.getReturnValue();
                component.set("v.permisos", permissions);
            }
        });
        
        $A.enqueueAction(action);
    },
    showHelp : function(component, event, helper) {
    	var help = component.find("divHelp");        
        $A.util.toggleClass(help, "slds-show");
    },
    showAdd : function(component, event, helper) {
    	component.set("v.showAdd", true);
    },
    showInfo : function(component, event, helper) {
    	component.set("v.showInfo", true);
    },
    filter : function(component, event, helper) {
    	// Filtrar por nombre
    	var filter = component.find("inFilter").get("v.value");	
    	if (filter.length == 0 || filter.length >= 3)
    		helper.filter(component, filter);
    },
    save : function(component, event, helper) {
    	// Actualizar listado de perfiles
    	var users = component.get("v.users");
        helper.save(component, users);
    },
})