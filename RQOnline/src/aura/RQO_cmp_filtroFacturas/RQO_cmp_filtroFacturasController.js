({
        
    advanceSearch : function(component, event, helper) {
        jQuery('.rq-search-advance').toggleClass('open fa-angle-down fa-angle-up');
        
        jQuery('.rq-input-inf').slideToggle();
    },
     doInit : function(component, event, helper) {
		helper.getInfoHerencia(component, event, helper);
        helper.doInit(component, event);
	},
    changeUser : function(component, event, helper) {
        helper.getTipoFacturas(component, event);
	},    
    buscar : function(component, event, helper) {
        helper.buscar(component, event);
    },
	limpiarBusqueda : function(component, event, helper) {
	    component.set("v.nFactura", null);
	    component.set("v.tipoFactura", null);
	    component.set("v.estado", null);
	    component.set("v.grado", null);
        component.set("v.nAlbaran", null);
	    component.set("v.nPedido", null);
	    component.set("v.fechaVencimientoDesde", null);
	    component.set("v.fechaVencimientoHasta", null);
               
        var addMonths = component.get("v.addMonths");
        var date = new Date();
        component.set("v.fechaEmisionHasta", date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? "" + (date.getMonth() + 1): "0" + (date.getMonth() + 1)) + "-" + date.getDate());
        date.setMonth(date.getMonth() - addMonths);
        component.set("v.fechaEmisionDesde", date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? "" + (date.getMonth() + 1): "0" + (date.getMonth() + 1)) + "-" + date.getDate());
        
        
        component.find("selectionDestMercancias").set("v.value", "");
        
    }, preventTyping : function(component, event, helper){
        var dp = component.find('dateFromValue');
        var valorActual = dp.getValue();
    	dp.set('v.value', valorActual);
    }
})