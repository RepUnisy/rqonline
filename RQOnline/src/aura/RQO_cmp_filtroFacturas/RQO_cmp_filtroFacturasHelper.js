({	
    doInit : function(component, event) {
        var busquedaFactura = false;
        //Comprobamos con una expresion regular si se han pasado parámetros por la url y en ese caso se establecen en el filtro
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                                                 function(m,key,value) {
                                                     console.log(key + ":"+value);
                                                     if(key=="numfact"){
                                                         busquedaFactura = true;
                                                         component.set("v.nFactura", value);
                                                     }
                                                 });
        if (!busquedaFactura){
           var action = component.get('c.getDateParametrizedData');
            action.setBackground();
            action.setAbortable();
            action.setCallback(this, function(response) {
                var state = response.getState();
                var addMonths = 3; 
                if (state === "SUCCESS") {
                    addMonths = response.getReturnValue();
                }
                component.set("v.addMonths", addMonths);
                var date = new Date();
                component.set("v.fechaEmisionHasta", date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? "" + (date.getMonth() + 1): "0" + (date.getMonth() + 1)) + "-" + date.getDate());
                date.setMonth(date.getMonth() - addMonths);
                component.set("v.fechaEmisionDesde", date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? "" + (date.getMonth() + 1): "0" + (date.getMonth() + 1)) + "-" + date.getDate());
                
            }); 
            // enqueue the Action   
            $A.enqueueAction(action);
		}
	},
    getTipoFacturas : function(component, event) {
        var action = component.get('c.getTipoFacturas');
        action.setBackground();
        action.setAbortable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                 var tiposFactura = JSON.parse(response.getReturnValue());   
                component.set("v.listTiposFacturas", tiposFactura);
            
               debugger;
            }
        }); 
        // enqueue the Action   
        $A.enqueueAction(action);
    },
    buscar : function(component, event){
        var validado = true;
        component.find("dateFromValue").set("v.errors", null);
        component.find("dateToValue").set("v.errors", null);
        var nFactura = component.get("v.nFactura");
        var tipoFactura = component.get("v.tipoFactura");
        var estado = component.get("v.estado");
        var fechaVencimientoDesde = component.get("v.fechaVencimientoDesde");
        var fechaVencimientoHasta = component.get("v.fechaVencimientoHasta");
        var grado = component.get("v.grado");
        var nAlbaran = component.get("v.nAlbaran");
        var nPedido = component.get("v.nPedido");
        var fechaEmisionDesde = component.get("v.fechaEmisionDesde");
        var fechaEmisionHasta = component.get("v.fechaEmisionHasta");
        var destinoMercancias = component.find("selectionDestMercancias").get("v.value");
 		//Validamos que rellene al menos un campo de búsqueda
        if (!nFactura && (!tipoFactura || tipoFactura == 'null') && (!estado || estado == 'null') && !fechaVencimientoDesde && !fechaVencimientoHasta && !grado && !nAlbaran
           && !nPedido && !fechaEmisionDesde && !fechaEmisionHasta && !destinoMercancias){
            this.fireAlertEvent($A.get("$Label.c.RQO_lbl_seleccionFiltroBusqueda"), $A.get("$Label.c.RQO_lbl_informacion"));
            return false;
        }
        var validado = this.validaciones(component);
        if (!validado){return;}
        
        if(validado){
            if (nFactura){
                nFactura = nFactura.padStart(10, "0");    
            }else{
                nFactura = null;
            }
            if (!tipoFactura || tipoFactura == "null"){tipoFactura = null;}
            if (!estado || estado == "null"){estado = null;}
            if (!fechaVencimientoDesde){fechaVencimientoDesde = null;}
            if (!fechaVencimientoHasta){fechaVencimientoHasta = null;}
            if (!grado){grado = null;}
            if (nAlbaran){
                nAlbaran = nAlbaran.padStart(10, "0");    
            }else{
                nAlbaran = null;
            }
			if (nPedido){
                nPedido = nPedido.padStart(10, "0");    
            }else{
                nPedido = null;
            }
            if (!fechaEmisionDesde){fechaEmisionDesde = null;}
            if (!fechaEmisionHasta){fechaEmisionHasta = null;}
            if (!destinoMercancias){destinoMercancias = null;}

            var appEvent = $A.get("e.c:RQO_evt_searchOrder");
            appEvent.setParams({"numFactura" : nFactura, "tipoFactura" : tipoFactura, "estado" : estado, 
            "dateFrom" : fechaEmisionDesde, "dateTo" : fechaEmisionHasta, "grado" : grado, "numAlbaran" : nAlbaran, "pedido" : nPedido, 
                                "dateEmisionFrom" : fechaVencimientoDesde, "dateEmisionTo" : fechaVencimientoHasta, "destino" : destinoMercancias});
            try{
                appEvent.fire();
            }catch(err){
                console.log(err.message);
            }
		}
    }, fireAlertEvent : function(textAlert, titleAlertValue){
        
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : textAlert, "confirmAlert" : false, 
                            "titleAlertValue" : titleAlertValue, "idAlert" : 'searchConsumption', "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }, validaciones : function(component){
        var validado = true;
        //Vamos a validar las fechas de emisión
		var inputDateFrom = component.find("dateFromValue");
        var value = inputDateFrom.get("v.value");
		var inputDateTo= component.find("dateToValue");
        var valueTo = inputDateTo.get("v.value");
		var addMonths = component.get("v.addMonths");

		if (!value){
            inputDateFrom.set("v.errors", [{message: $A.get("$Label.c.RQO_lbl_introducirFechaDesde")}]);
            return false;
        }
		if (!valueTo){
			inputDateTo.set("v.errors", [{message: $A.get("$Label.c.RQO_lbl_introducirFechaHasta")}]);
            return false;
        }
        //Si tengo al menos una fecha rellenada voy a por sus validaciones
        if (value || valueTo){
			validado = this.validarFechas(value, valueTo, addMonths);
        }
        if (!validado){return false;}
		
        //Vamos a validar las fechas de vencimiento
		var inputDateFromVencimiento = component.find("dateFromValueVencimiento");
        var valueVencimiento = inputDateFromVencimiento.get("v.value");
		var inputDateToVencimiento= component.find("dateToValueVencimiento");
        var valueToVencimiento = inputDateToVencimiento.get("v.value");
        //Si tengo al menos una fecha rellenada voy a por sus validaciones
        if (valueVencimiento || valueToVencimiento){
			validado = this.validarFechas(valueVencimiento, valueToVencimiento, addMonths);
        }
        
        return validado;
    }, validarFechas : function(value, valueTo, addMonths){
        //Si alguna de las dos fechas está vacía sacamos el error correspondiente
        if ((value && !valueTo) || (!value && valueTo)) {
            //$A.get("$Label.c.RQO_lbl_mensajeValidacionFecha");
            //$A.get("$Label.c.RQO_lbl_introducirFechaDesde");
            //$A.get("$Label.c.RQO_lbl_introducirFechaHasta");
            var message = $A.get("$Label.c.RQO_lbl_introducirFechaDesde");
            if (!valueTo){message = $A.get("$Label.c.RQO_lbl_introducirFechaHasta");}
            this.fireAlertEvent(message, $A.get("$Label.c.RQO_lbl_informacion"));
            return false;
        }
        
        var fechaDate = new Date(value);
        fechaDate.setHours(0);
        fechaDate.setMinutes(0);
        fechaDate.setSeconds(0);        
        var fechaDateTo = new Date(valueTo);
        fechaDateTo.setHours(0);
        fechaDateTo.setMinutes(0);
        fechaDateTo.setSeconds(0); 
        if (fechaDate > fechaDateTo){
            this.fireAlertEvent($A.get("$Label.c.RQO_lbl_mensajeValidacionFecha"), $A.get("$Label.c.RQO_lbl_informacion"));
			return false;
        }
       	var mes = fechaDateTo.getMonth()-addMonths;
        fechaDateTo.setMonth(mes);
        if (fechaDate < fechaDateTo){
			var mensajeRango = $A.get("$Label.c.RQO_lbl_mensajeFechaHistorico");
            if (mensajeRango){mensajeRango = mensajeRango.replace('<PARAM>', addMonths);}
            this.fireAlertEvent(mensajeRango,  $A.get("$Label.c.RQO_lbl_informacion"));
            return false;
        }
        
        return true;
	}
})