({
    enviarCorreo : function(component, event) {
        var pregunta = component.find("pregunta").get("v.value");
      //  var copiaMail = component.find("copiaMail").get("v.value");
        
        var action = component.get('c.enviarCorreo');
        action.setParams({ "subject" : pregunta, "body" : pregunta });
        action.setCallback(this, function(response) {
            //store response state 
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('Tu correo se ha enviado')
             //   var respuesta = response.getReturnValue();
               // component.set('v.listCategorias', respuesta);
            }
        });  
        // enqueue the Action   
        $A.enqueueAction(action);
    }
})