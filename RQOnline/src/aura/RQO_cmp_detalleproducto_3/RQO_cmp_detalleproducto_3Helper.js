({
	doInit : function(component, event) {
        
		var language = component.get("v.language");  
        var idgrado = component.get("v.idGrado");  
        
        var action = component.get('c.getDetalleGradoCaracteristicas');    
        action.setParams({ "lenguaje" : language, "idgrado" : idgrado });
        action.setCallback(this, function(response) {
            //store response state 
            var state = response.getState();
            console.log('ACL. state ' + state)
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();       
                
                var listSolicitudes = respuesta[$A.get("$Label.c.RQO_lbl_Segmentos")];
                var listAplicaciones = respuesta[$A.get("$Label.c.RQO_lbl_Aplicaciones")];
                
                console.log('ACL. Solicitudes ' + listSolicitudes)
				component.set('v.listSegmentacion', listSolicitudes);                                	
                component.set('v.listAplicaciones', listAplicaciones);
            }
        });  
        // enqueue the Action   
        $A.enqueueAction(action);
	}
})