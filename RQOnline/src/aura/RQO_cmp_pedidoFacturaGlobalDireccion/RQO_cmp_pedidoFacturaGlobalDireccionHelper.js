({
	guardar : function(component) {
        //	Obtenemos los elementos que vamos a almacenar
        var mapElementosAlmacenar = component.get("v.mapElementosAlmacenar");
        var mapDestinoMercancias = component.get("v.mapDestinoMercancias");
        var mapDireccionesFacturacion = component.get("v.mapaDirFacturacionHeredado");
        var listadoActualizado = [];
        for (var [key, value] of mapElementosAlmacenar) {
            var direccionFacturacion = mapDireccionesFacturacion.get(parseInt(value));
            var listadoPosicionesPedido = mapDestinoMercancias.get(key);
            for (var i= 0; i< listadoPosicionesPedido.length ; i++) {
                listadoPosicionesPedido[i].direccionFacturacion = direccionFacturacion.Id;
                listadoActualizado.push(listadoPosicionesPedido[i]);
            }
        }
        debugger;
        
        var action = component.get('c.assignDireccionFacturacionParcial');
        action.setParams({ listPedidoModificado : JSON.stringify(listadoActualizado)});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('ACL. Almacenar ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {   
                component.set("v.isEditado", false);
                
                var analyticsInteraction = $A.get("e.forceCommunity:analyticsInteraction");
                analyticsInteraction.setParams({
                    hitType : 'event',
                    eventCategory : 'Button',
                    eventAction : 'click',
                    eventLabel : 'Evento constan',
                    eventValue: 200
                });
                analyticsInteraction.fire();
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }    
        });
        $A.enqueueAction(action);
        debugger;
	}
})