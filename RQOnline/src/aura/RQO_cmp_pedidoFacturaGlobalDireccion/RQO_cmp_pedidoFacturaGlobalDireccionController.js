({
    getListEnvio : function(component, event, helper) {
        //	Creamos las opciones de 
        var mapFactu = component.get("v.mapaDirFacturacionHeredado");
        var mapOpciones = new Map();
        
        if (mapFactu != null){
            for (var [key, value] of mapFactu) {                
                mapOpciones.set(value.Id, value.Name  + ': ' + value.calle + ', ' + value.poblacion + ', ' + value.pais);
            }
            component.set("v.opcionesDirFacturacion", mapOpciones);

             debugger;
        }
        
        
        var listadoPedido = component.get("v.listPosicionPedidoHeredado");
        var listDirEnvioCode = [];
        var listDirEnvio = [];
        var mapDirEnvio = new Map();
        
        for (var i  = 0; i < listadoPedido.length; i++) {
            //	El mapa contendrá las como clave la dirEnvio y el incoterm
            var code = listadoPedido[i].dirEnvio + listadoPedido[i].incoterm;
            if (mapDirEnvio.has(code)){
                //	Añadimos la posición al listado
                var listAlmacenada = mapDirEnvio.get(code);
                debugger;
                listAlmacenada.push(listadoPedido[i]);
                debugger;
            }
            else{
                //	Insertamos un nuevo registro en el mapa
                var listDestMercan = [];
                listDestMercan.push(listadoPedido[i]);
                mapDirEnvio.set(code, listDestMercan);
            }
        }
                
        component.set("v.mapDestinoMercancias", mapDirEnvio);
        component.set("v.listDirEnvio", Array.from( mapDirEnvio.keys()));
        
        
        
        
        debugger;
    },
    
    cambiosAlmacena : function (component, even, helper){
        var mapElementosAlmacenar = component.get("v.mapElementosAlmacenar");
        
        if (mapElementosAlmacenar.size != 0){
            component.set("v.isEditado", true);
        }
        else{
            component.set("v.isEditado", false);
        }
        debugger;     
    },
    
    guardar : function (component, even, helper){
        var guardamos = component.get("v.guardarHeredado");   
        if (guardamos){
            var isEditado = component.get("v.isEditado");
            if (isEditado){
                helper.guardar(component);
            }   
        }        
    }
})