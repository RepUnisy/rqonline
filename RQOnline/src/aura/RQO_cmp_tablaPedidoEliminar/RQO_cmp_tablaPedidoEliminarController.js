({
    init: function(component, event, helper){
        
    },
    mostrarMensaje : function(component, event, helper) {
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_mensajeEliminarGrado"), 
                            "confirmAlert" : true, 
                            "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"),                                     
                            "parentLE" : false});
        try{
            appEvent.fire();
        }catch(err){
            this.spinner(component);
            console.log(err.message);
        }  
    },
	openInsertar : function(component, event, helper) {
        
        var listado = component.get("v.listEliminarHeredado");
		var listaVolverInsertar = component.get("v.listInsertadosHeredado");       
        var id = event.getSource().get("v.name");
        debugger;
           
        
        var elementoEncontrado =  listado[id];
        
        listaVolverInsertar.push(elementoEncontrado);
        var index = listado.indexOf(elementoEncontrado);
        listado.splice(index, 1);
        debugger;
        
        //	Actualizamos los listados
        component.set("v.listInsertadosHeredado", listaVolverInsertar);
        component.set("v.listEliminarHeredado", listado);    
        debugger;
	},
    
    handleResponseAlert : function(component, event, helper){
        var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");
        if(responseAlert){
            helper.eliminar(component);
        }        
    },
    
    toggleTable : function(component, event, helper) {
		helper.toggleTabla(component, event);
	}
})