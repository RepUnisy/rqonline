({
    eliminar : function(component) {
        //	Obtenemos los elementos que vamos a almacenar
        var listEliminar = component.get("v.listEliminarHeredado");
        var action = component.get('c.eliminarPosiciones');
        
        for(var i = 0; i<listEliminar.length; i++){
            listEliminar[i].listEnvases = null;
            listEliminar[i].listUnidadMedida = null;
        }
        action.setParams({ listEliminar : JSON.stringify(listEliminar)});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
               	var listVacia = []
                component.set("v.listEliminarHeredado", listVacia);
                var listInsertados = component.get("v.listInsertadosHeredado");
                if (listInsertados.length == 0){
                    component.set("v.idSolicitudHeredado", null);
                }
                
                var eventoNotificaciones = $A.get("e.c:RQO_evt_sumarUnoANotificaciones");
                eventoNotificaciones.setParams({"numSumar": 0});
                eventoNotificaciones.fire();
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }    
        });
        $A.enqueueAction(action);
    },
    
    
    toggleTabla : function(component,event) {
	 if(window.matchMedia("(max-width:977px)").matches)
         {
            var element = event.currentTarget;
            jQuery(element).addClass('thisdelete');
            jQuery('.thisdelete').closest('table').addClass('table-delete-active');
             
            //Si ya estaba desplegado
            if(jQuery('.thisdelete').closest('tr').children('td:not(:nth-child(-n+2))').hasClass('rq-table-block'))
            {
                
                jQuery('.thisdelete').closest('tr').children('td.rq-table-block').slideUp(0, function() {
                    jQuery('.thisdelete').closest('tr').children('td:not(:nth-child(-n+2))').removeClass('rq-table-block');
               		jQuery('.thisdelete').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                    
                });
            }
            else{
            //si esta oculto
                jQuery('.thisdelete').closest('tr').children('td:not(:nth-child(-n+2))').addClass('rq-table-block');
                jQuery('.thisdelete').addClass('fa-chevron-up').removeClass('fa-chevron-down');
                jQuery('td:last-child').removeClass('rq-table-block');
                jQuery('.rq-table-block').slideDown('rq-table-block');
            }
             
            jQuery(element).closest('table').removeClass('table-delete-active');
            jQuery(element).removeClass('thisdelete');
           
        }
    
	}
})