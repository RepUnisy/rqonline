({
	fetchMapCtrl : function(component, event) {
        // /*********NO BORRAR**********/
        // No borrar estos comentario bajo ninguna circunstancia. Precargan los labels. 
        // https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/labels_dynamic.htm
		// $Label.c.RQO_lbl_IdiomaSpa
		/***************************/
        debugger;
		var action = component.get('c.getMapCategorias');
        var stored = component.get('v.languageHeredado');
        var user = component.get('v.user');
        //var stored = localStorage['idioma'];
        if(stored){
            action.setParams({ "lenguaje" : stored });
            console.log('Entra en el stored.');
            console.log('Esto es lo que está almacenado: ' + localStorage['idioma']);
        }
        else{
            action.setParams({ "lenguaje" : $A.get("$Label.c.RQO_lbl_IdiomaSpa") });
            localStorage['idioma'] = $A.get("$Label.c.RQO_lbl_IdiomaSpa");
           
            console.log('NO Entra en el stored.');
        }
        action.setCallback(this, function(response) {
            //store response state 
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
                // create a empty array for store map keys 
                var arrayOfMapKeys = [];
                // store the response of apex controller (return map)     
                var StoreResponse = JSON.parse(response.getReturnValue());
                console.log('StoreResponse' + StoreResponse);
                // set the store response[map] to component attribute, which name is map and type is map.   
                component.set('v.fullMap', StoreResponse);
 
                for (var singlekey in StoreResponse) {
                    arrayOfMapKeys.push(singlekey);
                }
                // Set the all list of keys on component attribute, which name is lstKey and type is list.     
                component.set('v.lstKey', arrayOfMapKeys);
                console.log('Llega al final del Externo.');
            }
        });        
        
        // enqueue the Action   
        $A.enqueueAction(action);

	},
    recogeEventofetchMapCtrl : function(component, event) {
        console.log('Llega al inicio de la recogida del Evento en el Externo.');
        var idioma = event.getParam("idioma");
        component.set("v.idioma", idioma);
        var action = component.get('c.getMapCategorias');   
        action.setParams({ "lenguaje" : idioma });
        action.setCallback(this, function(response) {
            //store response state 
            var state = response.getState();
            if (state === "SUCCESS") {
                // create a empty array for store map keys 
                var arrayOfMapKeys = [];
                // store the response of apex controller (return map)     
                var StoreResponse = JSON.parse(response.getReturnValue());
                console.log('StoreResponse' + StoreResponse);
                // set the store response[map] to component attribute, which name is map and type is map.   
                component.set('v.fullMap', StoreResponse);
 
                for (var singlekey in StoreResponse) {
                    arrayOfMapKeys.push(singlekey);
                }
                // Set the all list of keys on component attribute, which name is lstKey and type is list.     
                component.set('v.lstKey', arrayOfMapKeys);
                console.log('Llega al final del Externo.');
            }
        });
        // enqueue the Action   
        $A.enqueueAction(action);
        console.log('Llega al final de la recogida del Evento en el Externo. ' + idioma);
    }
})