({
    doInit : function(component, event, helper) {
        var opciones = component.get("v.listOpcionesHeredada");
        var posicionSeleccionada = component.get("v.posicionSeleccionadaHeredada");
        var valor = component.get("v.valorHeredada");
        var valores = [];        
        for(var i=0 ; i<opciones.length; i++){
            valores.push({value: i, label: opciones[i]});
        }
        if (valores[posicionSeleccionada] != null){
            valor = valores[posicionSeleccionada].value;
        }
        
        component.set("v.listOpcionesHeredadaMod", valores);
        component.set("v.valorHeredada", String(valor));
    },
    valorHered : function(component, event, helper) {
        if(component.get("v.esDependiente") == true){
            var opciones = component.get("v.listOpcionesHeredada");
            var posicionSeleccionada = component.get("v.posicionSeleccionadaHeredada");
            var valor = component.get("v.valorHeredada");
            var valores = [];        
            for(var i=0 ; i<opciones.length; i++){
                valores.push({value: i, label: opciones[i]});
            }
            if (valores[posicionSeleccionada] != null){
                valor = valores[posicionSeleccionada].value;
            }
            
            component.set("v.listOpcionesHeredadaMod", valores);
            component.set("v.valorHeredada", String(valor));
        }
    },
    pintaValor : function(component, event, helper) {
        if(component.get("v.esDependiente") == true){
            var opciones = component.get("v.listOpcionesHeredada");
            var posicionSeleccionada = component.get("v.posicionSeleccionadaHeredada");
            var valor = component.get("v.valorHeredada");
            var valores = [];        
            for(var i=0 ; i<opciones.length; i++){
                valores.push({value: i, label: opciones[i]});
            }
            if (valores[posicionSeleccionada] != null){
                valor = valores[posicionSeleccionada].value;
            }
            
            component.set("v.listOpcionesHeredadaMod", valores);
            component.set("v.valorHeredada", String(valor));
        }
    },
    
    cambio : function(component, event, helper) {
        console.log('Select modificado a: ' + component.get("v.valorHeredada"))
        var selectEvento = component.getEvent("selectEvento");
        selectEvento.setParams({
            "idComponente" : component.get("v.name"),
            "valorSeleccionado" : component.get("v.valorHeredada"),
            "parametro" : component.get("v.parametro"),});
        
        selectEvento.fire();
    }
})