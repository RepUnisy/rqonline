({
    myAction : function(component, event, helper) {
        
    },
    doInit : function(component, event, helper) {

    },
    menuDown : function(component, event, helper) {
        var element = event.currentTarget;
        jQuery(element).toggleClass('menu-visible');
        jQuery('.menu-down').siblings().slideToggle();
    },
    goBack : function(component, event, helper) {
        if(window.matchMedia("(max-width:767px)").matches)
        {
            jQuery('.goback').parent().parent().css({"display": "none"});
        }
    },
    enlaceExterno : function(component, event, helper) {
        var source = event.getSource();
        var label = source.get("v.label");
        console.log('ACL. ' + label)
        var url;
        
        if (label == $A.get("$Label.c.RQO_lbl_laQuimicadeRepsol")){
            url = $A.get("$Label.c.RQO_lbl_repsolQuimicaUrlExterna"); 
        }
        if (label == $A.get("$Label.c.RQO_lbl_agricultura")){
            url = $A.get("$Label.c.RQO_lbl_agriculturaUrlExterna"); 
        }
        
        if (label == $A.get("$Label.c.RQO_lbl_automocion")){
                url = $A.get("$Label.c.RQO_lbl_automocionUrlExterna"); 
            }
        if (label == $A.get("$Label.c.RQO_lbl_automocion")){
                url = $A.get("$Label.c.RQO_lbl_automocionUrlExterna"); 
            }
        if (label == $A.get("$Label.c.RQO_lbl_bienestarConsumo")){
                url = $A.get("$Label.c.RQO_lbl_bienestarUrlExterna");
                }
        if (label == $A.get("$Label.c.RQO_lbl_contruccionInfraestructuras")){
                    url = $A.get("$Label.c.RQO_lbl_construccionUrlExterna");
                    }
        
        if (label == $A.get("$Label.c.RQO_lbl_packaging")){
                        url = $A.get("$Label.c.RQO_lbl_envasesUrlExterna");
                        }
        if (label == $A.get("$Label.c.RQO_lbl_salud")){
                            url = $A.get("$Label.c.RQO_lbl_saludUrlExterna");
                            }
        if (label == $A.get("$Label.c.RQO_lbl_salud")){
                                url = $A.get("$Label.c.RQO_lbl_hogar");
                                }
        if (label == $A.get("$Label.c.RQO_lbl_benceno")){
                                    url = $A.get("$Label.c.RQO_lbl_bencenoUrlExterna");
                                    }                    
        if (label == $A.get("$Label.c.RQO_lbl_butadieno")){
                                        url = $A.get("$Label.c.RQO_lbl_butadienoUrlExterna");
                                        }                   
        if (label == $A.get("$Label.c.RQO_lbl_caucho")){
                                            url = $A.get("$Label.c.RQO_lbl_cauchoUrlExterna");
                                            }                             
        if (label == $A.get("$Label.c.RQO_lbl_etileno")){
                                                 url = $A.get("$Label.c.RQO_lbl_etilenoUrlExterna");
                                                 }                            
        if (label == $A.get("$Label.c.RQO_lbl_evaEba")){
                                                     url = $A.get("$Label.c.RQO_lbl_evaebaUrlExterna");
                                                     }                                         
        if (label == $A.get("$Label.c.RQO_lbl_estireno")){
                                                         url = $A.get("$Label.c.RQO_lbl_estirenoUrlExterna");
                                                         }                                        
        if (label == $A.get("$Label.c.RQO_lbl_oxidodePropileno")){
                                                             url = $A.get("$Label.c.RQO_lbl_oxipropilenoUrlExterna");
                                                             }                                                
        if (label == $A.get("$Label.c.RQO_lbl_polietileno")){
                                                              url = $A.get("$Label.c.RQO_lbl_polietilenoUrlExterna");
                                                              }                                               
        if (label == $A.get("$Label.c.RQO_lbl_poliol")){
                                                        url = $A.get("$Label.c.RQO_lbl_poliolUrlExterna");
                                                         }                                                    
        if (label == $A.get("$Label.c.RQO_lbl_polipropileno")){
                                                                url = $A.get("$Label.c.RQO_lbl_polipropilenoUrlExterna");
                                                                }                                                     
        if (label == $A.get("$Label.c.RQO_lbl_propilenglicolIndustrial")){
                                                                      url = $A.get("$Label.c.RQO_lbl_propilenglicolIndustrialUrlExterna");
                                                                      }                                                               
        if (label == $A.get("$Label.c.RQO_lbl_propileno")){
                                                           url = $A.get("$Label.c.RQO_lbl_propilenoUrlExterna");
                                                           }                                                             
        if (label == $A.get("$Label.c.RQO_lbl_innovacionCalidad")){
                                                            url = $A.get("$Label.c.RQO_lbl_innovacionUrlExterna");
                                                            }                                                                       
        if (label == $A.get("$Label.c.RQO_lbl_seguridadMedioAmbiente")){
                                                            url = $A.get("$Label.c.RQO_lbl_seguridadUrlExterna");
                                                            }                                                              
        if (label == $A.get("$Label.c.RQO_lbl_publicacionesCatalogos")){
                                                            url = $A.get("$Label.c.RQO_lbl_publicacionesUrlExterna");
                                                            }                                                                          
        if (label == $A.get("$Label.c.RQO_lbl_quimicaSociedad")){
                                                            url = $A.get("$Label.c.RQO_lbl_quimicaSociedadUrlExterna");
                                                            }
        if (label == $A.get("$Label.c.RQO_lbl_quimicaNews")){
                                                            url = $A.get("$Label.c.RQO_lbl_enlaceQuimicaNews");
                                                            } 
                                                            
            
        
        
        if (url != null){
            //	Vamos a la url seleccionada
            window.location.href = url;
        }
        
        
    },
    
    openChild : function(component, event, helper) {
        if(window.matchMedia("(max-width:767px)").matches)
        {
            var element = event.getSource().get("v.title");
            jQuery("[title ='"+element +"'").children('.rq-nav-dropdown').css({"display": "block", "height" : "100%","top":"0"});
        }
    },
    gotoURL : function (component, event, helper) {
        var prueba = component.get("v.prueba");
        var source = event.getSource();
        var label = source.get("v.label");
        
        var labelCatalogo = $A.get("$Label.c.RQO_lbl_catalogo");
        var labelPedidos = $A.get("$Label.c.RQO_lbl_Pedidos");
        var labelFAQs = $A.get("$Label.c.RQO_lbl_preguntasFrecuentes");
        var labelMiCatalogo = $A.get("$Label.c.RQO_lbl_myCatalogue");
        var labelFormularioContacto = $A.get("$Label.c.RQO_lbl_formulario_de_contacto"); 
        
        var url = "/";
        if (label == labelPedidos){
            //	Almacenamos la información del grado
            /*
            var appEvent = $A.get("e.c:RQO_evt_almacenarInfo");
            appEvent.setParams({
                "infoAlmacenar" : "a2y9E000000CeZD" });
            appEvent.fire();
            */
            
            url = "/grado?id=a2y9E000000CeZD";
        }
        else{
            if(label == labelCatalogo){
                url = "/catalogo"
            }
            else{
                if (label == labelFAQs){
                       url = "/faqs";
                    
                }
                else{
                    if (label == labelMiCatalogo) {
                        url = "/mi-catalogo";
                    }
                    else{
                        if (label == 'Solicitud') {
                            url = "/sp";
                        }
                        else{
                            if (label == labelFormularioContacto){
                                url = "/contacto";
                            }
                        }
                    }
                }
            }
        }
        
        /* 
       var myQueryString=window.location.search;  
       // remove the '?' sign if exists        
       var myQueryString=myQueryString.substr(1, myQueryString.length-1);
       var dateString=myQueryString;
       */
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();        
    },
    
    getInfo : function(component, event, helper) {
        helper.getInfo(component, event);
    }
    
    
})