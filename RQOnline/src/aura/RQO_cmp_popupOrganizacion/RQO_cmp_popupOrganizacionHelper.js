({
    close : function(component, event){
		component.set('v.visible',false);
	},
	getAccounts : function(component, event){
		//	Obtenemos listado de clientes
        var action = component.get("c.getChildAccounts");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var accounts = response.getReturnValue();
                component.set("v.accounts", accounts);
            }
        });
        
        $A.enqueueAction(action);
	},
	changeAccount : function(component, event){
	
		component.set('v.visible',false);
		
		//	Cambiar el cliente
        var action = component.get("c.setUserAccount");
        action.setParams({
            accountId : component.find("slAccounts").get("v.value")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            }
        });
        
        $A.enqueueAction(action);
	},
})