({
    gotoURL : function (component, event, helper) {       
        var source = event.getSource();
        var label = source.get("v.label");
        var url = "/";
        
        //	Inicializamos las url
        switch(label) {
            case $A.get("$Label.c.RQO_lbl_irHome"):
                url = "/hm";
                break;
            case $A.get("$Label.c.RQO_lbl_irCatalogo"):
                url = "/catalogo"
                break;
            case $A.get("$Label.c.RQO_lbl_irHistorialPed"):
                url = "/hp"
                break;
            case $A.get("$Label.c.RQO_lbl_irSeguimientoPed"):
                url = "/segp"
                break;                     
        }
        
        //	Lanzamos el evento para redireccionar
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();        
                 
    }
})