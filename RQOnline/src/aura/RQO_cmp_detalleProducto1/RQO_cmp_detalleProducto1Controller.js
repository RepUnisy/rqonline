({
    doInit : function(component, event, helper) {
        if(!$A.util.isEmpty(component.get("v.id")) && !$A.util.isEmpty(component.get("v.language"))){
        	helper.doInit(component, event);   
        }                
    },
     toggleText : function(component, event, helper){
         if(window.matchMedia("(max-width:767px)").matches)
         {
            var element = component.find('title');
            $A.util.toggleClass(element,'plegado');
            jQuery('.rq-description-text').slideToggle(); 
         }
            
    }
})