({
	setViewGraphicEuros : function(component, event) {
		//Lanzamos evento para visualizar el gráfico de euros. El evento lo captura y propaga el padre
        //RQO_cmp_misConsumosExterno
        var appEvent = component.getEvent("changeViewEuros");
        appEvent.setParams({"viewTableEuros" : false});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}, exportInEuros : function(component, event) {
        //Lanzamos evento para exportar los datos de consumos en euros a CSV. El evento lo captura y propaga el padre
        //RQO_cmp_misConsumosExterno
        var appEvent = $A.get("e.c:RQO_evt_consumptionExport");
        appEvent.setParams({"exportType" : "euros"});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }, inicioEuros : function(component, event) {
        //Evento init, nos posicionamos en la primera página
        this.cambiarPaginaGeneral(component, 1, true, true);
    }, cambiarPaginaEuros : function(component, event, direccion) {
        //Páginación con la flechas
        var pagina = parseInt(event.getSource().get("v.value"));
        this.cambiarPaginaGeneral(component, pagina, direccion, false);
    },cambiarPaginaGeneral : function(component, pagina, direccion, inicial) {
        //Obtenemos el formato donde estamos visualizando y formateamos los elementos visibles - DESKTOP, PHONE, TABLET
        var formFactor = $A.get("$Browser.formFactor");
        switch(formFactor) {
            case 'PHONE':
                component.set("v.numeroElementosVisibles", 1);
                break;
            case 'TABLET':
                component.set("v.numeroElementosVisibles", 2);
                break;
            default:
                component.set("v.numeroElementosVisibles", 3);
        }
        
		//Calculo del número de meses que se muestran por página
		var elementosVisibles =   component.get("v.numeroElementosVisibles");
        component.set('v.paginaActivaEuros',pagina);
        var inicioPaginacion = (pagina * elementosVisibles - elementosVisibles);
        var finPaginacion = (pagina * elementosVisibles);
        var bodyListData = component.get("v.bodyListDataEuros");
        var headerListData = component.get("v.headerListDataEuros");
        if (inicial){
			//Cálculo datos paginación inicial
            var numeroPaginas = parseInt((headerListData.length / elementosVisibles) + 1);
            if(headerListData.length % elementosVisibles == 0){
                numeroPaginas -= 1;
            }
            var listaPaginas = [];
            for (var i = 1; i <= numeroPaginas; i++) {
                listaPaginas.push(i);
            }
            component.set("v.numeroPaginasEuros", listaPaginas);
            if(listaPaginas.length <= 1){
                component.set("v.mostrarPaginacionEuros", false);
            }else{
                component.set("v.mostrarPaginacionEuros", true);
            }
            component.set("v.paginaActivaEuros", 1);
        }
        component.set("v.inicioPaginacionEuros", inicioPaginacion);
        component.set("v.finalPaginacionEuros", finPaginacion);
        this.paginacionEuros(component, headerListData, bodyListData, inicioPaginacion, finPaginacion, pagina, direccion);
    }, paginacionEuros : function(component, listaHeader, listaBody, inicioPaginacion, finPaginacion, pagina, direccion){
        component.set("v.parcialHeaderListDataEuros", listaHeader.slice(inicioPaginacion, finPaginacion));
        //Sacamos 4 meses por página pero cada mes puede tener hasta dos años. 
        //Vamos a recuperar el número de años total para la página solicitada para poder pintar los datos correctos
        var listAuxHeader = component.get("v.parcialHeaderListDataEuros");
        var countPaginaSolicitada = 0;
        for (var z = 0; z<listAuxHeader.length;z++){
            countPaginaSolicitada += listAuxHeader[z].yearsCount;
        }
        var inicioPaginacionDatos = 0;
        var finPaginacionDatos = 0;
		var totalActualInicio = component.get("v.numeroDatosPaginaActualInicioEuros");
        var totalActualFin = component.get("v.numeroDatosPaginaActualFinEuros");
         //Primera ejecución
        if (!totalActualInicio && !totalActualFin){
            inicioPaginacionDatos = 0;
            finPaginacionDatos = countPaginaSolicitada;
        }else  if (direccion){
        	//La dirección marca si subimos o bajamos
            //Si estamos subiendo el fin de la paginación será el fin actual más los datos de la pagina solicitada
            finPaginacionDatos = totalActualFin + countPaginaSolicitada;
            //El inicio de la paginación es el fin actual 
            inicioPaginacionDatos = totalActualFin;
        }else{
            //Si estoy restando el fin de la paginación es el dato incial actual
            finPaginacionDatos = totalActualInicio;
            //El inicio es el dato incial actual menos el número de datos solicitado
            inicioPaginacionDatos = totalActualInicio - countPaginaSolicitada;
        }
        component.set("v.numeroDatosPaginaActualFinEuros", finPaginacionDatos);
        component.set("v.numeroDatosPaginaActualInicioEuros", inicioPaginacionDatos);
        this.getParcialListDataEuros(component, listaBody, inicioPaginacionDatos, finPaginacionDatos);
    }, getParcialListDataEuros : function (component, listData, inicioPaginacion, finPaginacion, direccion){
        //Recuperamos la lista parcial (lista paginada) que se muestra por pantalla
        var parcialList = [];
        var parcialobj = null;
        var parcialListData = null;
        for (var i = 0; i< listData.length; i++){   
            parcialListData = listData[i].dataList.slice(inicioPaginacion, finPaginacion);
            //Generamos una lista de objetos nuevos para no tener problemas con la lista principal
            parcialobj = {translatedFamily:listData[i].translatedFamily, dataList: parcialListData};
            parcialList.push(parcialobj);
        }
        component.set("v.parcialBodyListDataEuros", parcialList);
    }
})