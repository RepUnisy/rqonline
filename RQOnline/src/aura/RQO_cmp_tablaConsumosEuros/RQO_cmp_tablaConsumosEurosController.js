({
    setViewGraphicEuros : function(component, event, helper) {
		helper.setViewGraphicEuros(component, event);
	}, exportInEuros : function(component, event, helper) {
        helper.exportInEuros(component, event);
    }, cambiarPaginaEurosArriba : function(component, event, helper) {
		helper.cambiarPaginaEuros(component, event, true);
    }, cambiarPaginaEurosAbajo : function(component, event, helper) {
		helper.cambiarPaginaEuros(component, event, false);
    }, doInitEuros : function(component, event, helper) {
		helper.inicioEuros(component, event);
    }
})