({
    doInit : function(component, event, helper) {
         if ( component.get("v.language") == null){
             helper.getInfoHerencia(component, event, helper);
         }
        helper.getPagina(component, event);
	},
    getPagina : function(component, event, helper) {
        
        helper.getPagina(component, event);
    },
})