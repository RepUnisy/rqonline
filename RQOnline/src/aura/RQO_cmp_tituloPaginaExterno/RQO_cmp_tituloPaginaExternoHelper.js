({
	getPagina : function(component, event) {
        debugger;
        //	$Label.c.RQO_lbl_catalogoProductos
        //	$Label.c.RQO_lbl_informeCompras
        //	$Label.c.RQO_lbl_contacte_con_quimica
        //	$Label.c.RQO_lbl_defaultPage
        //	$Label.c.RQO_lbl_encuestas
        //	$Label.c.RQO_lbl_cabeceraFacturas
        //	$Label.c.RQO_lbl_preguntasFrecuentes
        //	$Label.c.RQO_lbl_ficha_grado
        //	$Label.c.RQO_lbl_historico_de_pedidos
        //	$Label.c.RQO_lbl_myCatalogue
        //	$Label.c.RQO_lbl_notificaciones
        //	$Label.c.RQO_lbl_encabezadoPerfil
        //	$Label.c.RQO_lbl_misPlantillas
        //	$Label.c.RQO_lbl_inicio
        //	$Label.c.RQO_lbl_seguimiento_de_pedidos
        //	$Label.c.RQO_lbl_crearSolicitud
        //	$Label.c.RQO_lbl_comunicacionMultimedia

        
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); 
        var sURLVariables = sPageURL.split('&'); 
        var sParameterName;
        var idGrado = '';
        var language = component.get("v.language");
        for (var i = 0; i < sURLVariables.length; i++) {
            console.log('todo '+ sURLVariables[i]);
            sParameterName = sURLVariables[i].split('='); 
            
            if (sParameterName[0] === 'id') {
                idGrado = sParameterName[1];
            }
        }
        
		var pathname = window.location.pathname;
        var listaStrings = pathname.split('/');
        var pagina = listaStrings[listaStrings.length - 1];
        if(pagina == $A.get("$Label.c.RQO_lbl_grado_const")){
            var actiongrade = component.get('c.getPageNameGrade');
            actiongrade.setBackground();
            //actiongrade.setAbortable();
            actiongrade.setParams({ "nameCustomSetting" : pagina,
                                   "grade" : idGrado, "language" : language});
            debugger;
        	actiongrade.setCallback(this, function(response) {
                var state = response.getState();
                debugger;
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    var nombre = respuesta[0];
                    var imagen = respuesta[1];
                    var categoria = respuesta[2];
                    component.set("v.grado", nombre);   
                    component.set("v.imagen", imagen);
                    component.set("v.categoria", categoria);
                    component.set("v.esGrado", true);
                    component.set("v.pagina", pagina);
                    component.set("v.idGrado", idGrado);
       
                }
                else if (state === "INCOMPLETE") {
                   console.log('Incomplete');
                }
                else if (state === "ERROR") {
                    
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(actiongrade);
        }
        else{
            console.log('BUS. pagina' + pagina);
            var action = component.get('c.getPageName');
            action.setBackground();
            action.setAbortable();
            action.setStorable();
            action.setParams({ "nameCustomSetting" : pagina});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
  
                    var respuesta = response.getReturnValue();
                    var respuestaCero = respuesta[0];
                    var label = $A.get("$Label.c." + respuestaCero);
                    console.log('BUS. label' + label);
                    var imagen = respuesta[1];
                    component.set("v.titulo", label);   
                    component.set("v.imagen", imagen);
                    component.set("v.pagina", pagina);
                    component.set("v.esGrado", false);
                }
                else if (state === "INCOMPLETE") {
                   console.log('Incomplete');
                }
                else if (state === "ERROR") {
                    
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
	},
    
    
    getInfoMod : function(component, event, helper) {
        var language = event.getParam("language");
        component.set("v.language", language);
        var user = event.getParam("user");      
        component.set("v.user", user); 
    }
})