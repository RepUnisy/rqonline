({
	getInfo : function(component, event, helper) {
        var language = event.getParam("language");
        var user = event.getParam("user");
        
        //	Almacenamos la información obtenida
        component.set("v.language", language);
        component.set("v.user", user);
        
        //	LLamamos al helper para obtener la información inicial
        helper.getInitialInfo(component);
    },
	 goTo :function(component, event, helper){
        var url = "/ntf";
          var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();     
    }
})