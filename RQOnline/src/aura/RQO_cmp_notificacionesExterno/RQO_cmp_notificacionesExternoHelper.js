({
	getInitialInfo : function(component) {		
        
        var idContacto = component.get("v.user.idContacto");
        //	Llamamos a la funcion para inicializar las notificacioens 
        this.obtenerNotificaciones(component, idContacto);
        
    },
    obtenerNotificaciones : function(component, idContacto) {

        var action = component.get('c.getLastNotifications');
        action.setParams({ idContacto : idContacto });

        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {

                /*var map = new Map(Object.entries(JSON.parse(response.getReturnValue())));
                                
                var listadoKey = Array.from(map.keys());                
                var myObjectMap = [];
                for(var x=0; x<listadoKey.length; x++) {
                    var temp = { "key": listadoKey[x], "list": map.get(listadoKey[x]) };
                    myObjectMap.push(temp);
                }
                component.set("v.mapaNotificaciones", myObjectMap);*/
                
                var data = JSON.parse(response.getReturnValue());
                component.set("v.dataNotificaciones", data);
                
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);   
    }
})