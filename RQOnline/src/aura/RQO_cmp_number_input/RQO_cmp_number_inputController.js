({
	aumentar : function(component, event, helper) {
		var nuevaCantidad = component.get("v.cantidad") + 1;
        component.set("v.cantidad", nuevaCantidad);
    },
    disminuir : function(component, event, helper) {
        var cantidadActual = component.get("v.cantidad");
        if (cantidadActual > 1){
            var nuevaCantidad = component.get("v.cantidad") - 1;        
            component.set("v.cantidad", nuevaCantidad);
        }       
    },
    editado : function(component, event, helper) {   
        component.set("v.editadoHeredado", true);
    }
})