({       
    
    getInfo : function(component, event) {
        console.log('PT anterior ' + component.get("v.language"))
        var language = event.getParam("language");
        var user = event.getParam("user");
        console.log('PT almacena ' + language)
        component.set("v.language", language);
        component.set("v.user", user);          
    },

    doInit : function (component, event, helper){ 
        console.log('PT solicitud ' + component.get("v.language"))
        if (component.get("v.language") == null){
            helper.init(component);
        } 
    }
    
})