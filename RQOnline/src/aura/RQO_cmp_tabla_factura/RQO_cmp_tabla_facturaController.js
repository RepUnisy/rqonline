({
    /*
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();    
    },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();    
    },
	*/
    
    reportar : function(component, event, helper){
        var url = "/contacto";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();     
    },
    
    busqueda : function(component, event, helper){	      
        helper.busqueda(component, event);
    },
    getInfo : function(component, event, helper){
       
        var language = event.getParam("language");
        var user = event.getParam("user");
        
        component.set("v.language", language);
        component.set("v.user", user);
        debugger;
        
        helper.init(component);
       
    },
    toggleTabla : function(component, event, helper){
    	 helper.toggleTabla(component, event);
	},
    toggleTablaInterior : function(component, event, helper){
    	 helper.toggleTablaInterior(component, event);
	},
   
    descarga : function(component, event, helper){
        debugger;
        var data = component.get("v.listFactura");
        
        // call the helper function which "return" the CSV data as a String   
        var csv = helper.convertArrayOfObjectsToCSV(component);   
        if (csv == null){return;}
        
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; // 
        hiddenElement.download = 'ExportData.csv';  // CSV file Name* you can change it.[only name not .csv] 
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click(); // using click() js function to download csv file
    }
    
})