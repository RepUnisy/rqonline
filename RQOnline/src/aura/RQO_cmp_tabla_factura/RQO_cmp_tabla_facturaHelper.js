({
    
    init : function(component) {
        var fechaActual = new Date();
        debugger;
        var dateFrom = fechaActual.getFullYear() + "-" + (fechaActual.getMonth() - 1) + "-" + fechaActual.getDate();
        var dateTo = fechaActual.getFullYear() + "-" + (fechaActual.getMonth() + 1) + "-" + fechaActual.getDate();
        this.getData(component, null, null, null, dateFrom, dateTo, null, null, null, null, null);
    }, busqueda : function (component, event){
        debugger;
        component.set("v.mostrarSpinner", true);
        component.set("v.error", false);
        component.set("v.vacio", false);
        var numFactura = event.getParam("numFactura");
        var tipoFactura = event.getParam("tipoFactura");
        var estado = event.getParam("estado");
        var dateFrom = event.getParam("dateFrom");
        var dateTo = event.getParam("dateTo");
        var grado = event.getParam("grado");
        var numAlbaran = event.getParam("numAlbaran");
        var numPedido = event.getParam("pedido");
        var dateEmisionFrom = event.getParam("dateEmisionFrom");
        var dateEmisionTo = event.getParam("dateEmisionTo");
        var destino = event.getParam("destino");
        
        debugger;
        this.getData(component, numFactura, tipoFactura, estado, dateFrom, dateTo, grado, numAlbaran, numPedido, dateEmisionFrom, dateEmisionTo, destino);
        
    },
    
    getData : function (component, numFactura, tipoFactura, estado, dateFrom, dateTo, grado, numAlbaran, numPedido, dateEmisionFrom, dateEmisionTo, destino){    	
        
        var usuario = component.get("v.user");
        var idCliente = usuario.idExternoCliente;
        debugger;
        
        
        var action = component.get('c.getFacturas');
        action.setParams({ idCliente : idCliente, numFactura : numFactura, tipoFactura : tipoFactura, estado: estado, dateFrom : dateFrom, dateTo : dateTo, grado : grado, numAlbaran : numAlbaran, 
                          numPedido : numPedido, dateEmisionFrom : dateEmisionFrom, dateEmisionTo : dateEmisionTo, ivNumReg : null, destinoMercancias : destino, isHome : false});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('ACL. Estado getFactura ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                debugger;
                var listFacturas = JSON.parse(response.getReturnValue());
                if (listFacturas.length != 0){
                    if(listFacturas[0].error){
                        component.set("v.error", true);
                    }
                }
                else{
                    component.set("v.vacio", true);
                }
                debugger;
                
                component.set("v.listFactura", listFacturas);                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                } 
            component.set("v.mostrarSpinner", false);
        });
        $A.enqueueAction(action);  
    },
    
    
    convertArrayOfObjectsToCSV : function(component){
		
		//Se recorre la lista de wrappers y la sublista de wrappers de cada objeto principal para pintar los datos básicos y los desglosados (si los tiene)
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        //Obtenemos la lista de registros y verificamos que tiene datos
        var objectRecords = component.get("v.listFactura");
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        
		//Definimos los separadores de columna y linea
        columnDivider = ';';
        lineDivider =  '\n';
        
        //Definimos las keys sobre las que luego buscaremos los datos. Tenemos keys para la lista de facturas y para el desglose de cada una de ellas.
		//Cada key debe coincidir con su atributo del wrapper de pedidos
        keys = ['numFactura','fechaEmision','fechaVencimiento','importe','tipo','estado' ];
        var keysBrokenDown = ['numRefCliente', 'numPedido', 'numAlbaran', 'grado', 'cantidad', 'tipoCantidad', 'precioTonelada', 'importe', 'destino'];
		//Cabecera que se pinta en la excel. 
        var labelizedHeader = [$A.get("$Label.c.RQO_lbl_invoiceNumber"),'fechaEmision','fechaVencimiento','importe','tipo','estado', 'numRefCliente', 'numPedido', 'numAlbaran', 'grado', 'cantidad', 'tipoCantidad', 'precioTonelada', 'importe', 'destino'];
       
        
        csvStringResult = '';
        csvStringResult += labelizedHeader.join(columnDivider);
        csvStringResult += lineDivider;
        var listaDetalle = null;
        var obj = null;
		//Vamos a recorrer los datos
        for(var i=0; i < objectRecords.length; i++){
			//Recuperamos cada factura y su desglose. 
            obj =objectRecords[i];
            listaDetalle = obj.detallesList;
			//Si tiene desglose pintaremos para cada registro del desglose los datos básicos y los datos detallados. Si no tiene pintamos únicamente los datos básicos
            if (listaDetalle && listaDetalle.length > 0){
                csvStringResult += this.createBrokenDownCsv(obj, listaDetalle, keysBrokenDown, keys, columnDivider, lineDivider);
            }else{
                csvStringResult += this.createBasicCsv(obj, keys, columnDivider);
                csvStringResult += lineDivider;
            }
        }
        
        // return the CSV formate String 
        return csvStringResult;        
    }, createBasicCsv: function (obj, keys, columnDivider){
        var csvStringResult = '';
        var counter = 0;
		var skey = null;
		var data = '';
        //Pintamos los datos básicos de factura (sin desglose). Se busca cada key dentro del objeto actual y se añaden los separadores de campo.
        for(var sTempkey in keys) {            
            skey = keys[sTempkey] ;  
			//Añadimos el separador de campos
            if(counter > 0){ 
                csvStringResult += columnDivider; 
            }   
			data = '';
			if (obj[skey]){
				data = obj[skey];
			}
			//Añadimos el datos al string que devolvemos al método principal
            csvStringResult += '"'+ data+'"'; 
            
            counter++;
            
        }
    	return csvStringResult;
	}, createBrokenDownCsv: function (obj, listObjDetail, keysBrokenDown, keys, columnDivider, lineDivider){
        var csvStringResult = '';
        var counter = 0;
		var objList = null;
		var skey = null;
		var data = '';
		debugger;
		//Vamos a pintar los datos de factura con desglose. Recorremos la lista detallada del objeto actual
		for(var i=0; i < listObjDetail.length; i++){
            counter = 0;
			//Pintamos los datos básicos del objeto. Cada linea desglosada lleva los datos básicos.
			csvStringResult += this.createBasicCsv(obj, keys, columnDivider);
			csvStringResult += columnDivider;
			objList = listObjDetail[i];
			//Vamos a recorrer todas las key dede desglose para pintar los datos del objeto desglosado que tenemos en la sublist del objeto actual
			for(var sTempkey in keysBrokenDown) {
				skey = keysBrokenDown[sTempkey];  
				//Añadimos el separador de campos
				if(counter > 0){ 
					csvStringResult += columnDivider; 
				}   
				data = '';
				if (objList[skey]){
					data = objList[skey];
				}
				//Añadimos el datos al string que devolvemos al método principal
				csvStringResult += '"'+ data +'"'; 
				
				counter++;
            
			}
			//Añadimos el separador de lineas
            csvStringResult += lineDivider;
        }
		
    	return csvStringResult;
	},toggleTabla: function (component, event){
        var tabla = event.currentTarget;
        jQuery(tabla).closest("tr").toggleClass('active-row').addClass('this');
        jQuery(tabla).toggleClass('fa-chevron-down fa-chevron-up');
        if (window.matchMedia("(max-width: 819px)").matches) {
            //Tabla exterior
            jQuery('.this td.movil-hide').toggleClass('show');
            jQuery('tr').removeClass('this');   
        }
        else{
            //Ha partir de escritorio muestra la tabla interior
            
            jQuery('.this + tr .wrapper-tabla-interior').toggleClass('show');
            jQuery('tr').removeClass('this');  
        }
    },
    toggleTablaInterior: function (component, event){
        
        /*::::::: T A B L A  I N T E R I O R :::::::*/
        
        var tablaInterior = event.currentTarget;
        jQuery(tablaInterior).closest('tr').addClass('this-active');
        jQuery(tablaInterior).toggleClass('fa-chevron-down fa-chevron-up');
        
        //Si el elemento actual se clicka pero su padre no esta activo se borra
        //jQuery('.this-active:not(active-row) + tr .wrapper-tabla-interior').removeClass('show');
        
        //Si el elemento actual se clicka y su padre esta desplegado se muestra
        jQuery('.this-active.active-row + tr .wrapper-tabla-interior').toggleClass('show');
        
        jQuery('tr').removeClass('this-active');
        
    }
})