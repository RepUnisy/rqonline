({
	getInfo : function(component, event, helper) {
        var user = event.getParam("user");
        component.set("v.user", user);
        
    	var action = component.get("c.selectCarousel");
        action.setParams({
            perfilCliente : component.get("v.user.perfilCliente"),
            perfilUsuario : component.get("v.user.perfilUsuario")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var comms = response.getReturnValue();
                component.set("v.contents", comms);
            }
        });
        $A.enqueueAction(action);
    },
    onRender: function(component, event, helper) {       
		helper.onRender(component, event);
	},
    currentSlide : function(component, event, helper) {
		helper.currentSlide(component);
	},
   	nextSlides : function(component, event, helper) {
		helper.nextSlides(component);
	},
    prevSlides : function(component, event, helper) {
		helper.prevSlides(component);
	}
})