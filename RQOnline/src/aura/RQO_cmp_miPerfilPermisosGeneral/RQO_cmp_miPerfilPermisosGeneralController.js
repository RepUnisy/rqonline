({
	doInit : function(component, event, helper) {
		//	Obtenemos listado de usuario
        var action = component.get("c.selectPermissionsByProfile");
        action.setParams({
            profile : component.get("v.user.perfilUsuario")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var permisos = response.getReturnValue();
                component.set("v.permisos", permisos);
            }
        });
        
        $A.enqueueAction(action);
    },
})