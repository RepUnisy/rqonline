({
	doInit : function(component, event, helper) {
    	var periodicity = component.get("v.emailSendingPeriodicityValue");
    	if (periodicity)
    		component.set("v.disabled", false);
    	else
    		component.set("v.disabled", true);
    },
})