({
	doInit : function(component, event, helper) {
		
    },
    personal : function(component, event, helper) {
        console.log('entro en personal');
        component.set("v.tab", 1);
        var cmpTarget = component.find('personalTab');
        $A.util.addClass(cmpTarget, 'activo');
        cmpTarget = component.find('notificacionesTab');
        $A.util.removeClass(cmpTarget, 'activo');
        cmpTarget = component.find('permisosTab');
        $A.util.removeClass(cmpTarget, 'activo');
    },
    notificaciones : function(component, event, helper) {
        component.set("v.tab", 2);
         console.log('entro en notificaciones');
        var cmpTarget = component.find('notificacionesTab');
        $A.util.addClass(cmpTarget, 'activo');
        cmpTarget = component.find('personalTab');
        $A.util.removeClass(cmpTarget, 'activo');
        cmpTarget = component.find('permisosTab');
        $A.util.removeClass(cmpTarget, 'activo');
    },
    permisos : function(component, event, helper) {
        component.set("v.tab", 3);
         console.log('entro en permisos');
        var cmpTarget = component.find('permisosTab');
        $A.util.addClass(cmpTarget, 'activo');
        cmpTarget = component.find('personalTab');
        $A.util.removeClass(cmpTarget, 'activo');
        cmpTarget = component.find('notificacionesTab');
        $A.util.removeClass(cmpTarget, 'activo');
    },
     tabSelect : function(component, event, helper) {
		var select = component.find('tabSelect').get('v.value');
        switch(select) {
            case '1': 
                 component.set("v.tab", 1);
                break;
            case '2': 
                 component.set("v.tab", 2);
                break;
            case '3': 
                component.set("v.tab", 3);
                break;
        }
       
    },
    getUserInfo:  function(component, event, helper) {
		var language = event.getParam("language");
        var user = event.getParam("user");
        
        component.set("v.language", language);
        component.set("v.user", user);
        
        //	Obtenemos información del cliente
        var action = component.get("c.selectAccountById");
        action.setParams({
            idAccount : component.get("v.user.cliente.Id")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var account = response.getReturnValue();
                component.set("v.account", account);
            }
        });
        
        $A.enqueueAction(action);
	},
})