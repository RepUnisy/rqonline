({
	doInit : function(component, event, helper) {
        component.set("v.mostrarSpinner", true);
		helper.getInfoHerencia(component, event, helper);
	},
    getInfo : function(component, event, helper) {
        var language = component.get("v.language");
        var user = component.get("v.user");
        
        //	Almacenamos la información obtenida
        /*component.set("v.language", language);
        component.set("v.user", user);*/
        
        //	Obtenemos la fecha diaria
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth();
        var yy = today.getFullYear();
        var today = new Date(yy, mm, dd);
       
        component.set("v.today", today);
        
        //	LLamamos al helper para obtener la información inicial
        helper.getInitialInfo(component);
    },
    searchData : function(component, event, helper) {
        component.set("v.mostrarSpinner", true);
		helper.searchData(component, event);
	}
})