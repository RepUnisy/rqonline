({
	getInitialInfo : function(component) {		
        
        var idContacto = component.get("v.user.idContacto");
        //	Llamamos a la funcion para inicializar las notificacioens 
        this.obtenerNotificaciones(component, idContacto, null, null, null, true);
        
    },
    searchData : function (component, event){
        
		var idContacto = component.get("v.user.idContacto");
        var categoria = event.getParam("categoria");
        if (categoria === $A.get("$Label.c.RQO_lbl_todas")){categoria = '';}
        var dateFrom = event.getParam("dateFrom");
        var dateTo = event.getParam("dateTo");
        
        //	Llamamos a la funcion para inicializar las notificacioens 
        this.obtenerNotificaciones(component, idContacto, categoria, dateFrom, dateTo, true);
     
    },
    
    obtenerNotificaciones : function(component, idContacto, categoria, fechaInicio, fechaFin, visualized) {
        var action = component.get('c.getNotifications');
        action.setParams({ idContacto : idContacto, categoria : categoria, fechaInicio : fechaInicio, 
                          fechaFin: fechaFin, visualized: visualized });

        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var data = JSON.parse(response.getReturnValue());
                component.set("v.dataNotificaciones", data);
                
                if (data && data.length > 0){
                    this.fireLimpiarNotificaciones();
                }
            }else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }else if (state === "ERROR") {
                var errors = response.getError()
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
			}
            component.set("v.mostrarSpinner", false);
        });
        $A.enqueueAction(action);   
    }, fireLimpiarNotificaciones : function(){
        //Lanza el evento para añadir un nuevo grado al contador de la parte superior
        var appEvent = $A.get("e.c:RQO_evt_sumarUnoANotificaciones");
        appEvent.setParams({"limpiarNotificaciones" : true});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }
})