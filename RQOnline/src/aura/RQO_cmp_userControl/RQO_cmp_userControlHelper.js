({
	getAccounts : function(component, event){
		//	Obtenemos listado de clientes
        var action = component.get("c.getChildAccounts");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var accounts = response.getReturnValue();
                component.set("v.accounts", accounts);
            }
        });
        
        $A.enqueueAction(action);
	},
})