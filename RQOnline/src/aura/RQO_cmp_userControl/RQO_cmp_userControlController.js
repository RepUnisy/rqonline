({
	doInit : function(component, event, helper) {
		helper.getAccounts(component, event);
    },
	logout : function(component, event, helper) {
        
	},
	showPopup : function(component, event, helper) {
        component.set('v.popupOrgVisible',true);
	},
	showPopupIdioma : function(component, event, helper) {
        component.set('v.popupIdiomaVisible',true);
	}
})