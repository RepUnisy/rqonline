({
	doInit : function(component, event, helper) {
		helper.getLanguages(component, event);
    },
    changeLanguage : function(component, event, helper) {
    	component.set("v.showSpinner",true);
    	helper.changeLanguage(component, event);
    	setTimeout(
    			function(){ 
    				location.reload(true);
    			}, 
    			1000);
    },
    close : function(component, event, helper) {
        helper.close(component, event);
	},
})