({
    close : function(component, event){
		component.set('v.visible',false);
	},
	getLanguages : function(component, event) {
        //   Obtenemos Listado de idiomas
        // hints to ensure labels are preloaded
              // $Label.c.RQO_lbl_idioma_en_US
              // $Label.c.RQO_lbl_idioma_es
              // $Label.c.RQO_lbl_idioma_fr
              // $Label.c.RQO_lbl_idioma_de
              // $Label.c.RQO_lbl_idioma_it
              // $Label.c.RQO_lbl_idioma_pt
        var action = component.get("c.getLanguages");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var idiomas = response.getReturnValue();
                var numIdiomas = idiomas.length;
                var opciones = [];
                for(var i = 0;i < numIdiomas;i++)
                {
                    var idiomaLabel = $A.get("$Label.c.RQO_lbl_idioma_" + idiomas[i]);
                    var selected = (component.get("v.user.language") == idiomas[i])
                    opciones.push({'label':idiomaLabel, 'value':idiomas[i], 'selected':selected});
                }
                component.set("v.languages", opciones);
                component.set("v.currentLanguage", $A.get("$Label.c.RQO_lbl_idioma_" + component.get("v.user.language")));
            }
        });
        
        $A.enqueueAction(action);
    },
	changeLanguage : function(component, event) {
	
		component.set('v.visible',false);
	
		var action = component.get("c.updateLanguage");
        action.setParams({
            language : component.find("sltLanguage").get("v.value")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            }
        });
        
        $A.enqueueAction(action);
	},
})