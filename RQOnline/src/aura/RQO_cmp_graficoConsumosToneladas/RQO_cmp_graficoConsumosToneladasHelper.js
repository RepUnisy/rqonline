({
	setViewGraphic : function(component, event) {
        //Lanzamos evento para visualizar la tabla de toneladas. El evento lo captura y propaga el padre
        //RQO_cmp_misConsumosExterno
        var appEvent = component.getEvent("changeViewTons");
        appEvent.setParams({"viewTableTons" : true});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}, createGroupedGraphic : function(component, event){
        //Captura del check de agrupación
        var groupedChecked = component.find("acumulados").get("v.checked");
       	if (groupedChecked){
            //Si está marcado agrupamos
            this.groupedGraphic(component, event);
        }else{
            //Si no está marcado generamos el gráfico normal
            this.createGraphic(component, event);
        }
    }, createGraphic : function(component, event) {
        //Generación del gráfico
        jQuery("#grafico").remove();
        jQuery('.canvas-container').append(' <canvas id="grafico" height="350"></canvas>');
        var ctx =jQuery("#grafico");
        var labelsHeader = [];
        debugger;
        var headerList = component.get("v.headerListData");
        var bodyList = component.get("v.bodyListData");
        //Leyenda horizontal a partir de la cabecera con los datos de mes/año
        for (var i = 0; i<headerList.length; i++) {
            for (var z = 0; z< headerList[i].years.length; z++){
                labelsHeader.push(headerList[i].month + ' ' + headerList[i].years[z]);
            }
        }
        var listData = [];
        var objData = null;
        var datosPorFamilia = [];
        var objConsumos = '';
        var color = '';
        //Datos del cuerpo del gráfico.
        //Recorremos cada familia de la lista, para cada familia recuperamos el objeto correspondiente 
        //y recorremos la sublista con los datos de consumos para generar el objeto que pinta el gráfico
        for (var z = 0; z<bodyList.length; z++) {
            objConsumos = bodyList[z];
            datosPorFamilia = [];
            //Vamos a por los datos de la sublista
            for (var x = 0; x<objConsumos.dataList.length;x++){
                datosPorFamilia.push(objConsumos.dataList[x].cantidad);
            }
            if (objConsumos.familyColor){color = objConsumos.familyColor;}else{color = this.getRandomColor();}
            //Familia - Lista de datos
        	objData = {label:objConsumos.translatedFamily, backgroundColor:color, data:datosPorFamilia};
            listData.push(objData);
        }
        //Generamos el gráfico con los datos
		var grafico = new Chart(ctx, {
             type: 'bar',
             data: {
             	labels: labelsHeader,
             	datasets: listData,
             },
             options:{
				maintainAspectRatio: false,
             }
		});
        
    }, getRandomColor : function() {
          //Recuperación Random de un color para cada elemento del gráfico
          var letters = '0123456789ABCDEF';
          var color = '#';
          for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
          }
          return color;
	}, groupedGraphic : function(component, event){
        //Generación del gráfico con los datos agrupados
        jQuery("#grafico").remove();
        jQuery('.canvas-container').append(' <canvas id="grafico" height="350"></canvas>');
        var ctx =jQuery("#grafico");
        var headerList = component.get("v.headerListData");
        var bodyList = component.get("v.bodyListData");
        var headerMap = new Map();
        //Leyenda horizontal a partir de la cabecera. Recorremos todos los datos de cabecera y nos quedamos con los años
        //en un mapa
        for (var i = 0; i<headerList.length; i++) {
            for (var z = 0; z< headerList[i].years.length; z++){
                if (!headerMap.has(headerList[i].years[z])){
                    headerMap.set(headerList[i].years[z], headerList[i].years[z]);
                }
            }
        }
        var labelsHeader = [];
        var datosAcumulados = [];
        var objConsumos = null;
        var cantidadAcumulado = null;
        var objData = null;
        var listData = [];
        var color = '';
        //Datos del cuerpo del gráfico agrupado.
        //Recorremos cada año del mapa de cabecera, para cada año recorremos cada familia de la lista, 
        //para cada familia recuperamos el objeto correspondiente 
        //y recorremos la sublista con los datos agrupando los datos de consumos cuando coincide el año con el de bucle 
        //superior
        for(var key of headerMap.keys()){
            labelsHeader.push(key);
            for (var z = 0; z<bodyList.length; z++) {
                objConsumos = bodyList[z];
                datosAcumulados = [];
                cantidadAcumulado = 0;
                //Vamos a por los datos de la sublista
                for (var x = 0; x<objConsumos.dataList.length;x++){
                    if (objConsumos.dataList[x].yearConsum == key && objConsumos.dataList[x].cantidad){
                        cantidadAcumulado = cantidadAcumulado + objConsumos.dataList[x].cantidad;
                    }
                }
                if (objConsumos.familyColor){color = objConsumos.familyColor;}else{color = this.getRandomColor();}
                datosAcumulados.push(cantidadAcumulado);
                //Familia - Cantidad acumulada para el año
                objData = {label:objConsumos.translatedFamily, backgroundColor:color, data:datosAcumulados};
                listData.push(objData);
            }
            
        }
        //Generamos el gráfico con los datos
        var grafico = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labelsHeader,
                datasets: listData,
            },
            options:{
                maintainAspectRatio: false,
            }
        });
    }
})