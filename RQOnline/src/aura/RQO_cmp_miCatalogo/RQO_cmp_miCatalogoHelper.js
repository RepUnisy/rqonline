({
    
    getCatPropuesto : function(component, event) {
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : '¿Deseas que se le proponga un nuevo Catálogo?', "confirmAlert" : true, 
                            "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), 
                            "idAlert" : 'catPropuesto', "parentLE" : false});
        try{
            appEvent.fire();
        }catch(err){
            //this.spinner(component);
            console.log(err.message);
        }  
    },
    
    
    
    /*	Obtenemos la información del menú	*/
    getInfoMenu : function(component, event) {
        var language = event.getParam("language");
        var user = event.getParam("user");
        component.set("v.language", language);
        component.set("v.user", user);
        console.log('ACL. Detalle producto language ' + language)
        //	Obtenemos la información de mi catalogo
        this.getMiCatalogo(component, event);
    },
    
    /*	Obtenemos los grados que tiene el usuario en mi catálogo	*/
    getMiCatalogo : function(component, event) {
        var IdContacto = component.get("v.user.idContacto");
        var language = component.get("v.language");
        console.log('ACL. id: ' + IdContacto)
        var action = component.get('c.getMiCatalogo');
        console.log('ACL. Id contacto ' + IdContacto)	
        action.setParams({ "IdContacto" : IdContacto, "language" : language });

        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                
                var respuesta = JSON.parse(response.getReturnValue());
                console.log('ACL. Respuesta: ' + respuesta)               
                
                if (respuesta == null){
                    component.set("v.hasCatalogo", false);
                }
                else{
                    component.set("v.listTabla" , respuesta);
                    component.set("v.hasCatalogo", true);
                    console.log('ACL. Introducida la tabla: ' + respuesta)                    
                }                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }  
            component.set("v.mostarSpinner", false);
        });
        $A.enqueueAction(action);
    },
    
    /*	Propone de nuevo mi catálogo	*/
    proponerCatalogo : function(component, event) {
        var IdCliente = component.get("v.user.idCliente");
        var IdContacto = component.get("v.user.idContacto");
        var action = component.get('c.volverProponer');
        component.set("v.visible",true);
        component.set("v.mostarSpinner", true);
        action.setParams({ "IdContacto" : IdContacto , idAccoun : IdCliente});
        console.log('ACL. proponer pedido para: ' + IdContacto)
        action.setCallback(this, function(response){
        
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                
                 var respuesta = JSON.parse(response.getReturnValue());
                console.log('ACL. Respuesta: ' + respuesta)               
                
                if (respuesta == null){
                    component.set("v.hasCatalogo", false);
                }
                else{
                    component.set("v.listTabla" , respuesta);
                    component.set("v.hasCatalogo", true);
                    var user = component.get("v.user");
                    if(user && user.isPrivate){
                        this.registrarEvento(component);
                    }
                }
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                        
           component.set("v.mostarSpinner", false);
        });
        $A.enqueueAction(action);  
    },
    
    spinner: function(component){
    	var spinnerTop = component.find("spinnerPadre");        
        $A.util.toggleClass(spinnerTop, "slds-show");   
	},
    registrarEvento: function(component){
        /*
         * Registro del evento de volver a proponer el catálogo
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         * 	Valor 9 = Re-propose the catalog
         * actionType - Grupo al que pertenece, 1 = Product Catalogue
         * actionDescription - descripción de la acción que lanza el evento. En este caso volver a proponer el catálogo.
         * Valores para este componente:
         * 	Valor 9 = Query my catalog (proposition)
         * reSearchData - Búsqueda realizada por el usuario
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_miCatalogo', "reComponentDescription" : '9', 
                            "actionType" : '1', "actionDescription" : '9'});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
    
})