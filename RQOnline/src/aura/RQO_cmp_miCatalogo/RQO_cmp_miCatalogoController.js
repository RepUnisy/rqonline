({
    
    getInfoMenu : function(component, event, helper) {   
        helper.getInfoMenu(component, event);
    },
    getCatPropuesto : function(component, event, helper) {     
      
        helper.getCatPropuesto(component,event);
    },
    handleResponseAlert : function(component, event, helper){
        var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");

        if(responseAlert){
                    console.log('Respuesta del alert:' + responseAlert);
           helper.proponerCatalogo(component,event);
        }        
    }
    
    
})