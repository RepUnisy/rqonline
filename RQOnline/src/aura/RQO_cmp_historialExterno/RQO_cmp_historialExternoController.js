({
    render : function(component, event, helper) {
        helper.getInfoHerencia(component, event, helper);
        
        // window.scrollTo(0, 0);  
        var listAgrupado = [];
        
        listAgrupado.push({"label" : $A.get("$Label.c.RQO_lbl_pedidoSeguimientoPreferentDate"), "value" : "srcFechaPreferente" });
        listAgrupado.push({"label" : $A.get("$Label.c.RQO_lbl_pedidoSeguimientoGrade"), "value" : "qp0gradeName" });         
        listAgrupado.push({"label" : $A.get("$Label.c.RQO_lbl_pedidoSeguimientoReferenciaCliente"), "value" : "numRefCliente" });
        listAgrupado.push({"label" : $A.get("$Label.c.RQO_lbl_pedidoSeguimientoDestination"), "value" : "destinoMercanciasNombre" });
        listAgrupado.push({"label" : $A.get("$Label.c.RQO_lbl_Status"), "value" : "statusDescription" });
        component.set("v.listAgrupador", listAgrupado)
        
    },
    getInfo : function(component, event, helper) {
        if (window.location.pathname != '/comunidadquimica/s/hp'){
            console.log('No se reejecuta');
        }else{
            var language = component.get("v.language");
            var user = component.get("v.user");
            var idAccount = user.idCliente;
            /*component.set("v.language", language);
            component.set("v.user", user);*/
            var solicitud = null;
            var pedido = null;
            var fecha = null;
            //Comprobamos con una expresion regular si se han pasado parámetros por la url y en ese caso se establecen en el filtro
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                                                     function(m,key,value) {
                                                         console.log(key + ":"+value);
                                                         if(key=="solicitud"){
                                                             solicitud = value;
                                                         }
                                                         if(key=="pedido"){
                                                             pedido= value;
                                                         }
                                                         if(key=="fecha"){
                                                             fecha= value;
                                                         }
                                                     });
            if(solicitud){solicitud = solicitud.padStart(10, "0");}
            if(pedido){pedido = pedido.padStart(10, "0");}
            helper.getData(component, idAccount, language, solicitud, pedido, null, null, null, null, null, null, fecha, fecha, true);
		}
    },
    
    agrupador : function(component, event, helper) {
		var listOrigen = component.get("v.resulsetData");
        helper.getOrderedData(component, listOrigen, true);
    },
    
    download: function(component, event, helper){
        
        var data = component.get("v.resulsetData");
        if (data){
            // call the helper function which "return" the CSV data as a String   
            var csv = helper.convertArrayOfObjectsToCSV(component,data);   
            if (csv == null){return;} 
             
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
            hiddenElement.target = '_self'; // 
            hiddenElement.download = 'ExportData.csv';  // CSV file Name* you can change it.[only name not .csv] 
            document.body.appendChild(hiddenElement); // Required for FireFox browser
            hiddenElement.click(); // using click() js function to download csv file
       }
        
    }, searchData : function(component, event, helper) {
		helper.searchData(component, event);
	}, cambiarPagina : function(component, event, helper) {
		helper.cambiarPagina(component, event);
    }
})