({
    onRender : function(component, event, helper) {
	var ctx =jQuery("#grafico");
      
     var grafico = new Chart(ctx, {
            type: 'bar',
            data: {
        labels: ["May", "Jun", "Jul", "Ago", "Sep"],
        datasets: [
            {
           		label: "Glicol",
            	backgroundColor: "#FF9B33",
            	data: [10000,25000,20000,7000,14002]
            },
            {
                label: "Copolimero EVA",
                backgroundColor: "#E93353",
                data: [15000,17000,13000,17000,8000]
            },
            {
                label: "Poliol",
                backgroundColor: "#364B68",
                data: [19000,13000,3000,27000,21000]
            }
            
        ]
    },
    options:
         {
              maintainAspectRatio: false,
              tooltips:
             {
                 enabled: false
             }
         }
   });
	},
    
    
    goTo :function(component, event, helper){
        var url = "/con";
          var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();     
    }
})