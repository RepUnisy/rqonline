({
	doInit : function(component, event, helper) {
        //	Obtenemos los parámetros de la url
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); 
        var sURLVariables = sPageURL.split('&'); 
        var sParameterName;
        var idComm = '';
        
        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); 
            if (sParameterName[0] === 'id') {
                idComm = sParameterName[1];
            }
        }

        var action = component.get("c.selectCommunicationById");
        action.setParams({
            commId : idComm
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var comm = response.getReturnValue();
                component.set("v.comm", comm);
            }
        });
        
        $A.enqueueAction(action);
        
        var action = component.get("c.selectImages");
        action.setParams({
            commId : idComm
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var imgs = response.getReturnValue();
                component.set("v.images", imgs);
            }
        });
        $A.enqueueAction(action);
        
        var action = component.get("c.selectDocuments");
        action.setParams({
            commId : idComm
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var atts = response.getReturnValue();
                component.set("v.attachments", atts);
            }
        });
        $A.enqueueAction(action);
    }
})