({
    getData : function(component, idAccount, idioma, solNumber, orderNumber, refCliente, grade, fechaEntregaDesde, fechaEntregaHasta,
                      searchStatus, searchDestination){
		component.set("v.mostrarSpinner", true);
        var action = component.get('c.getHisorialMostrar');
        
        action.setParams({"idAccount" : idAccount, "idioma" : idioma, "solNumber" : solNumber, "orderNumber" : orderNumber, "refCliente" : refCliente, 
                          "grade" : grade, "fechaEntregaDesdeDt" : fechaEntregaDesde, "fechaEntregaHastaDt" : fechaEntregaHasta,
                         "searchStatus" : searchStatus, "searchDestination" : searchDestination});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                debugger;
                var storedResponse = response.getReturnValue();
                if (storedResponse){
                    var listData = JSON.parse(storedResponse);
                    component.set("v.resulsetData", listData);
                    this.getOrderedData(component, listData, false);
                }else{
                    component.set("v.resulsetData", null);
                    component.set("v.orderResultSetData", null);
                    component.set("v.vacio", true);
                }
            }else{
        		component.set("v.error", true);
            }
            component.set("v.mostrarSpinner", false);
        }); 
        $A.enqueueAction(action);
    },
    
    getOrderedData : function (component, listData, showSpinner){
        if (showSpinner){
            component.set("v.mostrarSpinner", true);
        }
        component.set("v.vacio", false);
        component.set("v.error", false);
        var  groupField = component.get("v.groupByField");
        this.getOrderedLabel(component, groupField);
        var myMap = new Map();
        var objAux = null;
        var keyAux = null;
        var listAux = null;
        var envaseTradu = '';
        for(var i = 0;i<listData.length;i++){
            if (listData[i].envaseName != null){
                envaseTradu=  $A.get("$Label.c.RQO_lbl_" + listData[i].envaseName.substr(0,2));
                if (envaseTradu != ''){
                    listData[i].envaseDescription = envaseTradu;
                }
            }
            objAux = listData[i];
            keyAux = objAux[groupField];
            if (myMap.has(keyAux)){
                listAux = myMap.get(keyAux);
            }else{
                listAux = [];
                myMap.set(keyAux, listAux);
            }
            listAux.push(objAux);
        }
        
        var customMap = [];
        for (var key of myMap.keys()) {
            customMap.push({value:myMap.get(key), key:key});
        }
        component.set("v.orderResultSetData", customMap);
        //Cálculo datos paginación inicial
        var numeroPaginas = parseInt((customMap.length / $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla")) + 1);
        if(customMap.length % $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla") == 0){
            numeroPaginas -= 1;
        }
        var listaPaginas = [];
        for (var i = 1; i <= numeroPaginas; i++) {
            listaPaginas.push(i);
        }
        component.set("v.numeroPaginas", listaPaginas);
        if(listaPaginas.length <= 1){
            component.set("v.mostrarPaginacion", false);
        }else{
            component.set("v.mostrarPaginacion", true);
        }
        var inicioPaginacion = $A.get("$Label.c.RQO_lbl_cero");
        var finPaginacion = $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla");
        component.set("v.inicioPaginacion", inicioPaginacion);
        component.set("v.finalPaginacion", finPaginacion);
        this.paginacion(component, customMap, inicioPaginacion, finPaginacion, 1);
        if (showSpinner){
            component.set("v.mostrarSpinner", false);
        }
    }, getOrderedLabel : function (component, groupByCode){
		var translatedLabel = '';
        switch (groupByCode) {
            case 'srcFechaPreferente':
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoPreferentDate");
                break;
            case 'qp0gradeName':
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoGrade");
                break;
            case 'numRefCliente':
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoReferenciaCliente");
                break;
            case 'destinoMercanciasNombre':
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoDestination");
                break;
            case 'statusDescription':
                translatedLabel = $A.get("$Label.c.RQO_lbl_status");
                break;
            default: 
                translatedLabel = $A.get("$Label.c.RQO_lbl_pedidoSeguimientoGrade");
        }
        component.set("v.groupByLabel", translatedLabel);
    },
    convertArrayOfObjectsToCSV : function(component,objectRecords){
        debugger;
        component.set("v.mostrarSpinner", true);
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ';';
        lineDivider =  '\n';
        
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['nameSolicitud','numPedido','qp0gradeName','numRefCliente','envaseDescription', 'cantidad', 'destinoMercanciasNombre', 'statusDescription',
               'srcFechaPreferente', 'fechaEntrega'];
          
        var labelizedKeys = [$A.get("$Label.c.RQO_lbl_request"),$A.get("$Label.c.RQO_lbl_pedidoSeguimientoNumeroPedido"),
                         $A.get("$Label.c.RQO_lbl_pedidoSeguimientoGrade"),$A.get("$Label.c.RQO_lbl_pedidoSeguimientoReferenciaCliente"),
                         $A.get("$Label.c.RQO_lbl_pedidoSeguimientoEnvase"), $A.get("$Label.c.RQO_lbl_pedidoSeguimientoQuantity"), 
                        $A.get("$Label.c.RQO_lbl_pedidoSeguimientoDestination"), $A.get("$Label.c.RQO_lbl_status"),
               			$A.get("$Label.c.RQO_lbl_pedidoSeguimientoPreferentDate"), $A.get("$Label.c.RQO_lbl_pedidoSeguimientoDeliveryDate")];
        csvStringResult = '';
        csvStringResult += labelizedKeys.join(columnDivider);
        csvStringResult += lineDivider;
        var data = '';
        for(var i=0; i < objectRecords.length; i++){
            counter = 0;
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }   
                data = '';
                if (objectRecords[i][skey]){
                    data = objectRecords[i][skey];
                }
                csvStringResult += '"'+ data +'"'; 
                
                counter++;
                
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close 
        component.set("v.mostrarSpinner", false);
        // return the CSV formate String 
        return csvStringResult;        
    }, 
    searchData : function (component, event){
        component.set("v.vacio", false);
        component.set("v.error", false);
        component.set("v.listDataOrigen", null);
        component.set("v.mapPosicionPedido", null);
        component.set("v.listPosicionPedido", null);
        var user = component.get("v.user");
		var idAccount = user.idCliente;
        this.getData(component, idAccount, component.get("v.language"), event.getParam("solicitud"), event.getParam("pedido"), event.getParam("refCliente"),
                     event.getParam("grado"), event.getParam("dateFrom"), event.getParam("dateTo"), event.getParam("estado"), event.getParam("destino"));
        
    },
    cambiarPagina : function(component, event) {
        component.set("v.mostrarSpinner", true);
        var pagina = parseInt(event.getSource().get("v.value"));
        component.set('v.paginaActiva',pagina);
        var inicioPaginacion = (pagina * $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla")) - $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla");
        var finPaginacion = (pagina * $A.get("$Label.c.RQO_lbl_numeroElementosPaginaTabla"));
        
        component.set("v.inicioPaginacion", inicioPaginacion);
        component.set("v.finalPaginacion", finPaginacion);
		var customMap = component.get("v.orderResultSetData");
         /*:::: PARTE FRONT PAGINADOR :::::*/
            var paginadores = component.find('paginadores');
            
            for(var i in paginadores){
                $A.util.removeClass(paginadores[i], 'page-active');
            }
            
            $A.util.addClass(paginadores[pagina-1], 'page-active');
        /*:::: PARTE FRONT PAGINADOR :::::*/
        
        this.paginacion(component, customMap, inicioPaginacion, finPaginacion, pagina);
        component.set("v.mostrarSpinner", false);
    }, 
    paginacion : function(component, lista, inicioPaginacion, finPaginacion, pagina){
        debugger;
        component.set("v.listaPaginada", lista.slice(inicioPaginacion, finPaginacion));
    }
})