({
    doInit : function(component, event, helper) {
        //component.set('v.categoriaSeleccionado', 'Información Corporativa');
        helper.cargarLista(component, event);
    },
    
    marcarPredefinido : function(component, event, helper) {
        debugger;
        if(component.get('v.predefinido')){
            var botones = component.find('botones');
            $A.util.addClass(botones[0], 'btn on');   
        }
        component.set('v.predefinido', false);
    },
    
    recibir : function(component, event, helper) {
        helper.cargarLista(component, event);
    },
    
    seleccionCategoria : function(component, event, helper) {
        var botones = component.find('botones');
        
        for(var i in botones){
            $A.util.removeClass(botones[i], 'btn on');
        }
        
        $A.util.addClass(event.getSource(), 'btn on');
        var appEvent = $A.get("e.c:RQO_evt_buscadorFaqsList");
        appEvent.setParams({
            "topic" : event.getSource().get("v.label"),
            "palabra" : component.get('v.palabra')});
        appEvent.fire();
    }
})