({
    cargarLista : function(component, event) {
        var action = component.get('c.getFaqsCategorias');
        component.set('v.palabra', event.getParam("texto"));
        var user = component.get('v.user');
        var tipoUsu = (user == null || user.isPrivate == false) ? 'publico' : (user.esCliente == false) ? 'noCliente' : 'cliente';
        debugger;
        action.setParams({"lenguaje" : component.get('v.language'),
                          "palabra" : component.get('v.palabra'),
                          "tipoUsuario" : tipoUsu});
        action.setCallback(this, function(response) {
            //store response state
            //debugger; 
            var state = response.getState();
            var listaAux = [];
            if (state === "SUCCESS") {
                //debugger;
                var respuesta = response.getReturnValue();                
                component.set('v.listCategorias', respuesta);
                component.set('v.predefinido', true);            
                this.enviarEvento(respuesta[0], component.get('v.palabra'));
                debugger;
            }else if (state === "INCOMPLETE") {
                //debugger;
                // do something
            }else if (state === "ERROR") {
                //debugger;
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });  
        // enqueue the Action   
        $A.enqueueAction(action);
    },
    
    enviarEvento : function(topic, palabra){    
        var appEvent = $A.get("e.c:RQO_evt_buscadorFaqsList");
        appEvent.setParams({
            "topic" : topic,
        	"palabra" : palabra});
        appEvent.fire();
    }
})