({
		getInfo : function(component, event) {
		var language = event.getParam("language");
		var user = event.getParam("user");
            
        component.set("v.language", language);
        component.set("v.user", user);
		
		
		// Establecer permisos
		if (user.permissionUrls.includes("/catalogo"))
			component.set("v.hasAccessCatalogoPrivado", true);
		if (user.permissionUrls.includes("/catalogo"))
			component.set("v.hasAccessMiCatalogo", true);
		if (user.permissionUrls.includes("/sp"))
			component.set("v.hasAccessSolicitudPedido", true);
		if (user.permissionUrls.includes("/sp"))
			component.set("v.hasAccessMisPlantillas", true);
		if (user.permissionUrls.includes("/hp"))
			component.set("v.hasAccessHistoricoPedidos", true);
		if (user.permissionUrls.includes("/hp"))
			component.set("v.hasAccessSeguimientoPedidos", true);
		if (user.permissionUrls.includes("/fact"))
			component.set("v.hasAccessFacturas", true);
		if (user.permissionUrls.includes("/fact"))
			component.set("v.hasAccessMisConsumos", true);
		if (user.permissionUrls.includes("/perfil"))
			component.set("v.hasAccessPerfil", true);
	}
})