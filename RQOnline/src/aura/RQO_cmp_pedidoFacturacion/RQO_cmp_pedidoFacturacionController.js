({
    doInit : function(component, event, helper) {
        
        helper.getDirecciones(component);
        
        //	Obtenemos la información de las picklist
        helper.getTipoPedido(component);

        //	Obtenemos el usuario y las direcciones de facturacion
        helper.generarDirecciones(component);
    },
    changeSelect : function (component, event, helper) {
        //	Obtenemos la información del evento
        var idComponente = event.getParam("idComponente");
        var valorSeleccionado = event.getParam("valorSeleccionado");  
        
        var mapTipoPedido = component.get("v.mapTipoPedido");
        var valorSeleccionado = Array.from(mapTipoPedido.keys())[valorSeleccionado];
        
        component.set("v.codigoTipoPedidoHeredado", valorSeleccionado);
        //helper.almacenarTipoPedido(component);
    },
    guardar : function (component, event, helper){
        debugger;
        var guardarHeredado = component.get("v.guardarHeredado");
        if (guardarHeredado){
            debugger;
            helper.almacenarTipoPedido(component);
            helper.almacenarDireccionesFacturacion(component);
        }        
    }
    
})