({
    generarDirecciones : function(component) {
        var usuario = component.get("v.userHeredado");		  
        
        var listSeleccionFactura = [];
        var listFacturacion = [];
        var mapaPosi = new Map();
        
        //	Insertamos el propio cliente como posible envío de facturación
        mapaPosi.set(0, {Id : usuario.cliente.Id,
                         Name : usuario.cliente.Name, 
                         calle : usuario.cliente.RQO_fld_calleNumero__c,
                         poblacion : usuario.cliente.RQO_fld_poblacion__c,
                         pais : usuario.cliente.RQO_fld_pais__c
                        });
        //	Insertamos en el listado de selección el propio cliente
        listSeleccionFactura.push(usuario.cliente.Name);
        
        //	Obtenemos las direcciones de facturacion por area de ventas
        var mapFactu = usuario.mapDirFacturacion;
        var listDirFacturacion = usuario.listDirFacturacion;
        debugger;
        
        for (var i in listDirFacturacion) {
            debugger;
                /*
                mapaPosi.set(parseInt(i) + 1, {Id :  listDirFacturacion[i].RQO_fld_cliente__r.Id, 
                                               Name : listDirFacturacion[i].RQO_fld_cliente__r.Name, 
                                               calle : listDirFacturacion[i].RQO_fld_calleNumero__c,
                                               poblacion : listDirFacturacion[i].RQO_fld_city__c,
                                               pais : listDirFacturacion[i].RQO_fld_country__c
                                              });
                
                listSeleccionFactura.push(listDirFacturacion[i].RQO_fld_cliente__r.Name);
                */
                debugger;       
        }
        debugger;
        
        if (mapaPosi != null){
            component.set("v.listSeleccionFactura", listSeleccionFactura);
            debugger;
            component.set("v.mapaDirFacturacion", mapaPosi);
            component.set("v.seleccionadoInfo" , mapaPosi.get(0));
        }
    },
    
    getTipoPedido : function(component) {
        
        var action = component.get('c.getTipoPedido');  
        action.setCallback(this, function(response){
            var state = response.getState();
           
            if (state === "SUCCESS") {
              	var mapTipo = new Map(Object.entries(response.getReturnValue()));
                
                var listadoOpciones = [];
                for (var [key, value] of mapTipo){
                     listadoOpciones.push(value);
                }
                
                component.set("v.mapTipoPedido", mapTipo);
                component.set("v.listTipoPedido", listadoOpciones);
                component.set("v.posicionTipoPedido", 0);
                debugger;
       
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }     
        });
        $A.enqueueAction(action);  
    },
    
    almacenarTipoPedido : function(component){
 
       	//	Obtenemos la información para almacenar
        var idSolicitud = component.get("v.idSolicitudHeredado");
        var tipoPedido =  component.get("v.codigoTipoPedidoHeredado");

        var action = component.get('c.setTipoPedido');  
        action.setParams({ idSolicitud : idSolicitud, tipoPedido : tipoPedido});
        action.setCallback(this, function(response){
            var state = response.getState();
           debugger;
            if (state === "SUCCESS") {
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }     
        });
        $A.enqueueAction(action);  
    },
    
    almacenarDireccionesFacturacion : function(component) {
        //	Obtenemos los elementos que vamos a almacenar
        var listadoActualizado = component.get("v.listDirecciones");      
        debugger;
        var action = component.get('c.assignDireccionFacturacionParcial');
        action.setParams({ listPedidoModificado : JSON.stringify(listadoActualizado)});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {   
               // component.set("v.isEditado", false);
                                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }    
        });
        $A.enqueueAction(action);
        debugger;
	},
    
    getDirecciones : function(component) {
        
        //	Obtenemos el id del cliente
		var IdCliente = component.get("v.userHeredado.idCliente");
        
        //	Llamamos a la función para obtener los pedidos
        var action = component.get('c.getPedidosFacturacion');
        action.setParams({ idAccoun : IdCliente});
        debugger;
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //	Obtenemos la respuesta del servidor
                var direccionesMap = JSON.parse(response.getReturnValue()); 
                debugger;
                //	Inicializamos la lista de direcciones
                var listDirecciones = [];
                for(var key in direccionesMap){
                    listDirecciones.push(direccionesMap[key][0]);
                }
                //	Almacenamos la lista de direcciones
                component.set("v.listDirecciones", listDirecciones);     
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }       
        });
        $A.enqueueAction(action);  
	}
})