({
    doInit : function(component, event) {
        var urlImagen = component.get("v.urlImagen");
  		var auxURL = $A.get('$Resource.RQO_sr_imagenesCatalogo') + urlImagen ;
        component.set("v.resource" , auxURL);
	},
	lanzaComponenteTabla : function(component, event) {
        var action = component.get('c.getBusquedaCategoria');
        var user = component.get("v.user");
        if(user.isPrivate){
        action.setParams({ "busqueda" : component.get("v.idCategoriaCatalogo"),
                          "JSONGrados" : null,
                          "language" : component.get('v.language'),
                          "gradosRelacionados" : false,
                          "privado" : user.isPrivate,
                          "RelatedAccount" : user.cliente.Id});
        }
        else{
            action.setParams({ "busqueda" : component.get("v.idCategoriaCatalogo"),
                          "JSONGrados" : null,
                          "language" : component.get('v.language'),
                          "gradosRelacionados" : false,
                          "privado" : user.isPrivate,
                          "RelatedAccount" : null});
        }
        console.log('Entra en lanzaComponenteTabla2');
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('Estado: ' + response + '  ' + response.getState());
            if (state === "SUCCESS") {
                console.log('Por favor.');
                var lanzaEventoTabla = $A.get("e.c:RQO_evt_lanzaEventoTabla");
                var respuesta = JSON.parse(response.getReturnValue());
                lanzaEventoTabla.setParams({ "mapaTabla" : respuesta });
                lanzaEventoTabla.fire();
                component.set("v.catalogo", false);
            }
            else if (state === "INCOMPLETE") {
               console.log('Incomplete');
            }
            else if (state === "ERROR") {
                
                var errors = response.getError()
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    }
})