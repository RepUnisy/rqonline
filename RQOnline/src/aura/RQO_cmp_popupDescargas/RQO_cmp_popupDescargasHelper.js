({
	close : function(component, event) {
        component.set("v.visible", false);
    }, handleGetDocData : function(component, event){
        component.set("v.vacio", false);
        var idGrade = event.getParam("idGradoEvt");
        component.set("v.idGrade", idGrade);
        var idioma = event.getParam("idioma");
        var user = event.getParam("user");
        component.set("v.user", user);
        var actiontranslate = component.get('c.getDocTypeTittleTranslate');
        actiontranslate.setCallback(this, function(responseTranslate) {
            var stateTranslate = responseTranslate.getState();
            if (stateTranslate === "SUCCESS") {
                var customTranslateList = [];
                var storedResponseTranslate = responseTranslate.getReturnValue();
                for (var key in storedResponseTranslate) {
                    customTranslateList.push({value:storedResponseTranslate[key], key:key});
                }
                component.set('v.docTypeTittleTranslate',customTranslateList);
            }
        });
        // enqueue the Action   
        $A.enqueueAction(actiontranslate);
        var action = component.get('c.doInitDataProd4Apex');
        action.setParams({ "idGrade" : idGrade, "idioma" : idioma});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var customList = [];
                var storedResponse = response.getReturnValue();
                for (var key in storedResponse) {
                    customList.push({value:storedResponse[key], key:key});
                }
                component.set("v.resulsetData", customList);
                component.set("v.visible", true);
                if (!customList || customList.length==0){
                    component.set("v.vacio", true);
                }
            }
        });
        // enqueue the Action   
        $A.enqueueAction(action);
    }
})