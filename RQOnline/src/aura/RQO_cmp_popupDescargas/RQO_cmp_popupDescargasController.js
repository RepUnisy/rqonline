({
	close : function(component, event, helper) {
        helper.close(component, event);
    }, 
    handleGetDocData : function(component, event, helper){
        helper.handleGetDocData(component, event);
    }
})