({ 
    onRender : function(component, event, helper) {      
       helper.getNumPosiciones(component,event);
    },
    getInfo : function(component, event, helper) {
        helper.getInfo(component, event);
    },
    getInfoController : function(cmp, event, helper) {
        helper.getInfoController(cmp, event);
    },
    almacenarPeticion : function(cmp, event, helper) {
        helper.almacenarPeticion(cmp, event);
    },
    showPopupIdioma : function(component, event, helper) {
        component.set('v.popupIdiomaVisible',true);
    },
    irASp : function(component, event, helper){
        var url = '/sp';
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();
    },
     irANotificaciones : function(component, event, helper){
        var url = '/ntf';
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();
    },
    login : function(component, event, helper) {
        helper.login(component);
    }
})