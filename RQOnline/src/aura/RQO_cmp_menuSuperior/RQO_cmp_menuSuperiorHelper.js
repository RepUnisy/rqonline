({
    getInfo : function(component, event) {
        //	Obtenemos la información del usuario
        var action = component.get('c.getUserInfo');       
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                var user = JSON.parse(respuesta);
                 var sPageURL = '';
                
                var language = 'es_ES';
                
                //	Si no es privado obtenemos el lenguage de la url
                if (!user.isPrivate){
                    for (var i = 0; i <= 1; i++) {
                        sPageURL  = decodeURIComponent(window.location.search.substring(i));
                    }
                    var sURLVariables = sPageURL.split('&'); 
                    var sParameterName;
                    
                    for (var i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('='); 
                        
                        if (sParameterName[0] === 'language') {
                            if (sParameterName[1] == 'es'){
                                language = 'es_ES';
                            }
                            else{
                                language = 'en_US';
                            }                	
                            sParameterName[1] === undefined ? 'Not found' : sParameterName[1];                
                        }
                    }
                }
                else{
                    if (user.language == 'es'){
                        language = 'es_ES';
                    }
                    else{
                        language = user.language;
                    }
                }
                
                //	Almacenamos la información
                component.set('v.language', language);
                component.set('v.user', user);
                console.log('PT. MS. nuevo')
                 //	Enviamos el evento con la información obtenida
                this.envioEventoInfo(language, user);
            }
        });  
        $A.enqueueAction(action);        
    },    
    
    getInfoController : function(cmp, event, helper) {
        //	Obtenemos el tipo de petición
        var peticion = event.getParam("peticion");
        console.log('petición recibida: ' + peticion);
        
        if (peticion == 'infoUser'){
            var language = cmp.get('v.language');
            var user = cmp.get('v.user');
            if (language != null && user != null){
                console.log('ACL. Envio por peticion')
                //	Enviamos el evento con la información obtenida	                
                this.envioEventoInfo(language, user);
            }
            else{
                console.log('Info null en peticion')
            }                   
        }
    },
    
    
    almacenarPeticion : function(cmp, event, helper) {
        //	Obtenemos el tipo de petición
        var peticion = event.getParam("infoAlmacenar");
        console.log('vamos a almacenar: ' + peticion);
        
        cmp.set("v.idGrado", peticion);
    },
    
    
    login : function(component, event, helper){
        
        var isPrivado = component.get("v.user.isPrivate");
        if (isPrivado){
            window.location.replace("https://derqonline-repsol.cs88.force.com/comunidadquimica/secur/logout.jsp?retUrl=/comunidadquimica/s/catalogo");
        }
        else{
            window.location.replace("https://derqonline-repsol.cs88.force.com/comunidadquimica/login");            
        }
    },
    
    envioEventoInfo : function(language, user){
        //	Enviamos el evento con la información obtenida
        var eventoEnvioInfo = $A.get("e.c:RQO_evt_infoMenu");                
        eventoEnvioInfo.setParams({ "language" : language, "user": user });
        console.log('PT. MS Envio')
        eventoEnvioInfo.fire(); 
    },
     getNumPosiciones : function(component, event) {
        var action = component.get('c.hasSolicitudPendiente');        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //	Almacenamos el número de elementos en la solicitud                    
                component.set("v.numProduct", response.getReturnValue());                                    
            }                
        });
        $A.enqueueAction(action);  
        
    },

})