({
	close : function(component, event) {
        component.set("v.visible", false);
        component.set("v.vacio", false);
        component.set("v.error", false);
    }, handleGetDocData : function(component, event){
        debugger;
        component.set("v.visible", true);
        var numDocumento = event.getParam("numDocumento");
        var showAlbaranCMR = event.getParam("showAlbaranCMR");
        var showCertificadoAnalisis = event.getParam("showCertificadoAnalisis");
        var listData = [];
        if (showAlbaranCMR || showCertificadoAnalisis){
			var listData = [];
            if (showAlbaranCMR){
                listData.push({"label" : $A.get("$Label.c.RQO_lbl_cmrAlbaran"), "value" : "DN" + numDocumento});
            }
            if (showCertificadoAnalisis){
                listData.push({"label" : $A.get("$Label.c.RQO_lbl_certificadoDeAnalisis"), "value" : "AC" + numDocumento});
            }
            component.set("v.picklistData", listData);
        }else{
            component.set("v.vacio", true);
        }
        component.set("v.picklistData", listData);
		
    }, getDocData : function(component, event){
        debugger;
        this.showSpinner(component, 'slds-show', 'slds-hide');
        var user = component.get("v.user");
        var idAccount = user.idCliente;
        var selectedDoc = component.find("selectionDocument").get("v.value");
        var type = selectedDoc.substring(0, 2);
        var numDocumento = selectedDoc.substring(2, selectedDoc.length);

        var action = component.get('c.getDocumentData');
        action.setParams({"idCliente" : idAccount, "numDocumento" : numDocumento, "tipo" : type});
        action.setCallback(this, function(response) {
            this.showSpinner(component, 'slds-hide','slds-show');
            var state = response.getState();
            if (state === "SUCCESS") {
                var storedResponse = response.getReturnValue();
                if (storedResponse && storedResponse.length > 1 && storedResponse[0] != 'E' && storedResponse[0] != 'NOK' && storedResponse[2]){
                    try {
                        var byteString = atob(storedResponse[2]);
                        var ab = new ArrayBuffer(byteString.length);
                        var ia = new Uint8Array(ab);
                        for (var i = 0; i < byteString.length; i++) {
                            ia[i] = byteString.charCodeAt(i);
                        }	
                        var contentType = 'application/pdf';
                        var docName = 'document';
                        if (storedResponse.length > 2 && storedResponse[3]){
                            docName = storedResponse[3];
                        }
                        var blob = new Blob([ia], { type: contentType});                        
                        saveAs(window, blob,  docName + '.pdf');
                        
                    }catch(err) {
                        console.log('Error: ' + err.message);
                        component.set("v.error", true);
                    }
                }else{
                    component.set("v.error", true);
                }
            }else{
                component.set("v.error", true);    
            }
        });
        $A.enqueueAction(action);
    }, getInfo : function(component, event) {
        var language = event.getParam("language");
		var user = event.getParam("user");
        component.set("v.language", language);
        component.set("v.user", user);
	},showSpinner : function(component, classToAdd, classToremove){
        $A.util.removeClass(component.find('spinner'), classToremove);
        $A.util.addClass(component.find('spinner'), classToAdd);
    }
})