({
	close : function(component, event, helper) {
        helper.close(component, event);
    }, handleGetDocData : function(component, event, helper){
        helper.handleGetDocData(component, event);
    }, getDocData : function(component, event, helper){
        helper.getDocData(component, event);
    }, getInfo : function(component, event, helper) {
        helper.getInfo(component, event);
	}
})