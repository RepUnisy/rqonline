({
	setViewGraphicTons : function(component, event, helper) {
		helper.setViewGraphicTons(component, event);
	}, exportInTons : function(component, event, helper) {
		helper.exportInTons(component, event);
	}, cambiarPaginaArribaTons : function(component, event, helper) {
		helper.cambiarPaginaTons(component, event, true);
    }, cambiarPaginaAbajoTons : function(component, event, helper) {
		helper.cambiarPaginaTons(component, event, false);
    }, doInit : function(component, event, helper) {
		helper.inicio(component, event);
    }
})