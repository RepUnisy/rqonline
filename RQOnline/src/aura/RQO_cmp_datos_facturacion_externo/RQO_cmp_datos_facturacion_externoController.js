({
    init : function(component, event, helper) {
        
        var mapaObt = component.get("v.mapaResumenHeredada");
        var keyObt = component.get("v.idResumenHeredada");
        //	Obtenemos y asignamos la tabla
        var lista = mapaObt[keyObt];
        var destinoMercancias = '';
        var dirFacturacion = '';
        var incoterm;
        if (lista.length != 0){
            for (var i = 0; i<1; i++){
                destinoMercancias = lista[i].dirEnvio;
                dirFacturacion = lista[i].direccionFacturacion;
                incoterm = lista[i].incoterm;
            }
        }

        //	Obtenemos
        var usuario = component.get("v.userHeredado");
        var destino;
        var facturacion;
        debugger;
        if (usuario.idCliente == destinoMercancias){
            debugger;
            
             var nuevoObjeto = {name: usuario.cliente.Name, 
                                       nameOrg3: usuario.cliente.RQO_fld_name3__c, 
                                       calle: usuario.cliente.RQO_fld_calleNumero__c,
                                       city: usuario.cliente.RQO_fld_poblacion__c, 
                                       country: usuario.cliente.RQO_fld_pais__c,
                                       incoterm: incoterm,
                                       };
            destino = nuevoObjeto;

        }
        else{
            var listaDestinoMercancias = usuario.listDirMercancias;
            for(var i = 0; i< listaDestinoMercancias.length; i++ ){
                if (listaDestinoMercancias[i].RQO_fld_cliente__c == destinoMercancias){
                    debugger;
                    var nuevoObjeto = {name: listaDestinoMercancias[i].RQO_fld_cliente__r.Name, 
                                       nameOrg3: listaDestinoMercancias[i].RQO_fld_cliente__r.RQO_fld_name3__c, 
                                       calle: listaDestinoMercancias[i].RQO_fld_calleNumero__c,
                                       city: listaDestinoMercancias[i].RQO_fld_city__c, 
                                       country: listaDestinoMercancias[i].RQO_fld_country__c,
                                       incoterm: incoterm,
                                       };
                    destino = nuevoObjeto;
                }
            }
            debugger;            
        }
        //	Obtenemos la direccion de facturacion
        if (usuario.idCliente == dirFacturacion){
            var nuevoObjeto = {name: usuario.cliente.Name, 
                               cif: "", 
                               calle: usuario.cliente.RQO_fld_calleNumero__c,
                               city: usuario.cliente.RQO_fld_poblacion__c, 
                               country: usuario.cliente.RQO_fld_pais__c,
                               incoterm: incoterm,
                               };
            facturacion = nuevoObjeto;

        }
        else{
            var listaDirFacturacion = usuario.listDirFacturacion;
            for(var i = 0; i< listaDirFacturacion.length; i++ ){
                if (listaDirFacturacion[i].RQO_fld_cliente__c == dirFacturacion){
                     var nuevoObjeto = {name: listaDirFacturacion[i].RQO_fld_cliente__r.Name, 
                                       cif: "", 
                                       calle: listaDirFacturacion[i].RQO_fld_calleNumero__c,
                                       city: listaDirFacturacion[i].RQO_fld_city__c, 
                                       country: listaDirFacturacion[i].RQO_fld_country__c,
                                       incoterm: incoterm,
                                       };
                    facturacion = nuevoObjeto;
                }
            }
            debugger;            
        }
        
        
        component.set("v.dirFacturacion", facturacion);
        component.set("v.destinoMercancias", destino);
	}
})