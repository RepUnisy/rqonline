({
	doInit : function(component, event, helper) {
        var action = component.get("c.selectCommunicationById");
        debugger;
        action.setParams({
            commId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
            	debugger;
                var comm = response.getReturnValue();
                component.set("v.comm", comm);
            }
        });
        
        $A.enqueueAction(action);
        
        var action = component.get("c.selectImages");
        action.setParams({
            commId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var imgs = response.getReturnValue();
                component.set("v.images", imgs);
            }
        });
        $A.enqueueAction(action);
        
        var action = component.get("c.selectDocuments");
        action.setParams({
            commId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var atts = response.getReturnValue();
                component.set("v.attachments", atts);
            }
        });
        $A.enqueueAction(action);
    }
})