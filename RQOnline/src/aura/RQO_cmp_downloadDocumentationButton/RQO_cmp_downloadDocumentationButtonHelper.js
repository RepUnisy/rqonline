({
	getDocData : function(component, event) {
		this.showSpinner(component, 'slds-show', 'slds-hide');
		var idDocumentum = component.get("v.docId");
        var repository = component.get("v.repository");
        var docType = component.get("v.docType");
        var action = component.get('c.getDocDataValues');
        action.setParams({"idDocumentum" : idDocumentum, "repository" : repository, "docType" : docType});
        action.setCallback(this, function(response) {
            //store response state 
            var state = response.getState();
            var nok = false;
            var textAlert = '';
            var titleAlertValue = '';
            if (state === "SUCCESS") {
                this.showSpinner(component, 'slds-hide','slds-show');
                var storedResponse = response.getReturnValue();
                if (storedResponse && storedResponse.length > 2 && storedResponse[2]){
                    try {
                        var byteString = atob(storedResponse[2]);
                        var ab = new ArrayBuffer(byteString.length);
                        var ia = new Uint8Array(ab);
                        for (var i = 0; i < byteString.length; i++) {
                            ia[i] = byteString.charCodeAt(i);
                        }                        
                        var docName = ((storedResponse.length > 3 && storedResponse[3]) ?  storedResponse[3] : 'DOC' + new Date());
                        var docExtension = '';
                        var docType = ((storedResponse.length > 4 && storedResponse[4]) ?  storedResponse[4] : '');
                        var contentType = '';
                        if(docType && docType=='pdf'){
                            contentType = 'application/pdf';
                            docExtension = '.pdf';
                        }else if(docType && (docType == 'msw' || docType == 'msw8' || docType == 'docx')){
                            contentType = 'application/msword';
                        }else if(docType && docType=='txt'){
                            contentType = 'text/plain';
                            docExtension = '.txt';
                        }else{
                            contentType = 'application/octet-stream';
                            //docExtension = docType;
                        }						
                        var blob = new Blob([ia], { type: contentType});                        
                        saveAs(window, blob, docName + docExtension);
                        
                    }catch(err) {
                        console.log('Error: ' + err.message);
                        nok = true;
                        textAlert = $A.get("$Label.c.RQO_lbl_errorDescargaDocumento");
                        titleAlertValue = $A.get("$Label.c.RQO_lbl_error");
                    }
                }else if (storedResponse && storedResponse.length > 0 && storedResponse[0] && storedResponse[0] === 'ND' ){
                    nok = true;
                    textAlert = $A.get("$Label.c.RQO_lbl_noHayDocumento");
                    titleAlertValue = $A.get("$Label.c.RQO_lbl_error");
                }else{
                    nok = true;
                    textAlert = $A.get("$Label.c.RQO_lbl_errorDescargaDocumento");
                    titleAlertValue = $A.get("$Label.c.RQO_lbl_error");
                }
            }else{
                nok = true;
                textAlert = $A.get("$Label.c.RQO_lbl_errorDescargaDocumento");
                titleAlertValue = $A.get("$Label.c.RQO_lbl_error");
            }
            debugger;

            if (nok){
            	this.fireAlertEvent(textAlert, titleAlertValue);
            }else{
                var user = component.get("v.user");
                //Verificamos si el usuario esta logado y en caso de ser así lanzamos el registro de evento
                if(user && user.isPrivate){
                    this.registrarEvento(component);
                }
            }
        }); 
        // enqueue the Action   
        $A.enqueueAction(action);
    },
    showSpinner : function(component, classToAdd, classToremove){
        $A.util.removeClass(component.find('spinner'), classToremove);
        $A.util.addClass(component.find('spinner'), classToAdd);
    }, 
    registrarEvento: function(component){    	
    	var componentName = component.get("v.cmpInternalName");
    	var componentDescription = '';
        if (componentName == 'RQO_cmp_detalleProducto_4'){
            componentDescription = '1';
        }else if (componentName == 'RQO_cmp_popup_descargas'){
            componentDescription = '2';
        }
        /*
         * Registro del evento de descarga de documentación
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         * 	Valor 1 = Download Doc From Grade
         * 	Valor 2 = Download Doc From List Grade
         * actionType - Grupo al que pertenece, 1 = Product Catalogue
         * actionDescription - descripción de la acción que lanza el evento, en este caso el tipo de documento que 
         * se quiere descargar. Es un dato que llega desde un componente superior. Valores para este componente:
         * 	Valor Ficha Seguridad = Download security card
         * 	Valor Ficha técnica = Download technical note
         * 	Valor Certificado de cumplimento = Download certificate of compliance
         * reGradeId - id del grado que lanza el evento
         * reDocumentId - id del documento que se descarga
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
    	appEvent.setParams({"reComponentName" : componentName, "reComponentDescription" : componentDescription, 
    						"actionType" : '1', "actionDescription" : component.get("v.docDescriptionType"), 
							"reGradeId" : component.get("v.dwdGradeId"), "reDocumentId" : component.get("v.sfDocId"),
                            "rePathActual" : window.location.pathname});
		try{
    		appEvent.fire();
		}catch(err){
    		console.log(err.message);
		}
	},
    fireAlertEvent : function(textAlert, titleAlertValue){
        //Método genérico para lanzar el componente de alertas y confirmaciones
        //Le pasamos el mensaje, si es de confirmación o alerta, el texto de la cabecera y un identificador que utilizaremos 
        //para saber que funcionalidad ha lanzado el evento cuando recibamos respuesta
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : textAlert, "confirmAlert" : false, 
                            "titleAlertValue" : titleAlertValue, "idAlert" : 'downInvoice', "parentLE" : false});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }
})