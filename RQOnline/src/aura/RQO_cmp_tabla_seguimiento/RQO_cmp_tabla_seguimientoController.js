({
	getInfo : function(component, event, helper) {
		helper.getInfo(component, event);
	}, searchData : function(component, event, helper) {
		helper.searchData(component, event);
	}, agrupador : function(component, event, helper) {
        var listOrigen = component.get("v.resulsetData");
        helper.getOrderedData(component, listOrigen, true);
    },render : function(component, event, helper) {
        // window.scrollTo(0, 0);
        var listAgrupado = [];
        
        listAgrupado.push({"label" : $A.get("$Label.c.RQO_lbl_pedidoSeguimientoPreferentDate"), "value" : "srcFechaPreferente" });
        listAgrupado.push({"label" : $A.get("$Label.c.RQO_lbl_pedidoSeguimientoGrade"), "value" : "qp0gradeName" });         
        listAgrupado.push({"label" : $A.get("$Label.c.RQO_lbl_pedidoSeguimientoReferenciaCliente"), "value" : "numRefCliente" });
        listAgrupado.push({"label" : $A.get("$Label.c.RQO_lbl_pedidoSeguimientoDestination"), "value" : "destinoMercanciasNombre" });
        listAgrupado.push({"label" :  $A.get("$Label.c.RQO_lbl_status"), "value" : "statusDescription" });
        component.set("v.listAgrupador", listAgrupado);
        
    }, cambiarPagina : function(component, event, helper) {
		helper.cambiarPagina(component, event);
    }
})