({
    enviarCancelacion : function(component, event) {
        var selectedAsset = component.get("v.objData");
        component.set("v.cancelledId", selectedAsset.idAsset);
    }, changeCross : function(component, event) {
        //Capturamos con el onchange el cambio del atributo changeCancelStyle
       	var objActual = component.get("v.objData");
        var idActual = objActual.idAsset;
        var cancelSelected = component.get("v.changeCancelStyle"); 
        //Este onchange salta para todos los componentes hermanos que tenemos por pantalla. Comprobamos si es el componente afectado mediante
        //el id del activo que nos llega en changeCancelStyle. Si el ID de esta variable (el cancelado) es igual al Id del asset actual ocultamos
        //con el hideCross la linea y establecemos la cancelación por vista a true en el objeto. La cancelación por vista se establece 
        //para que el elemente permanezca oculto si se ejecuta cualquier acción que no implique una recarga de los datos
        if (idActual && cancelSelected && cancelSelected==idActual){
            component.set("v.hideCross", true);
            objActual.canceladoView = true;
            //Si hemos llegado hasta aquí y eliminado de pantalla el componente es porque se ha cancelado en SAP y por tanto se registra
            //el evento
            this.registrarEvento(objActual.idSolicitud, objActual.nameAsset);
        }
    }, registrarEvento: function(idSolicitud, searchData){
        /*
         * Registro del eventos de cancelación de posición de pedido
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         *  Valor 15 = Order modification (Cancel)
         * actionType - Grupo al que pertenece, 2 = Order
         * actionDescription - descripción de la acción que lanza el evento. En este caso consulta cancelación de posición de pedido. 
         * Valores para este componente:
         *  Valor 17 = Order modification
         * reRequestId - Solicitud cuya posición de solicitud se quiere cancelar
         * reSearchData - Name de la posición de solicitud que se va a cancelar
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_tablaSeguimiento', "reComponentDescription" : '15', 
                            "actionType" : '2', "actionDescription" : '17', "reRequestId" : idSolicitud,  
                            "reSearchData" : searchData, "rePathActual" : window.location.pathname});
        
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
 
})