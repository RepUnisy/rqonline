({
    doInit : function(component, event) {
        
        var language = component.get("v.language");  
        var idgrado = component.get("v.id");  
        
        var action = component.get('c.getDetalleGradoDescripcion');    
        action.setParams({ "lenguaje" : language, "idgrado" : idgrado });
        action.setCallback(this, function(response) {
            //store response state 
            var state = response.getState();
            console.log('state ' + state)
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();                
                component.set('v.descripcion', respuesta);                
            }
        });  
        // enqueue the Action   
        $A.enqueueAction(action);
       
    }
})