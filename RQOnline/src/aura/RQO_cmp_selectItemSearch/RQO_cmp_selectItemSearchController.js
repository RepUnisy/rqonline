({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},
    cambiarPicklistsProduct : function(component, event, helper){
        /*
        //var inputSelectProduct = component.find("inputSelectProduct");
        var inputSelectSegmento = component.find("inputSelectSegmento");
        var inputSelectAplicacion = component.find("inputSelectAplicacion");
        //component.set("v.producto", inputSelectProduct.get("v.value"));
        inputSelectSegmento.set("v.value", component.find("inputOptionTodosSegmento"));
        component.set("v.segmento",  inputSelectSegmento.get("v.value"));
        inputSelectAplicacion.set("v.value", component.find("inputOptionTodosAplicacion"));
        component.set("v.aplicacion",  inputSelectAplicacion.get("v.value"));
        */
        helper.cambiarPicklistsProducto(component, event);
    },
    cambiarPicklistsAplicacion : function(component, event, helper){
        var inputSelectSegmento = component.find("inputSelectSegmento");
        var inputSelectAplicacion = component.find("inputSelectAplicacion");
        component.set("v.aplicacion", inputSelectAplicacion.get("v.value"));
        inputSelectSegmento.set("v.value", component.find("inputOptionTodosSegmento"));
        component.set("v.segmento",  inputSelectSegmento.get("v.value"));
        helper.cambiarPicklistsAplicacion(component, event);
    },
    cambiarPicklistsSegmento : function(component, event, helper){
        var inputSelectSegmento = component.find("inputSelectSegmento");
        component.set("v.segmento", inputSelectSegmento.get("v.value"));
    	helper.cambiarPicklistsSegmento(component, event);
    },
    limpiarBusqueda : function(component, event, helper){
        //	Inicializamos los valores seleccionados a vacio
        component.set("v.txtWikiTextHeredada", null);
        component.set("v.producto", null);
        component.set("v.segmento", null);
        component.set("v.aplicacion", null);
        
        //	Insertamos los valores de las listas generico
        var listProductos = component.get("v.listaProductosCompleta");
        var lisSegmentos = component.get("v.listaSegmentosCompleta");
        var listAplicaciones = component.get("v.listaAplicacionesCompleta");
        component.set("v.listaProductos", listProductos);
        component.set("v.listaSegmentos",lisSegmentos);
        component.set("v.listaAplicaciones", listAplicaciones);
               
    },
    searchWikiOnButton : function(component, event, helper) {
        var eventoSearchBox = component.getEvent("busquedaGrado");
        eventoSearchBox.setParams({ "busqueda" : component.get("v.txtWikiTextHeredada") });
        eventoSearchBox.fire();
	}
})