({
	reCaptchaEvent : function(component, event, helper) {
		var vfOrigin = "https://" + window.location.hostname;
		// Capturar evento procedente del reCaptcha de la página visual force
        window.addEventListener("message", function(event) {
            if (event.origin !== vfOrigin) {
                // Not the expected origin: Reject the message!
                return;
            }
            
            if(event.data.action == component.get("v.reCaptchaMessage") && event.data.alohaResponseCAPTCHA == 'NOK'){
                component.set('v.textoAlerta', 'Debe validar el captcha');
	            component.set('v.showAlert',true);
            }
            else if(event.data.action == component.get("v.reCaptchaMessage") && event.data.alohaResponseCAPTCHA == 'OK'){
            	helper.enviarCorreo(component, event, component.get('v.Correo'), 
            						component.get('v.mensaje'), component.find("cbCopia").get("v.checked"));
            }
        }, false);
	},
    enviarCorreo : function(component, event, email, message, copyEmail) {
    
    	component.set("v.showSpinner",true);
        var action = component.get("c.sendRequest");
        action.setParams({
        	email: email,
        	message: message,
        	copyEmail: copyEmail
        });
 
        action.setCallback(this, function(response){
        	component.set("v.showSpinner",false);
            var state = response.getState();
            if (state === "SUCCESS") {
            	var btAccept = component.find("btAccept");
            	btAccept.set("v.disabled", true);
                component.set('v.showConfirm',true);
            }
            else {
            	component.set('v.textoAlerta', $A.get("$Label.c.RQO_lbl_errorGenerico"));
            	component.set('v.showAlert',true);
            }
        });
        
        $A.enqueueAction(action); 
    }
})