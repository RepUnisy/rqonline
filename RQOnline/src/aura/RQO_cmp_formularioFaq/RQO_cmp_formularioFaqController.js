({
	doInit : function(component, event, helper) {
		component.set('v.communityUrl', window.location.href.substring(0 ,window.location.href.indexOf('/s/')));
		helper.reCaptchaEvent(component, event, helper);
    },
	validate : function(component, event, helper) {
        var valPregunta = component.find("tbPregunta").get("v.validity");
        var valEmail = component.find("tbCorreo").get("v.validity");
        if (valPregunta.valid && valEmail.valid) {
        	var btAccept = component.find("btAccept");
        	btAccept.set("v.disabled", false);
        }
        else {
        	var btAccept = component.find("btAccept");
        	btAccept.set("v.disabled", true);
        }
    },
	envioCorreo : function(component, event, helper) {
	
		var valPregunta = component.find("tbPregunta").get("v.validity");
        var valEmail = component.find("tbCorreo").get("v.validity");
		var validityDerechosArco = component.find("cbDerechosARCO").get("v.validity");
		 
		if (valPregunta.valid && valEmail.valid && validityDerechosArco.valid) {
            debugger;
			var message = component.get("v.reCaptchaMessage");
			var vfOrigin = "https://" + window.location.hostname;
	        var vfWindow = component.find("vfFrame").getElement().contentWindow;
			vfWindow.postMessage({ action: message }, vfOrigin);
		 }
		 else
		 {
			 component.find("tbPregunta").showHelpMessageIfInvalid();
			 component.find("tbCorreo").showHelpMessageIfInvalid();
			 component.find("cbDerechosARCO").showHelpMessageIfInvalid();
			 var btAccept = component.find("btAccept");
			 btAccept.set("v.disabled", true);
		 }
	},
    toggleArco:function(component, event, helper) {
     	jQuery('.rq-tooltip-arco').slideToggle();
	},
})