({
    onRender : function(component, event){
     
         var slides = document.getElementsByClassName("slide").length;
         var candado = component.get('v.candado');
        
        if((slides > 0) && (candado == true)){
                jQuery('.rq-carrusel-home').bxSlider({
                    adaptiveHeight: false,
                    infiniteLoop: true,
                    hideControlOnEnd: true,
                    controls: true,
                    responsive: true,
                    pager:true,
                    auto:true
                    
                });
            component.set('v.candado',false);
        }

        

        //this.showSlides(sIndex,component);
     }, 
   	 nextSlides : function(component) {
     var sIndex = component.get("v.slideIndex");
     var sliders = document.querySelectorAll(".slide fade");
     var indice = parseInt(sIndex) + 1;
	 
       if(sliders.length == indice){
         component.set("v.slideIndex",indice);
         this.showSlides(indice,component); 
       }
       else{
         component.set("v.slideIndex",indice);
         this.showSlides(indice,component); 
       }

   },
    prevSlides : function(component) {
     var sIndex = component.get("v.slideIndex");
 	 var indice = parseInt(sIndex) - 1;
        if(indice >= 1)
        {
             component.set("v.slideIndex",indice);
             this.showSlides(indice,component);
        }
   },
  currentSlide :function(component) {
      
     var sIndex = component.get("v.slideIndex");
     var indice = event.target.id;
     component.set("v.slideIndex",indice);
     this.showSlides(indice,component);
   },
  showSlides : function(indice,component){
		 var i;
         var slides = document.getElementsByClassName("slide fade");
         var dots = document.getElementsByClassName("dot");
         var finish = component.get("v.finish");
         for (i = 0; i < slides.length; i++) {
             slides[i].style.display = "none";
         }
         for (i = 0; i < dots.length; i++) {
             dots[i].className = dots[i].className.replace(" active", ""); 
         } 
         if(slides.length > 0){
        	 slides[indice-1].style.display = "block";
         }
         if(dots.length > 0){
        	 dots[indice-1].className += " active";
         }
        if(i == slides.length && finish == false)
        {
            component.set("v.finish",true);
            this.timerSlider(component);
           
        }

    },
    timerSlider :function(component) {
       this.nextSlides(component);
        setInterval( this.timerSlider(component), 5000);
   },
})