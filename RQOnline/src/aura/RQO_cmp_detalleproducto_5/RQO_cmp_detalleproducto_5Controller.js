({
	doInit : function(component, event, helper) {
	},
    getInfo : function(component, event, helper) {
        if (component.get("v.id") != null){
            var today = new Date();
            component.set('v.fechaEnvio', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
            helper.getInfo(component, event);
        }
        
        
	},
    dependant : function(component, event, helper) {
        
        debugger; 
        
        var envase = component.get("v.envase");
        var mapModoEnvio = component.get("v.mapModoEnvio");
        var mapaIdEnvasesTP = component.get("v.mapaIdEnvasesTP");
        var mapaIdModosEnvio = component.get("v.mapaIdModosEnvio");
        var traduccionCode =  component.get("v.mapaTraduccionCofigo");
        var selectModoEnvio = [];
        
        
        var listModoEnvio = mapModoEnvio[mapaIdEnvasesTP[envase]];
        debugger;
        for(var i in listModoEnvio){
            var code = traduccionCode[listModoEnvio[i]] != null ? traduccionCode[listModoEnvio[i]] : listModoEnvio[i];
            selectModoEnvio.push({value : mapaIdModosEnvio[code], label : code});
        }
        
          debugger;    
        component.set("v.modoEnvio", selectModoEnvio.pop().value)
        component.set("v.selectModoEnvio", selectModoEnvio);
        
        
        
	},
    introducir : function(component, event, helper) {
			helper.introducir(component, event);
	},
    mostrarPopup : function(component, event, helper) {
			helper.mostrarPopup(component, event);
	},
    handleResponseAlert : function(component, event, helper){
        var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");
        this.showSpinner(component, 'slds-hide', 'slds-show');
    }, 
    showSpinner : function(component, classToAdd, classToremove){
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, classToremove);
        $A.util.addClass(spinner, classToAdd);
	},
    solicitar : function(component, classToAdd, classToremove){
        debugger;
                var url = "/contacto";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();     
    }
})