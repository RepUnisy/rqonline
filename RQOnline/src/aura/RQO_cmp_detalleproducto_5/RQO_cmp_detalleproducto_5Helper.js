({
    getInfo : function(component, event) { 
        var id = component.get("v.id");
        var action = component.get('c.getToCreateRequest');
        action.setParams({ "idGrado" : id });
        action.setCallback(this, function(response) { 
            debugger;
            var state = response.getState();
            if (state === "SUCCESS") {
               debugger;
                var pedido = JSON.parse(response.getReturnValue());
                debugger;
                var crearPedido = pedido.crearPedido;
                //var listaEnvases = pedido.listEnvases;
                //var listModoEnvio = pedido.listModoEnvio;
                debugger;
                var envase = pedido.envase;
                var modoEnvio = pedido.modoEnvio;
                var selectEnvases = pedido.selectEnvases;
                var selectModoEnvio = pedido.selectModoEnvio;
                var mapModoEnvio = pedido.mapModoEnvio;
                var mapaIdEnvases = pedido.mapaIdEnvases;
                var mapaIdEnvasesTP = pedido.mapaIdEnvasesTP;
                var mapaIdModosEnvio = pedido.mapaIdModosEnvio;                
                
                
                /*var mapa = new Map();
                debugger;
                for(var i in listaEnvases){
                    mapa.set(i, mapModoEnvio[listaEnvases[i]]);
                }*/       
                component.set("v.crearPedido", crearPedido);	
                component.set("v.mapModoEnvio", mapModoEnvio);
                component.set("v.envase", envase);
               // component.set("v.modoEnvio", modoEnvio);
                component.set("v.isCargado", true);
                component.set("v.mapaIdEnvases", pedido.mapaIdEnvases);
                component.set("v.mapaIdEnvasesTP", pedido.mapaIdEnvasesTP);
                component.set("v.mapaIdModosEnvio", pedido.mapaIdModosEnvio);
                component.set("v.mapaTraduccionCofigo", pedido.mapTradEnvaseUidadMedida);
                
                //component.set("v.listEnvases", listaEnvases);
                //component.set("v.listModoEnvio", mapa.get("0"));
                component.set("v.selectEnvases", selectEnvases);
                component.set("v.selectModoEnvio", selectModoEnvio);
            }
        }); 
        $A.enqueueAction(action);
        
    },
     mostrarPopup : function(component, event){
         component.set('v.visible',true);
     },
    introducir : function(component, event){
        debugger;
        var user = component.get("v.user");
        var idAccount = user.idCliente;
        var idContact = user.idContacto;
        var idGrado = component.get("v.id");
        
        var mapaIdEnvases = component.get("v.mapaIdEnvases");
        var mapaIdModosEnvio = component.get("v.mapaIdModosEnvio");
        
        var listEnvases = component.get("v.listEnvases");
        var envaseEnviar = component.get("v.envase");
        
        var mapModoEnvio = component.get("v.mapModoEnvio");
        var modoEnvioEnviar = component.get("v.modoEnvio");
        debugger;
               
        var fechaEnvio = component.get("v.fechaEnvio");
        var intCantidad = component.get("v.intCantidad");
        var action = component.get('c.insertAsset');
        action.setParams({ "idGrado" : idGrado, "AccountId" : idAccount, "ContactId" : idContact, "envase" : envaseEnviar, "modoEnvio" : modoEnvioEnviar,  "fechaEnvio" : fechaEnvio, "cantidad" : intCantidad });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var eventoNotificaciones = $A.get("e.c:RQO_evt_sumarUnoANotificaciones");
                eventoNotificaciones.setParams({});
                eventoNotificaciones.fire();
                var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
                appEvent.setParams({"textAlertValue" : 'Ha sido añadido correctamente', "confirmAlert" : false, 
                                    "titleAlertValue" : $A.get("$Label.c.RQO_lbl_GenericAlertTitle"), "idAlert" : 'gradoInsertado', "parentLE" : false});
                try{
                    appEvent.fire();
                }catch(err){
                    this.showSpinner(component);
                    console.log(err.message);
                }
            }else{
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        }); 
        $A.enqueueAction(action);
        
    }
    
})