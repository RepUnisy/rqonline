({
	goContact : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/contacto",
            "isredirect" : false
        });
        urlEvent.fire();
    }
})