({
    register : function(component, event){
		//	Registramos el contacto
    	var perfiles = component.get("v.perfiles");
		var perfilSeleccionadoTexto = perfiles[component.get("v.perfilSeleccionado")];
		
        var action = component.get("c.registerContact");
        action.setParams({
            idAccount : component.get("v.user.cliente.Id"),
            nombre : component.get("v.nombre"),
            primerApellido : component.get("v.primerApellido"),
            segundoApellido : component.get("v.segundoApellido"),
            email : component.get("v.email"),
            telefono : component.get("v.telefono"),
            perfil : perfilSeleccionadoTexto
        });
        action.setCallback(this, function(response){
        	component.set('v.textoAlerta','');
            var state = response.getState();
            if (state === "SUCCESS") {
            	component.set('v.visible',false);
            	component.set('v.textoAlerta',component.get("v.textoAlertaDefault"));
                component.set('v.showAlert',true);
            }
            else {
            	component.set('v.textoAlerta',response.getError()[0].message);
            	component.set('v.showAlert',true);
            }
        });
        
        $A.enqueueAction(action);
	},
     
    close : function(component, event){
		component.set('v.visible',false);
	},
})