({
    onSelectChange : function(component, event, helper) {
        var selected = component.find("selectionList").get("v.value");
        console.log('Dato Seleccionado: ' + selected);
        var listData = component.get("v.listLocaleData");
        var obj;
		for (var i = 0; i < listData.length; i++) {
  			obj = listData[i];
            if(obj.descriptionCode === selected){
                break;
            }
		}
		var appEvent = component.getEvent("changeLocale");
        appEvent.setParams({"dataLocaleDoc" : obj});
        try{
        	appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }
})