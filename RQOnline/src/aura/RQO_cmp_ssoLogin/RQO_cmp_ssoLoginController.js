({
	doInit : function(component, event, helper) {
		component.set('v.communityUrl', window.location.href.substring(0 ,window.location.href.indexOf('/s/')));
		
		//	Obtenemos los parámetros de la url
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); 
        var sURLVariables = sPageURL.split('&'); 
        var sParameterName;
        var mode = '';
        var samlContext = '';
        var spName = '';
        
        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); 
            if (sParameterName[0] === 'mode') {
                mode = sParameterName[1];
            }
            if (sParameterName[0] === 'samlContext') {
                samlContext = sParameterName[1];
            }
            if (sParameterName[0] === 'spName') {
                spName = sParameterName[1];
            }
        }
        
        component.set('v.mode', mode);
        component.set('v.samlContext', samlContext);
        component.set('v.spName', spName);
    }
})