({
	getSurvey : function(component, event) {
		//	Obtenemos próxima pregunta
        var action = component.get("c.selectLastActiveHome");
        action.setParams({
            perfilCliente : component.get("v.user.perfilCliente"),
            perfilUsuario : component.get("v.user.perfilUsuario"),
            cookies : document.cookie
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var question = response.getReturnValue();
                if (question)
                {
	    	    	var help = component.find("divMain");
	    	    	$A.util.toggleClass(help, "slds-show");
	    	    	
	                component.set("v.question", question);
	                
	                var action = component.get("c.selectSurveyById");
	                action.setParams({
			            idSurvey : component.get("v.question.RQO_fld_survey__c")
			        });
	                action.setCallback(this, function(response){
			            var state = response.getState();
			            if (state === "SUCCESS") {
			                var survey = response.getReturnValue();
			                component.set("v.survey", survey);
			            }
			        });
			        
			        $A.enqueueAction(action);
		        }
            }
        });
        
        $A.enqueueAction(action);
        
    },
	register : function(component, event){
		//	Registramos la respuesta
        var action = component.get("c.addResponse");
        
		var respuestas = component.get("v.question.Question_Responses__r");
		var respuestaSeleccionada;
		var idResponse;
		var numRespuestas = 0;
		if (respuestas)
			numRespuestas = respuestas.length;
		var i = 0;
		for (i = 0; i < numRespuestas; i++) {
			var checked = component.find("opcion")[i].elements[0].checked;
		    if (checked)
		    {
		    	idResponse = component.find("opcion")[i].elements[0].value;
		    	respuestaSeleccionada = i;
		    }
		}
        action.setParams({
            name : respuestas[respuestaSeleccionada].Name,
            idSurvey : component.get("v.question.RQO_fld_survey__c"),
            idQuestion : component.get("v.question.Id"),
            idResponse : idResponse
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	component.set("v.question", null);
            	var votar = component.find("btnVotar");
            	$A.util.toggleClass(votar, "slds-hide");
    	    	var help = component.find("divInfo");
    	    	$A.util.toggleClass(help, "slds-show");
            }
            else {

            }
        });
        
        $A.enqueueAction(action);
	},
})