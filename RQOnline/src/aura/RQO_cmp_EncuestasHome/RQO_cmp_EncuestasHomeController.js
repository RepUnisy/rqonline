({
    register : function(component, event, helper) {
        helper.register(component, event);
        document.cookie = "Survey-" + component.get("v.question.Id" + "=true");
    },
    getInfo : function(component, event, helper) {
        var user = event.getParam("user");
        component.set("v.user", user);
        helper.getSurvey(component, event);
    },
})