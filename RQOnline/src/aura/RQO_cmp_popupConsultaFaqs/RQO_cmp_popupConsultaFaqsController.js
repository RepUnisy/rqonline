({
    refresh : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
	},
	goHome : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/",
            "isredirect" : true
        });
        urlEvent.fire();
    }
})