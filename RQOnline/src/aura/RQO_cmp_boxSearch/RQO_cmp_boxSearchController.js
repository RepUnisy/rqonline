({
    searchWikiOnButton : function(component, event, helper) {
        var eventoSearchBox = component.getEvent("busquedaGrado");
        eventoSearchBox.setParams({ "busqueda" : component.get("v.txtWikiText") });
        eventoSearchBox.fire();
	},
    autocompletar : function(component, event, helper) {
        helper.autocompletar(component, event);
    },
    jsLoaded : function(component, event, helper) {
	},
    toggle : function(component, event, helper) {
        /*FRONTEND*/
        var btn = component.find('customSearch');
        $A.util.toggleClass(btn, 'visible');
        jQuery(".rq-filter-options").slideToggle();
	},
    rellenarBuscador : function(component, event, helper) {
        var texto = event.getParam("texto");
        console.log('rellenaBuscador');
        component.find('idTxtWikiText').set("v.value", texto);
        component.set("v.listaGradosCompleta", []);
	},
    vaciarLista : function(component, event, helper) {
        setTimeout(function(){ component.set("v.listaGradosCompleta", []); }, 150);
	} 
})