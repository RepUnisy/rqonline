({
    agregarSolicitud : function(component){
        var listPosition = component.get("v.plantillaHeredada.posicionPlantillaList");
        var user = component.get("v.userHeredado");
        var idCliente = user.idCliente;
        var idContacto = user.idContacto;
        
        var action = component.get('c.addRequest');       
        debugger;
        action.setParams({ listPosition : JSON.stringify(listPosition), AccountId : idCliente, ContactId : idContacto });
        
        action.setCallback(this, function(response){
            var state = response.getState();

            if (state === "SUCCESS") {
                //	Mostramos un mensaje indicando que se ha agregado correctamente
                var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
                appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_plantillaAnadidaExito"), "confirmAlert" : false, 
                                    "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), 
                                    "idAlert" : '', "parentLE" : false}); 
                try{
                    appEvent.fire();
                }catch(err){                
                    console.log(err.message);
                }
                
                //	Lanzamos un evento par aumentar el número de elementos de solicitud en las notificaciones
                var eventoNotificaciones = $A.get("e.c:RQO_evt_sumarUnoANotificaciones");
                eventoNotificaciones.setParams({"numSumar": response.getReturnValue()});
                eventoNotificaciones.fire();
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                      
        });
        $A.enqueueAction(action); 
        
    }
})