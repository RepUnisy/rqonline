({
    close : function(component, event){
		component.set('v.visible',false);
	},
    profileChange : function(component, profile) {
    	// Obtener listado de permisos
        var action = component.get("c.selectPermissionsByProfile");
        action.setParams({
            profile : profile
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var permissions = response.getReturnValue();
                component.set("v.permisos", permissions);
            }
        });
        
        $A.enqueueAction(action);
    },
})