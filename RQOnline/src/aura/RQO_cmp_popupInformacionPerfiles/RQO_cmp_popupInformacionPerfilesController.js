({
	doInit : function(component, event, helper) {
		helper.profileChange(component, component.get("v.perfilSeleccionado"));
	},
    close : function(component, event, helper) {
        helper.close(component, event);
	},
    profileChange : function(component, event, helper) {
    	var perfilSeleccionado = component.get("v.perfilSeleccionado");
    	helper.profileChange(component, perfilSeleccionado);
    },
})