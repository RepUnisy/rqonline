({
    close : function(component, event){
		this.fireResponseAlertEvent(component, false);
    }, handleShowAlert : function(component, event) {
        var textAlert = event.getParam("textAlertValue");
        var confirmAlert = event.getParam("confirmAlert");
        var titleAlertValue = event.getParam("titleAlertValue");
        var identificativo = event.getParam("idAlert");
        var parentPopup = event.getParam("parentLE");

        component.set("v.visible", true);
        component.set("v.textValue", textAlert);
        component.set("v.titleAlertValue", titleAlertValue);
        component.set("v.visibleAcceptButton", confirmAlert);
        component.set("v.idAlert", identificativo);
        
        if(parentPopup == true){
             var modalPopup = component.find('rqPopup');
              $A.util.addClass(modalPopup, 'forceStyles'); 
        }

    }, accept : function(component, event){
		this.fireResponseAlertEvent(component, true);
    }, 
    fireResponseAlertEvent : function(component, confirmAlertResponse){
        component.set("v.visible", false);
        var identificativo = component.get("v.idAlert");     
        var appEvent = $A.get("e.c:RQO_evt_ResponseAlert");
        appEvent.setParams({"responseAlert" : confirmAlertResponse, "idAlert" : identificativo});
        try{
            appEvent.fire();
            console.log('Ha lanzado el evento: ' + appEvent);
        }catch(err){
            console.log(err.message);
        }
    }
})