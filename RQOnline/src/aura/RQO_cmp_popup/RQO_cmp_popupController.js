({
    close : function(component, event, helper) {
        helper.close(component, event);
	}, handleShowAlert : function(component, event, helper) {
        helper.handleShowAlert(component, event);
    }, accept : function(component, event, helper) {
        helper.accept(component, event);
    }
})