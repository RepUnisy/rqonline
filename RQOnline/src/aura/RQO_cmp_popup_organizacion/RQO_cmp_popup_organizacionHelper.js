({
    close : function(component, event){
		component.set('v.visible',false);
	},
	changeAccount : function(component, event){
	
		component.set('v.visible',false);
		
		//	Cambiar el cliente
        var action = component.get("c.setUserAccount");
        action.setParams({
            accountId : component.find("slAccounts").get("v.value")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            }
        });
        
        $A.enqueueAction(action);

        //	Actualizamos componentes
        //$A.get('e.force:refreshView').fire();
	},
})