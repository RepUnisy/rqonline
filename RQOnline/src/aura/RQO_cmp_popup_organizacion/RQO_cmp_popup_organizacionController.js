({
    close : function(component, event, helper) {
        helper.close(component, event);
	},
	changeAccount : function(component, event, helper) {
        helper.changeAccount(component, event);
        location.reload();
	},
	getUserInfo:  function(component, event, helper) {
		var language = event.getParam("language");
        var user = event.getParam("user");
        
        component.set("v.language", language);
        component.set("v.user", user);
	},
})