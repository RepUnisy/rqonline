({
	 deleteQuestion :  function(component) {
        
        var idTemplate = component.get("v.item.identificador");
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : '¿Deseas eliminar la posición de plantilla?', "confirmAlert" : true, 
                            "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), 
                            "idAlert" : idTemplate, "parentLE" : false});
        try{
            appEvent.fire();
        }catch(err){
            this.showSpinner(component);
            console.log(err.message);
        }                
    },
    
    eliminarPlantilla : function(component, event) {
        
        var idTemplatePosition = component.get("v.item.identificador");
        
        var action = component.get('c.deleteTemplatePositionManagement');        
        action.setParams({ idTemplatePosition : idTemplatePosition });                
        
        action.setCallback(this, function(response){
            var state = response.getState();          
            if (state === "SUCCESS") {               
                $A.get('e.force:refreshView').fire();
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            
            $A.util.toggleClass(spinner, "slds-show");        
        });
        $A.enqueueAction(action); 
    },
    
    getListUnidadMedida : function (component, item){
       
        var mapaTrad =  component.get("v.mapTraduccionHeredado");
        
        var envaseKey = Array.from(mapaTrad.keys()).find(function (obj) { return JSON.parse(obj).value === item.envase; });
        
        
        //	Obtenemos la lista de unidades de medida
        var listaUnidadMedida = mapaTrad.get(envaseKey);
        
        return listaUnidadMedida;        
    }
})