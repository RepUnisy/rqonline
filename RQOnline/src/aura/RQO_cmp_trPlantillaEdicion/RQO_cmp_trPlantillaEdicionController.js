({
    doInit : function(component, event, helper) {     
       var a = component.get('v.item');
    },
	deleteQuestion : function(component, event, helper) {     
		helper.deleteQuestion(component, event);
	},
    handleResponseAlert : function(component, event, helper){
        var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");
        var idTemplate = component.get("v.item.identificador");
        
        if(responseAlert && responseAlert == true && idAlert && idAlert == idTemplate){
            helper.eliminarPlantilla(component);
        }        
    },
    changeEnvase : function(component, event, helper){
        var item = component.get("v.item");
        var envase = item.envase;
        for (var i = 0; i < item.listEnvases.length ; i ++){
            if (item.listEnvases[i].value == envase){
                item.envaseNombre = item.listEnvases[i].label;
            }            
        }     

        var listaUnidadMedida = helper.getListUnidadMedida(component, item);                
  
        var generamosNuevo = true;
        for (var i = 0; i <listaUnidadMedida.length ; i ++){
            if (listaUnidadMedida[i].value == item.unidadMedida){
                generamosNuevo = false;
            }            
        }   
        
        if (generamosNuevo){
            item.unidadMedida = listaUnidadMedida[0].value;
            item.unidadMedidaNombre = listaUnidadMedida[0].label;
        }
        
        
        item.listaUnidadMedida = listaUnidadMedida;
        component.set("v.item", item);
                
        
    },
    changeUnidadMedida : function(component, event, helper){
        var item = component.get("v.item");
        var unidadMedida = item.unidadMedida;
        for (var i = 0; i < item.listaUnidadMedida.length ; i ++){
            if (item.listaUnidadMedida[i].value == unidadMedida){
                item.unidadMedidaNombre = item.listaUnidadMedida[i].label;
            }            
        }        
    },
    changeDestino : function(component, event, helper){
        var listMercancias = component.get("v.userHeredado").listDirMercanciasSelect;
        var item = component.get("v.item");
        var destino = item.destino;
        for (var i = 0; i < listMercancias.length ; i ++){
            if (listMercancias[i].value == destino){
                item.destinoNombre = listMercancias[i].label;
            }            
        }        
    },
    changeFacturacion : function(component, event, helper){
        var listFacturacion = component.get("v.userHeredado").listDirFacturacionSelect;
        var item = component.get("v.item");
        var facturacion = item.facturacion;
        for (var i = 0; i < listFacturacion.length ; i ++){
            if (listFacturacion[i].value == facturacion){
                item.facturacionNombre = listFacturacion[i].label;
            }            
        }        
    },
    
    toggleSubItem : function(component, event, helper) {     
        /*FRONTEND*/
        var btnSubItem = event.currentTarget;
        jQuery(btnSubItem).addClass('thisItem').toggleClass('fa-chevron-down fa-chevron-up');
        jQuery('.thisItem.fa-chevron-up').closest('tr').find('.movil-hide').slideDown().addClass('rq-db');
        jQuery('.thisItem.fa-chevron-down').closest('tr').find('.movil-hide').slideUp(0).removeClass('rq-db');
        jQuery(btnSubItem).removeClass('thisItem');
    },
})