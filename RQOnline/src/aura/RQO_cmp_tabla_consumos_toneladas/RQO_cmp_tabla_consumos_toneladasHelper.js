({
	setViewGraphic : function(component, event) {
        var appEvent = component.getEvent("changeView");
        appEvent.setParams({"viewTableTons" : false});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }, exportInTons : function(component, event) {
        var appEvent = $A.get("e.c:RQO_evt_consumptionExport");
        appEvent.setParams({"exportType" : "tons"});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }
})