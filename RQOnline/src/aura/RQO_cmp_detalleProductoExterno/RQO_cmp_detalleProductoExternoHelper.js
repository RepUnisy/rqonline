({
    obtenerId : function(component) {
        //	Obtenemos los parámetros de la url
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); 
        var sURLVariables = sPageURL.split('&'); 
        var sParameterName;
        var idGrado = '';
        
        for (var i = 0; i < sURLVariables.length; i++) {
            console.log('todo '+ sURLVariables[i]);
            sParameterName = sURLVariables[i].split('='); 
            
            if (sParameterName[0] === 'id') {
                idGrado = sParameterName[1];
            }
        }
        component.set("v.id", idGrado);
    },     
    changeUser : function(component, event) {
        var user = component.get("v.user");
        var eventoLanzado = component.get("v.eventViewGrade");
        //Verificamos si el usuario esta logado y no se ha lanzado ya el evento y 
        //en caso de ser así lanzamos el registro de evento
        if(user && user.isPrivate && !eventoLanzado){
            var idGrado = component.get("v.id");
            this.registrarEvento(component, idGrado);
            component.set("v.eventViewGrade", true);
        }
    },
    registrarEvento: function(component, idGrado){
        /*
         * Registro del evento de visualización de la página de detalle de un producto.
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         * 	Valor 5 = Grade card
         * actionType - Grupo al que pertenece, 1 = Product Catalogue
         * actionDescription - descripción de la acción que lanza el evento. En este caso visualización del detalle de un 
         * producto.Valores para este componente:
         * 	Valor 3 = Query card
         * reGradeId - id del grado que lanza el evento
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_listaCategoriasTablaGrados', "reComponentDescription" : '5', 
                            "actionType" : '1', "actionDescription" : '3', "reGradeId" : idGrado,
                            "rePathActual" : window.location.pathname});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
    
})