({
    validarAgregarSolicitud : function(component) {
        var edicionNombre = component.get("v.edicionNombre");
        var edicion = component.get("v.edicion");
        debugger;
        if (edicionNombre || edicion){            
          //  component.find($A.get("$Label.c.RQO_lbl_anadirSolicitud")).set("v.disabled", true);
        }
        else{
           // component.find($A.get("$Label.c.RQO_lbl_anadirSolicitud")).set("v.disabled", false);
        } 
    },
    
    agregarSolicitud : function(component){
        
        var listPosition = component.get("v.plantillaHeredada.posicionPlantillaList");
        var user = component.get("v.userHeredado");
        var idCliente = user.idCliente;
        var idContacto = user.idContacto;
        
        var action = component.get('c.addRequest');  
        debugger;
        action.setParams({ listPosition : JSON.stringify(listPosition), AccountId : idCliente, ContactId : idContacto });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") { 
                //	Mostramos un mensaje indicando que se ha agregado correctamente
                var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
                appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_plantillaAnadidaExito"), "confirmAlert" : false, 
                                    "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), 
                                    "idAlert" : '', "parentLE" : false}); 
                try{
                    appEvent.fire();
                }catch(err){                
                    console.log(err.message);
                }
                
                //	Lanzamos un evento par aumentar el número de elementos de solicitud en las notificaciones
                var eventoNotificaciones = $A.get("e.c:RQO_evt_sumarUnoANotificaciones");
                eventoNotificaciones.setParams({"numSumar": response.getReturnValue()});
                eventoNotificaciones.fire();
				//Registramos el evento de añadir a solicitud
                this.registrarEvento(component);
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                      
        });
        $A.enqueueAction(action); 
        
    },
    
    guardarNombre : function(component) {          
        var idTemplate = 'id';
        var templateName = component.get("v.plantillaHeredada");
        
        
        var action = component.get('c.createOrModifyTemplate');        
        action.setParams({ idTemplate : templateName.identificador, templateName : templateName.nombre });
        
        
        action.setCallback(this, function(response){
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
                //  var listPosicionPedido = JSON.parse(response.getReturnValue());   
                debugger;
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                      
        });
        $A.enqueueAction(action); 
        
    },
    
    actualizarPosition : function (component){
        debugger;
        var posicionesPlantilla =  component.get("v.plantillaHeredada.posicionPlantillaList");
        
        var action = component.get('c.updatePositionTemplate');        
        action.setParams({ jsonPositionTemplata : JSON.stringify(posicionesPlantilla) });
        
        
        action.setCallback(this, function(response){
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
                //  var listPosicionPedido = JSON.parse(response.getReturnValue());   
                debugger;
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                      
        });
        $A.enqueueAction(action); 
        
    },
    deleteQuestion :  function(component) {
        debugger;
        var idTemplate = component.get("v.plantillaHeredada.identificador");
        var appEvent = $A.get("e.c:RQO_evt_ShowAlert");
        appEvent.setParams({"textAlertValue" : $A.get("$Label.c.RQO_lbl_mensajeEliminarPlantilla"), "confirmAlert" : true, 
                            "titleAlertValue" : $A.get("$Label.c.RQO_lbl_informacion"), 
                            "idAlert" : idTemplate, "parentLE" : false});
        try{
            appEvent.fire();
        }catch(err){
            this.showSpinner(component);
            console.log(err.message);
        }        
        
    },
    
    eliminarPlantilla : function(component) {
        
        var idTemplate = component.get("v.plantillaHeredada.identificador");
        var action = component.get('c.deteleTemplateManagement');        
        action.setParams({ idTemplate : idTemplate });
        
        action.setCallback(this, function(response){
            var state = response.getState();          
            if (state === "SUCCESS") {              
                $A.get('e.force:refreshView').fire();                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            
        });
        $A.enqueueAction(action); 
        
    }, registrarEvento: function(component){
        /*
         * Registro evento de uso de plantillas (Añadir a solicitud)
         * reComponentName - Nombre del componente que lanza el evento
         * reComponentDescription - Descripción corta de la funcionalidad del componente que lanza el evento. Es un picklist.
         * Valores para este componente:
         *  Valor 17 = Uso de plantillas
         * actionType - Grupo al que pertenece, 2 = Order
         * actionDescription - descripción de la acción que lanza el evento. En este caso uso de plantillas (Añadir a solicitud)
         * Valores para este componente:
         *  Valor 14 = Use of templates
         * reSearchData - Nombre de la template
         * rePathActual - Página actual
         * Los datos de componente se almacena para una posible gestión interna. Los datos de acción son los que se utilizan
         * en los informes
        */
        var plantilla = component.get("v.plantillaHeredada");
        var appEvent = $A.get("e.c:RQO_evt_registroEvento");
        appEvent.setParams({"reComponentName" : 'RQO_cmp_tablaSeguimiento', "reComponentDescription" : '17', 
                            "actionType" : '2', "actionDescription" : '14',  
                            "reSearchData" : plantilla.nombre, "rePathActual" : window.location.pathname});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
	}
})