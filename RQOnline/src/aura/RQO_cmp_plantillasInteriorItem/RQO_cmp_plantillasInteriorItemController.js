({
    agregarSolicitud : function(component, event, helper) {
       helper.agregarSolicitud(component);
    },
	editarNombre : function(component, event, helper) {
		component.set("v.edicionNombre", true);
        helper.validarAgregarSolicitud(component);
	},
    
    guardarNombre : function(component, event, helper) {
        debugger;
        component.set("v.edicionNombre", false);
        helper.guardarNombre(component);
        helper.validarAgregarSolicitud(component);
    },
    editar : function(component, event, helper) {
        component.set("v.edicion", true);
        helper.validarAgregarSolicitud(component);
    },
    guardar : function(component, event, helper) {
        component.set("v.edicion", false);
        helper.validarAgregarSolicitud(component);
        
        helper.actualizarPosition(component);
        
	}, 
    eliminarPlantilla  : function(component, event, helper) {
        debugger;
        helper.deleteQuestion(component);
    },
    handleResponseAlert : function(component, event, helper){
        debugger;
        var responseAlert = event.getParam("responseAlert");
        var idAlert = event.getParam("idAlert");
        var idTemplate = component.get("v.plantillaHeredada.identificador");
        
        if(responseAlert && responseAlert == true && idAlert && idAlert == idTemplate){
            helper.eliminarPlantilla(component);
        }        
    },
    toggleItem: function(component, event, helper) {
        /*FRONTEND*/
            var btnActivo = event.currentTarget;
            jQuery(btnActivo).addClass('thisbtn').toggleClass('fa-chevron-down fa-chevron-up');
            jQuery('.thisbtn').closest('.rq-custom-caption').next().slideToggle();
            jQuery(btnActivo).removeClass('thisbtn');
        
    },
    desplegarInfo: function(component, event, helper) {
        /*FRONTEND*/
        
        var enlaceActivo =  event.currentTarget;
        jQuery(enlaceActivo).addClass('thislink');
        jQuery('.last:not(.thislink)').siblings('.open').slideToggle().toggleClass('open');
        jQuery('.thislink + .rq-tooltip-grado').slideToggle().toggleClass('open');
        
        
        jQuery(enlaceActivo).removeClass('thislink');
        
    }

})