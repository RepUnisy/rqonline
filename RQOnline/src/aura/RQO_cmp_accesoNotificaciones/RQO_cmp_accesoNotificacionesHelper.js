({
    
    getNumPosiciones : function(component, event) {
        var action = component.get('c.hasSolicitudPendiente');        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //	Almacenamos el número de elementos en la solicitud                    
                component.set("v.numProduct", response.getReturnValue());                                    
            }                
        });
        $A.enqueueAction(action);  
        
    }
})