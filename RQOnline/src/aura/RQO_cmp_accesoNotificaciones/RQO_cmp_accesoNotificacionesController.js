({
    doInit : function(component, event, helper) {
    	var user = component.get('v.user');
        helper.getInfoHerencia(component, event, helper);
        
        helper.getNumPosiciones(component, event);   
    },
    sumarUnoANumProduct : function(component, event, helper){
        
        var limpiar = event.getParam("limpiarNotificaciones");
        if (limpiar){
            component.set("v.numNotificaciones", "0");
        }else{
            var numProduct = component.get("v.numProduct");
            var numSumar = event.getParam("numSumar");
            
            if (numSumar == 0){
                helper.getNumPosiciones(component, event);   
            }else{
                numProduct = numProduct + numSumar;
                component.set("v.numProduct", numProduct);
            }
		}
    },
    irASp : function(component, event, helper){
        var url = '/sp';
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        urlEvent.fire();
    }
})