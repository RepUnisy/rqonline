({
    doInit : function(component, event, helper) {
		helper.getInfoHerencia(component, event, helper);      
	},
    redireccion : function(component, event, helper) {		
        helper.redireccion(component, event);
	},
    
    getInfo : function(component, event, helper) {
        
        var language = event.getParam("language");
        var user = event.getParam("user");
        
        component.set("v.language", language);
        component.set("v.user", user);

        var url = '';
        if (user != null && user.isPrivate){
            url = "/hm";
        }
        else{
            url = "/catalogo";
        }
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        
        console.log('Redireccion Inicializa')
        if (component.get("v.redirigido") == false){
            component.set("v.redirigido", true);
            urlEvent.fire();
        }        
    },
    init : function (component, event, helper) {
        var val = event.getParam("value");
    }
})