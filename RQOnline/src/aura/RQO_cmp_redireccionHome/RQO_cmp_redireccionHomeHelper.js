({
	redireccion : function(component, event) {
        var url = '';
        var user = component.get("v.user");
        debugger;
        if (user != null && user.isPrivate){
            url = "/hm";
        }
        else{
            url = "/catalogo";
        }
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            "isredirect" : true
        });
        
        console.log('Redireccion Inicializa')
        if (component.get("v.redirigido") == false){
            component.set("v.redirigido", true);
            urlEvent.fire();
        }        
	}
})