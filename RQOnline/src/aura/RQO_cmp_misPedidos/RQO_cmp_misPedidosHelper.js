({
	getInfoPedidos : function(component) {
        var clientId = component.get("v.user.idCliente");
        var action = component.get('c.getNumPedidos');
        action.setBackground();
        action.setAbortable();
        action.setParams({"clientId" : clientId });
     
        action.setCallback(this, function(response){
            var state = response.getState();
            var error = true;
            if (state === "SUCCESS") {
 				var storedResponse = response.getReturnValue();
                if (storedResponse){
                    var objData = JSON.parse(storedResponse);
                    if (objData.executionCode === 'OK'){
                        component.set("v.cantidadRegistrado", objData.contadorRegistrado);
                        component.set("v.cantidadEnCurso", objData.contadorEnCurso);
                        component.set("v.cantidadConfirmado", objData.contadorConfirmado);
                        component.set("v.cantidadEnTransito", objData.contadorEnTransito);
                        error = false;
                    }
                }
            }else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            if (error){
                component.set("v.error", error);
            }
            //	Ocultamos el spinner de espera
            component.set("v.mostrarSpinner", false);
            
        });
        $A.enqueueAction(action);
	}
})