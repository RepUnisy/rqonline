({
	ordenacionEspecificaciones : function(component, event, helper) {
        var eventoOrdenarTabla = $A.get("e.c:RQO_evt_ordenarEspecificacionTabla");
        var titulo = component.get("v.titulo");
        eventoOrdenarTabla.setParams({ "especificacion" : component.get("v.idCategoria"),
                                      "nombre" : false,
                                      "idEvtCategoriaTabla" : component.get("v.idCategoriaTabla")});
       eventoOrdenarTabla.fire();
        
        /*PARTE FRONT-END*/
		 	var element = event.currentTarget;
        	var ordenacion = component.get('v.contadorOrdenacion');

			console.log('CRITERIO DE ORDENACION ACTUAL'+ ordenacion);
        
        
        
        	jQuery(element).addClass('active-order');
        	jQuery(element).closest('table').addClass('active-table');
        	//resetea estilos otras ordenaciones
        	
        
       	 	var indice = jQuery('.active-order').index();
        	//component.set('v.OrdenacionActual',indice);

       	 setTimeout(function(){
            jQuery('.active-table td').removeClass('sortable-active');
        	jQuery('.active-table th').removeClass('sortable-active');        
        	jQuery('.active-table th span').removeClass('fa fa-caret-down fa-caret-up');
             
             jQuery('.active-order').addClass('sortable-active');

             if(ordenacion % 2 == 0)
             {
                 jQuery('.active-order span').removeClass('fa fa-caret-down');
                 jQuery('.active-order span').addClass('fa fa-caret-up');
             }
             else{
                 jQuery('.active-order span').removeClass('fa fa-caret-up');
                 jQuery('.active-order span').addClass('fa fa-caret-down');
             }
             
                jQuery('.active-table tbody td').each(function()
				{
                    
              	 if(jQuery(this).index() == indice)
				   {
               		 jQuery(this).addClass('sortable-active');
            	   }
				});  
                
        	 jQuery(element).removeClass('active-order');
       		 jQuery(element).closest('table').removeClass('active-table');
              }, 200);

        /* FIN FRONT-END */
	},
    doInit : function(component, event, helper){
        var titulo = component.get("v.titulo");

    },

      
})