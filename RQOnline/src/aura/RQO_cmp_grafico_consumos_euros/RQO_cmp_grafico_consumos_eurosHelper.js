({
	setViewGraphic : function(component, event) {
        debugger;
        var appEvent = component.getEvent("changeViewEuros");
        appEvent.setParams({"viewTableEuros" : true});
        try{
            appEvent.fire();
        }catch(err){
            console.log(err.message);
        }
    }, createGraphic : function(component, event) {
        jQuery("#grafico2").remove();
        jQuery('.canvas-container2').append(' <canvas id="grafico2" height="350"></canvas>');
        var ctx =jQuery("#grafico2");
        var labelsHeader = [];
        var headerList = component.get("v.headerListData");
        var bodyList = component.get("v.bodyListData");
        for (var i = 0; i<headerList.length; i++) {
            for (var z = 0; z< headerList[i].years.length; z++){
                labelsHeader.push(headerList[i].month + ' ' + headerList[i].years[z]);
            }
        }

        var listData = [];
        var objData = null;
        var datosPorFamilia = [];
        var objConsumos = null;
        for (var z = 0; z<bodyList.length; z++) {
            objConsumos = bodyList[z];
            datosPorFamilia = [];
            //Vamos a por los datos de la sublista
            for (var x = 0; x<objConsumos.dataList.length;x++){
                datosPorFamilia.push(objConsumos.dataList[x].importeSAP);
            }
        	objData = {label:objConsumos.translatedFamily, backgroundColor:this.getRandomColor(), data:datosPorFamilia};
            listData.push(objData);
        }
        
		var grafico = new Chart(ctx, {
             type: 'bar',
             data: {
             	labels: labelsHeader,
             	datasets: listData,
             },
             options:{
				maintainAspectRatio: false,
             }
		});
        
    }, getRandomColor : function() {
          var letters = '0123456789ABCDEF';
          var color = '#';
          for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
          }
          return color;
    }, createGroupedGraphic : function(component, event){
        debugger;
        var groupedChecked = component.find("acumulados").get("v.checked");
       	if (groupedChecked){
            this.groupedGraphic(component, event);
        }else{
            this.createGraphic(component, event);
        }
    }, groupedGraphic : function(component, event){    
        jQuery("#grafico2").remove();
        jQuery('.canvas-container2').append(' <canvas id="grafico2" height="350"></canvas>');
        var ctx =jQuery("#grafico2");
        var headerList = component.get("v.headerListData");
        var bodyList = component.get("v.bodyListData");
        var headerMap = new Map();
        for (var i = 0; i<headerList.length; i++) {
            for (var z = 0; z< headerList[i].years.length; z++){
                if (!headerMap.has(headerList[i].years[z])){
                    headerMap.set(headerList[i].years[z], headerList[i].years[z]);
                }
            }
        }
        var labelsHeader = [];
        var datosAcumulados = [];
        var objConsumos = null;
        var importeAcumulado = null;
        var objData = null;
        var listData = [];
        var language = component.get("v.language").replace('_', '-');
        for(var key of headerMap.keys()){
            labelsHeader.push(key);
            for (var z = 0; z<bodyList.length; z++) {
                objConsumos = bodyList[z];
                datosAcumulados = [];
                importeAcumulado = 0;
                //Vamos a por los datos de la sublista
                for (var x = 0; x<objConsumos.dataList.length;x++){
                    if (objConsumos.dataList[x].yearConsum == key && objConsumos.dataList[x].importe){
                        importeAcumulado = importeAcumulado + objConsumos.dataList[x].importeSAP;
                    }
                }
                datosAcumulados.push(importeAcumulado.toLocaleString(language));
                objData = {label:objConsumos.translatedFamily, backgroundColor:this.getRandomColor(), data:datosAcumulados};
                listData.push(objData);
            }
            
        }
        
        var grafico = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labelsHeader,
                datasets: listData,
            },
            options:{
                maintainAspectRatio: false,
            }
        });
    }
})