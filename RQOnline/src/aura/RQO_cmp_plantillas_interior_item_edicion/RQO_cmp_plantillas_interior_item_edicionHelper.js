({
	deletePosition : function(component, event) {
        debugger;
        var listado = component.get("v.posicionesPlantillaHeredada");       
        var id = event.getSource().get("v.name");
        var idTemplatePosition = listado[id].identificador;
        
        var action = component.get('c.deleteTemplatePositionManagement');        
        action.setParams({ idTemplatePosition : idTemplatePosition });
        
		              
        
        action.setCallback(this, function(response){
            var state = response.getState();          
            if (state === "SUCCESS") {
                alert('Eliminado')
                 $A.get('e.force:refreshView').fire();
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            
            $A.util.toggleClass(spinner, "slds-show");        
        });
        $A.enqueueAction(action); 
	}
})