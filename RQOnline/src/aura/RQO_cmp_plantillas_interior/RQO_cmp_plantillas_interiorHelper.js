({
   
    getTranslateEnvaseUnidad : function(component, helper) {
        
        var action = component.get('c.getEnvaseUnidadMedida');               
        
        action.setCallback(this, function(response){
            var state = response.getState();          
            if (state === "SUCCESS") {
                var obj = JSON.parse(response.getReturnValue());   
                var map = new Map(Object.entries(obj));
                //	Almacenamos las traducciones obtenidas
                component.set("v.mapTraduccion",  map);
                
                //	LLamamos a la funcion "getTemplates" para obtener las plantillas de pedido
                helper.getTemplates(component);           
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            
            //$A.util.toggleClass(spinner, "slds-show");        
        });
        $A.enqueueAction(action); 
        
    },
    
     getTemplates : function(component) {
        var idContacto = component.get("v.user.idContacto");              
        var actionTemplate = component.get('c.getTemplates');        
        actionTemplate.setParams({ idContact : idContacto });
        
        actionTemplate.setCallback(this, function(response){
            
            var state = response.getState();     
            
            if (state === "SUCCESS") {
                var listPosicionPedido = JSON.parse(response.getReturnValue());   
                               
                var mapaTrad =  component.get("v.mapTraduccion");
                for (var i = 0; i< listPosicionPedido.length; i++){
                    for (var j = 0; j < listPosicionPedido[i].posicionPlantillaList.length; j++){
                        var listaUnidadMedida = mapaTrad.get(listPosicionPedido[i].posicionPlantillaList[j].envase);
                        debugger;
                        listPosicionPedido[i].posicionPlantillaList[j].listaUnidadMedida = listaUnidadMedida;
                        debugger;
                    }                    
                }
                
                debugger;
                component.set("v.listPlantillas", listPosicionPedido);
                debugger;                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
                else if (state === "ERROR") {
                    var errors = response.getError()
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                        
        });
        $A.enqueueAction(actionTemplate); 
        
    }
})