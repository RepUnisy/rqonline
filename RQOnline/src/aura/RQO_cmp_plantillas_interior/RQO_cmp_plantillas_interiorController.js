({
	getInfoMenu : function(component, event, helper) {
		
        var language = event.getParam("language");
        var user = event.getParam("user");        
        component.set("v.language", language);
        component.set("v.user", user);
        
        helper.getTranslateEnvaseUnidad(component, helper);
        
	}
})