({
	goTo : function(component, event, helper) {
		var codeCategoria = component.get("v.categoriaCodeHeredada");
		var id = component.get("v.identificador");
        var url= ''; 
        switch(codeCategoria) {
            case $A.get("$Label.c.RQO_lbl_notifCodeProducto"):
                 url = "/catalogo";
                break;
            case $A.get("$Label.c.RQO_lbl_notifCodeEntrega"):
                 url = "/hp";
                break;
            case $A.get("$Label.c.RQO_lbl_notifCodeFactura"):
                url = "/fact";
                break;
            case $A.get("$Label.c.RQO_lbl_notifCodeCliente"):
               
                break;
            case $A.get("$Label.c.RQO_lbl_notifCodeComunicacion"):
                url = "/visor-comunicaciones-multimedia?id=" + id;
                break;
            default:                
        }
        
        if (url != ''){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url,
                "isredirect" : true
            });
            urlEvent.fire();     
        }
    }
})