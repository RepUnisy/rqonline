({
	getSurvey : function(component, event) {
		//	Obtenemos los parámetros de la url
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); 
        var sURLVariables = sPageURL.split('&'); 
        var sParameterName;
        var idSurvey = '';
        
        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); 
            if (sParameterName[0] === 'id') {
                idSurvey = sParameterName[1];
            }
        }

        var action = component.get("c.selectSurveyById");
        action.setParams({
            idSurvey : idSurvey
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var survey = response.getReturnValue();
                if (survey)
                {
                    component.set("v.survey", survey);
                	var action = component.get("c.selectQuestions");
			        action.setParams({
			            idSurvey : idSurvey
			        });
			        action.setCallback(this, function(response){
			            var state = response.getState();
			            if (state === "SUCCESS") {
			                var questions = response.getReturnValue();
			                component.set("v.questions", questions);
			            }
			        });
			        
			        $A.enqueueAction(action);
                }
            }
        });
        
        $A.enqueueAction(action);
	},
	register : function(component, event){
	
		var questions = component.get("v.questions");
		var numQuestions = questions.length;
		var responses = [];
		for (var i = 0; i < numQuestions; i++) {
			var numResponses = questions[i].Question_Responses__r.length;
			for (var j = 0; j < numResponses; j++) {
				responses.push(questions[i].Question_Responses__r[j]);
			}
		}

		var action = component.get("c.addResponses");
		action.setParams({
			surveyId : component.get("v.survey.Id"),
            jsonResponses : JSON.stringify(responses)
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	var votar = component.find("btnEnviar");
            	$A.util.toggleClass(votar, "slds-hide");
    	    	component.set('v.textoAlerta',component.get("v.textoAlertaDefault"));
                component.set('v.showAlert',true);
            }
            else {
            	component.set('v.textoAlerta',response.getError()[0].message);
            	component.set('v.showAlert',true);
            }
        });
        
        $A.enqueueAction(action);
	},
})