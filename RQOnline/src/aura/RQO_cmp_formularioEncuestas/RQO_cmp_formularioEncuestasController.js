({
	doInit : function(component, event, helper) {
		helper.getSurvey(component, event);
    },
    register : function(component, event, helper) {
        helper.register(component, event);
    },
})