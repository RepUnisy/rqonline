({
    init : function(component, event, helper) {
        helper.init(component, event);
    },
    toggleTabla : function(component, event, helper) {
        helper.toggleTabla(component,event);
    }, volverSolicitar : function(component, event, helper){
        helper.volerSolicitar(component, event);
    }, openDownloadDocTable : function (component, event, helper){
        helper.openDownloadDocTable(component, event);
    }
})