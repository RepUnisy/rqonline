<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>ITPM_Help</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object stores the success criteria of the projects.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>ITPM_FOR_Weigh_Score__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(ITPM_SEL_Criterion_final_score__c, &apos;1&apos;), ITPM_PER_Weighing__c* 1, IF(ISPICKVAL(ITPM_SEL_Criterion_final_score__c, &apos;2&apos;), ITPM_PER_Weighing__c* 2, IF(ISPICKVAL(ITPM_SEL_Criterion_final_score__c, &apos;3&apos;), ITPM_PER_Weighing__c* 3, IF(ISPICKVAL(ITPM_SEL_Criterion_final_score__c, &apos;4&apos;), ITPM_PER_Weighing__c* 4, IF(ISPICKVAL(ITPM_SEL_Criterion_final_score__c, &apos;5&apos;), ITPM_PER_Weighing__c* 5, 0)))))</formula>
        <label>Weigh Score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ITPM_PER_Weighing__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Among all PSC it must add up to 100</inlineHelpText>
        <label>Weighing (%)</label>
        <precision>3</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>ITPM_REL_Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>ITPM_Project__c</referenceTo>
        <relationshipLabel>Project Success Criteria</relationshipLabel>
        <relationshipName>Project_Success_Criteria</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ITPM_SEL_Criterion_final_score__c</fullName>
        <externalId>false</externalId>
        <label>Criterion Final Score</label>
        <picklist>
            <picklistValues>
                <fullName>1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>3</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>4</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ITPM_SEL_Matrix_column_position__c</fullName>
        <externalId>false</externalId>
        <label>Matrix column position</label>
        <picklist>
            <picklistValues>
                <fullName>PSC1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PSC2</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PSC3</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PSC4</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PSC5</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PSC6</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PSC7</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PSC8</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PSC9</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PSC10</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ITPM_TX_Criterion_description__c</fullName>
        <externalId>false</externalId>
        <label>Criterion Description</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ITPM_TX_Criterion_name__c</fullName>
        <externalId>false</externalId>
        <label>Criterion Name</label>
        <length>50</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ITPM_TX_Observations__c</fullName>
        <externalId>false</externalId>
        <label>Observations</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>ITPM_TX_Score_description_1__c</fullName>
        <externalId>false</externalId>
        <label>Score Description  - 1</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ITPM_TX_Score_description_2__c</fullName>
        <externalId>false</externalId>
        <label>Score Description - 2</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ITPM_TX_Score_description_3__c</fullName>
        <externalId>false</externalId>
        <label>Score Description - 3</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ITPM_TX_Score_description_4__c</fullName>
        <externalId>false</externalId>
        <label>Score Description - 4</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ITPM_TX_Score_description_5__c</fullName>
        <externalId>false</externalId>
        <label>Score Description - 5</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <label>Project Success Criteria</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>Todos</label>
    </listViews>
    <nameField>
        <displayFormat>PSC-{000}</displayFormat>
        <label>PSC Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Project Success Criteria</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>ITPM_TX_Criterion_name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ITPM_TX_Criterion_description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ITPM_PER_Weighing__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ITPM_FOR_Weigh_Score__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ITPM_REL_Project__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ITPM_TX_Observations__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>ITPM_SEL_Matrix_column_position__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>ITPM_TX_Criterion_name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ITPM_TX_Criterion_description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ITPM_PER_Weighing__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ITPM_SEL_Criterion_final_score__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ITPM_TX_Observations__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ITPM_TX_Criterion_name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ITPM_TX_Criterion_description__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ITPM_PER_Weighing__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ITPM_SEL_Criterion_final_score__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ITPM_TX_Observations__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>ITPM_TX_Criterion_name__c</searchFilterFields>
        <searchFilterFields>ITPM_TX_Criterion_description__c</searchFilterFields>
        <searchFilterFields>ITPM_PER_Weighing__c</searchFilterFields>
        <searchFilterFields>ITPM_SEL_Criterion_final_score__c</searchFilterFields>
        <searchResultsAdditionalFields>ITPM_TX_Criterion_name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ITPM_TX_Criterion_description__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ITPM_PER_Weighing__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ITPM_SEL_Criterion_final_score__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ITPM_TX_Observations__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <webLinks>
        <fullName>Next_PSC</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Next PSC</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!requireScript(&quot;/soap/ajax/22.0/connection.js&quot;)} 
{!requireScript(&quot;/soap/ajax/22.0/apex.js&quot;)} 

var haymas = sforce.apex.execute(&quot;ITPM_manageNext&quot;,&quot;hayMasElementos&quot;,{idobjeto:&quot;{! ITPM_Project_Success_Criteria__c.Id}&quot;,proyectId :&quot;{!  ITPM_Project_Success_Criteria__c.ITPM_REL_ProjectId__c  }&quot;});
if( haymas &gt; 0 ){ 
var resultado=sforce.apex.execute(&quot;ITPM_manageNext&quot;,&quot;doPasarSiguiente&quot;,{idobjeto:&quot;{! ITPM_Project_Success_Criteria__c.Id}&quot;,proyectId :&quot;{! ITPM_Project_Success_Criteria__c.ITPM_REL_ProjectId__c }&quot;}); 
window.top.location.href=&apos;/&apos;+resultado; 

} 
else{ 
alert(&apos;No more Project Success Criteria&apos;); 

}</url>
    </webLinks>
    <webLinks>
        <fullName>Previous_PSC</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Previous PSC</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!requireScript(&quot;/soap/ajax/22.0/connection.js&quot;)} 
{!requireScript(&quot;/soap/ajax/22.0/apex.js&quot;)} 

var haymas = sforce.apex.execute(&quot;ITPM_manageNext&quot;,&quot;hayMasElementosAnteriores&quot;,{idobjeto:&quot;{! ITPM_Project_Success_Criteria__c.Id}&quot;,proyectId :&quot;{! ITPM_Project_Success_Criteria__c.ITPM_REL_ProjectId__c }&quot;}); 
if( haymas &gt; 0 ){ 
var resultado=sforce.apex.execute(&quot;ITPM_manageNext&quot;,&quot;doPasarAnterior&quot;,{idobjeto:&quot;{! ITPM_Project_Success_Criteria__c.Id}&quot;,proyectId :&quot;{! ITPM_Project_Success_Criteria__c.ITPM_REL_ProjectId__c }&quot;}); 
window.top.location.href=&apos;/&apos;+resultado; 

} 
else{ 
alert(&apos;No more Project Success Criteria.&apos;); 

}</url>
    </webLinks>
</CustomObject>
