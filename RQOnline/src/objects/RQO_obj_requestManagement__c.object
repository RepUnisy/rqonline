<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>RQO_lghtPage_RequestManagement</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Entidad para la gestión de las solicitudes de pedido</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>RQO_fld_comment__c</fullName>
        <description>Campo texto para indicar comentarios respecto a la solicitud de pedido</description>
        <externalId>false</externalId>
        <label>Comment</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>RQO_fld_legalCondition__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Check para confirmar que se aceptan las condiciones legales</description>
        <externalId>false</externalId>
        <label>Legal Condition</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RQO_fld_requestStatus__c</fullName>
        <description>Estado de la solicitud</description>
        <externalId>false</externalId>
        <label>Estado de la solicitud</label>
        <picklist>
            <picklistValues>
                <fullName>IP</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>SE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CL</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>RQO_fld_sentToQp0__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Check para marcar que la solicitud ha sido enviada a QP0</description>
        <externalId>false</externalId>
        <label>Sent to QP0</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RQO_fld_step__c</fullName>
        <defaultValue>1</defaultValue>
        <description>Pasos de la solicitud</description>
        <externalId>false</externalId>
        <label>Step</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RQO_fld_stockManager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Cliente responsable de stock</description>
        <externalId>false</externalId>
        <label>Stock manager</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Request Management</relationshipLabel>
        <relationshipName>Request_Management</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>RQO_fld_taxReceivingCountry__c</fullName>
        <description>Pais receptor fiscal</description>
        <externalId>false</externalId>
        <globalPicklist>RQO_pl_TaxReceivingCountry</globalPicklist>
        <label>Pais receptor fiscal</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>RQO_fld_tipoPedido__c</fullName>
        <defaultValue>&quot;PS&quot;</defaultValue>
        <externalId>false</externalId>
        <label>Tipo Pedido</label>
        <length>2</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RQO_fld_type__c</fullName>
        <description>Campo texto para indicar comentarios respecto a la solicitud de pedido</description>
        <externalId>false</externalId>
        <label>Type</label>
        <picklist>
            <picklistValues>
                <fullName>PS</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>KB</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>RQO_fld_variasDirFacturacion__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Seleccionado si el usuario selecciona distintas direcciones de facturacion</description>
        <externalId>false</externalId>
        <label>Varias dir Facturación</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <gender>Masculine</gender>
    <label>Request Management</label>
    <listViews>
        <fullName>ALL</fullName>
        <columns>NAME</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>RQO_fld_sentToQp0__c</columns>
        <columns>OBJECT_ID</columns>
        <filterScope>Everything</filterScope>
        <label>ALL</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>{0000000000}</displayFormat>
        <label>Request Management Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Request Management</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
