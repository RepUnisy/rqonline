/*------------------------------------------------------------
Author:         Jon Ruiz
Company:        Minsait
Description:    Trigger de AM_TRG_Radar .
History
<Date>      <Author>            <Description>
02/01/2017  Jon Ruiz         Creación del Trigger
------------------------------------------------------------*/
trigger AM_TRG_Radar on SurveyTaker__c (after insert, after delete) {
    SurveyTaker__c newRadar = new SurveyTaker__c();        
    if (trigger.isInsert)
        newRadar = Trigger.new[0];
    else
        newRadar =  Trigger.old[0];
        
    List<SurveyTaker__c> lstRadar = [SELECT Id, Name FROM SurveyTaker__c WHERE AM_Estacion_de_servicio__c =: newRadar.AM_Estacion_de_servicio__c];
    if(newRadar.AM_Estacion_de_servicio__c!=null){
    AM_Estacion_de_servicio__c estacionServicio = [SELECT Id, Name, AM_NU_NumRadarAsoc__c FROM AM_Estacion_de_servicio__c WHERE Id =: newRadar.AM_Estacion_de_servicio__c];
        if(estacionServicio != null){
            estacionServicio.AM_NU_NumRadarAsoc__c = lstRadar.size();
            update (estacionServicio);
        }
    }
}