/*------------------------------------------------------------
Author:         Borja Martín
Company:        Indra
Description:    Trigger de ITPM_TRG_UBC_Distribution .
History
<Date>      <Author>            <Description>
10/09/2016  Borja Martín         Creación del Trigger
------------------------------------------------------------*/
trigger ITPM_TRG_UBC_Distribution on ITPM_UBC_distribution__c (before insert, before update) {
    if (Trigger.isInsert) {
        Decimal sumaPorcent = 0;
        if (Trigger.new[0].ITPM_REL_Opportunity__c != null){
            for (ITPM_UBC_distribution__c dis : Trigger.new) {
                List<ITPM_UBC_distribution__c> listDis = [SELECT ITPM_PER_Distribution__c FROM ITPM_UBC_distribution__c WHERE ITPM_REL_Opportunity__c =: dis.ITPM_REL_Opportunity__c];
                for (ITPM_UBC_distribution__c ld : listDis){
                    sumaPorcent += ld.ITPM_PER_Distribution__c;
                }
                sumaPorcent += Trigger.new[0].ITPM_PER_Distribution__c;
            }
        }else if (Trigger.new[0].ITPM_REL_Conceptualization__c != null){
            for (ITPM_UBC_distribution__c dis : Trigger.new) {
                List<ITPM_UBC_distribution__c> listDis = [SELECT ITPM_PER_Distribution__c FROM ITPM_UBC_distribution__c WHERE ITPM_REL_Conceptualization__c =: dis.ITPM_REL_Conceptualization__c];
                for (ITPM_UBC_distribution__c ld : listDis){
                    sumaPorcent = sumaPorcent + ld.ITPM_PER_Distribution__c;
                }    
                sumaPorcent += Trigger.new[0].ITPM_PER_Distribution__c;         
            }
        }
        if (sumaPorcent > 100){
            trigger.new[0].addError(Label.ITPM_CustomerUnit_error_message_percentExceeded);
        }
    } else if (Trigger.isUpdate) {
        Decimal sumaPorcent = 0;
        if (Trigger.new[0].ITPM_REL_Opportunity__c != null){
            for (ITPM_UBC_distribution__c dis : Trigger.new) {
                List<ITPM_UBC_distribution__c> listDis = [SELECT ITPM_PER_Distribution__c FROM ITPM_UBC_distribution__c WHERE ITPM_REL_Opportunity__c =: dis.ITPM_REL_Opportunity__c];
                for (ITPM_UBC_distribution__c ld : listDis){
                    sumaPorcent += ld.ITPM_PER_Distribution__c;
                }
                sumaPorcent += Trigger.new[0].ITPM_PER_Distribution__c;
                sumaPorcent -= Trigger.old[0].ITPM_PER_Distribution__c;
            }
        }else if (Trigger.new[0].ITPM_REL_Conceptualization__c != null){
            for (ITPM_UBC_distribution__c dis : Trigger.new) {
                List<ITPM_UBC_distribution__c> listDis = [SELECT ITPM_PER_Distribution__c FROM ITPM_UBC_distribution__c WHERE ITPM_REL_Conceptualization__c =: dis.ITPM_REL_Conceptualization__c];
                for (ITPM_UBC_distribution__c ld : listDis){
                    sumaPorcent = sumaPorcent + ld.ITPM_PER_Distribution__c;
                }    
                sumaPorcent += Trigger.new[0].ITPM_PER_Distribution__c; 
                sumaPorcent -= Trigger.old[0].ITPM_PER_Distribution__c;             
            }
        }
        if (sumaPorcent > 100){
            trigger.new[0].addError(Label.ITPM_CustomerUnit_error_message_percentExceeded);
        }
    }
}