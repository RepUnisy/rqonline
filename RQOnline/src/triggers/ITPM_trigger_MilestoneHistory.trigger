/*------------------------------------------------------------------------
Author:         Borja Martín Ballesteros
Company:        Indra
Description:    trigger
History
<Date>          <Author>                    <Description>
01-Jul-2016     Borja Martín Ballesteros   Initial version
----------------------------------------------------------------------------*/
trigger ITPM_trigger_MilestoneHistory on ITPM_Milestone_History__c (before insert) {
    List<ITPM_Milestone_History__c> lstmlhist = new List<ITPM_Milestone_History__c>();
    if(trigger.isbefore && trigger.isinsert){
        for (ITPM_Milestone_History__c mlhis: trigger.new){  
            lstmlhist = ITPM_Trigger_Logic.ITPM_Milestone_History_Tr_Logic(lstmlhist, mlhis);
        }
        delete lstmlhist;
    }
}