trigger AM_TRG_Sumatorio_AMe on AM_Accion_de_mejora__c (after insert, after delete) {
    
    if (trigger.isInsert)
        AM_Sumatorio_AM.Sumatorio(Trigger.new[0]);
   
    else
		if ( Trigger.isDelete)
			AM_Sumatorio_AM.Sumatorio(Trigger.old[0]);
    


}