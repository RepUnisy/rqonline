/*------------------------------------------------------------------------
Author:         Borja Martín Ballesteros
Company:        Indra
Description:    trigger
History
<Date>          <Author>        <Description>
20-Sep-2016     Borja Martín Ballesteros   Initial version
----------------------------------------------------------------------------*/
trigger ITPM_TRG_Cost_Item on ITPM_Budget_Item__c (before insert, before update, before delete, after insert, after update, after delete) {
    
    List <ITPM_Budget_Item__c> listBIs = new List <ITPM_Budget_Item__c>(); 
    boolean isDelete;
    System.debug('trigger nuevo arriba: ' + Trigger.new);
    System.debug('trigger viejo: ' + Trigger.old);
    
    if (trigger.isInsert || trigger.isUpdate){
        listBIs = Trigger.new;
    }
    else{
        listBIs = Trigger.old;
    }
    //  Comprobamos para que aplicación se ejecuta (itgpm - dcpai)
    boolean planInversion = false;
    for(ITPM_Budget_Item__c con : listBIs ){
        if (con.DCPAI_fld_Budget_Investment__c != null){
            planInversion = true;
            break;
        }             
    }
    if (trigger.isBefore) {
        
        if(!planInversion){
            // No se permite modificar ni eliminar un Cost Item asociado con un proyecto. 
            if(trigger.isDelete || trigger.isUpdate){
                ITPM_manageCostItems.controlModificationsInProject(listBIs, Trigger.oldMap);
            }
            
            // Modifica datos de entrada para adecuarlos a las lista de selección del sistema
            if (trigger.isInsert){
                ITPM_manageCostItems.matchFields(listBIs); 
            }
            
            // Dejamos comentado Change Request hasta retomarlo
            String resultado = ITPM_CRs_Operations.validateAlterBIs(listBIs);
            if (!resultado.equals('OK') ){       
                for (ITPM_Budget_Item__c bi : listBIs)
                    bi.addError(resultado);
            }
        }
        
    }
    
    // Unifica los costes de una carga masiva en registros únicos, en base a Proyecto, Tipo de Coste, Categoría, Año y País.    
    else if (trigger.isAfter){
        if(!planInversion){
            // Actualizamos los campos de Total Expenses y Total Investments en Opportunity y Conceptualization
            if (trigger.isDelete){
                System.debug('trigger nuevo en borrar: ' + Trigger.new);
                System.debug('trigger viejo en borr<r: ' + Trigger.old);              
                ITPM_manageCostItems.calculateTotals(Trigger.old, false, true);
            }else{System.debug('trigger nuevo: ' + Trigger.new);
                  ITPM_manageCostItems.calculateTotals(listBIs, false, false);
                 }
        }
        else{

        }
    }
    
}