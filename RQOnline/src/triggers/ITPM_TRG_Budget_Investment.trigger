/*------------------------------------------------------------
Author:         Borja Martín
Company:        Minsait
Description:    Trigger de ITPM_TRG_Budget_Investment .
History
<Date>      <Author>            <Description>
20/09/2016  Borja Martín         Creación del Trigger
------------------------------------------------------------*/
trigger ITPM_TRG_Budget_Investment on ITPM_Budget_Investment__c (before insert, before update) {
    RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Version__c' AND Name = 'Budget Investment' LIMIT 1];
    List<ITPM_Budget_Investment__c> listActualBI = [SELECT Name,
                                              ITPM_BI_TX_Budget_Investment_name__c,
                                              ITPM_BIA_TX_Business_sponsor__c,ITPM_BIA_TXA_Description__c,                                              
                                              ITPM_BIA_DT_Ended_date__c,ITPM_BIA_SEL_Investment_type__c,
                                              ITPM_BIA_SEL_IT_Area__c,ITPM_BIA_SEL_IT_Subarea__c,
                                              ITPM_BIA_REL_Responsible__c, ITPM_BI_SEL_Responsible_IT_Direction__c,
                                              ITPM_BIA_TX_Version__c                                              
                                              FROM ITPM_Budget_Investment__c];                                              
    List<ITPM_Version__c> listVersiones = new List<ITPM_Version__c>();
    if (Trigger.isInsert || Trigger.isUpdate) {
        for (ITPM_Budget_Investment__c binew : Trigger.new) {
            for (ITPM_Budget_Investment__c bi : listActualBI) {    
                // Comprobamos si el Budget Investment es distinto a los existentes, buscamos por ID y Version
                // Si es distinto lo versionamos y copiamos los nuevos valores en bi existente
                if(binew.Id == bi.Id && (binew.Name != bi.Name || binew.ITPM_BIA_TX_Version__c != bi.ITPM_BIA_TX_Version__c)){
                    ITPM_Version__c ver = new ITPM_Version__c();
                    ver.ITPM_VER_TX_Budget_Investment_name__c=bi.ITPM_BI_TX_Budget_Investment_name__c;
                    ver.ITPM_VER_TX_Business_sponsor__c=bi.ITPM_BIA_TX_Business_sponsor__c;
                    ver.ITPM_VER_TX_Description__c=bi.ITPM_BIA_TXA_Description__c;
                    ver.ITPM_VER_DT_Ended_date__c=bi.ITPM_BIA_DT_Ended_date__c;
                    ver.ITPM_VER_SEL_Investment_type__c=bi.ITPM_BIA_SEL_Investment_type__c;
                    ver.ITPM_VER_SEL_IT_Area__c=bi.ITPM_BIA_SEL_IT_Area__c;
                    ver.ITPM_VER_SEL_IT_Subarea__c=bi.ITPM_BIA_SEL_IT_Subarea__c;
                    ver.ITPM_VER_SEL_Responsible__c=bi.ITPM_BIA_REL_Responsible__c;
                    ver.ITPM_VER_TX_Version__c=bi.ITPM_BIA_TX_Version__c; 
                    ver.ITPM_VER_SEL_Responsible_IT_Direction__c=bi.ITPM_BI_SEL_Responsible_IT_Direction__c; 
                    ver.ITPM_VER_REL_Budget_Investment__c=bi.Id;
                    ver.RecordTypeId=rt.Id;
                    listVersiones.add(ver);
                    
                    bi.ITPM_BI_TX_Budget_Investment_name__c=binew.ITPM_BI_TX_Budget_Investment_name__c;
                    bi.ITPM_BIA_TX_Business_sponsor__c=binew.ITPM_BIA_TX_Business_sponsor__c;
                    bi.ITPM_BIA_TXA_Description__c=binew.ITPM_BIA_TXA_Description__c;
                    bi.ITPM_BIA_DT_Ended_date__c=binew.ITPM_BIA_DT_Ended_date__c;
                    bi.ITPM_BIA_SEL_Investment_type__c=binew.ITPM_BIA_SEL_Investment_type__c;
                    bi.ITPM_BIA_SEL_IT_Area__c=binew.ITPM_BIA_SEL_IT_Area__c;
                    bi.ITPM_BIA_SEL_IT_Subarea__c=binew.ITPM_BIA_SEL_IT_Subarea__c;
                    bi.ITPM_BI_SEL_Responsible_IT_Direction__c=binew.ITPM_BI_SEL_Responsible_IT_Direction__c;
                    bi.ITPM_BIA_REL_Responsible__c=binew.ITPM_BIA_REL_Responsible__c;
                    bi.ITPM_BIA_TX_Version__c=binew.ITPM_BIA_TX_Version__c;                                                                               
                }            
            }
        }
        if(listVersiones.size()>0) insert listVersiones;
    }
}