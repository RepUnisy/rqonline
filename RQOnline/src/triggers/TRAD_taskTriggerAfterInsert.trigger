/*------------------------------------------------------------------------
* 
*   @name:              TRAD_taskTriggerAfterInsert
*   @version:           2 
*   @description:       Juego de datos para las clases test
* 
*   Test Class:         TRAD_TEST_taskTriggerAfterInsert
*
*   Modificación:       Jackeline Piedrahita - Unisys
*             
----------------------------------------------------------------------------*/
trigger TRAD_taskTriggerAfterInsert on Task (after insert) {
    
    REP_cs_activacionTrigger__c customsetting = new REP_cs_activacionTrigger__c();    
    
    customsetting = REP_cs_activacionTrigger__c.getInstance(label.TRAD_lbl_taskTriggerAfterInsertCustomSettings);        
    
    //Ejecuta si está activo el trigger en el custom setting. El código que ejecute este trigger debe ir dentro de este IF.
    if(Customsetting.REP_fld_triggerActive__c){
    
        Boolean esRecordTypeTrading = false;
        
        //Busca el Id del Record Type de Trading del objeto TASK
        Id rt = [Select id from RecordType where sObjectType = 'Task' and developerName ='TRD_rt_task' LIMIT 1].Id;
       
        for (Task t : trigger.new){
            if(t.RecordTypeId == rt){
                esRecordTypeTrading = true;
                break;
            }
        }
          
        //Código que se ejecuta si es del Record Type de Trading
        If(esRecordTypeTrading){
            
            Set<Id> idSet = new Set<Id>();
            for(Task a:Trigger.New){
                idSet.add(a.Id);
            }
            String query = 'SELECT Id, Activity_Account__c, WhatId, WhoId FROM  Task WHERE Id IN: idSet';
            
            //Database.executeBatch(new TRAD_taskBatchClass (query, idSet));
        }   
               }          
}