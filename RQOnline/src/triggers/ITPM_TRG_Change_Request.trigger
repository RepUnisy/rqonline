Trigger ITPM_TRG_Change_Request on ITPM_Change_Request__c (before insert, before update, before delete, after insert, after update) {
    
    boolean doProjectOperations = false;  // Operaciones vinculadas a proyectos.
    
    if (Trigger.isBefore) {
        
        if (Trigger.isInsert) {
            if (!String.isBlank(Trigger.new[0].ITPM_REL_Project__c)) {
                doProjectOperations = true;
            }
        }
        else if (Trigger.isUpdate) {
            if (!String.isBlank(Trigger.new[0].ITPM_REL_Project__c) && !String.isBlank(Trigger.old[0].ITPM_SEL_Change_Subcategory__c)) {
                // Si se modifica la subcategoría, debemos realizar las mismas tareas que si se insertase desde cero.
                if (!Trigger.old[0].ITPM_SEL_Change_Subcategory__c.equals(Trigger.new[0].ITPM_SEL_Change_Subcategory__c))
                    doProjectOperations = true;
            }
        }
        else if (Trigger.isDelete) {
            
            for (ITPM_Change_Request__c cr : Trigger.old) {
                if (!cr.ITPM_SEL_Status__c.equals('Draft')) {
                    cr.addError('You cannot delete a finished Change Request.');
                }
            }
        }
    }
    else if (Trigger.isAfter) {
        
        if (Trigger.isInsert) {
            ITPM_CRs_Operations.cloneBudgetItems(Trigger.new);
        }
        else if (Trigger.isUpdate) {
            
            // Si se modifica la subcategoría, debemos realizar tareas en el CR.
            String oldCategory = Trigger.old[0].ITPM_SEL_Change_Subcategory__c;
            String newCategory = Trigger.new[0].ITPM_SEL_Change_Subcategory__c;
            
            if (oldCategory != newCategory) {
                
                // Si antes era de tipo "Time" y ahora incluye tipo "Cost", debemos insertar los BIs del proyecto
                if (oldCategory.equals('Time') && (newCategory.equals('Cost') || newCategory.equals('Time & Cost')))
                    ITPM_CRs_Operations.cloneBudgetItems(Trigger.new);
                
                // Si antes incluía tipo "Cost" y ahora sólo es tipo "Time", debemos borrar los BIs del CR.
                if (newCategory.equals('Time') && (oldCategory.equals('Cost') || newCategory.equals('Time & Cost')))
                    ITPM_CRs_Operations.deleteBudgetItems(Trigger.new);
            }
        }
    }
    
    // Operaciones en común para nuevos registros y cambios de categoría.
    if (doProjectOperations) {
        
        // Valida los condicionantes para insertar un nuevo CR.
        String resultInsert = ITPM_CRs_Operations.validateInsertCR(Trigger.new[0]);
        
        if (!resultInsert.equals('OK')) {
            Trigger.new[0].ITPM_SEL_Change_Subcategory__c.addError(resultInsert);
        }
        else if (!String.isBlank(Trigger.new[0].ITPM_SEL_Change_Subcategory__c)) {
            
            // Si todo es correcto, nos traemos las fechas del proyecto si corresponde.
            if (Trigger.new[0].ITPM_SEL_Change_Subcategory__c.equals('Time')
                || Trigger.new[0].ITPM_SEL_Change_Subcategory__c.equals('Time & Cost')) {
                
                Id idProyecto = Trigger.new[0].ITPM_REL_Project__c;
                List <ITPM_Project__c> proyectos = [
                    SELECT Id, ITPM_DT_Planned_Finish_Date__c
                    FROM ITPM_Project__c
                    WHERE Id = :idProyecto
                ];
                
                if (proyectos.size() > 0) {
                    //Trigger.new[0].ITPM_DT_PR_Go_Live_Plan_Date__c = proyectos[0].ITPM_Project_DAT_go_live_planned_date__c;
                    Trigger.new[0].ITPM_DT_PR_Project_Plan_Finish_Date__c = proyectos[0].ITPM_DT_Planned_Finish_Date__c;
                }
                
            }
            else {
                Trigger.new[0].ITPM_DT_PR_Go_Live_Plan_Date__c = null;
                Trigger.new[0].ITPM_DT_Go_Live_Planned_Date__c = null;
                Trigger.new[0].ITPM_DT_PR_Project_Plan_Finish_Date__c = null;
                Trigger.new[0].ITPM_DT_Project_Planned_Finish_Date__c = null;
            }
            
        } // FIN VALIDACIÓN CORRECTA.
        
    } // FIN OPERACIONES RELACIONADAS CON PROYECTOS.
    
}