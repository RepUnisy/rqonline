trigger QUIM_TRG_ProductMedia on ccrz__E_ProductMedia__c (after insert, after update) {

    list<ccrz__E_ProductMedia__c> media = [SELECT id, Idioma_del_documento__c, ccrz__Locale__c, ccrz__Product__c, ccrz__EndDate__c, ccrz__Sequence__c, ccrz__StartDate__c, ccrz__Enabled__c, Id_Documento__c, Tipo_documento__c, LastModifiedById, CreatedById, ccrz__ProductMediaId__c,  Name from ccrz__E_ProductMedia__c where id in :trigger.new];

    list<String>idiDoc = new list<String>{'Alemán','Árabe','Checo','Danés','Eslovaco','Español','Finlandés','Francés','Griego','Holandés',
    'Húngaro','Indio','Inglés','Italiano','Japonés','Lituano','Mandarín','Polaco','Portugués','Rumano','Ruso','Sueco','Turco'};
    
    list<String> locale= new list<string>{'de_DE','ar_MA','cs_CZ','da_DK','sk_SK','es_ES','fi_FI','fr_FR','el_GR','nl_NL','hu_HU','en_US',                                    'en_US','it_IT','en_US', 'lt_LT','zh_CN','pl_PL','pt_PT','ro_RO','ru_RU','sv_SE','tr_TR'};
    
    Map<String,String> idiomas = new Map<String,String>();

    for(integer i=0; i < idiDoc.size() ;i++){ idiomas.put(idiDoc[i],locale[i]); }

    if(trigger.isInsert){

        for(ccrz__E_ProductMedia__c ficha : media){ 

            ficha.ccrz__Locale__c=idiomas.get(ficha.Idioma_del_documento__c);
                
        }

        update media;
    }
    
    /*else if(trigger.isUpdate){
    

        //list<ccrz__E_ProductMedia__c> NuevoMedia = new list<ccrz__E_ProductMedia__c>();
        
        //NuevoMedia=media.deepClone(false, true, true);

        for(ccrz__E_ProductMedia__c ficha : media){ 

            ficha.ccrz__Locale__c=idiomas.get(ficha.Idioma_del_documento__c);
                
        }
       


        update media;
        //insert NuevoMedia;
      
    }*/
}