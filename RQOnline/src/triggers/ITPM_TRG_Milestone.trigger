/*------------------------------------------------------------
Author:         Borja Martín
Company:        Minsait
Description:    Trigger de ITPM_Milestone__c .
History
<Date>      <Author>            <Description>
20/09/2016  Borja Martín         Creación del Trigger
------------------------------------------------------------*/
trigger ITPM_TRG_Milestone on ITPM_Milestone__c (after insert, after update) {
    ITPM_Milestone__c m = Trigger.new[0];
    if(m!=null && m.ITPM_REL_PROJECT__c!=null){
        RecordType rtExecution = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name =: Label.ITPM_LABEL_PROJECT_EXECUTION LIMIT 1];
        RecordType rtExecutionWithProject = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name =: Label.ITPM_LABEL_PROJECT_EXECUTION + ' with Project' LIMIT 1];        
        RecordType rtExecutionWithIntegration = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name =: Label.ITPM_LABEL_PROJECT_EXECUTION + ' with Integration' LIMIT 1];
        RecordType rtExecutionWithIntegrationWithProject = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name =: Label.ITPM_LABEL_PROJECT_EXECUTION + ' with Project with Integration' LIMIT 1];        
        List <ITPM_Project__c> lstProjs = [SELECT Id, RecordTypeId, ITPM_REL_Program__c FROM ITPM_Project__c WHERE Id =: m.ITPM_REL_PROJECT__c];             
        List <ITPM_Project__c> lstProjsUpdates = new List <ITPM_Project__c>();
        for(ITPM_Project__c p : lstProjs){
            if(p.RecordTypeId==rtExecution.Id){
                p.RecordTypeId=rtExecutionWithProject.Id;
                lstProjsUpdates.add(p);
            }else if(p.RecordTypeId==rtExecutionWithIntegration.Id){
                p.RecordTypeId=p.RecordTypeId=rtExecutionWithProject.Id;
                lstProjsUpdates.add(p);
            }            
        }
        if(lstProjsUpdates.size()>0) upsert lstProjsUpdates;
    }
}