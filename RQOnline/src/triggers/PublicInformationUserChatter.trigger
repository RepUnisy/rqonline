trigger PublicInformationUserChatter on User (before insert, before update) {
    System.debug('Estamos dentro del trigger  PublicInformationUserChatter .');
    for (User user: Trigger.new) {  
        if (user.isActive)
        {   
            user.UserPreferencesShowEmailToExternalUsers = true;   
            user.UserPreferencesShowEmailToExternalUsers  = true;  
            user.UserPreferencesShowFaxToExternalUsers = true;  
            user.UserPreferencesShowManagerToExternalUsers = true;  
            user.UserPreferencesShowMobilePhoneToExternalUsers = true;  
            user.UserPreferencesShowStreetAddressToExternalUsers = true;  
            user.UserPreferencesShowWorkPhoneToExternalUsers = true;  
            
            user.UserPreferencesShowCityToExternalUsers = true ;
            user.UserPreferencesShowCountryToExternalUsers = true;          
            user.UserPreferencesShowPostalCodeToExternalUsers = true;
            user.UserPreferencesShowStateToExternalUsers   = true;
        }
    }            
}