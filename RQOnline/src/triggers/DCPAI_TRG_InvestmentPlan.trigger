/*------------------------------------------------------------------------ 
Author:         Alfonso Constán López 
Company:        Unisys
Description:    trigger
History
<Date>          <Author>                <Description>
01-Jun-2017     Alfonso Constán López   Initial version
----------------------------------------------------------------------------*/

trigger DCPAI_TRG_InvestmentPlan on DCPAI_obj_Investment_Plan__c (before insert, after insert) {
    system.debug('Entra en el trigger: DCPAI_TRG_InvestmentPlan ');
    List <DCPAI_obj_Investment_Plan__c > listBIs = new List <DCPAI_obj_Investment_Plan__c >();
    listBIs = Trigger.new;
        
    if (trigger.isInsert && trigger.isBefore){
        //  Comprobamos que solo exista un plan por año
        DCPAI_cls_generatePlan.controlRepeat(listBIs) ;
        
    }    
    if (trigger.isInsert && trigger.isAfter){
        //  Relaccionamos los cost item de la iniciativa con su respectivo plan
        DCPAI_cls_generatePlan.assignCostItem(listBIs) ;
    }
}