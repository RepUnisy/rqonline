/*------------------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    trigger
History
<Date>          <Author>        <Description>
31-Ago-2016     Rubén Simarro   Initial version
----------------------------------------------------------------------------*/
trigger ITPM_Trigger_Opportunity on ITPM_Opportunity__c (before insert, before update) {

     
    Map<String,Id> mapOpportunityRecordtypes = new Map<String,Id>();
        
    for(RecordType recordtype :[SELECT Id,name FROM RecordType WHERE SObjectType ='ITPM_Opportunity__c' ] ){
            
        if(!mapOpportunityRecordtypes.containsKey(recordtype.name)){  
            mapOpportunityRecordtypes.put(recordtype.name, recordtype.Id);     
        } 
    }
        
    if (Trigger.isInsert) {
                
        for (ITPM_Opportunity__c oppportunity: trigger.new) {          
 
            RecordType rt = new Recordtype(Id=mapOpportunityRecordtypes.get('ITPM Opportunity Recordtype Created'));        
            oppportunity.RecordTypeId = rt.Id;   
            oppportunity.ITPM_Opportunity_DT_State_date__c= System.today();
        } 
    }   
    else if (Trigger.isUpdate) {
    
        for (ITPM_Opportunity__c oppportunity: trigger.new) {  
                            
            //Si el usuario cambia el estado de la Oportunidad, hay que modificar su recordtype
            if ( trigger.oldMap.get(oppportunity.Id).ITPM_SEL_Status__c!= oppportunity.ITPM_SEL_Status__c){
        
                if(oppportunity.ITPM_SEL_Status__c == Label.ITPM_Opportunity_Status_Created){               
               
                    RecordType rt = new Recordtype(Id=mapOpportunityRecordtypes.get('ITPM Opportunity Recordtype Created'));           
                    oppportunity.RecordTypeId = rt.Id;                 
                    oppportunity.ITPM_Opportunity_DT_State_date__c= System.today();
                    oppportunity.ITPM_Opportunity_TX_Reject_reason__c='';
                }    
                else if(oppportunity.ITPM_SEL_Status__c == Label.ITPM_Opportunity_Status_In_progress){                              
                    if(oppportunity.ITPM_Opportunity_FOR_Status_nprogre_open__c){  
                    
                        //Solo usuarios con Profile "System Administrator" o PermissionSet "GPS_Admin" pueden volver atrás una opp en estado Finalized 
                        if(trigger.oldMap.get(oppportunity.Id).ITPM_SEL_Status__c == Label.ITPM_Idea_Status_Finalized){                       
                            boolean is_System_Admin = false;     
                            List<Profile> listProfilesSystemAdministrator = [Select Id from Profile where name in ('System Administrator','Administrador del sistema') ];                        
                            if(listProfilesSystemAdministrator != null && listProfilesSystemAdministrator.size()==1 && UserInfo.getProfileId() == listProfilesSystemAdministrator.get(0).Id ){            
                               is_System_Admin = true;
                            }
                            Id IDpermissionSetGPSAdmin = [Select Id from PermissionSet where name=:'PMO' and label='GPS_Admin'].Id;                   
                            boolean is_GSP_ADmin = false;
                            //recorro todos los permission set que tenga asignado el usuario, ya que pueden ser mas de uno, buscando el de GPS_Admin     
                            for(PermissionSetAssignment permissionX : [select PermissionSetId from PermissionSetAssignment where AssigneeId =: UserInfo.getUserId()] ){            
                                if(permissionX.PermissionSetId == IDpermissionSetGPSAdmin){
                                    is_GSP_ADmin = true;
                                }
                            }                                         
                            if(is_System_Admin || is_GSP_ADmin){                  
                                RecordType rt = new Recordtype(Id=mapOpportunityRecordtypes.get('ITPM Opportunity Recordtype In progress'));   
                                oppportunity.RecordTypeId = rt.Id;          
                                oppportunity.ITPM_Opportunity_DT_State_date__c= System.today();
                            }else{
                                trigger.new[0].addError(' You cannot alter an opportunity in status Finalized');
                            }
                        }else{
                            RecordType rt = new Recordtype(Id=mapOpportunityRecordtypes.get('ITPM Opportunity Recordtype In progress'));   
                            oppportunity.RecordTypeId = rt.Id;          
                            oppportunity.ITPM_Opportunity_DT_State_date__c= System.today();
                        }
                        
                    }
                    else{      
                        trigger.new[0].addError(Label.ITPM_Opportunity_error_message_field_not_filled+' "'+Label.ITPM_Opportunity_Status_In_progress+'"');      
                    }       
                } 
                else if(oppportunity.ITPM_SEL_Status__c == Label.ITPM_Opportunity_Status_Finalized){               
                      
                    if(oppportunity.ITPM_Opportunity_FOR_Status_Finaliz_open__c){
 
                        RecordType rt = new Recordtype(Id=mapOpportunityRecordtypes.get('ITPM Opportunity Recordtype Finalized'));   
                        oppportunity.RecordTypeId = rt.Id;               
                        oppportunity.ITPM_Opportunity_DT_State_date__c= System.today();
                    }   
                   else{ 
                        trigger.new[0].addError(Label.ITPM_Opportunity_error_message_field_not_filled+' "'+Label.ITPM_Opportunity_Status_Finalized+'"');       
                    }        
                }            
                else if(oppportunity.ITPM_SEL_Status__c == Label.ITPM_Opportunity_Status_Rejected){               
                   
                    if(oppportunity.ITPM_Opportunity_FOR_Status_Rejected_ope__c){
      
                        RecordType rt = new Recordtype(Id=mapOpportunityRecordtypes.get('ITPM Opportunity Recordtype Rejected'));   
                        oppportunity.RecordTypeId = rt.Id;                        
                        oppportunity.ITPM_Opportunity_DT_State_date__c= System.today();
                    }  
                    else{                  
                        trigger.new[0].addError(Label.ITPM_Opportunity_error_message_field_not_filled+' "'+Label.ITPM_Opportunity_Status_Rejected+'"');       
                    }
                }       
                else if(oppportunity.ITPM_SEL_Status__c == Label.ITPM_Opportunity_Status_Hibernated){               
                   
                    if(oppportunity.ITPM_Opportunity_FOR_Status_Hiberna_open__c){
      
                        RecordType rt = new Recordtype(Id=mapOpportunityRecordtypes.get('ITPM Opportunity Recordtype Hibernated'));   
                        oppportunity.RecordTypeId = rt.Id;                        
                        oppportunity.ITPM_Opportunity_DT_State_date__c= System.today();
                    }  
                    else{                 
                        trigger.new[0].addError(Label.ITPM_Opportunity_error_message_field_not_filled+' "'+Label.ITPM_Opportunity_Status_Rejected+'"');       
                    }
                }       
            }  
        
        }

    }  

}