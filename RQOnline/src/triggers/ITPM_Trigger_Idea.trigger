/*------------------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    trigger
History
<Date>          <Author>        <Description>
30-Ago-2016     Rubén Simarro   Initial version
----------------------------------------------------------------------------*/
trigger ITPM_Trigger_Idea on ITPM_Idea__c (before insert, before update) {
     
    Map<String,Id> mapIdeaRecordtypes = new Map<String,Id>();        
    for(RecordType recordtype :[SELECT Id,name FROM RecordType WHERE SObjectType ='ITPM_Idea__c' ] ){            
        if(!mapIdeaRecordtypes.containsKey(recordtype.name)){  
            mapIdeaRecordtypes.put(recordtype.name, recordtype.Id);     
        } 
    }
        
    if (Trigger.isInsert) {                
        for (ITPM_Idea__c idea: trigger.new) {                 
            RecordType rt = new Recordtype(Id=mapIdeaRecordtypes.get('ITPM Idea Recordtype Created'));        
            idea.RecordTypeId = rt.Id;   
            idea.ITPM_Idea_DT_State_date__c= System.today();
        } 
    }   
    else if (Trigger.isUpdate) {    
        for (ITPM_Idea__c idea: trigger.new) {                              
            //Si el usuario cambia el estado de la Idea, hay que modificar su recordtype
            if ( trigger.oldMap.get(idea.Id).ITPM_SEL_Progress_Degree__c!= idea.ITPM_SEL_Progress_Degree__c){        
                if(idea.ITPM_SEL_Progress_Degree__c == Label.ITPM_Idea_Status_Created){                              
                    RecordType rt = new Recordtype(Id=mapIdeaRecordtypes.get('ITPM Idea Recordtype Created'));           
                    idea.RecordTypeId = rt.Id;                 
                    idea.ITPM_Idea_DT_State_date__c= System.today();
                    idea.ITPM_Idea_TX_Reject_reason__c='';
                }    
                else if(idea.ITPM_SEL_Progress_Degree__c == Label.ITPM_Idea_Status_In_progress){                              
                    if(idea.ITPM_Idea_FOR_Status_in_progress_open__c){  
                        
                        //Solo usuarios con Profile "System Administrator" o PermissionSet "GPS_Admin" pueden volver atrás una idea en estado Finalized 
                        if(trigger.oldMap.get(idea.Id).ITPM_SEL_Progress_Degree__c == Label.ITPM_Idea_Status_Finalized){                       
                            boolean is_System_Admin = false;     
                            List<Profile> listProfilesSystemAdministrator = [Select Id from Profile where name in ('System Administrator','Administrador del sistema') ];                        
                            if(listProfilesSystemAdministrator != null && listProfilesSystemAdministrator.size()==1 && UserInfo.getProfileId() == listProfilesSystemAdministrator.get(0).Id ){            
                               is_System_Admin = true;
                            }
                            Id IDpermissionSetGPSAdmin = [Select Id from PermissionSet where name=:'PMO' and label='GPS_Admin'].Id;                   
                            boolean is_GSP_ADmin = false;
                            //recorro todos los permission set que tenga asignado el usuario, ya que pueden ser mas de uno, buscando el de GPS_Admin     
                            for(PermissionSetAssignment permissionX : [select PermissionSetId from PermissionSetAssignment where AssigneeId =: UserInfo.getUserId()] ){            
                                if(permissionX.PermissionSetId == IDpermissionSetGPSAdmin){
                                    is_GSP_ADmin = true;
                                }
                            }                                         
                            if(is_System_Admin || is_GSP_ADmin){                  
                                RecordType rt = new Recordtype(Id=mapIdeaRecordtypes.get('ITPM Idea Recordtype In progress'));   
                                idea.RecordTypeId = rt.Id;          
                                idea.ITPM_Idea_DT_State_date__c= System.today();                                
                            }else{
                                trigger.new[0].addError(' You cannot alter an idea in status Finalized');
                            }
                        }else{                        
                            RecordType rt = new Recordtype(Id=mapIdeaRecordtypes.get('ITPM Idea Recordtype In progress'));   
                            idea.RecordTypeId = rt.Id;          
                            idea.ITPM_Idea_DT_State_date__c= System.today();
                        }                        
                    }
                    else{      
                        trigger.new[0].addError(Label.ITPM_Idea_error_message_fields_not_filled+' "'+Label.ITPM_Idea_Status_In_progress+'"');      
                    }       
                } 
                else if(idea.ITPM_SEL_Progress_Degree__c == Label.ITPM_Idea_Status_Finalized){
                    if(idea.ITPM_Idea_FOR_Status_finalized_open__c){ 
                        RecordType rt = new Recordtype(Id=mapIdeaRecordtypes.get('ITPM Idea Recordtype Finalized'));   
                        idea.RecordTypeId = rt.Id;               
                        idea.ITPM_Idea_DT_State_date__c= System.today();
                    }   
                    else{ 
                        trigger.new[0].addError(Label.ITPM_Idea_error_message_fields_not_filled+' "'+Label.ITPM_Idea_Status_Finalized+'"');       
                    }        
                }            
                else if(idea.ITPM_SEL_Progress_Degree__c == Label.ITPM_Idea_Status_Rejected){                                  
                    if(idea.ITPM_Idea_FOR_Status_rejected_open__c){      
                        RecordType rt = new Recordtype(Id=mapIdeaRecordtypes.get('ITPM Idea Recordtype Rejected'));   
                        idea.RecordTypeId = rt.Id;                        
                        idea.ITPM_Idea_DT_State_date__c= System.today();
                    }  
                    else{                  
                        trigger.new[0].addError(Label.ITPM_Idea_error_message_fields_not_filled+' "'+Label.ITPM_Idea_Status_Rejected+'"');       
                    }
                }                     
            }  
        }
    }  
}