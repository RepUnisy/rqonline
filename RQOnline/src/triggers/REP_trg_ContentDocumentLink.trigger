trigger REP_trg_ContentDocumentLink on ContentDocumentLink (before insert) {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_trg_ContentDocumentLink';
        
    /**
     * Inicio
     */
    System.debug(CLASS_NAME + ': INICIO');
    
    // Comprobación del estado del trigger
    REP_cs_activacionTrigger__c pt = RQO_cls_CustomSettingUtil.getParametrizacionTrigger(CLASS_NAME);
	Boolean isActive = (pt != null ? pt.REP_fld_triggerActive__c : true);
    
    if (isActive) {
        for(ContentDocumentLink newLink : trigger.new) {
        	System.debug(CLASS_NAME + ': newLink ' + newLink);
            if(newLink.LinkedEntityId.getSObjectType() == RQO_obj_Communication__c.sObjectType) {
            	RQO_cls_handlerBeforeContentDocumentLink.handlerBeforeContentDocumentLink(newLink);
            } 
        } 
	}
    
    System.debug(CLASS_NAME + ': FIN');

}