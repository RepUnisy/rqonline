/*------------------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    trigger
History
<Date>          <Author>        <Description>
08-Ago-2016     Rubén Simarro   Initial version
----------------------------------------------------------------------------*/
trigger ITPM_Trigger_matrix_risk on ITPM_Matrix_Risk__c (before insert, before update, before delete, after insert) {
            
    if (Trigger.isBefore) {      
    
         if(Trigger.isInsert || Trigger.isUpdate){
                      
              for (ITPM_Matrix_Risk__c matrix_risk: trigger.new) {
                  
                    List<ITPM_Project_Success_Criteria__c> listaPSC = [select Id, ITPM_SEL_Matrix_column_position__c 
                                                                       from ITPM_Project_Success_Criteria__c 
                                                                       WHERE ITPM_REL_Project__c =: matrix_risk.ITPM_Matrix_risk_REL_Project__c];
       
                  if(listaPSC.size()==0){
                        trigger.new[0].addError(Label.ITPM_Trigger_Matrix_Risk_no_PSC_asociated);
                  }
                  
                  String projectPhase = [SELECT ITPM_Project_Phase__c FROM ITPM_Project__c WHERE id=: matrix_risk.ITPM_Matrix_risk_REL_Project__c].ITPM_Project_Phase__c;
          
                  System.debug('@@@ ITPM_Trigger_matrix_risk, before insert/update. Status proyect: '+ projectPhase);
              
                  if( projectPhase.equals('Canceled') || projectPhase.equals('Project Closure') || projectPhase.equals('Rejected')) {
                       trigger.new[0].addError(Label.ITPM_Trigger_Matrix_Risk_Operation_not_allowed);
                  }
              }
         }
    }
    else if (Trigger.isAfter) {      
            
        if(Trigger.isInsert){
               
            List<ITPM_Master_risk__c> listaMasterRisk = [SELECT id FROM ITPM_Master_risk__c];    
      
                for (ITPM_Matrix_Risk__c matrix_risk: trigger.new) {
                  
                    if(!matrix_risk.ITPM_Matrix_risk_BOL_Is_cloned__c){
                        
                    List <ITPM_Matrix_risk_row__c> listMatrixRiskRows = new List<ITPM_Matrix_risk_row__c>();

                    for(ITPM_Master_risk__c masterRisk_X : listaMasterRisk){
                          
                        ITPM_Matrix_risk_row__c newMatrixRiskRow = new ITPM_Matrix_risk_row__c(ITPM_Matrix_Risk__c=matrix_risk.Id, ITPM_Master_risk__c=masterRisk_X.Id ); 
                       
                       
                        for(ITPM_Project_Success_Criteria__c PSC : [select Id, ITPM_SEL_Matrix_column_position__c 
                                                                from ITPM_Project_Success_Criteria__c 
                                                                WHERE ITPM_REL_Project__c =: matrix_risk.ITPM_Matrix_risk_REL_Project__c]){
                      
                        if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC1')){         
                            newMatrixRiskRow.ITPM_CEP1__c = PSC.id;
                        }        
                        else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC2')){              
                            newMatrixRiskRow.ITPM_CEP2__c = PSC.id;
                        }        
                        else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC3')){    
                            newMatrixRiskRow.ITPM_CEP3__c = PSC.id;
                        }     
                        else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC4')){       
                            newMatrixRiskRow.ITPM_CEP4__c = PSC.id;
                        }  
                        else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC5')){    
                            newMatrixRiskRow.ITPM_CEP5__c = PSC.id;   
                        }  
                        else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC6')){  
                           newMatrixRiskRow.ITPM_CEP6__c = PSC.id;                           
                        }   
                        else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC7')){    
                             newMatrixRiskRow.ITPM_CEP7__c = PSC.id;       
                        } 
                        else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC8')){    
                              newMatrixRiskRow.ITPM_CEP8__c = PSC.id;    
                        }       
                        else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC9')){    
                             newMatrixRiskRow.ITPM_CEP9__c = PSC.id;   
                        }  
                        else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC10')){    
                            newMatrixRiskRow.ITPM_CEP10__c = PSC.id;           
                        }  
                    }  
                                            
                        listMatrixRiskRows.add(newMatrixRiskRow);
              
                    }       
                    insert listMatrixRiskRows;
                }
 
            }      
        }   
    } 
}