trigger PT_trigger_patrocinio on Patrocinios__c (before update, before insert) {

     
    if (Trigger.isInsert) {
          
        for (Patrocinios__c patrocinio: trigger.new) {    
             
            RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='patrocinios__c' AND Name = 'tipo registro consulta' LIMIT 1];   
            patrocinio.RecordTypeId = rt.Id;
            patrocinio.Area_Calculada__c = [Select name from UserRole where id=: Userinfo.getUserRoleId()].name;
        }
        
    }
                
    else if (Trigger.isUpdate) {

             for (Patrocinios__c patrocinio: trigger.new) {  
                   
                 if( trigger.oldMap.get(patrocinio.Id).formula_estado__c == Label.etiqueta_estado_cancelado){               
                     trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_1);
              
                 }
                 else if(patrocinio.formula_estado__c == Label.etiqueta_estado_programa_en_curso && trigger.oldMap.get(patrocinio.Id).formula_estado__c == Label.etiqueta_estado_programa_en_curso){                  
                     trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_2);    
                 }      
                 else if(patrocinio.formula_estado__c == Label.etiqueta_estado_pendiente_evaluacion && trigger.oldMap.get(patrocinio.Id).formula_estado__c == Label.etiqueta_estado_pendiente_evaluacion){                  
                     trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_3);    
                 }  
                 else if(patrocinio.formula_estado__c == Label.etiqueta_estado_programa_evaluado && trigger.oldMap.get(patrocinio.Id).formula_estado__c == Label.etiqueta_estado_programa_evaluado){                  
                     trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_4);    
                 }                 
                else if(patrocinio.formula_estado__c == Label.etiqueta_estado_contrato_firmado && trigger.oldMap.get(patrocinio.Id).formula_estado__c == Label.etiqueta_estado_contrato_firmado){                 
                   trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_59);
                }
                
                else if((patrocinio.formula_estado__c == Label.etiqueta_estado_programa_aprobado || patrocinio.es_renovacion__c)){               
        
                   //sección Descripción del Programa de Patrocinio
                   if ( trigger.oldMap.get(patrocinio.Id).Nombre__c!= patrocinio.Nombre__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_34);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).descripcion__c!= patrocinio.descripcion__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_35);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).Entidad_Patrocinada__c!= patrocinio.Entidad_Patrocinada__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_36);
                   }       
                   //seccion Criterios de selección acordes al programa
                   else if ( trigger.oldMap.get(patrocinio.Id).liderazgo__c!= patrocinio.liderazgo__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_37);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).puntuacion_liderazgo__c!= patrocinio.puntuacion_liderazgo__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_38);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).descripcion_liderazgo__c!= patrocinio.descripcion_liderazgo__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_39);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).Innovacion__c!= patrocinio.Innovacion__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_40);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).Puntuacion_Innovacion__c!= patrocinio.Puntuacion_Innovacion__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_41);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).Descripcion_Innovacion__c!= patrocinio.Descripcion_Innovacion__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_42);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).etica__c!= patrocinio.etica__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_43);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).puntuacion_etica__c!= patrocinio.puntuacion_etica__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_44);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).descripcion_etica__c!= patrocinio.descripcion_etica__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_45);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).calidad__c!= patrocinio.calidad__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_46);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).puntuacion_calidad__c!= patrocinio.puntuacion_calidad__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_47);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).descripcion_calidad__c!= patrocinio.descripcion_calidad__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_48);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).futuro__c!= patrocinio.futuro__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_49);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).puntuacion_futuro__c!= patrocinio.puntuacion_futuro__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_50);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).Descripcion_del_Futuro__c!= patrocinio.Descripcion_del_Futuro__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_51);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).Circunstancia_excepcional__c!= patrocinio.Circunstancia_excepcional__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_52);
                   }
                   else if ( trigger.oldMap.get(patrocinio.Id).justificacion_circunstancia_excepcional__c!= patrocinio.justificacion_circunstancia_excepcional__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_53);
                   }
                   //sección Público objetivo del programa de patrocinio
                   else if( trigger.oldMap.get(patrocinio.Id).publico_objetivo__c!= patrocinio.publico_objetivo__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_54);
                  } 
                   else if( trigger.oldMap.get(patrocinio.Id).Descripcion_publico_objetivo__c!= patrocinio.Descripcion_publico_objetivo__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_55);
                  } 
                  //seccion Ámbito geográfico
                    else if( trigger.oldMap.get(patrocinio.Id).ambito_geografico__c!= patrocinio.ambito_geografico__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_56);
                  }                   
                    else if( trigger.oldMap.get(patrocinio.Id).Detalle_del_emplazamiento__c!= patrocinio.Detalle_del_emplazamiento__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_60);
                  } 
                   //seccion Notoriedad del programa de patrocinio    
                  else if( trigger.oldMap.get(patrocinio.Id).Notoriedad__c!= patrocinio.Notoriedad__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_57);
                  }
                  else if( trigger.oldMap.get(patrocinio.Id).Detalle_notoriedad__c!= patrocinio.Detalle_notoriedad__c){
                        trigger.new[0].addError(Label.etiqueta_trigger_patrocinio_58);
                  }                   
                }   

             }      
        }
}