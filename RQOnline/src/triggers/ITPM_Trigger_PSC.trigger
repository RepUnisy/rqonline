/*------------------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    trigger
History
<Date>          <Author>        <Description>
08-Ago-2016     Rubén Simarro   Initial version
----------------------------------------------------------------------------*/
trigger ITPM_Trigger_PSC on ITPM_Project_Success_Criteria__c (before insert) {
  
    Map<String, Boolean> mapExistingPSC = new Map<String,Boolean>();
   
   
   
    if (Trigger.isBefore) {
       
        if(Trigger.isInsert){
                      
            for (ITPM_Project_Success_Criteria__c newPSC: trigger.new) {
               
           
                for(ITPM_Project_Success_Criteria__c PSC : [select id, ITPM_SEL_Matrix_column_position__c 
                                                FROM ITPM_Project_Success_Criteria__c 
                                                WHERE  ITPM_REL_Project__c =: newPSC.ITPM_REL_Project__c]){
               
                                                    if(!mapExistingPSC.containsKey(PSC.ITPM_SEL_Matrix_column_position__c)){

                                                        mapExistingPSC.put(PSC.ITPM_SEL_Matrix_column_position__c, true); 
                                                    }
                                                }
                
                if(!mapExistingPSC.containsKey('PSC1')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC1';
                }
                else if(!mapExistingPSC.containsKey('PSC2')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC2';
                }
                else if(!mapExistingPSC.containsKey('PSC3')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC3';
                }
                else if(!mapExistingPSC.containsKey('PSC4')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC4';
                }
                else if(!mapExistingPSC.containsKey('PSC5')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC5';
                }
                else if(!mapExistingPSC.containsKey('PSC6')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC6';
                }
                else if(!mapExistingPSC.containsKey('PSC7')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC7';
                }
                else if(!mapExistingPSC.containsKey('PSC8')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC8';
                }
                else if(!mapExistingPSC.containsKey('PSC9')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC9';
                }
                 else if(!mapExistingPSC.containsKey('PSC10')){
                    newPSC.ITPM_SEL_Matrix_column_position__c = 'PSC10';
                }
                else{  
                    trigger.new[0].addError('Criteria not created. Maximum number 10 criteria reached.');
                }
               
            }
        }        
    }   
}