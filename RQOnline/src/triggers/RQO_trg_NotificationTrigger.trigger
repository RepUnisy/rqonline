/**
 *	@name: RQO_trg_NotificationTrigger
 *	@version: 1.0
 *	@creation date: 08/02/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Trigger para el objeto RQO_obj_notification__c
 *	@testClass: 
*/
trigger RQO_trg_NotificationTrigger on RQO_obj_notification__c (before insert, after insert) {
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_trg_NotificationTrigger';
        
    /**
     * Inicio
     */
    System.debug(CLASS_NAME + ': INICIO');
    
    // Comprobación del estado del trigger
    REP_cs_activacionTrigger__c pt = RQO_cls_CustomSettingUtil.getParametrizacionTrigger(CLASS_NAME);
	Boolean isActive = (pt != null ? pt.REP_fld_triggerActive__c : true);
    
    if (isActive) {
        RQO_cls_NotificationTriggerHandler notificationHandler = RQO_cls_NotificationTriggerHandler.getInstance();
        if (trigger.isBefore){
            //Llamamos al handler del trigger para que realice la lógica necesaria antes de insertar el registro
            notificationHandler.beforeInsertExecution(trigger.new);
        }else if(trigger.isAfter){
            //Llamamos al handler del trigger para que realice la lógica necesaria tras insertar el registro
            notificationHandler.afterInsertExecution(trigger.new);
        }
    }
    
    System.debug(CLASS_NAME + ': FIN');
 	
}