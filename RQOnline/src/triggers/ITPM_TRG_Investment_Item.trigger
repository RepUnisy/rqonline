/*------------------------------------------------------------
Author:         Borja Martín
Company:        Minsait
Description:    Trigger de ITPM_TRG_Investment_Item .
History
<Date>      <Author>            <Description>
20/09/2016  Borja Martín         Creación del Trigger
------------------------------------------------------------*/
trigger ITPM_TRG_Investment_Item on ITPM_Investment_Item__c (before insert, before update) {
    RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Version__c' AND Name = 'Investment Item' LIMIT 1];
    List<ITPM_Investment_Item__c> listActualII = [SELECT Name, ITPM_CUR_Actual_Costs__c,
                                                    ITPM_TX_Alternatives__c,
                                                    ITPM_SEL_Area__c,
                                                    ITPM_Invest_REL_BI_that_belongs__c,
                                                    ITPM_TX_Cost_Description__c,
                                                    ITPM_SEL_Country__c,
                                                    ITPM_Invest_SEL_Customer_country__c,
                                                    ITPM_Invest_TXA_Description__c,
                                                    ITPM_TX_Dispute_Reason__c,
                                                    ITPM_Invest_DT_End_date__c,
                                                    ITPM_DT_Estimate_Date__c,
                                                    ITPM_CUR_Estimated_Cost__c,
                                                    ITPM_Invest_SEL_Execution_contry__c,
                                                    ITPM_Invest_NU_Expense__c,
                                                    ITPM_Invest_NU_Investiment__c,
                                                    ITPM_Invest_SEL_Investment_Category__c,
                                                    ITPM_II_TX_Investment_Item_Name__c,
                                                    ITPM_Invest_SEL_Investment_item_type__c,
                                                    ITPM_REL2_Project__c,
                                                    ITPM_Invest_REL_Responsible__c,
                                                    ITPM_Invest_SEL_Responsible_Area__c,
                                                    ITPM_Invest_SEL_Responsible_IT_Direction__c,
                                                    ITPM_Invest_SEL_Responsible_subare__c,
                                                    //ITPM_II_TX_Segmentation__c,
                                                    ITPM_Invest_DT_Start_date__c,
                                                    ITPM_Invest_TX_Version__c,
                                                    ITPM_Invest_SEL_Year_validity__c, 
                                                    ITPM_CUR_January_investment__c, ITPM_CUR_February_investment__c,
                                                    ITPM_CUR_March_investment__c, ITPM_CUR_April_investment__c,
                                                    ITPM_CUR_May_investment__c, ITPM_CUR_June_investment__c,
                                                    ITPM_CUR_July_investment__c, ITPM_CUR_August_investment__c, 
                                                    ITPM_CUR_September_investment__c, ITPM_CUR_October_investment__c,
                                                    ITPM_CUR_November_investment__c, ITPM_CUR_December_investment__c, 
                                                    ITPM_CUR_January_expense__c, ITPM_CUR_February_expense__c,
                                                    ITPM_CUR_March_expense__c, ITPM_CUR_April_expense__c,
                                                    ITPM_CUR_May_expense__c, ITPM_CUR_June_expense__c,
                                                    ITPM_CUR_July_expense__c, ITPM_CUR_August_expense__c, 
                                                    ITPM_CUR_September_expense__c, ITPM_CUR_October_expense__c,
                                                    ITPM_CUR_November_expense__c, ITPM_CUR_December_expense__c                                                                                                                                                     
                                              FROM ITPM_Investment_Item__c];                                              
    List<ITPM_Version__c> listVersiones = new List<ITPM_Version__c>();
    if (Trigger.isInsert || Trigger.isUpdate) {
        for (ITPM_Investment_Item__c iinew : Trigger.new) {
            for (ITPM_Investment_Item__c ii : listActualII) {    
                // Comprobamos si el Investment Item es distinto a los existentes, buscamos por ID y Version
                // Si es distinto lo versionamos y copiamos los nuevos valores en bi existente
                if(iinew.Id == ii.Id && (iinew.Name != ii.Name || iinew.ITPM_Invest_TX_Version__c != ii.ITPM_Invest_TX_Version__c)){
                    ITPM_Version__c ver = new ITPM_Version__c();
                    ver.ITPM_VER_CUR_Actual_Costs__c=ii.ITPM_CUR_Actual_Costs__c;
                    ver.ITPM_VER_LTXA_Alternatives__c=ii.ITPM_TX_Alternatives__c;
                    ver.ITPM_VER_SEL_Area__c=ii.ITPM_SEL_Area__c;
                    ver.ITPM_VER_LTXA_Cost_Description__c=ii.ITPM_TX_Cost_Description__c;
                    ver.ITPM_VER_SEL_Country__c=ii.ITPM_SEL_Country__c;
                    ver.ITPM_VER_SEL_Customer_country__c=ii.ITPM_Invest_SEL_Customer_country__c;
                    ver.ITPM_VER_TX_Description__c=ii.ITPM_Invest_TXA_Description__c;
                    ver.ITPM_VER_LTXA_Dispute_Reason__c=ii.ITPM_TX_Dispute_Reason__c;
                    ver.ITPM_VER_DT_End_date__c=ii.ITPM_Invest_DT_End_date__c;
                    ver.ITPM_VER_DT_Estimate_Date__c=ii.ITPM_DT_Estimate_Date__c;                     
                    ver.ITPM_VER_CUR_Estimated_Cost__c  =ii.ITPM_CUR_Estimated_Cost__c;
                    ver.ITPM_VER_SEL_Execution_country__c=ii.ITPM_Invest_SEL_Execution_contry__c;
                    ver.ITPM_VER_NU_Expense__c=ii.ITPM_Invest_NU_Expense__c;
                    ver.ITPM_VER_NU_Investment__c=ii.ITPM_Invest_NU_Investiment__c;
                    ver.ITPM_VER_SEL_Investment_category__c=ii.ITPM_Invest_SEL_Investment_Category__c;
                    ver.ITPM_VER_TX_Investment_Item_Name__c=ii.ITPM_II_TX_Investment_Item_Name__c;
                    ver.ITPM_VER_SEL_Investment_item_type__c=ii.ITPM_Invest_SEL_Investment_item_type__c;
                    ver.ITPM_VER_REL_Project__c=ii.ITPM_REL2_Project__c;
                    ver.ITPM_VER_SEL_Responsible__c=ii.ITPM_Invest_REL_Responsible__c;
                    ver.ITPM_VER_SEL_Responsible_Area__c=ii.ITPM_Invest_SEL_Responsible_Area__c;
                    ver.ITPM_VER_SEL_Responsible_IT_Direction__c=ii.ITPM_Invest_SEL_Responsible_IT_Direction__c;
                    ver.ITPM_VER_SEL_Responsible_Subarea__c=ii.ITPM_Invest_SEL_Responsible_subare__c;
                    ver.ITPM_VER_DT_Start_date__c=ii.ITPM_Invest_DT_Start_date__c; 
                    ver.ITPM_VER_TX_Version__c=ii.ITPM_Invest_TX_Version__c; 
                    ver.ITPM_VER_SEL_Year_validity__c=ii.ITPM_Invest_SEL_Year_validity__c;
                    ver.ITPM_VER_REL_BI_that_belongs__c=ii.ITPM_Invest_REL_BI_that_belongs__c;                                                            
                    ver.ITPM_VER_REL_Investment_Item__c=ii.Id;
                    //Expense & Investment
                    ver.ITPM_VER_CUR_January_investment__c=ii.ITPM_CUR_January_investment__c;
                    ver.ITPM_VER_CUR_February_investment__c=ii.ITPM_CUR_February_investment__c;
                    ver.ITPM_VER_CUR_March_investment__c=ii.ITPM_CUR_March_investment__c;
                    ver.ITPM_VER_CUR_April_investment__c=ii.ITPM_CUR_April_investment__c;
                    ver.ITPM_VER_CUR_May_investment__c=ii.ITPM_CUR_May_investment__c;
                    ver.ITPM_VER_CUR_June_investment__c=ii.ITPM_CUR_June_investment__c;
                    ver.ITPM_VER_CUR_July_investment__c=ii.ITPM_CUR_July_investment__c;
                    ver.ITPM_VER_CUR_August_investment__c=ii.ITPM_CUR_August_investment__c;
                    ver.ITPM_VER_CUR_September_investment__c=ii.ITPM_CUR_September_investment__c;
                    ver.ITPM_VER_CUR_October_investment__c=ii.ITPM_CUR_October_investment__c;
                    ver.ITPM_VER_CUR_November_investment__c=ii.ITPM_CUR_November_investment__c;
                    ver.ITPM_VER_CUR_December_investment__c=ii.ITPM_CUR_December_investment__c;
                    ver.ITPM_VER_CUR_January_expense__c=ii.ITPM_CUR_January_expense__c;
                    ver.ITPM_VER_CUR_February_expense__c=ii.ITPM_CUR_February_expense__c;
                    ver.ITPM_VER_CUR_March_expense__c=ii.ITPM_CUR_March_expense__c;
                    ver.ITPM_VER_CUR_April_expense__c=ii.ITPM_CUR_April_expense__c;
                    ver.ITPM_VER_CUR_May_expense__c=ii.ITPM_CUR_May_expense__c;
                    ver.ITPM_VER_CUR_June_expense__c=ii.ITPM_CUR_June_expense__c;
                    ver.ITPM_VER_CUR_July_expense__c=ii.ITPM_CUR_July_expense__c;
                    ver.ITPM_VER_CUR_August_expense__c=ii.ITPM_CUR_August_expense__c;
                    ver.ITPM_VER_CUR_September_expense__c=ii.ITPM_CUR_September_expense__c;
                    ver.ITPM_VER_CUR_October_expense__c=ii.ITPM_CUR_October_expense__c;
                    ver.ITPM_VER_CUR_November_expense__c=ii.ITPM_CUR_November_expense__c;
                    ver.ITPM_VER_CUR_December_expense__c=ii.ITPM_CUR_December_expense__c;
                    // Record Type
                    ver.RecordTypeId=rt.Id;
                    listVersiones.add(ver);
                    
                    ii.ITPM_CUR_Actual_Costs__c =iinew.ITPM_CUR_Actual_Costs__c;
                    ii.ITPM_TX_Alternatives__c =iinew.ITPM_TX_Alternatives__c;
                    ii.ITPM_SEL_Area__c =iinew.ITPM_SEL_Area__c;
                    ii.ITPM_Invest_REL_BI_that_belongs__c =iinew.ITPM_Invest_REL_BI_that_belongs__c;
                    ii.ITPM_TX_Cost_Description__c =iinew.ITPM_TX_Cost_Description__c;
                    ii.ITPM_SEL_Country__c =iinew.ITPM_SEL_Country__c;
                    ii.ITPM_Invest_SEL_Customer_country__c =iinew.ITPM_Invest_SEL_Customer_country__c;
                    ii.ITPM_Invest_TXA_Description__c =iinew.ITPM_Invest_TXA_Description__c;
                    ii.ITPM_TX_Dispute_Reason__c =iinew.ITPM_TX_Dispute_Reason__c;
                    ii.ITPM_Invest_DT_End_date__c =iinew.ITPM_Invest_DT_End_date__c;
                    ii.ITPM_DT_Estimate_Date__c =iinew.ITPM_DT_Estimate_Date__c;
                    ii.ITPM_CUR_Estimated_Cost__c =iinew.ITPM_CUR_Estimated_Cost__c;
                    ii.ITPM_Invest_SEL_Execution_contry__c =iinew.ITPM_Invest_SEL_Execution_contry__c;
                    ii.ITPM_Invest_NU_Expense__c =iinew.ITPM_Invest_NU_Expense__c;
                    ii.ITPM_Invest_NU_Investiment__c =iinew.ITPM_Invest_NU_Investiment__c;
                    ii.ITPM_Invest_SEL_Investment_Category__c =iinew.ITPM_Invest_SEL_Investment_Category__c;
                    ii.ITPM_II_TX_Investment_Item_Name__c =iinew.ITPM_II_TX_Investment_Item_Name__c;
                    ii.ITPM_Invest_SEL_Investment_item_type__c =iinew.ITPM_Invest_SEL_Investment_item_type__c;
                    ii.ITPM_Invest_REL_Responsible__c =iinew.ITPM_Invest_REL_Responsible__c;
                    ii.ITPM_Invest_SEL_Responsible_Area__c =iinew.ITPM_Invest_SEL_Responsible_Area__c;
                    ii.ITPM_Invest_SEL_Responsible_IT_Direction__c =iinew.ITPM_Invest_SEL_Responsible_IT_Direction__c;
                    ii.ITPM_Invest_SEL_Responsible_subare__c =iinew.ITPM_Invest_SEL_Responsible_subare__c;
                    ii.ITPM_Invest_DT_Start_date__c =iinew.ITPM_Invest_DT_Start_date__c;
                    ii.ITPM_Invest_TX_Version__c =iinew.ITPM_Invest_TX_Version__c;
                    ii.ITPM_Invest_SEL_Year_validity__c =iinew.ITPM_Invest_SEL_Year_validity__c;
                    ii.ITPM_Invest_REL_BI_that_belongs__c =iinew.ITPM_Invest_REL_BI_that_belongs__c;   
                    // Expense & Investment
                    ii.ITPM_CUR_January_investment__c=iinew.ITPM_CUR_January_investment__c;
                    ii.ITPM_CUR_February_investment__c=iinew.ITPM_CUR_February_investment__c;
                    ii.ITPM_CUR_March_investment__c=iinew.ITPM_CUR_March_investment__c;
                    ii.ITPM_CUR_April_investment__c=iinew.ITPM_CUR_April_investment__c;
                    ii.ITPM_CUR_May_investment__c=iinew.ITPM_CUR_May_investment__c;
                    ii.ITPM_CUR_June_investment__c=iinew.ITPM_CUR_June_investment__c;
                    ii.ITPM_CUR_July_investment__c=iinew.ITPM_CUR_July_investment__c;
                    ii.ITPM_CUR_August_investment__c=iinew.ITPM_CUR_August_investment__c;
                    ii.ITPM_CUR_September_investment__c=iinew.ITPM_CUR_September_investment__c;
                    ii.ITPM_CUR_October_investment__c=iinew.ITPM_CUR_October_investment__c;
                    ii.ITPM_CUR_November_investment__c=iinew.ITPM_CUR_November_investment__c;
                    ii.ITPM_CUR_December_investment__c=iinew.ITPM_CUR_December_investment__c;
                    ii.ITPM_CUR_January_expense__c=iinew.ITPM_CUR_January_expense__c;
                    ii.ITPM_CUR_February_expense__c=iinew.ITPM_CUR_February_expense__c;
                    ii.ITPM_CUR_March_expense__c=iinew.ITPM_CUR_March_expense__c;
                    ii.ITPM_CUR_April_expense__c=iinew.ITPM_CUR_April_expense__c;
                    ii.ITPM_CUR_May_expense__c=iinew.ITPM_CUR_May_expense__c;
                    ii.ITPM_CUR_June_expense__c=iinew.ITPM_CUR_June_expense__c;
                    ii.ITPM_CUR_July_expense__c=iinew.ITPM_CUR_July_expense__c;
                    ii.ITPM_CUR_August_expense__c=iinew.ITPM_CUR_August_expense__c;
                    ii.ITPM_CUR_September_expense__c=iinew.ITPM_CUR_September_expense__c;
                    ii.ITPM_CUR_October_expense__c=iinew.ITPM_CUR_October_expense__c;
                    ii.ITPM_CUR_November_expense__c=iinew.ITPM_CUR_November_expense__c;
                    ii.ITPM_CUR_December_expense__c =iinew.ITPM_CUR_December_expense__c;                                                                                                                                       
                }            
            }
        }
        if(listVersiones.size()>0) insert listVersiones;
    }
}