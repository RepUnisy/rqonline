/*------------------------------------------------------------------------
Author:         Borja Martín
Company:        Indra
Description:    trigger
History
<Date>          <Author>        <Description>
14-Oct-2016     Borja Martín   Se controla la herencia del campo portfolio manager
07-Dec-2016     Borja Martín   Se controla que no se puedan modificar los códigos de los proyectos una vez creados. 
----------------------------------------------------------------------------*/
trigger ITPM_Trigger_Project on ITPM_Project__c (before insert, before update, after update, before delete) {
    

    
    if(trigger.isInsert && trigger.isBefore){
        // Si viene portfolio y no viene portfolio manager lo establecemos por código
        List <ITPM_Portfolio__c> listPortfolios = [SELECT Id FROM ITPM_Portfolio__c];
        /*for(ITPM_Project__c proj : Trigger.new){            
            if(proj.ITPM_REL_Portfolio__c != null && proj.ITPM_REL_Portfolio_Manager__c == null){
                for(ITPM_Portfolio__c portfolio : listPortfolios){
                    if(portfolio.Id == proj.ITPM_REL_Portfolio__c){
                        proj.ITPM_REL_Portfolio_Manager__c = portfolio.ITPM_REL_Portfolio_Manager__c;
                    }
                }
            }            
        }*/
    }
    
    else if(trigger.isUpdate && trigger.isBefore){
        list<ITPM_UBC_Project__c> listaUBC = [SELECT id, ITPM_REL_Project__c  FROM ITPM_UBC_Project__c WHERE ITPM_REL_Project__c IN :Trigger.New];
        
        map<id, list<ITPM_UBC_Project__c>> mapaListasUBC = new map<id, list<ITPM_UBC_Project__c>>();
        
        //Preparo el mapa
        
        for(ITPM_UBC_Project__c UBC : listaUBC){
            
            list<ITPM_UBC_Project__c> listaAux;
            
            if(mapaListasUBC.get(UBC.ITPM_REL_Project__c) == null){
                listaAux = new list<ITPM_UBC_Project__c>();
            }else{
                listaAux = mapaListasUBC.get(UBC.ITPM_REL_Project__c);
            }
            
            listaAux.add(UBC);
            mapaListasUBC.put(UBC.ITPM_REL_Project__c, listaAux);
        }
        
        //Fin preparo el mapa
        
        for(id proj : mapaListasUBC.keyset()){
            list<ITPM_UBC_Project__c> listaUBCS = mapaListasUBC.get(proj);
            
            
        }
        
        
    }
    else if(trigger.isDelete && trigger.isBefore){
        boolean is_System_Admin = false;     
        List<Profile> listProfilesSystemAdministrator = [Select Id from Profile where name in ('System Administrator','Administrador del sistema') ];
        
        if(listProfilesSystemAdministrator != null && listProfilesSystemAdministrator.size()==1 && UserInfo.getProfileId() == listProfilesSystemAdministrator.get(0).Id ){            
           is_System_Admin = true;
        }           
        
        Id IDpermissionSetGPSAdmin = [Select Id from PermissionSet where name=:'PMO' and label='GPS_Admin'].Id;    
                 
        boolean is_GSP_ADmin = false;
        
        for(PermissionSetAssignment permissionX : [select PermissionSetId from PermissionSetAssignment 
                                                    where AssigneeId =: UserInfo.getUserId()] ){            
            if(permissionX.PermissionSetId == IDpermissionSetGPSAdmin){
                is_GSP_ADmin = true;
            }
        }

        for(ITPM_Project__c pold : Trigger.old){

            if(!is_System_Admin && !is_GSP_ADmin){ Trigger.old[0].addError('You cannot delete the Project Id.'); }
        }
          

    }
    
}