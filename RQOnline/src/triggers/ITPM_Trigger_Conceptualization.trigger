/*------------------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    trigger
History
<Date>          <Author>        <Description>
01-Sep-2016     Rubén Simarro   Initial version
----------------------------------------------------------------------------*/
trigger ITPM_Trigger_Conceptualization on ITPM_Conceptualization__c  (before insert, before update, before delete) {
     
    Map<String,Id> mapConceptualizationRecordtypes = new Map<String,Id>();
        
    for(RecordType recordtype :[SELECT Id,name FROM RecordType WHERE SObjectType ='ITPM_Conceptualization__c' ] ){
            
        if(!mapConceptualizationRecordtypes.containsKey(recordtype.name)){  
            mapConceptualizationRecordtypes.put(recordtype.name, recordtype.Id);     
        } 
    }
        
    if (Trigger.isInsert) {
                
        for (ITPM_Conceptualization__c conceptu: trigger.new) {          
 
            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype Created'));        
            conceptu.RecordTypeId = rt.Id;   
            conceptu.ITPM_Conceptu_DT_State_date__c= System.today();
        } 
    }   
    else if (Trigger.isUpdate) {
    
        List<ITPM_Program__c> lstInts = [SELECT Id FROM Itpm_Program__c WHERE Name = 'Integration'];
        ITPM_Program__c integration;
        if(lstInts != null && lstInts.size()==1) 
            integration = lstInts.get(0);
    
        for (ITPM_Conceptualization__c conceptu: trigger.new) {  

            //Si el usuario cambia el estado de la Conceptualization, hay que modificar su recordtype
            if ( trigger.oldMap.get(conceptu.Id).ITPM_Conceptu_SEL_Status__c!= conceptu.ITPM_Conceptu_SEL_Status__c){

                if(conceptu.ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_Created){                              
                    RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype Created'));           
                    conceptu.RecordTypeId = rt.Id;                 
                    conceptu.ITPM_Conceptu_DT_State_date__c= System.today();
                    conceptu.ITPM_Conceptu_TX_Reject_reason__c='';
                }    
                else if(conceptu.ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_In_progress){                              
                    if(conceptu.ITPM_Conceptu_FOR_Status_in_progress_ope__c){
                        if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype In Progress with Integration'));   
                            conceptu.RecordTypeId = rt.Id;                                      
                        }else{
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype In progress'));   
                            conceptu.RecordTypeId = rt.Id;          
                        }
                        conceptu.ITPM_Conceptu_DT_State_date__c= System.today();                        
                    }
                    else{      
                        trigger.new[0].addError(Label.ITPM_Conceptu_error_message_fill_not_filled+' "'+Label.ITPM_Conceptu_Status_In_progress+'"');      
                    }       
                } 
                else if(conceptu.ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_Finalized){                                     
                    if(conceptu.ITPM_Conceptu_FOR_Status_Finalized_open__c){
                        if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype Finalized with Integration'));   
                            conceptu.RecordTypeId = rt.Id;          
                        }else{
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype Finalized'));   
                            conceptu.RecordTypeId = rt.Id;               
                        }
                        conceptu.ITPM_Conceptu_DT_State_date__c= System.today();                        
                    }   
                   else{ 
                        trigger.new[0].addError(Label.ITPM_Conceptu_error_message_fill_not_filled+' "'+Label.ITPM_Conceptu_Status_Finalized+'"');       
                    }        
                }            
                else if(conceptu.ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_Rejected){               
                   
                    if(conceptu.ITPM_Conceptu_FOR_Status_Rejected_open__c){
                        if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype Rejected with Integration'));   
                            conceptu.RecordTypeId = rt.Id;     
                        }else{
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype Rejected'));   
                            conceptu.RecordTypeId = rt.Id;                        
                        }
                        conceptu.ITPM_Conceptu_DT_State_date__c= System.today();                        
                    }  
                    else{                  
                        trigger.new[0].addError(Label.ITPM_Conceptu_error_message_fill_not_filled+' "'+Label.ITPM_Conceptu_Status_Rejected+'"');       
                    }
                }       
                else if(conceptu.ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_Hibernated){   
                    if(conceptu.ITPM_Conceptu_FOR_Status_Hibernated_open__c){      
                        if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype Hibernated with Integration'));
                            conceptu.RecordTypeId = rt.Id;          
                        }else{
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptualization Recordtype Hibernated'));   
                            conceptu.RecordTypeId = rt.Id;                        
                        }
                        conceptu.ITPM_Conceptu_DT_State_date__c= System.today();                        
                    }  
                    else{                 
                        trigger.new[0].addError(Label.ITPM_Conceptu_error_message_fill_not_filled+' "'+Label.ITPM_Conceptu_Status_Hibernated+'"');       
                    }
                }       
                 else if(conceptu.ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_Business_Area_Approved){                                  
                    if(conceptu.ITPM_Conceptu_FOR_Status_BAapproved_open__c){
                        
                        //Solo usuarios con Profile "System Administrator" o PermissionSet "GPS_Admin" pueden volver atrás una opp en estado Finalized 
                        if(trigger.oldMap.get(conceptu.Id).ITPM_Conceptu_SEL_Status__c == Label.ITPM_Idea_Status_Finalized){                       
                            boolean is_System_Admin = false;     
                            List<Profile> listProfilesSystemAdministrator = [Select Id from Profile where name in ('System Administrator','Administrador del sistema') ];                        
                            if(listProfilesSystemAdministrator != null && listProfilesSystemAdministrator.size()==1 && UserInfo.getProfileId() == listProfilesSystemAdministrator.get(0).Id ){            
                               is_System_Admin = true;
                            }
                            Id IDpermissionSetGPSAdmin = [Select Id from PermissionSet where name=:'PMO' and label='GPS_Admin'].Id;                   
                            boolean is_GSP_ADmin = false;
                            //recorro todos los permission set que tenga asignado el usuario, ya que pueden ser mas de uno, buscando el de GPS_Admin     
                            for(PermissionSetAssignment permissionX : [select PermissionSetId from PermissionSetAssignment where AssigneeId =: UserInfo.getUserId()] ){            
                                if(permissionX.PermissionSetId == IDpermissionSetGPSAdmin){
                                    is_GSP_ADmin = true;
                                }
                            }                                         
                            if(is_System_Admin || is_GSP_ADmin){                  
                                if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                                    RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Concepty Rtype Business Area Approved with Integration'));
                                    conceptu.RecordTypeId = rt.Id;                                 
                                }else{
                                    RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptu Rtype Business Area Approved'));   
                                    conceptu.RecordTypeId = rt.Id;                        
                                }
                                conceptu.ITPM_Conceptu_DT_State_date__c= System.today();
                            }else{
                                trigger.new[0].addError(' You cannot alter a Conceptualization in status Finalized');
                            }
                        }else{                        
                            if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                                RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Concepty Rtype Business Area Approved with Integration'));
                                conceptu.RecordTypeId = rt.Id;                                 
                            }else{
                                RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptu Rtype Business Area Approved'));   
                                conceptu.RecordTypeId = rt.Id;                        
                            }
                            conceptu.ITPM_Conceptu_DT_State_date__c= System.today();                        
                        }
                    }  
                    else{                 
                        trigger.new[0].addError(Label.ITPM_Conceptu_error_message_fill_not_filled+' "'+Label.ITPM_Conceptu_Status_Business_Area_Approved+'"');       
                    }
                }       
                 else if(conceptu.ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_Business_Area_Hibernated){                                  
                    if(conceptu.ITPM_Conceptu_FOR_Status_BAhibernat_open__c){
                        if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Concepty Rtype Business Area Hibernated with Integration'));
                            conceptu.RecordTypeId = rt.Id;          
                        }else{
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptu Rtype Business Area Hibernated'));   
                            conceptu.RecordTypeId = rt.Id;                        
                        }
                        conceptu.ITPM_Conceptu_DT_State_date__c= System.today();                        
                    }  
                    else{                 
                        trigger.new[0].addError(Label.ITPM_Conceptu_error_message_fill_not_filled+' "'+Label.ITPM_Conceptu_Status_Business_Area_Hibernated+'"');       
                    }
                }       
                 else if(conceptu.ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_Business_Area_Pending_Validation){                                  
                    if(conceptu.ITPM_Conceptu_FOR_Status_BApending_open__c){
                        
                         if(trigger.oldMap.get(conceptu.Id).ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_Business_Area_Rejected){
                            boolean is_System_Admin = false;     
                            List<Profile> listProfilesSystemAdministrator = [Select Id from Profile where name in ('System Administrator','Administrador del sistema') ];                        
                            if(listProfilesSystemAdministrator != null && listProfilesSystemAdministrator.size()==1 && UserInfo.getProfileId() == listProfilesSystemAdministrator.get(0).Id ){            
                               is_System_Admin = true;
                            }
                            Id IDpermissionSetGPSAdmin = [Select Id from PermissionSet where name=:'PMO' and label='GPS_Admin'].Id;                   
                            boolean is_GSP_ADmin = false;
                            //recorro todos los permission set que tenga asignado el usuario, ya que pueden ser mas de uno, buscando el de GPS_Admin     
                            for(PermissionSetAssignment permissionX : [select PermissionSetId from PermissionSetAssignment where AssigneeId =: UserInfo.getUserId()] ){            
                                if(permissionX.PermissionSetId == IDpermissionSetGPSAdmin){
                                    is_GSP_ADmin = true;
                                }
                            }                                         
                            if(is_System_Admin || is_GSP_ADmin){                  
                                if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                                    RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Concepty Rtype Business Area Pending Validation with Integration'));
                                    conceptu.RecordTypeId = rt.Id;          
                                }else{
                                    RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptu Rtype Business Area Pending Validation'));   
                                    conceptu.RecordTypeId = rt.Id;                        
                                }
                                conceptu.ITPM_Conceptu_DT_State_date__c= System.today();
                            }else{
                                trigger.new[0].addError(' You cannot alter a Conceptualization in status Business Area Rejected.');
                            }
                        }else{
                            if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                                RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Concepty Rtype Business Area Pending Validation with Integration'));
                                conceptu.RecordTypeId = rt.Id;          
                            }else{
                                RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptu Rtype Business Area Pending Validation'));   
                                conceptu.RecordTypeId = rt.Id;                        
                            }
                            conceptu.ITPM_Conceptu_DT_State_date__c= System.today();     
                        }                   
                    }  
                    else{                 
                        trigger.new[0].addError(Label.ITPM_Conceptu_error_message_fill_not_filled+' "'+Label.ITPM_Conceptu_Status_Business_Area_Pending_Validation+'"');       
                    }
                }      
                 else if(conceptu.ITPM_Conceptu_SEL_Status__c == Label.ITPM_Conceptu_Status_Business_Area_Rejected){
                    if(conceptu.ITPM_Conceptu_FOR_Status_BArejected_open__c){
                        if(conceptu.ITPM_Conceptu_REL_Program__c!=null && conceptu.ITPM_Conceptu_REL_Program__c == integration.Id){
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Concepty Rtype Business Area Rejected with Integration'));
                            conceptu.RecordTypeId = rt.Id;          
                        }else{
                            RecordType rt = new Recordtype(Id=mapConceptualizationRecordtypes.get('ITPM Conceptu Rtype Business Area Rejected'));   
                            conceptu.RecordTypeId = rt.Id;                        
                        }
                        conceptu.ITPM_Conceptu_DT_State_date__c= System.today();                        
                    }  
                    else{                 
                        trigger.new[0].addError(Label.ITPM_Conceptu_error_message_fill_not_filled+' "'+Label.ITPM_Conceptu_Status_Business_Area_Rejected+'"');       
                    }
                }
                
            }  
        
        }

    }
    else if(trigger.isDelete && trigger.isBefore) {
        boolean puedeBorrar = true;
        List<PermissionSetAssignment> lstcurrentUserPerSet =    [   SELECT Id, PermissionSet.Name,AssigneeId
                                                                FROM PermissionSetAssignment
                                                                WHERE AssigneeId = :Userinfo.getUserId() ];
        for ( PermissionSetAssignment psa : lstcurrentUserPerSet ) {
            System.debug('##psa.PermissionSet.Name' + psa.PermissionSet.Name);
            if ( psa.PermissionSet.Name.equals('PPM') ) {
                puedeBorrar = false;
            }
        
        }
        if(puedeBorrar == false){
        for (ITPM_Conceptualization__c c : Trigger.old) {
            		c.addError('Unable to delete record!');
        		}
        }
	}

}