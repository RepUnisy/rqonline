trigger TRAD_eventTriggerAfterInsert on Event (after insert) {
    Set<Id> idSet = new Set<Id>();
    for(Event a:Trigger.New){
        idSet.add(a.Id);
    }
    String query = 'SELECT Id, Activity_Account__c, WhatId, WhoId FROM  Event WHERE Id IN: idSet';
    
    Database.executeBatch(new TRAD_eventBatchClass(query, idSet));
}