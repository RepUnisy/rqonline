/*------------------------------------------------------------
Author:         Luis Morena Nevado
Company:        Indra
Description:    

History
<Date>             <Author>                    <Change Description>
03-Nov-2016     Luis Morena Nevado               Version inicial

------------------------------------------------------------*/
trigger AM_TRG_Actualizacion_Gespevesa on AM_Estacion_de_servicio__c (before update) {
    
    Map<String, Id> usuariosMap = new Map<String, Id>();
    List<User> users = [Select Id, Name, Id_Usuario_AM__c from User where Id_Usuario_AM__c != null];
    for(User us: users){
        usuariosMap.put(us.Id_Usuario_AM__c, us.Id);
    }
    
    for (AM_Estacion_de_servicio__c es : trigger.new) {

        if (es.AM_Carga_Gespevesa__c )
        {
           //comprobamos el usuario con el que haremos la equivalencia
                if (usuariosMap.containsKey(es.AM_Vkgrp__c))
                    es.OwnerId = usuariosMap.get(es.AM_Vkgrp__c);
            
         }
              
        es.AM_Carga_Gespevesa__c = false;
    }
}