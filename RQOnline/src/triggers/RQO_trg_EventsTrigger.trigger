/**
 *	@name: RQO_trg_EventsTrigger
 *	@version: 1.0
 *	@creation date: 17/11/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Trigger para el objeto RQO_obj_Event__c
 *	@testClass: 
*/
trigger RQO_trg_EventsTrigger on RQO_obj_Event__c (before insert) {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_trg_EventsTrigger';
        
    /**
     * Inicio
     */
    System.debug(CLASS_NAME + ': INICIO');
    
    // Comprobación del estado del trigger
    REP_cs_activacionTrigger__c pt = RQO_cls_CustomSettingUtil.getParametrizacionTrigger(CLASS_NAME);
	Boolean isActive = (pt != null ? pt.REP_fld_triggerActive__c : true);
    
    if (isActive) {
        RQO_cls_EventTriggerHandler eventHandler = RQO_cls_EventTriggerHandler.getInstance();
        if (trigger.isBefore && trigger.isInsert){
            //Llamamos al handler del trigger para que realice la lógica necesaria antes de insertar el registro
            eventHandler.beforeInsertExecution(trigger.new);
        }
    }
    
    System.debug(CLASS_NAME + ': FIN');
 	
}