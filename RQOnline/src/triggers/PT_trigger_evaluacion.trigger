trigger PT_trigger_evaluacion on Evaluacion__c (before update, before delete, before insert) {
         
    if (Trigger.isBefore) {
         
        if(Trigger.isInsert){
            
            for (Evaluacion__c evaluacion: trigger.new) {
           
                Patrocinios__c patrocinio = [SELECT id, name, ownerId, createdById, formula_estado__c from patrocinios__c where id = :evaluacion.patrocinio__c];
           
                List<Evaluacion__c> evaluaciones = [SELECT id, patrocinio__r.name from evaluacion__c where patrocinio__c = :evaluacion.patrocinio__c];
           
                if(patrocinio.ownerId != UserInfo.getUserId()){   
                    trigger.new[0].addError(Label.etiqueta_trigger_evaluacion_7);
                }    
                else if(evaluaciones.size() > 0){   
                    trigger.new[0].addError(Label.etiqueta_trigger_evaluacion_6);
                }                            
                else if(patrocinio.formula_estado__c != Label.etiqueta_estado_pendiente_evaluacion && patrocinio.formula_estado__c != Label.etiqueta_estado_programa_en_curso){
                    trigger.new[0].addError(Label.etiqueta_trigger_evaluacion_3);
                }
                else{
               
                    RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='evaluacion__c' AND Name = 'tipo registro consulta' LIMIT 1];   
                    evaluacion.RecordTypeId = rt.Id;
                }
                //calculo el valor del campo Alcanzados_criterios__c
                evaluacion.Alcanzados_criterios__c= PT_evaluacion_handler.calculaAlcanzadoCriterios(evaluacion.Liderazgo__c, evaluacion.Innovacion__c,evaluacion.Etica__c,evaluacion.Calidad__c,evaluacion.Futuro_de_la_energia__c,evaluacion.Alcanzado_conocimiento__c,evaluacion.alcanzado_innovacion__c,evaluacion.alcanzado_desarrollo__c,evaluacion.alcanzado_Calidad__c,evaluacion.Alcanzado_futuro__c, evaluacion.Circunstancia_excepcional__c, evaluacion.Justificacion_circunstancia_excepcional__c);      

                   System.debug('Alcanzado Criterios:   ' + evaluacion.Alcanzados_criterios__c);

                //calculo el valor del campo Alcanzado_objetivos__c
                evaluacion.Alcanzado_objetivos__c=  PT_evaluacion_handler.calculaAlcanzadoObjetivos(evaluacion.objetivo_incrementar_ventas__c, evaluacion.objetivo_mejorar_atributos_de_imagen__c, evaluacion.objetivo_otros__c, evaluacion.Alcanzado_objetivo_imagen__c, evaluacion.Alcanzado_objetivo_ventas__c);
           
                   System.debug('Objetivos incrementar ventas:     ' + evaluacion.objetivo_incrementar_ventas__c);
                   System.debug('Objetivos mejorar atribb img:     ' + evaluacion.objetivo_mejorar_atributos_de_imagen__c);
                   System.debug('Objetivos otros: '                  + evaluacion.objetivo_otros__c);
                   System.debug('Objetivos objetivo img:     '       + evaluacion.Alcanzado_objetivo_imagen__c);
                   System.debug('Objetivos ventas:     '             + evaluacion.Alcanzado_objetivo_ventas__c  );
                   //
                   System.debug('Alcanzado Objetivos:     '          + evaluacion.Alcanzado_objetivos__c);
            }
   
        }   
        else if (Trigger.isUpdate) {

            for (Evaluacion__c evaluacion: trigger.new) {
                                                               
                if( trigger.oldMap.get(evaluacion.Id).formula_estado__c == Label.etiqueta_estado_evaluacion_finalizada){               
                    trigger.new[0].addError(Label.etiqueta_trigger_evaluacion_1);
                }
                else if ( trigger.oldMap.get(evaluacion.Id).patrocinio__c!= evaluacion.patrocinio__c){

                    trigger.new[0].addError(Label.etiqueta_trigger_evaluacion_8);
                }  
                
                //calculo el valor del campo Alcanzados_criterios__c
                evaluacion.Alcanzados_criterios__c= PT_evaluacion_handler.calculaAlcanzadoCriterios(evaluacion.Liderazgo__c, evaluacion.Innovacion__c,evaluacion.Etica__c,evaluacion.Calidad__c,evaluacion.Futuro_de_la_energia__c,evaluacion.Alcanzado_conocimiento__c,evaluacion.alcanzado_innovacion__c,evaluacion.alcanzado_desarrollo__c,evaluacion.alcanzado_Calidad__c,evaluacion.Alcanzado_futuro__c, evaluacion.Circunstancia_excepcional__c, evaluacion.Justificacion_circunstancia_excepcional__c);      

                //calculo el valor del campo Alcanzado_objetivos__c
                evaluacion.Alcanzado_objetivos__c=  PT_evaluacion_handler.calculaAlcanzadoObjetivos(evaluacion.objetivo_incrementar_ventas__c, evaluacion.objetivo_mejorar_atributos_de_imagen__c, evaluacion.objetivo_otros__c, evaluacion.Alcanzado_objetivo_imagen__c, evaluacion.Alcanzado_objetivo_ventas__c);

                   System.debug('Objetivos incrementar ventas:     ' + evaluacion.objetivo_incrementar_ventas__c);
                   System.debug('Objetivos mejorar atribb img:     ' + evaluacion.objetivo_mejorar_atributos_de_imagen__c);
                   System.debug('Objetivos otros: '                  + evaluacion.objetivo_otros__c);
                   System.debug('Objetivos objetivo img:     '       + evaluacion.Alcanzado_objetivo_imagen__c);
                   System.debug('Objetivos ventas:     '             + evaluacion.Alcanzado_objetivo_ventas__c  );
                   //
                   System.debug('Alcanzado Objetivos:     '          + evaluacion.Alcanzado_objetivos__c);                
            }   
        }
        else if(Trigger.isDelete){
            
            for (Evaluacion__c evaluacion: trigger.old) {
           
                if(evaluacion.formula_estado__c == Label.etiqueta_estado_evaluacion_finalizada){
                  
                    trigger.old[0].addError(Label.etiqueta_trigger_evaluacion_5);
                }     
            }
        }
      
    }
}