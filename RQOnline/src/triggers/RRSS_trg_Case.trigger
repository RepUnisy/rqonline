/**
 * @name: RRSS_trg_Case
 * @version: 1.0
 * @creation date: 14/06/2017
 * @author: Ramon Diaz, Accenture
 * @description: Trigger sobre Casos para completar Milestones automáticamente y asignar Entitlement
*/

trigger RRSS_trg_Case on Case (after update, after insert) {    
    // Si es una actualización, completaremos el Milestone correspondiente al estado anterior
    if (Trigger.isUpdate){
        RRSS_cls_handlerAfterUpdateCase.handlerAfterUpdateCase(Trigger.new,Trigger.old);
    }   
    // Si es un nuevo caso, asignaremos al caso el Entitlement por defecto
    if (Trigger.isInsert) {
        RRSS_cls_handlerAfterInsertCase.handlerAfterInsertCase(Trigger.new,Trigger.old);
    }
}