/*------------------------------------------------------------
Author:         Jon Ruiz
Company:        Minsait
Description:    Trigger de AM_TRG_DAFO .
History
<Date>      <Author>            <Description>
02/01/2017  Jon Ruiz         Creación del Trigger
------------------------------------------------------------*/
trigger AM_TRG_DAFO on AM_DAFO__c (before insert) {
    AM_DAFO__c newDAFO = new AM_DAFO__c();        
    if (trigger.isInsert) {
        newDAFO = Trigger.new[0];
        List<AM_DAFO__c> esExistente = [SELECT Id, Name FROM AM_DAFO__c WHERE AM_REL_Estacion_de_servicio__c =: newDAFO.AM_REL_Estacion_de_servicio__c LIMIT 1];
        if (esExistente.size() == 1)
            trigger.new[0].addError(Label.AM_error_message_dafoExist);
    }
}