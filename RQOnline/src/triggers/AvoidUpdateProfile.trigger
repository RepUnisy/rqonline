trigger AvoidUpdateProfile on User (before update) {
    
    User userOld = Trigger.old[0];
    User user = Trigger.new[0];
    
    Profile profile= 
    [SELECT Id, Name FROM Profile WHERE Id = :user.ProfileId limit 1];
    Profile profileOld= 
    [SELECT Id, Name FROM Profile WHERE Id = :userOld.ProfileId limit 1];

    if(profile.Name == 'Terms-and-Conditions' && user.TaCAccepted__c){
        user.ProfileID = userOld.ProfileID;
    }
    if(profileOld.Name != 'Terms-and-Conditions' && profile.Name == 'Terms-and-Conditions'){
        user.FirstName = userOld.FirstName;
        user.LastName = userOld.LastName;
    }
 }