/**
 * @name: RRSS_trg_Account
 * @version: 1.0
 * @creation date: 14/06/2017
 * @author: Ramon Diaz, Accenture
 * @description: Trigger sobre Accounts para validar NIF/CIF
*/

trigger RRSS_trg_Account on Account (before insert, before update) {
    // Usamos la misma clase handler para insert y update
    if ((Trigger.isUpdate || Trigger.isInsert) && Trigger.isBefore) {
        RRSS_cls_handlerBeforeUpdateAccount.handlerBeforeUpdateAccount(Trigger.new,Trigger.old);
    }
}