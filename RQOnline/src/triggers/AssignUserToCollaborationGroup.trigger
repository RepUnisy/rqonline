trigger AssignUserToCollaborationGroup on User (after insert, after update) {

        System.debug('Estamos dentro del trigger  AssignUserToCollaborationGroup.');
        List<UserToCollaboration> asignaciones = new List<UserToCollaboration>();
        Map <Id, Profile > profilesMap  = new Map<Id, Profile >([select Id,Name from profile]);           
        List<Profile_comunity__c> profilesCommunity = [SELECT Id,CollaborationGroupId__c,ProfileName__c   FROM Profile_comunity__c ];                                   
        for (User user: Trigger.new) { 
            if (user.IsActive)
            {
                Profile pfl = (Profile) profilesMap.get(user.ProfileId );
                System.debug('Profile del usuario ' + user.ProfileId );                       
                for (Profile_comunity__c profileCommunity:profilesCommunity ){
                    if (profileCommunity.ProfileName__c ==pfl.Name ){
                        System.debug('Asignamos el grupo' + profileCommunity.CollaborationGroupId__c);
                        //CollaborationGroupUtil.addMember(profileCommunity.CollaborationGroupId__c, user.Id);   
                        UserToCollaboration asignacion = new UserToCollaboration();
                        asignacion.setGroupId(profileCommunity.CollaborationGroupId__c);
                        asignacion.setUserId(user.Id);
                        asignaciones.add(asignacion);
                    }                    
                }
            }
        }
       try 
       {        
           CollaborationGroupUtil.addMember(asignaciones);
       } catch (Exception e) {
          System.debug('Error al asociar usuario al grupo de chatter ' + e.getMessage() + ' ' + e.getLineNumber() ); 
       }        
}