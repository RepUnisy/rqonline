/**
 *	@name: RQO_trg_GradeTrigger
 *	@version: 1.0
 *	@creation date: 23/01/2018
 *	@author: David Iglesias - Unisys
 *	@description: Trigger para el objeto RQO_obj_grade__c
*/
trigger RQO_trg_GradeTrigger on RQO_obj_grade__c (after insert, after update) {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_trg_GradeTrigger';
        
    /**
     * Inicio
     */
    System.debug(CLASS_NAME + ': INICIO');
    
    // Comprobación del estado del trigger
    REP_cs_activacionTrigger__c pt = RQO_cls_CustomSettingUtil.getParametrizacionTrigger(CLASS_NAME);
    Boolean isActive = (pt != null ? pt.REP_fld_triggerActive__c : true);

    if (isActive) {
    
        RQO_cls_GradeTriggerHandler handler = RQO_cls_GradeTriggerHandler.getHandler();
    
        if (Trigger.isInsert) {
            if (Trigger.isAfter) {
                handler.afterInsert(Trigger.new, Trigger.NewMap);
            }
        }
    
        if (Trigger.isUpdate) {
            if (Trigger.isAfter) {
                handler.afterUpdate(Trigger.new, Trigger.Old, Trigger.NewMap, Trigger.OldMap);
            }
        }
    }
        
    System.debug(CLASS_NAME + ': FIN');
}