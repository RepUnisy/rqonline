trigger ITPM_Version on ITPM_Version__c (after insert) {

    set<id> setIds = new set<id>();

    for(ITPM_Version__c version : trigger.new){
        setIds.add(version.ITPM_VER_REL_Investment_Item__c);
    }

    List<Opportunity_Investment_Item__c> listaJunctionOportunidadInvestment = [SELECT id, ITPM_REL_Investment_Item__c, ITPM_REL_Opportunity__c FROM Opportunity_Investment_Item__c WHERE ITPM_REL_Investment_Item__c IN :setIds];
    
    List<ITPM_PdV_Concept_Investment_Item__c> listaJunctionConceptuInvestment = [SELECT id, ITPM_REL_Investment_Item__c, ITPM_REL_Conceptualization__c FROM ITPM_PdV_Concept_Investment_Item__c WHERE ITPM_REL_Investment_Item__c IN :setIds];
    
    List<Opportunity_Version__c> listaJunctionOportunidadVersion = new List<Opportunity_Version__c>();
    
    List<ITPM_PdV_Concept_Version__c> listaJunctionConceptuVersion = new List<ITPM_PdV_Concept_Version__c>();
    
    for(ITPM_Version__c version : trigger.new){
        for(Opportunity_Investment_Item__c oporInvest : listaJunctionOportunidadInvestment ){
            if(version.ITPM_VER_REL_Investment_Item__c == oporInvest.ITPM_REL_Investment_Item__c){
                Opportunity_Version__c oporVersion = new Opportunity_Version__c();
                
                oporVersion.ITPM_REL_Opportunity__c = oporInvest.ITPM_REL_Opportunity__c;
                oporVersion.ITPM_REL_Version__c = version.id;
                
                listaJunctionOportunidadVersion.add(oporVersion);
            }
        }
        for(ITPM_PdV_Concept_Investment_Item__c concepInvest : listaJunctionConceptuInvestment ){
            if(version.ITPM_VER_REL_Investment_Item__c == concepInvest.ITPM_REL_Investment_Item__c){
                ITPM_PdV_Concept_Version__c concepVersion = new ITPM_PdV_Concept_Version__c();
                
                concepVersion.ITPM_REL_Conceptualization__c = concepInvest.ITPM_REL_Conceptualization__c;
                concepVersion.ITPM_REL_Version__c = version.id;
                
                listaJunctionConceptuVersion.add(concepVersion);
            }
        }
    }
    
    insert listaJunctionOportunidadVersion;
    insert listaJunctionConceptuVersion;
}