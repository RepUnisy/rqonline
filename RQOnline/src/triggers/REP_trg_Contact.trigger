/**
 * @name: REP_trg_Contact
 * @version: 1.0
 * @creation date: 14/01/2018
 * @author: Unisys
 * @description: Trigger sobre Contact para actualizar consentimientos de notificaciones
*/

trigger REP_trg_Contact on Contact (after update, after insert) {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'REP_trg_Contact';
        
    /**
     * Inicio
     */
    System.debug(CLASS_NAME + ': INICIO');
    
    // Comprobación del estado del trigger
    REP_cs_activacionTrigger__c pt = RQO_cls_CustomSettingUtil.getParametrizacionTrigger(CLASS_NAME);
    Boolean isActive = (pt != null ? pt.REP_fld_triggerActive__c : true);
    System.debug(CLASS_NAME + ': isActive ' + isActive);
    if (isActive) {
        
        String recordTypeId = '';
        List<Schema.RecordTypeInfo> rtInfos = Contact.SObjectType.getDescribe().getRecordTypeInfos();
		for (Schema.RecordTypeInfo rtInfo : rtInfos)
		{
			if (rtInfo.getName() == 'RQO_rt_Quimica')
				recordTypeId = rtInfo.getRecordTypeId();
		}
		
		System.debug(CLASS_NAME + ': recordTypeId ' + recordTypeId);
		
		for (Contact contact : Trigger.new)
        {
			// Se trata de un usuario de RQO
	    	system.debug('************contact.RecordTypeId' + contact.RecordTypeId);
	        system.debug('************recordTypeId' + recordTypeId);
	    	if (contact.RecordTypeId != recordTypeId)
	    	{
				// Record type no admitido
				return;
    	   	}
    	}
    	
  		// Usamos la misma clase handler para insert y update
		if ((Trigger.isUpdate || Trigger.isInsert) && Trigger.isAfter) {
    		RQO_cls_handlerAfterContact.handlerAfterContact(Trigger.isInsert, Trigger.new, Trigger.oldMap);
		}
    }
}