/*------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    
Trigger para insertar registros del objeto Auditoria__c cuando se modifiquen valores de Perfil, Rol...

Test Class:     PT_test_trigger_usuarioAuditoria
History
<Date>          <Author>        <Change Description>
09-02-2016      Rubén Simarro   Initial Version
------------------------------------------------------------*/
trigger PT_trigger_usuario_auditoria on User (after update, after insert) {
            
     if (Trigger.isInsert) {
         
         System.debug('@@@ PT_trigger_usuario_auditoria. INSERT User');

        for (User usuarioAux: trigger.new) {    
             
            if(usuarioAux.UserRoleId != null){
  
                UserRole rolUsuario = [Select name from UserRole where id=: usuarioAux.UserRoleId];

                PT_auditoria_handler.insertaAuditoria('Asignacion','Funcion', rolUsuario.Name, usuarioAux.Id);
            }
            if(usuarioAux.ProfileId != null){
                           
                Profile perfilUsuario = [Select name from Profile where id=: usuarioAux.ProfileId];
         
                PT_auditoria_handler.insertaAuditoria('Asignacion','Perfil', perfilUsuario.Name, usuarioAux.Id);
            }
        }     
    }           
    else if (Trigger.isUpdate) {
        if(Trigger.isAfter){
        list<user> listaUsuarios = [Select id, UserRole.Name, Profile.name FROM User WHERE id IN : Trigger.new];
        
            for(user u : listaUsuarios){
                if(u.UserRoleId != Trigger.oldMap.get(u.id).get('UserRoleId') && (u.Profile.name == 'Gestor de Patrocinio' || u.Profile.name == 'Equipo de Patrocinio' || u.Profile.name == 'Equipo de Patrocinio (Lectura)')){
                    PT_actualizaPatrocinios.actualizaPatrocinios(u.id, u.UserRole.Name);
                }
            }
        }
          
        System.debug('@@@ PT_trigger_usuario_auditoria. UPDATE User');
        
         for (User usuarioAux: trigger.new) {      
     
             //compruebo si se le ha cambiado la Función
             if(trigger.oldMap.get(usuarioAux.Id).UserRoleId != usuarioAux.UserRoleId && usuarioAux.UserRoleId != null){   
                       
                 UserRole rolUsuario = [Select name from UserRole where id=: usuarioAux.UserRoleId];
                      
                 PT_auditoria_handler.insertaAuditoria('Asignacion','Funcion', rolUsuario.Name, usuarioAux.Id);
             }  
             //compruebo si se le ha cambiado el Perfil
             if(trigger.oldMap.get(usuarioAux.Id).ProfileId != usuarioAux.ProfileId){         
                 
                 Profile perfilUsuario = [Select name from Profile where id=: usuarioAux.ProfileId];
         
                 PT_auditoria_handler.insertaAuditoria('Asignacion','Perfil', perfilUsuario.Name, usuarioAux.Id);
             }       
             //compruebo si se le ha cambiado el conjunto de permisos
           /*  else if(trigger.oldMap.get(usuarioAux.Id).UserRole!= usuarioAux.UserRole){                   
                
                 PermissionSetAssignment asignacionConjuntoPermisos = [select PermissionSetId from PermissionSetAssignment where AssigneeId =: usuarioAux.Id];
                 PermissionSet conjuntoPermisos = [select name from PermissionSet where Id =: asignacionConjuntoPermisos.PermissionSetId];

                 PT_auditoria_handler.insertaAuditoria('Asignacion','Conjunto de permisos', conjuntoPermisos.name, usuarioAux.Id);
             }   */
             
         }      
    }
}