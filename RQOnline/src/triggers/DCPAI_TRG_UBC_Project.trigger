/*------------------------------------------------------------------------ 
Author:         Alfonso Constán López 
Company:        Unisys
Description:    trigger
History
<Date>          <Author>                <Description>
06-Jul-2017     Alfonso Constán López   Initial version
----------------------------------------------------------------------------*/

trigger DCPAI_TRG_UBC_Project on ITPM_UBC_Project__c (before insert, before update, before delete) {

    system.debug('Entra en el trigger: DCPAI_TRG_UBC_Project ');
    
    boolean isAdmin = DCPAI_cls_GlobalClass.isAdmin();

    if(trigger.isDelete){
        if(!isAdmin){
            List <ITPM_UBC_Project__c > listUBCs = new List <ITPM_UBC_Project__c >();
            listUBCs = Trigger.old;
            for(ITPM_UBC_Project__c aux : listUBCs){
                if (aux.DCPAI_fld_Budget_Investment__c != null){
                    aux.adderror('You can\'t delete UBCs.');
                }
            }
        }
    }
    if(Trigger.isUpdate || Trigger.isInsert){
        DCPAI_cls_trg_ubc_project.validateCreationModification(isAdmin, Trigger.new);
    }
}