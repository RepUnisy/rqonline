/*------------------------------------------------------------------------
Author:         Borja Martín Ballesteros
Company:        Indra
Description:    trigger
History
<Date>          <Author>        <Description>
5-Dec-2016     Borja Martín Ballesteros   Initial version
----------------------------------------------------------------------------*/
trigger ITPM_TRG_Status_Report on ITPM_Status_Report__c (after insert, after update, after delete) {
            
    List <ITPM_Status_Report__c> listSrs = new List <ITPM_Status_Report__c>();        
    if (trigger.isInsert || trigger.isUpdate)
        listSrs = Trigger.new;
    else
        listSrs = Trigger.old;        
      
    if (trigger.isAfter) {
        // Sólo aplica a Projects
        if(listSrs[0].ITPM_REL_Project__c!=null){
            ITPM_Project__c project = [SELECT id FROM ITPM_Project__c WHERE id =: listSrs[0].ITPM_REL_Project__c LIMIT 1];        
            List<ITPM_Status_Report__c> listActualSRs = [SELECT ITPM_DT_Report_date__c, ITPM_TX_Description__c FROM ITPM_Status_Report__c 
                                                         WHERE ITPM_REL_Project__c =: listSrs[0].ITPM_REL_Project__c
                                                         ORDER BY ITPM_DT_Report_date__c DESC
                                                         LIMIT 1];
    
            if(listActualSRs!=null && listActualSRs.size()>0){
                String aux = string.valueOfGmt(listActualSRs[0].ITPM_DT_Report_date__c);
                aux = aux.replace(' 00:00:00',''); // Eliminamos la hora
                project.ITPM_Project_TX_Last_Status_Report__c = aux + ': ' + listActualSRs[0].ITPM_TX_Description__c;
                upsert project;
            }
        }
                                                     
    }
    
}