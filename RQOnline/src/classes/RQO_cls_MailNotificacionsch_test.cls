/**
*   @name: RQO_cls_MailNotificacionsch_test
*   @version: 1.0
*   @creation date: 21/02/2018
*   @author: David Iglesias - Unisys
*   @description: Clase de test para probar la programación del batch de mails
*/
@IsTest
public class RQO_cls_MailNotificacionsch_test {

    /**
    * @creation date: 21/02/2018
    * @author: David Iglesias - Unisys
    * @description: Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            RQO_cls_TestDataFactory.createCSEnvioNotificaciones();
        }
    }
    
    /**
    * @creation date: 21/02/2018
    * @author: David Iglesias - Unisys
    * @description: Método test para probar la funcionalidad correcta dentro de la franja horaria de permiso
    * @exception: 
    * @throws:  
    */
    static testMethod void testEjecucionEnHora(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserCliente@testorg.com');
        System.runAs(u){
            
            String CRON_EXP = '0 0 0 15 3 ? 2022';
            
            System.Test.startTest();

            RQO_cls_MailNotificacion_sch ccScheduler = new RQO_cls_MailNotificacion_sch();
            system.schedule('test', CRON_EXP, ccScheduler);
            ccScheduler.lanzarProceso();
            
            System.Test.stopTest();
        }
    }
    
    /**
    * @creation date: 21/02/2018
    * @author: David Iglesias - Unisys
    * @description: Método test para probar la funcionalidad correcta fuera de la franja horaria de permiso
    * @exception: 
    * @throws: 
    */
    static testMethod void testEjecucionFueraHora(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserCliente@testorg.com');
        System.runAs(u){
            
            String CRON_EXP = '0 0 0 15 3 ? 2022';
            
            RQO_cs_procesoPlanificado__c cs = [SELECT Id,RQO_fld_domingo__c,RQO_fld_jueves__c,RQO_fld_lunes__c,RQO_fld_martes__c,RQO_fld_miercoles__c,
                                                    RQO_fld_sabado__c,RQO_fld_viernes__c 
                                                FROM RQO_cs_procesoPlanificado__c 
                                                WHERE Name = 'MailNotificacion'];
            
            cs.RQO_fld_lunes__c = false;
            cs.RQO_fld_martes__c = false;
            cs.RQO_fld_miercoles__c = false;
            cs.RQO_fld_jueves__c = false;
            cs.RQO_fld_viernes__c = false;
            cs.RQO_fld_sabado__c = false;
            cs.RQO_fld_domingo__c = false;
            
            update cs;
            
            System.Test.startTest();

            RQO_cls_MailNotificacion_sch ccScheduler = new RQO_cls_MailNotificacion_sch();
            system.schedule('test', CRON_EXP, ccScheduler);
            ccScheduler.lanzarProceso();
            
            System.Test.stopTest();
        }
    }

}