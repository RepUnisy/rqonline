@IsTest 
public with sharing class UserChangeProfileTest {
    
    @IsTest(SeeAllData=true) static void testUserChangeProfileTest() {
        try{
            User u = [select title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country
                      FROM User WHERE id =: UserInfo.getUserId()];    
            UserChangeProfile.UserChangeProfileRequest req = new UserChangeProfile.UserChangeProfileRequest();
            req.updateProfile = true;
            req.userId = u.id;
            req.idioma = 'es';
            List<UserChangeProfile.UserChangeProfileRequest> requests = new List<UserChangeProfile.UserChangeProfileRequest>();
            requests.add(req);
            UserChangeProfile.UserChangeProfile (requests);                    
        }catch(Exception e){
            System.debug(e.getMessage());            
        }        
    }
    
}