global class PT_button_renuevaPatrocinio {
      
    //metodo que genera e inserta en BD un duplicado del patrocinio pasado por parametro
    //si la operacion va bien, devuelve el ID del nuevo patrocinio, para mostrarlo en pantalla. si va mal, se muestra el actual
    webservice static List<String> renuevaPatrocinio(Id idPatrocinioArenovar){
       
        try{
      
            system.debug('@@@@@ RENOVAR PATROCINIO @@@@@@@@@'); 
            
            Patrocinios__c pAux = [SELECT formula_estado__c from Patrocinios__c where Id =:idPatrocinioArenovar];
        
            if(pAux.formula_estado__c == Label.etiqueta_estado_programa_en_curso || pAux.formula_estado__c == Label.etiqueta_estado_pendiente_evaluacion || pAux.formula_estado__c == Label.etiqueta_estado_programa_evaluado)   {

                String soql = getAtributosObject(idPatrocinioArenovar.getSObjectType().getDescribe().getName(),'id=\''+idPatrocinioArenovar+'\'');
               
                Patrocinios__c patrocinioArenovar = (Patrocinios__c)Database.query(soql);   
                
                system.debug('@@@@ Patrocinio a renovar: '+patrocinioArenovar.Nombre__c );  
                     
                //clone(preserveId, isDeepClone, preserveReadonlyTimestamps, preserveAutonumber)
                Patrocinios__c patrocinioNuevo = patrocinioArenovar.clone(false,true,false,true);                           
                 
                patrocinioNuevo.Patrocinio_renovado__c = idPatrocinioArenovar;      
                patrocinioNuevo.Estado__c = 'Inicial';
                
               //periodo temporal      
                patrocinioNuevo.fecha_limite_de_aprobacion2__c = null;                    
                patrocinioNuevo.Fecha_de_inicio__c = null;                                      
                patrocinioNuevo.Fecha_de_fin__c = null;   
                patrocinioNuevo.Duracion__c = null;
           
                //presupuesto
                patrocinioNuevo.Presupuesto2__c = null;
                patrocinioNuevo.Observaciones_presupuesto__c = null;
                patrocinioNuevo.Esta_planificado_en_el_area_solicitante__c = null;
                patrocinioNuevo.Moneda__c = null;
                
                //contratacion
                patrocinioNuevo.Contrato_firmado__c = null;
                patrocinioNuevo.Motivo_no_contratacion__c = null;
                
                insert(patrocinioNuevo);
              
                system.debug('@@@@ '+Label.etiqueta_clase_button_renueva_patrocinio+' @@@@');    
                
                return new List<string>{Label.etiqueta_clase_button_renueva_patrocinio, patrocinioNuevo.Id};
            }    
            else{
                return new List<string>{Label.etiqueta_clase_button_renueva_patrocinio_error, idPatrocinioArenovar}; 
            }                   
        }        
        catch(Exception e){
        
            system.debug('@@@ ERROR PT_button_renuevaPatrocinio renuevaPatrocinio(): '+e.getMessage());
           
            return new List<string>{Label.error_generico, idPatrocinioArenovar} ;
        }
        
    }
      
    //metodo que devuelve una SOQL dinamica con todos los campos creados por el usuario
    @TestVisible
    private static String getAtributosObject(String objectName, String whereClause){
         
        try{       
          
            String selects = '';
                    
            if(Schema.getGlobalDescribe().get(objectName.toLowerCase()) == null){ 
                system.debug('@@@ ERROR getAtributosObject(): debe indicar un nombre de objeto válido: '+objectName);
                return null; 
            }
            else{
            
                Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
                                                         
                if (fMap != null && whereClause != null && whereClause != ''){
                               
                     List<String> selectFields = new list<string>();
                    
                        for (Schema.SObjectField ft : fMap.values()){ //recorrecmos todos los field tokens (ft)
                            Schema.DescribeFieldResult fd = ft.getDescribe();
                            if (fd.isCreateable()){ 
                                selectFields.add(fd.getName());
                            }
                        }
               
                        if (!selectFields.isEmpty()){
                            for (string s:selectFields){
                                selects += s + ',';
                            }
                            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
                             
                        }
            
                     String soql = 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
            
                    system.debug('@@@ getAtributosObject(): '+ soql);
                  
                    return soql;                
                }
                else{            
                    system.debug('@@@ ERROR getAtributosObject(): debe indicar el parámetro WHERE no vacío y bien formado.');
                    return null; 
                }                                         
            }          
        }      
        catch(Exception e){  
             
            system.debug('@@@ ERROR getAtributosObject(): '+ e.getMessage());       
            return null;          
        }     
    }
               
}