/**
 * Clase encargada de aprobar automáticamente las solicitudes de aprobación de patrocinio que cumplan con los requisitos
 * para poder ser aprobados de manera automática, una vez transcurrido
 * que haya transcurrido el tiempo de margen hasta que se puede aprobar por este mecanismo, y sin que haya sido aprobado
 * de manera manual por el equipo de patrocinio.
 **/
global class PT_scheduled_aprobadoAutoPatrocinios implements Schedulable {

    global void execute(SchedulableContext SC) {
              
        String mensajeLog=' ';
         try{     

             mensajeLog = mensajeLog+'\n@@ '+DateTime.now().format()+' Proceso automático de aprobado de Patrocinios @@';   
                                
             //lo inicializamos a un valor por defecto, por si el usuario no lo ha configurado correctamente mediante la aplicación
             Integer numeroDiasMargen = 15;
                
             mensajeLog = mensajeLog+'\n@@ Días de margen establecido por defecto: '+ numeroDiasMargen;   

            //comprobamos que el parámetro 'ParAprobacion' está definido por el usuario       
            for(Configuracion_patrocinios__c configPatrocinio: [select Valor__c  from Configuracion_patrocinios__c where name =: 'ParAprobacion']){

                numeroDiasMargen  = Integer.valueOf(configPatrocinio.Valor__c);                 
                mensajeLog = mensajeLog+'\n@@ Días de margen establecido en "Configuracion patrocinios" atributo "ParAprobacion" (prevalece): '+ numeroDiasMargen;     
            }
                
             //compruebo que hay definido un proceso de aprobación de nombre Patrocinio y estado Active                  
             for(ProcessDefinition procesoAprobacion: [select id from ProcessDefinition where name='Patrocinio' and State='Active'] ){
                                         
                    ProcessInstance[] procesos = [
                           select Id, createdDate, ProcessDefinitionId, TargetObjectId, isDeleted, Status,(
                                    Select Id, ProcessInstanceId, ActorId, Actor.Name, StepStatus, Comments
                                    From StepsAndWorkItems
                                    Where  StepStatus = 'Pending'  and  isDeleted = false
                                )  
                        From ProcessInstance Where 
                        isDeleted = false 
                        and status='Pending' 
                        and ProcessDefinitionId =: procesoAprobacion.id   
                        ];
                    
                      mensajeLog = mensajeLog+'\n@@ Patrocinios pendientes de aprobación: '+ procesos.size();
                                                                           
                        for(ProcessInstance procesoX: procesos ){
  
                            Patrocinios__c patrocinio = [select name, nombre__c, publico_objetivo__c, ambito_geografico__c, notoriedad__c,aprobado_automaticamente__c
                                                         from patrocinios__c 
                                                         where id =: procesoX.TargetObjectId];                                                                              
                                                         
                               
                            Datetime diaParaAprobacionAutomatica = procesoX.CreatedDate.addDays(numeroDiasMargen);
      
                            mensajeLog = mensajeLog+'\n@@----------------------------------------';  
                            mensajeLog = mensajeLog+'\n@@ ID Patrocinio: '+ patrocinio.Name+ ' nombre del programa: '+patrocinio.Nombre__c;                                                 
                            mensajeLog = mensajeLog+'\n@@ Pendiente de aprobación desde: '+procesoX.CreatedDate.format();
                            mensajeLog = mensajeLog+'\n@@ Si cumple condiciones de autoaprobado se autoaprobará a partir del: '+diaParaAprobacionAutomatica.date().format();
                            
                                if( (patrocinio.ambito_geografico__c == 'Global' && patrocinio.publico_objetivo__c <> 'General' && diaParaAprobacionAutomatica.date() <= Date.today())
                                   ||
                                   (patrocinio.ambito_geografico__c <> 'Global' && patrocinio.notoriedad__c == 'Limitada' && diaParaAprobacionAutomatica.date() <= Date.today())
                                   ||                            
                                   (patrocinio.ambito_geografico__c <> 'Global' && patrocinio.notoriedad__c == 'Elevada' && patrocinio.publico_objetivo__c <> 'General' && diaParaAprobacionAutomatica.date() <= Date.today())
                                  ){                                    

                                                          
                                      patrocinio.aprobado_automaticamente__c = true;                                   
                                      update patrocinio;
                                      
                                       List<ProcessInstanceHistory> stepOrWorkItem = procesoX.StepsAndWorkitems;
                                      
                                       Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                                       req.setNextApproverIds(null);
                                       req.setWorkitemId(stepOrWorkItem[0].id);      
                                       req.setComments('Aprobado automático.');
                                       req.setAction('Approve');
                                        //por si quisiera denegarlo en vez de aprobarse
                                        //req.setComments('Patrocinio denegado.');
                                        //req.setAction('Reject');
                                        Approval.ProcessResult  result = Approval.process(req);      
                                                                         
                                        mensajeLog = mensajeLog+'\n@@ Cumple las condiciones de autoaprobación:\n@@ Ámbito geográfico: '+patrocinio.ambito_geografico__c+'\n@@ Público objetivo: '+patrocinio.Publico_objetivo__c+'\n@@ Notoriedad: '+patrocinio.notoriedad__c+'\n@@ Patrocinio '+patrocinio.Name+' aprobado.';
                                }
                                else{                             
                                        mensajeLog = mensajeLog+'\n@@ No cumple las condiciones de autoaprobación:\n@@ Ámbito geográfico: '+patrocinio.ambito_geografico__c+'\n@@ Público objetivo: '+patrocinio.Publico_objetivo__c+'\n@@ Notoriedad: '+patrocinio.notoriedad__c;
                                }     
                        }
            }            
            mensajeLog = mensajeLog+'\n@@-----------------------------------------------------------------';
            mensajeLog = mensajeLog+'\n@@ Fin proceso automático aprobado Patrocinios @@';          
        }
        catch(Exception e){    
            mensajeLog = mensajeLog+'\n@@ ERROR PT_scheduled_aprobadoAutoPatrocinios: '+e.getMessage();
        }
        system.debug(mensajeLog);      
        //si para facilitar la supervision de su correcto comportamiente, se quiere recibir en una direccion de email el log de la ejecucion de este trabajo programado, 
        //descomentar esta sentencia, y establecer la dirección de correo en el método enviarEmail
        //PT_button_enviarMail.enviarEmail('LOG proceso automático aprobado Patrocinios', mensajeLog);
    }
   
}