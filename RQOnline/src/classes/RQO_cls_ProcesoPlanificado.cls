/**
 *	@name: RQO_cls_ProcesoPlanificado
 *	@version: 1.0
 *	@creation date: 12/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase modelo para herencia en la planificacion de procesos
 *
 */
public abstract class RQO_cls_ProcesoPlanificado {
	
	// ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_ProcesoPlanificado';
    
	// ***** PROPIEDADES ***** //
    protected RQO_cs_procesoPlanificado__c p;
    
	// ***** CONSTRUCTORES ***** //
	
	/**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Constructor de la clase. 
	* @param:	name	Nombre del proceso a configurar
	* @return: 
	* @exception:	Exception
	* @throws:	Exception
	*/
    public RQO_cls_ProcesoPlanificado(String name){
		
        try{system.debug('name: '+name);
        	p = RQO_cls_CustomSettingUtil.getProcesoPlanificado(name);
            System.debug('p:' + p);
            
        }catch(Exception e){
			System.debug(CLASS_NAME + ' - Constructor: Error - No existe el nombre del proceso');
            throw e;
        }
    }
    
	// ***** METODOS PUBLICOS ***** //
    
	/**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Comprobacion de franja horaria de lanzamiento 
	* @param:	
	* @return: boolean Si esta dentro de franja
	* @exception:	
	* @throws:	
	*/
    public boolean isRunnable(){
        
        /**
		* Constantes
		*/
		final String METHOD = 'isRunnable';
        
		/**
		* Inicio
		*/
		System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        if(!isTheTime()){
            return false;
        }
        if(!isTheDay()){
            return false;
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': fin');
        
        return true;
    }

	/**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtiene la fecha y hora de la siguiente ejecucion 
	* @param:	
	* @return: Datetime		Fecha/Hora de ejecucion
	* @exception:	
	* @throws:	
	*/
	public Datetime getNextScheduleDatetime(Boolean blnForceNewDay) {
		
		/**
		 * Constantes
		 */
		final String METHOD = 'getNextScheduleDatetime';
		
		/**
		 * Variables
		 */
		Datetime dtNextScheduledDateTime;
		Datetime dtNow;
		Datetime dtTodayStartTime;
		Datetime dtTodayEndTime;
		Date dToday;
		Time tStartTime;
		Time tEndTime;
		Integer intDayOfWeek;
		Boolean blnValidDay;
		Integer intNumTries;
		
		/**
		 * Inicio
		 */
		 System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		 
		// Dia actual
		dToday = Date.today();

		// Hora de inicio configurada
		tStartTime = formatTime(p.RQO_fld_horaInicio__c);
		dtTodayStartTime = Datetime.newInstance(dToday, tStartTime);

		// Hora de fin configurada
		tEndTime = formatTime(p.RQO_fld_horaFin__c);
		dtTodayEndTime = Datetime.newInstance(dToday, tEndTime);

		// Comprobar si se fuerza el siguiente dia
		if ( blnForceNewDay ) {
			dtNextScheduledDateTime = Datetime.newInstance(dToday.addDays(1), tStartTime);//dtTodayStartTime.addDays(1);
		} else { // No se fuerza siguiente dia
			dtNextScheduledDateTime = System.now().addMinutes(Integer.valueOf(p.RQO_fld_intervalo__c));

			// Comprobar si esta fuera de rango
			if ( dtNextScheduledDateTime < dtTodayStartTime ) {
				dtNextScheduledDateTime = dtTodayStartTime;
			} else if ( dtNextScheduledDateTime > dtTodayEndTime ) {
				dtNextScheduledDateTime = Datetime.newInstance(dToday.addDays(1), tStartTime);//dtTodayStartTime.addDays(1);
			}
		}

		// Comprobar si el dia de la semana esta marcado
		blnValidDay = false;
		intNumTries = 0;
		while ( !blnValidDay && intNumTries < 8 ) {
			intDayOfWeek = Math.mod(Date.newInstance(1900, 1, 7).daysBetween(dtNextScheduledDateTime.date()), 7);
			if ( ( intDayOfWeek == 0 && p.RQO_fld_domingo__c ) ||
			     ( intDayOfWeek == 1 && p.RQO_fld_lunes__c ) ||
				 ( intDayOfWeek == 2 && p.RQO_fld_martes__c ) ||
				 ( intDayOfWeek == 3 && p.RQO_fld_miercoles__c ) ||
				 ( intDayOfWeek == 4 && p.RQO_fld_jueves__c ) ||
				 ( intDayOfWeek == 5 && p.RQO_fld_viernes__c ) ||
				 ( intDayOfWeek == 6 && p.RQO_fld_sabado__c ) ) {
				blnValidDay = true;
			} else {
				dtNextScheduledDateTime = dtNextScheduledDateTime.addDays(1);
			}

			intNumTries++;
		}
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return dtNextScheduledDateTime;
	}

	/**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtiene la fecha y hora de la siguiente ejecucion 
	* @param:	strTime		Hora en formato HH:MM	
	* @return: Time		Hora en formato Time
	* @exception:	
	* @throws:	
	*/
	public static Time formatTime(String strTime) {
		/**
		 * Constantes
		 */
		final Integer HOUR_POS = 0;
		final Integer MINUTE_POS = 1;
        final String METHOD = 'formatTime';
		
		/**
		 * Variables
		 */
		Time tFormattedTime;
		List<String> lstTimeValues;
		Integer intHour;
		Integer intMinutes;

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		// Obtener hora y minutos por separado
		intHour = 0;
		intMinutes = 0;
		if ( strTime != null && !strTime.equals('') ) {
			lstTimeValues = strTime.split(RQO_cls_Constantes.COLON);

			// Obtener hora
			if ( lstTimeValues.size() >= 1 ) {
				try {
					intHour = Integer.valueOf(lstTimeValues[HOUR_POS]);
				} catch (Exception e) {
                    System.debug(CLASS_NAME + ' - ' + METHOD + ': Error en la obtencion de hora');
                }
			}

			// Obtener minutos
			if ( lstTimeValues.size() >= 2 ) {
				try {
					intMinutes = Integer.valueOf(lstTimeValues[MINUTE_POS]);
				} catch (Exception e) {
                    System.debug(CLASS_NAME + ' - ' + METHOD + ': Error en la obtencion de los minutos');
                }
			}
		}

		// Transformar
		tFormattedTime = Time.newInstance(intHour, intMinutes, 0, 0);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');

		return tFormattedTime;
	}


	/**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtiene la instancia para programar el lanzamiento del job 
	* @param:		
	* @return: ScheduledInstance		Instancia de programacion del job
	* @exception:	
	* @throws:	
	*/
    public ScheduledInstance getNextSchedule(){
        return new ScheduledInstance(getJobName(), getCron(p.RQO_fld_intervalo__c.intValue()));
    }
    
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtiene la instancia para programar el lanzamiento del job 
	* @param:	d		Fecha en formato Date 		
	* @return: ScheduledInstance		Instancia de programacion del job
	* @exception:	
	* @throws:	
	*/
    public ScheduledInstance getSchedule(Date d){
        return new ScheduledInstance(getJobName(), getCron(d));
    }
    
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtiene la instancia para programar el lanzamiento del job 
	* @param:	d		Fecha en formato DateTime 		
	* @return: ScheduledInstance		Instancia de programacion del job
	* @exception:	
	* @throws:	
	*/
    public ScheduledInstance getSchedule(DateTime d){
        return new ScheduledInstance(getJobName(), getCron(d));
    }
	
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtiene variable que establece la hora/fecha de lanzamiento 
	* @param:	minutes		minutos para el lanzamiento 		
	* @return: String		valor con la fecha/hora formateada para programar la ejecucion
	* @exception:	
	* @throws:	
	*/
    @TestVisible
    private String getCron(Integer minutes){
    	DateTime fechaFutura = DateTime.now().addMinutes(minutes);
        return getCron(fechaFutura);
    }
    
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtiene variable que establece la hora/fecha de lanzamiento 
	* @param:	d		Fecha en formato Date para el lanzamiento 		
	* @return: String		valor con la fecha/hora formateada para programar la ejecucion
	* @exception:	
	* @throws:	
	*/
    private String getCron(Date d){
        DateTime fechaFutura = DateTime.newInstance(d.year(),d.month(),d.day());
        return getCron(fechaFutura);
    }
    
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtiene variable que establece la hora/fecha de lanzamiento 
	* @param:	d		Fecha en formato DateTime para el lanzamiento 		
	* @return: String		valor con la fecha/hora formateada para programar la ejecucion
	* @exception:	
	* @throws:	
	*/
    private String getCron(DateTime d){
        DateTime fechaFutura = d;
        return fechaFutura.second() + ' ' + fechaFutura.minute() + ' ' + fechaFutura.hour() + ' ' + fechaFutura.day() + ' ' + fechaFutura.month() + ' ? ' + fechaFutura.year();    
    }
    
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Genera el nombre del job que va a ser programado  
	* @param:	 		
	* @return: String		Nomenclatura del job
	* @exception:	
	* @throws:	
	*/
    private String getJobName(){
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        String minute = string.valueOf(system.now().minute());
        String second = string.valueOf(system.now().second());
        String year = string.valueOf(system.now().year()); 
        return p.Name + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
    }    
    
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Validación de el lanzamiento del proceso se encuentra dentro de la franja horaria establecida  
	* @param:	 		
	* @return: boolean		Franja horaria validada
	* @exception:	
	* @throws:	
	*/
    private boolean isTheTime(){

        if(p.RQO_fld_horaInicio__c==NULL){
            return true;
        }
        
        if(p.RQO_fld_horaFin__c==NULL){
            return true;
        }        
        
        Integer ahora = System.now().hour() * 60 + System.now().minute();
        Integer horainicio = (Integer.valueOf(p.RQO_fld_horaInicio__c.left(2)) * 60) + Integer.valueOf(p.RQO_fld_horaInicio__c.right(2));
        Integer horafin = (Integer.valueOf(p.RQO_fld_horaFin__c.left(2)) * 60) + Integer.valueOf(p.RQO_fld_horaFin__c.right(2));
        
        if(horainicio > horafin){
            if(ahora>=horainicio){
                return true;
            }
            if(ahora<=horafin){
                return true;
            }
            return false;
        }else{
            if( ahora >= horainicio && ahora <= horafin){
                return true;
            }else{
                return false;
            }
        }
    }
    
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Validación de el lanzamiento del proceso se encuentra en un dia establecido de lanzamiento  
	* @param:	 		
	* @return: boolean		Si el dia de hoy esta configurado
	* @exception:	
	* @throws:	
	*/
    private boolean isTheDay(){
        Date startDate = date.newInstance(0001, 1, 1);
        Integer dayOfWeek = Math.mod(startDate.daysBetween(Date.today()) , 7);
        
        if(dayOfWeek==0 && p.RQO_fld_domingo__c){
            return true;
        }else if(dayOfWeek==1 && p.RQO_fld_sabado__c){
            return true;
        }else if(dayOfWeek==2 && p.RQO_fld_lunes__c){
            return true;
        }else if(dayOfWeek==3 && p.RQO_fld_martes__c){
            return true;
        }else if(dayOfWeek==4 && p.RQO_fld_miercoles__c){
            return true;
        }else if(dayOfWeek==5 && p.RQO_fld_jueves__c){
            return true;
        }else if(dayOfWeek==6 && p.RQO_fld_viernes__c){
            return true;
        }else{
            return false;
        }
    }
    
     /**
     *	@name: ScheduledInstance
     *	@version: 1.0
     *	@creation date: 12/09/2017
     *	@author: David Iglesias - Unisys
     *	@description: Clase embebida para la programacion de la instancia
     *
     */
    public class ScheduledInstance{
        public String name;
        public String cron;
        
        public ScheduledInstance(){}
        public ScheduledInstance(String name, String cron){
            this.name=name;
            this.cron=cron;
        }
    }

}