/*------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    Clase controladora de la Matriz de Riesgos asociados a proyectos
Test Class:     ITPM_Controller_Matrix_Risk_Test
History
<Date>          <Author>        <Change Description>
24-Jun-2016     Rubén Simarro   Initial Version
------------------------------------------------------------*/
global with sharing class ITPM_Controller_Matrix_risk {
    
    public ITPM_Matrix_Risk__c matrixRisk {get; set;}
   
    public List<ITPM_Matrix_risk_row__c> lstFilasMatriz{get; set;} 

    public String PSC1name{get; set;} 
    public String PSC2name{get; set;} 
    public String PSC3name{get; set;} 
    public String PSC4name{get; set;} 
    public String PSC5name{get; set;} 
    public String PSC6name{get; set;} 
    public String PSC7name{get; set;} 
    public String PSC8name{get; set;} 
    public String PSC9name{get; set;} 
    public String PSC10name{get; set;} 
    
    public String PSC1description{get; set;} 
    public String PSC2description{get; set;} 
    public String PSC3description{get; set;} 
    public String PSC4description{get; set;} 
    public String PSC5description{get; set;} 
    public String PSC6description{get; set;} 
    public String PSC7description{get; set;} 
    public String PSC8description{get; set;} 
    public String PSC9description{get; set;} 
    public String PSC10description{get; set;}     
    
    public Boolean showColumnPSC1{get; set;} 
    public Boolean showColumnPSC2{get; set;} 
    public Boolean showColumnPSC3{get; set;} 
    public Boolean showColumnPSC4{get; set;} 
    public Boolean showColumnPSC5{get; set;} 
    public Boolean showColumnPSC6{get; set;} 
    public Boolean showColumnPSC7{get; set;} 
    public Boolean showColumnPSC8{get; set;} 
    public Boolean showColumnPSC9{get; set;} 
    public Boolean showColumnPSC10{get; set;}     
    
    public ITPM_Controller_Matrix_risk(ApexPages.StandardController controller){
 
        showColumnPSC1=false; 
        showColumnPSC2=false; 
        showColumnPSC3=false; 
        showColumnPSC4=false; 
        showColumnPSC5=false; 
        showColumnPSC6=false;
        showColumnPSC7=false;  
        showColumnPSC8=false; 
        showColumnPSC9=false; 
        showColumnPSC10=false;  
        
        matrixRisk = [select ID from ITPM_Matrix_Risk__c WHERE id = :ApexPages.currentPage().getParameters().get('Id')];
        
          lstFilasMatriz = [SELECT Id, Name, ITPM_Master_risk__r.Id,
                            ITPM_Matrix_Risk__r.name,
                            ITPM_Master_risk__r.Name,
                            ITPM_Master_risk__r.ITPM_Risk_Name__c,
                            ITPM_CEP1__c, 
                            ITPM_CEP2__c, 
                            ITPM_CEP3__c, 
                            ITPM_CEP4__c, 
                            ITPM_CEP5__c,
                            ITPM_CEP6__c, 
                            ITPM_CEP7__c,
                            ITPM_CEP8__c,
                            ITPM_CEP9__c,
                            ITPM_CEP10__c,
                            ITPM_CEP1__r.ITPM_PER_Weighing__c,
                            ITPM_CEP2__r.ITPM_PER_Weighing__c,
                            ITPM_CEP3__r.ITPM_PER_Weighing__c,
                            ITPM_CEP4__r.ITPM_PER_Weighing__c,
                            ITPM_CEP5__r.ITPM_PER_Weighing__c,
                            ITPM_CEP6__r.ITPM_PER_Weighing__c,
                            ITPM_CEP7__r.ITPM_PER_Weighing__c,   
                            ITPM_CEP8__r.ITPM_PER_Weighing__c,   
                            ITPM_CEP9__r.ITPM_PER_Weighing__c,   
                            ITPM_CEP10__r.ITPM_PER_Weighing__c,   
                            ITPM_value_CEP1__c,
                            ITPM_value_CEP2__c,
                            ITPM_value_CEP3__c,
                            ITPM_value_CEP4__c,
                            ITPM_value_CEP5__c,
                            ITPM_value_CEP6__c,
                            ITPM_value_CEP7__c,
                            ITPM_value_CEP8__c,
                            ITPM_value_CEP9__c,
                            ITPM_value_CEP10__c,
                            ITPM_FOR_Impact__c,
                            ITPM_Probability__c,
                            ITPM_PSC1_selected__c,
                            ITPM_PSC2_selected__c,
                            ITPM_PSC3_selected__c,
                            ITPM_PSC4_selected__c,
                            ITPM_PSC5_selected__c,
                            ITPM_PSC6_selected__c,
                            ITPM_PSC7_selected__c,
                            ITPM_PSC8_selected__c,
                            ITPM_PSC9_selected__c,
                            ITPM_PSC10_selected__c,
                            ITPM_FOR_Risk_calculated__c,
                            ITPM_TX_Contextualization__c
                            FROM ITPM_Matrix_risk_row__c 
                            where ITPM_Apply_risk__c = true AND ITPM_Matrix_Risk__c=: ApexPages.currentPage().getParameters().get('id') 
                            ORDER BY ITPM_Master_risk__r.Name ASC];

        try{
            for(ITPM_Matrix_risk_row__c filasMatriz : lstFilasMatriz){

                for(ITPM_Project_Success_Criteria__c PSC : [select Id, ITPM_TX_Criterion_name__c, ITPM_TX_Criterion_description__c, ITPM_PER_Weighing__c, ITPM_SEL_Matrix_column_position__c 
                                                            from ITPM_Project_Success_Criteria__c                                           
                                                            where ITPM_REL_Project__c =:[SELECT ITPM_Matrix_risk_REL_Project__c FROM ITPM_Matrix_Risk__c WHERE ID =: ApexPages.currentPage().getParameters().get('id')].ITPM_Matrix_risk_REL_Project__c 
                                                           AND CreatedDate <=:[SELECT CreatedDate FROM ITPM_Matrix_Risk__c WHERE ID =: ApexPages.currentPage().getParameters().get('id')].CreatedDate ] ){

   
                if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC1') && filasMatriz.ITPM_PSC1_selected__c){            
                    PSC1name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';             
                    PSC1description= PSC.ITPM_TX_Criterion_description__c;    
                    showColumnPSC1= true;
                    System.debug('@@@ show showColumnPSC1');
                }  
                else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC2') && filasMatriz.ITPM_PSC2_selected__c){        
                    PSC2name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                
                    PSC2description= PSC.ITPM_TX_Criterion_description__c;   
                     showColumnPSC2= true;                       
                    System.debug('@@@ show showColumnPSC2');
                }      
                else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC3') && filasMatriz.ITPM_PSC3_selected__c){              
                    PSC3name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                                 
                    PSC3description= PSC.ITPM_TX_Criterion_description__c; 
                     showColumnPSC3= true;     
                    System.debug('@@@ show showColumnPSC1');
                }    
                else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC4') && filasMatriz.ITPM_PSC4_selected__c){                      
                    PSC4name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                     
                    PSC4description= PSC.ITPM_TX_Criterion_description__c;   
                     showColumnPSC4= true; 
                    System.debug('@@@ show showColumnPSC4');
                }      
                else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC5') && filasMatriz.ITPM_PSC5_selected__c){      
                    PSC5name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                               
                    PSC5description= PSC.ITPM_TX_Criterion_description__c;  
                     showColumnPSC5= true;     
                    System.debug('@@@ show showColumnPSC5');
                }        
                else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC6') && filasMatriz.ITPM_PSC6_selected__c){  
                    PSC6name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                 
                    PSC6description= PSC.ITPM_TX_Criterion_description__c;
                     showColumnPSC6= true;                                  
                    System.debug('@@@ show showColumnPSC6');
                }  
                else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC7') && filasMatriz.ITPM_PSC7_selected__c){        
                    PSC7name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                           
                    PSC7description= PSC.ITPM_TX_Criterion_description__c;
                     showColumnPSC7= true;
                    System.debug('@@@ show showColumnPSC7');
                }      
                else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC8') && filasMatriz.ITPM_PSC8_selected__c){              
                    PSC8name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                      
                    PSC8description= PSC.ITPM_TX_Criterion_description__c; 
                     showColumnPSC8= true;     
                    System.debug('@@@ show showColumnPSC8');
                }  
                else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC9') && filasMatriz.ITPM_PSC9_selected__c){            
                    PSC9name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                  
                    PSC9description= PSC.ITPM_TX_Criterion_description__c; 
                     showColumnPSC9= true;
                    System.debug('@@@ show showColumnPSC9');
                }       
                else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC10') && filasMatriz.ITPM_PSC10_selected__c){       
                    PSC10name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                    
                    PSC10description= PSC.ITPM_TX_Criterion_description__c;
                     showColumnPSC10= true;                                  
                    System.debug('@@@ show showColumnPSC10');
               
                }  
            
              }  
       
            }   
      
        }
        catch (Exception ex) {                                 
            system.debug('@@@ ITPM_Controller_Matrix_risk constructor() ERROR: '+ex.getMessage());
        }  
         
    }
          
    public List<ITPM_Matrix_risk_row__c> getlstFilasMatriz(){
        return lstFilasMatriz;
    }
    
   public void guardarCriteriosExito(){     
        try{
           update lstFilasMatriz;

           for(ITPM_Matrix_risk_row__c filasMatriz : lstFilasMatriz){             
               system.debug('@@@@ ITPM_Controller_Matrix_risk MATRIZ: '+filasMatriz.ITPM_Matrix_Risk__r.name);
               system.debug('@ : '+filasMatriz.name);
                
               system.debug('@@@@ ITPM_Controller_Matrix_risk IMPACT VALUES');      
               system.debug('@@@@ value PSC1: '+filasMatriz.ITPM_value_CEP1__c);
               system.debug('@@@@ value PSC2: '+filasMatriz.ITPM_value_CEP2__c);
               system.debug('@@@@ value PSC3: '+filasMatriz.ITPM_value_CEP3__c);
               system.debug('@@@@ value PSC4: '+filasMatriz.ITPM_value_CEP4__c);
               system.debug('@@@@ value PSC5: '+filasMatriz.ITPM_value_CEP5__c);
               system.debug('@@@@ value PSC6: '+filasMatriz.ITPM_value_CEP6__c);
               system.debug('@@@@ value PSC7: '+filasMatriz.ITPM_value_CEP7__c);
               system.debug('@@@@ value PSC8: '+filasMatriz.ITPM_value_CEP8__c);
               system.debug('@@@@ value PSC9: '+filasMatriz.ITPM_value_CEP9__c);
               system.debug('@@@@ value PSC10: '+filasMatriz.ITPM_value_CEP10__c);
               
               system.debug('@@@@ Weighing PSC1: '+filasMatriz.ITPM_CEP1__r.ITPM_PER_Weighing__c);
               system.debug('@@@@ Weighing PSC2: '+filasMatriz.ITPM_CEP2__r.ITPM_PER_Weighing__c);
               system.debug('@@@@ Weighing PSC3: '+filasMatriz.ITPM_CEP3__r.ITPM_PER_Weighing__c);
               system.debug('@@@@ Weighing PSC4: '+filasMatriz.ITPM_CEP4__r.ITPM_PER_Weighing__c);
               system.debug('@@@@ Weighing PSC5: '+filasMatriz.ITPM_CEP5__r.ITPM_PER_Weighing__c);
               system.debug('@@@@ Weighing PSC6: '+filasMatriz.ITPM_CEP6__r.ITPM_PER_Weighing__c);
               system.debug('@@@@ Weighing PSC7: '+filasMatriz.ITPM_CEP7__r.ITPM_PER_Weighing__c);
               system.debug('@@@@ Weighing PSC8: '+filasMatriz.ITPM_CEP8__r.ITPM_PER_Weighing__c);
               system.debug('@@@@ Weighing PSC9: '+filasMatriz.ITPM_CEP9__r.ITPM_PER_Weighing__c);
               system.debug('@@@@ Weighing PSC10: '+filasMatriz.ITPM_CEP10__r.ITPM_PER_Weighing__c);
                
               system.debug('@@@@ Probability PSC1: '+filasMatriz.ITPM_Probability__c);
               system.debug('@@@@ Probability PSC2: '+filasMatriz.ITPM_Probability__c);
               system.debug('@@@@ Probability PSC3: '+filasMatriz.ITPM_Probability__c);
               system.debug('@@@@ Probability PSC4: '+filasMatriz.ITPM_Probability__c);
               system.debug('@@@@ Probability PSC5: '+filasMatriz.ITPM_Probability__c);
               system.debug('@@@@ Probability PSC6: '+filasMatriz.ITPM_Probability__c);
               system.debug('@@@@ Probability PSC7: '+filasMatriz.ITPM_Probability__c);
               system.debug('@@@@ Probability PSC8: '+filasMatriz.ITPM_Probability__c);
               system.debug('@@@@ Probability PSC9: '+filasMatriz.ITPM_Probability__c);
               system.debug('@@@@ Probability PSC10: '+filasMatriz.ITPM_Probability__c);
                      
               system.debug('@@@@ ITPM_Controller_Matrix_risk TOTAL IMPACT: '+filasMatriz.ITPM_FOR_Impact__c);
           }                                          
           
           
           // Calculamos el PRI con la siguiente fórmula -> Suma de todos los Risk Value / Nº Total de Riesgos que hay en el maestro.             
           Double sumRiskValues = 0;
           AggregateResult[] groupedResults = [SELECT SUM(ITPM_FOR_Risk_calculated__c)Risk_Value 
                                               FROM ITPM_Matrix_risk_row__c 
                                               WHERE ITPM_Matrix_Risk__c =: ApexPages.currentPage().getParameters().get('id')
                                               AND ITPM_FOR_Risk_calculated__c != null];
           for (AggregateResult ar : groupedResults)  {
                sumRiskValues = (Double)ar.get('Risk_Value');
           }
           
           Double sumTotalRisk = 0;
           groupedResults = [SELECT COUNT(Id)Risk_Total FROM ITPM_Master_risk__c ];
           for (AggregateResult ar : groupedResults)  {
                sumTotalRisk = (Double)ar.get('Risk_Total');
           }
           
           matrixRisk.ITPM_Matrix_risk_PRI__c = sumRiskValues / sumTotalRisk;           
           update matrixRisk;                     
           
           system.debug('@@@ ITPM_Controller_Matrix_risk guardarCriteriosExito()');
        }
        catch (Exception ex) {                                 
           system.debug('@@@ ITPM_Controller_Matrix_risk guardarCriteriosExito() ERROR: '+ex.getMessage());
        }  
    }
    
}