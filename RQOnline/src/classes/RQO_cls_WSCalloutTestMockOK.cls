/**
*	@name: RQO_cls_WSCalloutTestMockOK
*	@version: 1.0
*	@creation date: 19/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de Mock para el WSCallout
*/
@isTest
global class RQO_cls_WSCalloutTestMockOK implements HttpCalloutMock {
    
    private Integer codigo;
    
    /**
    * @creation date: 7/2/2018
    * @author: Juan Elías - Unisys
    * @description:	Constructor con parámetros de la clase 
    */
    global RQO_cls_WSCalloutTestMockOK(Integer codigo){
        this.codigo = codigo;
    }
    
    /**
    * @creation date: 7/2/2018
    * @author: Juan Elías - Unisys
    * @description:	Definición del método respond()
    */    
    public HttpResponse respond(HttpRequest request) {
        if (RQO_cls_Constantes.REST_COD_HTTP_OK == codigo){
            HttpResponse response = new HttpResponse();
            response.setStatusCode(200);
            String body = '{"idError": "OK", "msgError": null}';
            response.setBody(body);
            
            return response;
        } else {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(404);
            String body = '{"idError": "OK", "msgError": null}';
            response.setBody(body);
            
            return response;
        }
    }
}