/*------------------------------------------------------------------------
 * 
 * 	@name: 				DCPAI_cls_UnlockInvestmentPlan
 * 	@version: 			1 
 * 	@creation date: 	30/06/2017
 * 	@author: 			Alfonso Constán López	-	Unisys
 * 	@description: 		Los costes relaccionados al plan de inversión dejan de estar en waitting
 * 
----------------------------------------------------------------------------*/

global without sharing class  DCPAI_cls_UnlockInvestmentPlan {
    
    @InvocableMethod
    public static void DCPAI_UnlockInvestmentPlan (List<DCPAI_obj_Investment_Plan__c> plan) {
        
        
        List<ITPM_Budget_Item__c> ciAlmacenado = new List<ITPM_Budget_Item__c>();
        List<Id> listId = new List<Id>();
        for(DCPAI_obj_Investment_Plan__c p : plan){
            listId.add(p.Id);
        }
        List<ITPM_Budget_Item__c> ciWait = [Select id, DCPAI_fld_Waiting_to_Assign__c, DCPAI_fld_Investment_Plan__c from ITPM_Budget_Item__c where DCPAI_fld_Investment_Plan__c in :listId and DCPAI_fld_Waiting_to_Assign__c = true ];
        for(ITPM_Budget_Item__c ci: ciWait){
            ci.DCPAI_fld_Waiting_to_Assign__c = false;
            ciAlmacenado.add(ci);
        }
        update ciAlmacenado;        
    }
}