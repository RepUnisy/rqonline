/**
*   @name: RQO_cls_Users
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona Usuarios
*/
public with sharing class RQO_cls_Users extends RQO_cls_ApplicationDomain 
{   
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_Users';
    
    /**
    *   @name: RQO_cls_Users
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Constructor de la clase
    */
    public RQO_cls_Users(List<User> users) 
    {     
        super(users);   
    }
    
    /**
    *   @name: RQO_cls_Users
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase que implementa un constructor genérico
    */
    public class Constructor implements fflib_SObjectDomain.IConstructable 
    {     
        public fflib_SObjectDomain construct(List<SObject>sObjectList) 
        {       
            return new RQO_cls_Users(sObjectList);     
        }   
    }  
    
    /**
    *   @name: RQO_cls_Users
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que actualiza un usuario en base a un objeto de unidad de trabajo
    */
    public void updateUser(fflib_ISObjectUnitOfWork uow)
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'updateUser';
        
        for (User user : (List<user>)Records)
        {
            uow.registerDirty(user);
        }
    }
    
    /**
    *   @name: RQO_cls_Users
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que desactiva un usuario para una unidad de trabajo
    */
    public void deactivate(fflib_ISObjectUnitOfWork uow)
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'deactivate';
        
        for (User user : (List<user>)Records)
        {
            System.debug(CLASS_NAME + ' - ' + METHOD + ' user: ' + user);
            user.IsActive = false;
            System.debug(CLASS_NAME + ' - ' + METHOD + ' user.IsActive: ' + user.IsActive);
            uow.registerDirty(user);
        }
    }
    
    /**
    *   @name: RQO_cls_Users
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método para dar de baja usuarios
    */
    public void unregister()
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'unregister';
        
        for(User user : (List<User>) Records)
        {
            // Buscar el usuario/email en Gigya
            string searchUrl = GetGigyaSearchUrl(user.Username);
            RQO_cls_WSCallout searchCallout = new RQO_cls_WSCallout(searchUrl);
            RQO_cls_WSCalloutResponseBean searchResponse = searchCallout.send();
    
            if (searchResponse != null && searchResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK))
            {
                RQO_cls_GigyaSearchResponse.GigyaSearchResponse response = RQO_cls_GigyaSearchResponse.parse(searchResponse.contenido);
                System.debug(CLASS_NAME + ' - ' + METHOD + ' response.objectsCount: ' + response.objectsCount);
                if (response.objectsCount > 0)
                {
                    System.debug(CLASS_NAME + ' - ' + METHOD + ' response.results[0].data.Service: ' + response.results[0].data.Service);
                    // Si existe y ya está dado de alta en Química, darlo de baja
                    if (response.results[0].data.Service != null &&  response.results[0].data.Service.Quimica != null)
                    {
                        string setAccountUrl = GetSetAccountUrl(response.results[0].UID);
                        System.debug(CLASS_NAME + ' - ' + METHOD + ' setAccountUrl: ' + setAccountUrl);
                        RQO_cls_WSCallout setAccountCallout = new RQO_cls_WSCallout(setAccountUrl);
                        RQO_cls_WSCalloutResponseBean setAccountResponse = setAccountCallout.send();
                        if (setAccountResponse != null && setAccountResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK))
                        {
                            System.debug(CLASS_NAME + ' - ' + METHOD + ' usuario dado de baja en Gigya Química: ' + response.results[0].UID);
                        }
                        else
                        {
                            throw new CalloutException('Error llamando API Gigya');
                        }
                    }
                }
            }
            else
            {
                throw new CalloutException('Error llamando API Gigya');
            }
        }
    }
    
    /**
    *   @name: RQO_cls_Users
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene una GigayaSearchUrl en base aun email
    */
    private string GetGigyaSearchUrl(string email)
    {
         /**
         * Constantes
         */
        final String METHOD = 'GetGigyaSearchUrl';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ' email: ' + email);
        
        return 'https://accounts.eu1.gigya.com/accounts.search?apiKey=' + RQO_cls_CustomSettingUtil.getGigyaApiKeyAutorizados() + 
                    '&userKey=' + RQO_cls_CustomSettingUtil.getGigyaUserKey() + '&secret=' + RQO_cls_CustomSettingUtil.getGigyaSecret() + 
                    '&query=' + EncodingUtil.urlEncode('SELECT * FROM accounts where loginIDs.emails=\'' + email + '\' or profile.email contains \'' + email + '\'','UTF-8');
    }
    
    /**
    *   @name: RQO_cls_Users
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece y obtiene una AccountUrl en base aun user Id
    */
    private string GetSetAccountUrl(string uid)
    {
         /**
         * Constantes
         */
        final String METHOD = 'GetSetAccountUrl';
        
        return 'https://accounts.eu1.gigya.com/accounts.setAccountInfo?apiKey=' + RQO_cls_CustomSettingUtil.getGigyaApiKeyAutorizados() + 
                    '&userKey=' + RQO_cls_CustomSettingUtil.getGigyaUserKey() + '&secret=' + RQO_cls_CustomSettingUtil.getGigyaSecret() + 
                    '&UID=' + uid + 
                    getDataParamSetUser();
    }
    
    /**
    *   @name: RQO_cls_Users
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene datos de parámetros y establece un usuario
    */
    private string getDataParamSetUser()
    {
        return '&data={\'Service.Quimica\':null,\'CodCliente.Quimica\':null,\'RepsolClientType\':null}';
    }
    
}