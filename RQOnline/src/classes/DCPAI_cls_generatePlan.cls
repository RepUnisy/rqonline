/*------------------------------------------------------------------------
 * 
 * 	@name: 				DCPAI_cls_generatePlan
 * 	@version: 			1 
 * 	@creation date: 	30/06/2017
 * 	@author: 			Alfonso Constán López	-	Unisys
 * 	@description: 		Lógica del trigger DCPAI_TRG_InvestmentPlan
 * 
----------------------------------------------------------------------------*/

Global class DCPAI_cls_generatePlan {
    
    /*
     * 	@creation date: 		30/06/2017
     * 	@author: 				Alfonso Constán López	-	Unisys
     *	@description:			Método llamado desde el trigger DCPAI_TRG_InvestmentPlan, controlando que no se puedan crear investment plan con años existentes
     *  @param:					listBIs: plan de inversión que se desea crear
	*/
    public static void controlRepeat( List <DCPAI_obj_Investment_Plan__c > listBIs){
        
        Map<String, DCPAI_obj_Investment_Plan__c> mapObj = new Map<String, DCPAI_obj_Investment_Plan__c>(); 
        List<String> listYear = new List<String>();
        for (DCPAI_obj_Investment_Plan__c plan : listBIs){
            mapObj.put(plan.DCPAI_Year__c, plan);
            listYear.add(plan.DCPAI_Year__c);
        }
        
        List<DCPAI_obj_Investment_Plan__c > listInvest = [select id, DCPAI_Year__c from DCPAI_obj_Investment_Plan__c where DCPAI_Year__c in : listYear];        
        
        for (DCPAI_obj_Investment_Plan__c pl : listInvest){           
            mapObj.get(pl.DCPAI_Year__c).addError(Label.DCPAI_Error_Already_Exists);
        }
        
    }
    
    /*
     * 	@creation date: 		30/06/2017
     * 	@author: 				Alfonso Constán López	-	Unisys
     *	@description:			Busca todos los cost item con el mismo año del plan de inversión, y los relacciona
     *  @param:					listBIs: plan de inversión al que se quiere direccionar
	*/
    public static void assignCostItem (List <DCPAI_obj_Investment_Plan__c > listBIs){
        
        List<String> listYear = new List<String>();
        Map<String, Id> mapPlan = new Map<String, Id>();
        for(DCPAI_obj_Investment_Plan__c plan : listBIs){
            listYear.add(plan.DCPAI_Year__c);
            mapPlan.put(plan.DCPAI_Year__c, plan.id);
        }
        
        List<ITPM_Budget_Item__c> listCostItem = [select id, DCPAI_fld_Investment_Plan__c, ITPM_SEL_Year__c from ITPM_Budget_Item__c where DCPAI_fld_Budget_Investment__c != null AND ITPM_SEL_Year__c in : listYear];
        for (ITPM_Budget_Item__c ci: listCostItem ){
            ci.DCPAI_fld_Investment_Plan__c= mapPlan.get(ci.ITPM_SEL_Year__c);
        }
        update listCostItem;
    }
}