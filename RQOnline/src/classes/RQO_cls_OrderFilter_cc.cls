/**
*   @name: RQO_cls_OrderFilter_cc
*   @version: 1.0
*   @creation date: 27/11/2017
*   @author: Alvaro Alonso - Unisys
*   @description: Controlador apex para el componente de filtrado de pedidos
*	@testclass: RQO_cls_OrderFilter_test
*/
public without sharing class RQO_cls_OrderFilter_cc {

    private static final String CLASS_NAME = 'RQO_cls_OrderFilter_cc';
    
    /**
	* @creation date: 21/09/2017j
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera en un mapa todos los datos key - traducción de la lista de selección de estado de los activos
	* @param: origin tipo string, marca en que ventana estamos por si es necesario filtrar algún estado
	* @return: Map<String, String>, donde la key es el código y el value la propia tradcución 
	* @exception: 
	* @throws: 
	*/
    @AuraEnabled
    public static Map<String, String> getStatusPicklist(String origin){
        final String METHOD = 'getStatusPicklist';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Map<String, String> allValidStatus = null;
        Map<String, String> mapAllStatus = null;
        //Recuperamos los mapasa de estado de los objetos asset (posición de solicitud/ posición de pedido) y entrega
        RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
       	Map<String, String> positionStatus = util.getAssetStatus();
        //Si tengo mapa de estados de posiciones lo añado al mapa de todos los estados
        if (positionStatus != null && !positionStatus.isEmpty()){
            if (mapAllStatus == null){mapAllStatus = new Map<String, String>();}
            mapAllStatus.putAll(positionStatus);
        }
        //Si tengo mapa de estados de entrega lo añado al mapa de todos los estados
        Map<String, String> deliveryStatus = util.getDeliveryStatus();
        if (deliveryStatus != null && !deliveryStatus.isEmpty()){
            if (mapAllStatus == null){mapAllStatus = new Map<String, String>();}
            mapAllStatus.putAll(deliveryStatus);
        }
        
        //Recuperamos los estados válidos parametrizados por origen
        Map<String, String> validStatusDataMap = getValidStatusPerOrigin(origin);
        //Verificamos del listado completo que estados están verificados
        if (mapAllStatus != null && !mapAllStatus.isEmpty()){

            allValidStatus = new Map<String, String>();
            if (validStatusDataMap == null || validStatusDataMap.isEmpty()){
				allValidStatus.putAll(mapAllStatus);
			}else{
				String actualValue = '';
				for(String dato : mapAllStatus.keySet()){
					actualValue = mapAllStatus.get(dato);
					//Si el mapa está nulo o vacío añadimos 
					if (validStatusDataMap.containsKey(dato)){
						allValidStatus.put(dato, actualValue);
					}
				}
			}
        }       
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return allValidStatus;
    }
	
    /**
	* @creation date: 19/01/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera en un mapa todos los estados que son validados para el origen (historico, seguimiento) que le pasamos
	* @param: origin tipo string, marca en que ventana estamos por si es necesario filtrar algún estado
	* @return: Map<String, String>, donde la key es el código y el value la propia tradcución 
	* @exception: 
	* @throws: 
	*/
    private static Map<String, String> getValidStatusPerOrigin(String origin){
		final String METHOD = 'getValidStatusPerOrigin';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		Map<String, String> validStatusDataMap =null;
        for (RQO_cmt_orderFilterStatus__mdt recordData : [Select RQO_fld_validStatus__c from RQO_cmt_orderFilterStatus__mdt where RQO_fld_type__c = : origin]){
            if (validStatusDataMap == null){validStatusDataMap = new Map<String, String>();}
            validStatusDataMap.put(recordData.RQO_fld_validStatus__c, recordData.RQO_fld_validStatus__c);
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return validStatusDataMap;
    }
    
    /**
	* @creation date: 10/01/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera los grados para la búsqueda predictiva del grado. En el histórico y el seguimiento la búsqueda predictiva 
	* se realiza sobre los grados QP0 asociados a las posiciones de pedido/solicitud que ha pedido el cliente ya que es la descripción de esos grados QP0
	* la que se muestra por pantalla
	* @param: accountId tipo Id
	* @param: searchKey búsqueda del usuario
	* @return: String, JSON con la lista de grados QP0 disponibles
	* @exception: 
	* @throws: 
	*/
	@AuraEnabled
    public static List<String> getAllGrades(Id accountId, String searchKey){
        final String METHOD = 'getAllGrades';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String gradeDescription = '';
        Boolean noQP0Grade = false;
        String dataToSearch = Label.RQO_lbl_Porcentaje +  searchKey +  Label.RQO_lbl_Porcentaje;
        List<String> listPredictiveGrades = null;
        //Buscamos con una consulta agregada aquellos grados QP0 asociados a los activos (posiciones) de la cuenta
        for (AggregateResult ar : [Select RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c from Asset where AccountId =: accountId and RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c LIKE :dataToSearch group by RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c LIMIT 50000]){
            gradeDescription = String.valueOf(ar.get('RQO_fld_descripcionDelGrado__c'));
			//El activo no tiene grado QP0
            if (String.isEmpty(gradeDescription)){
                noQP0Grade = true;
            }else{
                if (listPredictiveGrades == null){listPredictiveGrades = new List<String>();}
                listPredictiveGrades.add(gradeDescription);
            }
        }
		//Si la variable está a true es porque hay algún caso en que las posiciones de pedido no tiene grado QP0. Esto puede ocurrir cuando se envían posiciones de
		//pedido desde SAP que no se han solicitado en Salesforce. En ese caso tenemos que ir a buscar el dato a un campo auxiliar que establece SAP al generar la 
		//posición
        if (noQP0Grade){
            for(Asset activo : [Select RQO_fld_descripciondeGrado__c from Asset where AccountId =: accountId and RQO_fld_qp0Grade__c = '' and RQO_fld_descripciondeGrado__c LIKE :dataToSearch LIMIT 50000]){
                if (String.isNotEmpty(activo.RQO_fld_descripciondeGrado__c)){
                    if (listPredictiveGrades == null){listPredictiveGrades = new List<String>();}
                    listPredictiveGrades.add(activo.RQO_fld_descripciondeGrado__c);
                }
            }
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return listPredictiveGrades;
    }
    
    /**
    * @creation date: 20/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera del custom metadata type el rango de años que aplican para el filtro de pedidos en función del origen
    * @return: Integer
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static Integer getDateParametrizedData(String origen){
        
        final String METHOD = 'getDateParametrizedData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Integer mesesDiferencia = 3;
        
        RQO_cmt_dateFilterData__mdt objDateFilter = RQO_cls_CustomSettingUtil.getParametrizacionDateFilterData(origen);
        if (objDateFilter != null && objDateFilter.RQO_fld_numeroMeses__c != null){
            mesesDiferencia = Integer.valueOf(objDateFilter.RQO_fld_numeroMeses__c);
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return mesesDiferencia;
    }
}