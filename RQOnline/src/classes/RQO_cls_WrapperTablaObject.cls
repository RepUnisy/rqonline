/**
 *	@name: RQO_cls_WrapperTablaObject
 *	@version: 1.0
 *	@creation date: 12/09/2017
 *	@author: Nicolás García - Unisys
 *	@description: Clase Wrapper que gestiona los datos que llegan a las tablas
 *
 */
public with sharing class RQO_cls_WrapperTablaObject {
	
    public Map<Id, RQO_obj_translation__c> traduccionesGrados{get; set;}
    private Map<Id, Map<String, String>> valoresTabla;
    private Map<Id, RQO_obj_grade__c> mapaInfoGrados;
    private List<Id> listOrderedGrades;
    public List<RQO_obj_catSpecification__c> listaEspecificaciones{get; set;}
    public List<RQO_obj_translation__c> listaTraduccionesEspeci{get; set;}
    public Id categoria {get; set;}
    public String nombreCategoria {get; set;}
    public Map<String, String> tipoDatosCategorias{get; set;}
    
    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método constructor de la clase RQO_cls_WrapperTablaObject
     *
     */
	public RQO_cls_WrapperTablaObject(){
        traduccionesGrados = new Map<Id, RQO_obj_translation__c>();
        valoresTabla = new Map<Id, Map<String, String>>();
        listaEspecificaciones = new List<RQO_obj_catSpecification__c>();
        mapaInfoGrados = new Map<Id, RQO_obj_grade__c>();
        listOrderedGrades = new List<Id>();
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método constructor de la clase RQO_cls_WrapperTablaObject
     *
     */
    public Map<Id, Map<String, String>> getValoresTabla(){
    	return valoresTabla;
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que establece un tipo de dato para la categorías de especificaciones
     *
     */
    public void setTipoDatoEspecificaciones(){
        tipoDatosCategorias = new Map<String, String>();
        for(RQO_obj_catSpecification__c esp : listaEspecificaciones){
            tipoDatosCategorias.put(esp.Id, esp.RQO_fld_masterSpecification__r.RQO_fld_tipologiaTexto__c);
        }
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que relaciona un grado con una característica y un elemento
     *
     */
    public void putElement(Id grado, String caracteristica, String elemento){
    	System.debug('grado: ' + grado + ', caracteristica: ' + caracteristica + ', elemento: ' + elemento);
    	if(valoresTabla.containsKey(grado)){
    		if(valoresTabla.get(grado).containsKey(caracteristica)){
    			valoresTabla.get(grado).put(caracteristica, elemento);
    			System.debug('1');
    		}
    		else{
    			Map<String, String> aux = new Map<String, String>();
    			aux.put(caracteristica, elemento);
    			valoresTabla.get(grado).put(caracteristica, elemento);
    			System.debug('2');
    		}
    	}
    	else{
    		Map<String, String> aux = new Map<String, String>();
    		aux.put(caracteristica, elemento);
    		valoresTabla.put(grado, aux);
    		System.debug('3');
    	}
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que relaciona un grado con una característica y un elemento
     *
     */
    public String getElement(Id grado, String caracteristica){
        if(valoresTabla.containsKey(grado)){
            if(valoresTabla.get(grado).containsKey(caracteristica)){
                return valoresTabla.get(grado).get(caracteristica);
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que establece una lista de especificaciones
     *
     */
    public void setListaEspecificaciones(List<RQO_obj_catSpecification__c> listaEntrada){
        listaEspecificaciones = listaEntrada.deepClone(true, true, false);
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que establece una lista de traducciones de especificaciones
     *
     */
    public void setListaTraduccionesEspecifi(List<RQO_obj_translation__c> listaEntrada){
        listaTraduccionesEspeci = listaEntrada.deepClone(true, true, false);
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que establece una lista de categorías para un id de entrada
     *
     */
    public void setCategoria(Id entrada){
        categoria = entrada;
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que establece un nombre de categoría
     *
     */
    public void setNombreCategoria (String entrada){
        nombreCategoria = entrada;
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que establece traducciones para un grado a través de un mapa (idGrade - translation)
     *
     */
    public void setTraduccionesGrados(Map<Id, RQO_obj_translation__c> traducciones){
        traduccionesGrados = traducciones;
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que rellena huecos en base a una lista de grados
     *
     */
    public void rellenarHuecos(List<RQO_obj_grade__c> listGrados){
    	for(Id grado : valoresTabla.keySet()){
    		Map<String, String> especificacionesGrado = valoresTabla.get(grado);
    		if(especificacionesGrado.size() < listaEspecificaciones.size()){
				for(RQO_obj_catSpecification__c especificacion : listaEspecificaciones){
		    		if(especificacionesGrado.containsKey(especificacion.Id)){
		    			this.putElement(grado, String.valueOf(especificacion.Id), especificacionesGrado.get(especificacion.Id));
		    		}
		    		else{
		    			this.putElement(grado, especificacion.Id, Label.RQO_lbl_guion);
		    		}
		    	}
    		}
    	}
        for(RQO_obj_grade__c gradoSinEspecificaciones: listGrados){
        	ordenarMapaInfoGrados(gradoSinEspecificaciones);
        	if(!valoresTabla.containsKey(gradoSinEspecificaciones.Id)){
                for(RQO_obj_catSpecification__c especificacion : listaEspecificaciones){
		    		this.putElement(gradoSinEspecificaciones.Id, especificacion.Id, Label.RQO_lbl_guion);
		    	}
            }
        }
    }

    /**
     *  @name: RQO_cls_WrapperTablaObject
     *  @version: 1.0
     *  @creation date: 12/09/2017
     *  @author: Nicolás García - Unisys
     *  @description: Método que establece traducciones para un grado a través de un mapa (idGrade - translation)
     *
     */
    public void ordenarMapaInfoGrados(RQO_obj_grade__c grado){
    	mapaInfoGrados.put(grado.Id, grado);
        if(listOrderedGrades.size() == 0){
            listOrderedGrades.add(grado.Id);
        }
        else{
            if(grado.RQO_fld_recommendation__c){
                listOrderedGrades.add(0, grado.Id);
            }
            else{
                listOrderedGrades.add(grado.Id);
            }
        }
    }
}