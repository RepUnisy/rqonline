/**
 *	@name: RQO_cls_Notificacion_cc
 *	@version: 1.0
 *	@creation date: 18/12/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase controladora de la parte visual de notificaciones
 */
public without sharing class RQO_cls_Notificacion_cc {
    
    // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_Notificacion_cc';
    
    /**
    * @creation date: 18/12/2017
    * @author: David Iglesias - Unisys
    * @description: Método invocable desde componente lightning que permite la recuperación de notificaciones por usuario
    * @param: idContacto 		Identificador del usuario para el que se realiza la búsqueda
	* @param: categoria 		Tipo de objeto 
	* @param: fechaInicio 		Fecha de inicio para la búsqueda
	* @param: fechaFin 			Fecha de fin para la búsqueda
	* @param: visualized 		Flag para busqueda inicial o por filtro
    * @return: String			Mapa serializado con las notificacciones a mostrar
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static String getNotifications(String idContacto, String categoria, String fechaInicio, String fechaFin, Boolean visualized) {
        
        /**
        * Constantes
        */
        final String METHOD = 'getNotifications';
	
		/**
		 * Variables
		 */
        Map <Date, List<RQO_cls_NotificacionBean>> notificacionesOrderMap = null;
        String responseData = '';
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		List<RQO_cls_NotificacionParent> listData = getNotificationData(idContacto, categoria, fechaInicio, fechaFin, visualized, false);
        if (listData != null && !listData.isEmpty()){
			listData.sort();
		}
        responseData = JSON.serialize(listData);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return responseData;
        
    }
	

	
	/**
    * @creation date: 19/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera el mapa de notificaciones por fecha para el contacto que llega por parámetro
    * @param: idContacto 		Identificador del usuario para el que se realiza la búsqueda
	* @param: categoria 		Tipo de objeto 
	* @param: fechaInicio 		Fecha de inicio para la búsqueda
	* @param: fechaFin 			Fecha de fin para la búsqueda
	* @param: visualized 		Flag para busqueda inicial o por filtro
    * @return: String			Mapa serializado con las notificacciones a mostrar
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static List<RQO_cls_NotificacionParent> getNotificationData(String idContacto, String categoria, String fechaInicio, String fechaFin, 
                                                                       Boolean visualized, Boolean isHome){
	    /**
        * Constantes
        */
        final String METHOD = 'getNotificationData';
	
		/**
		 * Variables
		 */
		String literal = null;
        RQO_cls_NotificacionBean notificacion = null;
		List<RQO_obj_notification__c> notificationByUserList = null;
		List<RQO_obj_notification__c> updateList = new List<RQO_obj_notification__c> ();
		List<RQO_cls_NotificacionBean> notificacionesList = null;
		List<String> orderPositionList = new List<String> ();
		List<String> entregasList = new List<String> ();
        List<String> requestPositionList = new List<String>();
		List<String> gradosList = new List<String> ();
        List<String> gradosQp0List = new List<String> ();
		List<Schema.PicklistEntry> objectTypeList = null;
		Map <Date, List<RQO_cls_NotificacionBean>> notificacionesByDateMap = new Map <Date, List<RQO_cls_NotificacionBean>> ();
		Map <String, Asset> pedidoByRequestPosition = null;
		Map <String, Asset> pedidoByOrderPosition = null;
		Map <String, RQO_obj_posiciondeEntrega__c> pedidoByEntregaMap = null;
		Map <String, String> translationByGradeMap = null;
		Map<String, String> objectTypeMap = new Map<String, String>();    
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        try {
			// Obtencion de las notificaciones asociadas al contacto
			notificationByUserList = getRecordsNotification(idContacto, categoria, fechaInicio, fechaFin, visualized);
			if (notificationByUserList != null && !notificationByUserList.isEmpty()){
                for (RQO_obj_notification__c item : notificationByUserList) {
                    if (RQO_cls_Constantes.COD_SF_NOT_ENTREGA.equalsIgnoreCase(item.RQO_fld_objectType__c)) {
                        entregasList.add(item.RQO_fld_externalId__c);
                    } else if (RQO_cls_Constantes.COD_SF_NOT_GRADO.equalsIgnoreCase(item.RQO_fld_objectType__c)) {
                        gradosList.add(item.RQO_fld_externalId__c);
                    }else if(RQO_cls_Constantes.COD_SF_NOT_DOCUMENTACION.equalsIgnoreCase(item.RQO_fld_objectType__c)
                            || RQO_cls_Constantes.COD_PROD_CERT_GRADO.equalsIgnoreCase(item.RQO_fld_objectType__c)){
                        gradosQp0List.add(item.RQO_fld_externalId__c);
					}else if (RQO_cls_Constantes.COD_SAP_NOT_PEDIDO.equalsIgnoreCase(item.RQO_fld_objectType__c)) {
						orderPositionList.add(item.RQO_fld_externalId__c);
					}else if (RQO_cls_Constantes.COD_SAP_NOT_SOLICITUD.equalsIgnoreCase(item.RQO_fld_objectType__c)) {
						requestPositionList.add(item.RQO_fld_externalId__c);
					}
                }
                
                // Obtencion de caracteristicas de grados asociados a las notificaciones
                if ((gradosList != null && !gradosList.isEmpty()) || (gradosQp0List != null && !gradosQp0List.isEmpty())) {
                    translationByGradeMap = getTranslationByGrade(gradosList, gradosQp0List);
                }
                
				// Obtencion de solicitudes asociadas a las posiciones de solicitud almacenadas en el id externo del objeto notificaciones
                if (requestPositionList != null && !requestPositionList.isEmpty()) {
                    pedidoByRequestPosition = getPedidoByRequestPosition(requestPositionList);
                }
                
                // Obtencion de pedidos asociados a las posiciones de pedido almacenadas en el ide externo del objeto notificaciones
                if (orderPositionList != null && !orderPositionList.isEmpty()) {
                    pedidoByOrderPosition = getPedidoByOrderPosition(orderPositionList);
                }
                
                // Obtencion de caracteristicas de entregas asociadas a las notificaciones
                if (entregasList != null && !entregasList.isEmpty()) {
                    pedidoByEntregaMap = getPedidoByEntrega(entregasList);
                }
                
                // Mapeo de picklist para el campo RQO_fld_objectType del objeto RQO_obj_notification__c
                objectTypeList = RQO_obj_notification__c.RQO_fld_objectType__c.getDescribe().getPicklistValues();        
                for( Schema.PicklistEntry f : objectTypeList){
                    objectTypeMap.put(f.getValue(), f.getLabel());
                }   
			
                // Generación de los objetos para la vista
                for (RQO_obj_notification__c item : notificationByUserList) {
                    notificacion = new RQO_cls_NotificacionBean();
                    notificacion.identificador = item.RQO_fld_externalId__c;
                    notificacion.tipoObjeto = objectTypeMap.get(item.RQO_fld_objectType__c);
                    notificacion.tipoObjetoKey = item.RQO_fld_objectType__c;
                    
                    notificacion.isNew = (!item.RQO_fld_visualized__c) ? true : false;
                    notificacion.fechaCreacion = item.Createddate;
                    
                    // Mapeo de literal a mostrar según estado y tipo de objeto
                    //notificacion.literal = mapearLiteral(item, translationByGradeMap, pedidoByEntregaMap, pedidoByOrderPosition);
                    mapearLiteral(item, notificacion, translationByGradeMap, pedidoByEntregaMap, pedidoByOrderPosition, pedidoByRequestPosition);
                    
                    // Si no existe literal no es una notificación a mostrar al usuario
                    if (notificacion.literal != null) {
                        // Ordenación del mapa a retornar
                        if (notificacionesByDateMap.get(item.CreatedDate.date()) == null) {
                            notificacionesList = new List<RQO_cls_NotificacionBean> ();
                        } else {
                            notificacionesList = notificacionesByDateMap.get(item.CreatedDate.date());
                        }
                        notificacionesList.add(notificacion);
                        notificacionesByDateMap.put(item.CreatedDate.date(), notificacionesList);
                        
                        // Modificación del flag de visualización
                        item.RQO_fld_visualized__c = true;
                        updateList.add(item);
                    }
    
                }
                // Actualización del flag de notificaciones visualizadas
                if (!isHome && updateList != null && !updateList.isEmpty()) {
                    update updateList;
                }
            }
            
           	// Se añaden las comunicaciones multimedia
            if (String.isEmpty(categoria) || categoria == Label.RQO_lbl_notifCodeComunicacion) {
            
	            List<Contact> contacts = new RQO_cls_ContactSelector().selectById(new Set<ID> { idContacto });
	            
	            System.debug(CLASS_NAME + ' - ' + METHOD + ': contacts.size()' + contacts.size());
	            
	            if (contacts != null && contacts.size() > 0)
	            {
	                Contact contacto = contacts[0];
	                List<String> customerProfiles = new List<String>{contacto.RQO_fld_perfilCliente__c};
	                List<String> userProfiles = new List<String>{contacto.RQO_fld_perfilUsuario__c};
	                Date startDate = null;
	                if (String.isNotEmpty(fechaInicio))
	                	startDate = Date.valueOf(fechaInicio);
					Date endDate = null;
	                if (String.isNotEmpty(fechaFin))
	                	endDate = Date.valueOf(fechaFin);
	                List<RQO_obj_communication__c> comms = new RQO_cls_CommunicationsSelector().
	                        	selectActiveMultimedia(new Set<Id> {UserInfo.getUserId()}, customerProfiles, userProfiles, startDate, endDate);
	                List<RQO_obj_notification__c> notifs = 
	                	new RQO_cls_NotificationSelector().selectVisualizedByContactId(new Set<String> { contacto.Id },
	                																   RQO_cls_Constantes.COD_COM_MULTIMEDIA);
	                for (RQO_obj_communication__c comm : comms) {
	                	
	                    notificacion = new RQO_cls_NotificacionBean();
	                    notificacion.identificador = comm.Id;
	                    notificacion.identificadorRedirect = comm.Id;
	                    notificacion.tipoObjeto = objectTypeMap.get(RQO_cls_Constantes.COD_SF_NOT_COMUNICACION);
	                    notificacion.tipoObjetoKey = RQO_cls_Constantes.COD_SF_NOT_COMUNICACION;
	                    notificacion.literal = comm.Name;
	                    notificacion.isNew = true;
	                    for (RQO_obj_notification__c nofif : notifs) {
	                    	if (nofif.RQO_fld_externalId__c == String.valueOf(comm.Id))
	                    		notificacion.isNew = false;
	                    }
	                    
	                    // Ordenación del mapa a retornar
	                    if (notificacionesByDateMap.get(comm.RQO_fld_startDate__c) == null) {
	                        notificacionesList = new List<RQO_cls_NotificacionBean> ();
	                    } else {
	                        notificacionesList = notificacionesByDateMap.get(comm.RQO_fld_startDate__c);
	                    }
	                    notificacionesList.add(notificacion);
	                    notificacionesByDateMap.put(comm.RQO_fld_startDate__c, notificacionesList);
	                    
	                   	// Registrar comunicación como visualizada (Solo si es nueva)
	                   	if (notificacion.isNew)
	                   	{
							RQO_obj_notification__c notification = new RQO_obj_notification__c();
							notification.RQO_fld_contact__c = contacto.Id;
							notification.RQO_fld_externalId__c = comm.Id;
							notification.RQO_fld_objectType__c = RQO_cls_Constantes.COD_SF_NOT_COMUNICACION;
							notification.RQO_fld_messageType__c = RQO_cls_Constantes.COD_COM_MULTIMEDIA;
							notification.RQO_fld_mailSent__c = true;
							notification.RQO_fld_visualized__c = true;
							insert notification;
	                   	}
	                }
	            }
            }
        } catch (Exception e) {
            System.debug(CLASS_NAME + ' - ' + METHOD + ': Excepción genérica: ' + e.getMessage());
            System.debug('StackTrace: ' + e.getStackTraceString());
            notificacionesByDateMap.clear();
			notificacion = new RQO_cls_NotificacionBean();
			notificacion.literal = Label.RQO_lbl_mensajeErrorSolicitud;
			notificacionesByDateMap.put(Date.Today(), new List<RQO_cls_NotificacionBean> {notificacion});
        }
        
        
        List<RQO_cls_NotificacionParent> returnData = createNotificationListObject(notificacionesByDateMap);
        
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return returnData;
	}
	
    /**
    * @creation date: 16/02/201
    * @author: Alvaro Alonso - Unisys
    * @description: Crea la lista de objeto de tipo RQO_cls_NotificacionParent en función de los datos de notificaciones que hemos recuperado y agrupado en un mapa
    * @param: notificacionesByDateMap Map <Date, List<RQO_cls_NotificacionBean>>
    * @return: List<RQO_cls_NotificacionParent>
    * @exception: 
    * @throws: 
    */
    private static List<RQO_cls_NotificacionParent> createNotificationListObject(Map <Date, List<RQO_cls_NotificacionBean>> notificacionesByDateMap){
       	final String METHOD = 'createNotificationListObject';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<RQO_cls_NotificacionParent> returnData = null;
        
        if (notificacionesByDateMap != null && !notificacionesByDateMap.isEmpty()){
            RQO_cls_NotificacionParent objData = null;
            List<RQO_cls_NotificacionBean> lista = null;
            //Recorremos el mapa y generamos la lista de objetos
            for (Date data : notificacionesByDateMap.keySet()){
                lista = notificacionesByDateMap.get(data);
                lista.sort();
                objData = new RQO_cls_NotificacionParent();
                objData.fechaCreacion = data;
                objData.listNotification = lista;
                if (returnData == null){returnData = new List<RQO_cls_NotificacionParent>();}
                returnData.add(objData);
            }
		}
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return returnData;
    }
    
    /**
    * @creation date: 18/12/2017
    * @author: Unisys
    * @description: Método invocable desde componente lightning que permite la recuperación las 3 últimas notificaciones
    * @param: idContacto 		Identificador del usuario para el que se realiza la búsqueda
    * @return: String			Mapa serializado con las notificacciones a mostrar
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static String getLastNotifications(String idContacto) {
        
        /**
        * Constantes
        */
        final String METHOD = 'getLastNotifications';
	
		/**
		 * Variables
		 */        

        String responseData = '';
        List<RQO_cls_NotificacionParent> listDataFinal = null;
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        
		List<RQO_cls_NotificacionParent> listData = getNotificationData(idContacto, null, null, null, true, true);
        if (listData != null && !listData.isEmpty()){
			listData.sort();
            RQO_cls_NotificacionParent objData = null;
            listDataFinal = new List<RQO_cls_NotificacionParent>();
            Integer count = 0;
            for(RQO_cls_NotificacionParent obj : listData){
				objData = new RQO_cls_NotificacionParent();
                objData.listNotification = new List<RQO_cls_NotificacionBean>();
                for (RQO_cls_NotificacionBean objBean : obj.listNotification){ 
                    objData.listNotification.add(objBean);
                    count++;
                    if (count == 3){System.debug('Break: ' + count);break;}
                }
                listDataFinal.add(objData);
                if (count == 3){System.debug('Break: ' + count);break;}
            }
		}
        responseData = JSON.serialize(listDataFinal);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return responseData;
    }
	
    // ***** METODOS PRIVADOS ***** //
	
    /**
    * @creation date: 18/12/2017
    * @author: David Iglesias - Unisys
	* @description:	Método encargado de mapear el literal de la notificación a mostrar
	* @param: idContacto 						Identificador del usuario para el que se realiza la búsqueda
	* @param: categoria 						Tipo de objeto 
	* @param: fechaInicio 						Fecha de inicio para la búsqueda
	* @param: fechaFin 							Fecha de fin para la búsqueda
	* @param: visualized 						Flag para busqueda inicial o por filtro
	* @return:	List<RQO_obj_notification__c>	List de notificaciones a tratar
	* @exception: 
	* @throws: 
	*/
    @TestVisible
	private static List<RQO_obj_notification__c> getRecordsNotification (String idContacto, String categoria, String fechaInicio, String fechaFin, Boolean visualized) {
		
		/**
        * Constantes
        */
        final String METHOD = 'getRecordsNotification';
	
		/**
		 * Variables
		 */
		List<RQO_obj_notification__c> notificationByUserList = null;
        String query = null;
        Date startDate = null;
        Date endDate = null;
        Map<String, RQO_cmt_notificationCategories__mdt> listValidMessageType = null;
        Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>> contatcValidationMap = null;
        RQO_cls_GeneralUtilities util = null;
        
		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
        try {
            //Vamos a buscar cuales son las notificaciones que el contacto actual puede visualizar
            util = new RQO_cls_GeneralUtilities();
            contatcValidationMap = util.getValidMessageTypeForContact(new List<Id>{idContacto});
            //Recuperamos el mapa de validaciones del contacto y si no está vacío lanzamos las consultas
            if (contatcValidationMap != null && !contatcValidationMap.isEmpty()){listValidMessageType = contatcValidationMap.get(idContacto);}
            if (listValidMessageType != null && !listValidMessageType.isEmpty()){
                
                query = 'SELECT Id, RQO_fld_externalId__c, RQO_fld_messageType__c, RQO_fld_objectType__c, RQO_fld_visualized__c, RQO_fld_dynamicField__c,CreatedDate FROM RQO_obj_notification__c WHERE RQO_fld_contact__c = :idContacto';
                //Añadimos en la consulta el listado de consentimientos que aplican para el contacto
                Set<String> keySetMess = listValidMessageType.keySet();
                query += ' and RQO_fld_messageType__c = :keySetMess ';
 
                if (!visualized) {
                    query = query + ' AND RQO_fld_visualized__c = :visualized';
                } else {
                    if (String.isNotBlank(categoria)) {
                        query = query + ' AND RQO_fld_objectType__c = :categoria';
                    }
                    if (String.isNotBlank(fechaInicio)) {
                        startDate = Date.valueOf(fechaInicio);
                        DateTime startDateDT = DateTime.newInstanceGMT(startDate.year(),startDate.month(),startDate.day(), 0, 0, 0);
                        query = query + ' AND CreatedDate >= :startDateDT';
                    }
                    if (String.isNotBlank(fechaFin)) {
                        endDate = Date.valueOf(fechaFin);
						DateTime endDateDT = DateTime.newInstanceGMT(endDate.year(),endDate.month(),endDate.day(), 23, 59, 59);
                        query = query + ' AND CreatedDate <= :endDateDT';
                    }
                }
                
                query = query + ' ORDER BY CreatedDate ASC';
                notificationByUserList = Database.query(query);
                
                System.debug(CLASS_NAME + ' - ' + METHOD + ': notificationByUserList.size() ' + notificationByUserList.size());
			}
        } catch (Exception e) {
            System.debug(CLASS_NAME + ' - ' + METHOD + ': Excepción genérica: ' + e.getMessage());
            throw e;
        }
		 
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return notificationByUserList;						
		
	}
	
    /**
    * @creation date: 18/12/2017
    * @author: David Iglesias - Unisys
	* @description:	Método encargado de mapear el literal de la notificación a mostrar
	* @param:	notificationItem				Objeto de notificacion a mapear
	* @param:	translationByGradeMap			Mapa con los valored de las traducciones de los grados afectados
	* @param:	pedidoByEntrega					Mapa con los valores de los pedidos para cada número de entrega
	* @return: 	String							Literal custom para la casuistica dada
	* @exception: 
	* @throws: 
	*/
    @TestVisible
	private static void mapearLiteral (RQO_obj_notification__c notificationItem, RQO_cls_NotificacionBean notificacion, Map <String, String> translationByGradeMap, 
                                         Map <String, RQO_obj_posiciondeEntrega__c> pedidoByEntrega, Map <String, Asset> pedidoByOrderPosition,
                                      	 Map <String, Asset> pedidoByRequestPosition) {
		
        /**
        * Constantes
        */
        final String METHOD = 'mapearLiteral';
	
		/**
		 * Variables
		 */
		//String valueReturn = null;
        ID idExterno = null;
		Asset auxAsset = null;
		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        //Se cargan los datos especificos correspondientes a cada tipo de notificación
		if (RQO_cls_Constantes.COD_PED_TRATAMIENTO.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación pedido en tratamiento/en curso
            //Recuperamos del mapa la posición de pedido (asset) para este id externo
            auxAsset = ((pedidoByOrderPosition != null && !pedidoByOrderPosition.isEmpty()) ? pedidoByOrderPosition.get(notificationItem.RQO_fld_externalId__c) : null);
            //Cargamos el identificador de redirección para el link de ver más con el número de pedido
            notificacion.identificadorRedirect = (auxAsset != null && auxAsset.RQO_fld_idRelacion__c !=null) ? auxAsset.RQO_fld_idRelacion__r.RQO_fld_idExterno__c : '';
			//Cargamos el literal a mostrar
            notificacion.literal = Label.RQO_lbl_mensajeenCurso + ' ' + notificacion.identificadorRedirect;
            //Cargamos la fecha preferente de entrega para el prefiltro en el histórico
            notificacion.dateRedirect = (auxAsset != null) ? auxAsset.RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c : null;
		} else if (RQO_cls_Constantes.COD_PED_CONFIRMADO.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación pedido confirmado (va a nivel de entrega)
            getPosicionEntregaViewData(notificationItem.RQO_fld_externalId__c, notificacion, pedidoByEntrega);
            notificacion.literal = Label.RQO_lbl_entregaConfirmado + ' ' + notificacion.identificadorRedirect;
		} else if (RQO_cls_Constantes.COD_PROD_NOTA_TECNICA.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación actualización de nota técnica
			idExterno = notificationItem.RQO_fld_externalId__c;
            notificacion.identificadorRedirect = notificationItem.RQO_fld_dynamicField__c;
            notificacion.literal = Label.RQO_lbl_actualizacionNotaTecnica + ' ' + translationByGradeMap.get(idExterno);
		} else if (RQO_cls_Constantes.COD_PROD_CERT_GRADO.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificiación actualización certificado de cumplimiento
            idExterno = notificationItem.RQO_fld_externalId__c;
            notificacion.identificadorRedirect = notificationItem.RQO_fld_dynamicField__c;
			notificacion.literal = Label.RQO_lbl_actualizacionCertificado + ' ' + translationByGradeMap.get(idExterno);
		} else if (RQO_cls_Constantes.COD_PROD_RECOM_GRADO.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación grado recomendado
            notificacion.identificadorRedirect = notificationItem.RQO_fld_externalId__c;
			notificacion.literal = Label.RQO_lbl_recomendacion + ' ' + translationByGradeMap.get(notificationItem.RQO_fld_externalId__c);
		} else if (RQO_cls_Constantes.COD_PROD_NEW_GRADO.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación nuevo grado
            notificacion.identificadorRedirect = notificationItem.RQO_fld_externalId__c;
			notificacion.literal = Label.RQO_lbl_nuevoGrado + ' ' + translationByGradeMap.get(notificationItem.RQO_fld_externalId__c);
		} else if (RQO_cls_Constantes.COD_PED_TRANSITO.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación pedido en tránsito (va a nivel de entrega)
            getPosicionEntregaViewData(notificationItem.RQO_fld_externalId__c, notificacion, pedidoByEntrega);
			notificacion.literal = Label.RQO_lbl_entregaEnTransito + ' ' + notificacion.identificadorRedirect;
		} else if (RQO_cls_Constantes.COD_PED_ENTREGADO.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación pedido entregado
            getPosicionEntregaViewData(notificationItem.RQO_fld_externalId__c, notificacion, pedidoByEntrega);
			notificacion.literal = Label.RQO_lbl_entregaEntregada + ' ' + notificacion.identificadorRedirect;
		} else if (RQO_cls_Constantes.COD_FAC_NEW_VIEW.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación nueva factura
            notificacion.identificadorRedirect = notificationItem.RQO_fld_externalId__c;
			notificacion.literal = Label.RQO_lbl_nuevaFactura + ' ' + notificationItem.RQO_fld_externalId__c;
		} else if (RQO_cls_Constantes.COD_FAC_PROX_VENCIMIENTO_VIEW.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación factura próximo vencimiento
            notificacion.identificadorRedirect = notificationItem.RQO_fld_externalId__c;
			notificacion.literal = Label.RQO_lbl_facturaProximaVencer + ' ' + notificationItem.RQO_fld_externalId__c;
		} else if (RQO_cls_Constantes.COD_FAC_VENCIDA_VIEW.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación factura vencida
            notificacion.identificadorRedirect = notificationItem.RQO_fld_externalId__c;
			notificacion.literal = Label.RQO_lbl_facturaVencida + ' ' + notificationItem.RQO_fld_externalId__c;
		} else if (RQO_cls_Constantes.COD_PED_SOLICITADO.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación solicitud de pedido
            //Recuperamos del mapa la posición de solicitud (asset) para este id externo
            auxAsset = ((pedidoByRequestPosition != null && !pedidoByRequestPosition.isEmpty()) ? pedidoByRequestPosition.get(notificationItem.RQO_fld_externalId__c) : null);
            //Cargamos el identificador de redirección para el link de ver más con el número de solicitud
            notificacion.identificadorRedirect = (auxAsset != null && auxAsset.RQO_fld_requestManagement__c !=null) ? auxAsset.RQO_fld_requestManagement__r.name : '';
            //Cargamos el literal a mostrar
			notificacion.literal = Label.RQO_lbl_entregaSolicitado + ' ' + notificationItem.RQO_fld_externalId__c;
            //Cargamos la fecha preferente de entrega para el prefiltro en el histórico
            notificacion.dateRedirect = (auxAsset != null) ? auxAsset.RQO_fld_CUDeliveryDate__c : null;
		} else if (RQO_cls_Constantes.COD_EQUIPO_COMERCIAL_VIEW.equalsIgnoreCase(notificationItem.RQO_fld_messageType__c)) {
            //Notificación cambio de equipo comercial
			notificacion.literal = Label.RQO_lbl_cambioEquipoRepsol;
            notificacion.auxIconCode = RQO_cls_Constantes.COD_NOTIFICACION_ICON_CAMBIO_EQUIPO_COMERCIAL;
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		//return valueReturn;
		
	}
	
	/**
    * @creation date: 22/02/2018
    * @author: Alvaro Alonso - Unisys
	* @description:	Método auxiliar de apoyo al método "mapearLiteral" que carga las característica comunes a presentar en la de los diferentes estados 
	* de la categoría entrega
	* @param: idExterno tipo String
	* @param:	notificacion tipo RQO_cls_NotificacionBean
	* @param: pedidoByEntrega tipo Map <String, RQO_obj_posiciondeEntrega__c> 
	* @return: 
	* @exception: 
	* @throws: 
	*/
    @TestVisible
    private static void getPosicionEntregaViewData(String idExterno, RQO_cls_NotificacionBean notificacion, Map <String, RQO_obj_posiciondeEntrega__c> pedidoByEntrega){
        //Recuperamos el objeto posición de entrega asociado al id externo (número de entrega) que llega por parámetro. En el objeto tenemos los datos relacionados
        //hasta el pedido
		RQO_obj_posiciondeEntrega__c auxPosEntrega = (pedidoByEntrega != null && !pedidoByEntrega.isEmpty()) ? pedidoByEntrega.get(idExterno) : null;
        if (auxPosEntrega != null && auxPosEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__c != null){
            //Si tenemos id externo a nivel de pedido lo establecemos en el identificador de redirección que utilizaremos para mandar datos al filtro de pedidos
            notificacion.identificadorRedirect = (String.isNotBlank(auxPosEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_idExterno__c)) ? 
                                                        auxPosEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_idExterno__c : '';
			//Recuperamos la fecha preferente del pedido asociado a la entrega
            notificacion.dateRedirect = (auxPosEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c != null) ? 
											auxPosEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c : null;
		}  
    }
    
    /**
    * @creation date: 18/12/2017
    * @author: David Iglesias - Unisys
	* @description:	Método encargado de obtener las caracteristicas de las entregas según los id´s
	* @param:	entregasList				Id´s de entregas encontrados en las notificaciones
	* @return: 	Map<String, RQO_obj_posiciondeEntrega__c>			Mapa con el mapeo de Id de entrega y Pedido asociado a esta
	* @exception: 
	* @throws: 
	*/
    @TestVisible
	private static Map<String, RQO_obj_posiciondeEntrega__c> getPedidoByEntrega (List<String> entregasList) {
		
        /**
        * Constantes
        */
        final String METHOD = 'getPedidoByEntrega';
	
		/**
		 * Variables
		 */
		Map<String, RQO_obj_posiciondeEntrega__c> posicionByEntregaMap = new Map<String, RQO_obj_posiciondeEntrega__c> ();
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		for (RQO_obj_posiciondeEntrega__c item : [SELECT Id, RQO_fld_idRelacion__r.RQO_fld_idExterno__c, RQO_fld_posicionPedido__r.RQO_fld_idRelacion__c,
                                                  	RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_idExterno__c, 
                                                    RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c
													FROM RQO_obj_posiciondeEntrega__c
													WHERE RQO_fld_idRelacion__r.RQO_fld_idExterno__c IN :entregasList]) {
			if (item.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__c != null &&
                	String.isNotBlank(item.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_idExterno__c)){
				posicionByEntregaMap.put(item.RQO_fld_idRelacion__r.RQO_fld_idExterno__c, item);
			}
		}

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return posicionByEntregaMap;
		
	}
	
    
    	
    /**
    * @creation date: 22/02/2018
    * @author: Alvaro Alonso - Unisys
	* @description:	Método encargado de obtener los datos de pedido para las posiciones de pedido registradas en notificaciones
	* @param:	orderPositionList			Id´s externos de posición de pedido encontrados en las notificaciones
	* @return: 	Map<String, Asset>			Mapa con el mapeo de Id de posicion de y Pedido asociado a esta
	* @exception: 
	* @throws: 
	*/
    @TestVisible
	private static Map<String, Asset> getPedidoByOrderPosition (List<String> orderPositionList) {
		
        final String METHOD = 'getPedidoByOrderPosition';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
	
		Map<String, Asset> pedidoByOrderPositionMap = null;
        
        //Montamos un mapa con key el id externo de asset (identificador de posición de pedido) y value el RQO_fld_idExterno__c del pedido relacionado (número de pedido)
        for (Asset item : [SELECT Id, RQO_fld_idExterno__c, RQO_fld_idRelacion__c, RQO_fld_idRelacion__r.RQO_fld_idExterno__c, RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c
                           FROM Asset WHERE RQO_fld_idExterno__c IN :orderPositionList]) {
			if (item.RQO_fld_idRelacion__c != null && String.isNotBlank(item.RQO_fld_idRelacion__r.RQO_fld_idExterno__c)){
                if (pedidoByOrderPositionMap == null){pedidoByOrderPositionMap = new Map<String, Asset> ();}
				pedidoByOrderPositionMap.put(item.RQO_fld_idExterno__c, item);
			}
		}
        
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return pedidoByOrderPositionMap;
	}
     
    
	/**
    * @creation date: 22/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método encargado de obtener los datos de solicitud para las posiciones de soliciutd registradas en notificaciones
    * @param:	orderPositionList			Id´s externos de posición de pedido encontrados en las notificaciones
    * @return: 	Map<String, Asset>			Mapa con el mapeo de Id de posicion de y Pedido asociado a esta
    * @exception: 
    * @throws: 
    */
    @TestVisible
	private static Map<String, Asset> getPedidoByRequestPosition (List<String> requestPositionList) {
		
        final String METHOD = 'getPedidoByRequestPosition';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
	
		Map<String, Asset> pedidoByRequestPositionMap = null;
        
        //Montamos un mapa con key el id externo de asset (identificador de posición de pedido) y value el RQO_fld_idExterno__c del pedido relacionado (número de pedido)
        for (Asset item : [SELECT Id, RQO_fld_requestManagement__c, RQO_fld_requestManagement__r.name, RQO_fld_CUDeliveryDate__c
                           FROM Asset WHERE RQO_fld_requestManagement__r.name IN :requestPositionList]) {
			if (item.RQO_fld_requestManagement__c != null && String.isNotBlank(item.RQO_fld_requestManagement__r.name)){
                if (pedidoByRequestPositionMap == null){pedidoByRequestPositionMap = new Map<String, Asset> ();}
				pedidoByRequestPositionMap.put(item.RQO_fld_requestManagement__r.name, item);
			}
		}
        
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return pedidoByRequestPositionMap;
	}
    
    
    /**
    * @creation date: 18/12/2017
    * @author: David Iglesias - Unisys
	* @description:	Método encargado de obtener las traducciones de los grados según los id´s
	* @param:	gradosList					Id´s de grados encontrados en las notificaciones
	* @param:	gradosQp0List			    Id´s de grados qp0 encontrados en las notificaciones (Documentos)
	* @return: 	Map<String, String>			Mapa con el mapeo de Id de grado y su traduccion asociado a esta
	* @exception: 
	* @throws: 
	*/
    @TestVisible
	private static Map<String, String> getTranslationByGrade (List<String> gradosList, List<String> gradosQp0List) {
		
        /**
        * Constantes
        */
        final String METHOD = 'getTranslationByGrade';
	
		/**
		 * Variables
		 */
		Map<String, String> translationByGradeMap = new Map<String, String> ();
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        if (gradosList != null && !gradosList.isEmpty()){
            for (RQO_obj_translation__c item : [SELECT Id, RQO_fld_translation__c, RQO_fld_Grade__c 
                                                FROM RQO_obj_translation__c 
                                                WHERE RQO_fld_Grade__c IN :gradosList]) {
				translationByGradeMap.put(item.RQO_fld_Grade__c, item.RQO_fld_Translation__c);
			}
		}
		//Si la lista de grados qp0 tiene datos tenemos que ir a por la descripción del grado qp0
        if (gradosQp0List != null && !gradosQp0List.isEmpty()){
            for (RQO_obj_qp0Grade__c itemQp0 : [SELECT Id, RQO_fld_descripcionDelGrado__c 
                                                FROM RQO_obj_qp0Grade__c 
                                                WHERE Id IN :gradosQp0List]) {				
                translationByGradeMap.put(itemQp0.Id, itemQp0.RQO_fld_descripcionDelGrado__c);
            }
        }
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return translationByGradeMap;
		
	}
	
    /**
    * @creation date: 18/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Método invocable desde componente lightning que recupera la lista de categorías que se muestra en el filtro de notificaciones
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static Map<String, String> getNotificationsCategories(){
        final String METHOD = 'getTranslationByGrade';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Map<String, String> mapaNotificationsCategory = RQO_cls_GeneralUtilities.getNotificationObjectType();
            
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
		return mapaNotificationsCategory;
        
    }
    
    public class RQO_cls_NotificacionParent implements Comparable{
        public Date fechaCreacion{get;set;}
        public List<RQO_cls_NotificacionBean> listNotification{get;set;}
       
        /**
        * @creation date: 19/02/2018
        * @author: Alvaro Alonso Trinidad - Unisys
        * @description: implementación del método compareTo para ordenar la lista de objetos
        * @param: compareTo tipo Object
        * @return: Integer
        * @exception: 
        * @throws: 
        */
        public Integer compareTo(Object compareTo) {
            RQO_cls_NotificacionParent compareToEmp = (RQO_cls_NotificacionParent)compareTo;
            if (fechaCreacion == compareToEmp.fechaCreacion) return 0;
            if (fechaCreacion > compareToEmp.fechaCreacion) return -1;
            return 1;
        }
    }
    
}