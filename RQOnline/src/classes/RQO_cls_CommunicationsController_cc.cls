/**
*   @name: RQO_cls_CommunicationsController_cc
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Controlador de comunicaciones
*/
public with sharing class RQO_cls_CommunicationsController_cc {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_CommunicationsController_cc';
    
    /**
    *   @name: RQO_cls_CommunicationsController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de ContentVersion como resultado de seleccionar un FlatBanner
    *   desde un perfil de cliente y uno de usuario
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<ContentVersion> selectFlatBanner(string perfilCliente, string perfilUsuario) 
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectFlatBanner';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': perfilCliente ' + perfilCliente);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': perfilUsuario ' + perfilUsuario);
        
        List<ContentVersion> contents = 
            new RQO_cls_CommunicationsSelector().selectActiveWithAttachments(new Set<Id>{UserInfo.getUserId()},
                                                                             perfilCliente != null ? new List<string> {perfilCliente} : null, 
                                                                             perfilUsuario != null ? new List<string> {perfilUsuario} : null, 
                                                                             'Flat Banner',
                                                                             UserInfo.getLanguage() != null ? UserInfo.getLanguage().substring(0,2) : null);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ' ' + contents.size() + ' contenidos.');
        
        return contents;
    }
    
    /**
    *   @name: RQO_cls_CommunicationsController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de Contentversion como resultado de la selecci´pn de un Carousel a través de 
    *   un perfilCliente y un perfilUsuario
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<ContentVersion> selectCarousel(string perfilCliente, string perfilUsuario) 
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectCarousel';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': perfilCliente ' + perfilCliente);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': perfilUsuario ' + perfilUsuario);
        
        List<ContentVersion> contents = 
                new RQO_cls_CommunicationsSelector().selectActiveWithAttachments(new Set<Id>{UserInfo.getUserId()},
                                                                                 perfilCliente != null ? new List<string> {perfilCliente} : null, 
                                                                                 perfilUsuario != null ? new List<string> {perfilUsuario} : null, 
                                                                                 'Carousel',
                                                                            	 UserInfo.getLanguage() != null ? UserInfo.getLanguage().substring(0,2) : null);
                                                                                
        System.debug(CLASS_NAME + ' - ' + METHOD + ' ' + contents.size() + ' contenidos.');
        
        return contents;
    }
    
    /**
    *   @name: RQO_cls_CommunicationsController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna un objeto communication cuando le pass el id de comunicación correspondiente por parámetro
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static RQO_obj_communication__c selectCommunicationById(ID commId) 
    {
        /**
        * Constantes
        */
        final String METHOD = 'selectCommunicationById';
        
        List<RQO_obj_communication__c> comms = new RQO_cls_CommunicationsSelector().selectByIdWithTemplate(new Set<ID> { commId });
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ' ' + comms.size() + ' resultados.');
        
        if (comms.size() > 0) {
            return comms[0];
        }
        else
            return null;
    }
    
    /**
    *   @name: RQO_cls_CommunicationsController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de Contentversion basado en la selección de un adjunto cuando le pasas por
    *   parámetro una Communication Id
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<ContentVersion> selectAttachment(ID commId) 
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectAttachment';
        
        List<ContentVersion> contents = new RQO_cls_CommunicationsSelector().selectAttachments(commId);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ' ' + contents.size() + ' contenidos.');
        
        return contents;
    }
    
    /**
    *   @name: RQO_cls_CommunicationsController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de Contentversion basado en la selección de una imagen cuando le pasas por
    *   parámetro una Communication Id
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<ContentVersion> selectImages(ID commId) 
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectImages';
        
        List<ContentVersion> images = new List<ContentVersion>();
        List<ContentVersion> contents = new RQO_cls_CommunicationsSelector().selectAttachments(commId);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ' ' + contents.size() + ' contenidos.');
        
        for (ContentVersion content : contents) {
            if (content.FileType == 'JPG' || content.FileType == 'PNG' || content.FileType == 'GIF')
                images.add(content);
        }
        return images; 
    }
    
    /**
    *   @name: RQO_cls_CommunicationsController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de Contentversion basado en la selección de documentos cuando le pasas por
    *   parámetro una Communication Id
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<ContentVersion> selectDocuments(ID commId) 
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectDocuments';
        
        List<ContentVersion> docs = new List<ContentVersion>();
        List<ContentVersion> contents = new RQO_cls_CommunicationsSelector().selectAttachments(commId);
        
        for (ContentVersion content : contents) {
            if (content.FileType == 'PDF')
                docs.add(content);
        }
        return docs; 
    }
}