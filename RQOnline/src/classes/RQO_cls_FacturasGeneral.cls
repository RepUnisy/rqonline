/**
 *	@name: RQO_cls_CatalogueWS
 *	@version: 1.0
 *	@creation date: 13/11/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase Apex con los métodos necesarios para extraer de SAP los datos de facturas que se puedan necesitar en la aplicación
 *  @testClass: RQO_cls_FacturasGeneral_test
*/
public class RQO_cls_FacturasGeneral {

    // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_FacturasGeneral';
    
    // ***** METODOS PUBLICOS ***** //

	/**
	* @creation date: 13/11/2017
	* @author: David Iglesias - Unisys
	* @description:	 
	* @param:	solicitantesList	
    * @param:	fechaInicio			Fecha de inicio de rango de facturas
    * @param:	fechaFin			Fecha de fin de rango de facturas
	* @return: 	RQO_cls_FacturaBean	
	* @exception: 
	* @throws: 
	*/
    public static List <RQO_cls_FacturaBean> getFacturasListBySearch(String iFactura, String iSolicitante, String ivClaseFact, List<String> ivCodMaterial, String ivDestFacturacion, 
																		String ivDestMercancias, String ivDocfactContable, String ivEntrega, String fechaInicio, String fechaFin, 
																		String fechaVencimientoInicio, String fechaVencimientoFin, Integer ivNumReg, String ivStatusFact, String ivTipoFact, 
                                                                     	String ivNumPedido){
    
        /**
        * Constantes
        */
        final String METHOD = 'getFacturasListBySearch';
        
        /**
		 * Variables
		 */
        RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE zqoFacturas = null;
        RQO_cls_SAPServicesWS.ZqoCargaFacturasResponse_element returnCallout = null;
		RQO_cls_FacturaBean.RQO_cls_DetalleFacturaWrapper detalleFactura = null;
		Boolean error = false;
		RQO_cls_FacturaBean factura = null;
		RQO_obj_logProcesos__c objLog = null;
		RQO_cls_GeneralUtilities genUtilities = null;
		Set <String> skuGradeSet = null;
		Set <String> destMercanciasSet = null;
		List <RQO_cls_FacturaBean> returnDataList = new List <RQO_cls_FacturaBean> ();
		List<RQO_obj_logProcesos__c> listProcessLog = null;
		Map <String, String> skuGradeDescMap = null;
		Map <String, String> idAccountNameMap = null;
		Map <String, RQO_cls_FacturaBean> facturasByIdenMap = null;
		Map <String, String> tipoFacturaMap = null;
                
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        zqoFacturas = new RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE();
        // Comprobación de datos obligatorios para la petición
        if(iSolicitante != null && fechaInicio != null && fechaFin != null) {
			
			try{
				
                // Parseo de datos
                RQO_cls_SAPServicesWS.ZqoTtSolicitante solicitantes = null;
				if (String.isNotBlank(iSolicitante)){
					solicitantes = new RQO_cls_SAPServicesWS.ZqoTtSolicitante ();
					solicitantes.item = new List<String>{iSolicitante};
				}
				RQO_cls_SAPServicesWS.ZqoTtFactura facturas = null;
				if (String.isNotBlank(iFactura)){
					facturas = new RQO_cls_SAPServicesWS.ZqoTtFactura ();
					facturas.item =  new List<String>{iFactura};
				}
				RQO_cls_SAPServicesWS.ZqoTtCodMaterial materiales = null;
				if (ivCodMaterial != null && !ivCodMaterial.isEmpty()){
					materiales = new RQO_cls_SAPServicesWS.ZqoTtCodMaterial();
					materiales.item =  ivCodMaterial;
				}
                //Llamamos al método genérico que monta la requets y realiza el callout
                returnCallout = zqoFacturas.ZqoCargaFacturas(facturas, solicitantes, ivClaseFact, materiales, ivDestFacturacion, 
																ivDestMercancias, ivDocfactContable, ivEntrega, fechaFin, fechaInicio,
																fechaVencimientoFin, fechaVencimientoInicio, ivNumReg, ivStatusFact, ivTipoFact, ivNumPedido, null);
				System.debug('Return Callout: ' + returnCallout);
                if(returnCallout == null) {
					
                    //Si no tengo datos de vuelta generamos el log correspondiente
					objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_SAP, RQO_cls_Constantes.SERVICE_CARGA_FACTURAS);
					RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, null);
					error = true;
					
                } else if (returnCallout.EtReturn != null && returnCallout.EtReturn.item != null && !returnCallout.EtReturn.item.isEmpty()) {
					for (RQO_cls_SAPServicesWS.Bapiret2 item : returnCallout.EtReturn.item) {
						if (String.isNotBlank(item.Type_x) && item.Type_x.equalsIgnoreCase(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR)) {
							error = true;
							
							// Comprobamos si el error es distinto a no encontrar resultados en la busqueda
							if (!item.Number_x.equalsIgnoreCase(RQO_cls_Constantes.FAC_COD_SEARCH_EMPTY)) {
								// Otro error en SAP
								factura = new RQO_cls_FacturaBean ();
								factura.error = true;
								returnDataList.add(factura);
								objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_SAP, RQO_cls_Constantes.SERVICE_CARGA_FACTURAS);
								RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, item.Message+' - '+item.Number_x);
							}
							
							break;
						}
					}
				}		
				
				if (!error) {
					
					facturasByIdenMap = new Map <String, RQO_cls_FacturaBean> ();
					skuGradeSet = new Set <String> ();
					destMercanciasSet = new Set <String> ();
					
					// Obtención del mapeo de codigos de estado de factura
					genUtilities = new RQO_cls_GeneralUtilities();
					tipoFacturaMap = genUtilities.getTipoFacturaLocale();
					
					// Obtencion de datos para consultas SOQL por campos
					for (RQO_cls_SAPServicesWS.ZqoStFacturaso item : returnCallout.EtFacturas.item) {
						if (String.isNotBlank(item.Codmaterial)) {
							skuGradeSet.add(item.Codmaterial.substring(0, item.Codmaterial.length()-2));
						}
						if (String.isNotBlank(item.DestMercancias)) {
							destMercanciasSet.add(item.DestMercancias);
						}
					}
					
					// Lanzamiento de consultas para mapeo de datos
					if (!skuGradeSet.isEmpty()) {
						skuGradeDescMap = new Map <String, String> ();
						for (RQO_obj_qp0Grade__c item : [SELECT RQO_fld_sku__c, RQO_fld_descripcionDelGrado__c FROM RQO_obj_qp0Grade__c WHERE RQO_fld_sku__c IN :skuGradeSet]) {
							skuGradeDescMap.put(item.RQO_fld_sku__c, item.RQO_fld_descripcionDelGrado__c);
						}
					}
					
					if (!destMercanciasSet.isEmpty()) {
						idAccountNameMap = new Map <String, String> ();
						for (Account item : [SELECT RQO_fld_idExterno__c, Name FROM Account WHERE RQO_fld_idExterno__c IN :destMercanciasSet]) {
							idAccountNameMap.put(item.RQO_fld_idExterno__c, item.Name);
						}
					}
					
					// Iteracion de la respuesta obtenida en el servicio
					for (RQO_cls_SAPServicesWS.ZqoStFacturaso item : returnCallout.EtFacturas.item) {
						// Ordenacion de datos, se aunan los datos generales de las facturas y se individualizan los especificos
						if (facturasByIdenMap.get(item.Factura) == null) {
							factura = new RQO_cls_FacturaBean ();
							factura.detallesList = new List <RQO_cls_FacturaBean.RQO_cls_DetalleFacturaWrapper> ();
							
							// Datos genericos de la factura
							factura.numFactura = item.Factura;
							
							// Casting de fechas del servicio (String) a Date
							if (String.isNotBlank(item.Fecfactura)) {
								factura.fechaEmision = RQO_cls_FormatUtils.obtenerFechaYYYYMMDD(item.Fecfactura.replace(RQO_cls_Constantes.HYPHEN,RQO_cls_Constantes.EMPTY));
							}
							if (String.isNotBlank(item.FechaVenci)) {
								factura.fechaVencimiento = RQO_cls_FormatUtils.obtenerFechaYYYYMMDD(item.FechaVenci.replace(RQO_cls_Constantes.HYPHEN,RQO_cls_Constantes.EMPTY));
							}
							
							factura.importe = item.ImporteTotal;
                            factura.importeFormateado = (String.isNotBlank(item.ImporteTotal)) ? RQO_cls_FormatUtils.formatDecimal(Decimal.valueOf(item.ImporteTotal)) : null;
							factura.tipoMoneda = item.Moneda;
							
							if (RQO_cls_Constantes.FAC_COD_COMPENSADA.equalsIgnoreCase(item.StatusFact)) {
								factura.estado = Label.RQO_lbl_estadoFacturaCompensado;
							} else if (RQO_cls_Constantes.FAC_COD_PENDIENTE.equalsIgnoreCase(item.StatusFact)) {
								factura.estado = Label.RQO_lbl_estadoFacturaPendiente;
							} else if (RQO_cls_Constantes.FAC_COD_VENCIDA.equalsIgnoreCase(item.StatusFact)) {
								factura.estado = Label.RQO_lbl_estadoFacturaVencida ;
							}
							
                            factura.idArchivado = item.IdArchivado;
							factura.error = false;
							
							// Mapeo del tipo de factura
							factura.tipo = tipoFacturaMap.get(item.Tipofact);
							
						} else {
							factura = facturasByIdenMap.get(item.Factura);
						}
						
						detalleFactura = new RQO_cls_FacturaBean.RQO_cls_DetalleFacturaWrapper ();
						detalleFactura.numRefCliente = item.PedidoRefCliente;
						detalleFactura.numPedido = item.Pedido;
						detalleFactura.numAlbaran = item.Entrega;
						detalleFactura.cantidad = (item.Cantidad != null) ? item.Cantidad.trim() : null;
						detalleFactura.tipoCantidad = item.Umctd;
						detalleFactura.precioTonelada = (item.PrecioUnitario != null) ? item.PrecioUnitario.trim() : null;
                        detalleFactura.precioToneladaFormateado = (String.isNotBlank(item.PrecioUnitario)) ? RQO_cls_FormatUtils.formatDecimal(Decimal.valueOf(item.PrecioUnitario.trim())) : null;
						detalleFactura.importe = item.ImportNetoPos;
                        detalleFactura.importeNetoFormateado = (String.isNotBlank(item.ImportNetoPos)) ? RQO_cls_FormatUtils.formatDecimal(Decimal.valueOf(item.ImportNetoPos.trim())) : null;
						
						// Obtención del grado a mostrar
						if (String.isNotBlank(item.Codmaterial) && skuGradeDescMap != null) {
							detalleFactura.grado = skuGradeDescMap.get(item.Codmaterial.substring(0, item.Codmaterial.length()-2));
							// Si no existe el grado en SF, se setea el SKU
							if (detalleFactura.grado == null) {
								detalleFactura.grado = item.Codmaterial.substring(0, item.Codmaterial.length()-2);
							}
						}
						
						// Obtención del destino a mostrar
						if (String.isNotBlank(item.DestMercancias) && idAccountNameMap != null) {
							detalleFactura.destino = idAccountNameMap.get(item.DestMercancias);
						}

						factura.detallesList.add(detalleFactura);
						facturasByIdenMap.put(factura.numFactura, factura);
						
					}
					
					returnDataList = facturasByIdenMap.values();
					
				}				
				
            } catch(System.CalloutException ex) {
                System.debug('CallOut Exception: ' + ex.getMessage());				
                //Si da una excepción de tipo callout genero el log correspondiente
				objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_SAP, RQO_cls_Constantes.SERVICE_CARGA_FACTURAS);
                RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, ex.getTypeName(), ex.getMessage());
			} catch(Exception ex) {
                System.debug('Exception: ' + ex.getMessage());
                //Si da una excepción genérica genero el log correspondiente
				objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_SAP, RQO_cls_Constantes.SERVICE_CARGA_FACTURAS);
				RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage());
            }            
        } else {
			objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_SAP, RQO_cls_Constantes.SERVICE_CARGA_FACTURAS);
			RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, 'datos obligatorios no informados: sol: '+iSolicitante +' ini: ' +fechaInicio+' fin: '+fechaFin);	
		}
		
		// Inserción de log si existe error
		if (objLog != null) {
			insert objLog;
			factura = new RQO_cls_FacturaBean ();
			factura.error = true;
			returnDataList.add(factura);	
		}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN'); 
		system.debug('Retorno --> '+returnDataList);
        
		return returnDataList;
		
    }
    
}