/**
 *	@name: RQO_cls_WSInConfirmaPedido
 *	@version: 1.0
 *	@creation date: 18/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase que se encarga de recibir una peticion de SAP (Pedidos) y procesarla.
 *	@description: Se notifica al BUS de forma sincrona que la peticion ha llegado correctamente y se procesa de forma asincrona.
 */
@RestResource(urlMapping='/RQO/ConfirmaPedidoCreado/*') 
global class RQO_cls_WSInConfirmaPedido {
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_WSInConfirmaPedido';
	
	@HttpPost
	global static RQO_cls_WSCalloutResponseBean recibirConfirmacionPedidoCreado() {

		/**
		 * Constantes
		 */
		final String METHOD = 'recibirConfirmacionPedidoCreado';

		/**
		 * Variables
		 */
		RQO_cls_WSCalloutResponseBean respuestaSincrona;
		Datetime now = System.now();

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

		// Inicializar la respuesta sincrona como si fuera correcta
		respuestaSincrona = new RQO_cls_WSCalloutResponseBean();
		respuestaSincrona.estado = RQO_cls_Constantes.PETICION_ESTADO_OK;

		// Procesar la peticion de forma asincrona
		System.debug(CLASS_NAME + ' - ' + METHOD + ': Procesando la informacion de forma asincrona');
		System.enqueueJob(new RQO_cls_WSInConfirmaPedidoProceso(now, RestContext.request.headers, RestContext.request.requestBody.toString()));

        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');

		return respuestaSincrona;
	}
}