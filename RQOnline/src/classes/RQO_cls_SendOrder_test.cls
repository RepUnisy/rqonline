/**
*	@name: RQO_cls_SendOrder_test
*	@version: 1.0
*	@creation date: 30/10/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar el servicio de envío de solicitudes de pedido a SAP
*/
@IsTest
public class RQO_cls_SendOrder_test {
    
    /**
    * @creation date: 30/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparte entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserSendOrder@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
            //   RQO_obj_requestManagement__c reqManagement = RQO_cls_TestDataFactory.createRequestManagement(RQO_cls_Constantes.REQ_STATUS_INPROGRESS, false, 'PS');
               RQO_cls_TestDataFactory.createRequestManagement(1);
                List<RQO_obj_requestManagement__c> listReqManagement = [SELECT id FROM RQO_obj_requestManagement__c LIMIT 1];
            	listReqManagement[0].RQO_fld_requestStatus__c = RQO_cls_Constantes.REQ_STATUS_INPROGRESS;
            	listReqManagement[0].RQO_fld_sentToQp0__c = false;
                listReqManagement[0].RQO_fld_type__c = 'PS';
                update listReqManagement[0];    
            // List<RQO_obj_requestManagement__c> reqListManagement = [Select createdBy.UserName from RQO_obj_requestManagement__c where id = : reqManagement.Id];
            
            // List<Account> listaCuentas = RQO_cls_TestDataFactory.createAccounts('cuentaSendOrder', RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ACCOUNT_QUIMICA, 'acExtId', 'testLN', 'ES', '914443322');
            RecordType rtActual = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ACCOUNT_QUIMICA LIMIT 1];
            List<Account> listaCuentas = RQO_cls_TestDataFactory.createAccount(1);
            listaCuentas[0].Name = 'cuentaSendOrder';
			listaCuentas[0].RecordtypeId = rtActual.Id;
            listaCuentas[0].RQO_fld_idExterno__c = 'acExtId';
            listaCuentas[0].RQO_fld_calleNumero__c = 'StreetNumber';
            listaCuentas[0].RQO_fld_poblacion__c = 'City';
            listaCuentas[0].RQO_fld_pais__c = 'ES';
            listaCuentas[0].Phone = '914443322';
            update listaCuentas[0];
            
            // Contact contactData = RQO_cls_TestDataFactory.createContact('contactSendOrder', 'contactLastName', listaCuentas[0].Id);
            RQO_cls_TestDataFactory.createContact(1);
            // Id skuDocumentum = RQO_cls_TestDataFactory.createQP0Grade('gradeQp0SendOrder', 'TestSendOrder', null, 'Qp0 grade test', null, null, null, null);
            // RQO_obj_grade__c grade = RQO_cls_TestDataFactory.createRqoGrade('gradeSendOrder', skuDocumentum, false, '', false, false, null, true, false);
            RQO_cls_TestDataFactory.createGrade(1);
            // List<Asset> assetData = RQO_cls_TestDataFactory.createAssets('assetSendOrder', listaCuentas[0].Id, contactData.Id, grade.Id, RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ASSET_REQUEST_POSITION);
            
            RecordType rtActualsec = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ASSET_REQUEST_POSITION];
            List<Asset> asset = RQO_cls_TestDataFactory.createAsset(200);
            asset[0].RecordTypeId = rtActualsec.Id;
            asset[0].Name = 'assetSendOrder';
            asset[0].RecordTypeId = rtActualsec.Id;
			update asset[0];
        }
    }
    
    /**
    * @creation date: 30/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para testear el envío de posiciones de solicitud
    * @exception: 
    * @throws: 
    */
    
    static testMethod void sendOrder(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSendOrder@testorg.com');
        System.runAs(u){
            //   List<RQO_obj_requestManagement__c> reqManagement = RQO_cls_TestDataFactory.getCreateRequestManagement('sfTestUserSendOrder@testorg.com', 1);
            
     
            List<RQO_obj_requestManagement__c> reqManagement = [Select Id from RQO_obj_requestManagement__c where CreatedBy.UserName = 'sfTestUserSendOrder@testorg.com'];
            system.debug('*************reqManagement: ' + reqManagement);
            
            /*
            List<RQO_obj_requestManagement__c> reqManagement = [SELECT id FROM RQO_obj_requestManagement__c LIMIT 1];
            reqManagement[0].RQO_fld_requestStatus__c = RQO_cls_Constantes.REQ_STATUS_SENT;
            reqManagement[0].CreatedBy.UserName = 'sfTestUserSendOrder@testorg.com';
            update reqManagement[0];
*/
            //  List<Asset> listaActivos = RQO_cls_TestDataFactory.getCreatedAssets('assetSendOrder');
            //  
            List<Asset> listaActivos = [SELECT Id, name, AccountId, ContactId, RQO_fld_grade__c, RecordTypeId, RQO_fld_positionStatus__c FROM Asset LIMIT 1];
            RecordType posicionSolicitud = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ASSET_REQUEST_POSITION];
			listaActivos[0].RecordTypeId = posicionSolicitud.Id;
            update listaActivos[0];
            
            List<Id> listIdPosiciones = new List<Id>();
            for(Asset act : listaActivos)
            {
                listIdPosiciones.add(act.Id);
            }
            test.startTest();
            //Petición con retorno ok del servicio
            //   	List<Asset> listaAssets = RQO_cls_TestDataFactory.getCreatedAssets('assetSendOrder');
            //	Asset listaAssets = listaAssets[0];
            Asset listaAssets = listaActivos[0];
            Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_SEND_ORDER, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
            RQO_cls_SendOrder sendOrderClass = new RQO_cls_SendOrder();
            system.debug('******reqManagement: ' + reqManagement);
            system.debug('******listIdPosiciones: ' + listIdPosiciones);
            RQO_cls_SendOrder.SendOrderCustomResponse response = sendOrderClass.sendPositions(reqManagement[0].Id, listIdPosiciones, RQO_cls_Constantes.REQ_WS_PROCESSTYPE_CREATE);
            System.assertEquals(response.errorCode, RQO_cls_Constantes.COD_EXEC_OK);
           // System.assertEquals(response.errorCode, 'NOK');
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 31/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para testear la función sendOrder del controlador RQO_cls_GestionPedido_cc
    * @exception: 
    * @throws: 
    */
    static testMethod void sendOrderController(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSendOrder@testorg.com');
        System.runAs(u){
            //      List<RQO_obj_requestManagement__c> reqManagement = RQO_cls_TestDataFactory.getCreateRequestManagement('sfTestUserSendOrder@testorg.com', 1);
            List<RQO_obj_requestManagement__c> reqManagement = [SELECT id FROM RQO_obj_requestManagement__c LIMIT 1];
            test.startTest();
            //Testeamos el controlador Apex
            RQO_cls_GestionPedido_cc.sentRequest(reqManagement[0].Id);
            RQO_obj_requestManagement__c updatedReq = [Select RQO_fld_requestStatus__c from RQO_obj_requestManagement__c where Id = : reqManagement[0].Id];
            System.assertEquals(RQO_cls_Constantes.REQ_STATUS_INPROGRESS, updatedReq.RQO_fld_requestStatus__c);
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 30/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para testear el envío de posiciones de solicitud a cancelar desde el controlador apex RQO_cls_GestionPedido_cc
    * @exception: 
    * @throws: 
    */
    static testMethod void cancelOrderController(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSendOrder@testorg.com');
        System.runAs(u){
            //    List<RQO_obj_requestManagement__c> reqManagement = RQO_cls_TestDataFactory.getCreateRequestManagement('sfTestUserSendOrder@testorg.com', 1);
            List<RQO_obj_requestManagement__c> reqManagement = [SELECT id FROM RQO_obj_requestManagement__c LIMIT 1];
            //   List<Asset> listaActivos = RQO_cls_TestDataFactory.getCreatedAssets('assetSendOrder');
            List<Asset> listaActivos = [SELECT Id, name, AccountId, ContactId, RQO_fld_grade__c, RecordTypeId, RQO_fld_positionStatus__c FROM Asset LIMIT 1];
            
            List<Id> listIdPosiciones = new List<Id>();
            for(Asset act : listaActivos)
            {
                listIdPosiciones.add(act.Id);
            }
            test.startTest();
            //Petición con retorno ok del servicio
            Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_SEND_ORDER, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
            String response = RQO_cls_GestionPedido_cc.sendCancelledPositions(listIdPosiciones);
            RQO_cls_SendOrder.SendOrderCustomResponse returnData = (RQO_cls_SendOrder.SendOrderCustomResponse)JSON.deserialize(response, RQO_cls_SendOrder.SendOrderCustomResponse.class);
        //**    System.assertEquals(RQO_cls_Constantes.SOAP_COD_NO_VERIFIED, returnData.errorCode);
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_NO_VERIFIED, 'NV');
            test.stopTest();
        }
    }
	
    /**
    * @creation date: 06/03/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para testear el envío de posiciones de solicitud a cancelar desde el controlador apex RQO_cls_GestionPedido_cc
    * cuando se produce un error controlado de posición ya cancelada
    * @exception: 
    * @throws: 
    */
    static testMethod void cancelOrderControledError(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSendOrder@testorg.com');
        System.runAs(u){
            List<Id> listIdPosiciones = new List<Id>();
            for(Asset act : [SELECT Id, name, AccountId, ContactId, RQO_fld_grade__c, RecordTypeId, RQO_fld_positionStatus__c FROM Asset LIMIT 1]){
                listIdPosiciones.add(act.Id);
            }
            test.startTest();
                //Petición con retorno nok del servicio
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_SEND_ORDER, RQO_cls_Constantes.MOCK_EXEC_NOK, 
                                                            RQO_cls_Constantes.REQ_WS_RESPONSE_INV_NO_NUMBER, listIdPosiciones[0], null, null, null));
                String response = RQO_cls_GestionPedido_cc.sendCancelledPositions(listIdPosiciones);
                RQO_cls_SendOrder.SendOrderCustomResponse returnData = (RQO_cls_SendOrder.SendOrderCustomResponse)JSON.deserialize(response, 
                                                                                                                    RQO_cls_SendOrder.SendOrderCustomResponse.class);
            test.stopTest();
            
            System.assertNotEquals(null, returnData);
        }
    }
}