/**
*	@name: RQO_cls_RegisterEvent_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: Alfonso Constan López - Unisys
*	@description: Clase que testea RQO_cls_CommunicationsController
*/
@IsTest
public class RQO_cls_CommunicationsController_test {
    
    /**
    *	@name: RQO_cls_CommunicationsController_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author: Alfonso Constan López - Unisys
    *	@description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    */
    @testSetup static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
    }
    
    /**
    *	@name: RQO_cls_CommunicationsController_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author: Alfonso Constan López - Unisys
    *	@description: Método que testea la casuística en la que se selecciona un FlatBanner y no se devuelve
    */
	@isTest
    public static void WhereSelectFlatBannerDoesNotReturn() {
        User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            List<ContentVersion> contents = 
                RQO_cls_CommunicationsController_cc.selectFlatBanner('WrongClient', 'WrongUser');
            
            System.assertEquals(0,
                                contents.size(),
                                'Se devuelven datos no esperados.');
            }
 	}
 	
    /**
    *	@name: RQO_cls_CommunicationsController_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author:Alfonso Constan López - Unisys
    *	@description: Método que testea la casuística en la que se selecciona un FlatBanner se devuelve bien
    */
 	@isTest
    public static void WhereSelectFlatBannerReturnsOk() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            test.startTest();
            //User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAs@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
            List<ContentVersion> contents;
            //System.runAs(userRunAs){
                List<RQO_obj_communication__c> comms = RQO_cls_TestDataFactory.createCoommunications();
                
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 'es',
                                                           'es_ES', 'Europe/Berlin', contact[0].Id);
                
                for (RQO_obj_communication__c comm : comms)
                {
                    RQO_obj_communicationAccount__c commAccount = new RQO_obj_communicationAccount__c(RQO_fld_account__c = contact[0].AccountId,
                                                                                                      RQO_fld_communication__c = comm.Id);
                    insert commAccount;
                }
                
                contents = RQO_cls_CommunicationsController_cc.selectFlatBanner(null, null);
            //}
                
            test.stopTest();
            
            System.assertEquals(0,
                                contents.size(),
                                'No de devuelven los datos esperados.');
            }
 	}
 	
    /**
    *	@name: RQO_cls_CommunicationsController_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author: Alfonso Constan López - Unisys
    *	@description: Método que testea la casuística en la que se selecciona un Carousel y no se devuelve nada
    */
 	@isTest
    public static void WhereSelectCarouselDoesNotReturn() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            List<ContentVersion> contents = 
                RQO_cls_CommunicationsController_cc.selectCarousel('WrongClient', 'WrongUser');
            
            System.assertEquals(0,
                                contents.size(),
                                'Se devuelven datos no esperados.');
        }
 	}
 	
    /**
    *	@name: RQO_cls_CommunicationsController_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author: Alfonso Constan López - Unisys
    *	@description: Método que testea la casuística en la que se selecciona una comunicación por id y no se retorna
    */
 	@isTest
    public static void WhereSelectCommunicationByIdDoesNotReturn() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Id commId = fflib_IDGenerator.generate(RQO_obj_communication__c.SObjectType);
            Id userId = fflib_IDGenerator.generate(User.SObjectType);
            Id contactId = fflib_IDGenerator.generate(Contact.SObjectType);
            Id accountId = fflib_IDGenerator.generate(Account.SObjectType);
            Id templateId = fflib_IDGenerator.generate(EmailTemplate.SObjectType);
            Id responseId = fflib_IDGenerator.generate(RQO_obj_questionResponse__c.SObjectType);
            
            new RQO_cls_CommunicationsSelector().selectActiveMultimedia(new Set<Id>{UserId}, null, null);
            new RQO_cls_ContactSelector().selectByAccountId(new Set<Id>{contactId});
            new RQO_cls_emailTemplateSelector().selectById(new Set<Id>{templateId});
            new RQO_cls_QuestionResponseSelector().selectById(new Set<Id>{responseId});
            new RQO_cls_SurveySelector().selectByAccountId(new Set<Id>{accountId});
            new RQO_cls_UserSelector().selectByUsername(String.valueOf(userId));
            
            RQO_obj_communication__c comm = 
                RQO_cls_CommunicationsController_cc.selectCommunicationById(commId);
            
            System.assertEquals(null,
                                comm,
                                'Se devuelven datos no esperados.');
        }
	}
 	
    /**
    *	@name: RQO_cls_CommunicationsController_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author: Alfonso Constan López - Unisys
    *	@description: Método que testea la casuística en la que se selecciona un adjunto y no se retorna
    */
 	@isTest
    public static void WhereSelectAttachmentDoesReturn() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            List<RQO_obj_communication__c> comms = RQO_cls_TestDataFactory.createCoommunications();
            
            List<ContentVersion> contents = 
                RQO_cls_CommunicationsController_cc.selectAttachment(comms[0].Id);
            
            System.assertEquals(0,
                                contents.size(),
                                'Se devuelven datos no esperados.');
		}
 	}
 	
    /**
    *	@name: RQO_cls_CommunicationsController_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author: Alfonso Constan López - Unisys
    *	@description: Método que testea la casuística en la que se selecciona una imagen y no se retorna
    */
	@isTest
    public static void WhereSelectImagesDoesNotReturn() {
        User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Id commId = fflib_IDGenerator.generate(RQO_obj_communication__c.SObjectType);
            
            List<ContentVersion> contents = 
                RQO_cls_CommunicationsController_cc.selectImages(commId);
            
            System.assertEquals(0,
                                contents.size(),
                                'Se devuelven datos no esperados.');
		}
 	}
 	
    /**
    *	@name: RQO_cls_CommunicationsController_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author: Alfonso Constan López - Unisys
    *	@description: Método que testea la casuística en la que se selecciona un documento y no se retorna
    */
	@isTest
    public static void WhereSelectDocumentsDoesNotReturn() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Id commId = fflib_IDGenerator.generate(RQO_obj_communication__c.SObjectType);
            
            List<ContentVersion> contents = 
                RQO_cls_CommunicationsController_cc.selectDocuments(commId);
            
            System.assertEquals(0,
                                contents.size(),
                                'Se devuelven datos no esperados.');
		}
 	}
 	
}