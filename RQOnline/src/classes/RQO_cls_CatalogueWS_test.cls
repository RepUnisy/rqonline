/**
    *	@name: RQO_cls_CatalogueWS_test
    *	@version: 1.0
    *	@creation date: 26/10/2017
    *	@author: Alvaro Alonso - Unisys
    *	@description: Clase de test para probar los servicios de asociados al catálogo: Obtención de grados asociados a acuerdos en SAP
    */
@IsTest
public class RQO_cls_CatalogueWS_test {
    
        /**
    * @creation date: 26/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparte entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
            RecordType rtActual = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ACCOUNT_QUIMICA LIMIT 1];
            List<Account> listaCuentas = RQO_cls_TestDataFactory.createAccount(1);
            listaCuentas[0].Name = 'cuentaAgreement';
            listaCuentas[0].RecordtypeId = rtActual.Id;
            listaCuentas[0].RQO_fld_idExterno__c = 'acExtId';
            listaCuentas[0].RQO_fld_calleNumero__c = 'StreetNumber';
            listaCuentas[0].RQO_fld_poblacion__c = 'City';
            listaCuentas[0].RQO_fld_pais__c = 'ES';
            listaCuentas[0].Phone = '914443322';
            update listaCuentas[0];
            
            List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
            contact[0].FirstName = 'contactAgreement';
            contact[0].LastName = 'contactLastName';
            update contact[0];
            
            RecordType rtActualsec = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ASSET_REQUEST_POSITION];
            List<Asset> asset = RQO_cls_TestDataFactory.createAsset(200);
            asset[0].RecordTypeId = rtActualsec.Id;
            asset[0].Name = 'assetSendOrder';
            update asset[0];
            List<RQO_obj_grade__c> listGrade = RQO_cls_TestDataFactory.createGrade(1);
    
            List<RQO_obj_myCatalogue__c> myCatalogue = RQO_cls_TestDataFactory.createMyCatalogue(1);
            myCatalogue[0].RQO_fld_grade__c = listGrade[0].Id;
            myCatalogue[0].RQO_fld_contact__c = contact[0].Id;
            update myCatalogue[0];
            system.debug('*testsetup****myCatalogue: ' + myCatalogue);
		}
    }
    
 	/**
    * @creation date: 26/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la obtención de "Mi catalogo" tanto la parte almacenada en Salesforce como los acuerdos recuperados de SAP
    * @exception: 
    * @throws: 
    */
	static testMethod void getMiCatalogoAcuerdos(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            //Petición con retorno ok del servicio
            Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_AGREEMENT, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
            List<Account> listaCuentas = [SELECT Id FROM Account LIMIT 1];
            List<Contact> contactData = [SELECT Id FROM Contact LIMIT 1];
            RQO_cls_CatalogueWS catalogo = new RQO_cls_CatalogueWS();
            RQO_cls_CatalogueWS.CatalogueCustomResponse response = catalogo.refreshMyCatalogue(listaCuentas[0].Id, contactData[0].Id);
            test.stopTest();
            System.assertEquals('OK', response.errorCode);
        }
    }  
	
    /**
    * @creation date: 22/02/2018
    * @author: Juan Elías - Unisys
    * @description:	Método test para probar la obtención de "Mi catalogo" tanto la parte almacenada en Salesforce como los acuerdos recuperados de SAP
    * @exception: 
    * @throws: 
    */
    static testMethod void getMiCatalogoAcuerdosException(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            //Petición con retorno fallido del servicio
            List<Account> listaCuentas = [SELECT Id FROM Account LIMIT 1];
            List<Contact> contactData = [SELECT Id FROM Contact LIMIT 1];
            RQO_cls_CatalogueWS catalogo = new RQO_cls_CatalogueWS();
            RQO_cls_CatalogueWS.CatalogueCustomResponse response = catalogo.refreshMyCatalogue(listaCuentas[0].Id, contactData[0].Id);
            test.stopTest();
            System.assertEquals('NOK', response.errorCode);
        }
    }
    /**
    * @creation date: 26/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la obtención de "Mi catalogo" cuando no tengo datos de cuenta y contacto
    * @exception: 
    * @throws: 
    */
    static testMethod void getMiCatalogoAcuerdosNoData(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            //Petición sin datos
            RQO_cls_CatalogueWS obj = new RQO_cls_CatalogueWS();
            obj.refreshMyCatalogue(null, null);
            List<RQO_obj_myCatalogue__c> listMiCatalogo = [Select Id from RQO_obj_myCatalogue__c where RQO_fld_grade__r.Name = 'gradeAgreement2'];
            System.assertEquals(listMiCatalogo, new List<RQO_obj_myCatalogue__c>());
            test.stopTest();
        }
    }
}