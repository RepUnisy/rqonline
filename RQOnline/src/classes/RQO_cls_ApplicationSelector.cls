/**
*   @name: RQO_cls_ApplicationSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la selección de aplicaciones
*/
public abstract class RQO_cls_ApplicationSelector extends fflib_SObjectSelector 
{

    /**
    *   @name: RQO_cls_ApplicationSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Constructor de la clase
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public RQO_cls_ApplicationSelector() 
    {         
        this(false);     
    }     
    
    /**
    *   @name: RQO_cls_ApplicationSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Constructor sobrecargado que recibe como parámetro un conjunto de campos 
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public RQO_cls_ApplicationSelector(Boolean includeFieldSetFields) 
    {         
        // Disable the default base class read security checking          
        //  in preference to explicit checking elsewhere                 
        this(includeFieldSetFields, false, false);     
    }     
    
    /**
    *   @name: RQO_cls_ApplicationSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Constructor sobrecargado que recibe como parámetro un conjunto de campos , un flag enforceCRUD y un flag
    *   enforceFLS
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public RQO_cls_ApplicationSelector(Boolean includeFieldSetFields, Boolean enforceCRUD, Boolean enforceFLS) 
    {         
        // Disable sorting of selected fields to aid debugging//  (performance optimisation)         
        super(includeFieldSetFields, enforceCRUD, enforceFLS, false);     
    } 
}