public with sharing class AM_Nuevo_RADAR_Controller {
    public string selected {get;set;}
   public string opt {get;set;}
    public AM_Estacion_de_servicio__c es {get;set;}
    public Survey_Question__c sq {get;set;}
    public SurveyTaker__c st {get;set;}
    public list<Survey_Question__c> listsq {get;set;}
    public map<id, Survey__c> ms  {get;set;}
    public boolean showAply {get;set;}
    public string url {get;set;}
    
    
   
    
    public AM_Nuevo_RADAR_Controller(ApexPages.StandardController controller) {
        es = (AM_Estacion_de_servicio__c)controller.getRecord();
        ms= new map<id, Survey__c>();
        showAply = False;
    }
    
      public List<SelectOption> getRadares() {
         List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('','--Elija un modelo Radar--'));
         for(Survey__c r : [Select id, name from Survey__c where AM_modelo__c = True]) {
             options.add(new SelectOption(r.id, r.name));
             ms.put(r.id,r);
         }
       return options;  
      } 
    
    public PageReference next(){
         
        
        if(selected==null || selected == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Debe Elegir un modelo Radar'));
            return null;
        } 
         showAply = True;
        return null;
        //string url = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + Page.TakeSurvey.getUrl() + '?&id='+selected+'&cId=none&caId=none&esid='+es.ID;
        /*
        system.debug(selected);
        return new pagereference(Page.TakeSurvey.getUrl() + '?id='+selected+'&cId=none&caId=none&esid='+es.ID);
       */
    
    }
           public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
                options.add(new SelectOption('True','Si'));
                options.add(new SelectOption('False','No'));
                return options;
    }
    
     Public pagereference noAplica(){
       st = new SurveyTaker__c ();
         if(opt == String.valueOf(false)){
         list<SurveyTaker__c > listR = [Select id, name, AM_Estacion_de_servicio__c from   SurveyTaker__c  where AM_Estacion_de_servicio__c =: es.id and Survey__c = :selected ];
         if(!listR.isEmpty()){
             delete listR ;
         }
             st.AM_Aplica_No_Aplica__c = true;
             st.AM_Estacion_de_servicio__c = es.id;
             st.Survey__c = selected;
             insert st;
             
             system.debug(opt+'- In');
            pagereference pf = new pagereference (ApexPages.currentPage().getHeaders().get('Host')+'/'+es.ID);
             system.debug(':::::::::::pf:::::::::::'+pf);
             pagereference pf2 = new pagereference ('/'+es.ID);
             system.debug('::::::::::::pf2::::::::::'+pf2);
          //  return new pagereference (ApexPages.currentPage().getHeaders().get('Host')+'/'+es.ID);      
        //return new PageReference ('https://'+ApexPages.currentPage().getHeaders().get('Host')+'/'+es.ID); // Classic
          return new PageReference ('/'+es.ID);   
       //   string navURL = 'https://sforce.one.navigateToSObject(' + es.Id +')'; // ligthning
        //   return new PageReference (navURL);

         } else {
               system.debug(opt+'- Out' ); 
         system.debug(selected);
            return new pagereference(Page.TakeSurvey.getUrl() + '?id='+selected+'&cId=none&caId=none&esid='+es.ID);
         }
        
    
       
    }
    

}