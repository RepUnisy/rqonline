/**
 *	@name: RQO_cls_Documentum
 *	@version: 1.0
 *	@creation date: 14/09/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase generica de acceso a las diferentes operaciones de los servicios de documentum
 *	@testClass: RQO_cls_Documentum_test
*/
public without sharing class RQO_cls_Documentum {
    
    private static final String CLASS_NAME = 'RQO_cls_Documentum';
    private RQO_cls_documentumResponse documentumResponse = null;
    private Map<String, String> reqHeaderData;
    private List<RQO_obj_logProcesos__c> listProcessLog;
    private RQO_obj_logProcesos__c objLog;
	private String intialRequestBody = '';
	private List<String> listValues = null;
	private Integer paramNumber = 0;
	private String methodType = '';
	private String soapAction = '';
	private List<String> gradeList = null;
	private Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>> mapDataService = null;
    public Boolean esFactura = false;
    public DateTime fechaFactura = null;
    private Boolean realizarInsercionLog = false;
   
    /**
	* @creation date: 03/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método principal para la obtención de documetos de Documentum por vía indirecta. Recupera a través de un servicio pasando el id de SAP el id 
	*	correspondiente de Documentum y con este se trae los datos del servicio principal.
	* @param: repository tipo String, repositorio sobre el que se quiere buscar el documento. Los repositorios y user/ password 
	*	de acceso están parametrizados en el custom settings RQO_cs_DocumentumRepository.
	* @param: idDocumentum tipo String, id en SAP del documento que queremos recuperar.
    * @return: Objeto de tipo RQO_cls_documentumResponse.
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_documentumResponse documentIndirectCall(String repository, String idDocumentum){
		final String METHOD = 'documentIndirectCall';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        RQO_cls_documentumResponse retData = this.documetumOp(RQO_cls_Constantes.WS_DOCUM_SAP_GET, repository, idDocumentum, null, null, null, null, null, false);        
        if (String.isNotEmpty(retData.docIdBySAP) && !Test.isRunningTest()){
            retData = this.documetumOp(RQO_cls_Constantes.WS_DOCUM_GET, repository, retData.docIdBySAP, null, null, null, null, null, false);
        }
        if (this.realizarInsercionLog){this.generateLog();}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return retData;
    }
            
    /**
	* @creation date: 14/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método principal para la generación de documetos de los distintos repositorios de Documentum
	* @param: typeOp, tipo String, tipo de operación (Obtención: DocumentumGetOp, Obtención SAP: DocumentumSapGetOp, Creación: DocumentumCreateOp).
	* @param: repository tipo String, repositorio sobre el que se quiere buscar el documento. Los repositorios y user/ password de acceso 
	*	están parametrizados en el custom settings RQO_cs_DocumentumRepository. 
	* @param: idDocumentum tipo String, id del documento que queremos recuperar.
	* @param: docName tipo String, nombre del document.
	* @param: tipoDoc tipo String, tipo de documento.
	* @param: base64DocBody tipo String, Blob del fichero decodificado en base64
	* @param: gradoName tipo String, Nombre de grado.
	* @param: idioma tipo String.
	* @param: generateLog tipo Boolean, indica si en la ejecucións se genera log.	
	* @return: Objeto de tipo RQO_cls_documentumResponse.
	* @exception: 
	* @throws: 
	*/
    public RQO_cls_documentumResponse documetumOp(String typeOp, String repository, String idDocumentum, String docName, String tipoDoc, String base64DocBody, String gradoName, String idioma, Boolean generateLog){
		final String METHOD = 'documetumOp';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        this.instanceInitalLog(typeOp);
        documentumResponse = null;
        //Comprobamos si el repositorio exite, en caso contrario devolvemos un error controlado. El error y le mensaje se gestiona dentro del propio método
        RQO_cs_documentumRepository__c csRepository = this.verifyRepository(repository);
        if (csRepository == null){return documentumResponse;}
        //Cargamos los datos de la request en función del tipo de operacióno
        if (!this.loadParamData(typeOp, csRepository, idDocumentum, docName, tipoDoc, gradoName, idioma, base64DocBody)){
            return new RQO_cls_documentumResponse(RQO_cls_Constantes.DOCUMENTUM_ERROR_GENERIC_NOK, Label.RQO_lbl_RequestBodyNoData);        
		}
        objLog.RQO_fld_body__c = (intialRequestBody != null && intialRequestBody.length() > 131070) ? intialRequestBody.substring(0, 131070) : intialRequestBody;
        System.debug('Vamos a gestionar la llamada');
        this.manageDocumentumCallOut(typeOp, intialRequestBody, paramNumber, listValues, methodType, soapAction);
        if (generateLog && this.realizarInsercionLog){this.generateLog();}
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return documentumResponse;
    }  
    
	/**
	* @creation date: 03/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método principal para la obtención de documetos de tipo factura de documentum.
	* @param: repository tipo String, repositorio sobre el que se quiere buscar el documento. Los repositorios y user/ password 
	*	de acceso están parametrizados en el custom settings RQO_cs_DocumentumRepository.
	* @param: numFactura tipo String, id en SAP del documento que queremos recuperar.
	* @param: fechaFactura tipo String, id en SAP del documento que queremos recuperar.
    * @return: Objeto de tipo RQO_cls_documentumResponse.
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_documentumResponse getInvoiceDoc(String repository,  String numFactura, String fechaFactura){
        final String METHOD = 'getInvoiceDoc';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        this.esFactura = true;
        this.fechaFactura = (String.isNotEmpty(fechaFactura)) ? datetime.valueOf(fechaFactura + ' 00:00:00') : null;
        RQO_cls_documentumResponse retData = this.documetumOp(RQO_cls_Constantes.WS_DOCUM_SAP_GET, repository, numFactura, null, null, null, null, null, false);
        if (String.isNotEmpty(retData.docIdBySAP) && !Test.isRunningTest()){
            retData = this.documetumOp(RQO_cls_Constantes.WS_DOCUM_GET, repository, retData.docIdBySAP, null, null, null, null, null, false);
        }
        if (this.realizarInsercionLog){this.generateLog();}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return retData;
    }

    /**
	* @creation date: 03/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Instancia la lista de logs y el objeto log con los datos iniciales
	* @param: String typeOp, tipo de operación
	* @return: void
	* @exception: 
	* @throws: 
	*/
    private void instanceInitalLog(String typeOp){
        final String METHOD = 'instanceInitalLog';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        if(listProcessLog == null){listProcessLog = new List<RQO_obj_logProcesos__c>();}
        objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_DOCUMENTUM, this.getProcessDescription(typeOp));
        listProcessLog.add(objLog);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
	* @creation date: 19/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera el nombre del servicio en función del tipo de operación
	* @param: String typeOp, tipo de operación
	* @return: String
	* @exception: 
	* @throws: 
	*/
	private String getProcessDescription(String typeOp){
        final String METHOD = 'getProcessDescription';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String processType = '';
		if (String.isNotEmpty(typeOp) && RQO_cls_Constantes.WS_DOCUM_GET.equalsIgnoreCase(typeOp)){
            processType = RQO_cls_Constantes.SERVICE_DOCUMETUM_DIRECT_CALL;
        }else if (String.isNotEmpty(typeOp) && RQO_cls_Constantes.WS_DOCUM_SAP_GET.equalsIgnoreCase(typeOp)){
            processType = RQO_cls_Constantes.SERVICE_DOCUMETUM_INDIRECT_CALL;
        }else if (String.isNotEmpty(typeOp) && RQO_cls_Constantes.WS_DOCUM_CREATE.equalsIgnoreCase(typeOp)){
            processType = RQO_cls_Constantes.SERVICE_DOCUMETUM_UPLOAD;
        }else if(String.isNotEmpty(typeOp) && RQO_cls_Constantes.WS_DOCUM_RP2_GET.equalsIgnoreCase(typeOp)){
            processType = RQO_cls_Constantes.SERVICE_DOCUMETUM_RP2;
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return processType;
    }
    
    /**
	* @creation date: 03/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Establece los datos finales y genera los registros de log.
	* @param:
	* @return: void
	* @exception: 
	* @throws: 
	*/
    private void generateLog(){
		final String METHOD = 'generateLog';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        if (listProcessLog != null && !listProcessLog.isEmpty()){
        	objLog.RQO_fld_estado__c = RQO_cls_ProcessLog.getStatusCodeByErrorCode(documentumResponse.errorCode);
            objLog.RQO_fld_errorCode__c = documentumResponse.errorCode;
            objLog.RQO_fld_errorMessage__c = documentumResponse.errorMessage;
            objLog.RQO_fld_exceptionMessage__c = documentumResponse.exceptionMessage;
            try{
                RQO_cls_ProcessLog.generateLogData(listProcessLog);
            }catch(Exception ex){
                System.debug('Se ha producido un error: ' + ex.getMessage());
            }
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }


    /**
	* @creation date: 02/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Carga los parámetros que se utilizan en la request del servicio en función del tipo de operación
	* @param: typeOp, tipo String, tipo de operación (Obtención: DocumentumGetOp, Creación: DocumentumCreateOp).
	* @param: csRepository tipo RQO_cs_documentumRepository__c, repositorio del CS sobre el que se quiere buscar el documento. Los repositorios y user/ password 
	* de acceso están parametrizados en el custom settings RQO_cs_DocumentumRepository.
	* @param: idDocumentum tipo String, id del documento que queremos recuperar.
	* @param: docName tipo String, nombre del document
	* @param: tipoDoc tipo String, tipo de documento.
	* @param: gradoName tipo String, Nombre de grado.
	* @param: idioma, tipo String.
	* @param: base64DocBody tipo String, Blob del fichero decodificado en base64
	* @return: Objeto de tipo RQO_cls_documentumResponse.
	* @exception: 
	* @throws: 
	*/
    private Boolean loadParamData(String typeOp, RQO_cs_documentumRepository__c csRepository, String idDocumentum, String docName, String tipoDoc, 
                                 String gradoName, String idioma, String base64DocBody){
                                     
		final String METHOD = 'loadParamData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
                                     
        Boolean dataReturn = false;
      	intialRequestBody = '';
		listValues = null;
		paramNumber = 0;
		methodType = '';
        soapAction = '';
		//En función del tipo de operación recuperamos los datos genericos de la request de varios custom labels y los datos que debemos parsear
        if (String.isNotEmpty(typeOp) && RQO_cls_Constantes.WS_DOCUM_GET.equalsIgnoreCase(typeOp)){
			intialRequestBody = Label.RQO_lbl_HeaderEnvGetDocumentum + Label.RQO_lbl_HeaderContentGetDocumentum + Label.RQO_lbl_BodyGetDocumentum;			
            listValues = new List<String>{csRepository.RQO_fld_password__c, csRepository.RQO_fld_repository__c, csRepository.RQO_fld_userName__c, csRepository.RQO_fld_repository__c, idDocumentum};
            paramNumber = 5;
            methodType = RQO_cls_Constantes.REST_METHOD_TYPE_GET;
            soapAction = RQO_cls_Constantes.SOAP_REST_SOAP_ACTION_DOCUMENTUM_URN_GET;
            return true;
        }else if (String.isNotEmpty(typeOp) && RQO_cls_Constantes.WS_DOCUM_SAP_GET.equalsIgnoreCase(typeOp)){
			intialRequestBody = Label.RQO_lbl_HeaderEnvGetDocumentumSap + Label.RQO_lbl_BodyGetDocumentumSAP;
            String strQueryParam = 'select r_object_id from do_uuii_fichaproducto where object_name = \'' + idDocumentum + '\'';
            if(esFactura){
				String fecha = (fechaFactura == null) ? DateTime.now().format('MM/dd/yyyy') : fechaFactura.format('MM/dd/yyyy');
               	strQueryParam = 'select r_object_id from do_firqu_fac where atr_tipo_factura = \'\' and object_name = \'' + idDocumentum + '\' ' +
                    			'and attr_fecha_fac = DATE(\'' + fecha + '\' ,\'mm/dd/aaaa\');';
            }
            listValues = new List<String>{csRepository.RQO_fld_password__c, csRepository.RQO_fld_repository__c, csRepository.RQO_fld_userName__c, strQueryParam, csRepository.RQO_fld_repository__c};
            paramNumber = 5;
            methodType = RQO_cls_Constantes.REST_METHOD_TYPE_POST;
            soapAction = RQO_cls_Constantes.SOAP_REST_SOAP_ACTION_DOCUMENTUM_URN_EXECUTE;
            return true;
        }else if (String.isNotEmpty(typeOp) && RQO_cls_Constantes.WS_DOCUM_CREATE.equalsIgnoreCase(typeOp)){
           	intialRequestBody = Label.RQO_lbl_HeaderEnvCreateDocumentum + Label.RQO_lbl_BodyContentCreateDocumentum1 + Label.RQO_lbl_BodyContentCreateDocumentum2 + Label.RQO_lbl_BodyContentCreateDocumentum3; 
           	//Recuperamos la extension del documento que vamos a subir
        	Integer extensionIndex = docName.lastIndexOf('.');
        	String extensionDocumento = docName.substring(extensionIndex+1, docName.length()).toUpperCase();
            //Cargamos la lista de valores que vamos a utilizar en la llamada 
            listValues = new List<String>{csRepository.RQO_fld_password__c, csRepository.RQO_fld_repository__c, csRepository.RQO_fld_userName__c, csRepository.RQO_fld_repository__c, docName,
                datetime.now().format('MM/dd/yyyy HH:mm:ss'), extensionDocumento, tipoDoc, gradoName, idioma, base64DocBody};
            paramNumber = 11;
            methodType = RQO_cls_Constantes.REST_METHOD_TYPE_POST;
            soapAction = RQO_cls_Constantes.SOAP_REST_SOAP_ACTION_DOCUMENTUM_URN_CREATE;
            return true;
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
                                     
        return false;
    }
    
    /**
	* @creation date: 14/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método principal para la generación de documetos de los distintos repositorios de Documentum
	* @param: typeOp, tipo String, tipo de operación (Obtención: DocumentumGetOp, Creación: DocumentumCreateOp).
	* @param: initialRequestBody tipo String, cuerpo inicial de la request sin parsear los parámetros.
	* @param: paramNumber tipo Integer, número de parámetros que se parsean.
	* @param: listValues tipo List<String>, lista de valores que se parsean.
	* @param: methodType tipo String, tipo de petición (GET, POST...).
	* @param: soapAction tipo String, tipo de acción soap.
	* @return:
	* @exception:
	* @throws:
	*/
    private void manageDocumentumCallOut(String typeOp, String intialRequestBody, Integer paramNumber, List<String> listValues, String methodType, String soapAction){	
		final String METHOD = 'manageDocumentumCallOut';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Recuperamos la request final con los datos parseados 
        RQO_cls_WSSOAPUtilities util = new RQO_cls_WSSOAPUtilities();
        String finalRequestBody = util.getHttpSOAPServiceFinalBody(intialRequestBody, paramNumber, listValues);
        if (String.isNotEmpty(finalRequestBody)){
            try{
                //Realizamos la petición contra documentum
            	HttpResponse result = doDocumentumCallOut(typeOp, finalRequestBody, methodType, soapAction);
                System.debug('HTTP Result: ' + result);
                Boolean typeResponseOk = false;
                if (RQO_cls_Constantes.REST_COD_HTTP_OK == result.getStatusCode()){typeResponseOk = true;}
                //Establecemos incialmente la respuesta http del servicio
                if (documentumResponse == null){documentumResponse = new RQO_cls_documentumResponse();}
                documentumResponse.errorCode = String.valueOf(result.getStatusCode());
        		documentumResponse.errorMessage = result.getStatus();
                //Vamos a recuperar los datos de response
                getResponseData(result.getBody(), typeOp, typeResponseOk);
            }catch(Exception ex){
				System.debug('Error al obtener el documento: ' + ex.getMessage());
                if (documentumResponse == null){documentumResponse = new RQO_cls_documentumResponse();}
                documentumResponse.errorCode = RQO_cls_Constantes.DOCUMENTUM_ERROR_GENERIC_NOK;
        		documentumResponse.errorMessage = Label.RQO_lbl_DocumentumExceptionMsg;
                documentumResponse.exceptionMessage = ex.getMessage();
                this.realizarInsercionLog = true;
            }
        }else{
            System.debug('No ha sido posible generar el documento.');
            if (documentumResponse == null){documentumResponse = new RQO_cls_documentumResponse();}
            documentumResponse.errorCode = RQO_cls_Constantes.DOCUMENTUM_ERROR_GENERIC_NOK;
            documentumResponse.errorMessage = Label.RQO_lbl_RequestBodyNoData;
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
	/**
	* @creation date: 14/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método que verifica contra el custom serttings RQO_cls_documentumResponse si el repositorio existe
	* @param: repository tipo String, repositorio a verificar
	* @return: RQO_cs_documentumRepository__c Objeto con los datos de acceso al repositorio
	* @exception: 
	* @throws: 
	*/
    private RQO_cs_documentumRepository__c verifyRepository(String repository){
		final String METHOD = 'verifyRepository';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        RQO_cs_documentumRepository__c csRepository = RQO_cs_documentumRepository__c.getInstance(repository);
        if (csRepository == null){
            if (documentumResponse == null){documentumResponse = new RQO_cls_documentumResponse();}
            documentumResponse.errorCode = RQO_cls_Constantes.DOCUMENTUM_ERROR_GENERIC_NOK;
            documentumResponse.errorMessage = Label.RQO_lbl_DocumentumErrorNoRepository;
            return null;
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return csRepository;
    }
    
	/**
	* @creation date: 14/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método que realiza la petición http contra documentum con los parametros que se especifiquen
	* @param: typeOp tipo String, tipo de operación
	* @param: body tipo String, cuerpo de la request que se va enviar. 
	* @param: methodType tipo String, tipo de petición (GET, POST...), 
	* @param: documentumURN tipo String, URN que se envía en la cabecera.
	* @return: HttpResponse Objeto con los datos de acceso al repositorio
	* @exception: 
	* @throws: 
	*/
    private HttpResponse doDocumentumCallout(String typeOp, String body, String methodType, String documentumURN){
        final String METHOD = 'doDocumentumCallout';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        RQO_obj_parametrizacionIntegraciones__c serviceParam  = RQO_obj_parametrizacionIntegraciones__c.getInstance(typeOp);
        //Llamada al servicio WEB
        HttpRequest req = new HttpRequest();
        req.setEndpoint(RQO_cls_Constantes.CALLOUT_BUS_NAMED_CREDENTIAL_DOCUMENTUM + serviceParam.RQO_fld_servicePath__c);
        //Cargamos la cabecera
        req.setHeader(RQO_cls_Constantes.REST_SOAP_ACTION_KEY, documentumURN);
        req.setHeader(RQO_cls_Constantes.REST_CONTENT_TYPE_KEY, RQO_cls_Constantes.REST_CONTENT_TYPE_XML);
        //Cargamos el cuerpo
        req.setBody(body);
        System.debug(body);
        req.setMethod(methodType);
        req.setTimeout((serviceParam.RQO_fld_timeout__c).intValue());
        Http http = new Http();
        HttpResponse result;        
        result = http.send(req);
        System.debug('Response: ' + result.getBody());
        objLog.RQO_fld_fechaRespuesta__c = System.now();
        objLog.RQO_fld_host__c = req.getEndpoint();
        objLog.RQO_fld_header__c = RQO_cls_Constantes.REST_SOAP_ACTION_KEY + RQO_cls_Constantes.COMMA + documentumURN + RQO_cls_Constantes.SEMICOLON 
            + RQO_cls_Constantes.REST_CONTENT_TYPE_KEY + RQO_cls_Constantes.COMMA + RQO_cls_Constantes.REST_CONTENT_TYPE_XML;
        objLog.RQO_fld_body__c = (body != null && body.length() > 131070) ? body.substring(0, 131070) : body;
        objLog.RQO_fld_response__c = (result.getBody() != null && result.getBody().length() > 131070) ? result.getBody().substring(0, 131070) : result.getBody();

        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return result;
    }
     
  	/**
	* @creation date: 14/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método genérico que parsea las respuestas de los servicios de documentum y rellena el objeto de respuesta general
	* @param: responseData tipo String, cuerpo de la response recibida. 
	* @Param: service tipo String, nombre del servicio cuya response se va a tratar.
	* @Param: isOk tipo Boolean, marca si hay que hacer gestión de errores o no.
	* @return: 
	* @exception: 
	* @throws: 
	*/
    private void getResponseData(String responseData, String service, Boolean isOk){
		final String METHOD = 'getResponseData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Generamos la estructura DOM a partir del XML de retorno y accedemos a sus hijos hasta encontrar los datos que necesitamos
        Dom.Document doc = new Dom.Document();
        doc.load(responseData);
        //Retrieve the root element for this document.        
        Dom.XMLNode Envelope = doc.getRootElement();
        Dom.XMLNode child= Envelope.getChildElements()[0];
        Dom.XMLNode child1= child.getChildElements()[0];
        if(!isOk){
            //Ha habido error, vamos a buscar el mensaje de error
            if(child1.getChildElements() != null && child1.getChildElements().size() > 1){
                documentumResponse.exceptionMessage = child1.getChildElements()[1].getText();
            }
        }else{
            //Tratamos las respuestas correctas de los diferentes servicios
            Dom.XMLNode child2= child1.getChildElements()[0];
            if (RQO_cls_Constantes.WS_DOCUM_GET.equalsIgnoreCase(service)){
                this.getResponseDataGetDocument(child2);
            }else if (RQO_cls_Constantes.WS_DOCUM_SAP_GET.equalsIgnoreCase(service)){
            	this.getResponseDataSAPGetDocument(child2);
            }else if(RQO_cls_Constantes.WS_DOCUM_CREATE.equalsIgnoreCase(service)){
                this.getResponseDataCreateDocument(child2);
            }
        }
       
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
   
     /**
	* @creation date: 14/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método específico del servicio de obtención de documentos que recorre los nodos properties para recuperar los datos que nos interesan.
	* @param: properties tipo Dom.XMLNode, nodo XML de la response que tenemos que recorrer
	* @return: 
	* @exception: 
	* @throws: 
	*/
    private void getResponseDataGetDocument(Dom.XMLNode child2){
		final String METHOD = 'getResponseDataGetDocument';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Dom.XMLNode child3= child2.getChildElements()[0];
        Dom.XmlNode properties = child3.getChildElements()[1];
        String docTypeTipoOb = '';
        String docTypeExtension = '';
        String docTypeContentType = '';
        for(Dom.XmlNode node: properties.getChildElements()) {
			if(node.getAttributeValue('name',null)=='atr_tipoob') {
                docTypeTipoOb = node.getChildElements()[0].getText().toLowerCase();
            }
            if(node.getAttributeValue('name',null)=='a_content_type') {
                docTypeContentType = node.getChildElements()[0].getText().toLowerCase();
            }
            if(node.getAttributeValue('name',null)=='object_name') {//devolvemos el nombre
                String docName = node.getChildElements()[0].getText();
                if(docName.contains(RQO_cls_Constantes.DOT)){
                    String[] docNameSplit = docName.split(RQO_cls_Constantes.ESCAPED_DOT);
                    docName = docNameSplit[0];
                    docTypeExtension = docNameSplit[1];
                }
                documentumResponse.docName = docName;
            }
        }
        documentumResponse.docType = (String.isnotEmpty(docTypeExtension)) ? docTypeExtension.toLowerCase() : (String.isnotEmpty(docTypeTipoOb)) ? docTypeTipoOb.toLowerCase() : (String.isnotEmpty(docTypeContentType)) ? docTypeContentType.toLowerCase() : '';
        Dom.XMLNode child4= child3.getChildElements()[2];  //Value
		documentumResponse.docData = child4.getChildElements()[2].getText();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
	* @creation date: 04/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método específico del servicio de obtención de id de documentum a través de id de SAP que recorre los nodos para recuperar los datos 
	*	que nos interesan.
	* @param: properties tipo Dom.XMLNode, nodo XML de la response que tenemos que recorrer
	* @return: 
	* @exception: 
	* @throws: 
	*/
    private void getResponseDataSAPGetDocument(Dom.XMLNode properties){
        final String METHOD = 'getResponseDataSAPGetDocument';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		Dom.XMLNode child3= properties.getChildElements()[1];
        String queryResult = child3.getAttribute(child3.getAttributeKeyAt(0),'');
        //Miramos el count de registros de la consulta que hemos realizado.
        //Si tiene 1 resultado recuperamos los datos, si no tiene o tiene más de uno devolvemos error
        if(RQO_cls_Constantes.NUMBER_1.equalsIgnoreCase(queryResult)){
            Dom.XMLNode childDataPackage= properties.getChildElements()[0];
            Dom.XMLNode childData= childDataPackage.getChildElements()[0];
            Dom.XMLNode identityData= childData.getChildElements()[0];
            Dom.XMLNode objectId= identityData.getChildElements()[0];
            documentumResponse.docIdBySAP = objectId.getAttribute(objectId.getAttributeKeyAt(0),'');
        }else if(RQO_cls_Constantes.NUMBER_0.equalsIgnoreCase(queryResult)){
			documentumResponse.errorCode = RQO_cls_Constantes.DOCUMENTUM_ERROR_GENERIC_NO_DATA;
            documentumResponse.errorMessage = Label.RQO_lbl_documentumErrorSapNoResults;
        }else{
			documentumResponse.errorCode = RQO_cls_Constantes.DOCUMENTUM_ERROR_GENERIC_NOK;
            documentumResponse.errorMessage = Label.RQO_lbl_DocumentumErrorSapManyResults;
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
	* @creation date: 04/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método específico del servicio de carga de documentos en Documentum que recorre los nodos para recuperar los datos que nos interesan.
	* @param: properties tipo Dom.XMLNode, nodo XML de la response que tenemos que recorrer
	* @return: 
	* @exception: 
	* @throws: 
	*/
    private void getResponseDataCreateDocument(Dom.XMLNode child2){
		final String METHOD = 'getResponseDataCreateDocument';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Dom.XMLNode child3= child2.getChildElements()[0];
        Dom.XMLNode child4= child3.getChildElements()[0];
        Dom.XMLNode child5= child4.getChildElements()[0];
        documentumResponse.docId = child5.getAttribute(child5.getAttributeKeyAt(0),'');
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }

	/**
	* @creation date: 17/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método para obtener los datos de documentos FDS y NIS de RP2 y cargarlos en Salesforce
	* @param: company tipo String, empresa sobre la que se quieren consultar los documentos (RQA - Repsol Química)
	* @param: fechaInicio, tipo Date. Fecha de inicio
	* @param: fechaFin, tipo Date. Fecha de fin
	* @return:
	* @exception: 
	* @throws: 
	*/
    public void getDocumentumRP2(String company, Date fechaInicio, Date fechaFin){
		final String METHOD = 'getDocumentumRP2';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Inicializamos los objetos de log
		this.instanceInitalLog(RQO_cls_Constantes.WS_DOCUM_RP2_GET);
        try{
            //Llamamos al método genérico que monta la request y realiza el callout
            RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element responseData = this.doRP2CallOut(company, fechaInicio, fechaFin);
            if(responseData != null){
                //Si tengo datos de vuelta llamamos al método de gestión de respuesta
                this.manageRP2ReturnData(responseData);
            }
        }catch(System.CalloutException ex) {
            System.debug('CallOut Exception: ' + ex.getMessage());
            this.realizarInsercionLog = true;
            //Si da una excepción de tipo callout genero el log correspondiente
            RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage());
        }catch(Exception ex){
            System.debug('Exception: ' + ex.getMessage());
            this.realizarInsercionLog = true;
            //Si da una excepción genérica genero el log correspondiente
            RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage());
        }
        if (this.realizarInsercionLog){ RQO_cls_ProcessLog.generateLogData(listProcessLog);}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
	/**
    * @creation date: 17/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método que carga la request y realiza el callout contra RP2
    * @param: company tipo String, empresa sobre la que se quieren consultar los documentos (RQA - Repsol Química)
    * @param: fechaInicio, tipo Date. Fecha de inicio
    * @param: fechaFin, tipo Date. Fecha de fin
    * @return:
    * @exception: 
    * @throws: 
	*/
    private RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element doRP2CallOut(String company, Date fechaInicio, Date fechaFin){
        final String METHOD = 'doRP2CallOut';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Cargamos los datos de la request
        String area = null;
        String empresa = company;
        String fechaDesde = (fechaInicio != null) ? String.valueOf(fechaInicio) : null;
        String fechaHasta = (fechaFin != null) ? String.valueOf(fechaFin) : null;
        String nProducto = null;
        String namsrd = null;
        String seccion = null;
        //Realizamos la llamada
        RQO_cls_GetDocumentumRP2.RQ_ONLINE_Rx2_fichasSeguridad_FE_psHttpsSoap11Endpoint objCall = 
            new RQO_cls_GetDocumentumRP2.RQ_ONLINE_Rx2_fichasSeguridad_FE_psHttpsSoap11Endpoint();
        RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element objReturn = objCall.ZsapGetprodRlesaRq(area, empresa, fechaDesde, fechaHasta, nProducto, namsrd, seccion, objLog);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return objReturn;
    }
    
    /**
    * @creation date: 15/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método get del atributo con la lista de grados
    * @param: List<String>
    * @return:
    * @exception: 
    * @throws: 
    */
    public List<String> getGradeList(){
        return gradeList;
    }
    
    /**
    * @creation date: 15/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método get del atributo con los datos recuperados del servicio
    * @param: Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>>
    * @return:
    * @exception: 
    * @throws: 
    */
    public Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>> getMapDataService(){
        return mapDataService;
    }

                        
	/**
    * @creation date: 17/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método que gestiona la response del servicio de RP2
    * @param: responseData tipo RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element, response del servicio
    * @return:
    * @exception: 
    * @throws: 
	*/
    private void manageRP2ReturnData(RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element responseData){
		final String METHOD = 'manageRP2ReturnData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String errorLog = RQO_cls_Constantes.SOAP_COD_NOK;
        String errorMessageLog = '';
        RQO_cls_GetDocumentumRP2.Zelog responseLog = responseData.Log;
        //Verificamos que tenemos log de respuesta
        if (responseLog != null){
            //Si tengo respuesta correcta o informativa vamos a por los datos
            if(String.isNotEmpty(responseLog.Type_x) && (RQO_cls_Constantes.REQ_WS_RESPONSE_OK.equalsIgnoreCase(responseLog.Type_x) 
                                                         || RQO_cls_Constantes.REQ_WS_RESPONSE_INFO.equalsIgnoreCase(responseLog.Type_x))){
                errorLog = RQO_cls_Constantes.SOAP_COD_OK;
                errorMessageLog = responseLog.Msg;
                RQO_cls_GetDocumentumRP2.ZtyteehsProdrlsa Productos;
				//Verificamos que tenemos lista de documentos a actualizar
                if (responseData.Productos != null && responseData.Productos.item != null && !responseData.Productos.item.isEmpty()){
                    gradeList = new List<String>();
                    mapDataService = new Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>>();
                    List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa> listServiceDataAux = null;
                    //Recorremos cada dato de documento devuelto por el servicio para cargar la lista de grados (con el PS) y el mapa la lista de documentos
                    //para cada grado. Solo se añaden los de tipo FDS y NIS.
                    for (RQO_cls_GetDocumentumRP2.ZeehsProdrlsa items : responseData.Productos.item){
                        if (RQO_cls_Constantes.DOC_TYPE_CODE_FICHA_SEGURIDAD.equalsIgnoreCase(items.Tficha) || RQO_cls_Constantes.DOC_TYPE_CODE_NIS.equalsIgnoreCase(items.Tficha)){
                            gradeList.add(items.Subid);
                            if (mapDataService != null && !mapDataService.isEmpty() && mapDataService.containsKEY(items.Subid)){
                                listServiceDataAux = mapDataService.get(items.Subid);
                            }else{
                                listServiceDataAux = new List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>();
                                mapDataService.put(items.Subid, listServiceDataAux);
                            }
                            listServiceDataAux.add(items);
                        }
                    }
                }
            }else{
                errorMessageLog = responseLog.Msg;
            }
        }else{
			errorMessageLog =  Label.RQO_lbl_RP2DocumentumNoDataMsg;
        }
        RQO_cls_ProcessLog.completeLog(objLog, errorLog, errorMessageLog, null);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
	
    /**
     *	@name: RQO_cls_documentumResponse
     *	@version: 1.0
     *	@creation date: 14/09/2017
     *	@author: Alvaro Alonso - Unisys
     *	@description: Clase wrapper con los datos de response comunes que queremos devolver tras la llamada a los servicios
     *
    */
    public class RQO_cls_documentumResponse{
        public String errorCode{get;set;}
        public String errorMessage{get;set;}
        public String exceptionMessage{get;set;}
        public String docType{get;set;}
    	public String docData{get;set;}
        public String docName{get;set;}
        public String docId{get;set;}
        public String objType{get;set;}
        public String docIdBySAP{get;set;}
        
		
        /**
        * @creation date: 14/09/2017
        * @author: Alvaro Alonso - Unisys
        * @description:	Constructor por defecto
        * @return: 
        * @exception: 
        * @throws: 
        */
        public RQO_cls_documentumResponse(){}
        
        /**
        * @creation date: 14/09/2017
        * @author: Alvaro Alonso - Unisys
        * @description:	Constructor con parámetros
        * @param: Param1 - errorCode tipo String, código de error. Param 2 - errorMessage tipo String, mensaje de error
        * @return: 
        * @exception: 
        * @throws: 
        */
        public RQO_cls_documentumResponse(String errorCode, String errorMessage){
			this.errorCode = errorCode;
        	this.errorMessage = errorMessage;
        }
    }
}