/**
*	@name: RQO_cls_MailUtils_test
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar el funcionamiento de la clase de utilidades de mails
*/
@IsTest
public class RQO_cls_MailUtils_test {

    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createContact(1);
            RQO_cls_TestDataFactory.createGrade(1);
            RQO_cls_TestDataFactory.createPosiciondeEntrega(1);
            RQO_cls_TestDataFactory.createAsset(1);
            RQO_cls_TestDataFactory.createTranslation(1);
            RQO_cls_TestDataFactory.createContainerJunction(1);
            RQO_cls_TestDataFactory.createShippingJunction(1);
            RQO_cls_TestDataFactory.createNotification(1);
            RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_gradoNuevo_es', 'RQO_eplt_resumenDiario_es'});
		}
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método sendMail
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testSendMail(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String contactId = '';
            String gradeId = '';
            test.startTest();
            for(Contact item : [SELECT id FROM Contact]){
                contactId = item.Id;
            }
            for(RQO_obj_grade__c item : [SELECT id FROM RQO_obj_grade__c]){
                gradeId = item.Id;
            }
            
            List<RQO_cls_IntegrationEmailBean> emailBeanList = new List<RQO_cls_IntegrationEmailBean>();
            RQO_cls_IntegrationEmailBean emailBean = new RQO_cls_IntegrationEmailBean();
            emailBean.contact = contactId;
            emailBean.idObject = gradeId;
            emailBean.templateName = 'RQO_eplt_gradoNuevo_es';
            emailBeanList.add(emailBean);
            List<EmailTemplate> item = [SELECT Id, Name, isActive FROM EmailTemplate WHERE Name = 'RQO_eplt_gradoNuevo_es'];
                
            Messaging.SendEmailResult[] returned = RQO_cls_MailUtils.sendMail(emailBeanList);
            test.stopTest();
            
            System.assertEquals(true, returned.get(0).IsSuccess());
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método fillHtmlBody
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testFillHtmlBody(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String strHtmlBody = '';
            String requestManagementId = '';
            String requestManagementName = '';
            test.startTest();

            for (EmailTemplate item : [SELECT Name, Subject, HtmlValue FROM EmailTemplate WHERE Name = 'RQO_eplt_gradoNuevo_es']) {
                strHtmlBody = item.HtmlValue;
            }
            for (RQO_obj_requestManagement__c item : [SELECT Name, Id FROM RQO_obj_requestManagement__c]) {
                requestManagementId = item.Id;
                requestManagementName = item.Name;
            }

            String returned = RQO_cls_MailUtils.fillHtmlBody(strHtmlBody, requestManagementName, requestManagementId, 'es');
            test.stopTest();
            
            System.assertEquals(true, String.isNotBlank(returned));
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método fillHtmlBodyResume
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testFillHtmlBodyResume(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String strHtmlBody = '';
            List<RQO_obj_notification__c> notificationConfirmadoList = new List<RQO_obj_notification__c>();
			List<RQO_obj_notification__c> notificationCursoList = new List<RQO_obj_notification__c>();
			Map<String,String> entregaIdAuxMap = new Map<String,String>();
			Map<String,String> posPedidoIdMap = new Map<String,String>();
			
            test.startTest();
			List<RQO_obj_notificationConsent__c> notificationConsentAuxList = [SELECT Id, RQO_fld_emailSendingPeriodicity__c 
                                                                                FROM RQO_obj_notificationConsent__c
                                                                				WHERE RQO_fld_notificationType__c IN ('CO','TR')];
            for (RQO_obj_notificationConsent__c item : notificationConsentAuxList) {
                item.RQO_fld_emailSendingPeriodicity__c = 'Diario';
            }
            update notificationConsentAuxList;
						
            
            List<RQO_obj_notification__c> notificationList = [SELECT Id, RQO_fld_contact__c, RQO_fld_mailSent__c, RQO_fld_visualized__c, RQO_fld_messageType__c,
                                                              RQO_fld_objectType__c FROM RQO_obj_notification__c];
            
            for (Asset posPedido : [SELECT Id, RQO_fld_idExterno__c FROM Asset]) {
				posPedidoIdMap.put(posPedido.RQO_fld_idExterno__c, posPedido.Id);
			}
            
            for (RQO_obj_posiciondeEntrega__c posicionEntrega : [SELECT Id, RQO_fld_idRelacion__r.RQO_fld_idExterno__c	FROM RQO_obj_posiciondeEntrega__c]) {
				entregaIdAuxMap.put(posicionEntrega.RQO_fld_idRelacion__r.RQO_fld_idExterno__c, posicionEntrega.Id);
			}
            for (EmailTemplate item : [SELECT Name, Subject, HtmlValue FROM EmailTemplate WHERE Name = 'RQO_eplt_resumenDiario_es']) {
                strHtmlBody = item.HtmlValue;
            }
            
            for (RQO_obj_notification__c item : notificationList) {
				if (item.RQO_fld_messageType__c.equalsIgnoreCase('CO')) {
					notificationConfirmadoList.add(item);
                    for (String idEntrega : entregaIdAuxMap.keySet()) {
                        item.RQO_fld_externalId__c = idEntrega;
                    }                    
				} else {
					notificationCursoList.add(item);
                    for (String idPosicion : posPedidoIdMap.keySet()) {
                        item.RQO_fld_externalId__c = idPosicion;
                    }
				}
			}
            
            update notificationList;
            String returned;
            try {
                returned = RQO_cls_MailUtils.fillHtmlBodyResume(strHtmlBody, notificationCursoList, notificationConfirmadoList, 
																	posPedidoIdMap, entregaIdAuxMap, 'es');
            } catch (Exception e) {
                System.assertEquals('exception', ''+e.getMessage()+' '+e.getLineNumber());
            }
            
            test.stopTest();
            
            System.assertEquals(true, String.isNotBlank(returned));
        }
    }
    
}