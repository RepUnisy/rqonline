/*---------------------------------------------------------------------------
Author:          Borja Martín
Company:         Indra
Description:     Clase para calcular los totales en Project
Test Class:      ITPM_CalculateCostItemsProject
History
<Date>        <Author>       <Change Description>
19-10-2016      Borja Martín  Versión inicial.
----------------------------------------------------------------------------*/

public class ITPM_Administration {

    public ITPM_Administration(){}
    
     public static map<string, decimal> mapaDivisa(){

        List <CurrencyType> listaCurrencyFinal = [SELECT ConversionRate, IsoCode FROM CurrencyType]; 
        map<string, decimal> mapaCurrencys = new map<string, decimal>();
        
        for(CurrencyType divisa : listaCurrencyFinal){
            mapaCurrencys.put(divisa.IsoCode, divisa.ConversionRate);
        }

        return mapaCurrencys;
    }

    
    public static void calculateTotalProjects(){                
            
        try{
            map<string, decimal> mapaCurrencys = mapaDivisa();

            
            map<id, list<ITPM_Budget_Item__c>> MapaProject = new map<id, list<ITPM_Budget_Item__c>>();

 
            List<ITPM_Project__c> listProjectsActualizar = new List<ITPM_Project__c>();        

            List<ITPM_Budget_Item__c> listCostItems = [SELECT Id, Name, ITPM_REL2_Project__c, ITPM_CI_SEL_Version__c,
                                                       convertCurrency(ITPM_FOR_Total_expense__c), convertCurrency(ITPM_FOR_Total_investment__c)
                                                       FROM ITPM_Budget_Item__c
                                                       WHERE ITPM_REL2_Project__c != null
                                                       ];


            set<ITPM_Budget_Item__c> setCostItems = new set<ITPM_Budget_Item__c>();
            setCostItems.addAll(listCostItems);


            for(ITPM_Budget_Item__c costItem : setCostItems){
                    List <ITPM_Budget_Item__c> listaAux;
                    
                    if(costItem.ITPM_REL2_Project__c != null ){
                        
                        if(MapaProject.get(costItem.ITPM_REL2_Project__c) == null){
                            
                            listaAux = new list<ITPM_Budget_Item__c>();
                            listaAux.add(costItem);
                            MapaProject.put(costItem.ITPM_REL2_Project__c, listaAux);

                        }else{
                            
                            listaAux = MapaProject.get(costItem.ITPM_REL2_Project__c);
                            listaAux.add(costItem);
                            MapaProject.put(costItem.ITPM_REL2_Project__c, listaAux);
                        }
                    }
                   
            }
            if(MapaProject != null ){
                listProjectsActualizar = calculateProjects(MapaProject, mapaCurrencys);
                upsert listProjectsActualizar;
            }


        }catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error. Contact your system administrator.');
            ApexPages.addMessage(msg);
        }
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, '');
        ApexPages.addMessage(msg);
    }

    public static list<ITPM_Project__c> calculateProjects(map<id, list<ITPM_Budget_Item__c>> MapaProject, map<string, decimal> mapaCurrencys){


        List<ITPM_Project__c> listProjectsActualizar = new List<ITPM_Project__c>();

        map<id, ITPM_Project__c> mapaProjectQuery = new map<id, ITPM_Project__c>([SELECT id, CurrencyIsoCode, ITPM_CUR_Total_Plan_Expenses__c, ITPM_CUR_Total_Plan_Investment__c, ITPM_CUR_Total_Real_Expenses__c, ITPM_CUR_Total_Real_Investment__c  FROM ITPM_Project__c WHERE id IN :MapaProject.keyset()]);
        

        for(id aux : MapaProject.keyset()){
            list<ITPM_Budget_Item__c> listaCost = MapaProject.get(aux);
            ITPM_Project__c project = new ITPM_Project__c(id = aux, ITPM_CUR_Total_Plan_Expenses__c = 0, ITPM_CUR_Total_Plan_Investment__c = 0, ITPM_CUR_Total_Real_Expenses__c = 0, ITPM_CUR_Total_Real_Investment__c = 0, ITPM_CUR_Total_Estimated_Expenses__c  = 0,ITPM_CUR_Total_Estimated_Investment__c = 0, CurrencyIsoCode = mapaProjectQuery.get(aux).CurrencyIsoCode );

            for(ITPM_Budget_Item__c costItem : listaCost){   

                if(costItem.ITPM_CI_SEL_Version__c=='Plan'){                                                    
                                
                    project.ITPM_CUR_Total_Plan_Expenses__c = project.ITPM_CUR_Total_Plan_Expenses__c + costItem.ITPM_FOR_Total_expense__c;
                    project.ITPM_CUR_Total_Plan_Investment__c = project.ITPM_CUR_Total_Plan_Investment__c + costItem.ITPM_FOR_Total_investment__c; 

                }else if(costItem.ITPM_CI_SEL_Version__c=='Actual'){                           
                    
                    project.ITPM_CUR_Total_Real_Expenses__c = project.ITPM_CUR_Total_Real_Expenses__c + costItem.ITPM_FOR_Total_expense__c;
                    project.ITPM_CUR_Total_Real_Investment__c = project.ITPM_CUR_Total_Real_Investment__c + costItem.ITPM_FOR_Total_investment__c; 
                }
                else if(costItem.ITPM_CI_SEL_Version__c=='Estimated'){                           
                    
                    project.ITPM_CUR_Total_Estimated_Expenses__c = project.ITPM_CUR_Total_Estimated_Expenses__c + costItem.ITPM_FOR_Total_expense__c;
                    project.ITPM_CUR_Total_Estimated_Investment__c = project.ITPM_CUR_Total_Estimated_Investment__c + costItem.ITPM_FOR_Total_investment__c; 
                }


            }

            if (UserInfo.getDefaultCurrency() != project.CurrencyIsoCode){

                //costes plan          
                project.ITPM_CUR_Total_Plan_Expenses__c = GlobalUtils.ConvertCurrencyComp(mapaCurrencys, UserInfo.getDefaultCurrency(), project.CurrencyIsoCode, project.ITPM_CUR_Total_Plan_Expenses__c).setscale(2);
                project.ITPM_CUR_Total_Plan_Investment__c = GlobalUtils.ConvertCurrencyComp(mapaCurrencys, UserInfo.getDefaultCurrency(), project.CurrencyIsoCode, project.ITPM_CUR_Total_Plan_Investment__c).setscale(2);

                //costes reales
                project.ITPM_CUR_Total_Real_Expenses__c = GlobalUtils.ConvertCurrencyComp(mapaCurrencys, UserInfo.getDefaultCurrency(), project.CurrencyIsoCode, project.ITPM_CUR_Total_Real_Expenses__c).setscale(2);
                project.ITPM_CUR_Total_Real_Investment__c  = GlobalUtils.ConvertCurrencyComp(mapaCurrencys, UserInfo.getDefaultCurrency(), project.CurrencyIsoCode, project.ITPM_CUR_Total_Real_Investment__c).setscale(2);

                //costes estimados
                project.ITPM_CUR_Total_Estimated_Expenses__c = GlobalUtils.ConvertCurrencyComp(mapaCurrencys, UserInfo.getDefaultCurrency(), project.CurrencyIsoCode, project.ITPM_CUR_Total_Estimated_Expenses__c).setscale(2);
                project.ITPM_CUR_Total_Estimated_Investment__c  = GlobalUtils.ConvertCurrencyComp(mapaCurrencys, UserInfo.getDefaultCurrency(), project.CurrencyIsoCode, project.ITPM_CUR_Total_Estimated_Investment__c).setscale(2);

            }
            
            listProjectsActualizar.add(project);
        }
        return listProjectsActualizar;

    }
    


    public static void removeCostItemsAuthorized(){                            
        try{    
            delete [select id from ITPM_Budget_Item__c where ITPM_BLN_Pendiente_eliminar__c = true];            
        }catch(Exception e){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error. Contact your system administrator.');
                ApexPages.addMessage(msg);
        }
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, '');
        ApexPages.addMessage(msg);
    }
}