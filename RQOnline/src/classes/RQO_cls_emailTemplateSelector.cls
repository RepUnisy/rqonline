/**
*   @name: RQO_cls_emailTemplateSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la obtención de un template de email
*/
public with sharing class RQO_cls_emailTemplateSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_emailTemplateSelector';
	
	/**
    *   @name: RQO_cls_emailTemplateSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la lista de campos del objeto EmailTemplate
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			EmailTemplate.Id,
			EmailTemplate.DeveloperName,
			EmailTemplate.Body,
			EmailTemplate.Subject,
			EmailTemplate.HtmlValue };
    }

   	/**
    *   @name: RQO_cls_emailTemplateSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el sObjectTyoe de un EmailTemplate
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public Schema.SObjectType getSObjectType()
    {
        return EmailTemplate.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_emailTemplateSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar los EmailTemplates
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public override String getOrderBy()
	{
		return 'DeveloperName';
	}

	/**
    *   @name: RQO_cls_emailTemplateSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de objetos EmailTemplate mediante un conjunto de sus Ids correspondientes
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<EmailTemplate> selectById(Set<ID> idSet)
    {
         return (List<EmailTemplate>) selectSObjectsById(idSet);
    }
    
    /**
    *   @name: RQO_cls_emailTemplateSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de objetos EmailTemplate mediante una lista de developer names
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
   	public List<EmailTemplate> selectByDeveloperName(Set<String> developerNames)
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectByDeveloperName';
        
        string query = newQueryFactory().setCondition('DeveloperName IN :developerNames').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
	   	return (List<EmailTemplate>) Database.query(query);
    }
}