/**
*	@name: RQO_cls_UserService_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author:  Alfonso Constan López - Unisys
*	@description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_UserService.
*/
@isTest
public class RQO_cls_UserService_test {
	/**
    *	@name: RQO_cls_UserService_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author:  Alfonso Constan López - Unisys
    *	@description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    */
	@testSetup static void initialSetup() {
		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(u){
			// Crear email templates RQO_eplt_correoNuevoContactoSAC_ RQO_eplt_correoNuevoContactoMaster_
			RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_correoNuevoContactoSAC_en', 'RQO_eplt_correoNuevoContactoMaster_en'}); 
		}
	}
	
   	/**
    *	@name: RQO_cls_UserService_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author:  Alfonso Constan López - Unisys
    *	@description: Método de test que comprueba la consulta de un contacto.
    */
    private static testMethod void whenGetcontactReturnsData() {
    	
    	User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsEmail@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');

		System.runAs(userRunAs){
        	test.startTest();
			
            List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
            User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 'es',
                                                                'es_ES', 'Europe/Berlin', contact[0].Id);
            Contact contactResult = RQO_cls_UserService.getContact(u.Id);

		    test.stopTest();
            
            System.assertNotEquals(null,
    								contactResult, 
    								'No se recuperan los datos esperados');
    								
    		System.assertEquals(contact[0].Id,
    							contactResult.Id, 
    							'No se recuperan los datos esperados');
		}
    }
	
    /**
    *	@name: RQO_cls_UserService_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author:  Alfonso Constan López - Unisys
    *	@description: Método de test que comprueba la actualización del idioma de un usuario.
    */
    private static testMethod void whenUpdateLanguageReturnsOK() {
        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsEmail@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(userRunAs){
            test.startTest();
    
            Boolean DidThrowException = false;
            
            try {
                Id userId = fflib_IDGenerator.generate(User.SObjectType);
                    
                RQO_cls_UserService.updateLanguage(userId, 'en_US');
            }
            catch (Exception e) {
                DidThrowException = true;
            }
            
            System.assertEquals(false,
                                DidThrowException, 
                                'Se ha generado una excepción no esperada.');
            
            test.stopTest();
        }
    }
    
    /**
    *	@name: RQO_cls_UserService_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author:  Alfonso Constan López - Unisys
    *	@description: Método de test que comprueba el envío de correo de registro de nuevo contacto.
    */
    private static testMethod void whenSendNewRegistrationEmailReturnsOK() {
    	
    	User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsEmail@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		Boolean DidThrowException = false;
        
		String exceptionMessage = '';
		System.runAs(userRunAs){
        	test.startTest();
			
	    	try {
	    		List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
				User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 'es',
	                                                   'es_ES', 'Europe/Berlin', contact[0].Id);
				RQO_cls_UserService.sendNewRegistrationEmail(u.contactId);
	        }
		    catch (Exception e) {
                exceptionMessage = e.getMessage();
		        DidThrowException = true;
		    }
		    test.stopTest();
		}
		
		System.assertEquals(false,
    						DidThrowException, 
    						'Se ha generado una excepción no esperada. ' + exceptionMessage);
    }
    
    /**
    *	@name: RQO_cls_UserService_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author:  Alfonso Constan López - Unisys
    *	@description: Método de test que comprueba el envío de correo de registro en formato Bulk..
    */
    private static testMethod void whenSendNewRegistrationEmailsReturnsOK() {
    	
    	User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsEmail@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		Boolean DidThrowException = false;
        
		String exceptionMessage = '';
		System.runAs(userRunAs){
        	test.startTest();
			
	    	try {
	    		List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
				User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 'es',
	                                                   'es_ES', 'Europe/Berlin', contact[0].Id);
				RQO_cls_UserService.sendNewRegistrationEmails(new Set<Id>{u.contactId});
	        }
		    catch (Exception e) {
                exceptionMessage = e.getMessage();
		        DidThrowException = true;
		    }
		    test.stopTest();
		}
		
		System.assertEquals(false,
    						DidThrowException, 
    						'Se ha generado una excepción no esperada. ' + exceptionMessage);
    }
    
    /**
    *	@name: RQO_cls_UserService_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author:  Alfonso Constan López - Unisys
    *	@description: Método de test que comprueba la baja de un usuario.
    */
    private static testMethod void whenUnregisterReturnsOK() {
        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsEmail@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(userRunAs){
            test.startTest();
    
            Boolean DidThrowException = false;
            
            try {
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 'es',
                                                       'es_ES', 'Europe/Berlin', contact[0].Id);
                RQO_cls_UserService.unregister(u.Id);
            }
            catch (Exception e) {
                DidThrowException = true;
            }
            
            System.assertEquals(true,
                                DidThrowException, 
                                'Se ha generado una excepción no esperada.');
            
            test.stopTest();
        }
    }
    
    /**
    *	@name: RQO_cls_UserService_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author:  Alfonso Constan López - Unisys
    *	@description: Método de test que comprueba la obtención de direcciones de email.
    */
    private static testMethod void whenGetAddressesReturnsZero() {
    	
    	User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsEmail@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(userRunAs){
        	test.startTest();
			List<RQO_cmt_informationMails__mdt> emails = 
	       		new RQO_cls_informationMailsSelector().selectMail(new Set<String> { 'WrongProfile' }, 
	       													  	new Set<String> { 'WrongProduct' }, 
	       													  	new Set<String> { 'WrongTopic' });
		    test.stopTest();
		    
			System.assertEquals(0,
    							emails.size(), 
    							'Se han obtenido resultados no esperados.');
		}
    }
    
    /**
    *	@name: RQO_cls_UserService_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author:  Alfonso Constan López - Unisys
    *	@description: Método de test que comprueba la consulta de direcciones de correo.
    */
    private static testMethod void whenGetAddressesReturnsData() {
    	
    	User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsEmail@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');

		System.runAs(userRunAs){
        	test.startTest();
			
            List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
            User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 'es',
                                                                'es_ES', 'Europe/Berlin', contact[0].Id);
            List<String> addresses = RQO_cls_UserService.getAddresses(u);

		    test.stopTest();
            
            System.assertNotEquals(0,
    						addresses.size(), 
    						'No se recuperan los datos esperados');
		}
    }
}