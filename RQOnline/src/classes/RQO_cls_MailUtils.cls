/**
 *	@name: RQO_cls_MailUtils
 *	@version: 1.0
 *	@creation date: 31/01/2018
 *	@author: David Iglesias - Unisys
 *	@description: Clase de utilidades para tratamiento y envío de mails
 */
public class RQO_cls_MailUtils {

    // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_MailUtils';    
    
    /**
	* @creation date: 11/12/2017
	* @author: David Iglesias - Unisys
	* @description:	 Método para el envio de mails 
	* @param:	templateType		Tipo de template a utilizar para el contenido	
    * @param:	objectId			Objeto para nutrir el mail
    * @param:	contactIdList		Contactos a enviar el mail
	* @return 	Messaging.SendEmailResult[]
	* @exception: 
	* @throws: 
	*/
    public static Messaging.SendEmailResult[] sendMail(List<RQO_cls_IntegrationEmailBean> emailBeanList) {
        
        /**
        * Constantes
        */
        final String METHOD = 'sendMail';
        
        /**
		 * Variables
		 */
        Messaging.SingleEmailMessage mail = null;
        Messaging.SendEmailResult[] resultMail = null;
        List<Messaging.SingleEmailMessage> mailList = null;
        Map<String, Id> idTemplateByNameMap = new Map<String, Id> ();
        Set<String> templateTypeSet = new Set<String> ();
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        try {
            
            mailList = new List<Messaging.SingleEmailMessage> ();
            
            for (RQO_cls_IntegrationEmailBean item : emailBeanList) {
                templateTypeSet.add(item.templateName);
            }
            System.debug(CLASS_NAME + ' - ' + METHOD + ': idTemplateByNameMap '+ templateTypeSet);
            // Obtención de la plantilla del mail
            for (EmailTemplate itemTemplate: [SELECT Id, Name FROM EmailTemplate WHERE Name IN :templateTypeSet]) {
                idTemplateByNameMap.put(itemTemplate.Name, itemTemplate.Id);
            }
             System.debug(CLASS_NAME + ' - ' + METHOD + ': idTemplateByNameMap '+ idTemplateByNameMap);            
            for (RQO_cls_IntegrationEmailBean item : emailBeanList) {
                
                mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(idTemplateByNameMap.get(item.templateName));
                mail.setSaveAsActivity(false);
                mail.setWhatId(item.idObject);
                mail.setTargetObjectId(item.contact);
                             
                // Specify the address used when the recipients reply to the email. 
                mail.setReplyTo('noreply@repsol.com');
                             
                // Specify the name used as the display name.
                mail.setSenderDisplayName('Repsol Química Online');
                
                mailList.add(mail);
                
            }
			
            resultMail = Messaging.sendEmail(mailList, false);
			System.debug(CLASS_NAME + ' - ' + METHOD + ': resultMail '+ resultMail);
        } catch (Exception e) {
			System.debug(CLASS_NAME + METHOD + 'ERROR: ' + e.getMessage());
            // TODO --> Generar log de errores
		}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return resultMail;

    }
    
    /**
	* @creation date: 24/01/2018
	* @author: David Iglesias - Unisys
	* @description:	 Método para la generación del body custom del mail 
	* @param:	strHtmlBody			Template a utilizar para el contenido	
    * @param:	solicitud			Identificador del Request Management
	* @return 	String
	* @exception: 
	* @throws: 
	*/
    public static String fillHtmlBody (String strHtmlBody, String solicitud, String idSolicitud, String language) {
        
        /**
        * Constantes
        */
        final String METHOD = 'fillHtmlBody';
		final String REQUEST_MANAGEMENT = '{RQO_REQUEST_MANAGEMENT}';
        
        /**
		 * Variables
		 */
        String strFinalBody = '';
		String strTableBody = '';
		Map<String, List<RQO_cls_WrapperPedido>> wrapper = null;
		Set<String> accountDestinosIdSet = new Set<String> ();
		Map<String, Account> destinoByIdMap = null;
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        try {
			
			// Wrapper para la tabla dinámica de resumen
			wrapper = RQO_cls_GestionPedido_cc.getPedidosResumenWithoutSerialize(idSolicitud, false, language);
            system.debug('wrapper: '+wrapper);
			// Obtención del binomio Id - Destino de Mercancias/Facturación referenciados en la solicitud
			destinoByIdMap = getDestinosById(wrapper);
			
			// Construcción del body
			strFinalBody = strHtmlBody;
            system.debug('XXXX: '+strFinalBody);
			// Request Management
			strFinalBody = strFinalBody.replace(REQUEST_MANAGEMENT, solicitud);
			system.debug('strFinalBody: '+strFinalBody);
			// Iteración de los destinos a enviar
			for (String item : wrapper.keySet()) {
				Decimal sumToneladas = 0;
				Integer idCont = 0;
				String destFacturacion;
				
                // Tratamiento de la parte superior de la tabla enviar
				strTableBody = strTableBody + RQO_cls_Constantes.TOP_MAIL;
				
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_1, destinoByIdMap.get(item).Name);
				//strTableBody = strTableBody.Replace(PARAM_2, wrapper.get(item).get(0).incoterm);
				system.debug('strTableBody: '+strTableBody);				
				// Iteración de las posiciones de pedido a enviar por destino
				for (RQO_cls_WrapperPedido itemAux : wrapper.get(item)) {
					
                    // Tratamiento de la parte intermedia de la tabla enviar
					strTableBody = strTableBody + RQO_cls_Constantes.MIDDLE_MAIL;
					
					strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_2, String.valueOf(++idCont));
					strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_3, (itemAux.getNumRefCliente()!= null)?itemAux.getNumRefCliente():'');
					strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_4, (itemAux.getGrado()!= null)?itemAux.getGrado():'');
					strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_5, (itemAux.getEnvaseName()!= null)?itemAux.getEnvaseName():'');
					strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_6, (itemAux.getCantidad() != null)?RQO_cls_FormatUtils.formatDecimal(itemAux.getCantidad()):'');
					//strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_7, (itemAux.getModoEnvioName()!= null)?itemAux.getModoEnvioName():'');
					strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_7, RQO_cls_Constantes.REQ_METER_UNIT_KG);
                    strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_8, (itemAux.getFechaPreferenteEntrega()!=null)?String.valueOf(itemAux.getFechaPreferenteEntrega()):'');
					strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_9, (itemAux.getIncoterm()!= null)?itemAux.getIncoterm():'');
					strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_10, RQO_cls_FormatUtils.formatDecimal(itemAux.getCantidad()/1000));
					
					// Contador de toneladas totales por destino de mercancias
					sumToneladas = sumToneladas + itemAux.getCantidad()/1000;
					
					// Destino de facturación para detalle del pedido
					destFacturacion = itemAux.getDireccionFacturacion();
                    					
				}				
				system.debug('strTableBody2: '+strTableBody);
                // Tratamiento de la parte inferior de la tabla enviar
				strTableBody = strTableBody + RQO_cls_Constantes.BOTTOM_MAIL;
				
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_11, RQO_cls_FormatUtils.formatDecimal(sumToneladas));
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_12, (destinoByIdMap.get(item).RQO_fld_name3__c != null)?destinoByIdMap.get(item).RQO_fld_name3__c:'');
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_13, (destinoByIdMap.get(item).RQO_fld_calleNumero__c != null)?destinoByIdMap.get(item).RQO_fld_calleNumero__c:'');
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_14, (destinoByIdMap.get(item).RQO_fld_poblacion__c != null)?destinoByIdMap.get(item).RQO_fld_poblacion__c:'');
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_15, (destinoByIdMap.get(destFacturacion).RQO_fld_name3__c != null)?destinoByIdMap.get(destFacturacion).RQO_fld_name3__c:'');
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_16, (destinoByIdMap.get(destFacturacion).RQO_fld_calleNumero__c != null)?destinoByIdMap.get(destFacturacion).RQO_fld_calleNumero__c:'');
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_17, (destinoByIdMap.get(destFacturacion).RQO_fld_poblacion__c != null)?destinoByIdMap.get(destFacturacion).RQO_fld_poblacion__c:'');
				system.debug('strTableBody3: '+strTableBody);
			}

			strFinalBody = strFinalBody.replace(RQO_cls_Constantes.TABLE, strTableBody);
            system.debug('strFinalBody: '+strFinalBody);
			
        } catch (Exception e) {
			System.debug(CLASS_NAME + METHOD + ' ERROR: ' + e.getMessage()+' - ' +e.getLineNumber());
            // TODO --> Generar log de errores
		}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return strFinalBody;

    }
	
	/**
	* @creation date: 24/01/2018
	* @author: David Iglesias - Unisys
	* @description:	 Método para la generación del body custom del mail de resumen diario
	* @param:	strHtmlBody			Template a utilizar para el contenido	
    * @param:	solicitud			Identificador del Request Management
	* @return 	String
	* @exception: 
	* @throws: 
	*/
    public static String fillHtmlBodyResume (String strHtmlBody, List<RQO_obj_notification__c> notificationCursoList, List<RQO_obj_notification__c> notificationConfirmadoList, 
												Map<String, String> posPedidoDatosMap, Map<String, String> entregaDatosMap, String languageTemplate) {
		
		/**
        * Constantes
        */
        final String METHOD = 'fillHtmlBodyResume';
		final String REQUEST_MANAGEMENT = '{RQO_REQUEST_MANAGEMENT}';
		
        
        /**
		 * Variables
		 */
        String strFinalBody = '';
		String strTableBody = '';
		Map<String, List<RQO_cls_WrapperPedido>> wrapper = null;
		Set<String> accountDestinosIdSet = new Set<String> ();
		Map<String, Account> destinoByIdMap = null;
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //try {
			
			// Construcción del body
			strFinalBody = strHtmlBody;
			
			// Tratamiento de la parte intermedia de la tabla con las notificaciones de estado "En curso"
			for (RQO_obj_notification__c item : notificationCursoList) {
				strTableBody = strTableBody + RQO_cls_Constantes.MIDDLE_MAIL_RESUME;
                strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_1, RQO_cls_GeneralUtilities.translate('RQO_lbl_pedidosPer', languageTemplate));
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_2, RQO_cls_GeneralUtilities.translate('RQO_lbl_mensajeenCurso', languageTemplate));
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_3, posPedidoDatosMap.get(item.RQO_fld_externalId__c));
                strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_4, RQO_cls_GeneralUtilities.translate('RQO_lbl_verMas', languageTemplate));
			}
			
			// Tratamiento de la parte intermedia de la tabla con las notificaciones de estado "Confirmado"
			for (RQO_obj_notification__c item : notificationConfirmadoList) {
				strTableBody = strTableBody + RQO_cls_Constantes.MIDDLE_MAIL_RESUME;
                strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_1, RQO_cls_GeneralUtilities.translate('RQO_lbl_pedidosPer', languageTemplate));
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_2, RQO_cls_GeneralUtilities.translate('RQO_lbl_entregaConfirmado', languageTemplate));
				strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_3, entregaDatosMap.get(item.RQO_fld_externalId__c));
                strTableBody = strTableBody.Replace(RQO_cls_Constantes.PARAM_4, RQO_cls_GeneralUtilities.translate('RQO_lbl_verMas', languageTemplate));
			}
			
			strFinalBody = strFinalBody.replace(RQO_cls_Constantes.TABLE, strTableBody);
            system.debug('strFinalBody: '+strFinalBody);
			
		//} catch (Exception e) {
		//	System.debug(CLASS_NAME + METHOD + ' ERROR: ' + e.getMessage()+' - ' +e.getLineNumber());
            // TODO --> Generar log de errores
		//}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return strFinalBody;
		
	}
	
	/**
	* @creation date: 24/01/2018
	* @author: David Iglesias - Unisys
	* @description:	 Método para la obtención del binomio Id - Destino de Mercancias/Facturación 
	* @param:	wrapper					wrapper del objeto Pedido	
	* @return 	Map<String, String>
	* @exception: 
	* @throws: 
	*/
    private static Map<String, Account> getDestinosById (Map<String, List<RQO_cls_WrapperPedido>> wrapper) {
		
		/**
        * Constantes
        */
        final String METHOD = 'getDestinosById';
        
        /**
		 * Variables
		 */
		Map<String, Account> destinoByIdMap = new Map<String, Account> ();
        Set<String> accountDestinosIdSet = new Set<String> ();
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		// Obtención de los Id´s de los destinos asociados a las posiciones
		for (String item : wrapper.keySet()) {
			accountDestinosIdSet.add(item);
			for(RQO_cls_WrapperPedido itemAux : wrapper.get(item)) {
				accountDestinosIdSet.add(itemAux.getDireccionFacturacion());
			}
		}
		
		// Obtención de las direcciones de envío
		for (Account item : [SELECT Id, Name, RQO_fld_name3__c, RQO_fld_calleNumero__c, RQO_fld_poblacion__c, RQO_fld_pais__c FROM Account WHERE Id IN :accountDestinosIdSet]) {
			destinoByIdMap.put(item.Id, item);
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return destinoByIdMap;
		
	}
}