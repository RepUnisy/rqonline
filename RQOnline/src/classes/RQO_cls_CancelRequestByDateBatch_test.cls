/**
*	@name: RQO_cls_CancelRequestByDateBatch_test
*	@version: 1.0
*	@creation date: 06/03/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar el batch RQO_cls_CancelRequestByDate_batch
*/
@IsTest
public class RQO_cls_CancelRequestByDateBatch_test {
    
	/**
	* @creation date: 06/03/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserCancelRequestBatch@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
            RQO_cls_TestDataFactory.createAsset(200);
        }
    }
    
    /**
	* @creation date: 06/03/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar la ejecución del batch
	* @exception: 
	* @throws: 
	*/
    static testMethod void ejecucionBatch(){
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserCancelRequestBatch@testorg.com');
        System.runAs(u){
            Recordtype rtType = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ASSET_REQUEST_POSITION];
            List<Asset> listaActivos = [Select Id from Asset LIMIT 200];
            Date fechaActual = System.today();
            for(Integer i=0; i<listaActivos.size();i++){
                listaActivos[i].RecordtypeId = rtType.Id;
                listaActivos[i].RQO_fld_positionStatus__c = RQO_cls_Constantes.PED_COD_STATUS_SOLICITADO;
                listaActivos[i].RQO_fld_cuDeliveryDate__c = fechaActual.addDays(-1);
            }
            update listaActivos;
            Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_SEND_ORDER, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
      		Test.startTest();
				Id batchId = Database.executeBatch(new RQO_cls_CancelRequestByDate_batch(), 200);
            Test.stopTest();
            System.assertNotEquals(null, batchId);
        }
	}
}