/**
*	@name: RQO_cls_DetalleProducto_cc
*	@version: 1.0
*	@creation date: 05/09/2017
*	@author: Alvaro Alonso - Unisys
*	@description: 
Controlador Apex para recuperar las caracteristicas asociadas a un grado
en el componente Rqo_cmp_detalleproducto_2
*
*/
public with sharing class RQO_cls_DetalleProducto_cc {
    private static Map<Id, String> mapTranslate{get;set;}
    
    /**
    * @creation date: 05/09/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Recupera las caracteristicas asociadas al id de grado se que recibe por parametro
    * @param:	lenguaje tipo String, idioma actual del usuario. 
    * @Param:	idProducto tipo String, id del grado sobre el que recuperar las caracteristicas
    * @return:	Lista de objetos de tipo RQO_cls_DetalleProductoAuxObj con las características
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static List<RQO_cls_DetalleProductoAuxObj> getDetalleProducto(String lenguaje, String idProducto){
        List<RQO_cls_DetalleProductoAuxObj> listSpec = null;
        
        //Recuperamos las especificaciones a partir del id de grado.
        List<RQO_obj_specification__c> listaPrincipal = [
            SELECT
            	RQO_fld_value__c,
            	RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c, 
            	RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.name,
            	RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.RQO_fld_unitMeasure__c
            FROM
            	RQO_obj_specification__c
            WHERE
            	RQO_fld_grade__c =: idProducto
            ORDER BY
            	RQO_fld_catSpecification__r.RQO_fld_position__c ASC
            LIMIT 1000
        ];
        
        //Recorremos la lista de especificaciones. Esta lista se utiliza en dos puntos del controlador.
        if ( listaPrincipal != null && !listaPrincipal.isEmpty() ) {
            //Carga del mapa de traducciones que utilizamos más abajo 
            getTranslate(listaPrincipal, lenguaje);
            RQO_cls_DetalleProductoAuxObj objSpec = null;
            String actualTranslate = '';
            
            for (RQO_obj_specification__c obj : listaPrincipal) {
                // Se recoge la traducción actual del mapa generado con anterioridad.
                // Si el mapa está vacío o no se encuentra el dato por Id, se establece el name
                // de Master Specification
                actualTranslate = (
                    mapTranslate != null &&
                    false == mapTranslate.isEmpty() &&
                    mapTranslate.containsKey(obj.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c)
                ) ?
                    	(String) mapTranslate.get(obj.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c)
                    :
                		obj.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.name;
                
                if (listSpec == null) {
                    listSpec = new List<RQO_cls_DetalleProductoAuxObj>();
                }
                
                //Cargamos la lista a devolver con los datos Value, Descripción y Unidad de medida
                listSpec.add(
                    new RQO_cls_DetalleProductoAuxObj(
                        actualTranslate,
                        obj.RQO_fld_value__c,
                        obj.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.RQO_fld_unitMeasure__c
                    )
                );
            }
        }
        
        System.debug('listSpec:' + listSpec);
        
        return listSpec;
    }
    
    /**
    * @creation date: 05/09/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Carga un mapa de traducciones en el idioma actual del usuario.
    * @param: listaPrincipal tipo RQO_obj_specification__c, lista de especificaciones. 
    * @param: lenguaje tipo String
    * @return: void
    * @exception: 
    * @throws: 
    */
    private static void getTranslate(
        List<RQO_obj_specification__c> listaPrincipal,
        String lenguaje
    ) {
        List<Id> listMaster = new List<Id>();
        
        /*	Recorremos la lista principal para obtener la lista de ids de la tabla Master Specification. 
        *	Esta tabla contiene la descripción de la especificación
        */ 
        for (RQO_obj_specification__c obj : listaPrincipal) {
            listMaster.add(obj.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c);
        }
        
        //Consultamos las traducciones para cada master de especificación y seteamos el mapa
        for (RQO_obj_translation__c objTrans : [
            SELECT
            	Id,
            	RQO_fld_masterSpecification__c,
            	RQO_fld_detailTranslation__c,
            	RQO_fld_translation__c
            FROM
            	RQO_obj_translation__c
            WHERE
            	RQO_fld_masterSpecification__c =: listMaster
            AND
            	RQO_fld_locale__c =: lenguaje
        ]) {
            if (null == mapTranslate) {
                mapTranslate = new Map<Id, String>();
            }
            
            mapTranslate.put(objTrans.RQO_fld_masterSpecification__c, objTrans.RQO_fld_translation__c);
        }
    }
    
    /**
    * @creation date: 28/09/2017
    * @author: Alfonso Constán lópez - Unisys
    * @description:	Búsqueda de la descripción de un grado.
    * @param: language - idioma de la búsqueda
    * @param: idgrado - id del grado que se desea buscar
    * @return: descripción del grado
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static String getDetalleGradoDescripcion(String lenguaje, Id idgrado){
        String descripcion = '';
        
        for (RQO_obj_translation__c trad : [
            SELECT
            	RQO_fld_detailTranslation__c
            FROM
            	RQO_obj_translation__c
            WHERE
            	RQO_fld_grade__c =: idgrado
            AND
            	RQO_fld_locale__c =: lenguaje
            LIMIT 1
        ]) {
            descripcion = trad.RQO_fld_detailTranslation__c ;
        }
        
        return descripcion;
    }
    
    /**
    * @creation date: 28/09/2017
    * @author: Alfonso Constán lópez - Unisys
    * @description:	Búsqueda de la descripción de un grado.
    * @param: language - idioma de la búsqueda
    * @param: idgrado - id del grado que se desea buscar
    * @return: descripción del grado
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static Map<String, List<RQO_obj_translation__c>> getDetalleGradoCaracteristicas(
        String lenguaje,
        Id idgrado
    ) {
        
        Map<String, List<RQO_obj_translation__c>> mapCaracteristicas = new Map<String, List<RQO_obj_translation__c>>();
        List<Id> listIdCategory = new List<Id>();
        
        mapCaracteristicas.put(Label.RQO_lbl_Segmentos, new List<RQO_obj_translation__c>());
        mapCaracteristicas.put(Label.RQO_lbl_Aplicaciones, new List<RQO_obj_translation__c>());
        
        List<RQO_obj_features__c> listCaract = [
            SELECT
            	RQO_fld_category__r.Id
            FROM
            	RQO_obj_features__c
            WHERE
            	RQO_fld_grade__c =: idgrado
        ];
        
        for (RQO_obj_features__c carac : listCaract) {
            listIdCategory.add(carac.RQO_fld_category__r.Id);
        }
        
        List<RQO_obj_translation__c> listCaracteristicas = [
            SELECT
                Id,
                RQO_fld_category__r.RQO_fld_imagenCategoria__c, 
                RQO_fld_translation__c,
                RQO_fld_category__r.RQO_fld_type__c
            FROM
            	RQO_obj_translation__c
            WHERE
            	RQO_fld_category__c in :listIdCategory
            AND
            	RQO_fld_locale__c =: lenguaje
        ];
        
        
        for (RQO_obj_translation__c trad : listCaracteristicas) {
            if (trad.RQO_fld_category__r.RQO_fld_type__c == Label.RQO_lbl_Segmentos) {
                mapCaracteristicas.get(Label.RQO_lbl_Segmentos).add(trad);
            } else if (trad.RQO_fld_category__r.RQO_fld_type__c == Label.RQO_lbl_Aplicaciones) {
                mapCaracteristicas.get(Label.RQO_lbl_Aplicaciones).add(trad);                
            }
        }
        
        system.debug('Mapa de características: ' + mapCaracteristicas);
        
        return mapCaracteristicas;
    }
    
}