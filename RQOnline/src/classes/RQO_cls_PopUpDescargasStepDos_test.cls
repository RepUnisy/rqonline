/**
*	@name: RQO_cls_UploadDocument_test
*	@version: 1.0
*	@creation date: 26/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar los métodos asociados a PopUpDescargasStepDos
*/
@IsTest
public class RQO_cls_PopUpDescargasStepDos_test {
    
    /**
    *	@name: RQO_cls_UploadDocument_test
    *	@version: 1.0
    *	@creation date: 26/2/2018
    *	@author: Juan Elías - Unisys
    *	@description: Método para inicializar el conjunto de datos necesario para la ejecución del test
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        
    }
    /**
    * @creation date: 26/02/2018
    * @author: Juan Elías - Unisys
    * @description:	Método test para probar el método getDocumentData
    * @exception: 
    * @throws: 
    */
    @isTest
    static void testUploadDocToRepository(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){  
            
            List<Account> listAccount = RQO_cls_TestDataFactory.createAccount(200);
            String idCliente = listAccount[0].Id;
            String esperado = 'numDocumento';
            test.startTest();
            String[] resultado = RQO_cls_PopUpDescargasStepDos_cc.getDocumentData(idCliente, 'numDocumento', 'stipo');
            System.assertEquals(esperado, resultado[3]);
            test.stopTest();
        }
    }
    
}