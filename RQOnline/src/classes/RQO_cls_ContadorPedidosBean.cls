/**
*   @name: RQO_cls_ContadorPedidosBean
*   @version: 1.0
*   @creation date: 29/01/2017
*   @author: Alvaro Alonso - Unisys
*   @description: Bean Apex para contar los pedidos
*	@testClass: 
*/
public class RQO_cls_ContadorPedidosBean {
    public String executionCode{get;set;}
    public String executionMessage{get;set;}
	public Integer contadorRegistrado{get;set;}
    public Integer contadorEnCurso{get;set;}
    public Integer contadorConfirmado{get;set;}
    public Integer contadorEnTransito{get;set;}
}