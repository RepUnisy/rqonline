/**
*   @name: RQO_cls_NotificationCategorySelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description:Clase que gestiona la selección de categorías de notificaciones
*/
public with sharing class RQO_cls_NotificationCategorySelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_NotificationCategorySelector';
	
    /**
    *   @name: RQO_cls_NotificationCategorySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Obtiene la lista de campos del mdt "NotificationCaegories"
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_cmt_notificationCategories__mdt.RQO_fld_notificationType__c,
			RQO_cmt_notificationCategories__mdt.RQO_fld_notificationConsent__c};
    }
    
    /**
    *   @name: RQO_cls_NotificationCategorySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Obtiene la lista de campos del mdt "NotificationCaegories"
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_cmt_notificationCategories__mdt.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_NotificationCategorySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo a través del cual ordenar
    */
    public override String getOrderBy()
	{
		return 'RQO_fld_notificationType__c';
	}

    /**
    *   @name: RQO_cls_NotificationCategorySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista completa de todas las NotificationCategories (custom metadata type)
    */
    public List<RQO_cmt_notificationCategories__mdt> selectAll()
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'RQO_cmt_notificationCategories__mdt';
        
        string query = newQueryFactory().toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
 		List<RQO_cmt_notificationCategories__mdt> consents = (List<RQO_cmt_notificationCategories__mdt>) 
														            Database.query(query); 
		return consents;
										                
    }
}