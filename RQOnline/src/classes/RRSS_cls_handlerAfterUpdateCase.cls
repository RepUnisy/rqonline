/**
 * @name: RRSS_cls_handlerAfterUpdateCase
 * @version: 1.0
 * @creation date: 14/06/2017
 * @author: Ramon Diaz, Accenture
 * @description: Clase Handler para Trigger After Update de Casos - Completa el milestone correspondiente
 *               además de, en caso de modificar el Account relacionado del caso, actualizar dicho Account
 *               en el registro de Social Persona correspondiente
*/

public class RRSS_cls_handlerAfterUpdateCase {
    
    /* @creation date: 14/06/2017
     * @author: Ramon Diaz, Accenture
     * @description: Método handler que completa el Milestone correspondiente según estado para casos de RRSS
     *               El método también modifica el Account relacionado con el Social Persona en caso de modificación
     *               del Account en el caso
     * @param: Trigger.new y Trigger.old en las variables newCases y oldCases
     * @return: void
     * @exception: No aplica
     * @throws: No aplica */
    public static void handlerAfterUpdateCase(List<Case> newCases, List<Case> oldCases) {
        System.debug('::::::::: Parámetro newCases:' + newCases + ':::::::::');
        System.debug('::::::::: Parámetro oldCases:' + oldCases + ':::::::::');
        // Preparación de las listas para completar milestones automáticamente
        List<Id> casesToUpdate = new List<Id>();
        List<Id> casesToUpdateReapertura = new List<Id>();
        List<string> milestones = new List<string>();
        // Recuperación de los Social Persona y Social Posts para actualizar en caso de cambio de Account
        Map<Id,SocialPersona> mExistingSocialPersona = new Map<Id,SocialPersona>([Select Id, ParentId FROM SocialPersona]);
        Map<Id,SocialPersona> mSocialPersona = new Map<Id,SocialPersona>();
        List<SocialPost> existingSocialPosts = [Select Id, PersonaId FROM SocialPost];
        Map<Id,SocialPost> mSocialPosts = new Map<Id,SocialPost>(existingSocialPosts);  
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.RRSS_RecordType).getRecordTypeId();
        // Inicializamos una variable contador para apuntar hacia el registro correspondiente entre newCases y oldCases
        Integer indice = 0;
        for (Case c : [Select Id, AccountId, RecordTypeId,rrss_Aprobado__c,rrss_Nueva_Repuesta__c, Status,rrss_Primera_Respuesta__c, SourceId FROM Case WHERE Id IN : newCases]) {     
            Case oldCase = oldCases[indice];
            // Verificamos que el caso tenga el RecordType de RRSS y que el Source no esté vacío
            if (c.RecordTypeId == recordTypeId && c.SourceId != null) {
                SocialPost p = mSocialPosts.get(c.SourceId);
                // Si el AccountId ha cambiado, modificaremos el Social Persona. Para ello, lo incluimos en una lista mSocialPersona
                if (mSocialPersona.get(p.PersonaId) == null && p.PersonaId != null && oldCase.AccountId != c.AccountId) {
                    SocialPersona sp = mExistingSocialPersona.get(p.PersonaId);
                    sp.ParentId = c.AccountId;
                    mSocialPersona.put(p.PersonaId, sp);
                }
            }
            // Si el estado del caso o el flag de Nueva Respuesta ha cambiado verificaremos si se puede completar
            // un Milestone. Si es así, lo añadiremos a la lista correspondiente
            if ((oldCase.status != c.status || oldCase.rrss_Nueva_Repuesta__c != c.rrss_Nueva_Repuesta__c)&& c.status != Label.RRSS_CaseStatus_Closed) {
            //if ((oldCase.status != c.status || oldCase.rrss_Nueva_Repuesta__c != c.rrss_Nueva_Repuesta__c)) {
                System.debug('Olcase estado: ' + oldCase.status);
                System.debug('Case estado: ' + c.status);
                System.debug('Label pendiente de aprobacion: ' + Label.RRSS_Pendiente_de_aprobacion);                
                if (oldCase.status == Label.RRSS_Escalado_a_cliente) {
                    casesToUpdate.add(c.Id);
                    milestones.add(Label.RRSS_Milestone_Escalado_a_Cliente);
                }
                
                if (oldCase.status == Label.RRSS_Escalado_a_negocio) {
                    casesToUpdate.add(c.Id);
                    milestones.add(Label.RRSS_Milestone_Escalado_a_Negocio);
                }
                
                if (oldCase.status == Label.RRSS_Pendiente_de_aprobacion) {
                    System.debug('Milestone name añadido: ' + Label.RRSS_Milestone_Aprobacion);
                    casesToUpdate.add(c.Id); 
                    milestones.add(Label.RRSS_Milestone_Aprobacion);
                }
                
                if(oldCase.rrss_Nueva_Repuesta__c && !c.rrss_Nueva_Repuesta__c) {
                    System.debug('Milestone name añadido: ' + Label.RRSS_Milestone_Segunda_Respuesta);
                    casesToUpdate.add(c.Id);
                    milestones.add(Label.RRSS_Milestone_Segunda_Respuesta);
                }
                
                if (oldCase.status == Label.RRSS_CaseStatus_Resuelto && c.Status != Label.RRSS_CaseStatus_Closed) {
                    casesToUpdateReapertura.add(c.Id);
                }
                
                if (c.rrss_Primera_Respuesta__c == true) {
                    System.debug('Milestone name añadido: ' + Label.RRSS_Milestone_Primera_Respuesta);
                    casesToUpdate.add(c.Id);
                    milestones.add(Label.RRSS_Milestone_Primera_Respuesta);
                }
                
                if (c.Status == Label.RRSS_CaseStatus_Resuelto || c.Status == Label.RRSS_CaseStatus_Closed) {
                    casesToUpdate.add(c.Id);
                    milestones.add(Label.RRSS_Milestone_Resolucion);
                }
            }
            indice++;
        }      
        RRSS_cls_MilestoneUtils.completeMilestone(casesToUpdate, milestones, System.now());                         
        // Para cada lista, actualizamos los valores o invocamos el método de la clase de Milestones para completarlos
        if (mSocialPersona.size() > 0) {
            update mSocialPersona.values();
        }                 
        // En el caso de reapertura de casos, debemos modificar el Completion Date del Milestone directamente
        if (casesToUpdateReapertura.size()>0) {
            List<CaseMilestone> cmsToUpdate = [select Id, completionDate
            from CaseMilestone cm
            where caseId in :casesToUpdateReapertura and cm.MilestoneType.Name=:Label.RRSS_Milestone_Resolucion
            and completionDate != null limit 1];
            if (cmsToUpdate.isEmpty() == false){
                for (CaseMilestone cm : cmsToUpdate){
                    cm.completionDate = null;
                    }
                update cmsToUpdate;
            }
        }
    }
}