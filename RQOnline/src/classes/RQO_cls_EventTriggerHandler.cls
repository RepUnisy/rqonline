/**
 *	@name: RQO_cls_EventTriggerHandler
 *	@version: 1.0
 *	@creation date: 17/11/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase handler con la lógica asociada al trigger para el objeto RQO_obj_event__c
 *	@testClass: 
*/
public with sharing class RQO_cls_EventTriggerHandler {
    

    private static RQO_cls_EventTriggerHandler instance = null;
	/**
	* @creation date: 17/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método para instanciar el handler desde el trigger 
	* @return: RQO_cls_EventTriggerHandler
	* @exception: 
	* @throws: 
	*/   
    public static RQO_cls_EventTriggerHandler getInstance(){
        if(instance == null) instance = new RQO_cls_EventTriggerHandler();
        return instance;
    }

    private List<Id> faqIdList = null;
    private List<Id> docIdList = null;
    
    /**
	* @creation date: 17/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método generico que realiza la lógica necesaria del trigger antes de insertar el registro 
	* @param: listData tipo List<RQO_obj_event__c>, lista de objetos que se van a insertar
	* @return: 
	* @exception: 
	* @throws: 
	*/   
    public void beforeInsertExecution(List<RQO_obj_event__c> listData){
        //Recuparamos las listas de datos iniciales que vamos a necesitar para realizar consultas y traernos datos de los padres del objeto
        this.setListData(listData);
        //Cargamos los mapas de datos de los objetos documento y faq que recuperaremos (de ser necesario) a partir de las listas generadas en el método anterior
        Map<Id, RQ0_obj_Facs__c> mapFaqs = this.getFaqsMap();
        Map<Id, RQO_obj_document__c> mapDocument = this.getDocMap();
        //Recorremos el mapa de datos principal que nos llega desde el trigger
        for (RQO_obj_event__c objEventAux : listData){
            //Verificamos si tiene informado el lookup de Faq y de ser así vamos a buscar los datos del objeto FAQ a su mapa. Si tiene dato en el campo
            //topic lo arrastramos al objeto evento actual
            if (objEventAux.RQO_fld_eventFaq__c != null && mapFaqs != null && !mapFaqs.isEmpty() && mapFaqs.containsKey(objEventAux.RQO_fld_eventFaq__c)){
                objEventAux.RQO_fld_faqCategory__c = mapFaqs.get(objEventAux.RQO_fld_eventFaq__c).RQO_fld_topic__c;
            }
			//Verificamos si tiene informado el lookup de Documentos y de ser así vamos a buscar los datos del objeto Documento a su mapa. Si tiene dato en los campos
            //locale y type lo arrastramos al objeto evento actual
            if (objEventAux.RQO_fld_eventDocument__c != null && mapDocument != null && !mapDocument.isEmpty() && mapDocument.containsKey(objEventAux.RQO_fld_eventDocument__c)){
                objEventAux.RQO_fld_eventDocumentLocale__c = mapDocument.get(objEventAux.RQO_fld_eventDocument__c).RQO_fld_locale__c;
                objEventAux.RQO_fld_eventDocumentType__c = mapDocument.get(objEventAux.RQO_fld_eventDocument__c).RQO_fld_type__c;
                //Si  no hubiese llegado el grado al componente a la hora de registrar el documento lo establecemos desde aquí.
                if (objEventAux.RQO_fld_eventGrade__c == null){
                    objEventAux.RQO_fld_eventGrade__c = mapDocument.get(objEventAux.RQO_fld_eventDocument__c).RQO_fld_grade__c;
                }
            }
        }
    }
    
    /**
	* @creation date: 17/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera las listas de Ids de los padres del objeto RQO_obj_event__c para luego poder consultarlos
	* @param: mapData tipo Map<Id, RQO_obj_event__c>, mapa de objetos que se van a insertar
	* @return: 
	* @exception: 
	* @throws: 
	*/  
    private void setListData(List<RQO_obj_event__c> listData){
        for (RQO_obj_event__c objEventAux : listData){
            //Verificamos si tiene informado el lookup de Faq y de ser así lo añadimos a su lista de ids
            if (objEventAux.RQO_fld_eventFaq__c != null){
                if (faqIdList == null){faqIdList = new List<Id>();}
                faqIdList.add(objEventAux.RQO_fld_eventFaq__c);
            }
            //Verificamos si tiene informado el lookup de Documentos y de ser así lo añadimos a su lista de ids
            if (objEventAux.RQO_fld_eventDocument__c != null){
                if (docIdList == null){docIdList = new List<Id>();}
                docIdList.add(objEventAux.RQO_fld_eventDocument__c);
            }
        }
    }
    
	/**
	* @creation date: 17/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera el mapa de datos de FAQs a partir de la lista de ids de faq
	* @return: 
	* @exception: 
	* @throws: 
	*/ 
    private Map<Id, RQ0_obj_Facs__c> getFaqsMap(){
        if (faqIdList != null){
            return new Map<Id, RQ0_obj_Facs__c>([Select Id, RQO_fld_topic__c from RQ0_obj_Facs__c where Id = : faqIdList]);
        }
        return null;
    }
    
	/**
	* @creation date: 17/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera el mapa de datos de Documentos a partir de la lista de ids de documento
	* @return: 
	* @exception: 
	* @throws: 
	*/ 
	private Map<Id, RQO_obj_document__c> getDocMap(){
        if (docIdList != null){
            return new Map<Id, RQO_obj_document__c>([Select Id, RQO_fld_grade__c, RQO_fld_type__c, RQO_fld_locale__c from RQO_obj_document__c where Id = : docIdList]);
        }
        return null;
    }
}