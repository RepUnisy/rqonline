/**
*   @name: RQO_cls_handlerAfterContact_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_handlerAfterContact asociada al trigger RQO_trg_Contact.
*/
@isTest
public class RQO_cls_handlerAfterContact_test {
	
    /**
    *   @name: RQO_cls_handlerAfterContact_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    */
	@testSetup static void initialSetup() {
		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(u){
			// Crear email templates RQO_eplt_correoNuevoContactoMaster_en RQO_eplt_correoNuevoContactoSAC_en
			RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_correoNuevoContactoMaster_en', 'RQO_eplt_correoNuevoContactoSAC_en'}); 
		}
	}
	
    /**
    *   @name: RQO_cls_handlerAfterContact_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la no creación de consentimientos cuando  no se actualiza un contacto.
    */
    private static testMethod void whenhandlerAfterContactNotChangeConsents() {
        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserClienteRunAsJob@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(userRunAs){
            test.startTest();
    
            List<Contact> newContacts = RQO_cls_TestDataFactory.createContact(1);
            Map<Id, Contact> mapOldContacts = new Map<Id, Contact>();
            
            RQO_cls_handlerAfterContact.handlerAfterContact(true, newContacts, mapOldContacts);
            
            List<RQO_obj_notificationConsent__c> consents = new RQO_cls_NotificationConsentSelector().
                            selectByType(new Set<ID> { newContacts[0].Id }, RQO_cls_Constantes.COD_COM_MULTIMEDIA);
                            
            System.assertEquals(0,
                                consents.size(), 
                                'No devuelve los consentimientos correctos.');
    
            test.stopTest();
        }
    }
    
    /**
    *   @name: RQO_cls_handlerAfterContact_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la creación de consentimientos cuando se actualiza un contacto.
    */
    private static testMethod void whenhandlerAfterContactCreatesNewConsents() {
        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserClienteRunAsJob@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(userRunAs){
            test.startTest();
    
            List<Contact> newContacts = RQO_cls_TestDataFactory.createContact(1);
            newContacts[0].RQO_fld_techInfConsent__c = true;
            
            RQO_cls_handlerAfterContact.handlerAfterContact(true, newContacts, null);
            
            List<RQO_obj_notificationConsent__c> consents = new RQO_cls_NotificationConsentSelector().
                            selectByType(new Set<ID> { newContacts[0].Id }, RQO_cls_Constantes.COD_PROD_NEW_GRADO);
                            
            System.assertEquals(0,
                                consents.size(), 
                                'No devuelve los consentimientos correctos.');
    
            test.stopTest();
        }
    }

}