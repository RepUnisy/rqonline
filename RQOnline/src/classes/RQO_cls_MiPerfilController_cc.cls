/**
*   @name: RQO_cls_MiPerfilController_cc
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Controlador de perfiles
*/
public with sharing class RQO_cls_MiPerfilController_cc {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_MiPerfilController';
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna un contacto mediante una Id de contacto
    */	
	@AuraEnabled
	public static Contact selectContactById(ID idContact) 
	{
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectContactById';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': id ' + idContact);
        
		List<Contact> contacts = new RQO_cls_ContactSelector().selectById(new Set<ID> { idContact });
		if (contacts != null && contacts.size() > 0)
			return contacts[0];
		else
			return null;
	}
	
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una cuenta según un Id de cuenta
    */	
	@AuraEnabled
	public static Account selectAccountById(ID idAccount)
	{
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectAccountById';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': id ' + idAccount);
        
		List<Account> accounts = new RQO_cls_AccountSelector().selectById(new Set<ID> { idAccount });
		if (accounts != null && accounts.size() > 0)
			return accounts[0];
		else
			return null;
	}
	
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que devuelve un listado de permisos de perfiles para usuarios en base
    *	a un perfil
    */	
	@AuraEnabled
	public static List<RQO_cmt_userProfilePermission__mdt> selectPermissions(string profile) 
	{
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectPermissions';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': profile ' + profile);
        
		return new RQO_cls_UserProfileSelector().selectByProfile(new Set<string> { profile });
	}
	
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que devuelve una lista con todos los perfiles seleccionadosa
    */	
	@AuraEnabled
	public static List<string> selectAllProfiles()
    {
    	Set<string> values = new RQO_cls_UserProfileSelector().selectProfiles();
    	List<String> profiles = new List<String>();
		profiles.addAll(values);
    	return profiles;
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que devuelve una lista con todos los perfiles seleccionadosa
    */	
	@AuraEnabled
	public static Map<String, String> selectProfiles()
    {
    	Map<String, String> options = new Map<String, String>();
    	Schema.DescribeFieldResult fieldResult = RQO_cmt_userProfilePermission__mdt.RQO_fld_userProfile__c.getDescribe();
        List<Schema.PicklistEntry> profiles = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry profile : profiles){
            options.put(profile.getValue(), profile.getLabel());
        }
    	return options;
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Selecciona una lista con todos los permisos
    */	
	@AuraEnabled
	public static List<string> selectAllPermissions()
    {
    	Set<string> values = new RQO_cls_UserProfileSelector().selectPermissionsLabels();
    	List<String> permissions = new List<String>();
		permissions.addAll(values);
    	return permissions;
    }

    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna un listado con los permisos para un perfil
    */	  
    @AuraEnabled
	public static List<string> selectPermissionsByProfile(string profile)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'selectPermissions';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': selectPermissionsByProfile ' + profile);
        
    	Set<string> values = new RQO_cls_UserProfileSelector().selectPermissionsByProfile(profile);
    	List<String> permissions = new List<String>();
		permissions.addAll(values);
    	return permissions;
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna los usuarios mediante un Id de cuenta
    */	
    @AuraEnabled
    public static List<User> selectUsersByAccountId(ID idAccount)
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectByAccountIdWithContact';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': idAccount ' + idAccount);
        
    	return new RQO_cls_UserSelector().selectByAccountIdWithContact(new Set<ID> { idAccount });						                
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna los usuarios mediante un Id de cuenta y un nombre de usuario
    */	
    @AuraEnabled
    public static List<User> selectUsersByName(ID idAccount, string name)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'selectUsersByName';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': idAccount ' + idAccount);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': name ' + name);
        
    	return new RQO_cls_UserSelector().selectByNameWithContact(new Set<ID> { idAccount }, name);						                
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que registra un contacto
    */	
    @AuraEnabled
    public static void registerContact(string idAccount, string nombre, string primerApellido, string segundoApellido,
    								   string email, string telefono, string perfil)
    {
    	try {
    		User currentUser = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()];
    		Contact currentContact = [SELECT RQO_fld_perfilCliente__c FROM Contact WHERE Id = :currentUser.ContactId];
    		
    		List<Contact> contactos = new List<Contact>();
    		Contact contacto = new Contact (
    			Account = [SELECT Id, RQO_fld_idExterno__c FROM Account WHERE Id = :idAccount],
				FirstName = nombre,
				LastName = primerApellido + ' ' + segundoApellido,
				AccountId = idAccount,
				Email = email,
				MobilePhone = telefono,
				RQO_fld_perfilUsuario__c = perfil,
				RQO_fld_perfilCliente__c = currentContact.RQO_fld_perfilCliente__c
				);
	    	contactos.add(contacto);
	    	new RQO_cls_Contacts(contactos).register();
	    }
	    catch (Exception e) {
	        throw new AuraHandledException(e.getMessage());    
	    }          
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que da una baja lógica de un usuario mediante un user Id
    */	
    @AuraEnabled
    public static void unregisterUser(ID userId)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'unregisterUser';
    	try {
    		RQO_cls_UserService.unregister(userId);	
	    }
	    catch (Exception e) {
	        throw new AuraHandledException(e.getMessage());    
	    }          
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna los usuarios mediante un Id de cuenta
    */	
   	@AuraEnabled
    public static void updateProfile(List<User> users)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'updateProfile';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': users ' + users);
        
        try {
        	RQO_cls_ContactService.updateProfile(users);	
        }
	    catch (Exception e) {
	        throw new AuraHandledException(e.getMessage());    
	    }  				                
    }
    
        /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de lenguajes
    */	
    @AuraEnabled
    public static List<String> getLanguages()
    {
    	return new RQO_cls_GeneralUtilities().getLanguageList();
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que actualiza un lenguaje para un usuario
    */	
    @AuraEnabled
    public static void updateLanguage(string language)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'updateLanguage';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': userId ' + UserInfo.getUserId() + ' language ' + language);
        
        try {
        	RQO_cls_UserService.updateLanguage(UserInfo.getUserId(), language);
        }
	    catch (Exception e) {
	        throw new AuraHandledException(e.getMessage());    
	    }  				                
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna lista de periodicidaeds de notificaciones
    */	
    @AuraEnabled
    public static List<String> getNotificationPeriodicity()
    {
    	List<String> values = new List<String>();
    	List<SelectOption> options = new RQO_cls_GeneralUtilities().getNotificationPeriodicity();
    	for (SelectOption option: options)
    		values.add(option.getValue());
    	return (values);
    }
    
	/**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna lista de periodicidaeds de notificaciones
    */	
    @AuraEnabled
    public static Map<String,String> getNotificationPeriodicities()
    {
    	Map<String,String> values = new Map<String,String>();
    	List<SelectOption> options = new RQO_cls_GeneralUtilities().getNotificationPeriodicity();
    	for (SelectOption option: options)
    		values.put(option.getValue(), option.getLabel());
    	return (values);
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna consentimientos para notificaciones según un id de contacto
    */	
    @AuraEnabled
    public static List<RQO_obj_notificationConsent__c> getNotificationConsents(ID idContact)
    {
    	List<RQO_obj_notificationConsent__c> consents = 
    		new RQO_cls_NotificationConsentSelector().selectByContactId(new Set<ID> { idContact });
    	
    	List<SelectOption> types = new RQO_cls_GeneralUtilities().getNotificationTypes();
    	List<SelectOption> periodicity = new RQO_cls_GeneralUtilities().getNotificationPeriodicity();
    	List<Contact> contacts = new RQO_cls_ContactSelector().selectById(new Set<ID> { idContact });
    	
		for (SelectOption typ : types)
    	{
    		boolean exist = false;
	    	for (RQO_obj_notificationConsent__c consent : consents)
	    	{
	    		if (consent.RQO_fld_notificationType__c == typ.getValue())
	    		{
	    			exist = true;
	    			break;
	    		}
	    	}
	    	if (!exist)
	    	{
	    		consents.add(new RQO_obj_notificationConsent__c(Name = contacts.size() > 0 ? contacts[0].Name : idContact, 
	    														RQO_fld_Contact__c = idContact,
	    														RQO_fld_emailSending__c = false,
	    														RQO_fld_emailSendingPeriodicity__c = null,
	    														RQO_fld_notificationType__c=typ.getValue()));
	    	}
    	}

    	return consents; 
    }
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que actualiza los consentimientos de notificaciones
    *	en base a un json de consentimientos
    */
    @AuraEnabled
    public static void updateNotificationConsents(string jsonConsents)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'updateNotificationConsents';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': jsonConsents ' + jsonConsents);
        
        List<RQO_obj_notificationConsent__c> consents = (List<RQO_obj_notificationConsent__c>)
        	JSON.deserialize(jsonConsents, List<RQO_obj_notificationConsent__c>.class);
        	
		Integer j = 0;
		while (j < consents.size())
		{
			if(consents.get(j).RQO_fld_emailSendingPeriodicity__c == null)
		    	consents.remove(j);
		  	else
		    	j++;
		}

		if (consents.size() > 0)
    		new RQO_cls_NotificationConsents(consents).updateConsents();
    }
}