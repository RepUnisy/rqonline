/**
*	@name: RQO_cls_ProcessLog_test
*	@version: 1.0
*	@creation date: 26/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar los métodos asociados a RQO_cls_ProcessLog
*/
@IsTest
public class RQO_cls_ProcessLog_test {
    
    /**
    *	@creation date: 26/2/2018
    *	@author: Juan Elías - Unisys
    *	@description: Método que crea el conjunto de datos inicial, necesario para la ejecución del test
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
        	RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
		}
        
    }
    
        /**
    * @creation date: 26/2/2018
    * @author: Juan Elías - Unisys
    * @description:	Método de test para el método instanceInitalLogService/completeLog
    * @exception: 
    * @throws: 
    */
    @isTest
    static void testInstanceInitalLogService(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
			RQO_obj_logProcesos__c objLog = new RQO_obj_logProcesos__c();
            test.startTest();
            RQO_obj_logProcesos__c resultado = RQO_cls_ProcessLog.instanceInitalLogService(null, null, null);
            RQO_cls_ProcessLog.completeLog(objLog, null, null, null);
            System.assertEquals(RQO_cls_Constantes.LOG_TYPE_SERVICE, resultado.RQO_fld_processType__c);
            test.stopTest();
        }
    }
    
    @isTest
    static void testInstanceInitalLogBatch(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_obj_logProcesos__c resultado = RQO_cls_ProcessLog.instanceInitalLogBatch(null, null, null, null, null, null);
            System.assertEquals(RQO_cls_Constantes.LOG_TYPE_BATCH, resultado.RQO_fld_processType__c);
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 26/2/2018
    * @author: Juan Elías - Unisys
    * @description:	Método de test para el método instanceInitalLogRest
    * @exception: 
    * @throws: 
    */
    
    @isTest
    static void testInstanceInitalLogRest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            
            test.startTest();
            RQO_obj_logProcesos__c resultado = RQO_cls_ProcessLog.instanceInitalLogRest(null, null, null, null, null, null, null, null, null);
            System.assertEquals(RQO_cls_Constantes.LOG_TYPE_SERVICE, resultado.RQO_fld_processType__c);
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 26/2/2018
    * @author: Juan Elías - Unisys
    * @description:	Método de test para el método instanceGenericLog
    * @exception: 
    * @throws: 
    */
    
    @isTest
    static void testInstanceGenericLog(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<RQO_obj_logProcesos__c> logList = RQO_cls_TestDataFactory.createLogProcesos(200);
            test.startTest();
            RQO_obj_logProcesos__c resultado = RQO_cls_ProcessLog.instanceGenericLog(null, null, null);
            RQO_cls_ProcessLog.generateLogData(logList);
            System.assertEquals(RQO_cls_Constantes.LOG_TYPE_GENERIC, resultado.RQO_fld_processType__c);
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 26/2/2018
    * @author: Juan Elías - Unisys
    * @description:	Método de test para el método generateLogServiceType
    * @exception: 
    * @throws: 
    */
    
    @isTest
    static void testGenerateLogServiceType(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            RQO_cls_ProcessLog processLog = new RQO_cls_ProcessLog();
            test.startTest();
            Datetime initialDate;
            Datetime endDate;
            Boolean isRetry = false;
            Id idResultado = processLog.generateLogServiceType(null, null, null, null, null, null, initialDate, endDate, null, isRetry, null, null, null);
            System.assertNotEquals(null, idResultado);
            test.stopTest();
        }
    }
}