/**
*   @name: RQO_cls_Contacts
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de que gestiona los contactos
*/
public with sharing class RQO_cls_Contacts extends RQO_cls_ApplicationDomain 
{   
	// // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_Contacts';
	
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Constructor de la clase RQO_cls_Contacts
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public RQO_cls_Contacts(List<Contact> contacts) 
    {     
    	super(contacts);   
    }   
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase constructor que implementa un construible. Clase de utilidad
    */
    public class Constructor implements fflib_SObjectDomain.IConstructable 
    {     
        public fflib_SObjectDomain construct(List<SObject>sObjectList) 
        {       
            return new RQO_cls_Contacts(sObjectList);     
        }   
    }  
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que actualiza un perfil para una unidad de trabajo (UnitOfWork)
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public void updateProfile(fflib_ISObjectUnitOfWork uow)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'updateProfile';
        
    	for (Contact contact : (List<Contact>)Records)
    	{
    		uow.registerDirty(contact);
    	}
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que da de alta un contacto
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public void register()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'register';
		
		for(Contact contact : (List<Contact>) Records)
		{
			// Buscar el usuario/email en Gigya
	    	string searchUrl = GetGigyaSearchUrl(contact.Email);
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' searchUrl: ' + searchUrl);
	    	RQO_cls_WSCallout searchCallout = new RQO_cls_WSCallout(searchUrl);
	    	RQO_cls_WSCalloutResponseBean searchResponse = searchCallout.send();
	
	    	if (searchResponse != null && searchResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK))
	    	{
	    		RQO_cls_GigyaSearchResponse.GigyaSearchResponse response = RQO_cls_GigyaSearchResponse.parse(searchResponse.contenido);
			    System.debug(CLASS_NAME + ' - ' + METHOD + ' response.objectsCount: ' + response.objectsCount);
	   			if (response.objectsCount > 0)
				{
					System.debug(CLASS_NAME + ' - ' + METHOD + ' response.results[0].data.Service: ' + response.results[0].data.Service);
					// Si existe y ya está dado de alta en Química, lanzar error
					if (response.results[0].data.Service != null && 
						response.results[0].data.Service.Quimica != null)
						throw new RQO_cls_CustomException('Correo electrónico existente');
					// Si existe y no está dado de alta en Química, darlo de alta en el servicio de Quimica
					else
					{
						string setAccountUrl = GetSetAccountUrl(response.results[0].UID, contact.AccountId, contact.Account.RQO_fld_idExterno__c, contact.RQO_fld_perfilCliente__c);
						System.debug(CLASS_NAME + ' - ' + METHOD + ' setAccountUrl: ' + setAccountUrl);
						RQO_cls_WSCallout setAccountCallout = new RQO_cls_WSCallout(setAccountUrl);
	    				RQO_cls_WSCalloutResponseBean setAccountResponse = setAccountCallout.send();
	    				if (setAccountResponse != null && setAccountResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK))
	    				{
	    					// Correo bienvenida (resetPassword con apiKey de autorizados)
	    					string sendEmailUrl = GetSendEmailUrl(contact.Email);
	    					System.debug(CLASS_NAME + ' - ' + METHOD + ' sendEmailUrl: ' + sendEmailUrl);
							RQO_cls_WSCallout sendEmailCallout = new RQO_cls_WSCallout(sendEmailUrl);
		    				RQO_cls_WSCalloutResponseBean sendEmailResponse = sendEmailCallout.send();
	    				}
	    				else
	    				{
							throw new CalloutException('Error llamando API Gigya');
	    				}
					}
			    }
		    	else
		    	{
			    	// Si no existe preregistrarlo y actualizar todos los datos
			    	string initRegistrationUrl = getInitRegistrationUrl();
			    	RQO_cls_WSCallout initRegistrationCallout = new RQO_cls_WSCallout(initRegistrationUrl);
			    	RQO_cls_WSCalloutResponseBean initRegistrationResponse = initRegistrationCallout.send();
			
			    	if (initRegistrationResponse != null && initRegistrationResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK))
			    	{
			    		string regToken;
			    		JSONParser parser = JSON.createParser(initRegistrationResponse.contenido);
	        
				        while (parser.nextToken() != null) {
				             if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'regToken'))
				             {
				                parser.nextToken();
				                regToken =  parser.getText();
				             }
				        }
				        
				      	string registrationUrl = getRegistrationUrl(regToken, contact.Email, contact.FirstName, contact.LastName, 
				      												contact.MobilePhone,  contact.Account.RQO_fld_idExterno__c, contact.RQO_fld_perfilCliente__c);
				    	RQO_cls_WSCallout registrationCallout = new RQO_cls_WSCallout(registrationUrl);
				    	RQO_cls_WSCalloutResponseBean registrationResponse = registrationCallout.send();
				
				    	if (registrationResponse != null && registrationResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK))
				    	{
				    		
				    		string uid;
				    		
				    		parser = JSON.createParser(registrationResponse.contenido);
	        
					        while (parser.nextToken() != null) {
					             if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'UID'))
					             {
					                parser.nextToken();
					                uid =  parser.getText();
					             }
					        }
				    		
				    		//darlo de alta en el servicio de Quimica
				    		string setAccountUrl = GetSetAccountUrl(uid, contact.AccountId, contact.Account.RQO_fld_idExterno__c, contact.RQO_fld_perfilCliente__c);
							System.debug(CLASS_NAME + ' - ' + METHOD + ' setAccountUrl: ' + setAccountUrl);
							RQO_cls_WSCallout setAccountCallout = new RQO_cls_WSCallout(setAccountUrl);
		    				RQO_cls_WSCalloutResponseBean setAccountResponse = setAccountCallout.send();
		    				if (setAccountResponse != null && setAccountResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK))
		    				{
		    				}
		    				else
		    				{
								throw new CalloutException('Error llamando API Gigya');
		    				}
				    	}
				    	else
		    			{
							throw new CalloutException('Error llamando API Gigya');
		    			}
			    	}
					else
	    			{
						throw new CalloutException('Error llamando API Gigya');
	    			}
		    	}
		    		
		   		// Alta de contacto en Salesforce
				System.debug(CLASS_NAME + ' - ' + METHOD + ' contact.firstName: ' + contact.firstName);
				System.debug(CLASS_NAME + ' - ' + METHOD + ' contact.lastname: ' + contact.lastname);
				System.debug(CLASS_NAME + ' - ' + METHOD + ' contact.email: ' + contact.email);
				System.debug(CLASS_NAME + ' - ' + METHOD + ' contact.RQO_fld_perfilCliente__c: ' + contact.RQO_fld_perfilCliente__c);
				
				addContact(contact);
	    	}
	    	else
	    	{
	    		throw new CalloutException('Error llamando API Gigya');
	    	}
		}
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que agrega un contacto al conjunto de contactos
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private void addContact (Contact contact)
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'addContact';
		
		// Login Salesforce
		string loginUrl = RQO_cls_CustomSettingUtil.getSalesforceLoginUrl();
		System.debug(CLASS_NAME + ' - ' + METHOD + ' loginUrl: ' + loginUrl);
		System.debug(CLASS_NAME + ' - ' + METHOD + ' RQO_cls_CustomSettingUtil.getSalesforceKey(): ' + RQO_cls_CustomSettingUtil.getSalesforceKey());
		System.debug(CLASS_NAME + ' - ' + METHOD + ' RQO_cls_CustomSettingUtil.getSalesforceSecret(): ' + RQO_cls_CustomSettingUtil.getSalesforceSecret());
		System.debug(CLASS_NAME + ' - ' + METHOD + ' RQO_cls_CustomSettingUtil.getSalesforceUser(): ' + RQO_cls_CustomSettingUtil.getSalesforceUser());
		System.debug(CLASS_NAME + ' - ' + METHOD + ' RQO_cls_CustomSettingUtil.getSalesforcePassword(): ' + RQO_cls_CustomSettingUtil.getSalesforcePassword());
		string body = 'grant_type=password&client_id=' + RQO_cls_CustomSettingUtil.getSalesforceKey() + 
						'&client_secret=' + RQO_cls_CustomSettingUtil.getSalesforceSecret() + 
						'&username=' + RQO_cls_CustomSettingUtil.getSalesforceUser() + 
						'&password=' + RQO_cls_CustomSettingUtil.getSalesforcePassword();
		System.debug(CLASS_NAME + ' - ' + METHOD + ' body: ' + body);
		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/x-www-form-urlencoded');
		RQO_cls_WSCallout loginCallout = new RQO_cls_WSCallout(loginUrl, RQO_cls_Constantes.REST_METHOD_TYPE_POST, body, headers);
		RQO_cls_WSCalloutResponseBean loginResponse = loginCallout.send();
		
		if (loginResponse != null && loginResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK))
		{
			string instance_url;
			string access_token;
			JSONParser parser = JSON.createParser(loginResponse.contenido);
	        
	        while (parser.nextToken() != null) {
	             if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'instance_url'))
	             {
	                parser.nextToken();
	                instance_url =  parser.getText();
	             }
	             else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token'))
	             {
	                parser.nextToken();
	                access_token = parser.getText();
	             }
	        }
	        
	        System.debug(CLASS_NAME + ' - ' + METHOD + ' instance_url: ' + instance_url);
	        System.debug(CLASS_NAME + ' - ' + METHOD + ' access_token: ' + access_token);
	        
	        if (!String.isBlank(instance_url) && !String.isBlank(access_token))
	        {
	        	// Alta Contacto
	      		string addContactUrl = instance_url + '/services/data/v41.0/sobjects/Contact';
				System.debug(CLASS_NAME + ' - ' + METHOD + ' addContactUrl: ' + addContactUrl);
				body = '{"FirstName" : "' + contact.FirstName + '", ' +
							  '"LastName" : "' + contact.LastName + '", ' +
							  '"AccountId" : "' + contact.AccountId + '", ' +
							  '"Email" : "' + contact.Email + '", ' +
							  '"MobilePhone" : "' + contact.MobilePhone + '", ' +
							  '"RQO_fld_perfilUsuario__c" : "' + contact.RQO_fld_perfilUsuario__c + '", ' +
							  '"RQO_fld_perfilCliente__c" : "' + contact.RQO_fld_perfilCliente__c + '"}';

				headers = new Map<String, String>();
				headers.put('Authorization', 'Bearer ' + access_token);
				headers.put('Content-Type', 'application/json');
				RQO_cls_WSCallout addContactCallout = new RQO_cls_WSCallout(addContactUrl, RQO_cls_Constantes.REST_METHOD_TYPE_POST, body, headers);
				RQO_cls_WSCalloutResponseBean addContactResponse = addContactCallout.send();
				
		       	if (addContactResponse != null && 
		       		(addContactResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_CREATED) ||
		       		 addContactResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK)))
				{
					string contactId;
					parser = JSON.createParser(addContactResponse.contenido);
	        
			        while (parser.nextToken() != null) {
			             if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'id'))
			             {
			                parser.nextToken();
			                contactId =  parser.getText();
			             }
			        }
			        
			        System.debug(CLASS_NAME + ' - ' + METHOD + ' contactId: ' + contactId);
					System.debug(CLASS_NAME + ' - ' + METHOD + ' UserInfo.getProfileId(): ' + UserInfo.getProfileId());
					
					// Alta Usuario
		      		string addUserUrl = instance_url + '/services/data/v41.0/sobjects/User';
					System.debug(CLASS_NAME + ' - ' + METHOD + ' addUserUrl: ' + addUserUrl);
					string nick = contact.email!=null?contact.email.substring(0, contact.email.indexOf('@'))+contact.lastname.substring(0,1):'';
					body = '{"Alias" : "' + contact.firstName.substring(0,3) + contact.lastname.substring(0,4) + '", ' +
							  '"Email" : "' + contact.Email + '", ' +
							  '"Emailencodingkey" : "UTF-8", ' +
							  '"Firstname" : "' + contact.Firstname + '", ' +
							  '"Lastname" : "' + contact.Lastname + '", ' +
							  '"languagelocalekey" : "es", ' +
							  '"localesidkey" : "es", ' +
							  '"ContactId" : "' + contactId + '", ' +
							  '"timezonesidkey" : "Europe/Berlin", ' +
							  '"Username" : "' + contact.Email + '", ' +
							  '"FederationIdentifier" : "' + contact.Email + '", ' +
							  '"CommunityNickname" : "' + nick + '", ' +
							  '"ProfileId" : "' + UserInfo.getProfileId() + '", ' +
							  '"IsActive" : "true"}';
					headers = new Map<String, String>();  
					headers.put('Authorization', 'Bearer ' + access_token);
					headers.put('Content-Type', 'application/json');
					RQO_cls_WSCallout addUserCallout = new RQO_cls_WSCallout(addUserUrl, RQO_cls_Constantes.REST_METHOD_TYPE_POST, body, headers);
					RQO_cls_WSCalloutResponseBean addUserResponse = addUserCallout.send();
					
					if (addContactResponse != null && addContactResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_CREATED))
					{
						System.debug(CLASS_NAME + ' - ' + METHOD + ' Alta usuario OK: ' + addUserUrl);
					}
					else
					{
						throw new CalloutException('Error llamando API Salesforce');
					}
				}
				else
				{
					throw new CalloutException('Error llamando API Salesforce');
				}
	        }
		}
		else
		{
			throw new CalloutException('Error llamando API Salesforce');
		}
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene una url de Gigya
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string GetGigyaSearchUrl(string email)
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'GetGigyaSearchUrl';
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ' email: ' + email);
		
    	return 'https://accounts.eu1.gigya.com/accounts.search?apiKey=' + RQO_cls_CustomSettingUtil.getGigyaApiKeyAutorizados() + 
    				'&userKey=' + RQO_cls_CustomSettingUtil.getGigyaUserKey() + '&secret=' + RQO_cls_CustomSettingUtil.getGigyaSecret() + 
    				'&query=' + EncodingUtil.urlEncode('SELECT * FROM accounts where loginIDs.emails=\'' + email + '\' or profile.email contains \'' + email + '\'','UTF-8');
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Obtiene el conjunto de URLs de cuenta
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string GetSetAccountUrl(string uid, Id accountId, string ctc, string clientProfile)
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'GetSetAccountUrl';
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ' ctc: ' + ctc);
		System.debug(CLASS_NAME + ' - ' + METHOD + ' clientProfile: ' + clientProfile);
		
    	return 'https://accounts.eu1.gigya.com/accounts.setAccountInfo?apiKey=' + RQO_cls_CustomSettingUtil.getGigyaApiKeyAutorizados() + 
    				'&userKey=' + RQO_cls_CustomSettingUtil.getGigyaUserKey() + '&secret=' + RQO_cls_CustomSettingUtil.getGigyaSecret() + 
    				'&UID=' + uid + 
    				getProfileParamSetUser() +
    				getDataParamSetUser(accountId, ctc, clientProfile);
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el parámetro de perfil de un usuario
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string getProfileParamSetUser()
    {
    	return '&profile={' + EncodingUtil.urlEncode('\'country\':\'\', \'locale\':\'\'','UTF-8') + '}';
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene los datos de parámetros de un usuario.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string getDataParamSetUser(Id accountId, string ctc, string clientProfile)
    {
    	List<RQO_obj_identificador__c> identificadores = new RQO_cls_IdentificadorSelector().selectByAccount(new Set<Id>{accountId});
    	
    	for (RQO_obj_identificador__c identificador : identificadores)
    	{
    		if (identificador.RQO_fld_tipodeIdentificador__c == 'ES1')
		    	return '&data={\'RepsolClient\':\'true\',\'Service.Quimica\':\'true\',\'FlagAutorizado.QuimicaOnline\':\'true\',' +
		    				   '\'CodCliente.Quimica\':\'' + ctc + '\',\'RepsolClientType\':\'' + clientProfile + '\',' + 
		    				   '\'DocId.Quimica\':\'' + identificador.RQO_fld_codigoIdentificador__c + '\',\'LegalTerms.Quimica\':null,\'LegalTermsDate.Quimica\':null}';  		
    	}
    	
    	return '&data={\'RepsolClient\':\'true\',\'Service.Quimica\':\'true\',\'FlagAutorizado.QuimicaOnline\':\'true\',' +
		    				   '\'CodCliente.Quimica\':\'' + ctc + '\',\'RepsolClientType\':\'' + clientProfile + '\',' + 
		    				   '\'LegalTerms.Quimica\':null,\'LegalTermsDate.Quimica\':null}';
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene y envía la URL de un mail
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string GetSendEmailUrl(string email)
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'GetSendEmailUrl';
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ' email: ' + email);
		
    	return 'https://accounts.eu1.gigya.com/accounts.resetPassword?apiKey=' + RQO_cls_CustomSettingUtil.getGigyaApiKeyAutorizados() + 
    				'&userKey=' + RQO_cls_CustomSettingUtil.getGigyaUserKey() + '&secret=' + RQO_cls_CustomSettingUtil.getGigyaSecret() + 
    				'&loginID=' + email;
    }
	
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la URL de inicio de registro
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string getInitRegistrationUrl()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'getInitRegistrationUrl';
		
    	return 'https://accounts.eu1.gigya.com/accounts.initRegistration?apiKey=' + RQO_cls_CustomSettingUtil.getGigyaApiKeyAutorizados() + 
    				'&userKey=' + RQO_cls_CustomSettingUtil.getGigyaUserKey() + '&secret=' + RQO_cls_CustomSettingUtil.getGigyaSecret();
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la URL de registro
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string getRegistrationUrl(string regToken, string email, string firstName, string lastName, string phone, string ctc, string perfil)
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'getRegistrationUrl';
		
		String password = ((Integer)Math.rint(Math.random()*1000)+100) + '@' + ((Integer)Math.rint(Math.random()*1000)+100) + 'Z';
		
    	return 'https://accounts.eu1.gigya.com/accounts.register?apiKey=' + RQO_cls_CustomSettingUtil.getGigyaApiKeyAutorizados() + 
    				'&userKey=' + RQO_cls_CustomSettingUtil.getGigyaUserKey() + '&secret=' + RQO_cls_CustomSettingUtil.getGigyaSecret() +
    				'&regToken=' + regToken + '&email=' + email + '&password=' + password +
    				getProfileParamAddUser(firstName, lastName, email, phone) + 
    				getDataParamAddUser(ctc, perfil) +
    				'&finalizeRegistration=true';
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el parámetro de un perfil para agregar un usuario
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string getProfileParamAddUser(string firstName, string lastName, string email, string phone)
    {
    	return '&profile={' + EncodingUtil.urlEncode('\'firstName\':\'' + firstName + '\', \'lastName\':\'' + lastName + '\', ' + 
    										  '\'email\':\'' + email + '\', \'phones.default\':\'' + phone + '\', ' + 
    										  '\'country\':\'ES\', \'locale\':\'es\'','UTF-8') + '}';
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene los datos del parámetro de un perfil para agregar un usuario
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string getDataParamAddUser(string ctc, string perfil)
    {
		return '&data={\'RepsolClient\':\'true\',' + EncodingUtil.urlEncode('\'Service.Quimica\':\'true\',\'RepsolClientType\':\'' + perfil + '\', ' + 
    				  '\'LegalTerms.Quimica\':\'true\',\'LegalTermsDate.Quimica\':\'' + Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'') + '\'','UTF-8') + '}';
    }
    
    /**
    *   @name: RQO_cls_Contacts
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la URL de reinicio de contraseña.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private string getResetPasswordUrl(string email)
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'getResetPasswordUrl';
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ' email: ' + email);
		
    	return 'https://accounts.eu1.gigya.com/accounts.resetPassword?apiKey=' + RQO_cls_CustomSettingUtil.getGigyaApiKeyAutorizados() + 
    				'&userKey=' + RQO_cls_CustomSettingUtil.getGigyaUserKey() + '&secret=' + RQO_cls_CustomSettingUtil.getGigyaSecret() + 
    				'&loginID=' + email;
    }
}