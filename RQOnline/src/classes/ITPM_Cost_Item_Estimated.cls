/*------------------------------------------------------------------------
Author:         Agustin Andres San Miguel
Company:        Unisys
Description:    Test class
History
<Date>          <Author>                        <Description>
23-Feb-2017     Agustin Andres San Miguel        Primera versión
----------------------------------------------------------------------------*/

public class ITPM_Cost_Item_Estimated{
    public ITPM_Budget_Item__c cost {get; set;}
    private ApexPages.StandardController sc;
    String lookup;
    /*String BudgetInvestment;
    String Conceptualizacion;*/
    
    public ITPM_Cost_Item_Estimated(ApexPages.StandardController stdController){
        this.cost = (ITPM_Budget_Item__c)stdController.getRecord();
        this.sc = stdController;
        
        lookup = ApexPages.currentPage().getParameters().get('lookup');
        
        cost.ITPM_REL2_Project__c = lookup;
        cost.ITPM_CI_SEL_Version__c = 'Estimated';
        
        
        /*BudgetInvestment = ApexPages.currentPage().getParameters().get('BudgetInvestment');
        Conceptualizacion = ApexPages.currentPage().getParameters().get('Conceptualizacion');*/
    }
    
    public PageReference save() {        
        list <RecordType> recordType_Project = [SELECT id FROM RecordType where DeveloperName = 'Project'];
        
        list<ITPM_Project__c> listaProjects = [SELECT id, ITPM_Project_REL_Conceptualization__c, ITPM_REL_Budget_Investment__c, ITPM_Project_REL_Conceptualization__r.ITPM_Conceptu_REL_Opportunity__c FROM ITPM_Project__c WHERE id = :lookup];
        
        cost.ITPM_REL_Budget_Investment__c = listaProjects[0].ITPM_REL_Budget_Investment__c;
        //cost.ITPM_CI_REL_Conceptualization__c = listaProjects[0].ITPM_Project_REL_Conceptualization__c;
        cost.ITPM_CI_REL_Opportunity__c= listaProjects[0].ITPM_Project_REL_Conceptualization__r.ITPM_Conceptu_REL_Opportunity__c;
        cost.RecordTypeId = recordType_Project[0].id;
        
        PageReference detailPage = sc.save();
        
        system.debug('EL DETALLE ES ' + cost );

        return detailPage;
    }
}