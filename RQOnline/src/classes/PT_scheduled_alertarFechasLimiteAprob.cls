/**
 * Clase encargada de comprobar si las fechas límite de aprobación de los respectivos programas de patrocinios 
 * están próximas en el tiempo, y de ser así, cambiar automáticamente el valor de un check, 
 * lo que desencadenará mediante una regla de flujo de trabajo y una alerta de correo electrónico, 
 * el envío de una notificación por email
 **/
global class PT_scheduled_alertarFechasLimiteAprob implements Schedulable {
    
    global void execute(SchedulableContext SC) {
          
         String mensajeLog=' ';
         try{     
                   
             mensajeLog = mensajeLog+'@@ '+Datetime.now().format()+' INICIO proceso automático comprobación fechas límite aprobación Patrocinios @@@';   
                                
             //lo inicializamos a un valor por defecto, por si no se ha configurado correctamente en la aplicación
             Integer numeroDiasMargen = 15;
                
             mensajeLog = mensajeLog+'\n@@ Días de margen establecido por defecto: '+ numeroDiasMargen;   

            //comprobamos que el parámetro 'MargenLimiteAprobacion' está definido por el usuario       
            for(Configuracion_patrocinios__c configPatrocinio: [select Valor__c  from Configuracion_patrocinios__c where name =: 'MargenLimiteAprobacion']){
     
                numeroDiasMargen  = Integer.valueOf(configPatrocinio.Valor__c);                 
                mensajeLog = mensajeLog+'\n@@ Días de margen establecido en "Configuracion patrocinios" atributo "MargenLimiteAprobacion" (prevalece): '+ numeroDiasMargen;   
            }
                 
             mensajeLog = mensajeLog+'\n@@ solo se notifica sobre programas de patrocinio que se encuentren pendientes de aprobación.';    
             mensajeLog = mensajeLog+'\n@@ la notificación es única y se enviará cuando falten '+numeroDiasMargen+' días para alcanzar la fecha límite de aprobación de cada programa de patrocinio.';    
             
             Patrocinios__c[] patrocinios = [select ID, name, nombre__c,
                                             avisado_fecha_limite_aprobacion__c, 
                                             fecha_limite_de_aprobacion2__c,
                                             fecha_de_envio_para_aprobacion__c
                                             From Patrocinios__c 
                                             where en_proceso_de_aprobacion__c =: true];
             
             for(Patrocinios__c patAux: patrocinios){
                                     
                 mensajeLog = mensajeLog+'\n@@----------------------------------------';  
                 mensajeLog = mensajeLog+'\n@@ ID Patrocinio: '+ patAux.Name+ ' nombre del programa: '+patAux.Nombre__c;                                                        
                 mensajeLog = mensajeLog+'\n@@ se envío para aprobación el: '+patAux.fecha_de_envio_para_aprobacion__c.format();     
                 mensajeLog = mensajeLog+'\n@@ fecha límite de aprobación el: '+patAux.fecha_limite_de_aprobacion2__c.format();
                       
                 if (patAux.fecha_limite_de_aprobacion2__c!=null && patAux.fecha_limite_de_aprobacion2__c - numeroDiasMargen == Date.today() ){     
                                  
                     patAux.avisado_fecha_limite_aprobacion__c=true;   
                     mensajeLog = mensajeLog+'\n@@ hoy faltan '+numeroDiasMargen+' días para alcanzar su fecha límite de aprobación, se envía por tanto notificación.';    
                 }
                 else{ 
                     mensajeLog = mensajeLog+'\n@@ hoy no es la fecha para enviar notificación de este programa de patrocinio.';       
                 }  
             } 

             update(patrocinios);                     
            
             mensajeLog = mensajeLog+'\n@@ FIN proceso automático comprobación fechas límite aprobación Patrocinios @@@@@@';                      
         }             
        catch(Exception e){                           
             mensajeLog = mensajeLog+'\n@@@ ERROR clase PT_scheduled_alertarFechasLimiteAprob: '+e.getMessage();          
        } 
        system.debug(mensajeLog);   
        //si para facilitar la supervision de su correcto comportamiente, se quiere recibir en una direccion de email el log de la ejecucion de este trabajo programado, 
        //descomentar esta sentencia, y establecer la dirección de correo en el método enviarEmail
        //PT_button_enviarMail.enviarEmail('LOG proceso automático comprobación fechas límite aprobación Patrocinios', mensajeLog);        
    }
   
}