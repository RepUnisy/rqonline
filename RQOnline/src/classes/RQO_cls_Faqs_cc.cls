/**
*   @name: RQO_cls_Faqs_cc
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona los Faqs
*/
public class RQO_cls_Faqs_cc {
    
    /**
    *   @name: RQO_cls_Faqs_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene un listado de nombres de categorías de Faqs
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<String> getFaqsCategorias(String lenguaje, String palabra, String tipoUsuario){     
             
        list<String> listaCategorias = new list<String>();
        set<String> setCategorias = new set<String>();
        map<string, string> mapaLabels = new map<string, string>();
        
        Schema.DescribeFieldResult fieldResult = RQ0_obj_Facs__c.RQO_fld_topic__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple){
            mapaLabels.put(f.getValue(), f.getLabel());
        }
        
        /*Schema.DescribeFieldResult fieldResult = RQ0_obj_Facs__c.RQO_fld_topic__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        
        for( Schema.PicklistEntry f : ple) {
            if(f.getLabel().contains(palabra)){
                listaCategorias.add(f.getLabel());
            }
        }*/
        
        if(String.isEmpty(palabra)){          
            for(string aux : mapaLabels.values()){
                listaCategorias.add(aux);
            }
            system.debug('CATEGORIAS 1 ' + listaCategorias);
            system.debug('CATEGORIAS 1 ' + mapaLabels);
        }else{        
            palabra = '%'+palabra+'%';
            system.debug('CATEGORIAS 2');
            List<RQ0_obj_Facs__c> preguntas = new List<RQ0_obj_Facs__c>();
            
            Contact contact = null;
            if (tipoUsuario == 'cliente' || tipoUsuario == 'noCliente')
                contact = RQO_cls_UserService.getContact(Id.valueOf(UserInfo.getUserId()));
            
            if(contact != null && contact.RQO_fld_perfilUsuario__c != RQO_cls_Constantes.USER_PROFILE_NO_CLIENTE){
                preguntas = [Select RQO_fld_topic__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'cliente' OR  RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra) ORDER BY RQO_fld_topic__c ASC];
                system.debug('CATEGORIAS 2.1');
            }else if(contact != null && contact.RQO_fld_perfilUsuario__c == RQO_cls_Constantes.USER_PROFILE_NO_CLIENTE){
                preguntas = [Select RQO_fld_topic__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra) ORDER BY RQO_fld_topic__c ASC];
                system.debug('CATEGORIAS 2.2');
            }else{
                preguntas = [Select RQO_fld_topic__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra) ORDER BY RQO_fld_topic__c ASC];
                system.debug('CATEGORIAS 2.3');
            }
            
            for(RQ0_obj_Facs__c aux : preguntas){
               setCategorias.add(aux.RQO_fld_topic__c);
            }
                                                                                                                                     
            for(string aux : setCategorias){
               listaCategorias.add(mapaLabels.get(aux)); 
            }
        }
        system.debug('LA PALABRA ' + palabra);
        system.debug('LAS CATEGORIAS ' + listaCategorias);
        return listaCategorias;
    }
    
    /**
    *   @name: RQO_cls_Faqs_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene las preguntas FAQ
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<RQ0_obj_Facs__c> getFaqsPreguntas(String lenguaje, String topic, String palabra, string tipoUsuario){
        List<RQ0_obj_Facs__c> preguntas = new List<RQ0_obj_Facs__c>();
        map<string, string> mapaLabels = new map<string, string>();
        
        Schema.DescribeFieldResult fieldResult = RQ0_obj_Facs__c.RQO_fld_topic__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple){
            mapaLabels.put(f.getLabel(), f.getValue());
        }
        system.debug('CONSTAN ' +  preguntas + lenguaje + topic + ' -- ' + mapaLabels);
        topic = mapaLabels.get(topic);
        
        Contact contact = null;
            if (tipoUsuario == 'cliente' || tipoUsuario == 'noCliente')
                contact = RQO_cls_UserService.getContact(Id.valueOf(UserInfo.getUserId()));
            
        if(String.isEmpty(palabra)){
            if(contact != null && contact.RQO_fld_perfilUsuario__c != RQO_cls_Constantes.USER_PROFILE_NO_CLIENTE){
                preguntas = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'cliente' OR  RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic];
                system.debug('PREGUNTAS 1.1 ' +  preguntas + lenguaje + topic);
                system.debug('VALORES ' + lenguaje + topic + palabra + tipoUsuario);
            }else if(contact != null && contact.RQO_fld_perfilUsuario__c == RQO_cls_Constantes.USER_PROFILE_NO_CLIENTE){
                preguntas = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic];
                system.debug('PREGUNTAS 1.2 ' +  preguntas);
            }else{
                preguntas = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic];
                system.debug('PREGUNTAS 1.3 ' +  preguntas);
            }  
        }else{ 
            palabra = '%'+palabra+'%';
            
            if(contact != null && contact.RQO_fld_perfilUsuario__c != RQO_cls_Constantes.USER_PROFILE_NO_CLIENTE){
                preguntas = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'cliente' OR  RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra)];
                system.debug('PREGUNTAS 2.1 ' +  preguntas);
            }else if(contact != null && contact.RQO_fld_perfilUsuario__c == RQO_cls_Constantes.USER_PROFILE_NO_CLIENTE){
                preguntas = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra)];
                system.debug('PREGUNTAS 2.2 ' +  preguntas);
            }else{
                preguntas = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra)];
                system.debug('PREGUNTAS 2.3 ' +  preguntas);
            }                       
        }
        return preguntas;
    }
}