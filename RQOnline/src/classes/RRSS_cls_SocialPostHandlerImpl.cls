/**
 * @name: RRSS_cls_SocialPostHandlerImpl
 * @version: 1.2
 * @creation date: 26/04/2017
 * @author: Ramon Diaz, Accenture
 * @description: Clase retocada del estándar de Salesforce basándose en la clase por defecto de 
 *               Social Customer Service.
 *               Origen: https://help.salesforce.com/articleView?id=social_customer_service_apexclass_ref.htm&language=en_US&type=0
 *               Solo se ha modificado y comentado en profundidad el método createCase()
*/

global virtual class RRSS_cls_SocialPostHandlerImpl implements Social.InboundSocialPostHandler
{
    final static Integer CONTENT_MAX_LENGTH = 32000;
    Boolean isNewCaseCreated = false;
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que retorna 0 días para reapertura de caso.
     * @return 0
     */
    // Reopen case if it has not been closed for more than this number
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        return 0;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que retorna el Id de la cuenta de "RRSS Generica"
     * @return Id de la cuenta genérica
     */
    global virtual String getDefaultAccountId() {
        Account[] generica = [SELECT Id FROM Account WHERE FirstName =: Label.RRSS_CuentaGenerica_Firstname AND LastName=: Label.RRSS_CuentaGenerica_Lastname];
        if (generica.isEmpty()) {
            Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.RRSS_RecordType_Account).getRecordTypeId();
            Account acct = new Account(FirstName = Label.RRSS_CuentaGenerica_Firstname, LastName=Label.RRSS_CuentaGenerica_Lastname,RecordTypeId = recordTypeId);
            insert acct;
            String result = acct.Id;
            return result;
        } else {
            String result = generica[0].Id;
            return result;
        }
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que en base a una publicación recibida desde Social Studio, crea o no un nuevo caso
     * @param post, persona, rawData / post contendrá el contenido de la publicación, persona la Social Persona que creó
     * la publicación y rawData toda la información proveniente de Social Studio
     */
    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        Social.InboundSocialPostResult result = new Social.InboundSocialPostResult();
        result.setSuccess(true);
        matchPost(post);
        matchPersona(persona);
        if ((post.Content != null) && (post.Content.length() > CONTENT_MAX_LENGTH)) {
            post.Content = post.Content.abbreviate(CONTENT_MAX_LENGTH);
        }
        if (post.Id != null) {
            handleExistingPost(post, persona);
            return result;
        }
        setReplyTo(post, persona);
        buildPersona(persona);
        // Custom code for TwitterSurvey Inbound DMs Threading
        /* Case parentCase = null;
        if (post.Provider == 'Twitter' && post.MessageType == 'Direct') {
        Map<String, String> mapExternalIdsParentsIds = new Map<String, String>();
        for(scs_twtSurveys__Social_Feedback_Survey__c sfS :
        [SELECT Id, scs_twtSurveys__Case__c, scs_twtSurveys__Direct_Message_ID__c FROM
        scs_twtSurveys__Social_Feedback_Survey__c WHERE scs_twtSurveys__Direct_Message_ID__c =
        :post.ExternalPostId LIMIT 1]) {
        post.ParentId = sfS.scs_twtSurveys__Case__c;
        post.IsOutbound = true;
        parentCase = [SELECT Id FROM Case WHERE Id = :post.ParentId LIMIT 1].get(0);
        }
        }
        if (parentCase == null)
        parentCase = buildParentCase(post, persona, rawData);*/
		//
		//Original
		Case parentCase = buildParentCase(post, persona, rawData);
		//
        setRelationshipsOnPost(post, persona, parentCase);
        setModeration(post, rawData);
        upsert post;
        if(isNewCaseCreated){
            updateCaseSource(post, parentCase);
        }
        return result;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que modifica el campo reviewedStatus en caso de necesitar moderación
     * @param post, persona, rawData / post contendrá el contenido de la publicación, persona la Social Persona que creó
     * la publicación y rawData toda la información proveniente de Social Studio
     */
    private void setModeration(SocialPost post, Map<String, Object> rawData){
    //if we don't automatically create a case, we should flag the post as requiring moderator review.
        if(post.parentId == null && !isUnsentParent(rawData))
            post.reviewedStatus = Label.RRSS_SocialPostStatus_Needed;
        }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que modifica el campo SourceId del caso creado a partir de una publicación para rellenarlo
     * con el Id del Social Post que originó la creación del caso.
     * @param post, case / post contendrá el contenido de la publicación, case contendrá el Caso relacionado con el Social Post
     */
    private void updateCaseSource(SocialPost post, Case parentCase){
        if(parentCase != null) {
            parentCase.SourceId = post.Id;
            //update as a new sobject to prevent undoing any changes done by insert triggers
            update new Case(Id = parentCase.Id, SourceId = parentCase.SourceId);
        }
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que actualiza el contenido del post y de la persona en caso de ya existir.
     * @param post, persona / post contendrá el contenido de la publicación, persona la Social Persona que creó
     * la publicación
     */
    private void handleExistingPost(SocialPost post, SocialPersona persona) {
        update post;
        if (persona.id != null)
            updatePersona(persona);
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que actualiza los campos de replyTo para reflejar que la publicación que llega desde Social
     * Studio es una respuesta de una publicación original
     * @param post, persona / post contendrá el contenido de la publicación, persona la Social Persona que creó
     * la publicación
     */
    private void setReplyTo(SocialPost post, SocialPersona persona) {
        SocialPost replyTo = findReplyTo(post, persona);
        if(replyTo.id != null) {
            post.replyToId = replyTo.id;
            post.replyTo = replyTo;
        }
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que crea o actualiza una Social Persona en función de si existe ya o se ha de crear
     * @param persona / persona la Social Persona que creó la publicación
     */
    private SocialPersona buildPersona(SocialPersona persona) {
        if (persona.Id == null)
            createPersona(persona);
        else
            updatePersona(persona);
        return persona;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que actualiza una Social Persona
     * @param persona / persona la Social Persona que creó la publicación
     */
    private void updatePersona(SocialPersona persona) {
        try{
            update persona;
        }catch(Exception e) {
            System.debug('Error updating social persona: ' + e.getMessage());
        }
    }
    
   /**
     * @author: Ramon Diaz - Accenture
     * @description Función que crea o devuelve un caso Parent para una nueva publicación
     * @param post, persona, rawData / / post contendrá el contenido de la publicación, persona la Social Persona que creó
     * la publicación y rawData toda la información proveniente de Social Studio
     * @return case Devuelve el caso que se ha creado o que se ha recuperado de un Social Post anterior
     */
    private Case buildParentCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData){ 
        if(!isUnsentParent(rawData)) {
            Case parentCase = findParentCase(post, persona);
            if (parentCase != null) {
                if (!parentCase.IsClosed || parentCase.status != Label.RRSS_CaseStatus_Closed) {
                    return parentCase;
                }
            }  
            if(shouldCreateCase(post, rawData)){
                isNewCaseCreated = true;
                return createCase(post, persona, rawData);
            }
        }
        return null;
    }
    
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que establece el vínculo entre un Social Post, la Social Persona y el Parent Case para una
     * publicación de Social Studio entrante.
     * @param postToUpdate, persona, parentCase / / postToUpdate contendrá el contenido de la publicación que se modificará
     * persona la Social Persona que creó la publicación y parentCase toda la información del caso Parent creado anteriormente
     */
    private void setRelationshipsOnPost(SocialPost postToUpdate, SocialPersona persona, Case parentCase) {
        if (persona.Id != null) {
            postToUpdate.PersonaId = persona.Id;
            if(persona.ParentId.getSObjectType() != SocialPost.sObjectType) {
                postToUpdate.WhoId = persona.ParentId;
            }
        }
        if(parentCase != null) {
            postToUpdate.ParentId = parentCase.Id;
        }
    }
    

    /**
     * @creation date: 26/04/2017
     * @author: Ramon Diaz, Accenture
     * @description: Función que crea un caso en base a toda la información recuperada desde Social Studio
     * @param: post, persona, rawData / post contendrá el contenido de la publicación, persona la Social Persona que creó
     * @return: Case Se retorna el caso creado con los parámetros especificados en la lógica
     * @exception: Excepción por un etiquetado incorrecto (como por ejemplo, valor no válido en la picklist restringida)
     * @throws: System.debug con el mensaje de error
     * */
    private Case createCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.RRSS_RecordType).getRecordTypeId();
        Case newCase = new Case();
        // Copiamos el contenido del post limitando a 255 caracteres en un campo de texto para su uso en artículos
        // sugeridos en Knowledge
        if(post.Content.length() > 255) {
            newCase = new Case(subject = post.Name, rrss_Sentimiento_Inicial__c = post.Sentiment , priority = post.PostPriority, Description = post.Content,RRSS_Contenido_del_post__c = post.Content.substring(0,254), rrss_Categoria_del_caso__c = post.Classification, RecordTypeId = caseRecordTypeId);            
        } else {
            newCase = new Case(subject = post.Name, rrss_Sentimiento_Inicial__c = post.Sentiment , priority = post.PostPriority, Description = post.Content,RRSS_Contenido_del_post__c = post.Content, rrss_Categoria_del_caso__c = post.Classification, RecordTypeId = caseRecordTypeId);            
        }
        // Si hay etiquetas, las separamos y de la primera extraemos los 3 niveles de tipología
        // A continuación, los insertamos en las picklist dependientes de Departamento, Nivel 1 y Nivel 2
        if (post.PostTags != null) {
            List<String> tags = post.PostTags.split(',',0);
            if (tags.size()>0) {
                List<String> categorias = tags.get(0).split(' - ',0);
                system.debug('DEBUG ------------------' + categorias);
                try {
                    newCase.rrss_Departamento__c = categorias[0];
                    newCase.rrss_Nivel_1__c = categorias[1];
                    newCase.rrss_Nivel_2__c = categorias[2];
                } catch (Exception e) {
                    System.debug('Error al intentar rellenar campos de etiquetado. ' + e.getMessage());
                }
            }
        }
        // Si existe una Author Label en Social Studio de 'Influencer' marcaremos el checkbox correspondiente en el caso
        String authorLabelData = String.valueOf(rawData.get(Label.RRSS_SocialPostLabel_authorTags));
        System.debug('Author Labels : ' + authorLabelData);
        if (authorLabelData != null) {
            List<String> authorLabels = authorLabelData.split(',',0);
            System.debug('Split de Author Labels: '+authorLabels);
            for (String s : authorLabels) {
                if (s == Label.RRSS_Influencer) {
                    newCase.Influencer__c = True;
                }
            }
        }
        // Si el autor del post ya está identificado, actualizaremos el Account del caso con el relacionado con el Social Persona
        if (persona != null && persona.ParentId != null) {
            if (persona.ParentId.getSObjectType() == Account.sObjectType) {
                newCase.AccountId = persona.ParentId;
            }
        }
        if (post != null && post.Provider != null) {
            newCase.Origin = post.Provider;
        }
        //Fetching the assignment rules on case
        AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType =: Label.RRSS_ObjectType_Case and Active = true limit 1];
        
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
        
        //Setting the DMLOption on Case instance
        newCase.setOptions(dmlOpts);
        insert newCase;
        return newCase;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que retorna el Parent Case en caso de encontrar alguna publicación que corresponda a la recibida
     * @param post, persona / post contendrá el contenido de la publicación, persona la Social Persona que creó
     * la publicación
     * @return Case Se retorna el caso que corresponda a una publicación anterior a la que se responde
     */
    private Case findParentCase(SocialPost post, SocialPersona persona) {
        Case parentCase = null;
        if (post.ReplyTo != null && !isReplyingToAnotherCustomer(post, persona) && !isChat(post)) {
            parentCase = findParentCaseFromPostReply(post);
            if (parentCase != null) {
                List<SocialPost> s = [SELECT Id, PersonaId FROM SocialPost WHERE Id = : parentCase.Id];
                if (!s.isEmpty()) {
                    if (s[0].PersonaId != persona.Id) {
                        parentCase = null;
                    }
                }
            }
        }
        if (parentCase == null) {
            parentCase = findParentCaseFromPersona(post, persona);
        }
        return parentCase;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que devuelve un valor booleano en función de si la respuesta recibida se ha realizado
     * por la misma Social Persona o no
     * @param post, persona / post contendrá el contenido de la publicación, persona la Social Persona que creó
     * la publicación
     * @return Boolean Se retorna True si las personas son distintas y False si son la misma
     */
    private boolean isReplyingToAnotherCustomer(SocialPost post, SocialPersona persona){
        return !post.ReplyTo.IsOutbound && post.ReplyTo.PersonaId != persona.Id;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que devuelve un valor booleano en función de si la publicación recibida es un mensaje
     * directo de twitter o si es una publicación privada de Facebook
     * @param post / post contendrá el contenido de la publicación
     * @return Boolean Se retorna True si la publicación viene de un MD de Twitter o un chat de Facebook y False si no
     */
    private boolean isChat(SocialPost post){
        return post.messageType == Label.RRSS_SocialPostType_Private || post.messageType == Label.RRSS_SocialPostType_Direct;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que devuelve un caso o un valor nulo en función de si existe un ParentCase relacionado
     * con una publicación de respuesta de un usuario desde Social Studio
     * @param post / post contendrá el contenido de la publicación
     * @return Case Se retorna el caso si se encuentra un Parent Case o null en caso contrario
     */
    private Case findParentCaseFromPostReply(SocialPost post) {
        if (post.ReplyTo != null && String.isNotBlank(post.ReplyTo.ParentId)) {
            List<Case> cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE Id = :post.ReplyTo.ParentId LIMIT 1];
            if(!cases.isEmpty()) {
                return cases[0];
            }
        }
        return null;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que devuelve un caso o un valor nulo en función de si existe un ParentCase relacionado
     * con la misma Social Persona desde Social Studio
     * @param post, persona / post contendrá el contenido de la publicación y persona contendrá la información de la
     * Social Persona que originó la publicación
     * @return Case Se retorna el caso si se encuentra un Parent Case o null en caso contrario
     */
    private Case findParentCaseFromPersona(SocialPost post, SocialPersona persona) {
        SocialPost lastestInboundPostWithSamePersonaAndRecipient = findLatestInboundPostBasedOnPersonaAndRecipient(post, persona);
        if (lastestInboundPostWithSamePersonaAndRecipient != null) {
            List<Case> cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE id = :lastestInboundPostWithSamePersonaAndRecipient.parentId LIMIT 1];
            if(!cases.isEmpty()) {
                return cases[0];
            }
        }
        return null;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que ejecuta dos funciones que verifican si la publicación recibida corresponde con el ID de R6
     * @param post / post contendrá el contenido de la publicación
     */
    private void matchPost(SocialPost post) {
        if (post.Id != null) return;
        performR6PostIdCheck(post);
        if (post.Id == null){
            performExternalPostIdCheck(post);
        }
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que verifica si existe una publicación con el Id de R6
     * @param post / post contendrá el contenido de la publicación
     */
    private void performR6PostIdCheck(SocialPost post){
        if(post.R6PostId == null) return;
        List<SocialPost> postList = [SELECT Id FROM SocialPost WHERE R6PostId = :post.R6PostId LIMIT 1];
        if (!postList.isEmpty()) {
            post.Id = postList[0].Id;
        }
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que verifica si existe una publicación con el Id externo de R6
     * @param post / post contendrá el contenido de la publicación
     */
    private void performExternalPostIdCheck(SocialPost post) {
        if (post.provider == Label.RRSS_SocialPostProvider_Facebook && post.messageType == Label.RRSS_SocialPostType_Private) return;
        if (post.provider == null || post.externalPostId == null) return;
        List<SocialPost> postList = [SELECT Id FROM SocialPost WHERE ExternalPostId = :post.ExternalPostId AND Provider = :post.provider LIMIT 1];
        if (!postList.isEmpty()) {
            post.Id = postList[0].Id;
        }
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que busca una publicación que esté relacionada con la publicación recibida en forma de respuesta
     * @param post, persona / post contendrá el contenido de la publicación y persona contendrá la información de la persona
     * @return El Social Post que originó la respuesta recibida
     */
    private SocialPost findReplyTo(SocialPost post, SocialPersona persona) {
        if(post.replyToId != null && post.replyTo == null) return findReplyToBasedOnReplyToId(post);
        if(post.responseContextExternalId != null){
            if((post.provider == Label.RRSS_SocialPostProvider_Facebook && post.messageType == Label.RRSS_SocialPostType_Private) || (post.provider == Label.RRSS_SocialPostProvider_Twitter && post.messageType == Label.RRSS_SocialPostType_Direct)){
                SocialPost replyTo = findReplyToBasedOnResponseContextExternalPostIdAndProvider(post);
                if(replyTo.id != null) return replyTo;
            }
            return findReplyToBasedOnExternalPostIdAndProvider(post);
        }
        return new SocialPost();
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que busca una publicación que esté relacionada con la publicación recibida en forma de respuesta
     * esta vez basándose en el Id recibido en el campo replyToId de Social Studio
     * @param post / post contendrá el contenido de la publicación
     * @return El Social Post que originó la respuesta recibida
     */
    private SocialPost findReplyToBasedOnReplyToId(SocialPost post){
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost WHERE id = :post.replyToId LIMIT 1];
        if(posts.isEmpty()) return new SocialPost();
        return posts[0];
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que busca una publicación que esté relacionada con la publicación recibida en forma de respuesta
     * esta vez basándose en el Id externo y el Provider recibidos en los campos ExternalPostId y Provider de Social Studio
     * @param post / post contendrá el contenido de la publicación
     * @return El Social Post que originó la respuesta recibida
     */
    private SocialPost findReplyToBasedOnExternalPostIdAndProvider(SocialPost post){
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost WHERE Provider = :post.provider AND ExternalPostId = :post.responseContextExternalId LIMIT 1];
        if(posts.isEmpty()) return new SocialPost();
        return posts[0];
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que busca una publicación que esté relacionada con la publicación recibida en forma de respuesta
     * esta vez basándose en el Id externo y el Provider recibidos en los campos responseContextExternalId y Provider de 
     * Social Studio
     * @param post / post contendrá el contenido de la publicación
     * @return El Social Post que originó la respuesta recibida
     */
    private SocialPost findReplyToBasedOnResponseContextExternalPostIdAndProvider(SocialPost post){
        List<SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost WHERE Provider = :post.provider AND responseContextExternalId = :post.responseContextExternalId ORDER BY posted DESC NULLS LAST LIMIT 1];
        if(posts.isEmpty()) return new SocialPost();
        return posts[0];
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que busca la última publicación enviada por la Social Persona
     * @param post, persona / post contendrá el contenido de la publicación y persona la información del remitente del mensaje
     * @return El Social Post que envió la persona que envía la nueva publicación o null si no se encuentra
     */
    private SocialPost findLatestInboundPostBasedOnPersonaAndRecipient(SocialPost post, SocialPersona persona) {
        if (persona != null && String.isNotBlank(persona.Id) && post != null && String.isNotBlank(post.Recipient)) {
            List<SocialPost> posts = [SELECT Id, ParentId FROM SocialPost WHERE Provider = :post.provider AND Recipient = :post.Recipient AND PersonaId = :persona.id AND IsOutbound = false ORDER BY CreatedDate DESC LIMIT 1];
            if (!posts.isEmpty()) {
                return posts[0];
            }
        }
        return null;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que busca si existe una Social Persona ya existente para una nueva persona recibida desde 
     * Social Studio y si existe rellena la información de Id y ParentId
     * @param persona / persona contendrá la información del remitente del mensaje
     */
    private void matchPersona(SocialPersona persona) {
        if (persona != null) {
            List<SocialPersona> personaList = new List<SocialPersona>();
            if(persona.Provider != Label.RRSS_SocialPostProvider_Other && String.isNotBlank(persona.ExternalId)) {
                personaList = [SELECT Id, ParentId FROM SocialPersona WHERE Provider = :persona.Provider AND ExternalId = :persona.ExternalId LIMIT 1];
            } else if(persona.Provider == Label.RRSS_SocialPostProvider_Other && String.isNotBlank(persona.ExternalId) && String.isNotBlank(persona.MediaProvider)) {
                personaList = [SELECT Id, ParentId FROM SocialPersona WHERE MediaProvider = :persona.MediaProvider AND ExternalId = :persona.ExternalId LIMIT 1];
            } else if(persona.Provider == Label.RRSS_SocialPostProvider_Other && String.isNotBlank(persona.Name) && String.isNotBlank(persona.MediaProvider)) {
                personaList = [SELECT Id, ParentId FROM SocialPersona WHERE MediaProvider = :persona.MediaProvider AND Name = :persona.Name LIMIT 1];
            }
            if (!personaList.isEmpty()) {
                persona.Id = personaList[0].Id;
                persona.ParentId = personaList[0].ParentId;
            }
        }
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que crea una Social Persona
     * @param persona / persona contendrá la información del remitente del mensaje
     */
    private void createPersona(SocialPersona persona) {
        if (persona == null || String.isNotBlank(persona.Id) || !isThereEnoughInformationToCreatePersona(persona)) return;
        SObject parent = createPersonaParent(persona);
        persona.ParentId = parent.Id;
        insert persona;
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que verifica que haya la información necesaria para crear una Social Persona
     * @param persona / persona contendrá la información del remitente del mensaje
     * @return True si hay suficiente información para crear la Social Persona o False si falta alguna información
     */
    private boolean isThereEnoughInformationToCreatePersona(SocialPersona persona) {
        return String.isNotBlank(persona.Name) && String.isNotBlank(persona.Provider) && String.isNotBlank(persona.MediaProvider);
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que verifica si se debería crear un caso en base a la publicación recibida o no
     * @param post, rawData / post contendrá el contenido de la publicación y rawData toda la información importada desde
     * Social Studio
     * @return True si se cumplen los requisitos para crear un nuevo caso o False si se debería obviar la creación de un caso
     */
    private boolean shouldCreateCase(SocialPost post, Map<String, Object> rawData) {
        return !isUnsentParent(rawData) && (!hasSkipCreateCaseIndicator(rawData));
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que verifica si el campo unsentParent se ha rellenado desde Social Studio
     * @param rawData / rawData contendrá la información importada desde Social Studio por el envío de una publicación
     * @return True si se ha recibido el parámetro "unsentParent" o False si no se ha recibido
     */
    private boolean isUnsentParent(Map<String, Object> rawData) {
        Object unsentParent = rawData.get('unsentParent');
        return unsentParent != null && 'true'.equalsIgnoreCase(String.valueOf(unsentParent));
    }
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que verifica si el campo skipCreateCase se ha rellenado desde Social Studio
     * @param rawData / rawData contendrá la información importada desde Social Studio por el envío de una publicación
     * @return True si se ha recibido el parámetro "skipCreateCase" o False si no se ha recibido
     */
    private boolean hasSkipCreateCaseIndicator(Map<String, Object> rawData) {
        Object skipCreateCase = rawData.get('skipCreateCase');
        return skipCreateCase != null && 'true'.equalsIgnoreCase(String.valueOf(skipCreateCase));
    }
    
    
 /*   global String getPersonaFirstName(SocialPersona persona) {
        String name = getPersonaName(persona);
        String firstName = '';
        if (name.contains(' ')) {
            firstName = name.substringBeforeLast(' ');
        }
        firstName = firstName.abbreviate(40);
        return firstName;
    }
    
    global String getPersonaLastName(SocialPersona persona) {
        String name = getPersonaName(persona);
        String lastName = name;
        if (name.contains(' ')) {
            lastName = name.substringAfterLast(' ');
        }
        lastName = lastName.abbreviate(80);
        return lastName;
    }
    
    private String getPersonaName(SocialPersona persona) {
        String name = persona.Name.trim();
        if (String.isNotBlank(persona.RealName)) {
            name = persona.RealName.trim();
        }
        return name;
    }
    */
    
    /**
     * @author: Ramon Diaz - Accenture
     * @description Función que crea la cuenta genérica que se usará como Parent para todas las publicaciones entrantes
     * cuya Social Persona no exista aún en el sistema
     * @param persona / persona contendrá la información del remitente del mensaje
     * @return Cuenta genérica que se asociará a la Social Persona
     */
    global virtual SObject createPersonaParent(SocialPersona persona) {
        String defaultAccountId = getDefaultAccountId();
        Account acct = new Account();
        acct.Id = defaultAccountId;
        return acct;
    }
}