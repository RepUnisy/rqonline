/**
 * Clase encargada de comprobar si hay programas de patrocinio con la firma de contrato pendiente 
 * tras haber transcurrido el tiempo de margen establecido para firmarlo, tras su aprobación, y de ser así, 
 * cambiar automáticamente el valor de un check, 
 * lo que desencadenará mediante una regla de flujo de trabajo y una alerta de correo electrónico, 
 * el envío de una notificación por email
 **/
global class PT_scheduledAlertarContratosPendienFirma implements Schedulable {

    global void execute(SchedulableContext SC) {
               
        String mensajeLog=' ';
        
        try{                        
             mensajeLog = mensajeLog+'\n@@ '+Datetime.now().format()+' INICIO proceso automático alertar programas aprobados pendiente firmar contrato @@';   
                                
             //lo inicializamos a un valor por defecto, por si no se ha configurado correctamente en la aplicación
             Integer numeroDiasMargen = 7;
                     
            mensajeLog = mensajeLog+'\n@@ Días de margen establecido por defecto: '+ numeroDiasMargen;   

            //comprobamos que el parámetro 'MargenAlertarPendienteFirmarContrato' está definido por el usuario       
            for(Configuracion_patrocinios__c configPatrocinio: [select Valor__c  from Configuracion_patrocinios__c where name =: 'MargenAlertarPendienteFirmarContrato']){

                numeroDiasMargen  =  Integer.valueOf(configPatrocinio.Valor__c);               
                mensajeLog = mensajeLog+'\n@@ Días de margen establecido en "Configuracion patrocinios" atributo "MargenAlertarPendienteFirmarContrato" (prevalece): '+ numeroDiasMargen;      
            }
  
            mensajeLog = mensajeLog+'\n@@ solo se notifica sobre programas de patrocinio que se encuentren en estado aprobado y aun no se ha indicado en su ficha contrato Sí firmado.';    
            mensajeLog = mensajeLog+'\n@@ la notificación es única y se enviará a partir de los '+numeroDiasMargen+' días desde la aprobación del programa de patrocinio.';    
             
            Patrocinios__c[] patrocinios = [select Id, name, nombre__c, avisado_pendiente_firmar_contrato__c, fecha_de_aprobacion__c 
                                             From Patrocinios__c 
                                             where estado__c =:'Programa aprobado'];
            
            for(Patrocinios__c patAux: patrocinios ){
                       
                            mensajeLog = mensajeLog+'\n@@----------------------------------------';  
                            mensajeLog = mensajeLog+'\n@@ ID Patrocinio: '+ patAux.Name+ ' nombre del programa: '+patAux.Nombre__c;                                                 
                            mensajeLog = mensajeLog+'\n@@ fecha de aprobación: '+patAux.fecha_de_aprobacion__c.format();         
                                                 
                    if (patAux.fecha_de_aprobacion__c != null && patAux.fecha_de_aprobacion__c + numeroDiasMargen == Date.today() ){     
                  
                        patAux.avisado_pendiente_firmar_contrato__c=true;
                                   
                        mensajeLog = mensajeLog+'\n@@ hoy han transcurrido '+numeroDiasMargen+' días desde su fecha de aprobación, se envía por tanto notificación.';
                    }    
                    else{
                        mensajeLog = mensajeLog+'\n@@ hoy no es la fecha para enviar notificación de este programa de patrocinio.';
                    }        
            }  
            
            update(patrocinios);   
           
            mensajeLog = mensajeLog+'\n@@ FIN proceso automático alertar programas aprobados pendiente firmar contrato @@@@@@';                        
        }      
        catch(Exception e){                           
            mensajeLog = mensajeLog+'\n@@@ ERROR clase PT_scheduledAlertarContratosPendienFirma: '+e.getMessage();          
        }  
         system.debug(mensajeLog);      
        //si para facilitar la supervision de su correcto comportamiente, se quiere recibir en una direccion de email el log de la ejecucion de este trabajo programado, 
        //descomentar esta sentencia, y establecer la dirección de correo en el método enviarEmail
       // PT_button_enviarMail.enviarEmail('LOG proceso automático alertar programas aprobados pendiente firmar contrato', mensajeLog);

    }
   
}