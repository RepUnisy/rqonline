/**
*	@name: RQO_cls_LabelTranslatorcc_test
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar el funcionamiento de la clase de traducciones de labels
*/
@IsTest
public class RQO_cls_LabelTranslatorcc_test {

    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
    }
    
    /**
	* @creation date: 21/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta dentro de la franja horaria de permiso
	* @exception: 
	* @throws:  
	*/
    static testMethod void testConstructor(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserCliente@testorg.com');
        System.runAs(u){
                       	
            System.Test.startTest();
            
            Test.setCurrentPageReference(new PageReference('Page.RQO_vf_LabelTranslator'));
			System.currentPageReference().getParameters().put('label_lang', 'label_lang');
            System.currentPageReference().getParameters().put('label', 'label');

            RQO_cls_LabelTranslator_cc translator = new RQO_cls_LabelTranslator_cc();
            
            System.Test.stopTest();
        }
	}
}