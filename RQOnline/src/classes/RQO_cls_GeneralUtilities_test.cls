/**
*	@name: RQO_cls_GeneralUtilities_test
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar el funcionamiento de la clase de utilidades generales
*/
@IsTest
public class RQO_cls_GeneralUtilities_test {

    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createDocument(1);
            RQO_cls_TestDataFactory.createContainerJunction(1);
            RQO_cls_TestDataFactory.createAsset(1);
            RQO_cls_TestDataFactory.createGrade(1);
            RQO_cls_TestDataFactory.createTranslation(1);
            RQO_cls_TestDataFactory.createContact(1);
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
		}
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método getDocumentLocale()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetDocumentLocale(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            Map<String, String> mapaReturned = genUtil.getDocumentLocale();
            
            test.stopTest();
            
            System.assertEquals('Español', mapaReturned.get('es_ES'));
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método getDocumentType()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetDocumentType(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            Map<String, String> mapaReturned = genUtil.getDocumentType();
            
            test.stopTest();
            
            System.assertEquals('Ficha Datos Seguridad ( FDS)', mapaReturned.get('Ficha Seguridad'));
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método getAssetStatus()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetAssetStatus(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            Map<String, String> mapaReturned = genUtil.getAssetStatus();
            test.stopTest();
            
            System.assertEquals('Registrado', mapaReturned.get('R'));
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método getDeliveryStatus()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetDeliveryStatus(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            Map<String, String> mapaReturned = genUtil.getDeliveryStatus();
            test.stopTest();
            
            System.assertEquals('Confirmado', mapaReturned.get('C'));
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método getTipoFacturaLocale()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetTipoFacturaLocale(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            Map<String, String> mapaReturned = genUtil.getTipoFacturaLocale();
            test.stopTest();
            
            System.assertEquals('Factura', mapaReturned.get('M'));
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método getNotificationObjectType()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetNotificationObjectType(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            Map<String, String> mapaReturned = RQO_cls_GeneralUtilities.getNotificationObjectType();
            test.stopTest();
            
            System.assertEquals('Factura', mapaReturned.get('FACTURA'));
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método getMonthName()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetMonthName(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            String return1 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_1);
            String return2 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_2);
            String return3 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_3);
            String return4 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_4);
            String return5 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_5);
            String return6 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_6);
            String return7 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_7);
            String return8 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_8);
            String return9 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_9);
            String return10 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_10);
            String return11 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_11);
            String return12 = genUtil.getMonthName(RQO_cls_Constantes.NUMBER_12);
            test.stopTest();
            
            System.assertEquals(Label.RQO_lbl_january, return1);
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método getMonthNumber()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetMonthNumber(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            Integer return1 = genUtil.getMonthNumber(Label.RQO_lbl_january);
            Integer return2 = genUtil.getMonthNumber(Label.RQO_lbl_february);
            Integer return3 = genUtil.getMonthNumber(Label.RQO_lbl_march);
            Integer return4 = genUtil.getMonthNumber(Label.RQO_lbl_april);
            Integer return5 = genUtil.getMonthNumber(Label.RQO_lbl_may);
            Integer return6 = genUtil.getMonthNumber(Label.RQO_lbl_june);
            Integer return7 = genUtil.getMonthNumber(Label.RQO_lbl_july);
            Integer return8 = genUtil.getMonthNumber(Label.RQO_lbl_august);
            Integer return9 = genUtil.getMonthNumber(Label.RQO_lbl_september);
            Integer return10 = genUtil.getMonthNumber(Label.RQO_lbl_october);
            Integer return11 = genUtil.getMonthNumber(Label.RQO_lbl_november);
            Integer return12 = genUtil.getMonthNumber(Label.RQO_lbl_december);
            test.stopTest();
            
            System.assertEquals(1, return1);
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método generateEvent()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGenerateEvent(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            genUtil.generateEvent('test', '1', '1', '1', null, null, 
                              		null, null, null, 'test', null, 'searchData', null, null, null, null);
            
            List<RQO_obj_event__c> eventList = [SELECT Id FROM RQO_obj_event__c];
            test.stopTest();
            
            System.assertEquals(1, eventList.size());
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método IsSandBox()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsSandBox(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            boolean returned = genUtil.IsSandBox();
            test.stopTest();
            
            System.assertEquals(true, returned);
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método traduccionesEnvasesUnidadMedida()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testTraduccionesEnvasesUnidadMedida(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            Map<String, String> mapReturned = genUtil.traduccionesEnvasesUnidadMedida('es_ES');
            test.stopTest();
            
            System.assertEquals(0, mapReturned.size());
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del método wrapperPedidoShippingModeMaps()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testWrapperPedidoShippingModeMaps(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            genUtil.wrapperPedidoShippingModeMaps();
            genUtil.wrapperPedidoContainerModeMaps();
            test.stopTest();            
        }
    }
    
    
	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getValidMessageTypeForContact()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetValidMessageTypeForContact(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<Contact> listContacto = [Select id from Contact Limit 1];
            test.startTest();
                RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            	Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>> returnData = genUtil.getValidMessageTypeForContact(new List<Id>{listContacto[0].Id});
            test.stopTest();
            System.assertNotEquals(null, returnData);
        }
    }
    
	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getPortalContactDataByAccountExternalId()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetPortalContactDataByAccountExternalId(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<Account> listAccount = [Select id, RQO_fld_idExterno__c from Account Limit 1];
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
            test.startTest();
            	Map<String, List<Contact>> returnData = RQO_cls_GeneralUtilities.getPortalContactDataByAccountExternalId(new List<String>{listAccount[0].RQO_fld_idExterno__c});
            test.stopTest();
            System.assertNotEquals(null, returnData);
        }
    }
    
	/**
    * @creation date: 26/02/2018
    * @author: David Iglesias - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getContainerByQP0Grade()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetContainerByQP0Grade(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            Set<Id> gradosQP0Set = new Set<Id>();
            List<RQO_obj_qp0Grade__c> qp0GradeList = [Select Id from RQO_obj_qp0Grade__c];
            for (RQO_obj_qp0Grade__c item : qp0GradeList) {
                gradosQP0Set.add(item.Id);
            }
            test.startTest();
            RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
            
            Map<Id, List<Id>> returnData = util.getContainerByQP0Grade(gradosQP0Set);
            test.stopTest();
            
            System.assertNotEquals(null, returnData);
        }
    }
    
	/**
    * @creation date: 26/02/2018
    * @author: David Iglesias - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método translate
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testTranslate(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            
            String returnData = RQO_cls_GeneralUtilities.translate(Label.RQO_lbl_Accept, 'es');
            test.stopTest();
            // testeamos assert vacio ya que desde el test no se admiten llamadas a un getContent de PageReference
            System.assertEquals('', returnData);
        }
    }
    
	/**
    * @creation date: 26/02/2018
    * @author: David Iglesias - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getContactRQO
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetContactRQO(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            List<Contact> contactList = [Select Id from Contact];
            List<String> returnData = RQO_cls_GeneralUtilities.getContactRQO();
            test.stopTest();

            System.assertEquals(contactList.get(0).Id, returnData.get(0));
        }
    }
}