/**
 *	@name: RQO_cls_UploadDocument_cc
 *	@version: 1.0
 *	@creation date: 09/10/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Controlador Apex para el componente que se encarga de hacer el upload a documentum
 *
*/
public with sharing class RQO_cls_UploadDocument_cc {
	
    /**
	* @creation date: 09/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Obtiene los documentos susceptibles de descarga agrupados por tipo de documento en un mapa 
	* @param: 
	* @return: 
	* RQO_cls_LCDocDataAux
	* @exception: 
	* @throws: 
	*/   
    @AuraEnabled
    public static void uploadDocToRepository(String idGrade, String docName, String tipoDoc, String docBody, String locale, String documentSFId){
        RQO_obj_grade__c grado = [Select id, name from RQO_obj_grade__c where id = : idGrade];
        RQO_cs_DocumentUtilities__c docUtil = RQO_cs_DocumentUtilities__c.getInstance(tipoDoc);
        String documentumTypeDoc = (docUtil != null) ? docUtil.RQO_fld_documentumDoctype__c : tipoDoc;
        String decodeDocBody = EncodingUtil.urlDecode(docBody, 'UTF-8');
        RQO_cls_Documentum docUp = new RQO_cls_Documentum();
       	RQO_cls_Documentum.RQO_cls_documentumResponse responseDoc = docUp.documetumOp(RQO_cls_Constantes.WS_DOCUM_CREATE, RQO_cls_Constantes.DOC_CS_REPOSITORY, null, docName, documentumTypeDoc, decodeDocBody, grado.name, locale, true);
        if (responseDoc != null && String.IsNotEmpty(responseDoc.docId)){
            //Upsert del document asociado al grado
            System.debug('Response: ' + responseDoc.docId);
            RQO_obj_document__c docObj = new RQO_obj_document__c(Id = documentSFId, RQO_fld_documentId__c = responseDoc.docId, RQO_fld_grade__c = idGrade, 
                                                                RQO_fld_type__c = tipoDoc, RQO_fld_locale__c = locale);
            
            System.debug('Actualizado');
            upsert docObj;
           	System.debug('Actualizado');
        }
    }
    
     /**
	* @creation date: 09/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Carga el picklist de tipo de documentos excluyendo aquellos que no esten marcado en el custom settings RQO_cs_DocumentUtilities__c como
	* 	AllowUpload
	* @param: 
	* @return: Map<String, String>, mapa con los valores código - valor que se van a mostrar en la lista de selección
	* @exception: 
	* @throws: 
	*/   
    @AuraEnabled
    public static Map<String, String> loadPicklistDocType(){ 
        RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
        Map<String, String> mapReturn = util.getDocumentType();
        RQO_cs_DocumentUtilities__c objAux = null;
        for(String key : mapReturn.keySet()){
            objAux = RQO_cs_DocumentUtilities__c.getInstance(key);
            if (objAux == null || !objAux.RQO_fld_allowUpload__c){
                mapReturn.remove(key);
            }
        }
        return mapReturn;
    }


     /**
	* @creation date: 09/01/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Carga el picklist de idiomas de documento
	* @param: 
	* @return: Map<String, String>, mapa con los valores código - valor que se van a mostrar en la lista de selección
	* @exception: 
	* @throws: 
	*/   
    @AuraEnabled
    public static Map<String, String> loadPicklistDocLocale(){ 
        RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
        return util.getDocumentLocale();
    }
    
    /**
	* @creation date: 09/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Valida si ya existe el tipo de documento en ese idioma asociado al grado.
	* @param: Param1 - idGrade tipo String, id del grado. Param2 - docType tipo String, tipo de documento. Param3 - locale tipo String, idioma del documento
	* @return: Map<String, String>, mapa con los valores código - valor que se van a mostrar en la lista de selección
	* @exception: 
	* @throws: 
	*/ 
    @AuraEnabled
    public static String validateDocExist(String idGrade, String docType, String locale){
        for (RQO_obj_document__c objDoc : [select Id from RQO_obj_document__c where RQO_fld_grade__c = : idGrade 
                                           and RQO_fld_type__c = : docType and RQO_fld_locale__c = : locale]){
            return objDoc.Id;
        }
        return null;
    }
}