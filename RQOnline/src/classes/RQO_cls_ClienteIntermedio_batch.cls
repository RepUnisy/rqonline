/**
 *	@name: RQO_cls_ClienteIntermedio_batch
 *	@version: 1.0
 *	@creation date: 18/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase ejecutada a traves de job programado para el volcado del objeto intermedio de clientes
 */
global class RQO_cls_ClienteIntermedio_batch implements Database.Batchable<sObject>, Database.Stateful {
    
    // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_ClienteIntermedio_batch';
    
    // ***** VARIABLES ***** //
    private map<String, Id> sobjectRelationMap = null;
    private map<String, RQO_cs_parametrizacionObjBulk__c> tablaMaestraMap = null;
    private map<String, RQO_cs_parametrizacionRTAccount__c> tablaRTAccountMap = null;
    private map<String, Id> orgVentasMap = null;
	private List<RQO_obj_logProcesos__c> logObjectList = null;
	private String tipoDato = null;
	//private Boolean initial = true;
    
    // ***** CONSTRUCTORES ***** //
    public RQO_cls_ClienteIntermedio_batch () {
        this.sobjectRelationMap = new map<String, Id> ();
        this.orgVentasMap = new map<String, Id> ();
		this.logObjectList = new List<RQO_obj_logProcesos__c> ();
		this.tipoDato = RQO_cls_Constantes.NUMBER_01;
    }
	
	public RQO_cls_ClienteIntermedio_batch (map<String, Id> sobjectRelationMap, String tipoDato) {
        this.sobjectRelationMap = sobjectRelationMap;
        this.orgVentasMap = new map<String, Id> ();
		this.logObjectList = new List<RQO_obj_logProcesos__c> ();
		this.tipoDato = tipoDato;
    }
    
    // ***** METODO GLOBALES ***** //

	/**
	* @creation date: 18/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Realiza el volcado del objeto intermedio a Clientes y sus relaciones 
	* @param: Database.BatchableContext
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
    global Database.QueryLocator start(Database.BatchableContext context) {
        
        /**
        * Constantes
        */
        final String METHOD = 'start';

        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

        String query = 'SELECT Id, RQO_fld_tipo__c, RQO_fld_idExterno__c, RQO_fld_idRelacion__c, RQO_fld_isDelete__c, '+
							'RQO_fld_columna1__c, RQO_fld_columna2__c, RQO_fld_columna3__c, RQO_fld_columna4__c, '+
                            'RQO_fld_columna5__c, RQO_fld_columna6__c, RQO_fld_columna7__c, RQO_fld_columna8__c, '+
                            'RQO_fld_columna9__c, RQO_fld_columna10__c, RQO_fld_columna11__c, RQO_fld_columna12__c, '+
                            'RQO_fld_columna13__c, RQO_fld_columna14__c, RQO_fld_columna15__c, RQO_fld_columna16__c, '+
                            'RQO_fld_columna17__c, RQO_fld_columna18__c, RQO_fld_columna19__c, RQO_fld_columna20__c, '+
                            'RQO_fld_columna21__c, RQO_fld_columna22__c, RQO_fld_columna23__c, RQO_fld_columna24__c, '+
                            'RQO_fld_columna25__c '+
                        'FROM RQO_obj_auxBulkObject__c ' +
            			'WHERE RQO_fld_procesado__c = false AND RQO_fld_tipo__c = :tipoDato '+
            			'ORDER BY RQO_fld_tipo__c';
        
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');   
        return Database.getQueryLocator(query);
    }
    
    /**
	* @creation date: 18/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Ejecucion por tramas del volcado del objeto intermedio a Cliente y relacionados 
	* @param: Database.BatchableContext
	* @param: List<RQO_obj_auxBulkObject__c>
	* @return: 	
	* @exception: 
	* @throws: 
	*/
    global void execute(Database.BatchableContext context, List<RQO_obj_auxBulkObject__c> scope) {
        
        /**
        * Constantes
        */
        final String METHOD = 'execute';
        
        /**
		 * Variables
		 */
        RQO_cs_parametrizacionObjBulk__c mapeoDatos = null;
        map <String, List<RQO_obj_auxBulkObject__c>> subListasMap = null;
        map <String, Id> accountRelMap = null;
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		// Se limpia la lista del log de errores en cada trama
		this.logObjectList.clear();
        
        // Obtención de la tabla de mapeo de tipos de dato
        if (this.tablaMaestraMap == null || this.tablaMaestraMap.isEmpty()) {
            this.tablaMaestraMap = RQO_cls_CustomSettingUtil.getParametrizacionObjBulk();
        }
        
        // Obtención de la tabla de mapeo de Record Type para Account
        if (this.tablaRTAccountMap == null || this.tablaRTAccountMap.isEmpty()) {
        	this.tablaRTAccountMap = RQO_cls_CustomSettingUtil.getParametrizacionRTAccount();
    	}
		
        //Obtención de las organizaciones de venta
		if (this.orgVentasMap == null || this.orgVentasMap.isEmpty()) {           
            for (RQO_obj_organizaciondeVentas__c item : [SELECT Id, RQO_fld_codeOrganization__c, RQO_fld_canal__c, RQO_fld_sector__c 
                                                           FROM RQO_obj_organizaciondeVentas__c]) {
                this.orgVentasMap.put(item.RQO_fld_codeOrganization__c+item.RQO_fld_canal__c+item.RQO_fld_sector__c, item.Id);
            }
        }
        
		// Separacion de la lista de objetos estandar en sublistas de objetos tipo
		subListasMap = sortingSubList(scope);     
        
        // Tratamiento de listas de objetos unicos
        for (String item : subListasMap.keySet()) {
            if (subListasMap.get(item) != null) {
                mapeoDatos = this.tablaMaestraMap.get(item);
            	mappingData(subListasMap.get(item), item, mapeoDatos, accountRelMap);
            }
        }
        
        // Actualización de registros insertados/actualizados correctamente de la tabla intermedia
        updateRegistrosIntermedios(subListasMap);
		
		// Insercion de logs en el caso de existir
		if (this.logObjectList != null && !this.logObjectList.isEmpty()) {
			RQO_cls_ProcessLog.generateLogData(this.logObjectList);
		}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
    }
    
	/**
	* @creation date: 18/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Finalización del execute y arranque secuencial de si mismo
	* @param: Database.BatchableContext
	* @return: 	
	* @exception: 
	* @throws: 
	*/
    global void finish(Database.BatchableContext context) {
		
		/**
        * Constantes
        */
        final String METHOD = 'finish';
		
		/**
        * Variables
        */
        String tipoDato;
		Integer tipoDatoInt;
		
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		// Cálculo del valor del tipo de dato a tratar
		tipoDatoInt = Integer.valueOf(this.tipoDato);
		tipoDato = String.valueOf(++tipoDatoInt).leftPad(2, RQO_cls_Constantes.NUMBER_0);
		
		// Lanzamiento de forma secuencial el mismo Batch para el tratamiento de los distintos tipos de datos.
		// Esto es debido a el problema acarreado por SF en relación a los bloques que contienen las tramas,
		// no se puede asegurar que sean tratadas en el orden indicado en la consulta del start.
		if (tipoDatoInt < Integer.valueOf(RQO_cls_Constantes.NUMBER_10)) {
			Database.executeBatch(new RQO_cls_ClienteIntermedio_batch (this.sobjectRelationMap, tipoDato), 200);
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}
	
	// ***** METODO PRIVADOS ***** //
	
    /**
	* @creation date: 11/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Mapeo de datos de la lista de objetos RQO_obj_auxBulkObject__c a objeto final
	* @param: objectList		Lista a mapear
	* @param: tipoObjeto		tipo de objeto tratado
	* @param: operacion			datos de tabla maestra
	* @param: accountRelMap		map Account: key --> External Id / value --> Id
	* @return: 	
	* @exception: 
	* @throws: 
	*/
	private void mappingData (List <RQO_obj_auxBulkObject__c> objectList, String tipoObjeto, RQO_cs_parametrizacionObjBulk__c mapeoDatos, map<String, Id> accountRelMap) {
        
        /**
        * Constantes
        */
        final String METHOD = 'mappingData';
		
        /**
        * Variables
        */
        List <sObject> objectListDML = new List <sObject> ();
		Map<String, Schema.RecordTypeInfo> accountRecordTypesMap = null;
		RQO_obj_logProcesos__c logObject = null;
		Boolean error = false;
		Boolean datoContAccount = false;
        
        /**
        * Inicio
        */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        // Si es tipo objeto Account obtenemos los Record Types
        if (tipoObjeto.equals(Label.RQO_lbl_accountIntegracion)) {
        	accountRecordTypesMap = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
    	}

        for (RQO_obj_auxBulkObject__c item : objectList) {
            try {
				logObject = null;
				error = false;
				datoContAccount = false;
                sObject objeto = createObject(tipoObjeto);
                if (String.isNotBlank(item.RQO_fld_idExterno__c)) {                   
                    objeto.put(Label.RQO_lbl_idExternoIntegracion, item.RQO_fld_idExterno__c);
					
					// Si el registro no tiene valor relacion (RQO_fld_idRelacion__c), se trata de objeto principal
					// Si el registro tiene padre obtenemos key (RQO_fld_idRelacion__c) para el mapa sobjectRelationMap
                    if (String.isBlank(item.RQO_fld_idRelacion__c) || this.sobjectRelationMap.get(getKeyMapByObjectType(item.RQO_fld_idRelacion__c, tipoObjeto)) != null 
                       || (tipoObjeto.equals(Label.RQO_lbl_datoContactoIntegracion) && this.sobjectRelationMap.get(getKeyMapByObjectType(item.RQO_fld_idRelacion__c, '')) != null)) {
                        
                        if (String.isNotBlank(item.RQO_fld_idRelacion__c)) {
                            String field = null;
                            if (tipoObjeto.equals(Label.RQO_lbl_contactIntegracion)) {
                                field = Label.RQO_lbl_accountIdIntegracion;
                            } else {
								if (tipoObjeto.equals(Label.RQO_lbl_datoContactoIntegracion) && String.isNotBlank(item.RQO_fld_columna5__c)) {
									field = Label.RQO_lbl_AccountDC_Int;
									datoContAccount = true;
								} else{
									field = Label.RQO_lbl_idRelacionIntegracion;
								}
                            }
							// Caso de mapeo especial, RQO_fld_idRelacion__c puede apuntar a distintos objetos en el tipo 06 (Dato de Contacto)
							// Si datoContAccount = true la relación apunta a Account, si no a Contact
							if (datoContAccount) {
								objeto.put(field, this.sobjectRelationMap.get(getKeyMapByObjectType(item.RQO_fld_idRelacion__c, '')));
								datoContAccount = false;
							} else {
								objeto.put(field, this.sobjectRelationMap.get(getKeyMapByObjectType(item.RQO_fld_idRelacion__c, tipoObjeto)));
							}
                            
                        }
						
						// Comprobación de si se trata de borrado lógico
						if (String.isNotBlank(item.RQO_fld_isDelete__c)) {
							objeto.put(Label.RQO_lbl_isDeleteIntegracion, true);
						} else {
							
							// Cubre el caso de un desbloqueo...
							objeto.put(Label.RQO_lbl_isDeleteIntegracion, false);
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna1__c)) {
							// Si es Relacion de ventas seteamos el objeto relacionado Account
								if (tipoObjeto.equals(Label.RQO_lbl_relacionVentasIntegracion)) {
									objeto.put(mapeoDatos.RQO_fld_columna1__c, this.sobjectRelationMap.get(getKeyMapByObjectType(item.RQO_fld_columna1__c, '')));
								} else if (tipoObjeto.equals(Label.RQO_lbl_areaVentasIntegracion)) {
									objeto.put(mapeoDatos.RQO_fld_columna1__c, this.orgVentasMap.get(item.RQO_fld_columna1__c+item.RQO_fld_columna2__c+item.RQO_fld_columna3__c));
								} else {
									objeto.put(mapeoDatos.RQO_fld_columna1__c, item.RQO_fld_columna1__c);
								}
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna2__c)) {
								if (tipoObjeto.equals(Label.RQO_lbl_datoContactoIntegracion)) {
									if (String.isBlank(item.RQO_fld_columna2__c)) {
										objeto.put(mapeoDatos.RQO_fld_columna2__c, false);
									} else {
										objeto.put(mapeoDatos.RQO_fld_columna2__c, true);
									}
								} else {
									objeto.put(mapeoDatos.RQO_fld_columna2__c, item.RQO_fld_columna2__c);
								}
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna3__c)) {
								// Si es tipo Account se concatenan columnas para generar el name
								if (tipoObjeto.equals(Label.RQO_lbl_accountIntegracion)) {
									String name = item.RQO_fld_columna3__c;
									if (String.isNotBlank(item.RQO_fld_columna4__c)) {
											 name = name + RQO_cls_Constantes.SPACE + item.RQO_fld_columna4__c;
									}
									objeto.put(mapeoDatos.RQO_fld_columna3__c, name);
								} else {
									objeto.put(mapeoDatos.RQO_fld_columna3__c, item.RQO_fld_columna3__c);
								}
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna4__c) && !tipoObjeto.equalsIgnoreCase(Label.RQO_lbl_accountIntegracion)) {
								if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_08) && String.isNotBlank(item.RQO_fld_columna4__c)) {
									Date fecha = RQO_cls_FormatUtils.obtenerFechaYYYYMMDD(item.RQO_fld_columna4__c);
									if (fecha != null) {
										objeto.put(mapeoDatos.RQO_fld_columna4__c, fecha);
									} else {
										error = true;
										logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(tipoObjeto, RQO_cls_Constantes.LOG_DATA_ERROR, 
																								Label.RQO_lbl_mensajeErrorFechaIntegracion + item.RQO_fld_columna4__c, 
																								null, item.RQO_fld_idExterno__c, RQO_cls_Constantes.BATCH_CLIENTES);
										this.logObjectList.add(logObject);
										System.debug(Label.RQO_lbl_mensajeErrorFechaIntegracion + item.RQO_fld_columna4__c);
									}
								} else if (tipoObjeto.equalsIgnoreCase(Label.RQO_lbl_datoContactoIntegracion)) { 
									Schema.RecordTypeInfo devRecordTypeInfo = Schema.SObjectType.RQO_obj_datodeContacto__c.getRecordTypeInfosByName().get(item.RQO_fld_columna4__c);
									if (devRecordTypeInfo != null) {
										objeto.put(Label.RQO_lbl_recordTypeIdIntegracion, devRecordTypeInfo.getRecordTypeId());
									} else {
										error = true;
										logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(tipoObjeto, RQO_cls_Constantes.LOG_DATA_ERROR, 
																								Label.RQO_lbl_mensajeErrorRecordTypeIntegracion + item.RQO_fld_columna4__c, 
																								null, item.RQO_fld_idExterno__c, RQO_cls_Constantes.BATCH_CLIENTES);
										this.logObjectList.add(logObject);
										System.debug(Label.RQO_lbl_mensajeErrorRecordTypeIntegracion + item.RQO_fld_columna4__c);
									}
								} else if (tipoObjeto.equalsIgnoreCase(Label.RQO_lbl_contactIntegracion) && String.isBlank(item.RQO_fld_columna4__c)) {
									objeto.put(mapeoDatos.RQO_fld_columna4__c, RQO_cls_Constantes.HYPHEN);
								} else {
									objeto.put(mapeoDatos.RQO_fld_columna4__c, item.RQO_fld_columna4__c);
								}
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna5__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna5__c, item.RQO_fld_columna5__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna6__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna6__c, item.RQO_fld_columna6__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna7__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna7__c, item.RQO_fld_columna7__c);							
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna8__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna8__c, item.RQO_fld_columna8__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna9__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna9__c, item.RQO_fld_columna9__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna10__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna10__c, item.RQO_fld_columna10__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna11__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna11__c, item.RQO_fld_columna11__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna12__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna12__c, item.RQO_fld_columna12__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna13__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna13__c, item.RQO_fld_columna13__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna14__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna14__c, item.RQO_fld_columna14__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna15__c)) {
                                // Si es un cliente se verifica Record Type
								if (tipoObjeto.equals(Label.RQO_lbl_accountIntegracion)) {
									// Comprobación de Record Type configurado en Custom Settings
									if (this.tablaRTAccountMap.get(item.RQO_fld_columna15__c) != null) {
										Schema.RecordTypeInfo sc = accountRecordTypesMap.get(this.tablaRTAccountMap.get(item.RQO_fld_columna15__c).RQO_fld_recordType__c);
										objeto.put(Label.RQO_lbl_recordTypeIdIntegracion, sc.getRecordTypeId());
									} else {
										error = true;
										logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(tipoObjeto, RQO_cls_Constantes.LOG_DATA_ERROR, 
																								Label.RQO_lbl_mensajeConfigRecordTypeIntegracion + item.RQO_fld_columna15__c, 
																								null, item.RQO_fld_idExterno__c, RQO_cls_Constantes.BATCH_CLIENTES);
										this.logObjectList.add(logObject);
										System.debug(Label.RQO_lbl_mensajeConfigRecordTypeIntegracion + item.RQO_fld_columna15__c);
									}
								} else {
									objeto.put(mapeoDatos.RQO_fld_columna15__c, item.RQO_fld_columna15__c);
								}
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna16__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna16__c, item.RQO_fld_columna16__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna17__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna17__c, item.RQO_fld_columna17__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna18__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna18__c, item.RQO_fld_columna18__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna19__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna19__c, item.RQO_fld_columna19__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna20__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna20__c, item.RQO_fld_columna20__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna21__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna21__c, item.RQO_fld_columna21__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna22__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna22__c, item.RQO_fld_columna22__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna23__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna23__c, item.RQO_fld_columna23__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna24__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna24__c, item.RQO_fld_columna24__c);
							}
							
							if (String.isNotBlank(mapeoDatos.RQO_fld_columna25__c)) {
								objeto.put(mapeoDatos.RQO_fld_columna25__c, item.RQO_fld_columna25__c);
							}
						}
						
                        // Lista de objetos a insertar/actualizar
						if (!error) {
							objectListDML.add(objeto);
						}
                        
                    } else {
						logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(tipoObjeto, RQO_cls_Constantes.LOG_DATA_ERROR, 
																				Label.RQO_lbl_mensajeErrorRelacionIntegracion, 
																				null, item.RQO_fld_idExterno__c, RQO_cls_Constantes.BATCH_CLIENTES);
						this.logObjectList.add(logObject);
						System.debug(Label.RQO_lbl_mensajeErrorRelacionIntegracion);
                    }
            	} else {
					logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(tipoObjeto, RQO_cls_Constantes.LOG_DATA_ERROR, 
																			Label.RQO_lbl_mensajeErrorExternalIdIntegracion, 
																			null, null, RQO_cls_Constantes.BATCH_CLIENTES);
					this.logObjectList.add(logObject);
					System.debug(Label.RQO_lbl_mensajeErrorExternalIdIntegracion);
            	}

            } catch (SObjectException sObjExc) {
				logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(tipoObjeto, sObjExc.getTypeName(), 
																		Label.RQO_lbl_mensajeErrorNomenclaturaIntegracion, sObjExc.getMessage(), 
                                                                      	item.RQO_fld_idExterno__c, RQO_cls_Constantes.BATCH_CLIENTES);
				this.logObjectList.add(logObject);
				System.debug(Label.RQO_lbl_mensajeErrorNomenclaturaIntegracion);
            } catch (Exception e) {
				logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(tipoObjeto, e.getTypeName(), 
																		Label.RQO_lbl_mensajeErrorExceptionGenericoIntegracion, e.getMessage(), 
                                                                      	item.RQO_fld_idExterno__c, RQO_cls_Constantes.BATCH_CLIENTES);
				this.logObjectList.add(logObject);
				System.debug(Label.RQO_lbl_mensajeErrorExceptionGenericoIntegracion+' - '+e.getMessage());
            }
			
        }
        
        // Ejecucion de operacion DML
        if (!objectListDML.isEmpty()) {
            insertOrUpdateData(objectListDML, tipoObjeto);
        } 

	}
    
    /**
	* @creation date: 11/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Insercion/actualizacion de la lista de objetos RQO_obj_auxBulkObject__c
	* @param: objectList		Lista a actualizar
	* @param: tipoObjeto		Tipo de objeto a tratar
	* @return: 	
	* @exception: 
	* @throws: 
	*/
    private void insertOrUpdateData (List <sObject> objectListDML, String tipoObjeto) {
        
        /**
        * Constantes
        */
        final String METHOD = 'insertOrUpdateData';
		
        /**
        * Variables
        */
        Schema.SObjectField f = null;
        Database.UpsertResult[] srListResult = null;
        Set<String> objetosOKSet = new Set<String> ();
		RQO_obj_logProcesos__c logObject = null;
        
        /**
        * Inicio
        */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		// Validacion del tipo de objeto para casting de la lista de sObject
		if (tipoObjeto.equals(Label.RQO_lbl_accountIntegracion)) {
			List<Account> accountList = new List<Account> ();
			for (sObject item : objectListDML) {
				Account acc = (Account) item;
				accountList.add(acc);
			}
			f = Account.Fields.RQO_fld_idExterno__c;
			srListResult = Database.upsert(accountList, f, false);
		} else if (tipoObjeto.equals(Label.RQO_lbl_identificadorIntegracion)) {
			List<RQO_obj_identificador__c> identificadorList = new List<RQO_obj_identificador__c> ();
			for (sObject item : objectListDML) {
				RQO_obj_identificador__c ide = (RQO_obj_identificador__c) item;
				identificadorList.add(ide);
			}
			f = RQO_obj_identificador__c.Fields.RQO_fld_idExterno__c;
			srListResult = Database.upsert(identificadorList, f, false);
		} else if (tipoObjeto.equals(Label.RQO_lbl_areaVentasIntegracion)) {
			List<RQO_obj_areadeVentas__c> areaVentasList = new List<RQO_obj_areadeVentas__c> ();
			for (sObject item : objectListDML) {
				RQO_obj_areadeVentas__c are = (RQO_obj_areadeVentas__c) item;
				areaVentasList.add(are);
			}
			f = RQO_obj_areadeVentas__c.Fields.RQO_fld_idExterno__c;
			srListResult = Database.upsert(areaVentasList, f, false);
		} else if (tipoObjeto.equals(Label.RQO_lbl_relacionVentasIntegracion)) {
			List<RQO_obj_relaciondeVentas__c> relacionVentasList = new List<RQO_obj_relaciondeVentas__c> ();
			for (sObject item : objectListDML) {
				RQO_obj_relaciondeVentas__c rel = (RQO_obj_relaciondeVentas__c) item;
				relacionVentasList.add(rel);
			}
			f = RQO_obj_relaciondeVentas__c.Fields.RQO_fld_idExterno__c;
			srListResult = Database.upsert(relacionVentasList, f, false);
		} else if (tipoObjeto.equals(Label.RQO_lbl_contactIntegracion)) {
			List<Contact> contactList = new List<Contact> ();
			for (sObject item : objectListDML) {
				Contact con = (Contact) item;
				contactList.add(con);
			}
			f = Contact.Fields.RQO_fld_idExterno__c;
			srListResult = Database.upsert(contactList, f, false);
		} else if (tipoObjeto.equals(Label.RQO_lbl_datoContactoIntegracion)) {
			List<RQO_obj_datodeContacto__c> datosContactoList = new List<RQO_obj_datodeContacto__c> ();
			for (sObject item : objectListDML) {
				RQO_obj_datodeContacto__c dC = (RQO_obj_datodeContacto__c) item;
				datosContactoList.add(dC);
			}
			f = RQO_obj_datodeContacto__c.Fields.RQO_fld_idExterno__c;
			srListResult = Database.upsert(datosContactoList, f, false);
		} /*else if (tipoObjeto.equals(Label.RQO_lbl_mailIntegracion)) {
			List<RQO_obj_mail__c> mailList = new List<RQO_obj_mail__c> ();
			for (sObject item : objectListDML) {
				RQO_obj_mail__c mail = (RQO_obj_mail__c) item;
				mailList.add(mail);
			}
			f = RQO_obj_mail__c.Fields.RQO_fld_idExterno__c;
			srListResult = Database.upsert(mailList, f, false);
		}*/ else if (tipoObjeto.equals(Label.RQO_lbl_datosConsentimientoIntegracion)) {
			List<RQO_obj_datosdeConsentimiento__c> datosConsentimientoList = new List<RQO_obj_datosdeConsentimiento__c> ();
			for (sObject item : objectListDML) {
				RQO_obj_datosdeConsentimiento__c dat = (RQO_obj_datosdeConsentimiento__c) item;
				datosConsentimientoList.add(dat);
			}
			f = RQO_obj_datosdeConsentimiento__c.Fields.RQO_fld_idExterno__c;
			srListResult = Database.upsert(datosConsentimientoList, f, false);
		} else if (tipoObjeto.equals(Label.RQO_lbl_atributoMarketingIntegracion)) {
			List<RQO_obj_atributodeMarketing__c> atrMarketingList = new List<RQO_obj_atributodeMarketing__c> ();
			for (sObject item : objectListDML) {
				RQO_obj_atributodeMarketing__c atr = (RQO_obj_atributodeMarketing__c) item;
				atrMarketingList.add(atr);
			}
			f = RQO_obj_atributodeMarketing__c.Fields.RQO_fld_idExterno__c;
			srListResult = Database.upsert(atrMarketingList, f, false);
		} 
 
        // Iteracion del resultado de la operacion DML
        for (Database.UpsertResult sr : srListResult) {
        	if (sr.isSuccess()) {
            	objetosOKSet.add(sr.getId());
                System.debug('sr.getId(): '+sr.getId());
            } else {
                for(Database.Error err : sr.getErrors()) {
					logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(tipoObjeto, String.valueOf(err.getStatusCode()), 
																			String.valueOf(err.getFields()), 
																			err.getMessage(), null, RQO_cls_Constantes.BATCH_CLIENTES);
					if (logObject != null) {
						this.logObjectList.add(logObject);
					}
					System.debug(err.getMessage());
                }
            }
        }
        
        // Asociacion de registros correctos mediante External ID --> SF ID
        for (sObject item : objectListDML) {
        	if (objetosOKSet.contains(item.Id)) {
				System.debug('Asociacion tipoObjeto: '+tipoObjeto);
				System.debug('item.get: '+(String) item.get('RQO_fld_idExterno__c'));
            	this.sobjectRelationMap.put(tipoObjeto + RQO_cls_Constantes.HYPHEN + (String) item.get('RQO_fld_idExterno__c'), item.Id);
            }
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
    }
	
    /**
	* @creation date: 25/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Separacion de la lista de objetos estandar en sublistas de objetos tipo
	* @param:	List<RQO_obj_auxBulkObject__c>					Lista a dividir en sublistas
	* @return: 	map <String, List<RQO_obj_auxBulkObject__c>>		Mapa de sublistas con clave de objeto tipo
	* @exception: 
	* @throws: 
	*/
	private map <String, List<RQO_obj_auxBulkObject__c>> sortingSubList (List<RQO_obj_auxBulkObject__c> scope) {
		
		/**
        * Constantes
        */
        final String METHOD = 'sortingSubList';
		
		/**
		* Variables
		*/
        map <String, List<RQO_obj_auxBulkObject__c>> subListasMap = new map <String, List<RQO_obj_auxBulkObject__c>> ();
		RQO_obj_logProcesos__c logObject = null;
		
        /**
		* Listas para el objeto global de cliente y relaciones (tipos:  1,2,3,4,5,6,7,8,9)
		*/
        List<RQO_obj_auxBulkObject__c> clientesList = null;
        List<RQO_obj_auxBulkObject__c> identificadoresList = null;
        List<RQO_obj_auxBulkObject__c> areasVentasList = null;
        List<RQO_obj_auxBulkObject__c> relacionesList = null;
        List<RQO_obj_auxBulkObject__c> contactosList = null;
        List<RQO_obj_auxBulkObject__c> datosContactoList = null;
        List<RQO_obj_auxBulkObject__c> mailList = null;
        List<RQO_obj_auxBulkObject__c> datosConsentimientoList = null;
        List<RQO_obj_auxBulkObject__c> atrMarketingList = null;
		
		/**
		* Inicio
		*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		for (RQO_obj_auxBulkObject__c item : scope) {
			
			logObject = null;
			system.debug('XXX --> '+item.RQO_fld_tipo__c);
			if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_01)) {
				if (clientesList == null) {
					clientesList = new List<RQO_obj_auxBulkObject__c> ();
				}
				clientesList.add(item);
				subListasMap.put(Label.RQO_lbl_accountIntegracion, clientesList);
			} else if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_02)) {
				if (identificadoresList == null) {
					identificadoresList = new List<RQO_obj_auxBulkObject__c> ();
				}
				identificadoresList.add(item);
				subListasMap.put(Label.RQO_lbl_identificadorIntegracion, identificadoresList);
			} else if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_03)) {
				if (areasVentasList == null) {
					areasVentasList = new List<RQO_obj_auxBulkObject__c> ();
				}
				areasVentasList.add(item);
				subListasMap.put(Label.RQO_lbl_areaVentasIntegracion, areasVentasList);
			} else if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_04)) {
				if (relacionesList == null) {
					relacionesList = new List<RQO_obj_auxBulkObject__c> ();
				}
				relacionesList.add(item);
				subListasMap.put(Label.RQO_lbl_relacionVentasIntegracion, relacionesList);
			} else if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_05)) {
				if (contactosList == null) {
					contactosList = new List<RQO_obj_auxBulkObject__c> ();
				}
				contactosList.add(item);
				subListasMap.put(Label.RQO_lbl_contactIntegracion, contactosList);
			} else if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_06)) {
				if (datosContactoList == null) {
					datosContactoList = new List<RQO_obj_auxBulkObject__c> ();
				}
				datosContactoList.add(item);
				subListasMap.put(Label.RQO_lbl_datoContactoIntegracion, datosContactoList);
			} /*else if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_07)) {
				if (mailList == null) {
					mailList = new List<RQO_obj_auxBulkObject__c> ();
				}
				mailList.add(item);
				subListasMap.put(Label.RQO_lbl_mailIntegracion, mailList);
			} */else if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_08)) {
				if (datosConsentimientoList == null) {
					datosConsentimientoList = new List<RQO_obj_auxBulkObject__c> ();
				}
				datosConsentimientoList.add(item);
				subListasMap.put(Label.RQO_lbl_datosConsentimientoIntegracion, datosConsentimientoList);
			} else if (item.RQO_fld_tipo__c.equals(RQO_cls_Constantes.NUMBER_09)) {
				if (atrMarketingList == null) {
					atrMarketingList = new List<RQO_obj_auxBulkObject__c> ();
				}
				atrMarketingList.add(item);
				subListasMap.put(Label.RQO_lbl_atributoMarketingIntegracion, atrMarketingList);
			} else {
				logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(null, RQO_cls_Constantes.LOG_DATA_ERROR, 
																		Label.RQO_lbl_mensaleErrorTipoObjetoIntegracion + item.RQO_fld_tipo__c, 
																		null, item.RQO_fld_idExterno__c, RQO_cls_Constantes.BATCH_CLIENTES);
				System.debug(Label.RQO_lbl_mensaleErrorTipoObjetoIntegracion + item.RQO_fld_tipo__c);
			}
			
			if (logObject != null) {
				this.logObjectList.add(logObject);
			}
        }
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return subListasMap;
	}
    
	/**
	* @creation date: 25/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Borrado de registros tratados correctamente en la tabla intermedia
	* @param:	scope	Lista de objetos intermedios tratados por trama
	* @return: 	
	* @exception: 
	* @throws: 
	*/
    private void updateRegistrosIntermedios(map <String, List<RQO_obj_auxBulkObject__c>> subListasMap) {
        
        /**
        * Constantes
        */
        final String METHOD = 'updateRegistrosIntermedios';
		
		/**
		* Variables
		*/
        List<RQO_obj_auxBulkObject__c> updateProccesList = new List<RQO_obj_auxBulkObject__c> ();
        RQO_obj_logProcesos__c logObject = null;
        
        /**
		* Inicio
		*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        try {
			
			for (String item : subListasMap.keySet()) {
				
				List<RQO_obj_auxBulkObject__c> auxList = subListasMap.get(item);
				
				// Se marca el flag de los registros procesados
				for (RQO_obj_auxBulkObject__c aux : auxList) {
					aux.RQO_fld_procesado__c = true;
					// Si el registro se ha registrado de forma correcta marcamos el check de verificado
					if (this.sobjectRelationMap.get(item + RQO_cls_Constantes.HYPHEN + aux.RQO_fld_idExterno__c) != null) {
						aux.RQO_fld_verificado__c = true;
					}
				}
				
				updateProccesList.addAll(auxList);
			}
            
            update updateProccesList;
            
        } catch (DmlException dml) {
			logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(null, RQO_cls_Constantes.LOG_DATA_ERROR, 
																	Label.RQO_lbl_mensajeErrorExceptionDMLIntegracion, 
																	dml.getMessage(), null, RQO_cls_Constantes.BATCH_CLIENTES);
			System.debug(Label.RQO_lbl_mensajeErrorExceptionDMLIntegracion);
        } catch (Exception e) {
			logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(null, RQO_cls_Constantes.LOG_DATA_ERROR, 
																	Label.RQO_lbl_mensajeErrorExceptionGenericoIntegracion, 
																	e.getMessage(), null, RQO_cls_Constantes.BATCH_CLIENTES);
			System.debug(Label.RQO_lbl_mensajeErrorExceptionGenericoIntegracion);
        }
		
		if (logObject != null) {
				this.logObjectList.add(logObject);
		}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
    }
	
    /**
	* @creation date: 29/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Creacion de kay para map de relacion
	* @param: idRelacion		external id del objeto padre
	* @param: typeName			Tipo de objeto a tratar
	* @return: 	String			Key de relacion
	* @exception: 
	* @throws: 
	*/
	private String getKeyMapByObjectType(String idRelacion, String typeName) {
		
		/**
        * Constantes
        */
        final String METHOD = 'getKeyMapByObjectType';
		
		/**
		* Variables
		*/
        String keyMapRel = null;
		
		/**
		* Inicio
		*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		if (typeName.equals(Label.RQO_lbl_identificadorIntegracion) || typeName.equals(Label.RQO_lbl_areaVentasIntegracion) || 
			typeName.equals(Label.RQO_lbl_contactIntegracion) || typeName.equals('')) {
			keyMapRel = Label.RQO_lbl_accountIntegracion + RQO_cls_Constantes.HYPHEN + idRelacion;
		} else if (typeName.equals(Label.RQO_lbl_datoContactoIntegracion) || typeName.equals(Label.RQO_lbl_datosConsentimientoIntegracion) ||
					 typeName.equals(Label.RQO_lbl_atributoMarketingIntegracion)) {
			keyMapRel = Label.RQO_lbl_contactIntegracion + RQO_cls_Constantes.HYPHEN + idRelacion;
		} else if (typeName.equals(Label.RQO_lbl_relacionVentasIntegracion)) {
			keyMapRel = Label.RQO_lbl_areaVentasIntegracion + RQO_cls_Constantes.HYPHEN + idRelacion;
		} 
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return keyMapRel;
	}
	
	// ***** METODO PUBLICOS ***** //
	
    /**
	* @creation date: 19/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Instanciacion de objetos de forma dinamica
	* @param: typeName			Tipo de objeto a instanciar
	* @return: 	sObject			Instancia de objeto creado
	* @exception: 
	* @throws: 
	*/
    public sObject createObject(String typeName) {
        
        /**
        * Constantes
        */
        final String METHOD = 'createObject';
		
		/**
		* Variables
		*/
        Schema.SObjectType targetType = null;
        RQO_obj_logProcesos__c logObject = null;
		
		/**
		* Inicio
		*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

        targetType = Schema.getGlobalDescribe().get(typeName);
        
        if (targetType == null) {
            logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(null, RQO_cls_Constantes.LOG_DATA_ERROR, 
																	Label.RQO_lbl_mensajeErrorCreateObjectIntegracion + typeName, 
																	null, null, RQO_cls_Constantes.BATCH_CLIENTES);
            System.debug(Label.RQO_lbl_mensajeErrorCreateObjectIntegracion + typeName);
        }
        
        if (logObject != null) {
				this.logObjectList.add(logObject);
		}
		System.debug(CLASS_NAME + ' - ' + METHOD + ': Instancia de sObject - ' + targetType.newSObject());
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        //  Instancia de sObject con el tipo de objeto pasado como parametro en tiempo de ejecucion
        return targetType.newSObject(); 
    }

}