global without sharing class QUIM_ctrl_sendMailFormContacto {

    @InvocableMethod
    public static void QUIM_ctrl_sendMailFormContacto(List<FormContactRequest> requestOptions){
        FormContactRequest request = requestOptions[0];
        
        sendMail(request.idForm, request.buzon, request.emailTemplate);
   }
    global class FormContactRequest {

        @InvocableVariable(required=true) 
        public ID idForm; 
        
        @InvocableVariable(required=true) 
        public String buzon; 
        
        @InvocableVariable(required=true) 
        public String emailTemplate;
        
	}
    
    public static void sendMail(string idFormulario,string buzonEnvio, string emailTemplate){
        //obtenemos los datos del formulario
        List<QUIM_Form__c> formulario =[select id, Nombre__c, Asunto__c, Correo__c, Descripcion__c, Empresa__c, Pais__c, Perfil__c, 
                                        Producto__c, SendCopia__c, Telefono__c, TemaContacto__c, isFAQ__c, Idioma__c, (SELECT Id FROM Attachments) 
                                        FROM QUIM_Form__c  where id=: idFormulario];

		//obtenemos el email template utilizado
		emailTemplate = emailTemplate+'_'+formulario.get(0).Idioma__c;
        EmailTemplate eTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where Name =:emailTemplate];
        
        //obtenemos los adjuntos del formulario
        List<Attachment> attachments = new List<Attachment>();
        if (formulario.get(0).Attachments != null){
            attachments = 
                [SELECT Id, Name, Body, ContentType FROM Attachment 
                 WHERE Parentid =:formulario.get(0).Id];
        }
        
        //INICIO - creacion y envio de mail
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {buzonEnvio};
        
        mail.setToAddresses(toAddresses);//establece el buzon a donde debe ir
        
        if(!Test.isRunningTest()){        
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress WHERE DisplayName = 'Repsol'];
            if(owea.size() > 0){
            	mail.setOrgWideEmailAddressId(owea.get(0).Id);    
            }
          
        }

        if (formulario.get(0).Asunto__c != null){
        mail.setSubject(eTemplate.Subject +' '+ formulario.get(0).Asunto__c);
        }else{
         mail.setSubject(eTemplate.Subject);   
        }

        String bodyMail = eTemplate.Body;

        if(formulario.get(0).Nombre__c != null){
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Nombre__c', formulario.get(0).Nombre__c);
        }else{
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Nombre__c', '');
        }
        
        if(formulario.get(0).Correo__c != null){
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Correo__c', formulario.get(0).Correo__c);
        }else{
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Correo__c', '');
        }
        
        if(formulario.get(0).Empresa__c != null){
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Empresa__c', formulario.get(0).Empresa__c);
        }else{
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Empresa__c', '');
        }
        
        if(formulario.get(0).Perfil__c != null){
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Perfil__c', formulario.get(0).Perfil__c);
        }else{
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Perfil__c', '');
        }
        
        if(formulario.get(0).Telefono__c != null){
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Telefono__c', formulario.get(0).Telefono__c);
        }else{
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Telefono__c', '');
        }
        
        if(formulario.get(0).TemaContacto__c != null){
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.TemaContacto__c', formulario.get(0).TemaContacto__c);
        }else{
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.TemaContacto__c', '');
        }

         
        if(formulario.get(0).Descripcion__c != null){
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Descripcion__c', formulario.get(0).Descripcion__c);
        }else{
            bodyMail = bodyMail.replaceAll('QUIM_Form__c.Descripcion__c', '');
        }
        
        
		mail.setHtmlBody(bodyMail);

        //en el caso de tener adjuntos los añadimos
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        for (Attachment a : attachments){
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName(a.Name);
            efa.setBody(a.Body);
            fileAttachments.add(efa);
        }
        mail.setFileAttachments(fileAttachments);
        //solo mandamos el mail si no estamos en el test
        if(!Test.isRunningTest()){
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
            
        if (formulario.get(0).SendCopia__c==true){
            String bodyCopia='';
            
            bodyCopia += bodyMail;
            String[] toAddressesCopia = new String[] {formulario.get(0).Correo__c};
        	// Who you are sending the email to
       		mail.setToAddresses(toAddressesCopia);//establece el buzon a donde debe ir
            mail.setHtmlBody(bodyCopia);
            
            if(!Test.isRunningTest()){
            	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
        
        
      }  
    
}