/**
*   @name: RQO_cls_InformationRequestSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Julio Maroto - Unisys
*   @description: Clase que gestiona los selectores de peticiones de información "InformationRequestSelector"
*/
public class RQO_cls_InformationRequestSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_InformationRequestSelector';
	
    /**
    *   @name: RQO_cls_InformationRequestSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Constructor de la clase
    */
	public RQO_cls_InformationRequestSelector() {
		super();
	}
    
    /**
    *   @name: RQO_cls_InformationRequestSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Método que obtiene el listado de campos de una InformationRequest
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_obj_informationRequest__c.Id,
			RQO_obj_informationRequest__c.RecordTypeId,
			RQO_obj_informationRequest__c.Name,
			RQO_obj_informationRequest__c.RQO_fld_personTitle__c,
			RQO_obj_informationRequest__c.RQO_fld_name__c,
			RQO_obj_informationRequest__c.RQO_fld_surname__c,
			RQO_obj_informationRequest__c.RQO_fld_phoneNumber__c,
			RQO_obj_informationRequest__c.RQO_fld_customerProfile__c,
			RQO_obj_informationRequest__c.RQO_fld_email__c,
			RQO_obj_informationRequest__c.RQO_fld_companyName__c,
			RQO_obj_informationRequest__c.RQO_fld_countryCode__c,
			RQO_obj_informationRequest__c.RQO_fld_topic__c,
			RQO_obj_informationRequest__c.RQO_fld_product__c,
			RQO_obj_informationRequest__c.RQO_fld_subject__c,
			RQO_obj_informationRequest__c.RQO_fld_message__c};
    }
    
    /**
    *   @name: RQO_cls_InformationRequestSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Constructor de la clase
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_informationRequest__c.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_InformationRequestSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Método que obtiene el atributo a través del cual se ordena
    */
    
    public override String getOrderBy()
	{
		return 'Name';
	}
    
    /**
    *   @name: RQO_cls_InformationRequestSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Método que selecciona las peticiones
    *   de información (InformationRequests) mediante un dataset de Ids
    */

	public List<RQO_obj_informationRequest__c> selectById(Set<ID> idSet)
    {
         return (List<RQO_obj_informationRequest__c>) selectSObjectsById(idSet);
    }	
}