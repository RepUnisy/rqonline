/*------------------------------------------------------------------------
Author:         Rubén Simarro Alcaide
Company:        Indra
Description:    Clase controladora de la visualforce page que presenta los informes Custom
History
<Date>          <Author>                      <Description>
25-Oct-2016     Rubén Simarro Alcaide         Version inicial
----------------------------------------------------------------------------*/
global with sharing class ITPM_Controller_Custom_reports {
    
    public ITPM_Project__c project {get; set;}

    public ITPM_Controller_Custom_reports(ApexPages.StandardController controller) {
        project = new ITPM_Project__c();
        construyeSQL_projectDetail();
    }
 
    //
    //VARIABLES DEL REPORT FICHAS DE PROYECTOS 
    // 
    public transient List<ITPM_ProjectInvestmentItem__c> TotalProjectInvestmentItems;
    public transient List<ITPM_UBC_Project__c> TotalProjectUBCs;
    public transient List<ITPM_Status_Report__c> TotalStatusReport; 
           
    public List<ITPM_ProjectInvestmentItem__c> getTotalProjectInvestmentItems(){  
        return TotalProjectInvestmentItems;
    }   
        
    public List<ITPM_UBC_Project__c> getTotalProjectUBCs(){  
        return TotalProjectUBCs;
    } 
    
    public List<ITPM_Status_Report__c> getTotalStatusReport(){  
        return TotalStatusReport;
    } 
    
    public List<ITPM_Project__c> lstProjectsDetails;
     
    private String SOQL_projectDetail;
    
    public String projectDireccionElegida {get; set;} 
    public String projectAreaElegida {get; set;} 
    public String projectSubareaElegida {get; set;} 
    
    public String projectDireccionElegidaJS {get; set;} 
    public String projectAreaElegidaJS {get; set;} 
    public String projectSubareaElegidaJS {get; set;}
   
    public List<ITPM_ProjectInvestmentItem__c> lstProjectInvestmentItem;
    public List<ITPM_Investment_Item__c> lstInvestmentItem;
       
    //indica si el orden será ascendente o descendente y por qué campo
    private String orden;
    private String sortField;
    private String previousSortField;
        
    private String SOQL_ProjectInvestmentItem;
    private String SOQL_investmentItem;
      
    public ITPM_Controller_Custom_reports(){
        construyeSQL_projectDetail();
    }
    
    @TestVisible
    private void construyeSQL_projectDetail(){
       setCon = null;
       
       TotalProjectInvestmentItems = [SELECT ITPM_REL_Investment_Item__r.Name, ITPM_REL_Project__r.Id FROM ITPM_ProjectInvestmentItem__c WHERE ITPM_REL_Project__c != null]; 
       TotalProjectUBCs = [SELECT Name, ITPM_REL_Project__c FROM ITPM_UBC_Project__c WHERE ITPM_REL_Project__c != null];
       TotalStatusReport = [SELECT Name, ITPM_TX_Description__c, ITPM_DT_Report_date__c, ITPM_REL_Project__r.Id FROM ITPM_Status_Report__c WHERE ITPM_REL_Project__c != null];
      
        SOQL_projectDetail = 'select '+
            ' Id,'+
            ' Name,'+
            ' ITPM_TX_Project_Name__c,'+     
            ' ITPM_Project_Phase__c,'+
            ' ITPM_SEL_project_manager__r.name,'+           
            //' ITPM_Project_TX_Description__c,'+
            //' ITPM_Project_TXT_Justif_scope_descrip__c, '+
            //' ITPM_Project_TXT_Objectives__c, '+
            ' ITPM_Project_Status_Date__c, '+             
            ' ITPM_DT_Planned_Start_Date__c, '+              
            ' ITPM_DT_Planned_Finish_Date__c, '+      
            //' ITPM_Project_NUM_Time_Deviation__c, '+  
            //' ITPM_Project_NUM_Cost_Deviation__c, '+  
            //' ITPM_Project_SEL_Final_cost__c, '+ 
            // ' ITPM_Project_SEL_Executing_area__c, '+  
            //' ITPM_Project_SEL_Executing_subarea__c, '+  
            //' ITPM_Project_SEL_Maintenance_Area__c, '+  
            //' ITPM_Project_SEL_Maintenance_subarea__c, '+  
            //' ITPM_Project_BLN_New_service__c, '+ 
            ' ITPM_Project_REL_Conceptualization__c, '+ 
            ' ITPM_REL_Program__c '+
            //' ITPM_Project_TX_Last_Status_Report__c, '+            
            //' ITPM_PROJ_DT_Actual_Finish_Date__c, '+ 
            //' ITPM_PROJ_DT_Actual_Go_Live_Date__c, '+
            //' ITPM_PROJ_DT_Actual_Start_Date__c '+            
            ' FROM ITPM_project__c ';
        
           try{
         
               boolean hayyawhere = false;                                                        
              
              //controlador direccion
               projectDireccionElegida = project.ITPM_Project_SEL_Executing_direction__c;
               System.debug('projectDireccionElegida: ' + projectDireccionElegida);
               if(projectDireccionElegida !=null && projectDireccionElegida.contains('&')){
                   projectDireccionElegidaJS = projectDireccionElegida.replace('&','%26');
                 }
               else{        
                   projectDireccionElegidaJS = projectDireccionElegida;
                  }

                //controlador area
               projectAreaElegida = project.ITPM_Project_SEL_Executing_area__c;
               System.debug('projectAreaElegida: ' + projectAreaElegida);
               if(projectAreaElegida!=null&&projectAreaElegida.contains('&')){
                   projectAreaElegidaJS = projectAreaElegida.replace('&','%26');
                 }
               else{        
                   projectAreaElegidaJS = projectAreaElegida;
                  }
               
               //controlador subarea
               projectSubAreaElegida = project.ITPM_Project_SEL_Executing_subarea__c;
                System.debug('projectSubAreaElegida: ' + projectSubAreaElegida);
               if(projectSubareaElegida!=null && projectSubareaElegida.contains('&'))
                   projectSubareaElegidaJS = projectSubareaElegida.replace('&','%26');
               else        
                   projectSubareaElegidaJS = projectSubareaElegida;
           
               // creacion de la condicion 
               
               if ( projectDireccionElegida != null) {                   
                   
                   
                              
                       SOQL_projectDetail = SOQL_projectDetail + ' where ITPM_Project__c.ITPM_Project_SEL_Executing_direction__c ='+'\''+  projectDireccionElegida+'\'';
                       System.debug('SOQL_projectDetail11 direcc mal:  ' + SOQL_projectDetail);                                                           
                       hayyawhere=true;                                                       
                                                                            
               }        


               if ( projectAreaElegida != null) {                   
                   
                   if(hayyawhere){                                                            
                       SOQL_projectDetail = SOQL_projectDetail + 'AND ITPM_Project__c.ITPM_Project_SEL_Executing_area__c ='+'\''+  projectAreaElegida+'\'';  
                       System.debug('SOQL_projectDetail1 area bn: ' + SOQL_projectDetail);                                                     
                   }                                                      
                                                           
               }        
               if ( projectSubAreaElegida != null) {                                                          
                   if(hayyawhere){                                                            
                       SOQL_projectDetail = SOQL_projectDetail + 'AND ITPM_Project__c.ITPM_Project_SEL_Executing_subarea__c ='+'\''+  projectSubAreaElegida+'\'';
                       System.debug('SOQL_projectDetail2 subarea bn:  ' + SOQL_projectDetail);                                                      
                   }                                                      
                                                         
               }
                //SOQL_projectDetail = SOQL_projectDetail + ' LIMIT 10 ';
                         
        }catch(Exception e){       
            system.debug('@@@ ERROR ITPM_Controller_Custom_reports construyeSQL_projectDetail(): '+e.getMessage());   
        }             
    }
    
    public void getProjects(){
        construyeSQL_projectDetail();
        //HacerBusqueda();
    }
  

    public List< ITPM_Project__c > getITPMRecords() {
         return (List< ITPM_Project__c >) setCon.getRecords();
    }
    
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(SOQL_projectDetail));
            }
            return setCon;
        }
        set;
    }
       
    public List<ITPM_Project__c> getlstProjectsDetails(){               
        try{    
           lstProjectsDetails = Database.Query(SOQL_projectDetail);                               
           return lstProjectsDetails;
        }
        catch(Exception e){        
            system.debug('@@@ ERROR ITPM_Controller_Custom_reports getlstProjectsDetails(): '+e.getMessage());                  
            return new List<ITPM_Project__c>();
        }
    }  
    
    //EXPORTAR EXCEL UNICO         
    public pagereference exportAllProjectToExcel(){                                             
        Pagereference p = new Pagereference('/apex/ITPM_VF_FichasProject_Excel');   
        p.setredirect(false);    
        return p;
    }    
    
    
          
}