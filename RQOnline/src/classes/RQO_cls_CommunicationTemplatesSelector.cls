/**
*   @name: RQO_cls_CommunicationTemplatesSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de que gestiona la selección u obtención de templates de comunicaciones
*/
public class RQO_cls_CommunicationTemplatesSelector extends RQO_cls_ApplicationSelector {
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_CommunicationsSelector';
	
    /**
    *   @name: RQO_cls_CommunicationTemplatesSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la lista de campos del objeto RQO_obj_communicationTemplate__c
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_obj_communicationTemplate__c.Name,
			RQO_obj_communicationTemplate__c.RQO_fld_title__c,
			RQO_obj_communicationTemplate__c.RQO_fld_opening__c,
			RQO_obj_communicationTemplate__c.RQO_fld_body__c,
			RQO_obj_communicationTemplate__c.RQO_fld_content__c,
			RQO_obj_communicationTemplate__c.RQO_fld_language__c,
			RQO_obj_communicationTemplate__c.RQO_fld_active__c };
    }

    /**
    *   @name: RQO_cls_CommunicationTemplatesSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el sObjectType de RQO_obj_communicationTemplate__c
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_communicationTemplate__c.sObjectType;
    }    
}