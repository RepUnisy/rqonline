/**
 *  @name: RQO_cls_WSCalloutResultBean
 *  @version: 1.0
 *  @creation date: 11/09/2017
 *  @author: David Iglesias - Unisys
 *  @description: Clase para enviar la respuesta de la llamada a un servicio web
 */
global class RQO_cls_WSCalloutResponseBean {

    // ***** PROPIEDADES ***** //
    public String estado { get; set; }
    public String msgError { get; set; }
    public String contenido { get; set; }
    
}