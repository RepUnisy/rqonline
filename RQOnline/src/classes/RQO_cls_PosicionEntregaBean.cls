/**
 *	@name: RQO_cls_PosicionEntregaBean
 *	@version: 1.0
 *	@creation date: 16/10/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de Posiciones de Entrega.
 */
public class RQO_cls_PosicionEntregaBean {

    public String externalId {get; set;}
    public String entrega {get; set;}
    public String posicionEntrega {get; set;}
    public String indSujetoLote {get; set;}
    public String cantidadEntrega {get; set;}
    public String unidad {get; set;}
    public String idArchivado {get; set;}
    public String fechaRegistro {get; set;}
    public String isDelete {get; set;}
    public String posicionPedido {get; set;}
    public List <RQO_cls_PosicionLoteBean> posicionesLoteList {get; set;}
    
}