/**
*   @name: RQO_cls_UserSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que selecciona usuarios
*/
public with sharing class RQO_cls_UserSelector extends RQO_cls_ApplicationSelector {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_UserSelector';
    
    /**
    *   @name: RQO_cls_UserSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase que retorna la lista de campos del objeto User
    */
    public List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            User.Id,
            User.Name,
            User.Username,
            User.FirstName,
            User.LastName,
            User.Email,
            User.ContactId,
            User.AccountId,
            User.ProfileId,
            User.IsActive,
            User.LanguageLocaleKey};
    }

    /**
    *   @name: RQO_cls_UserSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el sObjectType del objeto User
    */
    public Schema.SObjectType getSObjectType()
    {
        return User.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_UserSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar los usuarios
    */
    public override String getOrderBy()
    {
        return 'Name';
    }

    /**
    *   @name: RQO_cls_UserSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar los usuarios
    */
    public List<User> selectById(Set<ID> idSet)
    {
         return (List<User>) selectSObjectsById(idSet);
    }
    
    /**
    *   @name: RQO_cls_UserSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene una lista de usuarios por un conjunto de ids de contacto
    */
    public List<User> selectByContactId(Set<ID> idSet)
    {
         /**
        * Constantes
        */
        final String METHOD = 'selectByContactId';
        
        fflib_QueryFactory commFactory = newQueryFactory();   
        new RQO_cls_ContactSelector().configureQueryFactoryFields(commFactory,     
                User.ContactId.getDescribe().getRelationshipName());
        
        string query = commFactory.setCondition('ContactId IN :idSet').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        return (List<User>) Database.query(query);
    }
    
    /**
    *   @name: RQO_cls_UserSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de objetos de usuario mediante un nombre de usuario
    */
    public List<User> selectByUsername(string username)
    {
         /**
        * Constantes
        */
        final String METHOD = 'selectByUsername';
        
        fflib_QueryFactory commFactory = newQueryFactory();   
        new RQO_cls_ContactSelector().configureQueryFactoryFields(commFactory,     
                User.ContactId.getDescribe().getRelationshipName());
        
        string query = commFactory.setCondition('Username = :username').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        return (List<User>) Database.query(query);
    }
    
    /**
    *   @name: RQO_cls_UserSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de objetos de usuario  mediante Ids de cuenta
    */
    public List<User> selectByAccountIdWithContact(Set<ID> idSet)
    {
        /**
        * Constantes
        */
        final String METHOD = 'selectByAccountIdWithContact';
        
        fflib_QueryFactory commFactory = newQueryFactory();   
        new RQO_cls_ContactSelector().configureQueryFactoryFields(commFactory,     
                User.ContactId.getDescribe().getRelationshipName());
        
        string query = commFactory.setCondition('AccountId in :idSet AND IsActive = TRUE').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        return (List<User>) Database.query(query);
    }
    
    /**
    *   @name: RQO_cls_UserSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de usuarios mediante un nombre y un conjunto de ids de cuentas
    */
    public List<User> selectByNameWithContact(Set<ID> idSet, string name)
    {
        /**
        * Constantes
        */
        final String METHOD = 'selectByNameWithContact';
        
        fflib_QueryFactory commFactory = newQueryFactory();   
        new RQO_cls_ContactSelector().configureQueryFactoryFields(commFactory,     
                User.ContactId.getDescribe().getRelationshipName());
        
        string query = commFactory.setCondition('AccountId in :idSet AND Name LIKE \'%' + name + '%\'').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        return (List<User>) Database.query(query);
    }
    
}