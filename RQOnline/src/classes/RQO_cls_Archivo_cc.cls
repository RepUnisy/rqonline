/**
*   @name: RQO_cls_Archivo_cc
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de gestión de archivos
*/
public class RQO_cls_Archivo_cc {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_Archivo_cc';

    /**
    *   @name: RQO_cls_Archivo_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que envía una solicitud de archivo
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static void sendRequest(String title, String name, String surname, String profile,
                                   String companyName, String phoneArea, String phoneNumber,
                                   String countryCode, String email, String topic, String product,
                                   String subject, String message, Boolean copyEmail,
                                   List<String> fileNames, List<String> base64Datas) { 
        
        // ***** CONSTANTES ***** //
        final String METHOD = 'sendRequest';
        
        try {
            Id requestId = RQO_cls_informationRequestService.registerContact(name, surname, title,
                                                                             phoneArea,  phoneNumber, companyName,
                                                                             profile, email, countryCode,
                                                                             topic, product, subject, message);
            List<Blob> listaArchivos = new list<Blob>();
            for(String aux : base64Datas){
                listaArchivos.add(EncodingUtil.base64Decode(EncodingUtil.urlDecode(aux, 'UTF-8')));
            }
            RQO_cls_informationRequestService.sendEmail(copyEmail, requestId, listaArchivos, fileNames);
        }
        catch (Exception e) {
            AuraHandledException auraEx = new AuraHandledException(e.getMessage());
            auraEx.setMessage(e.getMessage());
            throw auraEx;
        }
    }
    
    /**
    *   @name: RQO_cls_Archivo_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de títulos de una persona
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<String> getPersonTitles()
    {
        List<String> values = new List<String>();
        List<SelectOption> options = new RQO_cls_GeneralUtilities().getPersonTitles();
        for (SelectOption option: options)
            values.add(option.getValue());
        return (values);
    }
    
    /**
    *   @name: RQO_cls_Archivo_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtene una lista de productos
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<String> getProducts()
    {
        List<String> values = new List<String>();
        List<SelectOption> options = new RQO_cls_GeneralUtilities().getProductList();
        for (SelectOption option: options)
            values.add(option.getValue());
        return (values);
    }
    
    /**
    *   @name: RQO_cls_Archivo_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método devuelve InformationTopics
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<String> getInformationTopics()
    {
        List<String> values = new List<String>();
        List<SelectOption> options = new RQO_cls_GeneralUtilities().getInformationTopicList();
        for (SelectOption option: options)
            values.add(option.getValue());
        return (values);
    }
    
    /**
    *   @name: RQO_cls_Archivo_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método devuelve una lista de CustomerProfiles
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @AuraEnabled
    public static List<String> getCustomerProfiles()
    {
        List<String> values = new List<String>();
        List<SelectOption> options = new RQO_cls_GeneralUtilities().getCustomerProfileList();
        for (SelectOption option: options)
            values.add(option.getValue());
        return (values);
    }
}