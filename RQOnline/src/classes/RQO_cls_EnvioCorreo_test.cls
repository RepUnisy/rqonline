/**
*   @name: RQO_cls_EnvioCorreo_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_EnvioCorreo_cc.
*/
@IsTest
public class RQO_cls_EnvioCorreo_test {
	/**
    *   @name: RQO_cls_EnvioCorreo_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    */
	@testSetup static void initialSetup() {
		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(u){
			// Crear email templates RQO_eplt_correoFaqs_en
			RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_correoFaqs_en'}); 
		}
	}
    
	/**
    *   @name: RQO_cls_EnvioCorreo_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la generación de una excepción cuando se envía un correo incorrecto en la petición de información.
    */
	@isTest
    public static void WhenSendRequestThrowsException() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
		System.runAs(usuario){
            Boolean DidThrowException = false;
            
            try {
                
                RQO_cls_EnvioCorreo_cc.sendRequest('WrongEmail', 'Message', false);
            }
            catch (AuraHandledException e) {
                System.debug(e.getMessage());
                DidThrowException = true;
            }
            
            System.assertEquals(DidThrowException,
                                true, 
                                'No se ha generado el error al enviar un email incorrecto.');
		}
 	}
 	
   	/**
    *   @name: RQO_cls_EnvioCorreo_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description:  Método de test que comprueba la creación de una petición de información en faqs.
    */
	@isTest
    public static void WhenSendRequestDoesNotThrowException() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
		System.runAs(usuario){
		//User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsRequests@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		List<RQO_obj_informationRequest__c> requests;
		//System.runAs(u){
			test.startTest();
			RQO_cls_EnvioCorreo_cc.sendRequest('test@test.test', 'Message', false);
			requests = [SELECT Id FROM RQO_obj_informationRequest__c WHERE RQO_fld_email__c = 'test@test.test'];
	    	test.stopTest();
		//}
    
		System.assertNotEquals(0,
						requests.size(), 
						'No se ha creado ningún registro.');
        }    
 	}

}