/**
*	@name: RQO_cls_DetalleProducto4_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar el controlador apex RQO_cls_DetalleProducto4_cc
*/
@IsTest
public class RQO_cls_DetalleProducto4_test {
	
    /**
	* @creation date: 28/02/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserDocumentum@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSDocumentumRepository();
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_DOCUM_GET, RQO_cls_Constantes.WS_DOCUM_SAP_GET, RQO_cls_Constantes.WS_DOCUM_CREATE});
            REP_cs_activacionTrigger__c actTri = new REP_cs_activacionTrigger__c();
            actTri.REP_fld_triggerActive__c = false;
            actTri.REP_fld_triggerName__c = 'RQO_trg_GradeTrigger';
            actTri.Name = 'RQO_trg_GradeTrigger';
            insert actTri;
            RQO_cls_TestDataFactory.createDocument(200);
		}
    }
    
    /**
	* @creation date: 28/02/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar el controlador Apex RQO_cls_DetalleProducto4_cc. Este controlador se encarga de recuperar los datos de la tabla de documentos 
	* almacenados en Salesforce que luegos se obtienen en Documetum
	* @exception: 
	* @throws: 
	*/
    static testMethod void detalleProducto4Controller(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            RQO_obj_grade__c gradoinicial = [Select Id, Name, RQO_fld_sku__c, RQO_fld_blocked__c, RQO_fld_codeDocumentum__c, RQO_fld_new__c, RQO_fld_private__c, RQO_fld_product__c, 
                RQO_fld_public__c, RQO_fld_recommendation__c from RQO_obj_grade__c limit 1];
            
            List<RQO_obj_document__c> listObjDoc = [Select RQO_fld_documentId__c, RQO_fld_grade__c, RQO_fld_locale__c, RQO_fld_type__c 
                from RQO_obj_document__c where RQO_fld_grade__c = : gradoinicial.Id];
            
            System.debug('ListDocs: ' + listObjDoc);
            test.startTest();
            Map<String, List<RQO_cls_LCDocDataAux>> testMap = RQO_cls_DetalleProducto4_cc.doInitDataProd4Apex(gradoinicial.Id, 'en_US');
            System.assertNotEquals(testMap,null);
            Map<String, String> mapDocTypeTranslate = RQO_cls_DetalleProducto4_cc.getDocTypeTittleTranslate();
            System.assertNotEquals(mapDocTypeTranslate,null);
            test.stopTest();
        }
    }
}