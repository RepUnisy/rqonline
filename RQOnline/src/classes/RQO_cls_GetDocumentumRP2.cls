/**
 *	@name: RQO_cls_GetDocumentumRP2
 *	@version: 1.0
 *	@creation date: 10/11/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase generada por wsdl2apex con los métodos, objetos, invocaciones etc necesarias para recuperar datos de documentos de SAP RP2
 *
*/
public class RQO_cls_GetDocumentumRP2 {
    public class ZeidentProd {
        public String Namcom;
        public String Onu;
        public String Cas;
        public String Zversion;
        public String Zfecha;
        public String NEinecs;
        public String NAuto;
        private String[] Namcom_type_info = new String[]{'Namcom','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Onu_type_info = new String[]{'Onu','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Cas_type_info = new String[]{'Cas','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Zversion_type_info = new String[]{'Zversion','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Zfecha_type_info = new String[]{'Zfecha','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] NEinecs_type_info = new String[]{'NEinecs','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] NAuto_type_info = new String[]{'NAuto','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Namcom','Onu','Cas','Zversion','Zfecha','NEinecs','NAuto'};
    }
    public class ZsapGetprodRlesaRq_element {
        public String Area;
        public String Empresa;
        public String FechaDesde;
        public String FechaHasta;
        public String NProducto;
        public String Namsrd;
        public String Seccion;
        private String[] Area_type_info = new String[]{'Area','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] Empresa_type_info = new String[]{'Empresa','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] FechaDesde_type_info = new String[]{'FechaDesde','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] FechaHasta_type_info = new String[]{'FechaHasta','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] NProducto_type_info = new String[]{'NProducto','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] Namsrd_type_info = new String[]{'Namsrd','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] Seccion_type_info = new String[]{'Seccion','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Area','Empresa','FechaDesde','FechaHasta','NProducto','Namsrd','Seccion'};
    }
    public class ZsapGetprodRlesaRqResponse_element {
        public RQO_cls_GetDocumentumRP2.Zelog Log;
        public RQO_cls_GetDocumentumRP2.ZtyteehsProdrlsa Productos;
        private String[] Log_type_info = new String[]{'Log','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Productos_type_info = new String[]{'Productos','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Log','Productos'};
    }
    public class Zelog {
        public String Id;
        public String Num;
        public String Type_x;
        public String Msg;
        private String[] Id_type_info = new String[]{'Id','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Num_type_info = new String[]{'Num','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Type_x_type_info = new String[]{'Type','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Msg_type_info = new String[]{'Msg','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Id','Num','Type_x','Msg'};
    }
    public class ZeehsProdrlsa {
        public String Empresa;
        public String Area;
        public String Seccion;
        public String Subid;
        public String Tficha;
        public String Idioma;
        public String Docid;
        public RQO_cls_GetDocumentumRP2.ZeidentProd Identificadores;
        private String[] Empresa_type_info = new String[]{'Empresa','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Area_type_info = new String[]{'Area','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Seccion_type_info = new String[]{'Seccion','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Subid_type_info = new String[]{'Subid','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Tficha_type_info = new String[]{'Tficha','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Idioma_type_info = new String[]{'Idioma','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Docid_type_info = new String[]{'Docid','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Identificadores_type_info = new String[]{'Identificadores','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Empresa','Area','Seccion','Subid','Tficha','Idioma','Docid','Identificadores'};
    }
    public class ZtyteehsProdrlsa {
        public RQO_cls_GetDocumentumRP2.ZeehsProdrlsa[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class RQ_ONLINE_Rx2_fichasSeguridad_FE_psHttpsSoap11Endpoint {
        //public String endpoint_x = 'https://dsoa.repsol.com:8243/services/RQ_ONLINE_Rx2_fichasSeguridad_FE_ps.RQ_ONLINE_Rx2_fichasSeguridad_FE_psHttpsSoap11Endpoint';
		RQO_obj_parametrizacionIntegraciones__c serviceParam  = RQO_obj_parametrizacionIntegraciones__c.getInstance(RQO_cls_Constantes.WS_DOCUM_RP2_GET);
       	//public String endpoint_x = 'https://requestb.in/19zymvp1';
        public String endpoint_x = RQO_cls_Constantes.CALLOUT_BUS_NAMED_CREDENTIAL +  serviceParam.RQO_fld_servicePath__c;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style', 'RQO_cls_GetDocumentumRP2', 'urn:sap-com:document:sap:rfc:functions', 'RQO_cls_GetDocumentumRP2Functions'};
        
            public RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element ZsapGetprodRlesaRq(String Area,String Empresa,String FechaDesde,String FechaHasta,String NProducto,String Namsrd,String Seccion, RQO_obj_logProcesos__c objLog) {
            RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRq_element request_x = new RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRq_element();
            request_x.Area = Area;
            request_x.Empresa = Empresa;
            request_x.FechaDesde = FechaDesde;
            request_x.FechaHasta = FechaHasta;
            request_x.NProducto = NProducto;
            request_x.Namsrd = Namsrd;
            request_x.Seccion = Seccion;
            if (objLog != null){
                objLog.RQO_fld_host__c = endpoint_x;
                objLog.RQO_fld_body__c = (String.valueOf(request_x).length() > 131070) ? String.valueOf(request_x).substring(0, 131070) : String.valueOf(request_x);
            }
            RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element response_x;
            Map<String, RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element> response_map_x = new Map<String, RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZsapGetprodRlesaRq',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZsapGetprodRlesaRqResponse',
              'RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            System.debug('response_x: ' + response_x);
            if (objLog != null){objLog.RQO_fld_response__c = (String.valueOf(response_x).length() > 131070) ? String.valueOf(response_x).substring(0, 131070) : String.valueOf(response_x);}            
            return response_x;
        }
    }
}