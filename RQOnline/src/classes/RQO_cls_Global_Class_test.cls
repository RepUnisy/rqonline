/**
*   @name: RQO_cls_UploadDocument_test
*   @version: 1.0
*   @creation date: 27/2/2018
*   @author: Juan Elías - Unisys
*   @description: Clase de test para probar los métodos asociados a Global_Class
*/
@IsTest
public class RQO_cls_Global_Class_test {
    
    /**
    *   @name: RQO_cls_UploadDocument_test
    *   @version: 1.0
    *   @creation date: 27/2/2018
    *   @author: Juan Elías - Unisys
    *   @description: Método para crear el conjunto inicial de datos necesario para la ejecución del test
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createGrade(20);
            RQO_cls_TestDataFactory.createTranslation(200);
        }
        
    }
    
    /**
* @creation date: 27/02/2018
* @author: Juan Elías - Unisys
* @description: Método test para probar el método getGradeTranslate
* @exception: 
* @throws: 
*/
    @isTest
    static void testGetGradeTranslat(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String language = 'ES_es';
            Map<Id, RQO_obj_translation__c> esperado;
            List<RQO_obj_translation__c> translationList = [SELECT id, RQO_fld_translation__c, RQO_fld_detailTranslation__c, RQO_fld_grade__c FROM RQO_obj_translation__c LIMIT 2];
            List<RQO_obj_grade__c> gradeList = [SELECT id FROM RQO_obj_grade__c LIMIT 2];
            
            List<Id> listGrade = new List<Id>();
            listGrade.add(gradeList[0].Id);
            listGrade.add(gradeList[1].Id);
            
            translationList[0].RQO_fld_grade__c = gradeList[0].Id;
            translationList[0].RQO_fld_locale__c = 'ES_es';
            update translationList[0];
            translationList[1].RQO_fld_grade__c = gradeList[1].Id;
            translationList[1].RQO_fld_locale__c = 'ES_es';
            update translationList[1];
            
            test.startTest();
            Map<Id, RQO_obj_translation__c> resultado = RQO_cls_Global_Class.getGradeTranslate(language, listGrade);
            System.assertNotEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
    /**
* @creation date: 27/02/2018
* @author: Juan Elías - Unisys
* @description: Método test para probar el método getGradeTranslate
* @exception: 
* @throws: 
*/
    @isTest
    static void testGetGradeTranslateB(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String language = 'ES_es';
            Map<Id, RQO_obj_translation__c> esperado;
            List<RQO_obj_grade__c> listGrade = new List<RQO_obj_grade__c>();
            test.startTest();
            Map<Id, RQO_obj_translation__c> resultado = RQO_cls_Global_Class.getGradeTranslate(language, listGrade);
            System.assertNotEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
}