public class QUIM_ctrl_images_busqueda {
    public static List<ccrz__E_CategoryI18N__c> productsI18N {get; set;}
    // Mapa traducciones Productos intermedios(ccrz__CategoryID__c[key] y I18N.Name[value])
    public static String productosIntermJSON {get; set;}
    // Parámetro GET idioma
    public static String parametroIdioma {get;set;}
    // Lengua a utilizar
    public static String language {get; set;}
    
    public QUIM_ctrl_images_busqueda() {
        parametroIdioma = ApexPages.currentPage().getParameters().get('cclcl');
        //System.debug(System.LoggingLevel.INFO, 'IMAGES parametroIdioma: ' + parametroIdioma);
        if (parametroIdioma != NULL && parametroIdioma.length()>1) {
            language = parametroIdioma;
        }
        else {
            language = ApexPages.currentPage().getHeaders().get('Accept-Language').left(2);
        }
        //System.debug(System.LoggingLevel.INFO, 'IMAGES language: ' + language);
        
        try {
            
            String langLike = language + '%';
            //System.debug('langLike: ' + langLike);
            ccrz__E_Category__c idProductosParent = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'AgrupacionProductos' LIMIT 1];
            List<ccrz__E_Category__c> productsMiddle = new List<ccrz__E_Category__c>();
            productsMiddle = [SELECT Id FROM ccrz__E_Category__c WHERE ccrz__ParentCategory__c = :idProductosParent.Id];
            List<ccrz__E_Category__c> products = new List<ccrz__E_Category__c>();
            products = [SELECT Id FROM ccrz__E_Category__c WHERE ccrz__ParentCategory__c IN :productsMiddle];
            //System.debug(System.LoggingLevel.INFO, 'products: ' + products);
            productsI18N = [SELECT Id, Name, ccrz__LongDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :products AND ccrz__Locale__c LIKE :langLike]; 
            if (productsI18N.isEmpty()) {
                productsI18N = [SELECT Id, Name, ccrz__LongDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :products AND ccrz__Locale__c LIKE 'en%'];
            }
            
            //System.debug(System.LoggingLevel.INFO, 'productsI18N: ' + productsI18N);
            //System.debug(System.LoggingLevel.INFO, 'productsI18N.size(): ' + productsI18N.size());
            // Obteniendo nombres TRADUCIDOS de los productos intermedios.
            List<ccrz__E_CategoryI18N__c> productsMiddleI18N = new List<ccrz__E_CategoryI18N__c>();
            productsMiddleI18N = [SELECT Id, ccrz__Category__c, Name, ccrz__Category__r.ccrz__CategoryID__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :productsMiddle AND ccrz__Locale__c LIKE :langLike ORDER BY Name];
            //System.debug(System.LoggingLevel.INFO, 'productsMiddleI18N: ' + productsMiddleI18N);
            if (productsMiddleI18N.isEmpty()) {
                productsMiddleI18N = [SELECT Id, ccrz__Category__c, Name, ccrz__Category__r.ccrz__CategoryID__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :productsMiddle AND ccrz__Locale__c LIKE 'en%' ORDER BY Name];
            }
            List<Map<String, String>> productosInterm = new List<Map<String, String>> ();
            Map <String, String> mapaMiddleI18N = new Map<String, String> ();
            
            for (ccrz__E_CategoryI18N__c productMidI18N : productsMiddleI18N) {
                mapaMiddleI18N.put(productMidI18N.ccrz__Category__r.ccrz__CategoryID__c, productMidI18N.Name);
            }
            //System.debug('mapaMiddleI18N: ' + mapaMiddleI18N);
            
            productosInterm.add(mapaMiddleI18N);
            //System.debug(System.LoggingLevel.INFO, 'productosInterm: ' + productosInterm);
            // paso lista a JSON [para paso por JS]
            productosIntermJSON = JSON.serialize(productosInterm);
            //System.debug(System.LoggingLevel.INFO, 'productosIntermJSON: ' + productosIntermJSON);
			
        } catch (Exception e) {
            System.debug(System.LoggingLevel.WARN, 'EXCEPTION QUIM_ctrl_images_busqueda(): ' + e);
        }
    }
}//class