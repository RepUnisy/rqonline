/**
*   @name: RQO_cls_WSInConfirmaPedido_test
*   @version: 1.0
*   @creation date: 14/2/2018
*   @author: Juan Elías - Unisys
*   @description: Clase de test para probar los métodos asociados a WSInConfirmaPedido
*/
@IsTest
public class RQO_cls_WSInConfirmaPedido_test {
	
    /**
    *   @name: RQO_cls_WSInConfirmaPedido_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Juan Elías - Unisys
    *   @description: Método que crea el conjunto de datos inicial necesario para realizar el test
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
        }
    }
    
    /**
    *   @name: RQO_cls_WSInConfirmaPedido_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Juan Elías - Unisys
    *   @description: Método que chequea la recepción de la confirmación de un pedido creado
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @isTest
    static void testRecibirConfirmacionPedidoCreado(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            RestRequest request  = new RestRequest();
         //   request.addHeader(No hace falta que pases nada);
            RestContext.request = request;
            RQO_cls_WSConfirmaPedidoCreadoBean bean = new RQO_cls_WSConfirmaPedidoCreadoBean ();
            request.requestBody = Blob.valueOf(JSON.serialize(bean));
            
            RQO_cls_WSCalloutResponseBean esperado = new RQO_cls_WSCalloutResponseBean();
            esperado.estado = RQO_cls_Constantes.PETICION_ESTADO_OK;
            test.startTest();
            RQO_cls_WSCalloutResponseBean resultado = RQO_cls_WSInConfirmaPedido.recibirConfirmacionPedidoCreado();
            System.assertEquals(esperado.estado, resultado.estado);
            test.stopTest();
        }
    }
}