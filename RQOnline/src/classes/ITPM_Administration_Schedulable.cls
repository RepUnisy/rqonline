/*---------------------------------------------------------------------------
Author:          Borja Martín
Company:         Indra
Description:     Clase para la programación del cálculo de totales de E/I en Projects.
Test Class:      ITPM_Administration_Schedulable_Test
History
<Date>        <Author>       <Change Description>
19-10-2016      Borja Martín  Versión inicial.
----------------------------------------------------------------------------*/
global class ITPM_Administration_Schedulable implements Schedulable {    
    global void execute(SchedulableContext sc) {
        ITPM_Administration controller = new ITPM_Administration();
        controller.calculateTotalProjects();
    }
}