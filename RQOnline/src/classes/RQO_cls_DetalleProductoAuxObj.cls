/**
 *	@name: RQO_cls_DetalleProductoAuxObj
 *	@version: 1.0
 *	@creation date: 07/09/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Bean Apex auxiliar accesible desde los componentes lightning que almacena las caracteristicas de un grado que se van a mostrar por pantalla
 *
*/
public with sharing class RQO_cls_DetalleProductoAuxObj {
    // ***** PROPIEDADES ***** //
    @AuraEnabled
    public String description{get;set;}
    
    @AuraEnabled
	public String value{get;set;}
    
    @AuraEnabled
	public String unitMeasure{get;set;}
    // **** //
    
    // ***** CONSTRUCTORES ***** //
	
    /**
	* @creation date: 07/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Constructor por defecto
	* @return: 
	* @exception: 
	* @throws: 
	*/
    public RQO_cls_DetalleProductoAuxObj(){}
    
    /**
	* @creation date: 07/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Constructor con parámetros
	* @param: Param1 - description tipo String. 
	* @param: value tipo String. 
	* @param: unitMeasure tipo String
	* @return: 
	* @exception: 
	* @throws: 
	*/
    public RQO_cls_DetalleProductoAuxObj(String description, String value, String unitMeasure){
        this.description = description;
        this.value = value;
        this.unitMeasure = unitMeasure;
    }
    // **** //
}