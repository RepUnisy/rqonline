/**
 *	@name: RQO_cls_WSCalloutResult
 *	@version: 1.0
 *	@creation date: 11/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de gestion para insercion de logs de servicio de callout WS
 */
public class RQO_cls_WSCalloutResult {

    public Datetime dtRequest { get; set; }
	public Datetime dtResponse { get; set; }
	public String status { get; set; }
	public Map<String, String> header { get; set; }
	public String requestMessage { get; set; }
	public String responseMessage { get; set; }
	public RQO_cls_WSCalloutResultBean result { get; set; }
	public String host { get; set; }

}