/**
*	@name: RQO_cls_GetOrderDocsWS_test
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase RQO_cls_GetOrderDocsWS
*/
@IsTest
public class RQO_cls_GetOrderDocsWS_test {
    
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserDocs@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de documento de factura
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getInvoice(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocs@testorg.com');
        System.runAs(u){
            
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_INVOICE_DOC, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
                RQO_cls_GetOrderDocsWS docs = new RQO_cls_GetOrderDocsWS();
            	RQO_cls_GetOrderDocsWS.OrderDocCustomResponse factura = docs.getInvoice('test', 'test', 'test');
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_OK, factura.errorCode);
        }
    }
	
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la ejecución incorrecta de la funcionalidad de obtención de documento de factura
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getInvoiceNOK(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocs@testorg.com');
        System.runAs(u){
            
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_INVOICE_DOC, RQO_cls_Constantes.MOCK_EXEC_NOK, RQO_cls_Constantes.REQ_WS_RESPONSE_NO_DATA_ERROR));
                RQO_cls_GetOrderDocsWS docs = new RQO_cls_GetOrderDocsWS();
            	RQO_cls_GetOrderDocsWS.OrderDocCustomResponse factura = docs.getInvoice('test', 'test', 'test');
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, factura.errorCode);
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la ejecución incorrecta (excepción o respuesta vacía del mock) de la funcionalidad de obtención de documento de factura
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getInvoiceNOKException(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocs@testorg.com');
        System.runAs(u){
            
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_INVOICE_DOC, RQO_cls_Constantes.MOCK_EXEC_EXCEPTION, '0'));
                RQO_cls_GetOrderDocsWS docs = new RQO_cls_GetOrderDocsWS();
            	RQO_cls_GetOrderDocsWS.OrderDocCustomResponse factura = docs.getInvoice('test', 'test', 'test');
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_NOK, factura.errorCode);
        }
    }
    
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de documento de albaran
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getDeliveryNote(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocs@testorg.com');
        System.runAs(u){
            
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_ALBARAN_DOC, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
                RQO_cls_GetOrderDocsWS docs = new RQO_cls_GetOrderDocsWS();
            	RQO_cls_GetOrderDocsWS.OrderDocCustomResponse factura = docs.getDeliveryNote('test', 'test');
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_OK, factura.errorCode);
        }
    }
	
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la ejecución incorrecta de la funcionalidad de obtención de documento de albaran
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getDeliveryNoteNOK(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocs@testorg.com');
        System.runAs(u){
            
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_ALBARAN_DOC, RQO_cls_Constantes.MOCK_EXEC_NOK, RQO_cls_Constantes.REQ_WS_RESPONSE_NO_DATA_ERROR));
                RQO_cls_GetOrderDocsWS docs = new RQO_cls_GetOrderDocsWS();
            	RQO_cls_GetOrderDocsWS.OrderDocCustomResponse factura = docs.getDeliveryNote('test', 'test');
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, factura.errorCode);
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la ejecución incorrecta (excepción o respuesta vacía del mock) de la funcionalidad de obtención de documento de albaran
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getDeliveryNoteNOKException(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocs@testorg.com');
        System.runAs(u){
            
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_ALBARAN_DOC, RQO_cls_Constantes.MOCK_EXEC_EXCEPTION, '0'));
                RQO_cls_GetOrderDocsWS docs = new RQO_cls_GetOrderDocsWS();
            	RQO_cls_GetOrderDocsWS.OrderDocCustomResponse factura = docs.getDeliveryNote('test', 'test');
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_NOK, factura.errorCode);
        }
    }
    
    
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de documento de certificado de analisis
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getAnilisysCertificate(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocs@testorg.com');
        System.runAs(u){
            
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_CERTIFICADO_ANALISIS_DOC, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
                RQO_cls_GetOrderDocsWS docs = new RQO_cls_GetOrderDocsWS();
            	RQO_cls_GetOrderDocsWS.OrderDocCustomResponse factura = docs.getAnilisysCertificate('test', 'test', 'test');
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_OK, factura.errorCode);
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la ejecución incorrecta (excepción o respuesta vacía del mock)  de la funcionalidad de obtención de documento de certificado de analisis
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getAnilisysCertificateNOK(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocs@testorg.com');
        System.runAs(u){
            
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_CERTIFICADO_ANALISIS_DOC, RQO_cls_Constantes.MOCK_EXEC_NOK, RQO_cls_Constantes.REQ_WS_RESPONSE_NO_DATA_ERROR));
                RQO_cls_GetOrderDocsWS docs = new RQO_cls_GetOrderDocsWS();
            	RQO_cls_GetOrderDocsWS.OrderDocCustomResponse factura = docs.getAnilisysCertificate('test', 'test', 'test');
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, factura.errorCode);
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la ejecución incorrecta de la funcionalidad de obtención de documento de certificado de analisis
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getAnilisysCertificateNOKException(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocs@testorg.com');
        System.runAs(u){
            
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_CERTIFICADO_ANALISIS_DOC, RQO_cls_Constantes.MOCK_EXEC_EXCEPTION, '0'));
                RQO_cls_GetOrderDocsWS docs = new RQO_cls_GetOrderDocsWS();
            	RQO_cls_GetOrderDocsWS.OrderDocCustomResponse factura = docs.getAnilisysCertificate('test', 'test', 'test');
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_NOK, factura.errorCode);
        }
    }
}