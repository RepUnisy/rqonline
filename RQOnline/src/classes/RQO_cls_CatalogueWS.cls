/**
 *	@name: RQO_cls_CatalogueWS
 *	@version: 1.0
 *	@creation date: 13/10/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase Apex con los métodos necesarios para extraer de SAP los datos de catálogo que se puedan necesitar en la aplicación
 *  @testClass: RQO_cls_CatalogueWSTest
*/
public without sharing class RQO_cls_CatalogueWS {
    
    private List<RQO_obj_logProcesos__c> listProcessLog;
    private RQO_obj_logProcesos__c objLog;
    
    /**
	* @creation date: 17/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método para obtener los grados de los acuerdos asociados a un cliente.
	* @param: accountId, tipo Id. Id de la cuenta en salesforce
	* @param: contactId, tipo Id. Id del contacto que se va a asociar en el objeto mi catalogo
	* @return: CatalogueCustomResponse, objeto con los datos de respuesta
	* @exception: 
	* @throws: 
	*/
    public CatalogueCustomResponse refreshMyCatalogue(Id accountId, Id contactId){
		final String METHOD = 'refreshMyCatalogue';
        final String CLASS_NAME = 'RQO_cls_CatalogueWS';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        CatalogueCustomResponse catCR = new CatalogueCustomResponse();
        Boolean generateLog = false;
        //Inicializamos los objetos de log
		this.instanceInitalLog();
        if(accountId != null && contactId != null){
            try{
                //Llamamos al método genérico que monta la requets y realiza el callout
                RQO_cls_SAPServicesWS.ZqoCargaAcuerdosResponse_element returnData = this.getSapCatalogueData(RQO_cls_Constantes.CAT_WS_TYPE_AGREEMENT, new List<Id>{accountId});
                if(returnData != null){
                    //Si tengo datos de vuelta llamamos al método de gestión de respuesta
                    this.manageMyCatalogueReturnData(returnData, contactId, catCR);
                    catCR.errorCode = RQO_cls_Constantes.SOAP_COD_OK;
                }
            }catch(System.CalloutException ex) {
                System.debug('CallOut Exception: ' + ex.getMessage());
                //Si da una excepción de tipo callout genero el log correspondiente
                RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getStackTraceString());
                catCR.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
                generateLog = true;
			}catch(Exception ex){
                System.debug('Exception: ' + ex.getMessage());
                //Si da una excepción genérica genero el log correspondiente
				RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getStackTraceString());
				catCR.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
                generateLog = true;
            }
        }else{
            String errMess = Label.RQO_lbl_MyCatalogueNoDataMsg + RQO_cls_Constantes.COLON + accountId + RQO_cls_Constantes.COMMA + contactId;
            //Si no tengo cuenta o contacto genero el log de error correspondiente
			RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, errMess, null);
            catCR.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            generateLog = true;
        }
        if (generateLog){RQO_cls_ProcessLog.generateLogData(listProcessLog);}        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return catCR;
    }
    
    /**
	* @creation date: 13/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método genérico que monta la request y realiza el callout contra SAP parsa obtener los acuerdos o pedidos
	* @param: typeAgreement, tipo String. Tipo de acuerdo, A: Acuerdos, P: Pedidos.
	* @param: clientData, tipo List<Id>. Lista de ids de las cuentas (clientes) sobre las que se quieren recuperar los acueros o pedidos
	* @return: RQO_cls_SAPServicesWS.ZqoTtGacuerdos
	* @exception: 
	* @throws: 
	*/
    private RQO_cls_SAPServicesWS.ZqoCargaAcuerdosResponse_element getSapCatalogueData(String typeAgreement, List<Id> clientData){
		final String METHOD = 'getSapCatalogueData';
        final String CLASS_NAME = 'RQO_cls_CatalogueWS';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        RQO_cls_SAPServicesWS.ZqoCargaAcuerdosResponse_element agreementResponse = null;
        //Convertimos la lista de id de cuenta en una lista de idexternos (ids de SAP) de esas cuentas
        List<String> listClient = this.getSapClientId(clientData);
        if (listClient != null && !listClient.isEmpty()){
            Date dateToday = date.today();
            /*Establecemos los items de cliente*/
            RQO_cls_SAPServicesWS.ZqoTtClientes clients = new RQO_cls_SAPServicesWS.ZqoTtClientes();
            clients.item = listClient;
            /**/
            String endDate = this.getEndDate(dateToday);
            String startDate = this.getStartDate(dateToday);
            String pedAc = typeAgreement;
            RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE objCall = new RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE();  
            agreementResponse = objCall.ZqoCargaAcuerdos(null, clients, null, null, endDate, startDate, pedAc, objLog);
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return agreementResponse;
    }
    
    /**
	* @creation date: 17/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera una lista de ids externos (ids de SAP) de la cuentas que nos llegan como parámetro
	* @param: clientData, tipo List<Id>. Lista de ids de las cuentas (clientes) sobre las que se quieren recuperar los acueros o pedidos
	* @return: List<String>
	* @exception: 
	* @throws: 
	*/
    private List<String> getSapClientId(List<Id> clientData){
		final String METHOD = 'getSapClientId';
        final String CLASS_NAME = 'RQO_cls_CatalogueWS';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<String> listClient = new List<String>();
        for(Account obj : [Select RQO_fld_idExterno__c from Account where Id =: clientData]){
            if(String.isNotempty(obj.RQO_fld_idExterno__c)){
				listClient.add(obj.RQO_fld_idExterno__c);
            }
        }
        if(listClient ==  null || listClient.isEmpty()){
            RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_MyCatalogueNoClientsSF + String.valueOf(clientData), null);
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return listClient;
    }
    
    /**
	* @creation date: 18/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera la fecha de inicio que se envía en la petición. La fecha de inicio es el primer día del mes anterior al actual
	* @param: dateToday, tipo Date. Día actual
	* @return: String
	* @exception: 
	* @throws: 
	*/
    private String getStartDate(Date dateToday){
        Date auxDate = dateToday.addMonths(-1).toStartOfMonth();
        return String.valueOf(auxDate);
    }
    
	/**
	* @creation date: 22/01/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera la fecha de fin que se envía en la petición. La fecha de fin es el último día del mes siguiente al actual
	* @param: dateToday, tipo Date. Día actual
	* @return: String
	* @exception: 
	* @throws: 
	*/
    private String getEndDate(Date dateToday){
        Date auxDate = dateToday.addMonths(2).toStartOfMonth().addDays(-1);
        return String.valueOf(auxDate);
    }
    
    /**
	* @creation date: 17/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Gestiona la respuesta del servicio para generar un mapa donde la key es el id del grado en Salesforce y el valor
	*	es el objeto mi catalogo con todos los datos cargados
	* @param: clientData, tipo List<Id>. Lista de ids de las cuentas (clientes) sobre las que se quieren recuperar los acueros o pedidos
	* @return:
	* @exception: 
	* @throws: 
	*/
    private void manageMyCatalogueReturnData(RQO_cls_SAPServicesWS.ZqoCargaAcuerdosResponse_element returnData, Id contactId, CatalogueCustomResponse catCR){
		final String METHOD = 'manageMyCatalogueReturnData';
        final String CLASS_NAME = 'RQO_cls_CatalogueWS';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        RQO_cls_SAPServicesWS.ZqoTtGacuerdos acuerdos = returnData.EtAcuerdos;
        Map<Id, RQO_obj_myCatalogue__c> sapCatlogue = new Map<Id, RQO_obj_myCatalogue__c>();
        if (acuerdos.item != null && !acuerdos.item.isempty()){
            List<String> sapGradeId = new List<String>();
            //Recuperamos de la response del servicio la lista de grados de los acuerdos y generamos una lista con su ids de SAP
            for(RQO_cls_SAPServicesWS.ZqoStGacuerdos objReturn : acuerdos.item){
                if (String.isNotempty(objReturn.GradoSap)){sapGradeId.add(objReturn.GradoSap);}
            }
            if(sapGradeId != null && !sapGradeId.isEmpty()){
                RQO_obj_myCatalogue__c objMC = null;
                //Consultamos los grados de Salesforce en los que coinciden los ids de sap de la lista generada con la response y generamos los objetos y el mapa
                //de mi catalogo
                for(RQO_obj_grade__c objGrad : [Select Id from RQO_obj_grade__c where RQO_fld_sku__r.RQO_fld_sku__c = : sapGradeId]){
                    objMC = new RQO_obj_myCatalogue__c();
                    objMC.RQO_fld_grade__c = objGrad.Id;
                    objMC.RQO_fld_contact__c = contactId;
                    sapCatlogue.put(objGrad.Id, objMC);
                }
                
            }
		}
        catCR.errorCode = RQO_cls_Constantes.SOAP_COD_OK;
        catCR.sapCatlogue = sapCatlogue;
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    
   	/**
	* @creation date: 17/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Instancia la lista de logs y el objeto log con los datos iniciales
	* @param:
	* @return: void
	* @exception: 
	* @throws: 
	*/
    private void instanceInitalLog(){
        if(listProcessLog == null){listProcessLog = new List<RQO_obj_logProcesos__c>();}
        objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_SAP, RQO_cls_Constantes.SERVICE_GET_AGREEMENTS);
        listProcessLog.add(objLog);
    }
	
    /**
	* @creation date: 20/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Objeto wrapper para devolver los datos de la llamada al servicio
	* @param:
	* @return: void
	* @exception: 
	* @throws: 
	*/
    public class CatalogueCustomResponse{
        public String errorCode{get;set;}
        public Map<Id, RQO_obj_myCatalogue__c> sapCatlogue{get;set;}
    }
}