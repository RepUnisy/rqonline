/*------------------------------------------------------------
Author:        	Rubén Simarro
Company:       	Indra
Description:   	
Clase encargada de cambiar el estado de la evaluacion a Finalizada, el del patrocinio a Programa Evaluado, actualiza ambos en BD 
y mostrar un mensaje informativo para mostrar por pantalla
Test Class:    	PT_test_button_finalizarEvaluacion
History
<Date>      	<Author>     	<Change Description>
04-02-2016	    Rubén Simarro 	Initial Version
------------------------------------------------------------*/
global class PT_button_finalizarEvaluacion {

     webservice static String finalizarEvaluacion(String idEvaluacionFinalizar){
                
         String mensajeRetorno='';
         
         try{
                
             system.debug('@@@@@@@@@ PT_button_finalizarEvaluacion finalizarEvaluacion()');            

             Evaluacion__c evaluacionAux = [SELECT name, Estado__c, formula_estado__c, patrocinio__c, alcanzado_objetivos__c,alcanzados_criterios__c,
                                            objetivo_incrementar_ventas__c,
                                            objetivo_mejorar_atributos_de_imagen__c,
                                            objetivo_mejorar_relaciones__c
                                            from Evaluacion__c where Id =:idEvaluacionFinalizar];
           
             if(evaluacionAux.alcanzado_objetivos__c == null || evaluacionAux.Alcanzados_criterios__c == null){          
                 mensajeRetorno = Label.etiqueta_clase_button_finalizar_evaluacion_objetivos;
             }
             else if(evaluacionAux.formula_estado__c == Label.etiqueta_estado_evaluacion_borrador)   {
            
                 Patrocinios__c patrocinioAux = [SELECT Estado__c from Patrocinios__c where Id =:evaluacionAux.patrocinio__c];
                 
                 evaluacionAux.Estado__c = 'Finalizada';   
          
                 patrocinioAux.Estado__c = 'Programa evaluado';
          
                 update(evaluacionAux);
                 update(patrocinioAux);
                     
                 mensajeRetorno = Label.etiqueta_clase_button_finalizar_evaluacion;
    
                 system.debug('@@@: '+ evaluacionAux.name);
                 system.debug('@@@ '+Label.etiqueta_clase_button_finalizar_evaluacion+' @@@@');
             }    
             else{
                  mensajeRetorno = Label.etiqueta_clase_button_finalizar_evaluacion_error;
                 
                  system.debug('@@@@@ '+Label.etiqueta_clase_button_finalizar_evaluacion_error+' @@@@');    
             }     
             return mensajeRetorno;      
         }
         catch(Exception e){
        
            mensajeRetorno = Label.error_generico;
            
             system.debug('@@@  ERROR PT_button_finalizarEvaluacion finalizarEvaluacion(): '+mensajeRetorno);
           
            return mensajeRetorno;
        }
    }
    
    webservice static String getMensajeinformativo(){
       return Label.etiqueta_clase_button_finalizar_evaluacion_aviso;
    }
  
}