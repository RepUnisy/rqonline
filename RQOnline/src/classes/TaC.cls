public class TaC{

   public user currentUser { get; set; }
   public boolean TACAcceptedCheckBox { get; set; }

    public TaC() {
        currentUser = [select   ProfileId, TaCAccepted__c 
                            from                user 
                            where id = :userInfo.getUserId()];

        System.debug('==> POK constructor currentUser.TaCAccepted__c : ' + currentUser.TaCAccepted__c);
        
       
    }
    
    public PageReference Save(){
        try{
            System.debug('==> POK Save before Update');


            //Call Web Service to update user profile
            partnerSoapSforceCom.Soap sp = new partnerSoapSforceCom.Soap();
                                  
            TaC_Custom_Setting__c settings = TaC_Custom_Setting__c.getOrgDefaults();
            Blob key = EncodingUtil.base64Decode(settings.Key__c);
            Blob user = EncodingUtil.base64Decode(settings.User__c);
            Blob pass = EncodingUtil.base64Decode(settings.Pass__c);
            
            Blob blobUserDecrypt = Crypto.decryptWithManagedIV('AES256', key, user);
            String user2 = blobUserDecrypt.toString();
            
            Blob blobPassDecrypt = Crypto.decryptWithManagedIV('AES256', key, pass);
            String pass2 = blobPassDecrypt.toString();
            
            partnerSoapSforceCom.LoginResult loginResult = sp.login(user2, pass2); 
            
            System.Debug('==> POK : ' + loginResult.serverUrl);
            System.Debug('==> POK : ' + loginResult.sessionId);
            
            TaCUpdateProfileWS.TaCUpdateProfile ws = new TaCUpdateProfileWS.TaCUpdateProfile();
            ws.SessionHeader = new TaCUpdateProfileWS.SessionHeader_element();
            ws.SessionHeader.sessionId = loginResult.sessionId;
            
            System.Debug ('===============> POK 1 before WS call'); 
            ws.DoUpdateProfile(currentUser.Id);
            System.Debug ('===============> POK 2 after WS call'); 

            // N.B.: No update here since update is done in WS call

            // Consider returning to User Preference landing page (in case TaC are asked again to same user later due to a law change or new functionality release)
            return new PageReference('/home/home.jsp');
        }catch(Exception e){
            System.debug(e.getMessage());   
            return null;
        }
    }
       
}