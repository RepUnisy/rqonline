/**
 * @name: RQO_cls_handlerBeforeContentDocumentLink
 * @version: 1.0
 * @creation date: 14/01/2018
 * @author: Unisys
 * @description: Clase Handler para Trigger Before Insert de ContentDocumentLink - Actualiza la visibilidad del document
*/

public class RQO_cls_handlerBeforeContentDocumentLink {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_handlerBeforeContentDocumentLink';
    
    /* @creation date: 14/01/2018
     * @author: Unisys
     * @description: Método handler que actualiza la visibilidad del documento
     * @param: newDocument y oldDocument 
     * @return: void
     * @exception: No aplica
     * @throws: No aplica */
    public static void handlerBeforeContentDocumentLink (ContentDocumentLink newDocument) {
    	
    	// ***** CONSTANTES ***** //
        final String METHOD = 'handlerBeforeContentDocumentLink';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': newDocument ' + newDocument);
        
		newDocument.Visibility = 'AllUsers';
    }
}