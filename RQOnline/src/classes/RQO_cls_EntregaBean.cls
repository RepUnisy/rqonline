/**
 *	@name: RQO_cls_EntregaBean
 *	@version: 1.0
 *	@creation date: 16/10/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de Entregas.
 */
public class RQO_cls_EntregaBean {

    public String externalId {get; set;}
    //public String posicionPedido {get; set;}
    public String mascEdicSegmFecha {get; set;}
    public String claseEntrega {get; set;}
    public String numExtEntrega {get; set;}
    public String fechaSalidaMercancias {get; set;}
    public String fechaEntregaClienteAlbaran {get; set;}
    public String fechaMovimientoMercancias {get; set;}
    public String fechaRealLlegada {get; set;}
    public String factura {get; set;}
    public String estadoEntrega {get; set;}
    public String isDelete {get; set;}
    public String fechaEstimadaLLegada {get; set;}
    public List <RQO_cls_PosicionEntregaBean> posicionesEntregaList {get; set;}
    
}