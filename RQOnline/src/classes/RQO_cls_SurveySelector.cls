/**
*   @name: RQO_cls_SurveySelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la obtención de Surveys
*/
public with sharing class RQO_cls_SurveySelector extends RQO_cls_ApplicationSelector {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_SurveySelector';
    
    /**
    *   @name: RQO_cls_SurveySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la lista de campos de una RQO_obj_survey__c
    */
    public List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            RQO_obj_survey__c.Name,
            RQO_obj_survey__c.RQO_fld_nameEn__c,
            RQO_obj_survey__c.RQO_fld_nameFr__c,
            RQO_obj_survey__c.RQO_fld_nameDe__c,
            RQO_obj_survey__c.RQO_fld_namePt__c,
            RQO_obj_survey__c.RQO_fld_nameIt__c,
            RQO_obj_survey__c.RQO_fld_description__c,
            RQO_obj_survey__c.RQO_fld_status__c,
            RQO_obj_survey__c.RQO_fld_startDate__c,
            RQO_obj_survey__c.RQO_fld_endDate__c,
            RQO_obj_survey__c.RQO_fld_customerProfile__c,
            RQO_obj_survey__c.RQO_fld_userProfile__c,
            RQO_obj_survey__c.RQO_fld_home__c,
            RQO_obj_survey__c.RQO_fld_anonymous__c,
            RQO_obj_survey__c.RQO_fld_emailSending__c,
            RQO_obj_survey__c.RQO_fld_alreadySent__c };
    }

    /**
    *   @name: RQO_cls_SurveySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el sObjectType de RQO_obj_survey__c
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_survey__c.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_SurveySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual se ordenan los RQO_fld_startDate__c
    */
    public override String getOrderBy()
    {
        return 'RQO_fld_startDate__c';
    }

    /**
    *   @name: RQO_cls_SurveySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de RQO_obj_survey__c mediante un conjunto de Ids
    */
    public List<RQO_obj_survey__c> selectById(Set<ID> idSet)
    {
         return (List<RQO_obj_survey__c>) selectSObjectsById(idSet);
    }
    
    /**
    *   @name: RQO_cls_SurveySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el resultado de una query con los localizadores de 
    *   los EmailSurveys
    */
    public Database.QueryLocator queryLocatorEmailSurveys()
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'queryLocatorEmailCommunications';
        
        // Encuestas seleccionadas para correo, publicadas en el pasado y que no se hayan envíado.
        string query = newQueryFactory().setCondition('RQO_fld_emailSending__c = true AND RQO_fld_alreadySent__c = false AND ' + 
                                                      'RQO_fld_startDate__c <= TODAY AND RQO_fld_endDate__c >= TODAY').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        return Database.getQueryLocator(query);
    }
    
    /**
    *   @name: RQO_cls_SurveySelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de Account Ids
    */
    public List<RQO_obj_survey__c> selectByAccountId(Set<ID> idSet)
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectByAccountId';
        
        // Recuperar encuestas para los clientes
        List<RQO_obj_surveyAccount__c> surveys = new RQO_cls_SurveyAccountSelector().selectByAccountId(idSet);
        Set<Id> surveyIds = new Set<Id>();
        for (RQO_obj_surveyAccount__c survey : surveys) {
           surveyIds.add(survey.RQO_fld_account__c);
        }
        
        string query = newQueryFactory().setCondition('RQO_fld_accountsNumber__c = 0 OR Id IN :surveyIds').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        return (List<RQO_obj_survey__c>) Database.query(query);
    }
}