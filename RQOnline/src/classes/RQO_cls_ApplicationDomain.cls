/**
*   @name: RQO_cls_ApplicationDomain
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona el dominio de la aplicaci
*/
public abstract class RQO_cls_ApplicationDomain extends fflib_SObjectDomain 
{   
    public RQO_cls_ApplicationDomain(List<SObject> records) 
    {     
        super(records);     
        // Disable CRUD security enforcement at the Domain class level     
        //	Configuration.disableTriggerCRUDSecurity();   
    }
}