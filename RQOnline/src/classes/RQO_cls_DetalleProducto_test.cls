/**
*   @name: RQO_cls_DetalleProducto_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Julio Maroto - Unisys
*   @description: Clase de test para probar la clase RQO_cls_DetalleProducto_test
*/
@isTest
public class RQO_cls_DetalleProducto_test {
    private static List<RQO_obj_masterSpecification__c> masterSpecificationList;
    private static List<RQO_obj_catSpecification__c> categorySpecificationList;
    private static List<RQO_obj_specification__c> gradeSpecificationList;
    private static List<RQO_obj_translation__c> masterSpecTranslationList;
    private static List<RQO_obj_translation__c> gradeTranslationList;
    private static List<RQO_obj_category__c> categoryList;
    private static List<RQO_obj_grade__c> gradeList;
    
    private static Map<Id, String> mapTranslate{get;set;}
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear el juego de datos inicial necesario para el test
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @testSetup
    public static void initialSetup() {
        // Create user
        createUser();
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            setCategoryList();
            setTranslationMasterSpecificationsList();
            setCategorySpecificationList();
            setGradeSpecificationList();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear usuario para el contexto de ejecución de los test de la clase
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createUser() {
		RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es',
                                             'es_ES','Europe/Berlin');
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Categories
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void setCategoryList() {
        categoryList = RQO_cls_TestDataFactory.createCategory(200);        
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear traducciones
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    private static void setTranslationMasterSpecificationsList() {
        masterSpecTranslationList = RQO_cls_TestDataFactory.createTranslation(200);
    }
    
        /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Category Specifications
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
        
    private static void setCategorySpecificationList() {
        categorySpecificationList = RQO_cls_TestDataFactory.createCatSpecification(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear especificaciones
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    private static void setGradeSpecificationList() {
        gradeSpecificationList = RQO_cls_TestDataFactory.createSpecification(200);      
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear getDetalleProductoNotEmpty de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testGetDetalleProductoNotEmpty() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            gradeList = [
                SELECT
                    Id
                FROM
                    RQO_obj_grade__c
            ];
            
            List<RQO_cls_DetalleProductoAuxObj> gradeProperties = RQO_cls_DetalleProducto_cc.getDetalleProducto(
                'test',
                gradeList.get(0).Id
            );
            
            System.assert(!gradeProperties.isEmpty());
            
            Test.stopTest();
    	}
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear getDetalleProductoReturnsMasterSpecName de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testGetDetalleProductoReturnsMasterSpecName() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            deleteMasterSpecTranslations();
            
            gradeList = [
                SELECT
                    Id
                FROM
                    RQO_obj_grade__c
            ];
            
            List<RQO_obj_specification__c> specificationsList = [
                SELECT
                    RQO_fld_value__c,
                    RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c,
                    RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.name,
                    RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.RQO_fld_unitMeasure__c
                FROM
                    RQO_obj_specification__c
                WHERE
                    RQO_fld_grade__c =: gradeList.get(0).Id
                ORDER BY
                    RQO_fld_catSpecification__r.RQO_fld_position__c ASC
                LIMIT 1000
            ];
            
            List<RQO_cls_DetalleProductoAuxObj> listExpected = new List<RQO_cls_DetalleProductoAuxObj>();
            String masterSpecName = '';
            
            for (RQO_obj_specification__c spec : specificationsList) {
                masterSpecName = spec.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.name;
                
                RQO_cls_DetalleProductoAuxObj productDetail = new RQO_cls_DetalleProductoAuxObj(
                    masterSpecName,
                    spec.RQO_fld_value__c,
                    spec.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.RQO_fld_unitMeasure__c
                );
                
                listExpected.add(productDetail);
            }
    
            List<RQO_cls_DetalleProductoAuxObj> listActual = new List<RQO_cls_DetalleProductoAuxObj>();
            listActual = RQO_cls_DetalleProducto_cc.getDetalleProducto('test', gradeList.get(0).Id);
            
            System.assertEquals(listExpected.size(), listActual.size());
            
            Integer listExpectedSize = listExpected.size();
            
            for (Integer i = 0; i < listExpectedSize; i++) {
                RQO_cls_DetalleProductoAuxObj listExpectedItemAux = listExpected.get(i);
                RQO_cls_DetalleProductoAuxObj listActualItemAux = listActual.get(i);
                
                System.assertEquals(listExpectedItemAux.description, listActualItemAux.description);
            }
            
            Test.stopTest();
    	}
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear getDetalleProductoReturnsMasterSpecNameTranslation de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testGetDetalleProductoReturnsMasterSpecNameTranslation() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            gradeList = [
                SELECT
                    Id
                FROM
                    RQO_obj_grade__c
            ];
            
            List<RQO_obj_specification__c> specList = [
                SELECT
                    RQO_fld_value__c,
                    RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c, 
                    RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.name,
                    RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.RQO_fld_unitMeasure__c
                FROM
                    RQO_obj_specification__c
                WHERE
                    RQO_fld_grade__c =: gradeList.get(0).Id
                ORDER BY
                    RQO_fld_catSpecification__r.RQO_fld_position__c ASC
                LIMIT 1000
            ];
            
            getTranslate(specList, 'test');
            
            List<RQO_cls_DetalleProductoAuxObj> productDetailListExpected = getProductDetailList(specList);
            
            List<RQO_cls_DetalleProductoAuxObj> productDetailListActual = RQO_cls_DetalleProducto_cc.getDetalleProducto(
                'test',
                gradeList.get(0).Id
            );
            
            for (Integer i = 0; i < productDetailListExpected.size(); i++) {
                RQO_cls_DetalleProductoAuxObj itemProdDetListExpected = productDetailListExpected.get(i);
                RQO_cls_DetalleProductoAuxObj itemProdDetListActual = productDetailListActual.get(i);
                
                System.assertEquals(itemProdDetListExpected.description, itemProdDetListActual.description);
            }
            
            Test.stopTest();
    	}
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para obtener traducciones
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
     private static void getTranslate(List<RQO_obj_specification__c> specList, String lenguaje) {
        List<Id> listMaster = new List<Id>();
        
        for (RQO_obj_specification__c spec : specList) {
            listMaster.add(spec.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c);
        }
  
        for (RQO_obj_translation__c objTrans : [
            SELECT
            	Id,
            	RQO_fld_masterSpecification__c,
            	RQO_fld_detailTranslation__c,
            	RQO_fld_translation__c
            FROM
            	RQO_obj_translation__c
            WHERE
            	RQO_fld_masterSpecification__c =: listMaster
            AND
            	RQO_fld_locale__c =: lenguaje
        ]) {
            if (null == mapTranslate) {
                mapTranslate = new Map<Id, String>();
            }
            
            mapTranslate.put(objTrans.RQO_fld_masterSpecification__c, objTrans.RQO_fld_translation__c);
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para obtener lista de detalles de producto
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static List<RQO_cls_DetalleProductoAuxObj> getProductDetailList(
        List<RQO_obj_specification__c> specList
    ) {
        List<RQO_cls_DetalleProductoAuxObj> productDetailList = new List<RQO_cls_DetalleProductoAuxObj>();
        String actualTranslate = '';
        
        for (RQO_obj_specification__c spec : specList) {
            actualTranslate = getActualTranslate(spec);
                        
            productDetailList.add(
            	new RQO_cls_DetalleProductoAuxObj(
                	actualTranslate,
                    spec.RQO_fld_value__c,
                    spec.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.RQO_fld_unitMeasure__c
                )
            );
        }
        
        return productDetailList;
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para la traducción de una especificación recibida por parámetro
    * @param: RQO_obj_specification__c specification
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static String getActualTranslate(RQO_obj_specification__c spec) {
      	String actualTranslate = null;
        
        if (mapTranslateNotEmptyAndValidForSpec(spec)) {
            actualTranslate = mapTranslate.get(spec.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c);
        } else {
        	actualTranslate = spec.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.name;
        }
        
        return actualTranslate;
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para testear si un mapa de traducciones cacheado no es vacío y es válido para una 
    * especificación (specification) pasada por parámetro
    * @param: RQO_obj_specification__c specification
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static Boolean mapTranslateNotEmptyAndValidForSpec(RQO_obj_specification__c spec) {
        if (mapTranslate != null &&
            false == mapTranslate.isEmpty() &&
            mapTranslate.containsKey(spec.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c)
        ) {
            return true;
        }
       	
        return false;
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para borrar registros de masterSpecification
    * @param:
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void deleteMasterSpecTranslations() {
        gradeList = [
            SELECT
            	Id
            FROM
            	RQO_obj_grade__c
        ];
        
        List<RQO_obj_specification__c> specList = [
            SELECT
            	RQO_fld_value__c,
            	RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c,
            	RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.name,
            	RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.RQO_fld_unitMeasure__c
            FROM
            	RQO_obj_specification__c
            WHERE
            	RQO_fld_grade__c =: gradeList.get(0).Id
            ORDER BY
            	RQO_fld_catSpecification__r.RQO_fld_position__c ASC
            LIMIT 1000
        ];
        
        List<Id> listMaster = new List<Id>();
        
        for (RQO_obj_specification__c spec : specList) {
            listMaster.add(spec.RQO_fld_catSpecification__r.RQO_fld_masterSpecification__c);
        }
        
    	List<RQO_obj_translation__c> masterSpecTranslationsList = [
            SELECT
            	Id,
            	RQO_fld_masterSpecification__c,
            	RQO_fld_detailTranslation__c,
            	RQO_fld_translation__c
            FROM
            	RQO_obj_translation__c
            WHERE
            	RQO_fld_masterSpecification__c =: listMaster
            AND
            	RQO_fld_locale__c =: 'test'
        ];
        
        delete masterSpecTranslationsList;
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para cubrir el método getDetalleGradoDescripcionReturnsEmpty de la clase RQO_cls_Notificacion__c
    * @param:
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testGetDetalleGradoDescripcionReturnsEmpty() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            gradeList = [
                SELECT
                    Id
                FROM
                    RQO_obj_grade__c
            ];
            
            String descripcionExpected = '';
            
            String descriptionActual = RQO_cls_DetalleProducto_cc.getDetalleGradoDescripcion('en_US', gradeList.get(0).Id);
            
            System.assertEquals(descripcionExpected, descriptionActual);
            
            Test.stopTest();
    	}
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para cubrir el método getDetalleGradoDescripcionReturnsValidDesc de la clase RQO_cls_Notificacion__c
    * @param:
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testGetDetalleGradoDescripcionReturnsValidDesc() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            gradeList = [
                SELECT
                    Id
                FROM
                    RQO_obj_grade__c
            ];
            
            List<RQO_obj_translation__c> translationListExpected = [
                SELECT
                    RQO_fld_detailTranslation__c
                FROM
                    RQO_obj_translation__c
                WHERE
                    RQO_fld_grade__c =: gradeList.get(0).Id
                AND
                    RQO_fld_locale__c =: 'test'
                LIMIT 1
            ];
                    
            RQO_obj_translation__c translationExpected = translationListExpected.get(0);
            
            String descriptionExpected = translationExpected.RQO_fld_detailTranslation__c;
            String descriptionActual = RQO_cls_DetalleProducto_cc.getDetalleGradoDescripcion('test', gradeList.get(0).Id);
        
            System.assertEquals(descriptionExpected, descriptionActual);
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para cubrir el método getDetalleGradoCaracteristicasReturnsEmptyMap de la clase RQO_cls_Notificacion__c
    * @param:
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testGetDetalleGradoCaracteristicasReturnsEmptyMap() {
       	User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            gradeList = [
                SELECT
                    Id
                FROM
                    RQO_obj_grade__c
            ];
            
            Map<String, List<RQO_obj_translation__c>> mapCaracteristicas =
                RQO_cls_DetalleProducto_cc.getDetalleGradoCaracteristicas('test', gradeList.get(0).Id);
            
            System.assert(false == mapCaracteristicas.isEmpty());
            
            Test.stopTest();
    	}
    }
}