public class PT_controller_presupuesto {
    
    public Patrocinios__c patrocinio {get; set;}
    
    public list<user> usuarios {get; set;}
   
    public boolean mostrarDeshacer {get; set;}
     
    public PT_controller_presupuesto(ApexPages.StandardController controller){  
        usuarios = [SELECT userRole.Name, profile.name FROM user WHERE id = :UserInfo.getUserId()];
        patrocinio = [select CurrencyIsoCode, instrucciones_presupuesto__c,  presupuesto2__c,  observaciones_presupuesto__c,  Esta_planificado_en_el_area_solicitante__c, moneda__c, area_calculada__c, mostrar_presupuesto2__c from patrocinios__c where id =: ApexPages.currentPage().getParameters().get('Id')];
              
        mostrarDeshacer = false;
                     
    }
    
    public void guardarPresupuesto(){
     
        try{       
            update patrocinio;
            
            system.debug('@@@ PT_controller_presupuesto guardarPresupuesto()');
        }
        catch (Exception ex) {                   
            mostrarDeshacer = true;
        }  
    }
    
     public void limpiarMensajeError(){
               
         mostrarDeshacer = false;
         
           patrocinio = [select CurrencyIsoCode, instrucciones_presupuesto__c, presupuesto2__c,  observaciones_presupuesto__c,  Esta_planificado_en_el_area_solicitante__c, moneda__c, area_calculada__c,mostrar_presupuesto2__c from patrocinios__c where id =: ApexPages.currentPage().getParameters().get('Id')];

     }  
}