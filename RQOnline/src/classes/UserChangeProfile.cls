global without sharing class UserChangeProfile {
   @InvocableMethod
   public static void UserChangeProfile (List<UserChangeProfileRequest> requests) {
        UserChangeProfileRequest request = requests[0];  
        changeProfileWs(request.userId, request.idioma,request.updateProfile)  ;   
   }
global class UserChangeProfileRequest {

    @InvocableVariable(required=true) 
    public boolean updateProfile; 
    
    @InvocableVariable(required=true) 
    public ID userId;
    
    @InvocableVariable (required=true)
    public string idioma;   

}

public static void changeProfileWs(string userId,string idioma, boolean updateProfile){
    
    System.debug('==> POK Save before Update');

    //Call Web Service to update user profile
    partnerSoapSforceCom.Soap sp = new partnerSoapSforceCom.Soap();
                              
    TaC_Custom_Setting__c settings = TaC_Custom_Setting__c.getOrgDefaults();
    Blob key = EncodingUtil.base64Decode(settings.Key__c);
    Blob user = EncodingUtil.base64Decode(settings.User__c);
    Blob pass = EncodingUtil.base64Decode(settings.Pass__c);
            
    Blob blobUserDecrypt = Crypto.decryptWithManagedIV('AES256', key, user);
    String user2 = blobUserDecrypt.toString();
            
    Blob blobPassDecrypt = Crypto.decryptWithManagedIV('AES256', key, pass);
    String pass2 = blobPassDecrypt.toString();
            
    partnerSoapSforceCom.LoginResult loginResult = sp.login(user2, pass2); 
    //partnerSoapSforceCom.LoginResult loginResult = sp.login('admin_iecisa@ieci.es.repsol.test', 'iecisa01'); 
    System.Debug('==> POK : ' + loginResult.serverUrl);
    System.Debug('==> POK : ' + loginResult.sessionId);
            
    TaCUpdateProfileLoginFlowWS.TaCUpdateProfileLoginFlow ws = new TaCUpdateProfileLoginFlowWS.TaCUpdateProfileLoginFlow();
    ws.SessionHeader = new TaCUpdateProfileLoginFlowWS.SessionHeader_element();
    ws.SessionHeader.sessionId = loginResult.sessionId;
            
    System.Debug ('===============> POK 1 before WS call UserChangeProfile'); 
    System.Debug ('===============> user: ' + userId + ', idioma:' + idioma);
    ws.DoUpdateProfile(userId, idioma, updateProfile);
    System.Debug ('===============> POK 2 after WS call UserChangeProfile'); 
    
    
}
   
}