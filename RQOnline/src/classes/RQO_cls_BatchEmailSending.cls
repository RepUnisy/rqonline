/**
 *	@name: RQO_cls_BatchEmailSending
 *	@version: 1.0
 *	@creation date: 04/12/2017
 *	@author: Unisys
 *	@description: Clase batch que realiza el envío de correo de comunicaciones, encuestas y notificaciones
 *	@testClass: RQO_cls_BatchEmailSendingTest
*/
global with sharing class RQO_cls_BatchEmailSending implements Database.Batchable<sObject>, Database.Stateful {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_BatchEmailSending';
	
	private RQO_obj_logProcesos__c generalLog;
	private Schema.SObjectType objectType;
	
	/**
	* @creation date: 04/12/2017
	* @author: Unisys
	* @description:	Constructor de la clase RQO_cls_BatchEmailSending
	* @param: String docType
	* @param: Database.BatchableContext
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
    public RQO_cls_BatchEmailSending(Schema.SObjectType objectType){
        this.objectType = objectType;
    }
	
    /**
	* @creation date: 04/12/2017
	* @author: Unisys
	* @description:	Método start del batch. Realiza la query que obtiene las notificaciones
	* @param: Database.BatchableContext
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'start';
        
        generalLog = RQO_cls_ProcessLog.instanceInitalLogBatch(Label.RQO_lbl_DocumentObject, 
	        													   RQO_cls_Constantes.BATCH_LOG_RESULT_OK, 
	        													   null, null, null, 
	        													   RQO_cls_Constantes.BATCH_EMAIL_SENDING);
        insert generalLog;
        System.debug(CLASS_NAME + ' - ' + METHOD + ' General log: ' + generalLog.Id);
        
        if (this.objectType == RQO_obj_communication__c.sObjectType)
	        return new RQO_cls_CommunicationsSelector().queryLocatorEmailCommunications();
        else if (this.objectType == RQO_obj_survey__c.sObjectType)
			return new RQO_cls_SurveySelector().queryLocatorEmailSurveys();
        else
        	return null;
		
    }
    
    /**
	* @creation date: 04/12/2017
	* @author: Unisys
	* @description:	Método execute del batch. Envía las emails de las notificaciones
	* @param: Database.BatchableContext
	* @param: List<sObject>
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
   	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
		// ***** CONSTANTES ***** //
        final String METHOD = 'execute';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ' tamaño scope: ' + scope.size());
        
        if (this.objectType == RQO_obj_communication__c.sObjectType)
        {
	        for(RQO_obj_communication__c comunicacion : (List<RQO_obj_communication__c>) scope) {
	        	this.sendEmail(comunicacion);
	        	comunicacion.RQO_fld_alreadySent__c = true;
	        	update comunicacion;
	        }
        }
        else if (this.objectType == RQO_obj_survey__c.sObjectType)
        {
			for(RQO_obj_survey__c encuesta : (List<RQO_obj_survey__c>) scope) {
	        	this.sendEmail(encuesta);
	        	encuesta.RQO_fld_alreadySent__c = true;
	        	update encuesta;
	        }
        }
   	}
   
   /**
	* @creation date: 04/12/2017
	* @author: Unisys
	* @description:	Método finish del batch.
	* @param: Database.BatchableContext
	* @return: 	
	* @exception: 
	* @throws: 
	*/
   global void finish(Database.BatchableContext BC)
   {
		// ***** CONSTANTES ***** //
        final String METHOD = 'finish';
        
       List<RQO_obj_logProcesos__c> listGeneralLog = 
       					[Select Id from RQO_obj_logProcesos__c where RQO_fld_parentLog__c = : generalLog.Id];
       					
       if (listGeneralLog != null && !listGeneralLog.isEmpty())
       {
           generalLog.RQO_fld_errorCode__c = RQO_cls_Constantes.BATCH_LOG_RESULT_NOK;
           update generalLog;
       }
   }
   
   /**
	* @creation date: 11/12/2017
	* @author: Unisys
	* @description:	Envío de correo de una comunicacion
	* @param: RQO_obj_communication__c
	* @return:	void
	* @exception: 
	* @throws: 
	*/
    private void sendEmail(sObject sendingObject)
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'sendEmail';
        
        // Lista de correos a enviar
        List<Messaging.SingleEmailMessage> allMails = new List<Messaging.SingleEmailMessage>();
        Set<ID> idUserSet = new Set<ID>();
        EmailTemplate emailTemplate;
        
        if (this.objectType == RQO_obj_communication__c.sObjectType)
        {
	        // Cuentas asociadas a la comuniación
	    	List<RQO_obj_communicationAccount__c> cuentas = 
	    		new RQO_cls_CommunicationAccountSelector().selectByCommunicationId(new Set<ID> { sendingObject.Id });
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' Cuentas asociadas a la comunicación: ' + cuentas.size());
	
	    	for (RQO_obj_communicationAccount__c cuenta : cuentas)
	    	{
	    		idUserSet.add(cuenta.RQO_fld_account__c);
	    	}
	    	
	    	// Obtener la plantilla de comunicaciones
    		List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = 'RQO_eplt_CorreoComunicaciones'];
    		emailTemplate = lstEmailTemplates[0];
        }
        else if (this.objectType == RQO_obj_survey__c.sObjectType)
        {
			// Cuentas asociadas a la encuesta
	    	List<RQO_obj_surveyAccount__c> cuentas = 
	    		new RQO_cls_SurveyAccountSelector().selectBySurveyId(new Set<ID> { sendingObject.Id });
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' Cuentas asociadas a la encuesta: ' + cuentas.size());
	
	    	for (RQO_obj_surveyAccount__c cuenta : cuentas)
	    	{
	    		idUserSet.add(cuenta.RQO_fld_account__c);
	    	}
	    	
	    	// Obtener la plantilla de encuestas
    		List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = 'RQO_eplt_correoEncuestas'];
    		emailTemplate = lstEmailTemplates[0];
        }
        
    	// Obtener usuarios de las cuentas
    	List<User> users = new RQO_cls_UserSelector().selectByAccountIdWithContact(idUserSet);
    	
    	System.debug(CLASS_NAME + ' - ' + METHOD + ' Usuarios asociados al objeto: ' + users.size());

    	// Número de direcciones de correos
    	Integer addressesNumber = users.size();
		
        // First, reserve email capacity for the current Apex transaction to ensure
		// that we won't exceed our daily email limits when sending email after
		// the current transaction is committed.
		Messaging.reserveMassEmailCapacity(addressesNumber);
		
		// Envío de correo a cada usuario
		for (User user : users)
		{
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			
			mail.setTemplateId(emailTemplate.Id);
			mail.setSaveAsActivity(false);
			mail.setWhatId(sendingObject.Id);
			mail.setTargetObjectId(user.ContactId);
			
			// Specify the address used when the recipients reply to the email. 
			mail.setReplyTo('noreply@repsol.com');
			
			// Specify the name used as the display name.
			mail.setSenderDisplayName('Repsol Química Omline');
			
			System.debug(CLASS_NAME + ' - ' + METHOD + ' Envío de correo a : ' + user.Name);
	
			allMails.add(mail);
		}
		
		Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(allMails); 
		// TODO: procesar errores de correo
    } 

}