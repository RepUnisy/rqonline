public class AM_Sumatorio_AM{
   public static void Sumatorio(AM_Accion_de_mejora__c newAmejora){
             //Your code in the class
			
			
			if(newAmejora.AM_REF_Estacion_de_servicio__c!=null){
	
				List<AM_Accion_de_mejora__c> lstAMejora = [SELECT Id, Name, AM_SEL_Estatus__c FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c =: newAmejora.AM_REF_Estacion_de_servicio__c and (AM_SEL_Estatus__c='Realizada' OR AM_SEL_Estatus__c='En curso')];
				
				List<AM_Accion_de_mejora__c> lstAMejora1=new List <AM_Accion_de_mejora__c>();
				List<AM_Accion_de_mejora__c> lstAMejora2=new List <AM_Accion_de_mejora__c>();
				system.debug(lstAMejora);
				for(AM_Accion_de_mejora__c aux:lstAMejora){
					if(aux.AM_SEL_Estatus__c=='En curso'){
												
						lstAMejora1.add(aux);
					}
					else{
						lstAMejora2.add(aux);
						
					}
				}	
					system.debug(lstAMejora1);
                	system.debug(lstAMejora2);
                
						AM_Estacion_de_servicio__c estacionServicio = [SELECT Id, Name, N_mero_AMejora_Finalizadas__c, N_mero_AMejora_EnCurso__c,N_mero_AMejora_Asociadas__c  FROM AM_Estacion_de_servicio__c WHERE Id =: newAmejora.AM_REF_Estacion_de_servicio__c];
						if(estacionServicio != null){
              	
                        estacionServicio.N_mero_AMejora_Asociadas__c = lstAMejora.size();
						estacionServicio.N_mero_AMejora_EnCurso__c = lstAMejora1.size();
						estacionServicio.N_mero_AMejora_Finalizadas__c = lstAMejora2.size();
                         
						update (estacionServicio);
        }
    }
   }
}