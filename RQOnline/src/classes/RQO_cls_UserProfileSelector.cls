public with sharing class RQO_cls_UserProfileSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_UserProfileSelector';
	
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_cmt_userProfilePermission__mdt.RQO_fld_userProfile__c,
			RQO_cmt_userProfilePermission__mdt.RQO_fld_permission__c,
			RQO_cmt_userProfilePermission__mdt.RQO_fld_url__c};
    }
    
    public List<String> getFieldNamesList()
    {
    	List<String> namesList = new List<String>();
    	for (Schema.SObjectField field : getSObjectFieldList()) {
		    namesList.add(field.getDescribe().getName());
		}
    	return namesList;
    }

    public Schema.SObjectType getSObjectType()
    {
        return RQO_cmt_userProfilePermission__mdt.sObjectType;
    }
    
    public override String getOrderBy()
	{
		return 'RQO_fld_userProfile__c, RQO_fld_permission__c';
	}

    public List<RQO_cmt_userProfilePermission__mdt> selectByProfile(Set<string> profileSet)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'selectByProfile';
        
        string query = newQueryFactory().setCondition('RQO_fld_userProfile__c in :profileSet').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
 		List<RQO_cmt_userProfilePermission__mdt> permissions = (List<RQO_cmt_userProfilePermission__mdt>) 
														            Database.query(query); 
		return permissions;
										                
    }
    
    public Set<string> selectProfiles()
    {
    	Set<string> distinctProfiles = new Set<string>();
    	
 		List<RQO_cmt_userProfilePermission__mdt> permissions =  (List<RQO_cmt_userProfilePermission__mdt>) 
														            Database.query(newQueryFactory().toSOQL()); 
 		for (RQO_cmt_userProfilePermission__mdt permission : permissions)
        	distinctProfiles.add((String)permission.RQO_fld_userProfile__c);

		return distinctProfiles;							                
    }

    public Set<string> selectPermissions()
    {
    	Set<string> distinctPermissions = new Set<string>();
    	
 		List<RQO_cmt_userProfilePermission__mdt> permissions =  (List<RQO_cmt_userProfilePermission__mdt>) 
														            Database.query(newQueryFactory().toSOQL()); 
 		for (RQO_cmt_userProfilePermission__mdt permission : permissions)
        	distinctPermissions.add((String)permission.RQO_fld_permission__c);

		return distinctPermissions;							                
    }
    
    public Set<string> selectPermissionsLabels()
    {
    	Set<string> distinctPermissions = new Set<string>();
    	
 		List<RQO_cmt_userProfilePermission__mdt> permissions =  (List<RQO_cmt_userProfilePermission__mdt>) 
														            Database.query(newQueryFactory().toSOQL()); 
														            
		for (RQO_cmt_userProfilePermission__mdt permission : permissions)
 		{
 			List<Schema.PicklistEntry> permissionEntries = 
 				RQO_cmt_userProfilePermission__mdt.RQO_fld_permission__c.getDescribe().getPicklistValues();
 			for (Schema.PicklistEntry permissionEntry : permissionEntries)
 			{
 				if (permissionEntry.getValue() == permission.RQO_fld_permission__c)
 					distinctPermissions.add(permissionEntry.getLabel());
 			}
 		}

		return distinctPermissions;							                
    }
    
    public Set<string> selectRestrictedUrls()
    {
    	Set<string> restrictedUrls = new Set<string>();
    	
 		List<RQO_cmt_userProfilePermission__mdt> permissions =  (List<RQO_cmt_userProfilePermission__mdt>) 
														            Database.query(newQueryFactory().toSOQL()); 
 		for (RQO_cmt_userProfilePermission__mdt permission : permissions)
        	restrictedUrls.add((String)permission.RQO_fld_url__c);

		return restrictedUrls;							                
    }
    
	public Set<string> selectPermissionsApiNameByProfile(string profile)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'selectPermissionsApiNameByProfile';
        
        string query = newQueryFactory().setCondition('RQO_fld_userProfile__c = \'' + profile + '\'').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
    	Set<string> distinctPermissions = new Set<string>();
    	
 		List<RQO_cmt_userProfilePermission__mdt> permissions =  (List<RQO_cmt_userProfilePermission__mdt>) 
														            Database.query(query); 
 		for (RQO_cmt_userProfilePermission__mdt permission : permissions)
 			distinctPermissions.add(permission.RQO_fld_permission__c);

		return distinctPermissions;							                
    }
    
   	public Set<string> selectPermissionsByProfile(string profile)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'selectPermissionsByProfile';
        
        string query = newQueryFactory().setCondition('RQO_fld_userProfile__c = \'' + profile + '\'').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
    	Set<string> distinctPermissions = new Set<string>();
    	
 		List<RQO_cmt_userProfilePermission__mdt> permissions =  (List<RQO_cmt_userProfilePermission__mdt>) 
														            Database.query(query); 
 		for (RQO_cmt_userProfilePermission__mdt permission : permissions)
 		{
 			List<Schema.PicklistEntry> permissionEntries = 
 				RQO_cmt_userProfilePermission__mdt.RQO_fld_permission__c.getDescribe().getPicklistValues();
 			for (Schema.PicklistEntry permissionEntry : permissionEntries)
 			{
 				if (permissionEntry.getValue() == permission.RQO_fld_permission__c)
 					distinctPermissions.add(permissionEntry.getLabel());
 			}
 		}

		return distinctPermissions;							                
    }
    
    public Set<string> selectUrlsByProfile(string profile)
    {
    	Set<string> distinctPermissions = new Set<string>();
    	
 		List<RQO_cmt_userProfilePermission__mdt> permissions =  (List<RQO_cmt_userProfilePermission__mdt>) 
														            Database.query(newQueryFactory().
														            				setCondition('RQO_fld_userProfile__c = \'' + profile + '\'').
														            				toSOQL()); 
 		for (RQO_cmt_userProfilePermission__mdt permission : permissions)
        	distinctPermissions.add((String)permission.RQO_fld_url__c);

		return distinctPermissions;							                
    }
}