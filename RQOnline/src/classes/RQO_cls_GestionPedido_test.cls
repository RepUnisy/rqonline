/**
    *	@name: RQO_cls_GestionPedido_test
    *	@version: 1.0
    *	@creation date: 23/01/2018
    *	@author: Alvaro Alonso - Unisys
    *	@description: Clase de test para probar la funcionalidad del controlador APEX RQO_cls_GestionPedido_cc
    */
@IsTest
public class RQO_cls_GestionPedido_test {
    
        /**
    * @creation date: 23/01/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparte entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    
    @testSetup static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserGestionPedido@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createAsset(10);
            RQO_cls_TestDataFactory.createGrade(10);
            RQO_cls_TestDataFactory.createTranslation(200);
            RecordType rtActual = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ACCOUNT_QUIMICA limit 1];
            List<Account> listAccount = [Select Id from Account limit 1];
            listAccount[0].RecordtypeId = rtActual.Id;
            listAccount[0].Name = 'cuentaGestionPedido';
            listAccount[0].RQO_fld_idExterno__c = 'acExtId';
            listAccount[0].RQO_fld_pais__c = 'ES';
            listAccount[0].Phone = '914443322';
            update listAccount[0];
            
            List<Contact> listContact = [Select Id from Contact limit 1];
            listContact[0].FirstName = 'contactGestionPedido';
            listContact[0].LastName = 'contactLastName';
            update listContact[0];
        }
    }
    
        /**
    * @creation date: 23/01/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para testear la obtención de datos de histórico y seguimiento
    * @exception: 
    * @throws: 
    */
    
    static testMethod void testHistoricoSeguimiento(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            List<RQO_obj_requestManagement__c> requestManagement = [Select Id from RQO_obj_requestManagement__c limit 1];
            List<Asset> listaActivos = [Select Id from Asset limit 1];
            List<Id> listIdPosiciones = new List<Id>();
            for(Asset act : listaActivos){
                listIdPosiciones.add(act.Id);
            }
            test.startTest();
            //Petición con retorno ok del servicio
            Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_SAP_SERVICES, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
            RQO_cls_SendOrder sendOrderClass = new RQO_cls_SendOrder();
            RQO_cls_SendOrder.SendOrderCustomResponse response;
            response = sendOrderClass.sendPositions(requestManagement[0].Id, listIdPosiciones, RQO_cls_Constantes.REQ_WS_PROCESSTYPE_CREATE);
            System.assertEquals(response.errorCode, RQO_cls_Constantes.COD_EXEC_NOK);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getPedidoIdAsset()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetPedidoIdAsset(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            String language = 'ES_es';
            Id idSolicitud;
            List<Asset> listAss = new List<Asset>();
            List<RQO_obj_requestManagement__c> listRequestManagement = [SELECT id, RQO_fld_sentToQp0__c FROM RQO_obj_requestManagement__c LIMIT 2];
            listRequestManagement[0].RQO_fld_sentToQp0__c = false;
            update listRequestManagement[0];
            String esperado = JSON.serialize(listAss);
            List<Asset> listAsset = [SELECT id, RQO_fld_requestManagement__c FROM Asset LIMIT 200];
            listAsset[0].RQO_fld_requestManagement__c = listRequestManagement[0].Id;
            
            test.startTest();
            String resultado = RQO_cls_GestionPedido_cc.getPedidoIdAsset(idSolicitud, language);
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método setTipoPedido()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testSetTipoPedido(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createRequestManagement(200);
            String tipoPedido = 'PS';
            List<RQO_obj_requestManagement__c> requestManagement = [select id from RQO_obj_requestManagement__c limit 1];
            Id idSolicitud = requestManagement[0].Id;
            list<RQO_obj_requestManagement__c> rm = [select id, RQO_fld_type__c from RQO_obj_requestManagement__c where id = : idSolicitud limit 1];
            test.startTest();
            RQO_cls_GestionPedido_cc.setTipoPedido(idSolicitud, tipoPedido);
            System.assertEquals(rm[0].RQO_fld_type__c, tipoPedido);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getPedidos()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetPedido(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            String language = 'ES_es';
            List<Account> listAccount = [SELECT id FROM Account LIMIT 1];
            Id idAccoun = listAccount[0].Id;
            List<Asset> listAss = new List<Asset>();
            String esperado = JSON.serialize(listAss);
            test.startTest();
            String resultado = RQO_cls_GestionPedido_cc.getPedidos(idAccoun, language);
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getInfoRequest()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetInfoRequest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            String language = 'ES_es';
            List<Asset> listAss = new List<Asset>();
            String esperado = JSON.serialize(listAss);
            List<Account> listAccount = [SELECT id FROM Account LIMIT 1];
            Id idAccoun = listAccount[0].Id;
            
            test.startTest();
            String resultado = RQO_cls_GestionPedido_cc.getInfoRequest(idAccoun, language);
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getPedidosFacturacion()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetPedidosFacturacion(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            List<Account> listAccount = [SELECT id FROM Account LIMIT 1];
            Id idAccoun = listAccount[0].Id;
            List<RQO_obj_requestManagement__c> listRequestManagement = [SELECT id, RQO_fld_sentToQp0__c FROM RQO_obj_requestManagement__c LIMIT 2];
            listRequestManagement[0].RQO_fld_sentToQp0__c = false;
            update listRequestManagement[0];
            
            List<Asset> listAsset = [SELECT id, RQO_fld_requestManagement__c FROM Asset LIMIT 200];
            listAsset[0].RQO_fld_requestManagement__c = listRequestManagement[0].Id;
            
            for(integer i = 0; i<listAsset.size(); i++){
                listAsset[i].RQO_fld_direccionEnvio__c = listAccount[0].id;
                update listAsset[i];
            }
            test.startTest();
            String resultado = RQO_cls_GestionPedido_cc.getPedidosFacturacion(idAccoun);
            System.assertEquals(false, listRequestManagement[0].RQO_fld_sentToQp0__c);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getPedidosResumen()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetPedidosResumen(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            List<Account> listAccount = [SELECT id FROM Account LIMIT 1];
            Id idAccoun = listAccount[0].Id;
            Map<String, List<RQO_cls_WrapperPedido>> mapFactura = new Map<String, List<RQO_cls_WrapperPedido>>();
            String esperado = JSON.serialize(mapFactura);
            test.startTest();
            String resultado = RQO_cls_GestionPedido_cc.getPedidosResumen(idAccoun, 'ES_es');
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getPedidosResumenWithoutSerialize()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetPedidosResumenWithoutSerialize(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            List<Account> listAccount = [SELECT id FROM Account LIMIT 1];
            Id idAccoun = listAccount[0].Id;
            Map<String, List<RQO_cls_WrapperPedido>> mapFacturaEsp = new Map<String, List<RQO_cls_WrapperPedido>>();
            test.startTest();
            Map<String, List<RQO_cls_WrapperPedido>> mapFacturaRes = RQO_cls_GestionPedido_cc.getPedidosResumenWithoutSerialize(idAccoun, true, 'es_ES');
            System.assertEquals(mapFacturaEsp, mapFacturaRes);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método hasSolicitudPendiente()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testHasSolicitudPendiente(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            Integer esperado = 0;
            test.startTest();
            Integer resultado = RQO_cls_GestionPedido_cc.hasSolicitudPendiente();
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método assignDireccionFacturacionGlobal()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testAssignDireccionFacturacionGlobal(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            List<Account> listAccount = [SELECT id FROM Account LIMIT 1];
            Id idAccoun = listAccount[0].Id;
            Id idDirFacturacion;
            List<RQO_obj_requestManagement__c> listRequestManagement = [SELECT id, RQO_fld_sentToQp0__c FROM RQO_obj_requestManagement__c LIMIT 2];
            listRequestManagement[0].RQO_fld_sentToQp0__c = false;
            update listRequestManagement[0];
            
            List<Asset> listAsset = [SELECT id, RQO_fld_requestManagement__c FROM Asset LIMIT 200];
            listAsset[0].RQO_fld_requestManagement__c = listRequestManagement[0].Id;

            test.startTest();
            RQO_cls_GestionPedido_cc.assignDireccionFacturacionGlobal(idDirFacturacion);
            System.assertEquals(10, listAsset.size());
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método assignDireccionFacturacionParcial()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testAssignDireccionFacturacionParcial(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            List<RQO_cls_WrapperPedido> listWrapper = new List<RQO_cls_WrapperPedido> ();
            listWrapper.add(new RQO_cls_WrapperPedido());
            String listPedidoModificado = JSON.serialize(listWrapper);
            test.startTest();
            RQO_cls_GestionPedido_cc.assignDireccionFacturacionParcial(listPedidoModificado);
            System.assertEquals(1, listWrapper.size());
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getFacturacion()
    * @exception: 
    * @throws:  
    */  
    @isTest
    static void testGetFacturacion(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createRelaciondeVentas(200);
            Id idAccoun;
            List<Asset> listAss = new List<Asset>();
            String esperado = JSON.serialize(listAss);
            test.startTest();
            String resultado = RQO_cls_GestionPedido_cc.getFacturacion(idAccoun);
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getToCreateRequest()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetToCreateRequest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            Id idGrado;
            List<RQO_obj_grade__c> listGrade = [select id, RQO_fld_sku__c from RQO_obj_grade__c limit 1];
            idGrado = listGrade[0].Id;
            List<Asset> listAss = new List<Asset>();
            String esperado = JSON.serialize(listAss);
            test.startTest();
            String resultado = RQO_cls_GestionPedido_cc.getToCreateRequest(idGrado, 'es_ES');
            System.assertEquals(esperado, JSON.serialize(listAss));            
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método insertAsset()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testInsertAsset(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            Id idQP0Grade, envase, modoEnvio, destinoMercancias;
            Decimal cantidad;
            String fechaEnvio, incoterm;
            List<RQO_obj_grade__c> listGrade = [select id, RQO_fld_sku__c from RQO_obj_grade__c limit 1];
            List<Contact> listContact = [select id from Contact limit 1];
            Id ContactId = listContact[0].id;
            List<Account> listAccount = [select id from Account limit 1];
            Id AccountId = listAccount[0].id;
            Id idGrado = listGrade[0].Id;
            test.startTest();
            RQO_cls_GestionPedido_cc.insertAsset(idGrado, idQP0Grade, AccountId, ContactId, envase, modoEnvio, cantidad, 
                                                 fechaEnvio, incoterm, destinoMercancias);
            System.assertEquals(listGrade[0].Id, idGrado);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método CantidadDuplicar()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testCantidadDuplicar(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            Id idPosicion;
            integer cantidadDuplicar = 1;
            List<Asset> listaActivos = [Select Id from Asset limit 1];
            idPosicion = listaActivos[0].id;
            test.startTest();
            RQO_cls_GestionPedido_cc.cantidadDuplicar(idPosicion, cantidadDuplicar);
            System.assertEquals(listaActivos[0].id, idPosicion);
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getActiveAsset()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetContent2(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            List<RQO_obj_container__c> listContainer = [select id from RQO_obj_container__c];
            delete listContainer;
            List<RQO_obj_qp0Grade__c> listQp0Grade = [select id from RQO_obj_qp0Grade__c];
            delete listQp0Grade;
            List<RQO_obj_shippingJunction__c> esperado;
            RQO_cls_TestDataFactory.createContainerJunction(200);
            List<Id> listSKUGrade = new List<Id>();
            List<RQO_obj_containerJunction__c> listContainerJunction = [select id, RQO_fld_gradoQp0__c from RQO_obj_containerJunction__c];
            id idGradoQp0 = listContainerJunction[0].RQO_fld_gradoQp0__c;
            listSKUGrade.add(idGradoQp0);                                              
            test.startTest();
            List<RQO_obj_shippingJunction__c> resultado = RQO_cls_GestionPedido_cc.getContent2(listSKUGrade);
			System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método eliminarPosiciones()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testEliminarPosiciones(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){                                
			List<RQO_cls_WrapperPedido> listWrapper = new List<RQO_cls_WrapperPedido> ();
            listWrapper.add(new RQO_cls_WrapperPedido());
            String listEliminar = JSON.serialize(listWrapper);
            test.startTest();
            RQO_cls_GestionPedido_cc.eliminarPosiciones(listEliminar);
            System.assertEquals(1, listWrapper.size());
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getTipoPedido()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetTipoPedido(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){     
            Map<String, String> esperado = new Map<String, String>();
            esperado.put('KB', 'Consigna');
            esperado.put('PS', 'Estandar');
            test.startTest();
            Map<String, String> resultado = RQO_cls_GestionPedido_cc.getTipoPedido();
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método sendNotification()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testSendNotification(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        String requestManagementId = '';
        String requestManagementName = '';
        System.runAs(u){ 
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
            RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_gradoNuevo_es', 'RQO_eplt_resumenDiario_es'});
            String solicitud, idSolicitud;
            List<Contact> listContact = [select id from Contact limit 1];
            String contactId = listContact[0].id;
            for (RQO_obj_requestManagement__c item : [SELECT Name, Id FROM RQO_obj_requestManagement__c]) {
                requestManagementId = item.Id;
                requestManagementName = item.Name;
            }
            test.startTest();
			RQO_cls_GestionPedido_cc.sendNotification(contactId, requestManagementName, requestManagementId);
            System.assertEquals(1, listContact.size());
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 26/02/2018
    * @author: Juan E. Laiseca - Unisys
    * @description:	Método test para probar la funcionalidad correcta del método getNumPedidos()
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetNumPedidos(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){     
            String esperado;
            List<Contact> contactos = RQO_cls_TestDataFactory.createContact(200);
            Id clientId = contactos[0].Id;
            test.startTest();
			String resultado = RQO_cls_GestionPedido_cc.getNumPedidos(clientId);
            System.assertNotEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear el listado de wrapper para la entrega
    * @exception: 
    * @throws: 
    */    
    static testMethod void testCrearListadoWrapperEntrega(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            List<RQO_obj_qp0Grade__c> listQp0 = [select id from RQO_obj_qp0Grade__c];
            delete listQp0;
            List<RQO_obj_pedido__c> listPedido = [select id from RQO_obj_pedido__c];
            delete listPedido;
            List<Asset> listAsset2 = [select id from asset];
            delete listAsset2;
            
            RQO_cls_TestDataFactory.createPosiciondeEntrega(200);
            test.startTest();
            List<RQO_obj_posiciondeEntrega__c> listEntrega = [select id, RQO_fld_posicionPedido__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_grade__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_grade__r.RQO_fld_sku__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c,
                                                              RQO_fld_idRelacion__r.RQO_fld_fechaRealdeLlegada__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_incoterm__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_interlocutorWE__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__r.Name,
                                                              RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_container__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_container__r.RQO_fld_codigo__c,
                                                              RQO_fld_unidad__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c,
                                                              RQO_fld_idRelacion__r.RQO_fld_estadodelaEntrega__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_gradeDescriptionData__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_qp0Grade__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_idRelacion__c,
                                                              RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_idExterno__c,
                                                              RQO_fld_posicionPedido__r.ParentId,
                                                              RQO_fld_posicionPedido__r.Parent.RQO_fld_positionRequestDate__c,
                                                              RQO_fld_posicionPedido__r.Parent.RQO_fld_requestManagement__c,
                                                              RQO_fld_idRelacion__r.RQO_fld_idExterno__c,
                                                              CreatedDate
                                                              from RQO_obj_posiciondeEntrega__c];
            
            List<RQO_cls_WrapperPedido> listWrapper = RQO_cls_GestionPedido_cc.crearListadoWrapperEntrega(listEntrega, 'test', false);           
            
            System.assertEquals(200, listWrapper.size());
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear la actualización de posiciones
    * @exception: 
    * @throws: 
    */    
    static testMethod void testActualizarPosiciones(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            decimal cantidadActualizar = 99;
            List<RQO_obj_qp0Grade__c> listQp0 = [select id from RQO_obj_qp0Grade__c];
            delete listQp0;
            List<RQO_obj_pedido__c> listPedido = [select id from RQO_obj_pedido__c];
            delete listPedido;
            List<Asset> listAssetEliminar = [select id from asset];
            delete listAssetEliminar;
            
            RQO_cls_TestDataFactory.createPosiciondeEntrega(200);
            test.startTest();
            
            RQO_cls_WrapperPedido wrap = new RQO_cls_WrapperPedido();
            
            List<RQO_cls_WrapperPedido> listWrapper = new List<RQO_cls_WrapperPedido> ();
            List<Asset> listAsset = [select id FROM Asset limit 1];
            Asset ass = (listAsset.size()>0) ? listAsset[0]: null;
            if (ass != null){
                wrap.setId(ass.id);
                wrap.setCantidad(cantidadActualizar);
            }
            listWrapper.add(wrap);
            
            RQO_cls_GestionPedido_cc.actualizarPosiciones(JSON.serialize(listWrapper));
            List<Asset> listAssetPost = [select id, RQO_fld_cantidad__c FROM Asset where id = : ass.id];
            
            System.assertEquals(cantidadActualizar, listAssetPost[0].RQO_fld_cantidad__c);
            test.stopTest();
        }
    }
    
	/**
    * @creation date: 01/03/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de registros de la ventana de seguimiento
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetSeguimiento(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            Date fechaActual = Date.today();
            List<Asset> listaPosiciones = [Select Id,AccountId  from Asset LIMIT 10];
            test.startTest();
            	String result = RQO_cls_GestionPedido_cc.getSeguimiento(listaPosiciones[0].AccountId, 'es_ES', null, null, null, null, null, null, 
                                                                        null, null, null, null, false);
            test.stopTest();
            System.assertNotEquals(null, result);
            System.assertNotEquals('', result);
            List<RQO_cls_PedidoAuxiliarBean> listaPedido = (List<RQO_cls_PedidoAuxiliarBean>)JSON.deserialize(result, List<RQO_cls_PedidoAuxiliarBean>.class);
            System.assertEquals(10, listaPedido.size());
        }
    }
    
	/**
    * @creation date: 01/03/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de registros de la ventana de histórico
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testGetHisorialMostrar(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            Date fechaActual = Date.today();
            List<Asset> listaPosiciones = [Select Id,AccountId  from Asset LIMIT 10];
            test.startTest();
            	String result = RQO_cls_GestionPedido_cc.getHisorialMostrar(listaPosiciones[0].AccountId, 'es_ES', null, null, null, null, null, null, 
                                                                        	null, null, null, null, false);
            test.stopTest();
            System.assertNotEquals(null, result);
            System.assertNotEquals('', result);
            List<RQO_cls_PedidoAuxiliarBean> listaPedido = (List<RQO_cls_PedidoAuxiliarBean>)JSON.deserialize(result, List<RQO_cls_PedidoAuxiliarBean>.class);
            System.assertEquals(10, listaPedido.size());
        }
    }
}