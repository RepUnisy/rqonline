/**
*   @name: RQO_cls_Facturas_test
*   @version: 1.0
*   @creation date: 27/02/2018
*   @author: Alvaro Alonso - Unisys
*   @description: Clase de test para probar la clase RQO_cls_Facturas_cc
*/
@IsTest
public class RQO_cls_Facturas_test {
    
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserFacturas@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSDocumentumRepository();
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES,
                                                                                            RQO_cls_Constantes.WS_DOCUM_GET, 
                                                                                            RQO_cls_Constantes.WS_DOCUM_SAP_GET, 
                                                                                            RQO_cls_Constantes.WS_DOCUM_CREATE});
        }
    }
    
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método test para probar la ejecución correcta del método de obtención de facturas
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getFacturas(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFacturas@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createContainerJunction(1);
            Account cuenta = [Select ID from Account LIMIT 1];
            
            //RQO_cls_TestDataFactory.createQp0Grade(1);
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_INVOICES, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
                String response = RQO_cls_Facturas_cc.getFacturas(cuenta.Id, null, null, null, String.valueOf(Date.today()), String.valueOf(Date.today()), 
                                            'Descripcion test', null, null, null, null, null, cuenta.Id, false);
            test.stopTest();
            List <RQO_cls_FacturaBean> listaFacturas = (List <RQO_cls_FacturaBean>)JSON.deserialize(response, List <RQO_cls_FacturaBean>.class);
            System.assertEquals(1, listaFacturas.size());
        }
    }
        
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método test para probar la ejecución correcta del método getTipoFacturas
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getTipoFacturas(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFacturas@testorg.com');
        System.runAs(u){
            test.startTest();
                String response = RQO_cls_Facturas_cc.getTipoFacturas();
            test.stopTest();
            List<RQO_cls_Facturas_cc.seleccionador> listaSeleccionador 
                        = (List <RQO_cls_Facturas_cc.seleccionador>)JSON.deserialize(response, List <RQO_cls_Facturas_cc.seleccionador>.class);
            System.assertNotEquals(null, listaSeleccionador.size());
        }
    }
    
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método test para probar la ejecución correcta del método getDateParametrizedData
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getDateParametrizedData(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFacturas@testorg.com');
        System.runAs(u){
            test.startTest();
                Integer response = RQO_cls_Facturas_cc.getDateParametrizedData();
            test.stopTest();
            System.assertNotEquals(0, response);
        }
    }

    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método test para probar la ejecución correcta del método getFileFactura
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getFileFactura(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFacturas@testorg.com');
        System.runAs(u){
            test.startTest();
                Test.setMock(HttpCalloutMock.class, new RQO_cls_DocumetumRESTMockTest(RQO_cls_Constantes.WS_DOCUM_SAP_GET, RQO_cls_Constantes.MOCK_EXEC_OK));
                List<String> listaFacturas = RQO_cls_Facturas_cc.getFileFactura(null, null, 'TestFact', null);
            test.stopTest();
            System.assertEquals(String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK), listaFacturas[0]);
        }
    }
    
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método test para probar la ejecución sin número de factura del método getFileFactura
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getFileFacturaNoFactura(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFacturas@testorg.com');
        System.runAs(u){
            test.startTest();
                Test.setMock(HttpCalloutMock.class, new RQO_cls_DocumetumRESTMockTest(RQO_cls_Constantes.WS_DOCUM_SAP_GET, RQO_cls_Constantes.MOCK_EXEC_OK));
                List<String> listaFacturas = RQO_cls_Facturas_cc.getFileFactura(null, null, null, null);
            test.stopTest();
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_NOK, listaFacturas[0]);
        }
    }
    
}