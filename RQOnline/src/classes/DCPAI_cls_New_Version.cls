/*------------------------------------------------------------------------
 * 
 *  @name:              DCPAI_cls_New_Version
 *  @version:           1 
 *  @creation date:     30/06/2017
 *  @author:            Alfonso Constán López   -   Unisys
 *  @description:       Copia toda la estructura del plan de proyectos (Plan/Budget/Cost Item/UBC) al objeto versión
 * 
----------------------------------------------------------------------------*/
global without sharing class DCPAI_cls_New_Version {
    
    @InvocableMethod
    public static void newVersion (List<DCPAI_obj_Investment_Plan__c> plan) {
        
        system.debug('Class DCPAI_cls_New_Version: newVersion ' + plan);
        
        for (DCPAI_obj_Investment_Plan__c p : plan){
            //  Realizamos la version del plan de inversion
            Id planVersionado = versionarInvestmentPlan(p);
            
            //  versionamos los budget, cost item y ubcs relaccionados
            versionarBudgetCostItemUbc(planVersionado, p.Id, p.DCPAI_fld_Last_Type__c);
        }        
    }    
    
    /*
     *  @creation date:         30/06/2017
     *  @author:                Alfonso Constán López   -   Unisys
     *  @description:           Versiona el plan de inversión
     *  @param:                 plan: plan de inversión que se desea versionar
     *  @return:                El id de la versión del plan que se ha creado
    */
    public static Id versionarInvestmentPlan(DCPAI_obj_Investment_Plan__c plan){
        //  Obtenemos el recordtype perteneciente al investment plan
        Id recordtypeIP = Schema.SObjectType.ITPM_Version__c.getRecordTypeInfosByName().get(Label.DCPAI_Version_RecordType_IP).getRecordTypeId();
        
        ITPM_Version__c versionPlan = new ITPM_Version__c(recordTypeId = recordtypeIP, 
                                                          DCPAI_fld_Investment_Plan__c = plan.Id,
                                                          ITPM_VER_SEL_Year_validity__c = plan.DCPAI_Year__c,
                                                          Name = plan.DCPAI_fld_Last_Type__c + ' ' + plan.DCPAI_Year__c,
                                                          DCPAI_fld_Status__c = Label.DCPAI_Not_Approved
                                                         );
        insert versionPlan;
        return versionPlan.id;
    }
    
        /*
     *  @creation date:         30/06/2017
     *  @author:                Alfonso Constán López   -   Unisys
     *  @description:           Versiona todos los budget, cost item y ubcs relaccionadas con el plan
     *  @param1:                idPlan: id del plan en el objeto versión
     *  @param2:                planOrigen: id del plan en el objeto investment plan
     *  @param3:                typeUpa: Upa de la nueva versión generada
    */
    @future
    public static void versionarBudgetCostItemUbc(Id idPlan, Id planOrigen, String typeUpa){        
        
        List<ITPM_Version__c> listVersionBudget = new List<ITPM_Version__c>(); 
        //  Obtenemos todos los budget relaccionados con el plan
        List<ITPM_Budget_Investment__c> listBudgetInvest = [select id,
                                                            Name,
                                                            ITPM_BI_TX_Budget_Investment_name__c,
                                                            ITPM_BIA_REL_Responsible__c,
                                                            ITPM_BIA_TXA_Description__c,
                                                            ITPM_BI_SEL_Responsible_IT_Direction__c,
                                                            ITPM_BIA_SEL_IT_Area__c,
                                                            ITPM_BIA_SEL_IT_Subarea__c,
                                                            DCPAI_fld_Decision_Area__c,
                                                            DCPAI_fld_Segmentation__c
                                                            from ITPM_Budget_Investment__c where id in (select DCPAI_fld_Budget_Investment__c 
                                                                                                        from ITPM_Budget_Item__c where DCPAI_fld_Investment_Plan__c = : planOrigen and DCPAI_fld_Assing_Status__c = :Label.DCPAI_CostItem_Status_Assigned and DCPAI_fld_Waiting_to_Assign__c = false )];
        
        //  Obtenemos el recordtype perteneciente al budget
        Id recordtypeBudget = Schema.SObjectType.ITPM_Version__c.getRecordTypeInfosByName().get(Label.DCPAI_Version_RecordType_BudgetUser).getRecordTypeId();
        for(ITPM_Budget_Investment__c budgetUser : listBudgetInvest){
            //  Generamos la version del budget
            ITPM_Version__c versionBudget = new ITPM_Version__c(RecordTypeId = recordtypeBudget, 
                                                                Name = budgetUser.Name,
                                                                DCPAI_fld_Investment_Plan_Version__c = idPlan , 
                                                                DCPAI_fld_Budget_Investment__c = budgetUser.id,
                                                                DCPAI_fld_Name__c = budgetUser.ITPM_BI_TX_Budget_Investment_name__c,
                                                                ITPM_VER_SEL_Responsible__c = budgetUser.ITPM_BIA_REL_Responsible__c,
                                                                ITPM_VER_TX_Description__c = budgetUser.ITPM_BIA_TXA_Description__c,
                                                                ITPM_VER_SEL_Responsible_IT_Direction__c = budgetUser.ITPM_BI_SEL_Responsible_IT_Direction__c,
                                                                ITPM_VER_SEL_Responsible_Area__c = budgetUser.ITPM_BIA_SEL_IT_Area__c,
                                                                ITPM_VER_SEL_Responsible_Subarea__c = budgetUser.ITPM_BIA_SEL_IT_Subarea__c,
                                                                DCPAI_fld_Decision_Area__c = budgetUser.DCPAI_fld_Decision_Area__c,
                                                                DCPAI_fld_Segmentation__c = budgetUser.DCPAI_fld_Segmentation__c,
                                                                DCPAI_fld_Type__c = typeUpa
                                                               );
            listVersionBudget.add(versionBudget);
        }
        if (listVersionBudget.size() != 0)
            insert listVersionBudget;
        
        //  Creamos un mapa con la relacción de budget oficial con budget versión
        Map<Id, Id> mapBudget = new Map<Id, Id>();
        for(ITPM_Version__c budgetUser : listVersionBudget){
            mapBudget.put(budgetUser.DCPAI_fld_Budget_Investment__c, budgetUser.Id);
        }
        //  Obtenemos todas las ubcs para versionar
        List<ITPM_UBC_Project__c> listUbcs = [select id, DCPAI_fld_Budget_Investment__c, ITPM_SEL_Delivery_Country__c, 
                                              DCPAI_fld_Code_of_project__c, ITPM_SEL_Year__c, 
                                              ITPM_REL_UBC__r.Name,
                                              ITPM_REL_UBC__r.ITPM_TX_Description__c,
                                              ITPM_REL_UBC__r.ITPM_UBC_TX_Business_Area_Name__c,
                                              ITPM_REL_UBC__r.ITPM_UBC_TX_Business_Subarea_Name__c,
                                              ITPM_REL_UBC__r.ITPM_UBC_TX_Business_Unit_Name__c,
                                              ITPM_REL_UBC__r.ITPM_UBC_TX_Business_Country_Name__c,
                                              ITPM_Percent__c
                                              from ITPM_UBC_Project__c where DCPAI_fld_Budget_Investment__c in :mapBudget.keySet()];
        
        Map<String, List<ITPM_UBC_Project__c>> mapUBC =  DCPAI_cls_GlobalClass.DCPAI_getMapUBCs(listUbcs);
        
        //  Obtenemos todos los cost item para versionar
        List<ITPM_Budget_Item__c> listCostItem = [select id, Name,
                                                  ITPM_SEL_Budget_Type__c, DCPAI_fld_Budget_Investment__c, ITPM_SEL_Country__c, ITPM_SEL_Year__c, DCPAI_fld_Code__c,
                                                  ITPM_CUR_January_expense__c,ITPM_CUR_January_investment__c, ITPM_CUR_February_expense__c, ITPM_CUR_February_investment__c, ITPM_CUR_March_Expense__c,
                                                  ITPM_CUR_March_investment__c, ITPM_CUR_April_expense__c, ITPM_CUR_April_investment__c, ITPM_CUR_May_expense__c, ITPM_CUR_May_investment__c, ITPM_CUR_June_expense__c,
                                                  ITPM_CUR_June_investment__c, ITPM_CUR_July_expense__c, ITPM_CUR_July_investment__c, ITPM_CUR_August_expense__c, ITPM_CUR_August_investment__c, ITPM_CUR_September_expense__c,
                                                  ITPM_CUR_September_investment__c, ITPM_CUR_October_expense__c, ITPM_CUR_October_investment__c, ITPM_CUR_November_expense__c, ITPM_CUR_November_investment__c,
                                                  ITPM_CUR_December_expense__c, ITPM_CUR_December_investment__c, DCPAI_fld_Total__c
                                                  from ITPM_Budget_Item__c 
                                                  where DCPAI_fld_Budget_Investment__c in :mapBudget.keySet() and DCPAI_fld_Assing_Status__c = :Label.DCPAI_CostItem_Status_Assigned and DCPAI_fld_Waiting_to_Assign__c = false and DCPAI_fld_Investment_Plan__c =: planOrigen];
        
        Set<String> setPaisesInvolucrados = new Set<String>();
        Set<String> setAniosInvolucrados = new Set<String>();
        Set<Id> setBudgetInvestmentsInvolucrados = new Set<Id>();
        List<ITPM_Version__c> listPOVersionCostItems = new List<ITPM_Version__c>();
        //Este mapa se utilizará para la asignación del campo Planned Cost, por lo que tendrá una clave distinta a la normal
        Map<String, ITPM_Version__c> mapPOVersionCostItems = new Map<String, ITPM_Version__c>();

        //Obtenemos todos los Cost Items que estaban en el Version PO antes de generar esta nueva Version
        //Y generamos el mapa con todos los Cost Items ordenados
        for(ITPM_Budget_Item__c ci :listCostItem){
          setPaisesInvolucrados.add(ci.ITPM_SEL_Country__c);
          setAniosInvolucrados .add(ci.ITPM_SEL_Year__c);
          setBudgetInvestmentsInvolucrados.add(ci.DCPAI_fld_Budget_Investment__c);
        }

        //Obtenemos todos los Cost Items que estaban en el PO con las mismas condiciones que los Cost Items 
        //que vamos a generar en el nuevo Version y los organizamos en un mapa con el mismo estilo que el mapCostItem
        listPOVersionCostItems = [SELECT Id, DCPAI_fld_Budget_Investment_Cost__c, ITPM_VER_SEL_Country__c, ITPM_VER_SEL_Year_validity__c, DCPAI_fld_Code_of_project__c, DCPAI_fld_Total__c, DCPAI_fld_Cost_Type__c
                                  FROM ITPM_Version__c
                                  WHERE DCPAI_fld_Budget_Investment_Cost__c IN :setBudgetInvestmentsInvolucrados 
                                  AND ITPM_VER_SEL_Year_validity__c IN :setAniosInvolucrados AND ITPM_VER_SEL_Country__c IN :setPaisesInvolucrados 
                                  AND DCPAI_fld_Type__c = 'PO'];

        for(ITPM_Version__c verCi : listPOVersionCostItems){
            mapPOVersionCostItems.put(String.valueOf(verCi.DCPAI_fld_Budget_Investment_Cost__c) + '&' + String.valueOf(verCi.ITPM_VER_SEL_Country__c) + '&' + String.valueOf(verCi.ITPM_VER_SEL_Year_validity__c) + '&' + String.valueOf(verCi.DCPAI_fld_Code_of_project__c) + '&' + String.valueOf(verCi.DCPAI_fld_Cost_Type__c), verCi);
        }

        List<ITPM_Version__c> listBudgetVersion = new List<ITPM_Version__c>(); 
        
        //  Obtenemos el código de la PA
        DCPAI_IdConsecutivoPA__c codPA = [select id, DCPAI_Nombre__c, DCPAI_Consecutivo__c from DCPAI_IdConsecutivoPA__c where DCPAI_Nombre__c = : Label.DCPAI_PA2 limit 1];
        Integer numeracionPA = Integer.valueOf(codPA.DCPAI_Consecutivo__c);
        numeracionPA++;
        String numeracionFinalPA ;
        String numeracionPAyaExistente;
        List<ITPM_Version__c> listVersionCostItems = new List<ITPM_Version__c>();

        listVersionCostItems = [SELECT Id, DCPAI_fld_Budget_Investment_Cost__c, ITPM_VER_SEL_Country__c, ITPM_VER_SEL_Year_validity__c, DCPAI_fld_Code_of_project__c, DCPAI_fld_PEP__c
                                  FROM ITPM_Version__c
                                  WHERE DCPAI_fld_Budget_Investment_Cost__c IN :setBudgetInvestmentsInvolucrados 
                                  AND ITPM_VER_SEL_Year_validity__c IN :setAniosInvolucrados AND ITPM_VER_SEL_Country__c IN :setPaisesInvolucrados ];

        Map<String, ITPM_Version__c> mapVersionCostItems = new Map<String, ITPM_Version__c>();

        for(ITPM_Version__c ver : listVersionCostItems){
          mapVersionCostItems.put(String.valueOf(ver.DCPAI_fld_Budget_Investment_Cost__c) + '&' + String.valueOf(ver.ITPM_VER_SEL_Country__c) + '&' + String.valueOf(ver.ITPM_VER_SEL_Year_validity__c), ver);
        }

        Map<String, String> PAasign = new Map<String, String>();
        for(ITPM_Budget_Item__c ci : listCostItem){
            
            Boolean codigoNuevo = true;
            Boolean existiaCodigoPA = false;
            String combinatoria = ci.DCPAI_fld_Budget_Investment__c + ci.ITPM_SEL_Country__c;
            if (PAasign.containsKey(combinatoria)){
                numeracionFinalPA = PAasign.get(combinatoria);
                numeracionFinalPA = Label.DCPAI_PA2 + numeracionFinalPA;
                codigoNuevo = false;
            }
            else{
                if(mapVersionCostItems.containsKey(String.valueOf(ci.DCPAI_fld_Budget_Investment__c) + '&' + String.valueOf(ci.ITPM_SEL_Country__c) + '&' + String.valueOf(ci.ITPM_SEL_Year__c))){
                  existiaCodigoPA = true;
                  numeracionPAyaExistente = mapVersionCostItems.get(String.valueOf(ci.DCPAI_fld_Budget_Investment__c) + '&' + String.valueOf(ci.ITPM_SEL_Country__c) + '&' + String.valueOf(ci.ITPM_SEL_Year__c)).DCPAI_fld_PEP__c;
                  System.debug('numeracionPAyaExistente: ' + numeracionPAyaExistente);
                  PAasign.put(String.valueOf(ci.DCPAI_fld_Budget_Investment__c) + String.valueOf(ci.ITPM_SEL_Country__c), numeracionFinalPA);
                }
                else{
                  //  Numeración de la PA
                  String numeracion0 = codPA.DCPAI_Consecutivo__c.substring(0, (codPA.DCPAI_Consecutivo__c.length() - String.valueOf(numeracionPA).length()));
                  numeracionFinalPA = numeracion0 + numeracionPA;
                  numeracionFinalPA = Label.DCPAI_PA2 + numeracionFinalPA;
                  PAasign.put(combinatoria, numeracionFinalPA);
                }
            }
            
            //  Obtenemos el recordtype pertenecientes a los cost item
            Id recordtypeCostItem = Schema.SObjectType.ITPM_Version__c.getRecordTypeInfosByName().get(Label.DCPAI_Version_RecordType_CostItem).getRecordTypeId();
            ITPM_Version__c versionBudget = new ITPM_Version__c(RecordTypeId = recordtypeCostItem,  
                                                                Name = ci.Name,
                                                                DCPAI_fld_Cost_Item__c = ci.id, 
                                                                DCPAI_fld_Budget_Version_CostItem__c = mapBudget.get(ci.DCPAI_fld_Budget_Investment__c),
                                                                DCPAI_fld_Cost_Type__c = ci.ITPM_SEL_Budget_Type__c,
                                                                ITPM_VER_SEL_Country__c = ci.ITPM_SEL_Country__c,
                                                                ITPM_VER_SEL_Year_validity__c = ci.ITPM_SEL_Year__c,
                                                                CurrencyIsoCode = 'EUR',
                                                                DCPAI_fld_Budget_Investment_Cost__c = ci.DCPAI_fld_Budget_Investment__c,
                                                                ITPM_VER_CUR_January_expense__c = ci.ITPM_CUR_January_expense__c,
                                                                ITPM_VER_CUR_January_investment__c = ci.ITPM_CUR_January_investment__c,
                                                                ITPM_VER_CUR_February_expense__c = ci.ITPM_CUR_February_expense__c,
                                                                ITPM_VER_CUR_February_investment__c = ci.ITPM_CUR_February_investment__c,
                                                                ITPM_VER_CUR_March_Expense__c = ci.ITPM_CUR_March_Expense__c,
                                                                ITPM_VER_CUR_March_investment__c = ci.ITPM_CUR_March_investment__c,
                                                                ITPM_VER_CUR_April_expense__c = ci.ITPM_CUR_April_expense__c,
                                                                ITPM_VER_CUR_April_investment__c = ci.ITPM_CUR_April_investment__c,
                                                                ITPM_VER_CUR_May_expense__c = ci.ITPM_CUR_May_expense__c,
                                                                ITPM_VER_CUR_May_investment__c = ci.ITPM_CUR_May_investment__c,
                                                                ITPM_VER_CUR_June_expense__c = ci.ITPM_CUR_June_expense__c,
                                                                ITPM_VER_CUR_June_investment__c = ci.ITPM_CUR_June_investment__c,
                                                                ITPM_VER_CUR_July_expense__c = ci.ITPM_CUR_July_expense__c,
                                                                ITPM_VER_CUR_July_investment__c = ci.ITPM_CUR_July_investment__c,
                                                                ITPM_VER_CUR_August_expense__c = ci.ITPM_CUR_August_expense__c,
                                                                ITPM_VER_CUR_August_investment__c = ci.ITPM_CUR_August_investment__c,
                                                                ITPM_VER_CUR_September_expense__c = ci.ITPM_CUR_September_expense__c,
                                                                ITPM_VER_CUR_September_investment__c = ci.ITPM_CUR_September_investment__c,
                                                                ITPM_VER_CUR_October_expense__c = ci.ITPM_CUR_October_expense__c,
                                                                ITPM_VER_CUR_October_investment__c = ci.ITPM_CUR_October_investment__c,
                                                                ITPM_VER_CUR_November_expense__c = ci.ITPM_CUR_November_expense__c,
                                                                ITPM_VER_CUR_November_investment__c = ci.ITPM_CUR_November_investment__c,
                                                                ITPM_VER_CUR_December_expense__c = ci.ITPM_CUR_December_expense__c,
                                                                ITPM_VER_CUR_December_investment__c = ci.ITPM_CUR_December_investment__c,
                                                                DCPAI_fld_PEP__c = numeracionFinalPA,
                                                                DCPAI_fld_Type__c = typeUpa
                                                               );
            if(existiaCodigoPA){
              System.debug('existiaCodigoPA');
              versionBudget.DCPAI_fld_PEP__c = numeracionPAyaExistente;
              System.debug('versionBudget dentro de existiaCodigoPA: ' + versionBudget);
            }
            if(typeUpa != 'PO'){
              if(mapPOVersionCostItems.containsKey(String.valueOf(versionBudget.DCPAI_fld_Budget_Investment_Cost__c) + '&' + String.valueOf(versionBudget.ITPM_VER_SEL_Country__c) + '&' + String.valueOf(versionBudget.ITPM_VER_SEL_Year_validity__c) + '&' + String.valueOf(versionBudget.DCPAI_fld_Code_of_project__c) + '&' + String.valueOf(versionBudget.DCPAI_fld_Cost_Type__c))){
                versionBudget.DCPAI_fld_Planned_cost__c = mapPOVersionCostItems.get(String.valueOf(versionBudget.DCPAI_fld_Budget_Investment_Cost__c) + '&' + String.valueOf(versionBudget.ITPM_VER_SEL_Country__c) + '&' + String.valueOf(versionBudget.ITPM_VER_SEL_Year_validity__c) + '&' + String.valueOf(versionBudget.DCPAI_fld_Code_of_project__c) + '&' + String.valueOf(versionBudget.DCPAI_fld_Cost_Type__c)).DCPAI_fld_Total__c;
              }
              else{
                versionBudget.DCPAI_fld_Planned_cost__c = 0;
              }
            }
            else{
              versionBudget.DCPAI_fld_Planned_cost__c = ci.DCPAI_fld_Total__c;
            }
            
            listBudgetVersion.add(versionBudget);
            String code = String.valueOf(ci.DCPAI_fld_Code__c);
            if (code == 'null' || code == null){
                code = '';
            }
            //  Obtenemos las UBCs correspondientes
            String clave = ci.DCPAI_fld_Budget_Investment__c + ci.ITPM_SEL_Country__c + ci.ITPM_SEL_Year__c + code;
            List<ITPM_UBC_Project__c> listUBCObt = mapUBC.get(clave);
            if (codigoNuevo){
                if (listUBCObt != null){
                    for(ITPM_UBC_Project__c ubc : listUBCObt){
                        //  Obtenemos el recordtype pertenecientes a las UBCs
                        Id recordtypeUBC = Schema.SObjectType.ITPM_Version__c.getRecordTypeInfosByName().get(Label.DCPAI_Version_RecordType_UBC).getRecordTypeId();
                        
                        ITPM_Version__c versionBudgetUBC = new ITPM_Version__c(RecordTypeId = recordtypeUBC, 
                                                                               Name = ubc.ITPM_REL_UBC__r.Name,
                                                                               DCPAI_fld_Budget_Version_UBC__c = mapBudget.get(ci.DCPAI_fld_Budget_Investment__c), 
                                                                               DCPAI_fld_UBC__c = ubc.id,
                                                                               ITPM_VER_SEL_Year_validity__c = ubc.ITPM_SEL_Year__c,
                                                                               DCPAI_fld_Valido_hasta_el_ano__c = Integer.valueOf(ubc.ITPM_SEL_Year__c),
                                                                               ITPM_VER_SEL_Country__c = ubc.ITPM_SEL_Delivery_Country__c,
                                                                               DCPAI_fld_UBC_Name__c = ubc.ITPM_REL_UBC__r.Name,
                                                                               DCPAI_fld_UBC_Description__c = ubc.ITPM_REL_UBC__r.ITPM_TX_Description__c,
                                                                               DCPAI_fld_Business_Area_Name__c = ubc.ITPM_REL_UBC__r.ITPM_UBC_TX_Business_Area_Name__c,
                                                                               DCPAI_fld_Business_Subarea_Name__c = ubc.ITPM_REL_UBC__r.ITPM_UBC_TX_Business_Subarea_Name__c,
                                                                               DCPAI_fld_Business_Unit_Name__c = ubc.ITPM_REL_UBC__r.ITPM_UBC_TX_Business_Unit_Name__c,
                                                                               DCPAI_fld_Business_Country__c = ubc.ITPM_REL_UBC__r.ITPM_UBC_TX_Business_Country_Name__c,
                                                                               DCPAI_fld_Percent__c = String.valueOf(ubc.ITPM_Percent__c),
                                                                               DCPAI_fld_PEP__c = numeracionFinalPA,                                                                               
                                                                               DCPAI_fld_Type__c = typeUpa);
                        if(existiaCodigoPA){
                          System.debug('existiaCodigoPA');
                          versionBudgetUBC.DCPAI_fld_PEP__c = numeracionPAyaExistente;
                          System.debug('versionBudgetUBC dentro de existiaCodigoPA: ' + versionBudgetUBC);
                        }
                        System.debug('versionBudgetUBC: ' + versionBudgetUBC);
                        listBudgetVersion.add(versionBudgetUBC);
                        System.debug('listBudgetVersion: ' + listBudgetVersion);
                    }
                }
                if(!existiaCodigoPA){
                  numeracionPA ++;                
                }
            }
        }
        if (numeracionFinalPA != null){
            System.debug('numeracionFinalPA con substring: ' + numeracionFinalPA.substring(3, numeracionFinalPA.length()));
            System.debug('numeracionFinalPA sin substring: ' + numeracionFinalPA);
            codPA.DCPAI_Consecutivo__c = numeracionFinalPA.substring(4, numeracionFinalPA.length());
            update codPA;
        }
        
        if (listBudgetVersion.size() != 0)
            insert listBudgetVersion; 

        //Volvemos a desactivar el Waiting to Assign en el nuevo Project Plan los Cost Item que estaban en Waiting to Assign activado
        DCPAI_obj_Investment_Plan__c planDirigido = new DCPAI_obj_Investment_Plan__c(Id = planOrigen);
        List<DCPAI_obj_Investment_Plan__c> listPlan = new List<DCPAI_obj_Investment_Plan__c>();
        listPlan.add(planDirigido);
        DCPAI_cls_UnlockInvestmentPlan.DCPAI_UnlockInvestmentPlan(listPlan);
    }
}