/*------------------------------------------------------------------------
Author:         Borja Martín Ballesteros

Description:    class
History
<Date>          <Author>                      <Description>
21-Sept-2016     Borja Martín Ballesteros       Initial version
------------------------------------------------------------------------------------*/
public class ITPM_manageCostItems{
    /* 
        Método que controla que sólo usuarios administradores o con permission set GPS_Admin puedan realizar modificaciones sobre cost items asociados a project.
    */ 
    public static void controlModificationsInProject(List <ITPM_Budget_Item__c> listBIs, map <id, ITPM_Budget_Item__c> oldMap) {               
        boolean is_System_Admin = false;     
        List<Profile> listProfilesSystemAdministrator = [Select Id from Profile where name in ('System Administrator','Administrador del sistema') ];
        
        if(listProfilesSystemAdministrator != null && listProfilesSystemAdministrator.size()==1 && UserInfo.getProfileId() == listProfilesSystemAdministrator.get(0).Id ){            
           is_System_Admin = true;
        }               
        Id IDpermissionSetGPSAdmin = [Select Id from PermissionSet where name=:'PMO' and label='GPS_Admin'].Id;                   
        boolean is_GSP_ADmin = false;
        //recorro todos los permission set que tenga asignado el usuario, ya que pueden ser mas de uno, buscando el de GPS_Admin     
        for(PermissionSetAssignment permissionX : [select PermissionSetId from PermissionSetAssignment where AssigneeId =: UserInfo.getUserId()] ){            
            if(permissionX.PermissionSetId == IDpermissionSetGPSAdmin){
                is_GSP_ADmin = true;
            }
        }             
        //Solo usuarios con Profile "System Administrator" o PermissionSet "GPS_Admin" pueden editar/eliminar los cost item (o gps si son de tipo estimated)
        if(!is_System_Admin && !is_GSP_ADmin){
            for (ITPM_Budget_Item__c ci : listBIs) {
                if((ci.ITPM_REL2_Project__c!=null || ci.ITPM_REL_Budget_Investment__c!=null) && ci.ITPM_CI_SEL_Version__c != 'Estimated'){                    
                    if(ci.ITPM_CI_SEL_Version__c == 'Plan'){
                        ci.addError(' You can not modify the version of Plan Cost Items.');
                    }else if(ci.ITPM_CI_SEL_Version__c == 'Actual'){
                        ci.addError(' You can not modify the version of Actual Cost Items.');
                    }else if(ci.ITPM_REL2_Project__c==null || ci.ITPM_REL_Budget_Investment__c!=null){
                        ci.addError(' You can not modify or delete Cost Items associated with Projects or Budget Investment.');    
                    }else if(ci.ITPM_REL2_Project__c!=null || ci.ITPM_REL_Budget_Investment__c==null){
                        ci.addError(' You can not modify or delete Cost Items associated with Projects or Budget Investment.');    
                    }else if(ci.ITPM_REL2_Project__c!=null || ci.ITPM_REL_Budget_Investment__c!=null){
                        ci.addError(' You can not modify or delete Cost Items associated with Projects or Budget Investment.');
                    }
                    
                }
            }
        }else if(!is_System_Admin){
            for (ITPM_Budget_Item__c ci : listBIs) {
                if(ci.ITPM_CI_SEL_Version__c != 'Estimated' && oldMap.get(ci.id) != null && oldMap.get(ci.id).ITPM_CI_SEL_Version__c == 'Estimated'){
                    ci.addError(' You can not modify the version of Estimated Cost Items.');
                }
                /*if((ci.ITPM_CI_SEL_Version__c == 'Estimated' || oldMap.get(ci.id).ITPM_CI_SEL_Version__c == 'Estimated') && ci.ITPM_CI_REL_Conceptualization__c != oldMap.get(ci.id).ITPM_CI_REL_Conceptualization__c){
                    ci.addError(' You can not modify the field "Conceptualization" of Estimated Cost Items.');
                }*/
            }
        }
    }
    
    /* 
        Método que matchea campos que vienen en la carga con distintos valores a los esperados en Salesforce
    */ 
    public static void matchFields(List <ITPM_Budget_Item__c> listBIs) {
        for (ITPM_Budget_Item__c ci : listBIs) {
            // Traducimos los valores del fichero a los almacenados en SF.
            if(ci.ITPM_SEL_Budget_Category__c=='PP') ci.ITPM_SEL_Budget_Category__c='Personal propio';
            else if(ci.ITPM_SEL_Budget_Category__c=='PE') ci.ITPM_SEL_Budget_Category__c='Personal externo';
            if(ci.ITPM_CI_SEL_Version__c=='P') ci.ITPM_CI_SEL_Version__c='Plan';
            else if(ci.ITPM_CI_SEL_Version__c=='R') ci.ITPM_CI_SEL_Version__c='Actual';
        }
    }
    
    /* 
        Método que calcula los campos totales en Opportunity y Conceptualization. Para el caso de GPS_Admin calcula todos. 
    */
    
    public static void calculateTotals(List <ITPM_Budget_Item__c> listBIs, boolean ejecutarTodo, boolean isDelete) {
        map<id, list<ITPM_Budget_Item__c>> MapaOpor = new map<id, list<ITPM_Budget_Item__c>>();
        
        list<ITPM_Opportunity__c> listaOporActualizar = new list<ITPM_Opportunity__c>();
       
        
        system.debug('LA LISTA INICIAL ES ' + listBIs);

        List <CurrencyType> listaCurrencyFinal = [SELECT ConversionRate, IsoCode FROM CurrencyType]; 
        map<string, decimal> mapaCurrencys = new map<string, decimal>();
        
        for(CurrencyType divisa : listaCurrencyFinal){
            mapaCurrencys.put(divisa.IsoCode, divisa.ConversionRate);
        }

        set<id> setIdOpor = new set<id>();
       
        
        for(ITPM_Budget_Item__c aux : listBIs){
    
            if(aux.ITPM_CI_REL_Opportunity__c != null){
                setIdOpor.add(aux.ITPM_CI_REL_Opportunity__c);
            }
        }
        /*system.debug('GELI1 ' + listBIs);
        system.debug('GELI2  ' + setIdOpor);
        system.debug('GELI3  ' + setIdConcep);*/

        list<ITPM_Budget_Item__c> listaCostItems = [SELECT id, ConvertCurrency(ITPM_CUR_Estimated_Cost__c), ITPM_CI_REL_Opportunity__c, ITPM_SEL_Budget_Type__c FROM ITPM_Budget_Item__c WHERE recordType.Name = 'Annual' AND (id IN : listBIs OR ITPM_CI_REL_Opportunity__c IN :setIdOpor )];
        //system.debug('OYE1 ' + listaCostItems);
        set<ITPM_Budget_Item__c> setCostItems = new set<ITPM_Budget_Item__c>();
        setCostItems.addAll(listaCostItems);
        //system.debug('OYE2 ' + setCostItems);
        
        boolean is_System_Admin = false;     
        List<Profile> listProfilesSystemAdministrator = [Select Id from Profile where name in ('System Administrator','Administrador del sistema') ];
        
        if(listProfilesSystemAdministrator != null && listProfilesSystemAdministrator.size()==1 && UserInfo.getProfileId() == listProfilesSystemAdministrator.get(0).Id ){            
           is_System_Admin = true;
        }               
        Id IDpermissionSetGPSAdmin = [Select Id from PermissionSet where name=:'PMO' and label='GPS_Admin'].Id;                   
        boolean is_GPS_Admin = false;
        //recorro todos los permission set que tenga asignado el usuario, ya que pueden ser mas de uno, buscando el de GPS_Admin     
        for(PermissionSetAssignment permissionX : [SELECT PermissionSetId from PermissionSetAssignment where AssigneeId =: UserInfo.getUserId()] ){            
            if(permissionX.PermissionSetId == IDpermissionSetGPSAdmin){
                is_GPS_Admin = true;
            }
        }             
        //Solo con usuarios con Profile "System Administrator" o PermissionSet "GPS_Admin" se actualizan todos los totales
        if((is_System_Admin || is_GPS_Admin) && ejecutarTodo){
            system.debug('calculateTotals. Es un administrador o gps_admin, actualizamos todos los totales de cost items.');
          //  calculateTotalOpportunities(null);
          //  calculateTotalConceptualizations(null);
        }else{

            //CARGAR MAPAS
            /*system.debug('HOLA 1' + listaCostItems );
            system.debug('HOLA 1' + listBIs);*/
            
            if(listaCostItems == null || listaCostItems.size() == 0){
                borrarUltimo(listBIs);
            }else{            
                for(ITPM_Budget_Item__c aux : setCostItems){
                    List <ITPM_Budget_Item__c> listaAux;
                    
                    //OPORTUNIDADES
                    if(aux.ITPM_CI_REL_Opportunity__c != null ){
                        if(MapaOpor.get(aux.ITPM_CI_REL_Opportunity__c) == null){
                            listaAux = new list<ITPM_Budget_Item__c>();
                            listaAux.add(aux);
                            MapaOpor.put(aux.ITPM_CI_REL_Opportunity__c, listaAux);
                        }else{
                            listaAux = MapaOpor.get(aux.ITPM_CI_REL_Opportunity__c);
                            listaAux.add(aux);
                            MapaOpor.put(aux.ITPM_CI_REL_Opportunity__c, listaAux);
                        }
                    }
    
                    //CONCEPTUALIZACIONES (eliminadas)
                   
                    }

                    //MAPAS YA CARGADOS

                    if(MapaOpor != null ){
                        listaOporActualizar = calculateTotalOpportunities(MapaOpor, mapaCurrencys);
                        upsert listaOporActualizar;
                    }
                }
            }
              
    }

    public static list<ITPM_Opportunity__c> calculateTotalOpportunities(map<id, list<ITPM_Budget_Item__c>> MapaOpor, map<string, decimal> mapaCurrencys){
        list<ITPM_Opportunity__c> listaOporActualizar = new list<ITPM_Opportunity__c>();
        map<id, ITPM_Opportunity__c> mapaOporsQuery = new map<id, ITPM_Opportunity__c>([SELECT id, CurrencyIsoCode, ITPM_OP_CUR_Total_Expenses__c, ITPM_OP_CUR_Total_Investments__c FROM ITPM_Opportunity__c WHERE id IN :MapaOpor.keyset()]);
        
        for(id aux : MapaOpor.keyset()){
            list<ITPM_Budget_Item__c> listaCost = MapaOpor.get(aux);
            ITPM_Opportunity__c opor = new ITPM_Opportunity__c(id = aux, ITPM_OP_CUR_Total_Expenses__c = 0, ITPM_OP_CUR_Total_Investments__c = 0, CurrencyIsoCode = mapaOporsQuery.get(aux).CurrencyIsoCode );

            for(ITPM_Budget_Item__c costItem : listaCost){        
                if(costItem.ITPM_SEL_Budget_Type__c=='Expense'){
                    opor.ITPM_OP_CUR_Total_Expenses__c = opor.ITPM_OP_CUR_Total_Expenses__c + costItem.ITPM_CUR_Estimated_Cost__c;
                }else if(costItem.ITPM_SEL_Budget_Type__c=='Investment'){
                     opor.ITPM_OP_CUR_Total_Investments__c = opor.ITPM_OP_CUR_Total_Investments__c + costItem.ITPM_CUR_Estimated_Cost__c;
                }
            }

            if (UserInfo.getDefaultCurrency() != opor.CurrencyIsoCode){
                opor.ITPM_OP_CUR_Total_Expenses__c = GlobalUtils.ConvertCurrencyComp(mapaCurrencys, UserInfo.getDefaultCurrency(), opor.CurrencyIsoCode, opor.ITPM_OP_CUR_Total_Expenses__c);
                opor.ITPM_OP_CUR_Total_Investments__c = GlobalUtils.ConvertCurrencyComp(mapaCurrencys, UserInfo.getDefaultCurrency(), opor.CurrencyIsoCode, opor.ITPM_OP_CUR_Total_Investments__c);
            }
            
            listaOporActualizar.add(opor);
        }
        return listaOporActualizar;
    }

    
    public static void borrarUltimo(List <ITPM_Budget_Item__c> listBIs){
        set<ITPM_Opportunity__c> setOporActualizar = new set<ITPM_Opportunity__c>();
        
        for(ITPM_Budget_Item__c aux : listBIs){
            if(aux.ITPM_CI_REL_Opportunity__c != null ){
                ITPM_Opportunity__c opor = new ITPM_Opportunity__c(id = aux.ITPM_CI_REL_Opportunity__c);
                opor.ITPM_OP_CUR_Total_Expenses__c = 0;
                opor.ITPM_OP_CUR_Total_Investments__c = 0;
                setOporActualizar.add(opor);
            }
            
        }
        /*system.debug('HOLA 2 ' + setOporActualizar);
        system.debug('HOLA 3 ' + setConcepActualizar);*/

        list<ITPM_Opportunity__c> listaOporActualizar = new list<ITPM_Opportunity__c>();
        listaOporActualizar.addAll(setOporActualizar);
        
        
        upsert listaOporActualizar;
    }
}