global class UserToCollaboration
{
   private String groupId;
   private String userId;
   
   public void setGroupId(string groupId)
   {
       this.groupId = groupId;
   }
   public string getGroupId()
   {
       return groupId;
   }
   
   
   public void setUserId(string userId)
   {
       this.userId= userId;
   }
   public string getUserId()
   {
       return userId;
   }   
}