/*------------------------------------------------------------------------
 * 
 *   @name:         DCPAI_cls_Budget_AssignController_cc
 *   @version:       1 
 *   @creation date:   30/06/2017
 *   @author:       Alfonso Constán López  -  Unisys
 *   @description:     Custom controller of visualforce page "DCPAI_vf_Budget_Assign"
 * 
----------------------------------------------------------------------------*/
public with sharing class DCPAI_cls_Budget_AssignController_cc {
    
    public DCPAI_cls_Budget_AssignController_cc(ApexPages.StandardController controller) {
        system.debug('Comienza la clase: DCPAI_cls_Budget_AssignController_cc');
        initial();
    }  
    
    public List<SObject> groupCostItem = new List<SObject>();
    
    private void initial(){
        mapWaitSelect = new Map<Boolean, Boolean> ();
        mapWaitSelect.put(true, true);
        mapWaitSelect.put(false, false);
        //  Obtenemos la agrupación de cost item
        groupCostItem= [select ITPM_SEL_Year__c,
                        ITPM_SEL_Country__c,
                        DCPAI_fld_Waiting_to_Assign__c,
                        DCPAI_fld_Investment_Nature__c,
                        DCPAI_fld_Code__c,
                        DCPAI_fld_Assing_Status__c,
                        DCPAI_fld_Budget_Investment__c,
                        DCPAI_fld_Investment_Plan__r.DCPAI_fld_Status__c planStatus,
                        DCPAI_fld_Investment_Plan__r.DCPAI_fld_Type__c planType,                    
                        SUM(ITPM_FOR_Total_expense__c) totalExpense,
                        SUM(ITPM_FOR_Total_investment__c) totalInvestment
                        from ITPM_Budget_Item__c
                        where DCPAI_fld_Budget_Investment__c = : ApexPages.currentPage().getParameters().get('id') and ITPM_SEL_Year__c != null 
                        GROUP BY  ITPM_SEL_Year__c, ITPM_SEL_Country__c, DCPAI_fld_Investment_Nature__c, DCPAI_fld_Code__c, DCPAI_fld_Budget_Investment__c,  DCPAI_fld_Waiting_to_Assign__c, DCPAI_fld_Assing_Status__c ,  DCPAI_fld_Investment_Plan__r.DCPAI_fld_Status__c, DCPAI_fld_Investment_Plan__r.DCPAI_fld_Type__c 
                        ORDER BY ITPM_SEL_Year__c desc, ITPM_SEL_Country__c ] ;
        
        system.debug('groupCostItem ' + groupCostItem);
        
        
        
        //  Obtenemos las UBCs del budget
        List<ITPM_UBC_Project__c> listUbcs = [select id, 
                                              ITPM_SEL_Delivery_Country__c, 
                                              ITPM_SEL_Year__c, 
                                              DCPAI_fld_Budget_Investment__c, 
                                              ITPM_Percent__c,
                                              DCPAI_fld_Code_of_project__c
                                              from ITPM_UBC_Project__c 
                                              where DCPAI_fld_Budget_Investment__c = : ApexPages.currentPage().getParameters().get('id') ];
        
        Map<String, List<ITPM_UBC_Project__c>> mapUBC = DCPAI_cls_GlobalClass.DCPAI_getMapUBCs(listUbcs);
        
        // Inicializamos el mapa para poder leer los estados
        mapaEstados = new Map<String, String>();
        mapaAsignacion = new Map<String, Boolean>();
        mapaWaitingAssignment = new Map<String, Boolean>();
        for (SObject obj : groupCostItem){
            String code = String.valueOf(obj.get('DCPAI_fld_Code__c'));
            if (code == 'null' || code == null){
                code = '';
            }
            mapaEstados.put(String.valueOf(obj.get('ITPM_SEL_Year__c')) + '&' + String.valueOf(obj.get('ITPM_SEL_Country__c') ) + '&' + String.valueOf(obj.get('DCPAI_fld_Assing_Status__c')) + '&' + String.valueOf(obj.get('DCPAI_fld_Budget_Investment__c') ) + '&' + code, String.valueOf(obj.get('DCPAI_fld_Assing_Status__c')));            
            
            //  Buscamos las agrupaciones en el mapa, para obtener un mapa con la suma de % 
            String key = String.valueOf(obj.get('DCPAI_fld_Budget_Investment__c')) + String.valueOf(obj.get('ITPM_SEL_Country__c')) + String.valueOf(obj.get('ITPM_SEL_Year__c')) + code;
            system.debug('key ' + key);
            List<ITPM_UBC_Project__c> ubcs = mapUBC.get(key);
            if (ubcs == null){
                mapaUBCDecimal.put(key, 0);
            }
            else{
                Decimal suma = 0;
                system.debug('ubcs ' + ubcs);
                for(ITPM_UBC_Project__c ubc :ubcs){
                    suma += ubc.ITPM_Percent__c;
                    system.debug('suma ' + suma);
                }
                mapaUBCDecimal.put(key, suma);
            }
            mapaAsignacion.put(String.valueOf(obj.get('ITPM_SEL_Year__c')) + '&' + String.valueOf(obj.get('ITPM_SEL_Country__c') ) + '&' + String.valueOf(obj.get('DCPAI_fld_Assing_Status__c')) + '&' + String.valueOf(obj.get('DCPAI_fld_Budget_Investment__c') ) + '&' + code, blockAssiged(obj, key));
            mapaWaitingAssignment.put(String.valueOf(obj.get('ITPM_SEL_Year__c')) + '&' + String.valueOf(obj.get('ITPM_SEL_Country__c') ) + '&' + String.valueOf(obj.get('DCPAI_fld_Assing_Status__c')) + '&' + String.valueOf(obj.get('DCPAI_fld_Budget_Investment__c') ) + '&' + code, Boolean.valueOf(obj.get('DCPAI_fld_Waiting_to_Assign__c')));
        }
        System.debug('mapaAsignacion: ' + mapaAsignacion);
        System.debug('mapaWaitingAssignment: ' + mapaWaitingAssignment);
    }
    
    public String error{
        get;
        set;
    }
    public String ok{
        get;
        set;
    }
    public Map<String, Boolean> mapaWaitingAssignment{
        get;
        set;
    }
    public Map<String, String> mapaEstados{
        get;
        set;
    }
    public Map<String, Boolean> mapaAsignacion{
        get;
        set;
    }    
    public Boolean isAdmin {
        get{
            return DCPAI_cls_GlobalClass.isAdmin();
        }
    }
    public Map<String, Decimal> mapUBCFinal{
        get{
            return mapaUBCDecimal;
        }
        set;
    }
    
    private Map<String, Decimal> mapaUBCDecimal = new Map<String, Decimal>();
    
    
    public List<SObject> groupCostItemFinal {
        get {
            return groupCostItem;
        }
        set;
    }
    public Map<Boolean, Boolean> mapWaitSelect{
        get;
        set;
    }
    
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(Label.DCPAI_Status_InProgress, Label.DCPAI_Status_InProgress));
        options.add(new SelectOption(Label.DCPAI_CostItem_Status_Assigned, Label.DCPAI_CostItem_Status_Assigned));
        return options;
    }
    
    public String asignacionEditada {get;set;}
    public String asignacionEditadaCheckbox {get;set;}
    private Map<Id, ITPM_Budget_Item__c > costItemModificar = new Map<Id, ITPM_Budget_Item__c>() ;
    
    /*
     *   @creation date:     30/06/2017
     *   @author:         Alfonso Constán López  -  Unisys
     *  @description:      Añadimos a una lista que valores han sido modificado y cual es su resultado final
   */
    public void editStatus () {
        borrarMensajes();
        system.debug('mapaEstados ' + mapaEstados);
        system.debug('asignacionEditada ' + asignacionEditada + ' su estado: ' + mapaEstados.get(asignacionEditada));
        String[] parts = asignacionEditada.split('&');
        String code;
        try{
            code = parts[4];
        }
        catch(Exception e){
            code = '';
        }
         List<ITPM_Budget_Item__c> costItemMod = [Select id, ITPM_SEL_Country__c, DCPAI_fld_Assing_Status__c , DCPAI_fld_Investment_Plan__r.DCPAI_fld_Status__c 
                                                 from ITPM_Budget_Item__c 
                                                 where ITPM_SEL_Year__c = :parts[0] and ITPM_SEL_Country__c = :parts[1] and DCPAI_fld_Assing_Status__c = : parts[2] and DCPAI_fld_Budget_Investment__c = : parts[3] and DCPAI_fld_Code__c =:code ];
        
        for (ITPM_Budget_Item__c ci: costItemMod){
            if (costItemModificar.containsKey(ci.id)){
                ITPM_Budget_Item__c costItemObtenido = costItemModificar.remove(ci.id);
                system.debug('El cost item ya habia sido modificado: ' + costItemObtenido);

                if (costItemObtenido.DCPAI_fld_Investment_Plan__r.DCPAI_fld_Status__c == Label.DCPAI_Status_Blocked){
                    if (mapaEstados.get(asignacionEditada) == Label.DCPAI_CostItem_Status_Assigned){
                        costItemObtenido.DCPAI_fld_Waiting_to_Assign__c = true; 
                    }
                    else{
                        costItemObtenido.DCPAI_fld_Waiting_to_Assign__c = false; 
                    }                    
                }

                costItemObtenido.DCPAI_fld_Assing_Status__c = mapaEstados.get(asignacionEditada);
                costItemModificar.put(costItemObtenido.id, costItemObtenido);
            }
            else{
                system.debug('el cost item no ha sido modificado con anterioridad');

                if (ci.DCPAI_fld_Investment_Plan__r.DCPAI_fld_Status__c == Label.DCPAI_Status_Blocked){
                    if (mapaEstados.get(asignacionEditada) == Label.DCPAI_CostItem_Status_Assigned){
                        ci.DCPAI_fld_Waiting_to_Assign__c = true; 
                    }
                    else{
                        ci.DCPAI_fld_Waiting_to_Assign__c = false; 
                    }
                }
                system.debug('asignacionEditada ' + asignacionEditada);
                system.debug('ci antes' + ci.DCPAI_fld_Assing_Status__c);
                ci.DCPAI_fld_Assing_Status__c = mapaEstados.get(asignacionEditada);
                system.debug('ci ' + ci.DCPAI_fld_Assing_Status__c);
                costItemModificar.put(ci.id, ci);
            }
        }
        
    }

    /*
     *   @creation date:     30/06/2017
     *   @author:         Alfonso Constán López  -  Unisys
     *  @description:      Añadimos a una lista que valores han sido modificado y cual es su resultado final
   */
    public void editWaitingForAssignment () {
        borrarMensajes();
        system.debug('mapaWaitingAssignment ' + mapaWaitingAssignment);
        system.debug('asignacionEditadaCheckbox ' + asignacionEditadaCheckbox + ' su estado: ' + mapaWaitingAssignment.get(asignacionEditadaCheckbox));
        String[] parts = asignacionEditadaCheckbox.split('&');
        String code;
        try{
            code = parts[4];
        }
        catch(Exception e){
            code = '';
        }

        
        List<ITPM_Budget_Item__c> costItemMod = [Select id, DCPAI_fld_Waiting_to_Assign__c 
                                                 from ITPM_Budget_Item__c 
                                                 where ITPM_SEL_Year__c = :parts[0] and ITPM_SEL_Country__c = :parts[1] and DCPAI_fld_Assing_Status__c = : parts[2] and DCPAI_fld_Budget_Investment__c = : parts[3] and DCPAI_fld_Code__c =:code ];
        
        for (ITPM_Budget_Item__c ci: costItemMod){
            if (!costItemModificar.containsKey(ci.id)){
                system.debug('el cost item no ha sido modificado con anterioridad');
                System.debug('El resultado del if es: ' + mapaWaitingAssignment.get(asignacionEditadaCheckbox));
                if(mapaWaitingAssignment.get(asignacionEditadaCheckbox) == true){
                    System.debug('Modificamos el cost Item.');
                    ci.DCPAI_fld_Waiting_to_Assign__c = false;
                    costItemModificar.put(ci.id, ci);
                }
                else{
                    if(mapaWaitingAssignment.get(asignacionEditadaCheckbox) == false){
                        System.debug('Modificamos el cost Item.');
                        ci.DCPAI_fld_Waiting_to_Assign__c = true;
                        costItemModificar.put(ci.id, ci);
                    }
                }
            }
            else{
                system.debug('el cost item ha sido modificado con anterioridad');
                System.debug('El resultado del if es: ' + mapaWaitingAssignment.get(asignacionEditadaCheckbox));
                if(mapaWaitingAssignment.get(asignacionEditadaCheckbox) == false){
                    ci.DCPAI_fld_Waiting_to_Assign__c = true;
                    costItemModificar.put(ci.id, ci);
                }
                else{
                    if(mapaWaitingAssignment.get(asignacionEditadaCheckbox) == true){
                        ci.DCPAI_fld_Waiting_to_Assign__c = false;
                        costItemModificar.put(ci.id, ci);
                    }
                }
            }
        }
        
    }
    /*
     *   @creation date:     30/06/2017
     *   @author:         Alfonso Constán López  -  Unisys
     *  @description:      Almacenamos la información
   */
    public void saveStatus () {
        borrarMensajes();
        System.debug('costItemModificar.size(): ' + costItemModificar.size());
        if (costItemModificar.size()>0){
            // Actualizamos los cost item 
            system.debug('Vamos a actualizar: ' + costItemModificar.values());
            update costItemModificar.values();
            costItemModificar.clear();
            ok = Label.DCPAI_Saved_successfully;
        }
    }
    
    private void borrarMensajes(){
        error = '';
        ok = '';
    }
    
    /*
     *   @creation date:     30/06/2017
     *   @author:         Alfonso Constán López  -  Unisys
     *  @description:      Comprobamos si se encuentra asignado
   */
    private Boolean blockAssiged(SObject obj, String key){
        if (String.valueOf(obj.get('DCPAI_fld_Assing_Status__c')) == Label.DCPAI_Status_InProgress){
            if (mapaUBCDecimal.get(key) == 100){
                return false;
            }
            else{
                return true;
            }
        }
        else{
            //  Comprobamos si no se encuentra bloqueado
            if (String.valueOf(obj.get('planStatus')) == Label.DCPAI_Plan_No_Bloqued || String.valueOf(obj.get('planStatus')) == null){
                return false;
            }
            else{
                if (obj.get('DCPAI_fld_Waiting_to_Assign__c') == true){
                    return false;
                }
                else{
                    return true;
                }
                
            }
        }
        
    }
}