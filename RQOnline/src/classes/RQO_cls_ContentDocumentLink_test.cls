/**
*	@name: RQO_trg_ContentDocumentLink_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar la clase RQO_trg_ContentDocumentLink
*/
@IsTest
public class RQO_cls_ContentDocumentLink_test {

    /**
    * @creation date: 28/02/2018
    * @author: David Iglesias - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserEvents@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSActivacionTrigger(true);
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: David Iglesias - Unisys
    * @description:	Método de prueba de inserción del objeto
    * @exception: 
    * @throws: 
    */
    private static testMethod void testCreate() {
         User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
            System.runAs(usuario){
            ContentVersion contentVersion_1 = new ContentVersion(
              Title = 'Quimica',
              PathOnClient = 'Quimica.jpg',
              VersionData = Blob.valueOf('Test Content'),
              IsMajorVersion = true
            );
            insert contentVersion_1;
            
            ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            System.assertEquals(documents.size(), 1);
            System.assertEquals(documents[0].Id, contentVersion_2.ContentDocumentId);
            System.assertEquals(documents[0].LatestPublishedVersionId, contentVersion_2.Id);
            System.assertEquals(documents[0].Title, contentVersion_2.Title);
          }
     }
}