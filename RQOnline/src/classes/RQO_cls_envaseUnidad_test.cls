/**
*   @name: RQO_cls_envaseUnidad_test
*   @version: 1.0
*   @creation date: 28/02/2018
*   @author: Julio Maroto - Unisys
*   @description: Clase de test para probar la clase RQO_cls_envaseUnidad_test
*/
@isTest
public class RQO_cls_envaseUnidad_test {
    /**
    *   @name: envaseUnidadMedida
    *   @version: 1.0
    *   @creation date: 28/02/2018
    *   @author: Julio Maroto - Unisys
    *   @description: Clase embebida usada como wrapper de las unidades de medida
    */
    
    public class envaseUnidadMedida {
        string envase = '';
        string unidadMedida = '';
        decimal pesoMaximo ;
        decimal pesoMinimo ;
        decimal multiploKgUnidad;
        boolean excluirValidacion = false;
        
        public envaseUnidadMedida(string envase, string unidadMedida, decimal pesoMaximo, decimal pesoMinimo,
                                  decimal kgUnidad, boolean excluirValidacion) {
            this.envase = envase;
            this.unidadMedida = unidadMedida;
            this.pesoMaximo = pesoMaximo;
            this.pesoMinimo = pesoMinimo;
            this.excluirValidacion = excluirValidacion;
            this.multiploKgUnidad = kgUnidad;
        }
    }
    
    private static List<RQO_obj_container__c> containerList;
    private static List<RQO_obj_shippingMode__c> shippingModeList;
    
    /**
    *   @name: initialSetup
    *   @version: 1.0
    *   @creation date: 28/02/2018
    *   @author: Julio Maroto - Unisys
    *   @description: Clase para crear el juego de datos necesario para realizar las pruebas
    */
    
    @testSetup
    public static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            // Create shipping Junction
            createShippingJunctions();
            
            // Create translation
            createTranslations();
        }
    }
    
    /**
    *   @name: createShippingJunctions
    *   @version: 1.0
    *   @creation date: 28/02/2018
    *   @author: Julio Maroto - Unisys
    *   @description: Método para shipping juntions
    */
    
    private static void createShippingJunctions() {
        RQO_cls_TestDataFactory.createShippingJunction(200);
    }
    
    /**
    *   @name: createTranslations
    *   @version: 1.0
    *   @creation date: 28/02/2018
    *   @author: Julio Maroto - Unisys
    *   @description: Método para crear traducciones
    */

    private static void createTranslations() {
        RQO_cls_TestDataFactory.createTranslation(200);
    }
    
    /**
    *   @name: testGetEnvaseUnidadMedidaIsNotEmpty
    *   @version: 1.0
    *   @creation date: 28/02/2018
    *   @author: Julio Maroto - Unisys
    *   @description: Método testeaer el método getEnvaseUnidadMedidaIsNotEmpty de la clase RQO_cls_envaseUnidad__c
    */
    
    @isTest
    public static void testGetEnvaseUnidadMedidaIsNotEmpty() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            String getEnvaseUnidadMedidaResult = RQO_cls_envaseUnidad_cc.getEnvaseUnidadMedida('');
   
            System.assert(false == getEnvaseUnidadMedidaResult.equals(''));
            Test.stopTest();
        }
    }
    
    /**
    *   @name: testGetAllShippingJuntion
    *   @version: 1.0
    *   @creation date: 28/02/2018
    *   @author: Julio Maroto - Unisys
    *   @description: Método testeaer el método getAllShippingJuntion de la clase RQO_cls_envaseUnidad__c
    */
    
    @isTest
    public static void testGetAllShippingJuntion() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            List<RQO_obj_shippingJunction__c> listShipping = getShippingJunctionData();
        
            Map<String, envaseUnidadMedida> shippingModeEnvUnidadMedidaExpected =
                getShippingModeEnvaseUnidadMedida(listShipping); 
            
            Map<string, envaseUnidadMedida> shippingModeEnvUnidadMedidaActual =
                (Map<string, envaseUnidadMedida>) JSON.deserialize(RQO_cls_envaseUnidad_cc.getAllShippingJuntion(),
                                                                   Map<string, envaseUnidadMedida>.class);
              
            System.assertEquals(shippingModeEnvUnidadMedidaExpected.size(), shippingModeEnvUnidadMedidaActual.size()); 
            
            System.assertEquals(shippingModeEnvUnidadMedidaExpected.get('envase'),
                                shippingModeEnvUnidadMedidaActual.get('envase') );
            
            System.assertEquals(shippingModeEnvUnidadMedidaExpected.get('excluirValidacion'),
                               shippingModeEnvUnidadMedidaActual.get('excluirValidacion'));
            
            System.assertEquals(shippingModeEnvUnidadMedidaExpected.get('multiploKgUnidad'),
                               shippingModeEnvUnidadMedidaActual.get('multiploKgUnidad'));
            
            System.assertEquals(shippingModeEnvUnidadMedidaExpected.get('pesoMaximo'),
                               shippingModeEnvUnidadMedidaActual.get('pesoMaximo'));
            
            System.assertEquals(shippingModeEnvUnidadMedidaExpected.get('pesoMinimo'),
                               shippingModeEnvUnidadMedidaActual.get('pesoMinimo'));
            
            System.assertEquals(shippingModeEnvUnidadMedidaExpected.get('unidadMedida'),
                               shippingModeEnvUnidadMedidaActual.get('unidadMedida'));

            Test.stopTest();
        }
    }
    
    /**
    *   @name: getShippingJunctionData
    *   @version: 1.0
    *   @creation date: 28/02/2018
    *   @author: Julio Maroto - Unisys
    *   @description: Método auxiliar para obtener los datos de las shipping junction insertadas
    */
    
    private static List<RQO_obj_shippingJunction__c> getShippingJunctionData() {
        return [
                SELECT
                    id,
                    RQO_fld_containerCode__c,
                    RQO_fld_shippingCode__c,
                    RQO_fld_container__c,
                    RQO_fld_shippingMode__c,
                    RQO_fld_maximumWeightKg__c,
                    RQO_fld_minimumWeightKg__c,
                    RQO_fld_multiploKgUnidad__c,
                    RQO_fld_excluirValidacion__c
                FROM
                    RQO_obj_shippingJunction__c
        ];
    }
	
    /**
    *   @name: getShippingJunctionData
    *   @version: 1.0
    *   @creation date: 28/02/2018
    *   @author: Julio Maroto - Unisys
    *   @description: Método para testear que se devuelve el ShippingMode del envaseUnidadMedida
    */
    private static Map<string, envaseUnidadMedida> getShippingModeEnvaseUnidadMedida(
        List<RQO_obj_shippingJunction__c> listShipping)
    {
        Map<string, envaseUnidadMedida> mapEnvaseUndMedida = new Map<string, envaseUnidadMedida>();
        
        for (RQO_obj_shippingJunction__c cont : listShipping) {
            String containerShippingMode = String.valueOf(cont.RQO_fld_container__c) +
                                           String.valueOf(cont.RQO_fld_shippingMode__c);
                                            
            //  Insertamos la combinatoria del shipping_juntion
            mapEnvaseUndMedida.put(containerShippingMode, new envaseUnidadMedida(cont.RQO_fld_containerCode__c,
                                                          cont.RQO_fld_shippingCode__c,
                                                          cont.RQO_fld_maximumWeightKg__c,
                                                          cont.RQO_fld_minimumWeightKg__c,
                                                          cont.RQO_fld_multiploKgUnidad__c,
                                                          cont.RQO_fld_excluirValidacion__c) );
        }
        
        return mapEnvaseUndMedida;
    }
    
}