/**
 *	@name: RQO_cls_FormatUtils
 *	@version: 1.0
 *	@creation date: 22/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase de utilidades para formatear datos
 */
public class RQO_cls_FormatUtils {
    
    // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_FormatUtils';    
    
	/**
	* @creation date: 18/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Transforma una fecha en formato YYYYMMDD en fecha (devuelve null si la fecha es incorrecta) 
	* @param: strFecha		Fecha en formato YYYYMMDD
	* @return: Date			Fecha en formato SF	
	* @exception: 
	* @throws: 
	*/    
	public static Date obtenerFechaYYYYMMDD(String strFecha) {
        
        /**
        * Constantes
        */
        final String METHOD = 'obtenerFechaYYYYMMDD';
        
		/**
		 * Variables
		 */
		Date fecha;
		String strAno;
		String strMes;
		String strDia;

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		try {
			// Obtener Ano, mes y dia
			strAno = strFecha.substring(0, 4);
			strMes = strFecha.substring(4, 6);
			strDia = strFecha.substring(6);

			// Transformar en fecha
			fecha = Date.newInstance(Integer.valueOf(strAno), Integer.valueOf(strMes), Integer.valueOf(strDia));
		} catch(Exception e) {
            System.debug('[RQO_cls_FormatUtils] obtenerFechaYYYYMMDD - Error : ' + e.getMessage());
			fecha = null;
		}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');

		return fecha;
	}
    
    /**
	* @creation date: 18/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Transforma un String en Decimal 
	* @param: num			Texto a formatear
	* @return: Decimal		Date formateado del parametro
	* @exception: 
	* @throws: 
	*/
    public static Decimal stringToDecimal(String num) {
		
        /**
        * Constantes
        */
        final String METHOD = 'stringToDecimal';
        
		/**
		 * Variables
		 */
		Decimal retorno;

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        try {
            retorno = Decimal.valueOf(num);
		} catch(Exception e) {
			System.debug('[RQO_cls_FormatUtils] stringToDecimal - Error : ' + e.getMessage());
			return null;
		}
		
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return retorno;
	}
    
    /**
	* @creation date: 30/01/2018
	* @author: David Iglesias - Unisys
	* @description:	Formateo de un Decimal a String con formato determinado
	* @param: num			Texto a formatear
	* @return: Decimal		Date formateado del parametro
	* @exception: 
	* @throws: 
	*/
    public static String formatDecimal (Decimal rA) {
		
        /**
        * Constantes
        */
        final String METHOD = 'formatDecimal';
        
		/**
		 * Variables
		 */
		String retorno;

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        try {
            List<String> args = new String[]{'0','number','###,###,##0.00'};
			retorno = String.format(rA.format(), args);
		} catch(Exception e) {
			System.debug('[RQO_cls_FormatUtils] formatDecimal - Error : ' + e.getMessage());
			return null;
		}
		
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return retorno;
	}

}