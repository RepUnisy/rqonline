@isTest
public class UserManagerTest {

    static testMethod void afterInsert() {
            
        Profile pfl = [select id,name from profile where name='Contract Manager'];
        User testUser = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'u1',
            timezonesidkey='America/Los_Angeles', username='acf1@testorg.com' , FederationIdentifier='acf1@testorg.com' );
          
        insert(testUser);      
            
        String federationId =  'acf1@testorg.com';
        String nombre = 'prueba';
        String apellidos = 'prueba apellidos';
        String telefono = '634567656';
        String telefonoFallo = 'fallo telefono ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo';
        String email = 'acf1@testorg.com';        
        String jefe = '1';
        String nombreUUOO = 'nombreUUOO';
        String ubicacionFisica = 'ubicacionFisica';
        String nombreSede = 'NombreSede';
        String pais = 'ES';
        String unidadNegocio = 'unidad Negocio';       
        UserManagerReturn userManagerReturn = UserManager.UpdateUser(federationId,nombre,apellidos,telefono,
        jefe , nombreUUOO , ubicacionFisica, nombreSede , pais , unidadNegocio  ) ;           

        userManagerReturn = UserManager.UpdateUser(federationId,nombre,apellidos,telefono,
        '0' , nombreUUOO , ubicacionFisica, nombreSede , pais , unidadNegocio  ) ;           
        userManagerReturn = UserManager.UpdateUser('fallo',nombre,apellidos,telefono,
        '0' , nombreUUOO , ubicacionFisica, nombreSede , pais , unidadNegocio  ) ;           
        userManagerReturn = UserManager.UpdateUser(federationId,nombre,apellidos,telefonoFallo,
        '0' , nombreUUOO , ubicacionFisica, nombreSede , pais , unidadNegocio  ) ;           

        
        
    }

}