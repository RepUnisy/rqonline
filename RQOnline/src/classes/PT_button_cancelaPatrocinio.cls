global class PT_button_cancelaPatrocinio {
    
     //metodo que cambia el estado del patrocinio a Cancelado y lo actualiza en BD
     //devuelve un mensaje informativo con el resultado de la operación
     webservice static String cancelaPatrocinio(String idPatrocinioAcancelar){
                
         String mensajeRetorno='';
         
         try{
       
             system.debug('@@@@@ CANCELAR PATROCINIO @@@@');     
    
             Patrocinios__c patrocinio = [SELECT estado__c, Motivo_cancelacion__c from Patrocinios__c where Id =:idPatrocinioAcancelar];
              
             if(patrocinio.Motivo_cancelacion__c != null){
                 
                 patrocinio.Estado__c= 'Cancelado';       
             
                 update(patrocinio);          
    
                 mensajeRetorno = Label.etiqueta_clase_button_cancelar_patrocinio;                 
                         
                 system.debug('@@@@@ '+Label.etiqueta_clase_button_cancelar_patrocinio+' @@@@');     
              
             }
             else{
                  mensajeRetorno = Label.etiqueta_clase_button_cancelar_patrocinio_error;
                 
                  system.debug('@@@@@ '+Label.etiqueta_clase_button_cancelar_patrocinio_error+' @@@@');    
             }
             return mensajeRetorno;  
         }         
         catch(Exception e){
        
             mensajeRetorno = Label.error_generico;
            
             system.debug('@@@ PT_button_cancelaPatrocinio cancelaPatrocinio(): '+mensajeRetorno);
           
            return mensajeRetorno;
        }
     
    }
      
}