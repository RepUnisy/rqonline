@isTest
public with sharing class TRAD_TEST_taskTriggerAfterInsert {
    @testSetup
    static void dataTest(){
        Account acc = new Account();
        acc.Name = 'Account Name';
        insert acc;
        
        Contact cont = new Contact();
        cont.FirstName = 'First Name';
        cont.LastName = 'Last Name';
        cont.AccountId = acc.Id;
        insert cont;
    }
    
    @isTest
    static void testTrigger(){
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Contact cont = [SELECT Id, AccountId FROM Contact LIMIT 1];
        Test.startTest();
        
        Task newTaskWAccount = new Task();
        newTaskWAccount.Subject = 'Subject';
        newTaskWAccount.WhatId = acc.Id;
        insert newTaskWAccount;
        
        Task newTaskWOAccount = new Task();
        newTaskWOAccount.Subject = 'Subject';
        newTaskWOAccount.WhoId = cont.Id;
        insert newTaskWOAccount;
        
        Test.stopTest();
    }
}