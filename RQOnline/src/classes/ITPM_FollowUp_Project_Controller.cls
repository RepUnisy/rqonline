/*------------------------------------------------------------------------
Author:         Borja Martín 
Company:        Indra
Description:    Clase utilizada para mostrar la sección visualforce Follow Up en el objeto Project
History
<Date>          <Author>                      <Description>
13-Jul-2016     Borja Martín                 Initial version
14-Nov-2016     Borja Martín                 Se incluyen nuevos indicadores
----------------------------------------------------------------------------*/
global class ITPM_FollowUp_Project_Controller {
    Apexpages.StandardController controller;
    public String projectId{get;set;}
    public ITPM_Project__c project{get;set;}
    public List<FollowUp> lstFollowUp{get;set;}
    public String fullView{get;set;}

    public ITPM_FollowUp_Project_Controller (ApexPages.StandardController controller){
        projectId = ApexPages.currentPage().getParameters().get('id');
        fullView = ApexPages.currentPage().getParameters().get('fv');
        project = [SELECT Name, ITPM_SEL_PERIODICITY__c, ITPM_DT_Project_XML_Start_Date__c, ITPM_DT_Project_XML_Finish_Date__c, ITPM_DT_Planned_Start_Date__c, ITPM_DT_Planned_Finish_Date__c, ITPM_FOR_Total_Project_Budget__c 
                     FROM ITPM_Project__c WHERE ID =: projectId];              
        lstFollowUp = new List<FollowUp>();
        if(fullView!=null) fullView = fullView.toUpperCase();
        if(fullView=='Y'){
            setLstFollowUp(true);
        }else{
            setLstFollowUp(false);
        }
    }
        
    public class FollowUp{
        public String periodicity {get; set;}                
        public String plannedCost {get; set;}
        public String valueOfWorkDone {get; set;}
        public String plannedWork {get; set;}        
        public String workDone {get; set;}        
        public String performanceIndex {get; set;}        
        public String actualCost {get; set;}
                
        public FollowUp(String vperiodicity, String vplannedCost, String vvalueOfWorkDone , String vplannedWork , String vworkDone , 
                    String vperformanceIndex, String vactualCost){
            periodicity = vperiodicity;
            plannedCost = vplannedCost;
            valueOfWorkDone = vvalueOfWorkDone ;
            plannedWork = vplannedWork;
            actualCost = vactualCost;            
            workDone = vworkDone;
            performanceIndex= vperformanceIndex;            
            actualCost= vactualCost;
        }        
    }
    
    public void setLstFollowUp(boolean fullview){        
        Date dtStart, dtEnd;
        // Si las fechas del XML son nulas, cogemos las del objeto
        if(project.ITPM_DT_Project_XML_Start_Date__c!=null && project.ITPM_DT_Project_XML_Finish_Date__c!=null){
            dtStart = project.ITPM_DT_Project_XML_Start_Date__c.toStartOfMonth();
            dtEnd = project.ITPM_DT_Project_XML_Finish_Date__c;        
        }else if(project.ITPM_DT_Planned_Start_Date__c!=null && project.ITPM_DT_Planned_Finish_Date__c!=null){
            dtStart = project.ITPM_DT_Planned_Start_Date__c.toStartOfMonth();
            
            dtEnd = project.ITPM_DT_Planned_Finish_Date__c;        
        }else{
            FollowUp followUp = new FollowUp(Label.ITPM_M_E_NO_MILESTONE, '', '', '', '', '', '');            
            lstFollowUp.add(followUp);
            return;   
        }
            
        List<ITPM_Milestone__c> listMilestones = [SELECT ITPM_DT_Deadline__c, ITPM_DT_Kickoff__c, ITPM_CUR_COST__c, ITPM_NU_WORK__c, ITPM_PER_COMPLETED__c
                                                        FROM ITPM_Milestone__c 
                                                        WHERE ITPM_REL_PROJECT_LEVELS__c =: projectId];        
        if(listMilestones != null || listMilestones.size()==0){
            listMilestones = [SELECT ITPM_DT_Deadline__c, ITPM_DT_Kickoff__c, ITPM_CUR_COST__c, ITPM_NU_WORK__c, ITPM_PER_COMPLETED__c
                                                        FROM ITPM_Milestone__c 
                                                        WHERE ITPM_REL_PROJECT__c =: projectId]; 
        }                                                
                                                       
        List<ITPM_Milestone_History__c> listProjectHistory = [SELECT ITPM_DT_Date__c, ITPM_CUR_EarnedValue__c, ITPM_PER_RealWork__c, ITPM_CUR_Actual_Cost__c
                                                        FROM ITPM_Milestone_History__c
                                                        WHERE ITPM_REL_Project__c =: projectId
                                                        ORDER BY ITPM_DT_Date__c];        
                                                        
        List<ITPM_Budget_Item__c> listCostItems = [SELECT ITPM_SEL_Year__c, ITPM_CUR_January_expense__c, ITPM_CUR_January_investment__c, ITPM_CUR_February_expense__c, ITPM_CUR_February_investment__c, 
                                                    ITPM_CUR_March_Expense__c, ITPM_CUR_March_investment__c, ITPM_CUR_April_expense__c, ITPM_CUR_April_investment__c, 
                                                    ITPM_CUR_May_expense__c, ITPM_CUR_May_investment__c, ITPM_CUR_June_expense__c, ITPM_CUR_June_investment__c, ITPM_CUR_July_expense__c, 
                                                    ITPM_CUR_July_investment__c, ITPM_CUR_August_expense__c, ITPM_CUR_August_investment__c, ITPM_CUR_September_expense__c, 
                                                    ITPM_CUR_September_investment__c, ITPM_CUR_October_expense__c, ITPM_CUR_October_investment__c, ITPM_CUR_November_expense__c, 
                                                    ITPM_CUR_November_investment__c, ITPM_CUR_December_expense__c, ITPM_CUR_December_investment__c 
                                                   FROM ITPM_Budget_Item__c
                                                   WHERE ITPM_REL2_Project__c =: projectId 
                                                   AND ITPM_CI_SEL_Version__c = 'Actual'
                                                   ORDER BY ITPM_SEL_Year__c];                                                        
                                                        
        Decimal sumaTotalWorks=0;
        for(ITPM_Milestone__c ml : listMilestones){
            sumaTotalWorks = sumaTotalWorks + ml.ITPM_NU_WORK__c;             
        }
        
        List<FollowUp> lstLastFollowUp = new List<FollowUp>();
        Boolean firstLoad = true;
        Decimal accumulated=0;
        if(listMilestones!=null && listMilestones.size()>0){        
            while(dtStart<dtEnd){
                // calculate next date
                if(project.ITPM_SEL_PERIODICITY__c == 'Monthly (30 days)'){
                    dtStart = dtStart.addMonths(1);
                }else if(project.ITPM_SEL_PERIODICITY__c == 'Fortnightly (15 days)'){
                    if(dtStart.day()==1)
                        dtStart = dtStart.addDays(14);
                    else{
                        dtStart = dtStart.addMonths(1).toStartOfMonth();
                    }
                }else if(project.ITPM_SEL_PERIODICITY__c == 'Weekly (7 days)'){
                    if(dtStart.day()==1 || dtStart.day()==8 || dtStart.day()==15)
                        dtStart = dtStart.addDays(7);
                    else{
                        dtStart = dtStart.addMonths(1).toStartOfMonth();
                    }                        
                } 
                           
                // is the last date
                if(dtStart>dtEnd) dtStart = dtEnd;                            
                // set date
                String periodicity = dtStart.day() + '-' + dtStart.month() + '-' + dtStart.year();
                // variables
                Decimal sumaCost=0, sumaPlannedWorks=0, actualCost=0, sumaRealWorks=0, plannedWork=0, realWork=0, earnedValue=0;

                // PlannedCost & PlannedWork
                for(ITPM_Milestone__c ml : listMilestones){
                    // Planned Cost y Planned Work
                    if(dtStart >= ml.ITPM_DT_Deadline__c){
                        // Si el hito ha terminado, se suma entero
                        sumaCost = sumaCost + ml.ITPM_CUR_COST__c;
                        sumaPlannedWorks = sumaPlannedWorks + ml.ITPM_NU_WORK__c;                        
                    }else if(dtStart > ml.ITPM_DT_Kickoff__c){
                        // Si el hito no ha terminado pero teóricamente si ha empezado, se calcula la parte de avance
                        Integer totalDaysMilestones = ml.ITPM_DT_Kickoff__c.daysBetween(ml.ITPM_DT_Deadline__c); // dias totales del hito
                        Integer totalDaysWork = ml.ITPM_DT_Kickoff__c.daysBetween(dtStart); // dias hasta la fecha de corte
                        Decimal percentWork = (totalDaysWork * 100) / totalDaysMilestones; // porcentaje de avance
                        sumaCost = sumaCost + integer.valueof(percentWork*ml.ITPM_CUR_COST__c/100);                        
                        sumaPlannedWorks = sumaPlannedWorks + integer.valueof(percentWork*ml.ITPM_NU_WORK__c/100);
                    }                    
                }
                // earnedValue & Real Work
                for(ITPM_Milestone_History__c his : listProjectHistory){
                    if(dtStart == his.ITPM_DT_Date__c){
                        earnedValue = his.ITPM_CUR_EarnedValue__c;
                        sumaRealWorks = his.ITPM_PER_RealWork__c;
                        actualCost = his.ITPM_CUR_Actual_Cost__c;
                        break;
                    }                    
                }
                
                if(sumaCost==null) sumaCost= 0;
                if(sumaTotalWorks==null) sumaTotalWorks = 0;
                
                // calculate percent
                if(sumaPlannedWorks!=0) plannedWork = (sumaPlannedWorks * 100) / sumaTotalWorks; 
                
                // calculate nulls & adding €/%
                String finalEarnedValue, finalRealWork, finalActualCost;
                if(earnedValue!=0) finalEarnedValue = earnedValue + '€';
                if(actualCost!=0 && actualCost!=null) finalActualCost = actualCost + '€';
                if(sumaRealWorks!=0) finalRealWork= sumaRealWorks + '%';                
                
                // MGX                                
                
                Decimal accounting = 0;    
                for(ITPM_Budget_Item__c ci : listCostItems){
                    Decimal januaryExp=0, januaryInv=0, februaryExp=0, februaryInv=0, marchExp=0, marchInv=0, aprilExp=0, aprilInv=0, mayExp=0, mayInv=0, juneExp=0, juneInv=0, julyExp=0, julyInv=0, augustExp=0, augustInv=0, septExp=0, septInv=0, octExp=0, octInv=0, novExp=0, novInv=0, decExp=0, decInv=0;
                    if(ci.ITPM_CUR_January_expense__c!=null) januaryExp = ci.ITPM_CUR_January_expense__c;
                    if(ci.ITPM_CUR_January_investment__c!=null) januaryInv = ci.ITPM_CUR_January_investment__c;
                    if(ci.ITPM_CUR_February_expense__c!=null) februaryExp = ci.ITPM_CUR_February_expense__c;
                    if(ci.ITPM_CUR_February_investment__c!=null) februaryInv = ci.ITPM_CUR_February_investment__c;
                    if(ci.ITPM_CUR_March_Expense__c!=null) marchExp = ci.ITPM_CUR_March_Expense__c;
                    if(ci.ITPM_CUR_March_investment__c!=null) marchInv = ci.ITPM_CUR_March_investment__c;
                    if(ci.ITPM_CUR_April_expense__c!=null) aprilExp = ci.ITPM_CUR_April_expense__c;
                    if(ci.ITPM_CUR_April_investment__c!=null) aprilInv = ci.ITPM_CUR_April_investment__c;
                    if(ci.ITPM_CUR_May_expense__c!=null) mayExp = ci.ITPM_CUR_May_expense__c;
                    if(ci.ITPM_CUR_May_investment__c!=null) mayInv = ci.ITPM_CUR_May_investment__c;
                    if(ci.ITPM_CUR_June_expense__c!=null) juneExp = ci.ITPM_CUR_June_expense__c;
                    if(ci.ITPM_CUR_June_investment__c!=null) juneInv = ci.ITPM_CUR_June_investment__c;
                    if(ci.ITPM_CUR_July_expense__c!=null) julyExp = ci.ITPM_CUR_July_expense__c;
                    if(ci.ITPM_CUR_July_investment__c!=null) julyInv = ci.ITPM_CUR_July_investment__c;
                    if(ci.ITPM_CUR_August_expense__c!=null) augustExp = ci.ITPM_CUR_August_expense__c;
                    if(ci.ITPM_CUR_August_investment__c!=null) augustInv = ci.ITPM_CUR_August_investment__c;
                    if(ci.ITPM_CUR_September_expense__c!=null) septExp = ci.ITPM_CUR_September_expense__c;
                    if(ci.ITPM_CUR_September_investment__c!=null) septInv = ci.ITPM_CUR_September_investment__c;
                    if(ci.ITPM_CUR_October_expense__c!=null) octExp = ci.ITPM_CUR_October_expense__c;
                    if(ci.ITPM_CUR_October_investment__c!=null) octInv = ci.ITPM_CUR_October_investment__c;
                    if(ci.ITPM_CUR_November_expense__c!=null) novExp = ci.ITPM_CUR_November_expense__c;
                    if(ci.ITPM_CUR_November_investment__c!=null) novExp = ci.ITPM_CUR_November_investment__c;
                    if(ci.ITPM_CUR_December_expense__c!=null) decExp = ci.ITPM_CUR_December_expense__c;
                    if(ci.ITPM_CUR_December_investment__c!=null) decInv = ci.ITPM_CUR_December_investment__c;
                    
                    system.debug('dtStart.year():'+dtStart.year() + ' --- CI YEAR:' + Integer.valueOf(ci.ITPM_SEL_Year__c));
                    
                    if( dtStart.year() == Integer.valueOf(ci.ITPM_SEL_Year__c) ){    
                        //system.debug('MONTH ' + dtStart.month());
                        if(dtStart.month()==1){
                            accounting = accounting + januaryExp + januaryInv;
                        }else if(dtStart.month()==2){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv;
                        }else if(dtStart.month()==3){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv;
                        }else if(dtStart.month()==4){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv;
                        }else if(dtStart.month()==5){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv + mayExp + mayInv;                                                    
                        }else if(dtStart.month()==6){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv + mayExp + mayInv + juneExp + juneInv;                                                        
                        }else if(dtStart.month()==7){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv + mayExp + mayInv + juneExp + juneInv + julyExp + julyInv;                                                        
                        }else if(dtStart.month()==8){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv + mayExp + mayInv + juneExp + juneInv + julyExp + julyInv + augustExp + augustInv;                            
                        }else if(dtStart.month()==9){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv + mayExp + mayInv + juneExp + juneInv + julyExp + julyInv + augustExp + augustInv + septExp + septInv;
                        }else if(dtStart.month()==10){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv + mayExp + mayInv + juneExp + juneInv + julyExp + julyInv + augustExp + augustInv + septExp + septInv + octExp + octInv;
                        }else if(dtStart.month()==11){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv + mayExp + mayInv + juneExp + juneInv + julyExp + julyInv + augustExp + augustInv + septExp + septInv + octExp + octInv + novExp + novInv;
                        }else if(dtStart.month()==12){
                            accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv + mayExp + mayInv + juneExp + juneInv + julyExp + julyInv + augustExp + augustInv + septExp + septInv + octExp + octInv + novExp + novInv + decExp + decInv;
                        }                        
                    }else if( dtStart.year() > Integer.valueOf(ci.ITPM_SEL_Year__c) ){ 
                        accounting = accounting + januaryExp + januaryInv + februaryExp + februaryInv + marchExp + marchInv + aprilExp + aprilInv + mayExp + mayInv + juneExp + juneInv + julyExp + julyInv + augustExp + augustInv + septExp + septInv + octExp + octInv + novExp + novInv + decExp + decInv;                    
                    }
                }
                
                Decimal performanceIndex = 0;
                if(earnedValue!=null && sumaCost!=null && earnedValue!=0 && sumaCost!=0) performanceIndex = earnedValue/sumaCost;
                
                FollowUp followUp = new FollowUp(periodicity, sumaCost+'€', finalEarnedValue, plannedWork.setScale(2, RoundingMode.HALF_UP)+'%', 
                                                 finalRealWork, performanceIndex.setScale(2, RoundingMode.HALF_UP)+'', accounting+'€');
                lstFollowUp.add(followUp);
                if(firstLoad){
                    lstLastFollowUp.add(followUp);
                    firstLoad=false;
                }
                
                // Si estamos en vista de project sólo mostramos la última fila cargada                
                if(!fullview && finalEarnedValue!=null){                                        
                    lstLastFollowUp = new List<FollowUp>();
                    lstLastFollowUp.add(followUp);                            
                }
                                
            }
            if(!fullview){ 
                lstFollowUp = new List<FollowUp>();
                for(FollowUp f : lstLastFollowUp){                    
                    lstFollowUp.add(f);
                }
            }
        }else{
            FollowUp followUp = new FollowUp(Label.ITPM_M_E_NO_MILESTONE, '', '', '', '', '', '');
            lstFollowUp.add(followUp);
        }
    }
        
}