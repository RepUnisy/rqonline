/**
*	@name: RQO_cls_ConsumptionWS
*	@version: 1.0
*	@creation date: 08/11/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Clase handler que se encarga de gestionar la llamada y la response al servicio de SAP que recupera los consumos del cliente
*	@testClass: RQO_cls_ConsumptionWS_test
*/
public without sharing class RQO_cls_ConsumptionWS {

    private static final String CLASS_NAME = 'RQO_cls_ConsumptionWS';
    private List<RQO_obj_logProcesos__c> listProcessLog;
    private RQO_obj_logProcesos__c objLog;
    private ConsumptionCustomResponse consumpResponse = null;
    
	/**
	* @creation date: 08/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método para obtener los consumos de un cliente en SAP
	* @param: clientId, tipo Id. Id de la cuenta en salesforce
	* @param: gradeSKU, tipo String. El dato de grado que ha seteado el cliente en el buscador de consumos
	* @param: family, tipo String. Familia de grados sobre la que se quieren recuperar los consumos
	* @param: fechaInicio, tipo Date.
	* @param: fechaFin, tipo Date.
	* @return: ConsumptionCustomResponse, objeto con los datos de respuesta
	* @exception: 
	* @throws: 
	*/
    public ConsumptionCustomResponse getSapConsumption(String clientId, String gradeSKU, String family, Date fechaInicio, Date fechaFin){
        final String METHOD = 'getSapConsumption';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        consumpResponse = new ConsumptionCustomResponse(RQO_cls_Constantes.SOAP_COD_OK, RQO_cls_Constantes.SOAP_COD_OK);
        //Inicializamos los objetos de log
        this.instanceInitalLog();
        if (String.isNotEmpty(clientId)){
            try{
                system.debug('clientId: ' + clientId);
            	Account cuenta = [Select id, RQO_fld_idExterno__c from Account where Id = : clientId];
                List<String> listaGrados = this.getSKUGradesContainer(gradeSKU);
                List<String> listFamily = (String.isNotEmpty(family)) ? new List<String>{family} : null;
                //Realizamo la llamada
                RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element response = this.doConsumptionCallout(cuenta.RQO_fld_idExterno__c, listaGrados, listFamily, fechaInicio, fechaFin);
                //Gestionamos la response
                this.manageServiceResponse(response);
            }catch(System.CalloutException ex) {
                System.debug('CallOut Exception: ' + ex.getMessage() +ex.getStackTraceString());
                //Si da una excepción de tipo callout genero el log correspondiente
                RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage()+ex.getStackTraceString());
                consumpResponse.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
                consumpResponse.errorMessage = ex.getStackTraceString();
            }catch(Exception ex){
                System.debug('Exception: ' + ex.getMessage() + ex.getStackTraceString());
                //Si da una excepción genérica genero el log correspondiente
                RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage()+ex.getStackTraceString());
                consumpResponse.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
                consumpResponse.errorMessage = ex.getStackTraceString();
            }
        }else{
            String errMess = Label.RQO_lbl_ConsumptionNoDataMsg + RQO_cls_Constantes.COLON + clientId;
            //Si no tengo cuenta genero el log de error correspondiente
			RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, errMess, null);
            consumpResponse.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            consumpResponse.errorMessage = errMess;
        }
        //Si algo no ha ido bien en la ejecución de la llamada genero log de errores
        if (!RQO_cls_Constantes.SOAP_COD_OK.equalsIgnoreCase(consumpResponse.errorCode)){
            RQO_cls_ProcessLog.generateLogData(listProcessLog);
        }
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return consumpResponse;
    }
	
	/**
	* @creation date: 31/01/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Si tenemos SKU buscamos dentro de Salesforce el dato y su container para enviar en la request
	* @param: gradeSKU, tipo String. El dato de grado que ha seteado el cliente en el buscador de consumos
	* @return: List<String>
	* @exception: 
	* @throws: 
	*/
    private List<String> getSKUGradesContainer(String gradeSKU){
        final String METHOD = 'getSKUGradesContainer';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<String> listaGrados = null;
        //Si tenemos SKU vamos a buscar todos sus envases para enviarlos en la request
        if (String.isNotEmpty(gradeSKU)){
            listaGrados = new List<String>();
            String gradeData = '';
            String aux = '';
            for (RQO_obj_containerJunction__c conJunc : [Select RQO_fld_container__r.RQO_fld_container__c from RQO_obj_containerJunction__c where RQO_fld_gradoQp0__r.RQO_fld_sku__c =: gradeSKU]){
                if (conJunc.RQO_fld_container__r.RQO_fld_container__c != null){
                    //Si el código de envase tiene más de dos digitos cogemos los dos primeros
                    aux = (conJunc.RQO_fld_container__r.RQO_fld_container__c.length() > 2) ? conJunc.RQO_fld_container__r.RQO_fld_container__c.substring(0, 2) : conJunc.RQO_fld_container__r.RQO_fld_container__c;
                    //Añadimos a la lista el sku + el envase
                    listaGrados.add(gradeSKU + aux);
                }
            }
            //Aunque no encontremos datos en Saelsforce añadimos a la lista los que se ha seteado en el buscador. 
            //De esta manera el servicio devolverá lo que el cliente ha solicitado
            if (listaGrados == null || listaGrados.isEmpty()){listaGrados.add(gradeSKU);}
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return listaGrados;
    }
    
    /**
	* @creation date: 08/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método genérico que monta la request y realiza el callout contra SAP 
	* @param: sapClientId, tipo String. Cliente sobre el que se quiere recuperar los consumos
	* @param: listSKUData, tipo List<String>. Lista de SKUs
	* @param: listFamily, tipo List<String>. Lista de familias
	* @param: fechaInicio, tipo Date.
	* @param: fechaFin, tipo Date.
	* @return: RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element
	* @exception: 
	* @throws: 
	*/
    private RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element doConsumptionCallout(String sapClientId, List<String> listSKUData, List<String> listFamily, Date fechaInicio, Date fechaFin){
		final String METHOD = 'doConsumptionCallout';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Cargamos la request
        String IvFechaIni = (fechaInicio != null) ? String.valueOf(fechaInicio) : null;
        String IvFechaFin = (fechaFin != null) ? String.valueOf(fechaFin) : null;
        RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE objCall = new RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE();
        RQO_cls_SAPServicesWS.ZqoTtClientes itClientes = new RQO_cls_SAPServicesWS.ZqoTtClientes();
        RQO_cls_SAPServicesWS.ZqoTtMatnr itMateriales = null;
        RQO_cls_SAPServicesWS.ZqoTtFamilias itFamilias = null;
        
        itClientes.item = new List<String>{sapClientId};
        //Seteamos la lista de SKU si es que viene rellenada
        if (listSKUData != null){
            itMateriales = new RQO_cls_SAPServicesWS.ZqoTtMatnr();
            itMateriales.item = listSKUData;
        }
        //Seteamos la lista de familias si es que viene rellenada
        if (listFamily != null){
            itFamilias = new RQO_cls_SAPServicesWS.ZqoTtFamilias();
            itFamilias.item = listFamily;
        }
        
		//Realizamos la llamada y retornamos los datos
		RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element elemtResponse = objCall.ZqoCargaConsumos(itClientes, itFamilias, itMateriales, IvFechaFin, IvFechaIni, objLog);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return elemtResponse;
    }
    
	/**
	* @creation date: 08/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Gestiona la respuesta del servicio
	* @param:response tipo RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element, respuesta del servicio
	* @return: void
	* @exception: 
	* @throws: 
	*/
    private void manageServiceResponse(RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element response){
        final String METHOD = 'manageServiceResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Verificamos que tenemos response, en caso contrario será error genérico.
        if (response != null){
            //Verificamos si tenemos mensajes de vuelta
            if (response.EtReturn != null && response.EtReturn.item != null && !response.EtReturn.item.isEmpty()){
            	this.manageGenericResponse(response.EtReturn.item);
            }
			//Si no tenemos mensaje de tipo error en la response
            if (!RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR.equalsIgnoreCase(consumpResponse.errorCode)){
                this.generateReturnObject(response.EtConsumos);
            }
        }else{
            consumpResponse.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            consumpResponse.errorMessage = Label.RQO_lbl_GenericExceptionMsg;
        }
        //Cargamos los datos en el objeto log
        RQO_cls_ProcessLog.completeLog(objLog, consumpResponse.errorCode, consumpResponse.errorMessage, null);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }

	/**
    * @creation date: 19/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Gestion genérica del objeto de los servicio que retorna los mensajes de respuesta
    * @param: listObjMessResponse tipo RQO_cls_SAPServicesWS.Bapiret2. Lista de mensajes de respuesta
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void manageGenericResponse(List<RQO_cls_SAPServicesWS.Bapiret2> listObjMessResponse){
        final String METHOD = 'manageGenericResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        for(RQO_cls_SAPServicesWS.Bapiret2 objAux : listObjMessResponse){
            //Recuperamos el mensaje de error.
            if (String.isNotEmpty(objAux.Message)){
                consumpResponse.errorMessage = (String.isNotEmpty(objAux.Type_x)) ? objAux.Type_x + RQO_cls_Constantes.COLON + objAux.Message : objAux.Message;
                if (String.isNotEmpty(objAux.MessageV1)){consumpResponse.errorMessage = consumpResponse.errorMessage.replace('&1', objAux.MessageV1);}
                if (String.isNotEmpty(objAux.MessageV2)){consumpResponse.errorMessage = consumpResponse.errorMessage.replace('&2', objAux.MessageV2);}
                if (String.isNotEmpty(objAux.MessageV3)){consumpResponse.errorMessage = consumpResponse.errorMessage.replace('&3', objAux.MessageV3);}
                if (String.isNotEmpty(objAux.MessageV4)){consumpResponse.errorMessage = consumpResponse.errorMessage.replace('&4', objAux.MessageV4);}
                consumpResponse.errorMessage += RQO_cls_Constantes.SEMICOLON;
            }
            //Si da error no seguimos recuperando mensajes
            if (RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR.equalsIgnoreCase(objAux.Type_x)){
                consumpResponse.errorCode = RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR;
                consumpResponse.errorNumber = objAux.Number_x;
                if (RQO_cls_Constantes.REQ_WS_RESPONSE_NO_DATA_ERROR.equalsIgnoreCase(objAux.Number_x)){
                    consumpResponse.noDataError = true;
                }
                break;
            }
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    
	/**
    * @creation date: 30/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Genera el objeto de respuesta a devolver a la vista si todo es correcto. El método utiliza mapas para agrupar los datos de año, mes, grado para calcular 
    * el importe y cantidad correspondientes a cada mes de cada año y posteriormente genera las listas ordenadas que se pintarán por pantalla.
    * @param: consumosData tipo RQO_cls_SAPServicesWS.ZqoTtConsumos 
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void generateReturnObject(RQO_cls_SAPServicesWS.ZqoTtConsumos consumosData){
        final String METHOD = 'generateReturnObject';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Verificamos si tenemos datos de consumos en la lista. De no ser así cargamos en la response el error ND
        if (consumosData != null && consumosData.item != null && !consumosData.item.isEmpty()){
            //Definición de variables
            String keyFamily = '';
            String keyTranslatedMonth = '';
            String keyYearMonth = '';
            
            RQO_cls_ConsumptionAuxObj objConsumo = null;
            Map<String, RQO_cls_ConsumptionAuxObj.RQO_cls_ConsumptionAuxObjDate> mapFechasConsumo = null;
            RQO_cls_ConsumptionAuxObj.RQO_cls_ConsumptionAuxObjDate objFechasConsumo = null;
            
            List<RQO_cls_ConsumptionAuxObj> listaDatos = new List<RQO_cls_ConsumptionAuxObj>();
            Map<String, RQO_cls_ConsumptionAuxObj> mapData = new Map<String, RQO_cls_ConsumptionAuxObj>();
            
            Map<String, List<String>> mapHeaderData = new Map<String, List<String>>();
            List<String> yearListData = null;
			
            Decimal cantidad = null;
            Decimal importe = null;
            Integer countYear = 0;
            Integer totalCount = 0;
            
            RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
            Map<String, String> mapFamilies = this.getFamilyDataMap();
            /**/
            //Vamos a generar un mapa<String, List<String>> con el mes y la lista de años para ese mes. Este mapa se utilizará para pintar la cabecera.
            //Por otro lado vamos a generar un mapa de objetos de tipo RQO_cls_ConsumptionAuxObj en el que almacenaremos los datos por familia. Este objeto tiene definido otro atributo
            //mapa de tipo Map<String, RQO_cls_ConsumptionAuxObjDate> donde RQO_cls_ConsumptionAuxObjDate es un objeto embedido que contendrá agrupado el importe y la cantidad de esa familia
            //para un año-mes concreto. En definitiva, se generá un mapa de datos agrupado por familia con un mapa para cada grado con las cantidades e importe acumulados.
            for(RQO_cls_SAPServicesWS.ZqoStConsumos data : consumosData.item){
                System.debug('Data: ' + data);
                //Clave para la familia
                keyFamily = data.Familia;
                //Recuperamos el mes en el idioma actual del usuario. Se utilizará como key y también se mostrará en pantalla
                keyTranslatedMonth = genutil.getMonthName(data.Periodo);
                //Clave año#mes para generar la cabecera de datos
                keyYearMonth = data.Ejercicio + RQO_cls_Constantes.PAD + keyTranslatedMonth;
                //Para cargar los datos en los mapas jugamos con posiciones de memoria.
                //Mapa de datos por grado, si el grado ya está añadido recuperamos su submapa de cantidad/importe por año#mes. Si no existe generamos los datos del grado
                //e instaciamos su submapa
                if (mapData != null && !mapData.isEmpty() && mapData.containsKey(keyFamily)){
                    objConsumo = mapData.get(keyFamily);
                    mapFechasConsumo = objConsumo.dataMap;
                }else{
                    objConsumo = new RQO_cls_ConsumptionAuxObj();
                    //objConsumo.grade = data.Material;
                    objConsumo.family = data.Familia;
                    objConsumo.familyColor = this.getFamilyColor(data.Familia);
                    objConsumo.label = (mapFamilies != null && !mapFamilies.isEmpty() && mapFamilies.containsKey(data.Familia)) ? mapFamilies.get(data.Familia) : null;
                    mapFechasConsumo = new Map<String, RQO_cls_ConsumptionAuxObj.RQO_cls_ConsumptionAuxObjDate>();
                    objConsumo.dataMap = mapFechasConsumo;
                    mapData.put(keyFamily, objConsumo);
                    listaDatos.add(objConsumo);
                }
                //Submapa de año#mes instanciado o recuperado en el paso anterior. Vamos a establecer los datos en este mapa. 
                //Verificamaos si existen datos para el año#mes en el mapa, si es así recuperamos el objeto para acumular la cantidad y el importe. Si no existen datos
                //Generamos nuevo dato en el mapa para ese grado
                if (mapFechasConsumo != null && !mapFechasConsumo.isEmpty() && mapFechasConsumo.containsKey(keyYearMonth)){
                    objFechasConsumo = mapFechasConsumo.get(keyYearMonth);
                }else{
                    objFechasConsumo = new RQO_cls_ConsumptionAuxObj.RQO_cls_ConsumptionAuxObjDate();
                    mapFechasConsumo.put(keyYearMonth, objFechasConsumo);
                    objFechasConsumo.yearConsum = data.Ejercicio;
                    objFechasConsumo.monthComsum = keyTranslatedMonth;
                    objFechasConsumo.monthSortData = genUtil.getMonthNumber(keyTranslatedMonth);
                }
                //Sumamos el dato de cantidad para ese grado
                cantidad = (String.isNotEmpty(objFechasConsumo.data) && String.isNotEmpty(data.Cantidad)) ? objFechasConsumo.cantidad + Decimal.valueOf(data.Cantidad) : Decimal.valueOf(data.Cantidad);
                objFechasConsumo.cantidad = (cantidad != null) ? cantidad / 1000 : null;
                objFechasConsumo.cantidadKilos = (cantidad != null) ? String.valueOf(cantidad) : null;
                objFechasConsumo.data = (cantidad != null) ? String.valueOf(((cantidad / 1000).setScale(2)).format()) : null;
                objFechasConsumo.unidad = (data.Unidad != null) ? data.Unidad.trim() : null;
                /*objFechasConsumo.moneda = (data.Moneda != null) ? 
                    (RQO_cls_Constantes.CODE_CURRENCY_EURO.equalsIgnoreCase(data.Moneda.trim())) ?  RQO_cls_Constantes.SYMBOL_EURO :
                	(RQO_cls_Constantes.CODE_CURRENCY_DOLAR.equalsIgnoreCase(data.Moneda.trim())) ?  RQO_cls_Constantes.SYMBOL_DOLAR : data.Moneda.trim(): null;*/
                objFechasConsumo.moneda = data.Moneda;
                
                //Sumamos el dato de importe para ese grado
                importe = (objFechasConsumo.importeSAP != null && String.isNotEmpty(data.Importe)) ? objFechasConsumo.importeSAP + Decimal.valueOf(data.Importe) : Decimal.valueOf(data.Importe);
				objFechasConsumo.importeSAP = (importe != null) ? importe : null;
                objFechasConsumo.importe = (importe != null) ? String.valueOf((importe.setScale(2)).format()) : null;

                //Generamos el dato de cabecera. Verificamos si ya existe el mes en el mapa. Si es así recuperamos la lista de años, si no es así generomos una nueva lista
                if (mapHeaderData != null && !mapHeaderData.isEmpty() && mapHeaderData.containsKey(keyTranslatedMonth)){
                    yearListData = mapHeaderData.get(keyTranslatedMonth);
                }else{
                    countYear = 0;
                    yearListData = new List<String>();
                    mapHeaderData.put(keyTranslatedMonth, yearListData);
                    System.debug('mapHeaderData: ' + mapHeaderData);
                }
                //Verificamos si hay que añadir el año a la lista
                countYear = this.addToListYear(data.Ejercicio, yearListData);
                if (countYear > totalCount){totalCount = countYear;}
            }
            //Obtenemos a partir del mapa de cabecera una lista ordenada de datos.
            consumpResponse.headerDataList = this.getHeaderInListMode(mapHeaderData);
            //Obtenemos a partir del mapa de datos una lista ordenada de datos. La ordenación tiene que coincidir con la de cabecera
            this.getDataInListMode(listaDatos, consumpResponse.headerDataList);
            consumpResponse.sapConsumptionsList = listaDatos;
        }else{
            consumpResponse.errorCode = RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR;
			consumpResponse.noDataError = true;
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
	/**
    * @creation date: 19/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Recupera un mapa a partir de un custom settings con la traducción de todas las familias de SAP
    * @return: Map<String, String>
    * @exception: 
    * @throws: 
    */
    private Map<String, String> getFamilyDataMap(){
        final String METHOD = 'getFamilyDataMap';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Map<String, String> families = null;
        List<RQO_cs_sapFamiliesTranslate__c> csData = RQO_cs_sapFamiliesTranslate__c.getall().values();
        if (csData != null && !csData.isEmpty()){
            families = new Map<String, String>();
            for(RQO_cs_sapFamiliesTranslate__c objCs : csData){
                families.put(objCs.name, objCs.RQO_fld_label__c);
            }
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return families;
    }

   	/**
    * @creation date: 30/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Convierte el mapa de datos que tenemos en cada objeto RQO_cls_ConsumptionAuxObj en una lista ordenada para que sea más sencillo mostrarla por pantalla en el componente lightning
    * @param:listaDatos tipo List<RQO_cls_ConsumptionAuxObj> 
    * @param:listHeader tipo List<RQO_cls_ConsumptionWS.HeaderDataList>
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void getDataInListMode(List<RQO_cls_ConsumptionAuxObj> listaDatos, List<RQO_cls_ConsumptionWS.HeaderDataList> listHeader){
        final String METHOD = 'getDataInListMode';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<RQO_cls_ConsumptionAuxObj.RQO_cls_ConsumptionAuxObjDate> aux = null;  
        RQO_cls_ConsumptionAuxObj.RQO_cls_ConsumptionAuxObjDate consumAuxDate = null;
        String auxKey = '';
        //Recorremos la lista de consumos
        for (RQO_cls_ConsumptionAuxObj consump : listaDatos){
            System.debug('consump: ' + consump);
            consumAuxDate = null;
            aux = null;
            //Recuperamos el submapa de datos con los datos acumulados por año#mes
            if (consump.dataMap != null){
                aux = new List<RQO_cls_ConsumptionAuxObj.RQO_cls_ConsumptionAuxObjDate>();
                //Recorremos los datos de cabecera ya que vamos a añadir un dato de consumo por cada dato de la cabecera. En aquellos en los que no coincida con el mapa tendremos dato vacío
                //De esta manera tenemos cuadrados los datos de cabecera con los datos de cada grado a la ghora de mostrar por pantalla.
                for(RQO_cls_ConsumptionWS.HeaderDataList objAux : listHeader){
                    
                    for(String annio : objAux.years){
                        auxKey =  annio + '#' + objAux.month;
                        if (consump.dataMap.containsKey(auxKey)){
							consumAuxDate= consump.dataMap.get(auxKey);
                        }else{
                            consumAuxDate = new RQO_cls_ConsumptionAuxObj.RQO_cls_ConsumptionAuxObjDate();
                            consumAuxDate.yearConsum = annio;
                            consumAuxDate.monthComsum = objAux.month;
                            consumAuxDate.monthSortData = objAux.monthSortData;
                            consumAuxDate.data = '';
                            consumAuxDate.importe = '';
                        }
                        aux.add(consumAuxDate);
                    }
                }
                //Ordenamos la lista por el campo mes
                aux.sort();
                
            }
            if (aux != null && !aux.isEmpty()){consump.dataList = aux;}
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
	/**
    * @creation date: 30/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Convierte el mapa de datos de cabecera en una lista para que sea más sencillo mostrarla por pantalla en el componente lightning
    * @param:mapDatos tipo Map<String, List<String>>
    * @return: List<RQO_cls_ConsumptionWS.HeaderDataList>
    * @exception: 
    * @throws: 
    */
    private List<RQO_cls_ConsumptionWS.HeaderDataList> getHeaderInListMode(Map<String, List<String>> mapDatos){
        final String METHOD = 'getHeaderInListMode';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<RQO_cls_ConsumptionWS.HeaderDataList> aux = new List<HeaderDataList>();
  		RQO_cls_ConsumptionWS.HeaderDataList auxObject = null;
        List<String> listYears = null;
        RQO_cls_GeneralUtilities genUtil = new RQO_cls_GeneralUtilities();
        //Recorremos los datos del mapa de cabecera 
        for(String objAux : mapDatos.keySet()){
            listYears= mapDatos.get(objAux);
            auxObject = new RQO_cls_ConsumptionWS.HeaderDataList();
            auxObject.month = objAux;
            auxObject.monthSortData = genUtil.getMonthNumber(objAux);
            auxObject.years = listYears;
            auxObject.years.sort();
            aux.add(auxObject);
        } 
        //Ordenamos la lista por mes
        aux.sort();
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return aux;
    }
    
    /**
	* @creation date: 30/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Añade el año a la lista de cabecera si es necesario
	* @param: ejercicio tipo String
	* @param: yearListData tipo List<String>
	* @return: Integer con el total de datos que hjay en la lista actual
	* @exception: 
	* @throws: 
	*/
    private Integer addToListYear(String ejercicio, List<String> yearListData){
        final String METHOD = 'addToListYear';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Integer addCount = 0;
        if (String.isNotEmpty(ejercicio)){
            if (yearListData == null || yearListData.isEmpty()){
                yearListData.add(ejercicio);
            }else{
                Boolean existInList = false;
                for(String srcYear : yearListData){
                    if (srcYear.equalsIgnoreCase(ejercicio)){
                        existInList = true;
                        break;
                    }
                }
                if (!existInList){yearListData.add(ejercicio);addCount++;}                
            }
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return addCount;
    }
    
	/**
    * @creation date: 07/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Recupera el color parametrizado en el custom settings RQO_cs_sapFamiliesTranslate__c para la familia. En caso de no encontrar 
    * el dato pone un color random
    * @param: family tipo String
    * @return: String, el color
    * @exception: 
    * @throws: 
    */
    private String getFamilyColor(String family){
        String familyColor = '';
        //Intentamos recuperar el color de la familia
        if (String.isNotBlank(family)){
            RQO_cs_sapFamiliesTranslate__c obj = RQO_cs_sapFamiliesTranslate__c.getInstance(family);
            if (obj != null && String.isNotBlank(obj.ROQ_fld_familyColor__c)){familyColor = obj.ROQ_fld_familyColor__c;}
        }
        //Si no hemos rellenado el color para la familia obtenemos un color random
        if (String.isEmpty(familyColor)){familyColor = this.getRandomColor();}
        
        return familyColor;
    }
	
    /**
    * @creation date: 07/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Recupera un color random para la familia
    * @return: String con el color
    * @exception: 
    * @throws: 
    */
    private String getRandomColor(){
        String color = '#';
        Integer position = null;
        for (Integer i = 0; i < 6; i++) {
            position = Integer.valueOf(Math.floor(Math.random() * 15));
            color += RQO_cls_Constantes.LETTERS.substring(position, position+1);
        }
        return color;
    }
    
	/**
	* @creation date: 08/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Instancia la lista de logs y el objeto log con los datos iniciales
	* @param:
	* @return: void
	* @exception: 
	* @throws: 
	*/
    private void instanceInitalLog(){
        final String METHOD = 'instanceInitalLog';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        if(listProcessLog == null){listProcessLog = new List<RQO_obj_logProcesos__c>();}
        objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_SAP, RQO_cls_Constantes.SERVICE_GET_CONSUMPTION);
        listProcessLog.add(objLog);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
	* @creation date: 08/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Objeto wrapper para devolver los datos de la llamada al servicio
	* @param:
	* @return: void
	* @exception: 
	* @throws: 
	*/
    public class ConsumptionCustomResponse{
        public String errorCode{get;set;}
        public String errorMessage{get;set;}
        public String errorNumber{get;set;}
        public Boolean noDataError{get;set;}
        public Integer totalYearsCount{get;set;}
        public List<RQO_cls_ConsumptionWS.HeaderDataList> headerDataList{get;set;}
        public List<RQO_cls_ConsumptionAuxObj> sapConsumptionsList{get;set;}
        
        public ConsumptionCustomResponse(String errorCode, String errorMessage){
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
        }
    }
	
    /**
	* @creation date: 08/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Objeto wrapper con los datos de cabecera
	* @param:
	* @return: void
	* @exception: 
	* @throws: 
	*/
    public class HeaderDataList implements Comparable{
		public String month{get;set;}
        public Integer monthSortData{get;set;}
        public Integer yearsCount{
            get{
                if (this.years != null && !this.years.isEmpty()){return years.size();}else{return 0;}
            }set;}
        
        public List<String> years{get;set;}
        
		/**
        * @creation date: 18/12/2017
        * @author: Alvaro Alonso Trinidad - Unisys
        * @description: implementación del método compareTo para ordenar la lista de objetos
        * @param: compareTo tipo Object
        * @return: Integer
        * @exception: 
        * @throws: 
        */
        public Integer compareTo(Object compareTo) {
            HeaderDataList compareToEmp = (HeaderDataList)compareTo;
            if (monthSortData == compareToEmp.monthSortData) return 0;
            if (monthSortData > compareToEmp.monthSortData) return 1;
            return -1;
        }
    }
}