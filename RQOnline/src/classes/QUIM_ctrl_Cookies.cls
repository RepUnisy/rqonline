global with sharing class QUIM_ctrl_Cookies {
	global string variable {get; set;}
    public string lang {get; set;}


	public QUIM_ctrl_Cookies() {
		
      lang = (String) ApexPages.currentPage().getParameters().get('cclcl');
      
	  Cookie counter = ApexPages.currentPage().getCookies().get('count');
      Cookie pepe = ApexPages.currentPage().getCookies().get('rend');
    
        // If this is the first time the user is accessing the page, 
        // create a new cookie with name 'counter', an initial value of '1', 
        // path 'null', maxAge '-1', and isSecure 'false'. 
        if (counter == null) {
           
            counter = new Cookie('count','1',null,-1,false);

        }else{
        // If this isn't the first time the user is accessing the page
        // create a new cookie, incrementing the value of the original count by 1
            
            Integer count = Integer.valueOf(counter.getValue());
            counter = new Cookie('count', String.valueOf(count+1),null,-1,false);
        }
        if(pepe == null){
            
            pepe = new Cookie('rend', '0',null, -1, false);

        }else{

            if(variable == '1'){
                pepe = new Cookie('rend', '1',null, -1, false);
            }
        }
    
        // Set the new cookie for the page
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
        ApexPages.currentPage().setCookies(new Cookie[]{pepe});

        
    }

    // This method is used by the Visualforce action {!count} to display the current 
    // value of the number of times a user had displayed a page. 
    // This value is stored in the cookie.
    public String getCount() {
        Cookie counter = ApexPages.currentPage().getCookies().get('count');
        
        if(counter == null) { return '0'; }

        return counter.getValue();
    }

    public String getRend(){
         Cookie pepe = ApexPages.currentPage().getCookies().get('rend'); 
          
        if(pepe == null) {  return '0';  }

        return pepe.getValue();
    }

   
    @RemoteAction
    global static String pepito(String mostrar){
         system.debug('HOLA1:     ' + mostrar);

     

         //nteger mostrar = Integer.valueOf(pepe.getValue());
         
        if(mostrar == '0' || mostrar == null){
             system.debug('HOLA14:');
         
             system.debug('HOLA143:');
             //variable='pepe';
            mostrar ='1';
            //ApexPages.currentPage().setCookies(new Cookie[]{pepe});
        }
        else{system.debug('algo falla:  ');}
  
       
        return mostrar;

    }
    //DCPAI_vf_assignedToPlan 
    //DCPAI_vf_Budget_Assign 
}