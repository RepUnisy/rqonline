/**
*	@name: RQO_cls_UtilSeguimientoHistorico_test
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase RQO_cls_UtilSeguimientoHistorico
*/
@IsTest
public class RQO_cls_UtilSeguimientoHistorico_test {
    
    
    /**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserSegHist@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
    }
    
    
	/*Seguimiento*/

	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de posiciones de solicitud en la ventana de seguimiento
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getSeguimientoPosicionSolicitud(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSegHist@testorg.com');
        System.runAs(u){
            Date fechaActual = Date.today();
            List<Asset> listaPosicionesSolicitud = RQO_cls_TestDataFactory.createAsset(200);
            //Por defecto se crean como posiciones de pedido, vamos a actualizar los datos para generar posiciones de solicitud
			RecordType recordType = [Select Id from Recordtype where DeveloperName = 'RQO_rt_Posicion_de_solicitud']; 
            for(Integer i= 0; i<listaPosicionesSolicitud.size();i++){
                listaPosicionesSolicitud[i].RecordtypeId = recordType.Id;
                listaPosicionesSolicitud[i].RQO_fld_numRefCliente__c = 'test';
                listaPosicionesSolicitud[i].RQO_fld_CUDeliveryDate__c = fechaActual;
                listaPosicionesSolicitud[i].RQO_fld_positionStatus__c = RQO_cls_Constantes.PED_COD_STATUS_SOLICITADO;
			}
            update listaPosicionesSolicitud;
            
            test.startTest();
            	RQO_cls_UtilSeguimientoHistorico util = new RQO_cls_UtilSeguimientoHistorico();
				util.isHistoricInitialData = false;
				util.isInitialData = false; 
            	String result = util.getSeguimientoData(listaPosicionesSolicitud[0].AccountId, 'es_ES', listaPosicionesSolicitud[0].RQO_fld_requestManagement__r.name, 
                                                        null, listaPosicionesSolicitud[0].RQO_fld_numRefCliente__c, 
                                                        listaPosicionesSolicitud[0].RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c, null, null, 
                                                        listaPosicionesSolicitud[0].RQO_fld_positionStatus__c, null, 
                                                        String.valueOf(listaPosicionesSolicitud[0].RQO_fld_CUDeliveryDate__c), 
                                                        String.valueOf(listaPosicionesSolicitud[0].RQO_fld_CUDeliveryDate__c));
            	
            
            test.stopTest();
            System.assertNotEquals(null, result);
            System.assertNotEquals('', result);
            List<RQO_cls_PedidoAuxiliarBean> listaPedido = (List<RQO_cls_PedidoAuxiliarBean>)JSON.deserialize(result, List<RQO_cls_PedidoAuxiliarBean>.class);
            //System.debug('listaPedido: ' + result);
            System.assertEquals(200, listaPedido.size());
        }
    }

    /**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de posiciones de pedido en la ventana de seguimiento
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getSeguimientoPosicionPedido(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSegHist@testorg.com');
        System.runAs(u){
            List<Asset> listaPosicionesPedido = RQO_cls_TestDataFactory.createAsset(200);
            test.startTest();
            	RQO_cls_UtilSeguimientoHistorico util = new RQO_cls_UtilSeguimientoHistorico();
				util.isHistoricInitialData = false;
				util.isInitialData = false; 
            	RQO_cls_customException ex = new RQO_cls_customException();
            	ex.stringException = 'Generic Exception';
            	String result = util.getSeguimientoData(listaPosicionesPedido[0].AccountId, 'es_ES', null, 
                                                        listaPosicionesPedido[0].RQO_fld_idRelacion__r.RQO_fld_idExterno__c,
                                                        listaPosicionesPedido[0].RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c, 
                                                        listaPosicionesPedido[0].RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c, null, null, 
                                                        listaPosicionesPedido[0].RQO_fld_positionStatus__c, listaPosicionesPedido[0].RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__c, 
                                                        String.valueOf(Date.today()), 
                                                        String.valueOf(Date.today()));
            test.stopTest();
            
            System.assertNotEquals(null, result);
            System.assertNotEquals('', result);
            List<RQO_cls_PedidoAuxiliarBean> listaPedido = (List<RQO_cls_PedidoAuxiliarBean>)JSON.deserialize(result, List<RQO_cls_PedidoAuxiliarBean>.class);
            //System.debug('listaPedido: ' + result);
            System.assertEquals(200, listaPedido.size());
        }
    }
	
	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de posiciones de entrega en la ventana de seguimiento
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getSeguimientoPosicionEntrega(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSegHist@testorg.com');
        System.runAs(u){
            List<RQO_obj_posiciondeEntrega__c> listaPosicionesEntrega = RQO_cls_TestDataFactory.createPosiciondeEntrega(200);
            List<Asset> listaActivos = [Select AccountId, RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c,RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c,
                                        RQO_fld_idRelacion__r.RQO_fld_idExterno__c
                                        from Asset where Id = : listaPosicionesEntrega[0].RQO_fld_posicionPedido__c];            
            
            test.startTest();
            	RQO_cls_UtilSeguimientoHistorico util = new RQO_cls_UtilSeguimientoHistorico();
				util.isHistoricInitialData = false;
				util.isInitialData = false;
                String result = util.getSeguimientoData(listaActivos[0].AccountId, 'es_ES', null, 
                                                        listaActivos[0].RQO_fld_idRelacion__r.RQO_fld_idExterno__c, 
                                                        listaActivos[0].RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c, 
                                                        null, 
                                                        String.valueOf(Date.today()), String.valueOf(Date.today()), 
                                                        listaPosicionesEntrega[0].RQO_fld_idRelacion__r.RQO_fld_estadodelaEntrega__c, null, 
                                                        null, null);
            test.stopTest();
            
            System.assertNotEquals(null, result);
            System.assertNotEquals('', result);
            List<RQO_cls_PedidoAuxiliarBean> listaPedido = (List<RQO_cls_PedidoAuxiliarBean>)JSON.deserialize(result, List<RQO_cls_PedidoAuxiliarBean>.class);
            System.assertEquals(200, listaPedido.size());
        }
    }
    
    /**/
    
	/*Historial*/

	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de posiciones de solicitud en la ventana de historial
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getHistorialPosicionSolicitud(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSegHist@testorg.com');
        System.runAs(u){
            Date fechaActual = Date.today();
            List<Asset> listaPosicionesSolicitud = RQO_cls_TestDataFactory.createAsset(200);
            //Por defecto se crean como posiciones de pedido, vamos a actualizar los datos para generar posiciones de solicitud
			RecordType recordType = [Select Id from Recordtype where DeveloperName = 'RQO_rt_Posicion_de_solicitud']; 
            for(Integer i= 0; i<listaPosicionesSolicitud.size();i++){
                listaPosicionesSolicitud[i].RecordtypeId = recordType.Id;
                listaPosicionesSolicitud[i].RQO_fld_numRefCliente__c = 'test';
                listaPosicionesSolicitud[i].RQO_fld_CUDeliveryDate__c = fechaActual;
                listaPosicionesSolicitud[i].RQO_fld_positionStatus__c = RQO_cls_Constantes.PED_COD_STATUS_SOLICITADO;
			}
            update listaPosicionesSolicitud;
            
            test.startTest();
            	RQO_cls_UtilSeguimientoHistorico util = new RQO_cls_UtilSeguimientoHistorico();
				util.isHistoricInitialData = false;
				util.isInitialData = false; 
            	String result = util.getHistorialData(listaPosicionesSolicitud[0].AccountId, 'es_ES', listaPosicionesSolicitud[0].RQO_fld_requestManagement__r.name, 
                                                        null, listaPosicionesSolicitud[0].RQO_fld_numRefCliente__c, 
                                                        listaPosicionesSolicitud[0].RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c, null, null, 
                                                        listaPosicionesSolicitud[0].RQO_fld_positionStatus__c, null, 
                                                        String.valueOf(listaPosicionesSolicitud[0].RQO_fld_CUDeliveryDate__c), 
                                                        String.valueOf(listaPosicionesSolicitud[0].RQO_fld_CUDeliveryDate__c));
            	
            
            test.stopTest();
            System.assertNotEquals(null, result);
            System.assertNotEquals('', result);
            List<RQO_cls_PedidoAuxiliarBean> listaPedido = (List<RQO_cls_PedidoAuxiliarBean>)JSON.deserialize(result, List<RQO_cls_PedidoAuxiliarBean>.class);
            System.assertEquals(200, listaPedido.size());
        }
    }

    /**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de posiciones de pedido en la ventana de historial
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getHistorialPosicionPedido(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSegHist@testorg.com');
        System.runAs(u){
            List<Asset> listaPosicionesPedido = RQO_cls_TestDataFactory.createAsset(200);
            test.startTest();
            	RQO_cls_UtilSeguimientoHistorico util = new RQO_cls_UtilSeguimientoHistorico();
				util.isHistoricInitialData = false;
				util.isInitialData = false; 
            	String result = util.getHistorialData(listaPosicionesPedido[0].AccountId, 'es_ES', null, 
                                                        listaPosicionesPedido[0].RQO_fld_idRelacion__r.RQO_fld_idExterno__c,
                                                        listaPosicionesPedido[0].RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c, 
                                                        listaPosicionesPedido[0].RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c, null, null, 
                                                        listaPosicionesPedido[0].RQO_fld_positionStatus__c, listaPosicionesPedido[0].RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__c, 
                                                        String.valueOf(Date.today()), 
                                                        String.valueOf(Date.today()));
            test.stopTest();
            
            System.assertNotEquals(null, result);
            System.assertNotEquals('', result);
            List<RQO_cls_PedidoAuxiliarBean> listaPedido = (List<RQO_cls_PedidoAuxiliarBean>)JSON.deserialize(result, List<RQO_cls_PedidoAuxiliarBean>.class);
            System.assertEquals(200, listaPedido.size());
        }
    }
	
	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de posiciones de entrega en la ventana de historial
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getHistorialPosicionEntrega(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSegHist@testorg.com');
        System.runAs(u){
            List<RQO_obj_posiciondeEntrega__c> listaPosicionesEntrega = RQO_cls_TestDataFactory.createPosiciondeEntrega(200);
            List<Asset> listaActivos = [Select AccountId, RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c,RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c,
                                        RQO_fld_idRelacion__r.RQO_fld_idExterno__c
                                        from Asset where Id = : listaPosicionesEntrega[0].RQO_fld_posicionPedido__c];            
            
            test.startTest();
            	RQO_cls_UtilSeguimientoHistorico util = new RQO_cls_UtilSeguimientoHistorico();
				util.isHistoricInitialData = false;
				util.isInitialData = false;
                String result = util.getHistorialData(listaActivos[0].AccountId, 'es_ES', null, 
                                                        listaActivos[0].RQO_fld_idRelacion__r.RQO_fld_idExterno__c, 
                                                        listaActivos[0].RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c, 
                                                        null, 
                                                        String.valueOf(Date.today()), String.valueOf(Date.today()), 
                                                        listaPosicionesEntrega[0].RQO_fld_idRelacion__r.RQO_fld_estadodelaEntrega__c, null, 
                                                        null, null);
            test.stopTest();
            
            System.assertNotEquals(null, result);
            System.assertNotEquals('', result);
            List<RQO_cls_PedidoAuxiliarBean> listaPedido = (List<RQO_cls_PedidoAuxiliarBean>)JSON.deserialize(result, List<RQO_cls_PedidoAuxiliarBean>.class);
            System.assertEquals(200, listaPedido.size());
        }
    }
    
    /**/   
    
    /*OTROS*/
	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para comprobar la consulta por fechas de entrega de una posición de solicitud
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getFechaEntregaPosicionSolicitud(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSegHist@testorg.com');
        System.runAs(u){
            Date fechaActual = Date.today();
            List<Asset> listaPosicionesSolicitud = RQO_cls_TestDataFactory.createAsset(200);
            //Por defecto se crean como posiciones de pedido, vamos a actualizar los datos para generar posiciones de solicitud
			RecordType recordType = [Select Id from Recordtype where DeveloperName = 'RQO_rt_Posicion_de_solicitud']; 
            for(Integer i= 0; i<listaPosicionesSolicitud.size();i++){
                listaPosicionesSolicitud[i].RecordtypeId = recordType.Id;
			}
            update listaPosicionesSolicitud;
            
            test.startTest();
            	RQO_cls_UtilSeguimientoHistorico util = new RQO_cls_UtilSeguimientoHistorico();
				util.isHistoricInitialData = false;
				util.isInitialData = false; 
            	String result = util.getSeguimientoData(listaPosicionesSolicitud[0].AccountId, 'es_ES', null, null, null, 
                                                        null, String.valueOf(Date.today()), String.valueOf(Date.today()), null, null, null, null);
            	
            
            test.stopTest();            
            System.assertEquals('', result);
        }
    }
    
	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para comprobar la consulta por fechas de entrega de una posición de pedido
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getFechaEntregaPosicionPedido(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSegHist@testorg.com');
        System.runAs(u){
            Date fechaActual = Date.today();
            List<Asset> listaPosicionesSolicitud = RQO_cls_TestDataFactory.createAsset(1);
            
            test.startTest();
            	RQO_cls_UtilSeguimientoHistorico util = new RQO_cls_UtilSeguimientoHistorico();
				util.isHistoricInitialData = false;
				util.isInitialData = false; 
            	String result = util.getSeguimientoData(listaPosicionesSolicitud[0].AccountId, 'es_ES', null, null, null, 
                                                        null, String.valueOf(Date.today()), String.valueOf(Date.today()), null, null, null, null);
            	
            test.stopTest();            
            System.assertEquals('', result);
        }
    }
    
        /**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención un número de posiciones de pedido concreta en la búsqueda inicial de histórico
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getHistorialInitialDataPosicionPedido(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSegHist@testorg.com');
        System.runAs(u){
            List<Asset> listaPosicionesPedido = RQO_cls_TestDataFactory.createAsset(200);
            test.startTest();
            	RQO_cls_UtilSeguimientoHistorico util = new RQO_cls_UtilSeguimientoHistorico();
				util.isHistoricInitialData = true;
				util.isInitialData = false; 
            	String result = util.getHistorialData(listaPosicionesPedido[0].AccountId, 'es_ES', null, 
                                                        listaPosicionesPedido[0].RQO_fld_idRelacion__r.RQO_fld_idExterno__c,
                                                        listaPosicionesPedido[0].RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c, 
                                                        listaPosicionesPedido[0].RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c, null, null, 
                                                        listaPosicionesPedido[0].RQO_fld_positionStatus__c, listaPosicionesPedido[0].RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__c, 
                                                        String.valueOf(Date.today()), 
                                                        String.valueOf(Date.today()));
            test.stopTest();
            
            System.assertNotEquals(null, result);
            System.assertNotEquals('', result);
            List<RQO_cls_PedidoAuxiliarBean> listaPedido = (List<RQO_cls_PedidoAuxiliarBean>)JSON.deserialize(result, List<RQO_cls_PedidoAuxiliarBean>.class);
            System.assertEquals(Integer.valueOf(Label.RQO_lbl_historicoNumeroRegistrosInicial), listaPedido.size());
        }
    }
    /**/
    
}