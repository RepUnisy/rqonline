/**
*   @name: RQO_cls_UserInfo_cc
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la información de un usuario
*/
public without sharing class RQO_cls_UserInfo_cc {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_UserInfo_cc';
    
    /**
    *   @name: RQO_cls_UserInfo_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase que gestiona la información de un usuario
    */
    @AuraEnabled
    public static String getUserInfo(String relativeUrl){
    	
    	// ***** CONSTANTES ***** //
        final String METHOD = 'getUserInfo';
        
        List<Contact> infoContacto = new List<Contact>();
        List<Id> ListIdCliente = new List<Id>();
        
        RQO_cls_WrapperUser user = new RQO_cls_WrapperUser();
        user.name = UserInfo.getName();
        user.language = UserInfo.getLanguage();
        
        User listContact = [SELECT contactid FROM User WHERE id =: Userinfo.getUserid() LIMIT 1];
        
        if (listContact.contactid != null) {
        	Contact contacto = [Select id, RQO_fld_perfilUsuario__c, RQO_fld_perfilCliente__c from Contact where id = : listContact.contactid limit 1];    
            
            if (contacto != null) {
                user.setContacto(contacto);
            }
            
            infoContacto  = [Select Account.Id, Account.Name from Contact where id = :listContact.contactid];
            user.clientes = infoContacto;
            
            for (Contact cont : infoContacto) {
                ListIdCliente.add(cont.Account.Id);
            }
        } 
        
        if (UserInfo.getSessionId() != null) {
	    	Cache.SessionPartition sessionPart = Cache.Session.getPartition('local.RQOcacheSessionCache');
	    	
            if (sessionPart != null && sessionPart.contains('accountIdSelected')) {
				Id accountIdSelected = (Id) sessionPart.get('accountIdSelected');	
				System.debug(CLASS_NAME + ' - ' + METHOD + ': accountIdSelected ' + accountIdSelected);
				
                // Sobreescribir el id cliente con el cliente seleccionado en el listado de cabecera de grupo
                ListIdCliente.clear();
                ListIdCliente.add(accountIdSelected);
	        //	user.setIdCliente(accountIdSelected);
	    	}
        }
        
        if (ListIdCliente.size() != 0) {
            Account cliente = [select id, Name, RQO_fld_calleNumero__c, RQO_fld_idExterno__c, RQO_fld_poblacion__c, RQO_fld_pais__c, RQO_fld_name3__c, RQO_fld_esCliente__c, RQO_fld_groupHeaderEnabled__c from Account where id in :ListIdCliente];
            user.setCliente(cliente);
        }
        
        Set<string> permissionUrls = new RQO_cls_UserProfileSelector().selectUrlsByProfile(user.perfilUsuario());
        
        // Verificar acceso a url restringidas
        if (String.isNotEmpty(relativeUrl)) {
	        System.debug(CLASS_NAME + ' - ' + METHOD + ': relativeUrl ' + relativeUrl);
	        Set<string> restrictedUrls = new RQO_cls_UserProfileSelector().selectRestrictedUrls();
	        
	        if (restrictedUrls != null && restrictedUrls.size() > 0 &&
	        	permissionUrls != null && permissionUrls.size() > 0 &&
	        	restrictedUrls.contains(relativeUrl) && !permissionUrls.contains(relativeUrl))
	        	throw new AuraHandledException('403 Forbidden');
        }
        
        user.setContacto(permissionUrls);
        
        user.setPaisReceptor();
        system.debug('user ' + user);
        return JSON.serialize(user) ;
    }
    
    /**
    *   @name: RQO_cls_UserInfo_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtien euna lista de cuentas de clientes hijos
    */
    @AuraEnabled
    public static List<Account> getChildAccounts(){
    	
    	// ***** CONSTANTES ***** //
        final String METHOD = 'getChildAccounts';
    	
    	List<Account> accounts = new List<Account>();
    	
    	System.debug(CLASS_NAME + ' - ' + METHOD + ': UserInfo.getUserid() ' + UserInfo.getUserid());
    	
    	//Obtener clientes hijos (definidas en relciones de ventas) de usuario actual
    	List<User> users = new RQO_cls_UserSelector().selectById(new Set<ID> { UserInfo.getUserid() });
    	
    	if (users != null && users.size() > 0) {
    		System.debug(CLASS_NAME + ' - ' + METHOD + ': users[0].AccountId ' + users[0].AccountId);
    		
    		List<Account> userAccounts = new RQO_cls_AccountSelector().selectById(new Set<ID> { users[0].AccountId });
    		
    		if (userAccounts != null && userAccounts.size() > 0) {
	    		accounts.add(userAccounts[0]); 

	    		//	Obtenemos sus relaciones de venta        
        		List<RQO_obj_relaciondeVentas__c> relations = [SELECT id,
        													  RQO_fld_idRelacion__r.RQO_fld_idRelacion__r.Id,
        													  RQO_fld_idRelacion__r.RQO_fld_idRelacion__r.Name,
        													  RQO_fld_relacionTipo__c,
                                                              RQO_fld_cliente__c
                                                              FROM RQO_obj_relaciondeVentas__c WHERE RQO_fld_cliente__c = :userAccounts[0].Id ];	
	    		if (relations != null && relations.size() > 0) {
	    			System.debug(CLASS_NAME + ' - ' + METHOD + ': relations.size() ' + relations.size());

		    		for (RQO_obj_relaciondeVentas__c relation : relations) {
		    			Set<Id> accountIds = (new Map<Id,SObject>(accounts)).keySet();
                       
		    			if (relation.RQO_fld_relacionTipo__c == 'Z00000ZG' && !accountIds.contains(relation.RQO_fld_idRelacion__r.RQO_fld_idRelacion__r.Id)){
		    				accounts.add(new Account (Id = relation.RQO_fld_idRelacion__r.RQO_fld_idRelacion__r.Id,
		    										  Name = relation.RQO_fld_idRelacion__r.RQO_fld_idRelacion__r.Name));
		    			}
		    		}
	    		}
	    	}
    	}
    	
    	return accounts;
    }
    
    /**
    *   @name: RQO_cls_UserInfo_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Métpdp que establece una cuenta de usuario mediante el Id de una cuenta
    */
    @AuraEnabled
    public static void setUserAccount(ID accountId){
    	
    	// ***** CONSTANTES ***** //
        final String METHOD = 'setUserAccount';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': accountId ' + accountId);
        
    	// Almacenamos la cuenta seleccionada en sesión
    	Cache.SessionPartition sessionPart = Cache.Session.getPartition('local.RQOcacheSessionCache');
        
    	if (sessionPart != null)
			sessionPart.put('accountIdSelected', accountId);
    }
    
}