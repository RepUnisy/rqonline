/**
*   @name: RQO_cls_InformationRequests_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que testea la clase RQO_cls_InformationRequests
*/
@IsTest
public class RQO_cls_InformationRequests_test {
    
    // ***** CONSTANTES ***** //
    private static final Id FAQ_RECORD_TYPE = 
        Schema.SObjectType.RQO_obj_informationRequest__c.getRecordTypeInfosByName().get('FAQ').getRecordTypeId();
    private static final Id CONTACT_RECORD_TYPE = 
        Schema.SObjectType.RQO_obj_informationRequest__c.getRecordTypeInfosByName().get('Contact Form').getRecordTypeId();
    
    /**
    *   @name: RQO_cls_InformationRequests_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que testea la casuística de añadir un FAW y registrarlo como nuevo en una unidad de trabajo
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @IsTest
    private static void whenAddFaqUowRegisterNew()
    {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        
        System.runAs(u){
            fflib_ApexMocks mocks = new fflib_ApexMocks();
            
            // Given
            fflib_SObjectUnitOfWork mockUow = 
                (fflib_SObjectUnitOfWork) mocks.mock(fflib_SObjectUnitOfWork.class);
            RQO_cls_Application.UnitOfWork.setMock(mockUow);
            Id informationRequestId = 
                fflib_IDGenerator.generate(RQO_obj_informationRequest__c.SObjectType);
            List<RQO_obj_informationRequest__c> infoRequests = 
                new List<RQO_obj_informationRequest__c> {
                    new RQO_obj_informationRequest__c(
                        Id=informationRequestId,
                        Name = 'test@test.com',
                        RQO_fld_email__c = 'test@test.com',
                        RQO_fld_subject__c = 'Test Subject',
                        RQO_fld_message__c = 'Test Message',
                        RecordTypeId = FAQ_RECORD_TYPE
                    )
                        };
                            
                            // When
                            ((RQO_cls_InformationRequests) new RQO_cls_InformationRequests.Constructor().construct(infoRequests)).add(mockUow);
            
            // Then
            ((fflib_SObjectUnitOfWork) mocks.verify(mockUow, 1)).
                registerNew(fflib_Match.sObjectWith(
                    new Map<SObjectField, Object>{
                        RQO_obj_informationRequest__c.Id => informationRequestId,
                            RQO_obj_informationRequest__c.RQO_fld_email__c => 'test@test.com',
                            RQO_obj_informationRequest__c.RQO_fld_subject__c => 'Test Subject',
                            RQO_obj_informationRequest__c.RQO_fld_message__c => 'Test Message',
                            RQO_obj_informationRequest__c.RecordTypeId => FAQ_RECORD_TYPE
                            }
                ));
        }
    }
}