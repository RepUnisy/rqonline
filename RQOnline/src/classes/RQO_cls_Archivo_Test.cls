/**
    *	@name: RQO_cls_Archivo_Test
    *	@version: 1.0
    *	@creation date: 23/01/2018
    *	@author: Alfonso Constan Lopez - Unisys
    *	@description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_Archivo_cc.
    */
@IsTest
public class RQO_cls_Archivo_Test {
	/**
    * @creation date: 23/01/2018
    * @author: Alfonso Constan Lopez - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods.
    * @exception: 
    * @throws: 
    */
	@testSetup static void initialSetup() {
		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(u){
			// Crear email templates RQO_eplt_correoContacto_en
			RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_correoContacto_en'});
		}
	}
	/**
    * @creation date: 23/01/2018
    * @author: Alfonso Constan Lopez - Unisys
    * @description:	Método de test que comprueba la creación de una petición de información.
    * @exception: 
    * @throws: 
    */
	@isTest
    public static void WhenSendRequestRegistersContact() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(u){
            //User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsRequests@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
            List<RQO_obj_informationRequest__c> requests;
            //System.runAs(u){
                test.startTest();
                RQO_cls_Archivo_cc.sendRequest(RQO_cls_Archivo_cc.getPersonTitles()[0], 'Name', 'Surname', 
                                                    RQO_cls_Archivo_cc.getCustomerProfiles()[0],
                                                    'Company Name', '+34', '666666666', '28', 'test@test.test', 
                                                    RQO_cls_Archivo_cc.getInformationTopics()[0], 
                                                    RQO_cls_Archivo_cc.getProducts()[0],
                                                    'Subject', 'Message', false, new List<String>{}, new List<String>{});
        
                requests = [SELECT Id FROM RQO_obj_informationRequest__c WHERE RQO_fld_email__c = 'test@test.test'];
                test.stopTest();
            //}
            
            System.assertNotEquals(0,
                                requests.size(), 
                                'No se ha creado ningún registro.');
		}
 	}
 	/**
    * @creation date: 23/01/2018
    * @author: Alfonso Constan Lopez - Unisys
    * @description:	Método de test que comprueba la generación de excepción cuando se envía un topic inexistente.
    * @exception: 
    * @throws: 
    */
 	@isTest
    public static void WhenSendRequestThrowsException() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(u){
            Boolean DidThrowException = false;
            
            try {
                
                RQO_cls_Archivo_cc.sendRequest('Title', 'Name', 'Surname', 'Profile',
                                                'Company Name', 'Phone Area', 'Phone Number',
                                                'Country Code', 'test@test.test', 'Topic', 'Product',
                                                'Subject', 'Message', false, null, null);
            }
            catch (AuraHandledException e) {
                System.debug(e.getMessage());
                DidThrowException = true;
            }
            
            System.assertEquals(DidThrowException,
                                true, 
                                'No se ha generado el error al enviar datos incorrectos.');
		}
 	}
	/**
    * @creation date: 23/01/2018
    * @author: Alfonso Constan Lopez - Unisys
    * @description:	Método de test que comprueba el listado de titles.
    * @exception: 
    * @throws: 
    */
	@isTest
    public static void WhenGetPersonTitlesReturnsTitles() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(u){
            List<String> values = new List<String>();
            List<SelectOption> options = new RQO_cls_GeneralUtilities().getPersonTitles();
            for (SelectOption option: options)
                values.add(option.getValue());
                
            System.assertEquals(RQO_cls_Archivo_cc.getPersonTitles(),
                                values, 
                                'No se devuelven los titles correctos.');
		}
    }
    
   	/**
    * @creation date: 23/01/2018
    * @author: Alfonso Constan Lopez - Unisys
    * @description:	Método de test que comprueba el listado de productos.
    * @exception: 
    * @throws: 
    */
    @isTest
    public static void WhenGetProductsReturnsProducts() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(u){
            List<String> values = new List<String>();
            List<SelectOption> options = new RQO_cls_GeneralUtilities().getProductList();
            for (SelectOption option: options)
                values.add(option.getValue());
            
            System.assertEquals(RQO_cls_Archivo_cc.getProducts(),
                                values, 
                                'No se devuelven los productos correctos.');
            }
    }
    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constan Lopez - Unisys
    * @description:	Método de test que comprueba el listado de topics.
    * @exception: 
    * @throws: 
    */
    @isTest
    public static void WhenGetInformationTopicsReturnsTopics() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(u){
            List<String> values = new List<String>();
            List<SelectOption> options = new RQO_cls_GeneralUtilities().getInformationTopicList();
            for (SelectOption option: options)
                values.add(option.getValue());
                
            System.assertEquals(RQO_cls_Archivo_cc.getInformationTopics(),
                                values, 
                                'No se devuelven los topics correctos.');
		}
    }
	/**
    * @creation date: 23/01/2018
    * @author: Alfonso Constan Lopez - Unisys
    * @description:	Método de test que comprueba el listado de perfiles.
    * @exception: 
    * @throws: 
    */
	@isTest
    public static void WhenGetCustomerProfilesReturnsProfiles() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(u){
            List<String> values = new List<String>();
            List<SelectOption> options = new RQO_cls_GeneralUtilities().getCustomerProfileList();
            for (SelectOption option: options)
                values.add(option.getValue());
                
            System.assertEquals(RQO_cls_Archivo_cc.getCustomerProfiles(),
                                values, 
                                'No se devuelven los perfiles correctos.');
            }
    }
}