/**
 *	@name: RQO_cls_ConsumptionSearch_cc
 *	@version: 1.0
 *	@creation date: 01/12/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Controlador apex para el componente de filtrado de consumos
 *	@testClass: RQO_cls_ConsumptionSearch_test
*/
public class RQO_cls_ConsumptionSearch_cc {
    
    private static final String CLASS_NAME = 'RQO_cls_ConsumptionSearch_cc';
    
	/**
	* @creation date: 01/12/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Obtiene el listado de meses traducido
	* @param: 
	* @return: List<String>
	* @exception: 
	* @throws: 
	*/    
    @AuraEnabled
    public static Map<String, String> getMonths(){
        final String METHOD = 'getMonths';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Map<String, String> mapaMeses = new Map<String, String>();
        mapaMeses.put('1', Label.RQO_lbl_january);
        mapaMeses.put('2', Label.RQO_lbl_february);
        mapaMeses.put('3', Label.RQO_lbl_march);
        mapaMeses.put('4', Label.RQO_lbl_april);
        mapaMeses.put('5', Label.RQO_lbl_may);
        mapaMeses.put('6', Label.RQO_lbl_june);
        mapaMeses.put('7', Label.RQO_lbl_july);
        mapaMeses.put('8', Label.RQO_lbl_august);
        mapaMeses.put('9', Label.RQO_lbl_september);
        mapaMeses.put('10', Label.RQO_lbl_october);
        mapaMeses.put('11', Label.RQO_lbl_november);
        mapaMeses.put('12', Label.RQO_lbl_december);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return mapaMeses;
    }
    
	/**
	* @creation date: 01/12/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Obtiene el listado de los últimos cinco años
	* @param: 
	* @return: List<String>
	* @exception: 
	* @throws: 
	*/    
    @AuraEnabled
    public static List<String> getYears(){
        final String METHOD = 'getYears';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Integer year = Date.today().year();
        List<String> listYears = new List<String>{String.valueOf(year), String.valueOf(year - 1), String.valueOf(year - 2), String.valueOf(year - 3), String.valueOf(year - 4)};
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
		return listYears;
    }

	/**
	* @creation date: 01/02/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Obtiene el listado de familias
	* @return: Map<String, String>
	* @exception: 
	* @throws: 
	*/    
    @AuraEnabled
    public static Map<String, String> getFamilies(){
        final String METHOD = 'getFamilies';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        Map<String, String> listData = new Map<String, String>();
        for(RQO_cs_sapFamiliesTranslate__c objFamily : [Select name, RQO_fld_label__c from RQO_cs_sapFamiliesTranslate__c Limit 100]){
            listData.put(objFamily.name, objFamily.RQO_fld_label__c);
        }
		
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
		return listData;
    }
}