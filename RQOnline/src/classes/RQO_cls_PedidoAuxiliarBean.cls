/**
*  @name: RQO_cls_WrapperPedido
*  @version: 1.0
*  @creation date: 01/10/2017
*  @author: Alfonso Constan - Unisys
*  @description: Bean Apex para almacenar los datos de pedido obtenidos de diferentes tablas
*
*/
public class RQO_cls_PedidoAuxiliarBean implements Comparable{
    
    public id idSolicitud{get;set;}
	public Id idAsset{get;set;}
    public id idGrado{get;set;}
    public id idEnvase{get;set;}
    public id idDestinoMercancias{get;set;}
    public id idPedido{get;set;}
    public String nameSolicitud{get;set;}
    public String nameAsset{get;set;}
	public String srcFechaPreferente{get;set;}
    public String cantidadFormateada{get;set;}
    public String numEntrega{get;set;}
    public string destinoMercanciasNombre{get;set;}
    public string envaseName{get;set;}
    public string envaseTranslate{get;set;}
    public string numRefCliente{get;set;}
    public String statusCode{get;set;}
    public String statusDescription{get;set;}
    public String statusCssClass{get;set;}
    public String qp0gradeName{get;set;}
    public String numPedido{get;set;}
    public String fechaSolicitado{get;set;}
    public String fechaEntrega{get;set;}
    public String fechaEstado{get;set;}
    public Date fechaCreacion{get;set;}
    public Date fechaPreferente{get;set;}
    public Boolean showAlbaranCMR{get;set;}
    public Boolean showCertificadoAnalisis{get;set;}
    public Boolean cancelable{get;set;}
    public Boolean canceladoView{get;set;}
    public decimal cantidad{get;set;}
    
	/**
    * @creation date: 09/01/2018
    * @author: Alvaro Alonso Trinidad - Unisys
    * @description: implementación del método compareTo para ordenar la lista de objetos
    * @param: compareTo tipo Object
    * @return: Integer
    * @exception: 
    * @throws: 
    */
    public Integer compareTo(Object objToCompare) {
        //Ordenación por defecto por fecha preferente de fecha más reciente a fecha más antigua
        //Si el objeto actual y el objeto a comparar tienen la misma fecha se aplica la ordenación por solicitud y despues por pedido
        if (fechaPreferente == ((RQO_cls_PedidoAuxiliarBean)objToCompare).fechaPreferente){
			String datoSolicToCompare = (!String.isEmpty(((RQO_cls_PedidoAuxiliarBean)objToCompare).nameSolicitud)) ? ((RQO_cls_PedidoAuxiliarBean)objToCompare).nameSolicitud : '';
            String datoSolicActual = (!String.isEmpty(nameSolicitud)) ? nameSolicitud : '';
            
            //Si voy a comparar y es la solicitud actual la que está vacía o tengo la misma solicitud en ambos objetos entonces se miran los números de pedido
            if (datoSolicActual.compareTo(datoSolicToCompare) == 0){
                String datoPedCompare = (!String.isEmpty(((RQO_cls_PedidoAuxiliarBean)objToCompare).numPedido)) ? ((RQO_cls_PedidoAuxiliarBean)objToCompare).numPedido : '';
                String datoPedActual = (!String.isEmpty(numPedido)) ? numPedido : '';
                if (datoPedActual.compareTo(datoPedCompare) > 0) {
                    return -1;
                } else if (datoPedActual.compareTo(datoPedCompare) < 0) {
                    return 1;
                } else {
                    return 0;
                } 
            }else if (datoSolicActual.compareTo(datoSolicToCompare) > 0){
                return -1;
            }else{
                return 1;
            }
        }else if(fechaPreferente > ((RQO_cls_PedidoAuxiliarBean)objToCompare).fechaPreferente){
            return -1;
        }else{
            return 1;
        }
	}
}