/**
*   @name: RQO_cls_informationRequestService
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que realiza el servicio de gestión de las peticiones de información
*	"InformationRequest"
*/
public with sharing class RQO_cls_informationRequestService {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_informationRequestService';
	private static final Id FAQ_RECORD_TYPE = 
		Schema.SObjectType.RQO_obj_informationRequest__c.getRecordTypeInfosByName().get('FAQ').getRecordTypeId();
	private static final Id CONTACT_RECORD_TYPE = 
		Schema.SObjectType.RQO_obj_informationRequest__c.getRecordTypeInfosByName().get('Contact Form').getRecordTypeId();
	private static final String EMAIL_TEMPLATE_FAQS = 'RQO_eplt_correoFaqs_';
	private static final String EMAIL_TEMPLATE_CONTACT = 'RQO_eplt_correoContacto_';
	private static final String DEFAULT_LOCALE = 'en';
	private static final String TOPIC_FAQS = 'Faqs';
	
	/**
	* @creation date: 11/12/2017
	* @author: Unisys
	* @description:	 Método para el registro de una petición de información 
	* @param:	email		dirección de correo del usuario que solicita la información	
    * @param:	message		texto del mensaje asociado a la petición
	* @exception: 
	* @throws: 
	*/
	public static Id registerFaq(String email, String message) 
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'registerFaq';
        
        List<RQO_obj_informationRequest__c> requests = new List<RQO_obj_informationRequest__c>();
        
		RQO_obj_informationRequest__c request = new RQO_obj_informationRequest__c(
													Name = email,
													RQO_fld_email__c = email,
													RQO_fld_subject__c = Label.RQO_lbl_preguntaFaqs,
													RQO_fld_message__c = message,
													RecordTypeId = FAQ_RECORD_TYPE
													);
    
		requests.add(request);

		fflib_ISObjectUnitOfWork uow = RQO_cls_Application.UnitOfWork.newInstance();
        new RQO_cls_InformationRequests(requests).add(uow);
		uow.commitWork();
        
        return request.Id;
	}

	/**
	*   @name: RQO_cls_informationRequestService
	*   @version: 1.0
	*   @creation date: 31/10/2017
	*   @author: Alfonso Constan López - Unisys
	*   @description: Método que registra un contacto
	*/
	public static Id registerContact(String name, String surname, String title,
									 String phoneArea,  String phoneNumber, String companyName,
									 String profile, String email, String countryCode,
									 String topic, String product, String subject, String message) 
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'registerContact';
        
        List<RQO_obj_informationRequest__c> requests = new List<RQO_obj_informationRequest__c>();
        
		RQO_obj_informationRequest__c request = new RQO_obj_informationRequest__c(
													Name = (surname + ', ' + name),
													RQO_fld_personTitle__c = title,
													RQO_fld_name__c = name,
													RQO_fld_surname__c = surname,
													RQO_fld_phoneNumber__c = (phoneArea + ' ' + phoneNumber),
													RQO_fld_companyName__c = companyName,
													RQO_fld_customerProfile__c = profile,
													RQO_fld_email__c = email,
													RQO_fld_countryCode__c = countryCode,
													RQO_fld_topic__c = topic,
													RQO_fld_product__c = product,
													RQO_fld_subject__c = subject,
													RQO_fld_message__c = message,
													RecordTypeId = CONTACT_RECORD_TYPE
													);
    
		requests.add(request);

		fflib_ISObjectUnitOfWork uow = RQO_cls_Application.UnitOfWork.newInstance();
        new RQO_cls_InformationRequests(requests).add(uow);  
        uow.commitWork();
        
        return request.Id;
	} 
	
	/**
	* @creation date: 11/12/2017
	* @author: Unisys
	* @description:	 Método para el envío de correo confirmando una petición de información 
	* @param:	email		dirección de correo del usuario que solicita la información	
    * @param:	message		texto del mensaje asociado a la petición
    * @param:	copyEmail	indica si el usuario quiere que se le mande un correo de confirmación
	* @throws: 	RQO_cls_CustomException		Cuando no se encuentra una plantilla para el correo
	*/
	public static void sendEmail(Boolean copyEmail, Id requestId, List<Blob> bodyArchivos, List<String> nameArchivos) 
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'sendEmail';
        
    	List<RQO_obj_informationRequest__c> requests = 
    		new RQO_cls_InformationRequestSelector().selectById(new Set<Id> { requestId });
    		
    	if (requests.size() > 0)
    	{
    		RQO_obj_informationRequest__c request = requests[0];
        
	        // Seleccionar plantilla de correo
	        EmailTemplate emailTemplate = RQO_cls_informationRequestService.getEmailTemplate(request);
	    		
	        // Seleccionar destinatario
			List<String> emailsTo = RQO_cls_informationRequestService.getAddresses(request, copyEmail);
	        
	        // Envío de correo
	        for (String emailTo: emailsTo)
	        {
		        RQO_cls_informationRequestService.sendEmailWithTemplate(request, emailTemplate, emailTo, 
		        														bodyArchivos, nameArchivos);
        	}
        	
        	System.debug(CLASS_NAME + ' - ' + METHOD + ' emailsTo.size() : ' + emailsTo.size());
        }
    }

   	/**
	*   @name: RQO_cls_informationRequestService
	*   @version: 1.0
	*   @creation date: 31/10/2017
	*   @author: Alfonso Constan López - Unisys
	*   @description: Método que obtiene la plantilla de email de una InformationRequest
	*/
	public static EmailTemplate getEmailTemplate(RQO_obj_informationRequest__c request) 
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'getEmailTemplate';
        
        String templateName = '';
        if (request.RecordTypeId == FAQ_RECORD_TYPE)
        	templateName = EMAIL_TEMPLATE_FAQS;
        else if (request.RecordTypeId == CONTACT_RECORD_TYPE)
        	templateName = EMAIL_TEMPLATE_CONTACT;
        
		String locale = UserInfo.getLanguage().substring(0,2);
        System.debug(CLASS_NAME + ' - ' + METHOD + ' locale : ' + locale);
        EmailTemplate emailTemplate;
		List<EmailTemplate> lstEmailTemplates = 
			new RQO_cls_emailTemplateSelector().selectByDeveloperName(new Set<String> { templateName + locale });
		if (lstEmailTemplates.size() > 0)
		{
			emailTemplate = lstEmailTemplates[0];
		}
		else
		{
			lstEmailTemplates = 
				new RQO_cls_emailTemplateSelector().selectByDeveloperName(new Set<String> { templateName + DEFAULT_LOCALE });
			if (lstEmailTemplates.size() > 0)
				emailTemplate = lstEmailTemplates[0];
			else
				throw new RQO_cls_CustomException('Plantilla de correo inexistente');
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ' emailTemplate.DeveloperName : ' + emailTemplate.DeveloperName);
		System.debug(CLASS_NAME + ' - ' + METHOD + ' emailTemplate.HtmlValue : ' + emailTemplate.HtmlValue);
		
		return emailTemplate;
    }

    /**
	*   @name: RQO_cls_informationRequestService
	*   @version: 1.0
	*   @creation date: 31/10/2017
	*   @author: Alfonso Constan López - Unisys
	*   @description: Método que obtiene una lista de direcciones desde una InformationRequest
	*/  
    private static List<String> getAddresses(RQO_obj_informationRequest__c request, Boolean copyEmail) 
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'getEmailTemplate';
        
        List<String> emailsTo = new List<string>();
        
        String topic = '';
        if (request.RecordTypeId == FAQ_RECORD_TYPE)
        	topic = TOPIC_FAQS;
        else if (request.RecordTypeId == CONTACT_RECORD_TYPE)
        	topic = request.RQO_fld_topic__c;
        
    	List<RQO_cmt_informationMails__mdt> emails = 
	       	new RQO_cls_informationMailsSelector().selectMail(null, null, new Set<String> { topic });
		if (emails.size() > 0)
			emailsTo.add(emails[0].RQO_fld_email__c);
		else
			throw new RQO_cls_CustomException('Dirección de correo inexistente');

    	// Incluir correo al usuario solicitante
	    if (copyEmail)
	    	emailsTo.add(request.RQO_fld_email__c);
	    	
	    return emailsTo;
    }

    /**
	*   @name: RQO_cls_informationRequestService
	*   @version: 1.0
	*   @creation date: 31/10/2017
	*   @author: Alfonso Constan López - Unisys
	*   @description: Envía un correo relativo a una informationRequest 
	*	con una plantilla determinada
	*/  
    private static void sendEmailWithTemplate(RQO_obj_informationRequest__c request, EmailTemplate emailTemplate, string email,
    											list<Blob> bodyArchivos, list<String> nameArchivos) 
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'sendEmailWithTemplate';
        
        List<Messaging.SingleEmailMessage> allMails = new List<Messaging.SingleEmailMessage>();
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new List<String>{email});
		mail.subject = emailTemplate.Subject;
		string body = emailTemplate.HtmlValue;
		if (request.RecordTypeId == FAQ_RECORD_TYPE)
			body = body.replace('{!RQO_obj_informationRequest__c.RQO_fld_message__c}', request.RQO_fld_message__c.replace('\r\n', '<br/>').replace('\r', '<br/>').replace('\n', '<br/>'));
        else if (request.RecordTypeId == CONTACT_RECORD_TYPE)
        {
        	if (request.RQO_fld_personTitle__c == 'Mr')
        		body = body.replace('{!RQO_obj_informationRequest__c.RQO_fld_personTitle__c}', Label.RQO_lbl_personTitleMr);
        	else if (request.RQO_fld_personTitle__c == 'Mrs')
        		body = body.replace('{!RQO_obj_informationRequest__c.RQO_fld_personTitle__c}', Label.RQO_lbl_personTitleMrs);
        	body = body.replace('{!RQO_obj_informationRequest__c.RQO_fld_name__c}', request.RQO_fld_name__c);
        	body = body.replace('{!RQO_obj_informationRequest__c.RQO_fld_surname__c}', request.RQO_fld_surname__c);
			body = body.replace('{!RQO_obj_informationRequest__c.RQO_fld_message__c}', request.RQO_fld_message__c.replace('\r\n', '<br/>').replace('\r', '<br/>').replace('\n', '<br/>'));	
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ' body : ' + body);
        mail.setHtmlBody(body);
		mail.setSaveAsActivity(false);
		// Specify the address used when the recipients reply to the email. 
		mail.setReplyTo('noreply@repsol.com');
		// Specify the name used as the display name.
		mail.setSenderDisplayName('Repsol Química Omline');
		
		if (nameArchivos != null && nameArchivos.size() > 0)
		{
			list<Messaging.EmailFileAttachment> listaArchivos = new list<Messaging.EmailFileAttachment>();
	        
	        // Create the email attachment
	        for(integer i=0; i<nameArchivos.size(); i++){
	            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
	            efa.setFileName(nameArchivos[i]);
	            efa.setBody(bodyArchivos[i]);
	            listaArchivos.add(efa);
	        }
	        
			mail.setFileAttachments(listaArchivos);      
		} 
			
		allMails.add(mail);
		
		Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(allMails); 
		if (resultMail[0].success) {
		    System.debug(CLASS_NAME + ' - ' + METHOD + ' Envío de correo : ' + email);
		} else {
			System.debug(CLASS_NAME + ' - ' + METHOD + ' Error : ' + resultMail[0].errors[0].message);
		}
    }
}