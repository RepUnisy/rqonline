/**
 *	@name: RQO_cls_NotificationTriggerHandler
 *	@version: 1.0
 *	@creation date: 08/02/2018
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase handler con la lógica asociada al trigger para el objeto RQO_obj_notification__c
 *	@testClass: RQO_cls_NotificationTriggerHandler_test
*/
public class RQO_cls_NotificationTriggerHandler {

    private static final String CLASS_NAME = 'RQO_cls_NotificationTriggerHandler';
    private static RQO_cls_NotificationTriggerHandler instance = null;
	private List<Id> contactIdList = null;
    private List<String> accountListExternalId = null;
	//private RQO_obj_notificationConsent__c notificationConsentAuxObj = null;
	private Map<String, RQO_cmt_notificationCategories__mdt> notificationPermission = null;
    
	/**
	* @creation date: 08/02/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método para instanciar el handler desde el trigger 
	* @return: RQO_cls_NotificationTriggerHandler
	* @exception: 
	* @throws: 
	*/   
    public static RQO_cls_NotificationTriggerHandler getInstance(){
        if(instance == null) instance = new RQO_cls_NotificationTriggerHandler();
        return instance;
    }
	
    /**
    * @creation date: 08/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método genérica para realizar la lógica del trigger necesaria antes de insertar el registro
    * @param: listData tipo List<RQO_obj_notification__c> , lista de notificaciones que han hecho saltar el trigger
    * @return: 
    * @exception: 
    * @throws: 
    */
    public void beforeInsertExecution(List<RQO_obj_notification__c> listData){
        final String METHOD = 'beforeInsertExecution';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Recuperamos las listas de ids y el resto de datos iniciales que utilizaremos para realizar consultas
        this.getInitialData(listData);
        //Vamos a buscar cuales son las notificaciones que el contacto actual puede visualizar
		Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>> mapConsent = this.getNotificationConsentPermissions();
        //Recorremos la lista y actualizamos los datos de los registros        
        this.updateBeforeInsertData(listData, mapConsent);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
	/**
    * @creation date: 07/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método genérico para realizar la lógica del trigger necesaria después de insertar el registro
    * @param: listData tipo List<RQO_obj_notification__c> , lista de notificaciones que han hecho saltar el trigger
    * @return: 
    * @exception: 
    * @throws: 
    */
    public void afterInsertExecution(List<RQO_obj_notification__c> listData){
		final String METHOD = 'afterInsertExecution';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Map<String, List<Contact>> mapContactList = null;
        
		//Recuperamos las listas de ids y el resto de datos iniciales que utilizaremos para realizar consultas
        this.getInitialDataAfter(listData);
        //Si tengo lista de cuentas hay que intentar generar notificaciones a partir de ellas
        if (accountListExternalId != null && !accountListExternalId.isEmpty()){
            //Vamos a buscar los contactos de portal asociados a la lista de clientes
            mapContactList = this.getContactData();
        }
        //Recorre la lista de datos y realiza las acciones necesarias
		this.updateAfterInsertData(listData, mapContactList);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
    * @creation date: 08/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método para recuperar los datos iniciales en el trigger de tipo before (listas de id y similares) necesarios para 
    * realizar consultas etc
    * @param: 
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void getInitialData(List<RQO_obj_notification__c> listData){
        final String METHOD = 'getInitialData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        contactIdList = new List<Id>();
        //Recorremos la lista de datos
        for (RQO_obj_notification__c obj : listData){
            //Si tengo contacto y tipo de mensaje de notificación y el tipo de mensaje no es de tipo cuenta añado el id de contacto
            //a la lista. Algunos tipos de mensaje son de tipo cuenta y almacenan el Id de esta en el campo Contact.Por eso no se tratan
            //en este caso
            if (String.isNotBlank(obj.RQO_fld_contact__c) && String.isNotBlank(obj.RQO_fld_messageType__c)
                && !this.isAccountTypeMessagge(obj.RQO_fld_messageType__c)){ 
                contactIdList.add(obj.RQO_fld_contact__c);
            }
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
	/**
    * @creation date: 08/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Comprueba si es una notificación de tipo cuenta. Son de tipo cuenta las de 
    * equipo comercial (Agente comercial, gestor comercial o atención comercial) y las de facturación (Nueva factura, 
    * Factura próxima a vencer, Factura vencida)
    * @param: 
    * @return: 
    * @exception: 
    * @throws: 
    */
    private Boolean isAccountTypeMessagge(String message){
        final String METHOD = 'isAccountTypeMessagge';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Boolean isAccountType = false;
        //Verificamos si es de tipo cuenta
        if (RQO_cls_Constantes.COD_EQU_COMERCIAL.equalsIgnoreCase(message) 
            || RQO_cls_Constantes.COD_EQU_GESTOR_COMERCIAL.equalsIgnoreCase(message)
           	|| RQO_cls_Constantes.COD_EQU_ATENCION_COMERCIAL.equalsIgnoreCase(message)
            || RQO_cls_Constantes.COD_FAC_NEW.equalsIgnoreCase(message)
            || RQO_cls_Constantes.COD_FAC_PROX_VENCIMIENTO.equalsIgnoreCase(message)
            || RQO_cls_Constantes.COD_FAC_VENCIDA.equalsIgnoreCase(message)){

            isAccountType = true;
        }
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return isAccountType;
    }
    
	/**
    * @creation date: 09/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera los permisos de notificación de cada contacto que llega al trigger
    * @param: 
    * @return: Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>> mapa de contactos con los permisos de cada uno
    * @exception: 
    * @throws: 
    */
    private Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>> getNotificationConsentPermissions(){
        final String METHOD = 'getNotificationConsentPermissions';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
        //Llamamos al método genérico de obtención de permisos de notificaciones a partir de una lista de contactos
        RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
        Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>> permisos = util.getValidMessageTypeForContact(contactIdList);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
		return permisos;
    }
	
    /**
    * @creation date: 09/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera los datos de contacto para la lista de IDs de cuenta que 
    * @param: 
    * @return: Map<Id, Contact> mapa de contactos con sus datos
    * @exception: 
    * @throws: 
    */
    private Map<String, List<Contact>> getContactData(){
        final String METHOD = 'getContactData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        //Llamamos al método genérico de obtención de contactos del portal a partir del id de cliente
        Map<String, List<Contact>> mapContactData = RQO_cls_GeneralUtilities.getPortalContactDataByAccountExternalId(accountListExternalId);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
		return mapContactData;
    }
    
	/**
    * @creation date: 09/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Proceso de actualización de los datos de los registros que nos llegan en la lista de triggers
    * @param: listData tipo List<RQO_obj_notification__c>, lista de registros que han desencadenado el trigger
    * @return: mapConsent tipo Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>>, permisos de notificación por contacto
    * @exception: 
    * @throws: 
    */
    private void updateBeforeInsertData(List<RQO_obj_notification__c> listData, Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>> mapConsent){
        final String METHOD = 'updateBeforeInsertData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Recorremos la lista de datos
        for (RQO_obj_notification__c obj : listData){
            //Proceso de actualización de los checks de visualizado y notificado en función de los permisos del contacto
            this.updateNotificationPermissionData(obj, mapConsent);
        }
            
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
    * @creation date: 09/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Actualiza los checks de viusualizado y mail enviado en función de los permisos del contacto. 
    * Si no tiene permisos marcamos como enviado y visualizado
	* Si tiene permisos desmarcamos visualizado y verificamos el campo configurable del objeto de consentimiento para comprobar si envía mail o no
    * @param: objData tipo objData, registros que ha desencadenado el trigger
    * @return: mapConsent tipo Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>>, permisos de notificación por contacto
    * @exception: 
    * @throws: 
    */
    private void updateNotificationPermissionData(RQO_obj_notification__c objData, Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>> mapConsent){
        final String METHOD = 'updateNotificationPermissionData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //notificationConsentAuxObj = null;
        
        //Por defecto no tenemos permisos marco los dos checks notificado y visualizado
        //objData.RQO_fld_mailSent__c = true;
        objData.RQO_fld_visualized__c = true;
        
        //Si no es de tipo cuenta. Los de tipo cuenta generan hijos válidos en el after pero el propio registro no se muestra ni notifica 
        //con lo que se quedarán marcados los checks
        if (!this.isAccountTypeMessagge(objData.RQO_fld_messageType__c)){
            //Comprobamos que permisos tiene el contacto. Si no tiene permisos ambos check marcados
            if (mapConsent != null && !mapConsent.isEmpty() && mapConsent.containsKey(objData.RQO_fld_contact__c)){
                //Recuperamos la lista (mapa) de permisos para el contacto
                notificationPermission = mapConsent.get(objData.RQO_fld_contact__c);
                //Si tiene datos en la lista de permisos para el tipo de notificación actual comprobamos si tiene el check de notificar
                if (notificationPermission != null && !notificationPermission.isEmpty() && notificationPermission.containsKey(objData.RQO_fld_messageType__c)){
                    //Una vez que encuentro el permiso desmarco el visualizado para que se vea por pantalla como nuevo
                    objData.RQO_fld_visualized__c = false;
                    //Vamos a comprobar como está el check de envío de mail
                    //Recuperamos el objeto de notificaciones para el permiso
                    /*notificationConsentAuxObj = notificationPermission.get(objData.RQO_fld_messageType__c);
                    if (notificationConsentAuxObj != null){
                        //Si tengo marcado envío de mail, mail enviado = false, si no lo tengo marcado email enviado = true
                        objData.RQO_fld_mailSent__c = (notificationConsentAuxObj.RQO_fld_emailSending__c != null && notificationConsentAuxObj.RQO_fld_emailSending__c) ? false : true;
                    }*/
                }
            }
		}
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    
    /**
    * @creation date: 08/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método para recuperar los datos iniciales en el trigger de tipo after (listas de id y similares) necesarios para 
    * realizar consultas etc
    * @param: 
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void getInitialDataAfter(List<RQO_obj_notification__c> listData){
        final String METHOD = 'getInitialDataAfter';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        accountListExternalId = new List<String>();
        //Recorremos la lista de datos
        for (RQO_obj_notification__c obj : listData){
            //Algunos tipos de mensaje son de tipo cuenta y almacenan el Id de esta en el campo Contact. Si son de este tipo almacenamos los datos
            //en la lista de cuentas
            if (this.isAccountTypeMessagge(obj.RQO_fld_messageType__c)){
                accountListExternalId.add(obj.RQO_fld_contact__c);
            }
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
        
	/**
    * @creation date: 09/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Proceso de generación/actualización de los datos de los registros que nos llegan en la lista de triggers
    * @param: listData tipo List<RQO_obj_notification__c>, lista de registros que han desencadenado el trigger
    * @param: mapConsent tipo Map<String, List<Contact>>, lista de contactos de portal para cada cuenta
    * @return
    * @exception: 
    * @throws: 
    */
    private void updateAfterInsertData(List<RQO_obj_notification__c> listData,Map<String, List<Contact>> mapContactList){
        final String METHOD = 'updateAfterInsertData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        List<RQO_obj_notification__c> listChilds = null;
        
        System.debug('mapContactList: ' + mapContactList);
        System.debug('listData: ' + listData);
		//Si se han encontrado contactos de portal para los clientes
        if (mapContactList != null && !mapContactList.isEmpty()){
            
            //Recorremos la lista de datos
            for (RQO_obj_notification__c obj : listData){
                //Comprobamos si la notificación actual es de tipo cuenta y de ser así buscamos su lista de contactos y generamos (en memoria) 
                //los registros hijos que corresponda
                if (this.isAccountTypeMessagge(obj.RQO_fld_messageType__c) && mapContactList.containsKey(obj.RQO_fld_contact__c)){
                    if (listChilds == null){listChilds = new List<RQO_obj_notification__c>();}
                    //Montamos un listado de notificaciones a partir de la lista de contactos del cliente y lo añadimos a la lista general de notificaciones
                    //que se va a insertar posteriormente 
                    this.generateChilds(listChilds, mapContactList.get(obj.RQO_fld_contact__c), obj.RQO_fld_messageType__c, obj.RQO_fld_externalId__c);
                }
            }
            //Insertamos en BBDD la lista de notificaciones hijas
            if (listChilds != null && !listChilds.isEmpty()){insert listChilds;}
            
        }  
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }

    /**
    * @creation date: 12/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: A partir de la lista de contactos que llega por parámetro generamos una notificación para cada uno con la tipología que le corresponda 
    * @param: listChilds tipo List<RQO_obj_notification__c>, lista actual de notificaciones que se van a agregar sobre la que se añaden las nuevas
    * @param: mapConsent tipo Map<Id, List<Contact>>, lista de contactos de portal para cada cuenta
    * @param: parentNotificationType tipo String, tipo de notificación padre que va a dar lugar a la generación de los hijos.Con este parámetro sabemos de que tipo
    * son los hijos. 
    * @param:  parentExternalId tipo String, id externo de la notificación padre
    * @return
    * @exception: 
    * @throws: 
    */
    private void generateChilds(List<RQO_obj_notification__c> listChilds, List<Contact> listContact, String parentNotificationType, String parentExternalId){
		final String METHOD = 'generateChilds';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        if (listContact != null && !listContact.isEmpty() && String.isNotEmpty(parentNotificationType)){
            RQO_obj_notification__c notificationAux = null;
            //Recorremos la lista de contactos y generamos las notificaciones en la lista
            for (Contact objContact : listContact){
                notificationAux = new RQO_obj_notification__c(RQO_fld_contact__c = objContact.Id, RQO_fld_externalId__c = parentExternalId);
                this.getChildNotificationData(notificationAux, parentNotificationType);
                listChilds.add(notificationAux);
            }
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }

	/**
    * @creation date: 12/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera el tipo de notificación de un hijo en función del tipo del padre para las notificaciones padre de tipo cuenta
    * @param: parentNotificationType tipo String, tipo de notificación del padre
    * son los hijos. 
    * @return: String
    * @exception: 
    * @throws: 
    */
   	private void getChildNotificationData(RQO_obj_notification__c notificationAux, String parentNotificationType){
        final String METHOD = 'getChildNotificationData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String objectType = '';
        String messageType = '';
        String dynamicField = '';
        
        //Recuperamos el tipo de notificación de los hijos en función del tipo de notificación del padre
        if(RQO_cls_Constantes.COD_EQU_COMERCIAL.equalsIgnoreCase(parentNotificationType)){
			messageType = RQO_cls_Constantes.COD_EQUIPO_COMERCIAL_VIEW;
            objectType = RQO_cls_Constantes.COD_SF_NOT_OTROS;
        }else if (RQO_cls_Constantes.COD_EQU_GESTOR_COMERCIAL.equalsIgnoreCase(parentNotificationType)){
            messageType = RQO_cls_Constantes.COD_EQUIPO_COMERCIAL_VIEW;
            objectType = RQO_cls_Constantes.COD_SF_NOT_OTROS;
        }else if (RQO_cls_Constantes.COD_EQU_ATENCION_COMERCIAL.equalsIgnoreCase(parentNotificationType)){
            messageType = RQO_cls_Constantes.COD_EQUIPO_COMERCIAL_VIEW;
            objectType = RQO_cls_Constantes.COD_SF_NOT_OTROS;
        }else if (RQO_cls_Constantes.COD_FAC_NEW.equalsIgnoreCase(parentNotificationType)){
            messageType = RQO_cls_Constantes.COD_FAC_NEW_VIEW;
            objectType = RQO_cls_Constantes.COD_SF_NOT_FACTURA;
        }else if (RQO_cls_Constantes.COD_FAC_PROX_VENCIMIENTO.equalsIgnoreCase(parentNotificationType)){
            messageType = RQO_cls_Constantes.COD_FAC_PROX_VENCIMIENTO_VIEW;
            objectType = RQO_cls_Constantes.COD_SF_NOT_FACTURA;
        }else if (RQO_cls_Constantes.COD_FAC_VENCIDA.equalsIgnoreCase(parentNotificationType)){
            messageType = RQO_cls_Constantes.COD_FAC_VENCIDA_VIEW;
            objectType = RQO_cls_Constantes.COD_SF_NOT_FACTURA;
        }
        
        notificationAux.RQO_fld_objectType__c = objectType;
        notificationAux.RQO_fld_messageType__c = messageType;
        notificationAux.RQO_fld_dynamicField__c = dynamicField;
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
}