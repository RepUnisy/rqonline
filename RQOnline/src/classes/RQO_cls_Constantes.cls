/**
 *	@name: RQO_cls_Constantes
 *	@version: 1.0
 *	@creation date: 07/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase contenedora de constantes en la aplicacion
 *
 */
public class RQO_cls_Constantes {
   	/**
	 * Valor simbolo
	 */
	public static final String EQ = '=';
	public static final String AMP = '&';
	public static final String HYPHEN = '-';
	public static final String CONCAT = '+';
    public static final String COLON = ':';
    public static final String SPACE = ' ';
    public static final String DOT = '.';
    public static final String ESCAPED_DOT = '\\.';
    public static final String SEMICOLON = ';';
    public static final String COMMA = ',';
    public static final String SLASH = '/';
    public static final String PAD = '#';
    public static final String UNDERSCORE = '_';
    public static final String EMPTY = '';
	public static final String SYMBOL_EURO = '€';
    public static final String SYMBOL_DOLAR = '$';
        
    /**
	 * Valor númerico
	 */
    public static final String NUMBER_0 = '0';
    public static final String NUMBER_1 = '1';
    public static final String NUMBER_2 = '2';
    public static final String NUMBER_3 = '3';
    public static final String NUMBER_4 = '4';
    public static final String NUMBER_5 = '5';
    public static final String NUMBER_6 = '6';
    public static final String NUMBER_7 = '7';
    public static final String NUMBER_8 = '8';
    public static final String NUMBER_9 = '9';
    public static final String NUMBER_01 = '01';
    public static final String NUMBER_02 = '02';
    public static final String NUMBER_03 = '03';
    public static final String NUMBER_04 = '04';
    public static final String NUMBER_05 = '05';
    public static final String NUMBER_06 = '06';
    public static final String NUMBER_07 = '07';
    public static final String NUMBER_08 = '08';
    public static final String NUMBER_09 = '09';
    public static final String NUMBER_10 = '10';
    public static final String NUMBER_11 = '11';
    public static final String NUMBER_12 = '12';
    public static final String NUMBER_13 = '13';
    public static final String NUMBER_14 = '14';
    
	/**
	 * Valor mensual
	 */
    public static final String ENERO = 'Enero';
    public static final String FEBRERO = 'Febrero';
    public static final String MARZO = 'Marzo';
    public static final String ABRIL = 'Abril';
    public static final String MAYO = 'Mayo';
    public static final String JUNIO = 'Junio';
    public static final String JULIO = 'Julio';
    public static final String AGOSTO = 'Agosto';
    public static final String SEPTIEMBRE = 'Septiembre';
    public static final String OCTUBRE = 'Octubre';
    public static final String NOVIEMBRE = 'Noviembre';
    public static final String DICIEMBRE = 'Diciembre';
    
    /**
	 * Codigo Interfaces
	 */
    public static final String WS_PED_SAP = 'WS_Pedidos_SAP';
    public static final String WS_DOCUM_GET = 'DocumentumGetOp';
    public static final String WS_DOCUM_SAP_GET = 'DocumentumSapGetOp';
    public static final String WS_DOCUM_CREATE = 'DocumentumCreateOp';
    public static final String WS_DOCUM_RP2_GET = 'DocumentumRP2GetOp';
    public static final String WS_GET_AGREEMENT = 'SapGetAgreement';
    public static final String WS_GET_SEND_ORDER = 'SapSendOrder';
    public static final String WS_GET_CONSUMPTION = 'Consumos';
    public static final String WS_GET_INVOICES = 'Facturas';
    public static final String WS_GET_INVOICE_DOC = 'DocFacturas';
    public static final String WS_GET_ALBARAN_DOC = 'DocAlbaran';
    public static final String WS_GET_CERTIFICADO_ANALISIS_DOC = 'DocCertificadoAnalisis';
    public static final String WS_GET_BILLS = 'SapSendBills';
    public static final String WS_SAP_SERVICES = 'SapServices';
    
	/**
	 * Listado de codigos de resultado de una peticion
	 */
    public static final String PETICION_ESTADO_PENDIENTE = '202';
    public static final String PETICION_ESTADO_ERROR_RESULTADO = '500';
    public static final String PETICION_ESTADO_ERROR_COMUNICACION = '502';
    public static final String PETICION_RESULTADO_ERR_HTTP = '-1';
    public static final String PETICION_RESULTADO_ERR_CALLOUT = '-2';
    public static final String PETICION_RESULTADO_ERR_EXCEPTION = '-3';
    public static final String PETICION_ESTADO_OK = '1';
    public static final String PETICION_ESTADO_NOK = '2';
    public static final String PETICION_ESTADO_GENERIC = '99';
    public static final String PETICION_RESULTADO_ERR_INTERNAL_SERVER_ERROR = '3';
    public static final String PETICION_RESULTADO_ERR_NOT_FOUND = '4';
    public static final String PETICION_RESULTADO_ERR_TIME_OUT = '5';
    
    /**
	 * Listado de codigos de error para envio de mail
	 */
    public static final String ERR_CONNECTION = '';
    public static final String ERR_MESSAGE = '';
    
    /**
     * Nombre descriptivo de procesos
	 */
    public static final String BATCH_CLIENTES = 'Migración de clientes';
    public static final String BATCH_GRADOS = 'Migración de grados';
    public static final String BATCH_PEDIDOS = 'Migración de pedidos';
    public static final String BATCH_BORRADO_HISTORICO = 'Borrado de registros';
    public static final String BATCH_DOCUMENTOS_RP2 = 'Descarga documentos RP2';
    public static final String BATCH_EMAIL_SENDING = 'Envío de correos';
    public static final String BATCH_LOG_RESULT_OK = 'OK';
    public static final String BATCH_LOG_RESULT_NOK = 'NOK';
    public static final String SERVICE_CREACION_PEDIDOS = 'Creación de pedidos';
    public static final String SERVICE_DOCUMETUM_DIRECT_CALL = 'Obtener id documentum';
    public static final String SERVICE_DOCUMETUM_INDIRECT_CALL = 'Obtener id documento SAP';
    public static final String SERVICE_DOCUMETUM_UPLOAD = 'Carga documento';
    public static final String SERVICE_DOCUMETUM_RP2 = 'Descarga masiva/diaria RP2';
    public static final String SERVICE_GET_AGREEMENTS = 'Obtención acuerdos';
    public static final String SERVICE_SEND_ORDER = 'Envío solicitud';
    public static final String SERVICE_SEND_NOTIFICATION = 'Envío de notificación';
    public static final String SERVICE_SEND_CANCELED_POSITIONS = 'Envío cancelación posiciones solicitud';
    public static final String SERVICE_GET_CONSUMPTION = 'Obtención consumos';
    public static final String SERVICE_CARGA_FACTURAS = 'Carga de facturas';
    public static final String SERVICE_GET_INVOICE = 'Obtención factura';
    public static final String SERVICE_GET_DELIVERY_NOTE = 'Albarán';
    public static final String SERVICE_GET_ANALYSIS_CETIFICATE= 'Obtención certificado de analisis';
    public static final String SCHEDULE_CODE_RP2 = 'DocumentumRP2';
    public static final String SCHEDULE_CODE_CANCEL_REQUEST = 'CancelRequest';
    
    /**
     * Constantes generales para servicios REST
     * 
	*/
    public static final String REST_METHOD_TYPE_GET = 'GET';
    public static final String REST_METHOD_TYPE_POST = 'POST';
    public static final String REST_SOAP_ACTION_KEY = 'SoapAction';
    public static final String REST_CONTENT_TYPE_KEY = 'Content-Type';
    public static final String REST_CONTENT_TYPE_XML = 'text/xml; charset=utf-8';
	public static final String CALLOUT_BUS_NAMED_CREDENTIAL = 'callout:RQO_nmc_NCBusBase';
    public static final String CALLOUT_BUS_NAMED_CREDENTIAL_DOCUMENTUM = 'callout:RQO_nmc_NCBusBaseDocumentum';    
    public static final Integer REST_COD_HTTP_OK = 200;
    public static final Integer REST_COD_HTTP_CREATED = 201;
    public static final Integer REST_COD_HTTP_NOT_FOUND = 404;
    public static final Integer REST_COD_HTTP_TIME_OUT = 504;
    public static final Integer REST_COD_HTTP_INTERNAL_SERVER_ERROR = 500;
    
	/**
    * Constantes generales para servicios SOAP
    * 
    */
    public static final String SOAP_COD_OK = 'OK';
    public static final String SOAP_COD_NOK = 'NOK';
    public static final String SOAP_COD_NO_DATA = 'ND';
    public static final String SOAP_COD_NO_VERIFIED = 'NV';
    
    /**
     * Constantes servicios documentum
     * 
	*/
    public static final String SOAP_REST_SOAP_ACTION_DOCUMENTUM_URN_GET = 'urn:core.services.fs.documentum.emc.com:get';
    public static final String SOAP_REST_SOAP_ACTION_DOCUMENTUM_URN_CREATE = 'urn:core.services.fs.documentum.emc.com:create';
    public static final String SOAP_REST_SOAP_ACTION_DOCUMENTUM_URN_EXECUTE = 'urn:search.services.fs.documentum.emc.com:execute';
    public static final String DOCUMENTUM_ERROR_GENERIC_NOK = 'NOK';
    public static final String DOCUMENTUM_ERROR_GENERIC_NO_DATA = 'ND';
    public static final String DOCUMENTUM_RECOVERY_TYPE_DIRECT= 'D';
    public static final String DOCUMENTUM_RECOVERY_TYPE_VISOR= 'V';
    public static final String DOCUMENTUM_RP2_QUIMICA_CODE= 'RQA';
    public static final String DOC_TYPE_FICHA_TECNICA= 'Ficha técnica';
    public static final String DOC_TYPE_FICHA_SEGURIDAD= 'Ficha Seguridad';
    public static final String DOC_TYPE_FICHA_CERTIFICADO='Certificado de cumplimento';
    public static final String DOC_TYPE_CODE_FICHA_SEGURIDAD= 'FDS';
    public static final String DOC_TYPE_CODE_NIS= 'NIS';
    public static final String DOC_CS_REPOSITORY= 'REPDOCQUIMICA';
    
    /**
     * Constantes generales objeto log 
    */
    public static final String LOG_TYPE_SERVICE= '1';
    public static final String LOG_TYPE_BATCH= '2';
    public static final String LOG_TYPE_GENERIC= '3';
    
    public static final String LOG_NO_APPLY= 'N/A';
    public static final String LOG_DATA_ERROR= 'Data Error';

    public static final String LOG_ORIGIN_SALESFORCE= 'Salesforce';
    public static final String LOG_ORIGIN_DOCUMENTUM= 'Documentum';
    public static final String LOG_ORIGIN_SAP = 'SAP';
    
    /**
     * Constantes generales solicitudes
     */
    public static final String REQ_STATUS_SEND = 'rss';
    public static final String REQ_METER_UNIT_KG = 'kg';
    public static final String REQ_METER_UNIT_TON = 'ton';
    public static final String REQ_WS_CAMION_SI = 'SI';
    public static final String REQ_WS_CAMION_NO = 'N';
    public static final String REQ_STATUS_INPROGRESS = 'IP';
    public static final String REQ_STATUS_SENT = 'SE';
    public static final String REQ_STATUS_CLOSE = 'CL';
    public static final String REQ_STATUS_CANCEL = 'CA';
    public static final String REQ_WS_RESPONSE_OK = 'S';
    public static final String REQ_WS_RESPONSE_ERROR = 'E';
    public static final String REQ_WS_RESPONSE_NO_DATA_ERROR = '000';
    public static final String REQ_WS_RESPONSE_INV_NO_NUMBER = '001';
    public static final String REQ_WS_RESPONSE_INV_INCORRECT = '002';
    public static final String REQ_WS_RESPONSE_INV_ELECTRIC_INVOICE = '003';
    public static final String REQ_WS_RESPONSE_INV_PROBLEM_TO_GET = '004';
    public static final String REQ_WS_RESPONSE_INV_NO_PREVIOUS_INVOICE = '005';
    public static final String REQ_WS_RESPONSE_WARNING = 'W';
    public static final String REQ_WS_RESPONSE_INFO = 'I';
    public static final String REQ_WS_RESPONSE_ABORT = 'A';
    public static final String REQ_WS_PROCESSTYPE_CANCEL = 'Cancel';
    public static final String REQ_WS_PROCESSTYPE_CREATE = 'Create';
    public static final String REQ_WS_CREATE_REQUEST_SAP = 'SO';
    public static final String REQ_WS_CANCEL_REQUEST_SAP = 'CU';
    
    /**
     * Catalogo
     */
	public static final String CAT_WS_TYPE_AGREEMENT = 'A';
    public static final String CAT_WS_TYPE_ORDER = 'P';
    
	/**
    * Pedidos
    */
    public static final String PED_COD_STATUS_SOLICITADO = 'SE';
    public static final String PED_COD_STATUS_CANCELADO = 'CA';
    public static final String PED_COD_STATUS_REGISTRADO = 'R';
    public static final String PED_COD_STATUS_EN_TRATAMIENTO = 'P';
    public static final String PED_COD_STATUS_CONFIRMADO = 'C';
    public static final String PED_COD_STATUS_ENTRANSITO = 'T';
    public static final String PED_COD_STATUS_ENTREGADO = 'E';
    public static final String PED_COD_STATUS_FACTURADO = 'F';
    
	/**
     * CSS de pedidos
     */
    public static final String PED_COD_STATUS_SOLICITADO_CSS_CLASS = 'wrapper-paso paso-1';
    public static final String PED_COD_STATUS_EN_TRATAMIENTO_CSS_CLASS = 'wrapper-paso paso-3';
    public static final String PED_COD_STATUS_REGISTRADO_CSS_CLASS = 'wrapper-paso paso-2';
    public static final String PED_COD_STATUS_CONFIRMADO_CSS_CLASS = 'wrapper-paso paso-4';
    public static final String PED_COD_STATUS_ENTRANSITO_CSS_CLASS = 'wrapper-paso paso-5';
    public static final String PED_COD_STATUS_ENTREGADO_CSS_CLASS = 'wrapper-paso paso-6';
    
    public static final String PED_COD_DOWNLOAD_ALBARAN_CMR = 'DN';
    public static final String PED_COD_DOWNLOAD_CERTIFICADO_ANALISIS = 'AC';
    
	/**
    * Facturas
    */
    public static final String FAC_COD_SEARCH_EMPTY = '001';
    public static final String FAC_COD_COMPENSADA = 'C';
    public static final String FAC_COD_PENDIENTE = 'P';
    public static final String FAC_COD_VENCIDA = 'V';
    public static Boolean FAC_ORDER_BY_FECHA_VENCIMIENTO = false;
    public static Boolean FAC_ORDER_BY_FECHA_EMISION = false;
    
    /**
     * Util
     */
    public static final String USER_GUEST = 'Guest';
    public static final String QUIMICA_PERMISSION_SET = 'RQO_ps_quimicaOnline';
    public static final String COD_CURRENCY_EURO = 'EUR';
    public static final String CONTACT = 'Contact';
    public static final String DATO_CONTACTO = 'RQO_obj_datodeContacto__c';
    public static final String COD_RECORDTYPE_DEVNAME_ACCOUNT_QUIMICA = 'RQO_rt_Quimica';
    public static final String COD_RECORDTYPE_DEVNAME_CONTACT_QUIMICA = 'RQO_rt_Quimica'; 
    public static final String COD_RECORDTYPE_DEVNAME_MAIL_DATO_CONTACTO = 'RQO_rt_Mail';
    public static final String COD_RECORDTYPE_DEVNAME_ASSET_REQUEST_POSITION = 'RQO_rt_Posicion_de_solicitud';
    public static final String COD_RECORDTYPE_DEVNAME_ASSET_ORDER_POSITION ='RQO_rt_Posicion_de_pedido';
    public static final String ENV_GC = 'GC';
	public static final String COD_AREA_VENTAS_10 = '0010';
    public static final String RTID_ASSET_ORDER_POSITION = Label.RQO_lbl_Posiciones_de_Pedidos_RT;
    public static final String RTID_ASSET_REQUEST_POSITION = Label.RQO_lbl_Posicion_de_Solicitud_RT;
    public static final String RQO_lbl_pedidosPer = 'RQO_lbl_pedidosPer';
    public static final String RQO_lbl_entregaConfirmado = 'RQO_lbl_entregaConfirmado';
    public static final String RQO_lbl_mensajeenCurso = 'RQO_lbl_mensajeenCurso';
    public static final String RQO_lbl_verMas = 'RQO_lbl_verMas';
    public static final String PICKLIST_DEFAULT_VALUE = 'none';
    public static final String COD_SAP_IDIOMA_ESPANOL = 'ESP';
	public static final String COD_SAP_NOT_SOLICITUD = 'SOLICITUD';
    public static final String COD_SAP_NOT_PEDIDO = 'PEDIDO';
    public static final String COD_SF_NOT_GRADO = 'PRODUCTO';
    public static final String COD_SF_NOT_DOCUMENTACION = 'DOCUMENTACION';
    public static final String COD_SF_NOT_ENTREGA = 'ENTREGA';
    public static final String COD_SF_NOT_FACTURA = 'FACTURA';
    public static final String COD_SF_NOT_CLIENTE = 'CLIENTE';
    public static final String COD_SF_NOT_COMUNICACION = 'COMUNICACION';
    public static final String COD_SF_NOT_OTROS = 'OTROS';
    public static final String PICKLIST_TODOS_VALUE = 'Todos';
	public static final String CODE_CURRENCY_EURO = 'EUR';
    public static final String CODE_CURRENCY_DOLAR = 'USD';
    public static final String OBJ_NOTIFICATION_ENTREGA = 'Delivery';
	public static final String OBJ_NOTIFICATION_PRODUCTO = 'Product';
    public static final String COD_FILTRO_PEDIDO_SEGUIMIENTO = 'seg';
    public static final String COD_FILTRO_PEDIDO_HISTORIAL = 'his';
    public static final String COD_FILTRO_FACTURAS = 'fac';
    public static final String COD_EXEC_OK = 'OK';
    public static final String COD_EXEC_NOK = 'NOK';
    public static final String LETTERS = '0123456789ABCDEF';
    
    /**
     * Códigos de comunicaciones
     */
    public static final String COD_PED_CONFIRMADO = 'CO';
    public static final String COD_PROD_NOTA_TECNICA = 'NT';
    public static final String COD_PROD_CERT_GRADO = 'CG';
    public static final String COD_PROD_RECOM_GRADO = 'GR';
    public static final String COD_PROD_NEW_GRADO = 'GN';
    public static final String COD_PED_REGISTRADO = 'RE';
    public static final String COD_PED_TRANSITO = 'ET';
    public static final String COD_PED_TRATAMIENTO = 'TR';
    public static final String COD_PED_ENTREGADO = 'EN';
    public static final String COD_FAC_NEW = 'FA';
    public static final String COD_FAC_PROX_VENCIMIENTO = 'FP';
    public static final String COD_FAC_VENCIDA = 'FV';
    public static final String COD_PED_SOLICITADO = 'SO';
    public static final String COD_EQU_COMERCIAL = 'AG';
    public static final String COD_EQU_GESTOR_COMERCIAL = 'GC';
    public static final String COD_EQU_ATENCION_COMERCIAL = 'AT';
    public static final String COD_COM_MULTIMEDIA = 'NM';
    
    public static final String COD_EQUIPO_COMERCIAL_VIEW = 'CT';
    public static final String COD_FAC_NEW_VIEW = 'FN';
    public static final String COD_FAC_PROX_VENCIMIENTO_VIEW = 'FX';
    public static final String COD_FAC_VENCIDA_VIEW = 'FE';
    
	/**
     * Códigos de notificaciones
	*/   
    public static final String COD_NOTIFICATION_CATEGORY_COMMERCIAL = 'Commercial Information';
    public static final String COD_NOTIFICATION_CATEGORY_TECHNICAL = 'Technical Information';
    public static final String COD_NOTIFICATION_CATEGORY_CORPORATE = 'Corporate Information';
    public static final String COD_NOTIFICATION_CATEGORY_SURVEYS = 'Surveys';
    public static final String COD_NOTIFICACION_ICON_CAMBIO_EQUIPO_COMERCIAL = 'CEC';
        
    /**
     * Códigos de relaciones de venta
	*/    
    public static final String RELACION_VENTA_DESTINO_MERCANCIAS = '00000002';
    public static final String RELACION_VENTA_DIRECCION_FACTURACION = '00000003';
    public static final String RELACION_VENTA_RESPONSABLE_STOCK = 'Z00000SB';
    public static final String RELACION_VENTA_CABECERA_GRUPO = 'Z00000ZG';    
    public static final String RELACION_VENTA_AGENTE_COMERCIAL = 'Z0000014';
    public static final String RELACION_VENTA_TECNICO_COMERCIAL = 'Z0000024';
    public static final String RELACION_VENTA_ASISTENCIA_TECNICA = 'Z00000ZE';        
        
    /**
    * Códigos de relaciones de venta
    */   
    public static final String CLIENTE_EXCLUYENTE_0000070005 = '0000070005';
    
	/**
     * Perfiles de usuario
	*/
    public static final String USER_PROFILE_MASTER = 'Máster';
    public static final String USER_PROFILE_NO_CLIENTE = 'No cliente';
    public static final String CONTACT_HELP_AND_SUPPORT = 'HelpAndSupport';
    
    /**
     * Mail Templates
     */
    public static final String MAIL_TMP_ESTADO = 'RQO_eplt_estadoPedido_';
    public static final String MAIL_TMP_TRATAMIENTO = 'RQO_eplt_estadoTratamiento_';
    public static final String MAIL_TMP_NEW_GRADE = 'RQO_eplt_gradoNuevo_';
    public static final String MAIL_TMP_REC_GRADE = 'RQO_eplt_gradoRecomendado_';
    public static final String MAIL_TMP_ACT_NT = 'RQO_eplt_notaTecnica_';
    public static final String MAIL_TMP_ACT_CG = 'RQO_eplt_certificadoGrado_';
    public static final String MAIL_TMP_SOL_PED = 'RQO_eplt_solicitudPedido_';
    public static final String MAIL_TMP_TRATAMIENTO_RESUMEN = 'RQO_eplt_estadoTratamientoResumen_';
    public static final String MAIL_TMP_ESTADO_RESUMEN = 'RQO_eplt_estadoPedidoResumen_';
    public static final String MAIL_TMP_RESUMEN_DIARIO = 'RQO_eplt_resumenDiario_';

    /**
     * Parametros de sustitución de custom mail 
     */
    public static final String TABLE = '{TABLE}';
	public static final String PARAM_1 = '{PARAM1}';
	public static final String PARAM_2 = '{PARAM2}';
	public static final String PARAM_3 = '{PARAM3}';
	public static final String PARAM_4 = '{PARAM4}';
	public static final String PARAM_5 = '{PARAM5}';
	public static final String PARAM_6 = '{PARAM6}';
	public static final String PARAM_7 = '{PARAM7}';
	public static final String PARAM_8 = '{PARAM8}';
	public static final String PARAM_9 = '{PARAM9}';
	public static final String PARAM_10 = '{PARAM10}';
	public static final String PARAM_11 = '{PARAM11}';
	public static final String PARAM_12 = '{PARAM12}';
	public static final String PARAM_13 = '{PARAM13}';
	public static final String PARAM_14 = '{PARAM14}';
	public static final String PARAM_15 = '{PARAM15}';
	public static final String PARAM_16 = '{PARAM16}';
	public static final String PARAM_17 = '{PARAM17}';
    
    /**
     * Locale Language
     */
    public static final String MAIL_LCL_Lang_ES = 'es';
    public static final Map<String, String> languageMap = new Map<String, String>{'es' => 'es', 'fr' => 'fr', 'en' => 'en_US', 'it' => 'it', 'pt' => 'pt', 'de' => 'de'};
    
    /**
     * Test
	*/
    public static final String MOCK_EXEC_OK = 'OK';
    public static final String MOCK_EXEC_NOK = 'NOK';
    public static final String MOCK_EXEC_EXCEPTION = 'EXCEPTION';
    
    /**
     * Valores por defecto para crear usuarios
	*/
    public static final String USER_DEFAULT_EMAIL_ENCONDING_KEY = 'UTF-8';
    
    /**
     * Composición de mail de solicitud
     */
    public static final String TOP_MAIL =   '<table class="rq-solicitud" style="width:95%;margin:15px auto;font-size:14px;">'+
       										'<thead>'+
                                            '<tr><th colspan="9" style="background: #F9F8F7;padding:10px;text-align:left;color: #5c4e44;font-weight: bold;border-top:1px solid #afa8a3;border-bottom:1px solid #afa8a3;">'+
                                            '<span>{PARAM1}</span>'+
                                            '</th></tr>'+
                                            '<tr style="color:#5c4e44; font-weight:bold;">'+
                                            '<th style="padding:10px;text-align:left;width:5%;">'+Label.RQO_lbl_idSolicitud+'</th>'+
                                            '<th style="padding:10px;text-align:left;width:10%;">'+Label.RQO_lbl_numRefCliente+'</th>'+
                                            '<th style="padding:10px;text-align:left;width:15%;">'+Label.RQO_lbl_grado+'</th>'+
                                            '<th style="padding:10px;text-align:left;width:10%;">'+Label.RQO_lbl_envase+'</th>'+
                                            '<th style="padding:10px;text-align:center;width:10%;">'+Label.RQO_lbl_cantidad+'</th>'+
                                            '<th style="padding:10px;text-align:left;width:10%;">'+Label.RQO_lbl_unidadMedida+'</th>'+
                                            '<th style="padding:10px;text-align:left;width:18%;">'+Label.RQO_lbl_fechaPrefEntrega+'</th>'+
                                            '<th style="padding:10px;text-align:left;width:10%;">'+Label.RQO_lbl_incoterm+'</th>'+
                                            '<th style="padding:10px;text-align:center;width:15%;">'+Label.RQO_lbl_cantidadToneladas+'</th>'+
                                            '</tr>'+
                                            '</thead>'+
                                            '<tbody>'+
                                            '<tr>'+
                                            '<td colspan="9" style="border-top:3px solid #ff8200;"></td>'+
                                            '</tr>';

				
	public static final String MIDDLE_MAIL = '<tr>'+
                                                '<td style="padding:10px;">{PARAM2}</td>'+
                                                '<td style="padding:10px;">{PARAM3}</td>'+
                                                '<td style="padding:10px;">{PARAM4}</td>'+
                                                '<td style="padding:10px;">{PARAM5}</td>'+
                                                '<td style="padding:10px;text-align:center;">{PARAM6}</td>'+
                                                '<td style="padding:10px;">{PARAM7}</td>'+
                                                '<td style="padding:10px;">{PARAM8}</td>'+
                                                '<td style="padding:10px;">{PARAM9}</td>'+
                                                '<td style="padding:10px;text-align:center">{PARAM10}</td>'+
                                               '</tr>'+
                                                '<tr>'+
                                                '<td colspan="9" style="border-top:1px solid #e0ded9;"></td>'+
                                                '</tr>';
				
	public static final String BOTTOM_MAIL = '<tr style="background:#FFF7EE;color:#5c4e44;font-weight:bold;">'+
                                                '<td colspan="9" style="border-top:1px solid #afa8a3;border-bottom:1px solid #afa8a3;padding:10px;text-align:right;">'+
                                                '<span>'+Label.RQO_lbl_totalToneladas+RQO_cls_Constantes.COLON+' {PARAM11}</span>'+
                                                '</td>'+
                                                '</tr>'+
                                                '<tr>'+
                                                '<td colspan="9" style="background:#fff;">&nbsp;</td>'+
                                                '</tr>'+
                                                '</tbody>'+
                                                '<tfoot style="color:#5c4e44;">'+
                                                '<tr style="background:#F9F8F7">'+
                                                '<td colspan="5" style="border:1px solid #e0ded9;">'+
                                                '<div class="rq-inline-desk" style="margin:25px;">'+
                                                '<p style="font-weight:bold;font-size:18px;margin-top:0;">'+Label.RQO_lbl_datosEnvio+'</p>'+
                                                '<p style="font-weight:bold;">'+Label.RQO_lbl_direccion+'</p>'+
                                                '<p style="margin:5px 0;">{PARAM12}</p>'+
                                                '<p style="margin:5px 0;">{PARAM13}</p>'+
                                                '<p style="margin:5px 0;">{PARAM14}</p>'+
                                                '</div>'+
                                                '</td>'+
                                                '<td colspan="4" style="border:1px solid #e0ded9;">'+
                                                '<div class="rq-inline-desk" style="margin:25px;">'+
                                                '<p style="font-weight:bold;font-size:18px;margin-top:0;">'+Label.RQO_lbl_datosFacturacion+'</p>'+
                                                '<p style="font-weight:bold;">'+Label.RQO_lbl_direccion+'</p>'+
                                                '<p style="margin:5px 0;">{PARAM15}</p>'+
                                                '<p style="margin:5px 0;">{PARAM16}</p>'+
                                                '<p style="margin:5px 0;">{PARAM17}</p>'+
                                                '</div>'+
                                                '</td>'+
                                                '</tr>'+
                                                '</tfoot>'+
                                                '</table>';
    
	public static final String MIDDLE_MAIL_RESUME = '<tr>'+
                                                    '<td><img style="padding:0 1em;display:block;margin:0 auto;" src="'+Label.RQO_lbl_urlCommunity+'resource/RQO_sr_QuimicaResources/images/icon-calendar-notificaciones.png"/></td>'+
                                                    '<td>'+
                                                    '<p style="color:#5c4e44;font-size:26px;margin:0;">{PARAM1}</p>'+
                                                    '<p style="padding:0.5em 0;">{PARAM2}<b style="padding:0 0.5em;color:#5c4e44;"> {PARAM3} </b> <a href="#" style="margin:0 1em;color:#ff8200;text-decoration:underline;">{PARAM4}</a></p>'+
                                                    '</td>'+
                                                    '</tr>'+
                                                    '<tr>'+
                                                    '<td colspan="2">'+
                                                    '<p style="border-bottom:1px solid #e0ded9;margin:1em 0;">&nbsp;</p>'+
                                                    '</td>'+
                                                    '</tr>';
}