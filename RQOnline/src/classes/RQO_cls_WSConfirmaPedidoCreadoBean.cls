/**
 *	@name: RQO_cls_WSConfirmaPedidoCreadoBean
 *	@version: 1.0
 *	@creation date: 16/10/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean para la deserialización de la petición de SAP (Pedidos).
 */
public class RQO_cls_WSConfirmaPedidoCreadoBean {

    public List<RQO_cls_PedidoBean> pedidosList {get; set;}
    
}