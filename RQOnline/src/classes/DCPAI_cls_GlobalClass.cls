/*------------------------------------------------------------------------
 * 
 * 	@name: 				DCPAI_cls_GlobalClass
 * 	@version: 			1 
 * 	@creation date: 	30/06/2017
 * 	@author: 			Alfonso Constán López	-	Unisys
 * 	@description: 		Clase con funciones globales para la aplicación DCPAI
 * 
----------------------------------------------------------------------------*/
Global class DCPAI_cls_GlobalClass {
    
    /*
     * 	@creation date: 		30/06/2017
     * 	@author: 				Alfonso Constán López	-	Unisys
     *	@description:			Agrupación de ubcs en un mapa para su identificación unívoca. 
     *  @param:					listUbcs: Listado de UBCs para agrupar
     * 	@return:				Mapa de agrupación de UBCs. Key = Budget + Pais + Año + Code
	 */
    public static Map<String, List<ITPM_UBC_Project__c>> DCPAI_getMapUBCs (List<ITPM_UBC_Project__c> listUbcs){
        
        Map<String, List<ITPM_UBC_Project__c>> mapUBC = new Map<String, List<ITPM_UBC_Project__c>>();
        
        for(ITPM_UBC_Project__c ubc : listUbcs){
            String code = ubc.DCPAI_fld_Code_of_project__c;
            if (code == null){
                code = '';
            }
            //	map key: concatenacion del budgetId + pais + año + code
            String clave = ubc.DCPAI_fld_Budget_Investment__c + ubc.ITPM_SEL_Delivery_Country__c + ubc.ITPM_SEL_Year__c + code;
            if (mapUBC.containsKey(clave)){
                List<ITPM_UBC_Project__c> listObt = mapUBC.get(clave);
                mapUBC.remove(clave);
                listObt.add(ubc);                
                mapUBC.put(clave, listObt);
            }
            else{
                List<ITPM_UBC_Project__c> listNew = new List<ITPM_UBC_Project__c>();
                listNew.add(ubc);
                mapUBC.put(clave, listNew);
            }
        }
        system.debug('Lista UBC: ' + mapUBC);
        return mapUBC;
    }
    
    /*
     * 	@creation date: 		30/06/2017
     * 	@author: 				Alfonso Constán López	-	Unisys
     *	@description:			Agrupación de ubcs version en un mapa para su identificación unívoca. 
     *  @param:					listUbcs: Listado de UBCs para agrupar
     * 	@return:				Mapa de agrupación de UBCs. Key = Budget + Pais + Año + Code
	 */
    public static Map<String, List<ITPM_Version__c>> DCPAI_getMapUBCsVersion (List<ITPM_Version__c> listUbcs){
        
        Map<String, List<ITPM_Version__c>> mapUBC = new Map<String, List<ITPM_Version__c>>();
        
        for(ITPM_Version__c ubc : listUbcs){
            String code = ubc.DCPAI_fld_Code_of_project__c;
            if (code == null){
                code = '';
            }
            //	map key: concatenacion del budgetId + pais + año + code
            String clave = ubc.DCPAI_fld_Budget_Version_UBC__C + ubc.ITPM_VER_SEL_Country__c + ubc.ITPM_VER_SEL_Year_validity__c + code;
            if (mapUBC.containsKey(clave)){
                List<ITPM_Version__c> listObt = mapUBC.get(clave);
                mapUBC.remove(clave);
                listObt.add(ubc);                
                mapUBC.put(clave, listObt);
            }
            else{
                List<ITPM_Version__c> listNew = new List<ITPM_Version__c>();
                listNew.add(ubc);
                mapUBC.put(clave, listNew);
            }
        }
        system.debug('Lista UBC: ' + mapUBC);
        return mapUBC;
    }
    
    /*
     * 	@creation date: 		30/06/2017
     * 	@author: 				Alfonso Constán López	-	Unisys
     *	@description:			Comprobación si el usuario que lanza la aplicación tiene asignado el permission set IP_Admin
     * 	@return:				true si se trata de un usuario con el permission set ip_admin
	 */
     webservice static boolean isAdmin(){
         Id IDpermissionSetGPSAdmin = [Select Id from PermissionSet where Name = :Label.DCPAI_Permision_IP_Admin].Id;
        List<PermissionSetAssignment> permisoAdmin = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId() AND PermissionSetId = :IDpermissionSetGPSAdmin];
        
        if (permisoAdmin.size() != 0){
            return true;
        }  
        else{
            return false;
        }
    }
}