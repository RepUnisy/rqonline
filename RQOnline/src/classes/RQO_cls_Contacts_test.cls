/**
*   @name: RQO_cls_Contacts_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_Contacts.
*/
@isTest
public class RQO_cls_Contacts_test {
    /**
    *   @name: RQO_cls_Contacts_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    */
	@testSetup static void initialSetup() {
		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(u){
			RQO_cls_TestDataFactory.createCSGigya();
			RQO_cls_TestDataFactory.createCSSalesforce();
		}
	}
	
    /**
    *   @name: RQO_cls_Contacts_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba que se lanza excepción cuando el email ya está registrado.
    */
    private static testMethod void whenRegisterThrowsDuplicateException() {
		
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		System.runAs(u){
            Boolean DidThrowException = false;
            
            try {
    		//	RQO_cls_Contacts_test.encryptData();
    			List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                test.startTest();
                
                Test.setMock(HttpCalloutMock.class, new RQO_cls_Contacts_test.MockHttpResponseEmailDuplicated());
                ((RQO_cls_Contacts) new RQO_cls_Contacts.Constructor().construct(contact)).register();
                test.stopTest();
            }
            catch (Exception e) {
                DidThrowException = true;
            }
            
            System.assertEquals(true,
                                DidThrowException, 
                                'No se ha generado la excepción esperada.');
        }
    }
    
    /**
    *   @name: RQO_cls_Contacts_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba el correcto funcionamento del registro.
    */
    private static testMethod void whenRegisterThrowsInvalidAccount() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		System.runAs(u){
            Boolean DidThrowException = false;
            
            try {
    		//	RQO_cls_Contacts_test.encryptData();
    			List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                test.startTest();

                Test.setMock(HttpCalloutMock.class, new RQO_cls_Contacts_test.MockHttpResponseInvalidAccount());
                ((RQO_cls_Contacts) new RQO_cls_Contacts.Constructor().construct(contact)).register();
                test.stopTest();
            }
            catch (Exception e) {
                DidThrowException = true;
            }
            
            System.assertEquals(true,
                                DidThrowException, 
                                'No se ha generado la excepción esperada.');
       	}
    }
    
    /**
    *   @name: RQO_cls_Contacts_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba el correcto funcionamento del registro.
    */
    private static testMethod void whenRegisterThrowsNotCreated() {
        
		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		System.runAs(u){
            Boolean DidThrowException = false;
            
            try {
    			//RQO_cls_Contacts_test.encryptData();
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                test.startTest();
                
                Test.setMock(HttpCalloutMock.class, new RQO_cls_Contacts_test.MockHttpResponseEmailNotExist());
                ((RQO_cls_Contacts) new RQO_cls_Contacts.Constructor().construct(contact)).register();
                test.stopTest();
            }
            catch (Exception e) {
                DidThrowException = true;
            }
            
            System.assertEquals(true,
                                DidThrowException, 
                                'No se ha generado la excepción esperada.');
        }
    }
	
    /**
    *   @name: RQO_cls_Contacts_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método mockeado que comprueba que el correo de respuesta esté duplicado
    */
	public class MockHttpResponseEmailDuplicated implements HttpCalloutMock {
	    // Implement this interface method
	    public HTTPResponse respond(HTTPRequest req) {
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody(' {' +
							  '"results": [' +
							  '  {' +
							  '    "data": {' +
							  '      "Service": {' +
							  '        "Quimica": "true"' +
							  '      }' +
							  '    }' +
							  '  }],' +
							  '"objectsCount": 1,' +
							  '"totalCount": 1,' +
							  '"statusCode": 200,' +
							  '"errorCode": 0,' +
							  '"statusReason": "OK",' +
							  '"callId": "ed334ae1f5f0437a97fd9e03069f91d9",' +
							  '"time": "2018-02-28T09:39:06.972Z"' +
							'}');
	        res.setStatusCode(200);
	        return res;
	    }
	}
    
    /**
    *   @name: RQO_cls_Contacts_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método auxiliar de encriptación de datos
    */
    private static void encryptData() {
        Blob cryptoKey = Crypto.generateAesKey(256);
        Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey, Blob.valueOf('sampleData'));
        
        RQO_cs_salesforceKeyEncriptacion__c salesforceKey = new RQO_cs_salesforceKeyEncriptacion__c (Name = 'RQO_cs_salesforceKeyEncriptacion__c', RQO_fld_keyEncriptacion__c = EncodingUtil.base64Encode(cryptoKey));
    	RQO_cs_salesforceIntegracion__c salesforceData = new RQO_cs_salesforceIntegracion__c (Name = 'RQO_cs_salesforceIntegracion__c', 
                                                                                              RQO_fld_apiUrl__c = EncodingUtil.base64Encode(encryptedData),
                                                                                              RQO_fld_user__c = EncodingUtil.base64Encode(encryptedData),
                                                                                              RQO_fld_password__c = EncodingUtil.base64Encode(encryptedData),
                                                                                              RQO_fld_secret__c = EncodingUtil.base64Encode(encryptedData),
                                                                                              RQO_fld_apiKey__c = EncodingUtil.base64Encode(encryptedData));
    	RQO_cs_gigyaKeyEncriptacion__c gigyaKey = new RQO_cs_gigyaKeyEncriptacion__c (Name = 'RQO_cs_gigyaKeyEncriptacion__c', RQO_fld_keyEncriptacion__c = EncodingUtil.base64Encode(cryptoKey));
    	RQO_cs_gigyaIntegracion__c gigyaData = new RQO_cs_gigyaIntegracion__c (Name = 'RQO_cs_gigyaIntegracion__c', 
                                                                               RQO_fld_secret__c = EncodingUtil.base64Encode(encryptedData),
                                                                               RQO_fld_userKey__c = EncodingUtil.base64Encode(encryptedData),
                                                                               RQO_fld_apiKeyAutorizados__c = EncodingUtil.base64Encode(encryptedData));
    }
	
    /**
    *   @name: RQO_cls_Contacts_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método mockeado que testea la respuesta HTTP a una cuenta inválida
    */
	public class MockHttpResponseInvalidAccount implements HttpCalloutMock {
	    // Implement this interface method
	    public HTTPResponse respond(HTTPRequest req) {
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody(' {' +
							  '"results": [' +
							  '  {' +
							  '    "data": {' +
							  '      "Service": {' +
							  '        "GLP": "true"' +
							  '      }' +
							  '    }' +
							  '  }],' +
							  '"objectsCount": 1,' +
							  '"totalCount": 1,' +
							  '"statusCode": 200,' +
							  '"errorCode": 0,' +
							  '"statusReason": "OK",' +
							  '"callId": "ed334ae1f5f0437a97fd9e03069f91d9",' +
							  '"time": "2018-02-28T09:39:06.972Z",' +
							  '"id":"https://login.salesforce.com/id/00D50000000IZ3ZEAW/00550000001fg5OAAQ",' +
							  '"issued_at":"1296509381665",' +
							  '"instance_url":"https://na1.salesforce.com",' +
							  '"signature":"+Nbl5EOl/DlsvUZ4NbGDno6vn935XsWGVbwoKyXHayo=",' +
							  '"access_token":"00D50000000IZ3Z!AQgAQH0Yd9M51BU_rayzAdmZ6NmT3pXZBgzkc3JTwDOGBl8BP2AREOiZzL_A2zg7etH81kTuuQPljJVsX4CPt3naL7qustlb"' +
							'}');
	        res.setStatusCode(200);
	        return res;
	    }
	}
	
    /**
    *   @name: RQO_cls_Contacts_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método mockeado que testea que el email de respuesta no existe
    */
	public class MockHttpResponseEmailNotExist implements HttpCalloutMock {
	    // Implement this interface method
	    public HTTPResponse respond(HTTPRequest req) {
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody(' {' +
							  '"results": [' +
							  '  {' +
							  '    "data": {' +
							  '      "Service": {' +
							  '        "GLP": "true"' +
							  '      }' +
							  '    }' +
							  '  }],' +
							  '"objectsCount": 0,' +
							  '"totalCount": 1,' +
							  '"statusCode": 200,' +
							  '"errorCode": 0,' +
							  '"statusReason": "OK",' +
							  '"callId": "ed334ae1f5f0437a97fd9e03069f91d9",' +
							  '"time": "2018-02-28T09:39:06.972Z",' +
							  '"id":"https://login.salesforce.com/id/00D50000000IZ3ZEAW/00550000001fg5OAAQ",' +
							  '"issued_at":"1296509381665",' +
							  '"instance_url":"https://na1.salesforce.com",' +
							  '"signature":"+Nbl5EOl/DlsvUZ4NbGDno6vn935XsWGVbwoKyXHayo=",' +
							  '"access_token":"00D50000000IZ3Z!AQgAQH0Yd9M51BU_rayzAdmZ6NmT3pXZBgzkc3JTwDOGBl8BP2AREOiZzL_A2zg7etH81kTuuQPljJVsX4CPt3naL7qustlb"' +
							'}');
	        res.setStatusCode(200);
	        return res;
	    }
	}
}