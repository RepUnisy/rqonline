/**
*   @name: RQO_cls_SAMLJitHandler
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona SAMLJit
*/

// This class provides logic for inbound just-in-time provisioning of single sign-on users in
// your Salesforce organization.
global class RQO_cls_SAMLJitHandler implements Auth.SamlJitHandler {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_SAMLJitHandler';
	
    private class JitException extends Exception{}
    /**
    *   @name: RQO_cls_SAMLJitHandler
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico de gestión de usuario
    */
    private void handleUser(boolean create, User u, Map<String, String> attributes,
        					String federationIdentifier, boolean isStandard) {
        						
    	// ***** CONSTANTES ***** //
        final String METHOD = 'handleUser';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': create ' + create);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': u ' + u);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': attributes ' + attributes);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': federationIdentifier ' + federationIdentifier);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': isStandard ' + isStandard);
        
		boolean updateUser = false;

        if(create && attributes.containsKey('User.UserName')) {
        	System.debug(CLASS_NAME + ' - ' + METHOD + ': User.UserName ' + attributes.get('User.UserName'));
            u.Username = attributes.get('User.UserName');
        }
        if(create) {
            if(attributes.containsKey('Identity')) {
            	System.debug(CLASS_NAME + ' - ' + METHOD + ': Identity ' + attributes.get('Identity'));
                u.FederationIdentifier = attributes.get('Identity');
            } else {
                u.FederationIdentifier = federationIdentifier;
            }
        }
        if(attributes.containsKey('User.Email') && !String.isEmpty(attributes.get('User.Email'))) {
        	if (u.Email != attributes.get('User.Email'))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': u.Email ' + u.Email);
            	u.Email = attributes.get('User.Email');
            	updateUser = true;
        	}
        }
        if(attributes.containsKey('User.FirstName') && !String.isEmpty(attributes.get('User.FirstName'))) {
        	if (u.FirstName != attributes.get('User.FirstName'))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': u.FirstName ' + u.FirstName);
            	u.FirstName = attributes.get('User.FirstName');
            	updateUser = true;
        	}
        }
        if(attributes.containsKey('User.LastName') && !String.isEmpty(attributes.get('User.LastName'))) {
			if (u.LastName != attributes.get('User.LastName'))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': u.LastName ' + u.LastName);
            	u.LastName = attributes.get('User.LastName');
            	updateUser = true;
        	}
        }
        
        if(create) {
            String alias = '';
            if(u.FirstName == null) {
                alias = u.LastName;
            } else {
                alias = u.FirstName.charAt(0) + u.LastName;
            }
            if(alias.length() > 5) {
                alias = alias.substring(0, 5);
            }
            u.Alias = alias;
	       	u.Emailencodingkey = RQO_cls_Constantes.USER_DEFAULT_EMAIL_ENCONDING_KEY;
	       	// Mapear desde el campo de idioma
	       	u.languagelocalekey = 'es';
	       	u.localesidkey = 'es';
	       	u.timezonesidkey = UserInfo.getTimeZone().getID();
	       	u.CommunityNickname = u.Username.substring(0, u.Username.indexOf('@')) +
	       							u.Username.substring(u.Username.indexOf('@'), u.Username.lastIndexOf('.'));
        }
        
        /*
         * If you are updating Contact or Account object fields, you cannot update the following User fields at the same time.
         * If your identity provider sends these User fields as attributes along with Contact 
         * or Account fields, you must modify the logic in this class to update either these 
         * User fields or the Contact and Account fields.
         */
        if(attributes.containsKey('User.ProfileID') && !String.isEmpty(attributes.get('User.ProfileID'))) {
        	System.debug(CLASS_NAME + ' - ' + METHOD + ': User.ProfileID ' + attributes.get('User.ProfileID'));
            String profileId = attributes.get('User.ProfileID');
            Profile p = [SELECT Id FROM Profile WHERE Name=:profileId];
            if (p != null && u.ProfileId != p.Id)
            {
            	u.ProfileId = p.Id;
            	updateUser = true;
            }
        }

        //Handle custom fields here
        if(!create && updateUser) {
        	System.debug(CLASS_NAME + ' - ' + METHOD + ': Modificar usuario ' + u);
            update(u);
        }
    }

    /**
    *   @name: RQO_cls_SAMLJitHandler
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico de gestión de contacto
    */
    private void handleContact(boolean create, String accountId, User u, Map<String, String> attributes) {
    	
    	// ***** CONSTANTES ***** //
        final String METHOD = 'handleContact';
        
        Contact c;
        boolean newContact = false;
        boolean updateContact = false;
        
        if(create) {
    		c = new Contact();
    		c.AccountId = accountId;
    		newContact = true;
        }
        else
        {
        	List<Contact> contacts = new RQO_cls_ContactSelector().selectById(new Set<Id> { u.ContactId });
	        if (contacts.size() > 0)
	    		c = contacts[0];	
	    	else
	    		throw new JitException('No existe Contact con id' + u.ContactId);
        }

        if(attributes.containsKey('Contact.Email') && !String.isEmpty(attributes.get('Contact.Email'))) {
        	if (c != null && c.Email != attributes.get('Contact.Email'))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.Email ' + c.Email);
            	c.Email = attributes.get('Contact.Email');
            	updateContact = true;
        	}
        }
        if(attributes.containsKey('User.FirstName') && !String.isEmpty(attributes.get('User.FirstName'))) {
            if (c != null && c.FirstName != attributes.get('User.FirstName'))
        	{
            	System.debug(CLASS_NAME + ' - ' + METHOD + ': c.FirstName ' + c.FirstName);
            	c.FirstName = attributes.get('User.FirstName');
            	updateContact = true;
        	}
        }
        if(attributes.containsKey('Contact.LastName') && !String.isEmpty(attributes.get('Contact.LastName'))) {
            if (c != null && c.LastName != attributes.get('Contact.LastName'))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.LastName ' + c.LastName);
            	c.LastName = attributes.get('Contact.LastName');
            	updateContact = true;
        	}
        }
        if(attributes.containsKey('Contact.MobilePhone') && !String.isEmpty(attributes.get('Contact.MobilePhone'))) {
        	if (c != null && c.MobilePhone != attributes.get('Contact.MobilePhone'))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.MobilePhone ' + c.MobilePhone);
            	c.MobilePhone = attributes.get('Contact.MobilePhone');
            	updateContact = true;
        	}
        }
        if(attributes.containsKey('Contact.RQO_fld_perfilCliente__c') && !String.isEmpty(attributes.get('Contact.RQO_fld_perfilCliente__c'))) {
			if (c != null && c.RQO_fld_perfilCliente__c != attributes.get('Contact.RQO_fld_perfilCliente__c'))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.RQO_fld_perfilCliente__c ' + c.RQO_fld_perfilCliente__c);
            	c.RQO_fld_perfilCliente__c = attributes.get('Contact.RQO_fld_perfilCliente__c');
            	
            	// Si no viene identificador de cliente se le asignar el perfil de usuario "No cliente"
            	if(!attributes.containsKey('Account.RQO_fld_idExterno__c') || String.isEmpty(attributes.get('Account.RQO_fld_idExterno__c')))
            		c.RQO_fld_perfilUsuario__c = RQO_cls_Constantes.USER_PROFILE_NO_CLIENTE;

            	updateContact = true;
        	}
        }
        if(attributes.containsKey('Contact.RQO_fld_pais__c') && !String.isEmpty(attributes.get('Contact.RQO_fld_pais__c'))) {
        	String pais = attributes.get('Contact.RQO_fld_pais__c');
        	if (pais == 'Spain')
        		pais = 'España';
			if (c != null && c.RQO_fld_pais__c != pais)
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.RQO_fld_pais__c ' + c.RQO_fld_pais__c);
            	c.RQO_fld_pais__c = 'España';
            	updateContact = true;
        	}	
        }
        if(attributes.containsKey('Contact.RQO_fld_idioma__c') && !String.isEmpty(attributes.get('Contact.RQO_fld_idioma__c'))) {
			if (c != null && c.RQO_fld_idioma__c != attributes.get('Contact.RQO_fld_idioma__c'))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.RQO_fld_idioma__c ' + c.RQO_fld_idioma__c);
            	c.RQO_fld_idioma__c = attributes.get('Contact.RQO_fld_idioma__c');
            	updateContact = true;
        	}
        }
		if(attributes.containsKey('Contact.RQO_fld_corpInfConsent__c') && !String.isEmpty(attributes.get('Contact.RQO_fld_corpInfConsent__c'))) {
			if (c != null && c.RQO_fld_corpInfConsent__c != Boolean.valueOf(attributes.get('Contact.RQO_fld_corpInfConsent__c')))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.RQO_fld_corpInfConsent__c ' + c.RQO_fld_corpInfConsent__c);
            	c.RQO_fld_corpInfConsent__c = Boolean.valueOf(attributes.get('Contact.RQO_fld_corpInfConsent__c'));
            	updateContact = true;
        	}
        }
        if(attributes.containsKey('Contact.RQO_fld_commInfConsent__c') && !String.isEmpty(attributes.get('Contact.RQO_fld_commInfConsent__c'))) {
			if (c != null && c.RQO_fld_commInfConsent__c != Boolean.valueOf(attributes.get('Contact.RQO_fld_commInfConsent__c')))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.RQO_fld_commInfConsent__c ' + c.RQO_fld_commInfConsent__c);
            	c.RQO_fld_commInfConsent__c = Boolean.valueOf(attributes.get('Contact.RQO_fld_commInfConsent__c'));
            	updateContact = true;
        	}
        }
		if(attributes.containsKey('Contact.RQO_fld_techInfConsent__c') && !String.isEmpty(attributes.get('Contact.RQO_fld_techInfConsent__c'))) {
			if (c != null && c.RQO_fld_techInfConsent__c != Boolean.valueOf(attributes.get('Contact.RQO_fld_techInfConsent__c')))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.RQO_fld_techInfConsent__c ' + c.RQO_fld_techInfConsent__c);
            	c.RQO_fld_techInfConsent__c = Boolean.valueOf(attributes.get('Contact.RQO_fld_techInfConsent__c'));
            	updateContact = true;
        	}
        }
        if(attributes.containsKey('Contact.RQO_fld_surveyConsent__c') && !String.isEmpty(attributes.get('Contact.RQO_fld_surveyConsent__c'))) {
			if (c != null && c.RQO_fld_surveyConsent__c != Boolean.valueOf(attributes.get('Contact.RQO_fld_surveyConsent__c')))
        	{
        		System.debug(CLASS_NAME + ' - ' + METHOD + ': c.RQO_fld_surveyConsent__c ' + c.RQO_fld_surveyConsent__c);
            	c.RQO_fld_surveyConsent__c = Boolean.valueOf(attributes.get('Contact.RQO_fld_surveyConsent__c'));
            	updateContact = true;
        	}
        }
		
        if(newContact) {
        	System.debug(CLASS_NAME + ' - ' + METHOD + ': Crear contacto ' + c);
            insert(c);
            u.ContactId = c.Id;
        } else if (updateContact) {
        	System.debug(CLASS_NAME + ' - ' + METHOD + ': Modificar contacto ' + c);
            update(c);
        }
    }
    
    /**
    *   @name: RQO_cls_SAMLJitHandler
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico de gestión de cuenta
    */
    private String handleAccount(boolean create, User u, Map<String, String> attributes) {
        
        // ***** CONSTANTES ***** //
        final String METHOD = 'handleAccount';
        
        Account a;
        boolean newAccount = false;
        if(create) {
            if(attributes.containsKey('Account.RQO_fld_idExterno__c') && !String.isEmpty(attributes.get('Account.RQO_fld_idExterno__c'))) {
            	String accountCTC = attributes.get('Account.RQO_fld_idExterno__c');
            	List<Account> accounts = new RQO_cls_AccountSelector().selectByCTC(new Set<String> { accountCTC });
            	if (accounts.size() > 0) {
            		a = accounts[0];
            		newAccount = false;
            	}
            	else
            		throw new JitException('No hay Account para el campo RQO_fld_idExterno__c');
			} else {
				System.debug(CLASS_NAME + ' - ' + METHOD + ' El campo RQO_fld_idExterno__c no está en el SAML Assertion');
				// Crear  account para el usuario
				String accountName = '';
				if(attributes.containsKey('Contact.FirstName') && !String.isEmpty(attributes.get('Contact.FirstName')))
					accountName = attributes.get('Contact.FirstName');
				if(attributes.containsKey('Contact.LastName') && !String.isEmpty(attributes.get('Contact.LastName')))
					accountName = accountName + ' ' + attributes.get('Contact.LastName');
				a = new Account(Name=accountName);
				RecordType quimicaRecordType =  
						[SELECT Id FROM RecordType WHERE DeveloperName = 'RQO_rt_Quimica' AND SObjectType = 'Account'];
				a.RecordType = quimicaRecordType;
				newAccount = true;
            }
        }
        else
        {
        	List<Account> accounts = new RQO_cls_AccountSelector().selectById(new Set<Id> { u.AccountId });
        	if (accounts.size() > 0) {
        		a = accounts[0];
        		newAccount = false;
        	}
        	else
        		throw new JitException('No hay Account para el campo u.AccountId');
        }
        
        if(newAccount) {
            insert(a);
        }
        
        return a.Id;
    }
    
    /**
    *   @name: RQO_cls_SAMLJitHandler
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de conversión de un account a un PersonAccount
    */
	private void convertToPersonAccount(boolean create, String accountId, User u, Map<String, String> attributes) {
        
        // ***** CONSTANTES ***** //
        final String METHOD = 'convertToPersonAccount';
        
        if(create) {
            if(attributes.containsKey('Account.RQO_fld_idExterno__c') && !String.isEmpty(attributes.get('Account.RQO_fld_idExterno__c'))) {
            	System.debug(CLASS_NAME + ' - ' + METHOD + ': No es personal account ');
			} else {
				Account a = [SELECT Id FROM Account WHERE Id = :accountId];
				if (a != null) {
            		RecordType personAccountRecordType =  
						[SELECT Id FROM RecordType WHERE DeveloperName = 'RQO_rt_personalAccount' AND SObjectType = 'Account' AND IsPersonType=True];
					a.RecordTypeId = personAccountRecordType.Id;
					System.debug(CLASS_NAME + ' - ' + METHOD + ': Convertir a personal account ' + a);
					update a;
            	}
            	else
            		throw new JitException('No hay Account para el campo RQO_fld_idExterno__c');
            }
        }
    }
    
    /**
    *   @name: RQO_cls_SAMLJitHandler
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico de manejo de Jit
    */
    private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        	
       	// ***** CONSTANTES ***** //
        final String METHOD = 'handleJit';
	
        if(communityId != null) {
        	String account = handleAccount(create, u, attributes);
            handleContact(create, account, u, attributes);
            handleUser(create, u, attributes, federationIdentifier, false);
            convertToPersonAccount(create, account, u, attributes);
        }
    }

    /**
    *   @name: RQO_cls_SAMLJitHandler
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico de creación de usuario
    */
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        	
        // ***** CONSTANTES ***** //
        final String METHOD = 'createUser';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': samlSsoProviderId ' + samlSsoProviderId);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': communityId ' + communityId);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': portalId ' + portalId);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': federationIdentifier ' + federationIdentifier);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': attributes ' + attributes);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': assertion ' + assertion);
        
        User u = new User();
        handleJit(true, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        return u;
    }
    
    /**
    *   @name: RQO_cls_SAMLJitHandler
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico de actualización de usuario
    */
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        	
        // ***** CONSTANTES ***** //
        final String METHOD = 'updateUser';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': userId ' + userId);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': samlSsoProviderId ' + samlSsoProviderId);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': communityId ' + communityId);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': portalId ' + portalId);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': federationIdentifier ' + federationIdentifier);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': attributes ' + attributes);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': assertion ' + assertion);
        	
        List<User> users = new RQO_cls_UserSelector().selectById(new Set<Id> { userId });
        if (users.size() > 0)
    		handleJit(false, users[0], samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
    	else
    		throw new JitException('No existe User con id' + userId);
    }
}