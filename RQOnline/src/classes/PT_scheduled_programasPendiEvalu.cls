/*------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    
Description:    
Clase encargada de comprobar si en la fecha actual finaliza el periodo de vigencia de algún programa de patrocinio
y de ser así, cambia automáticamente el estado del patrocinio a "Programa pendiente evaluacion". 
 
Test Class:     PT_test_scheduled_progPendiEvalu
History
<Date>          <Author>        <Change Description>
10-10-2015      Rubén Simarro   Initial Version
22-04-2016      Rubén Simarro   Se optimiza la consulta añadiendo al where la condicion
------------------------------------------------------------*/

global class PT_scheduled_programasPendiEvalu implements Schedulable {
 
    global void execute(SchedulableContext SC) {
        
        try{     
  
            system.debug('@@'+DateTime.now().format()+' Proceso automático comprueba fin vigencias patrocinios @@');   
           
             Patrocinios__c[] patrocinios = [select Id, fecha_de_fin__c 
                                             From Patrocinios__c 
                                             where estado__c ='Programa en curso' and fecha_de_fin__c=TODAY];
                        
             for(Patrocinios__c patAux: patrocinios ){
                 patAux.estado__c='Pendiente evaluacion';            
             }
            
            update(patrocinios);    
                
            system.debug('@@ FIN proceso automático comprueba fin vigencias patrocinios @@@@');          
        }
        catch(Exception e){    
            system.debug('@@ ERROR PT_scheduled_programasPendiEvalu: '+e.getMessage());
        }    
    }
   
}