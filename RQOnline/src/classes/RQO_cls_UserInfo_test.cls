/**
*	@name: RQO_cls_UserInfo_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: Alfonso Constán López - Unisys
*	@description: Clase de test correspondiente a RQO_cls_UserInfo_test
*/
@isTest
public class RQO_cls_UserInfo_test {
    private static List<Account> accountList;
    private static id idAccount;
    private static List<Contact> contactList;
    private static id idContacto;
    private static List<RQO_obj_areadeVentas__c> salesAreaList;
    private static List<RQO_obj_relaciondeVentas__c> salesRelationshipList;
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método de creación del conjunto de datos necesarios para la ejecución del test
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    @testSetup
    public static void initialSetup() {
        User user = RQO_cls_TestDataFactory.userCreation('sfTestUserEvents@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(user){
             RQO_cls_TestDataFactory.createAccount(200);
            
            contactList = RQO_cls_TestDataFactory.createContact(200);
            idContacto = contactList[0].id;
            User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserAgreement@testorg.com', 'Customer Community User', null,  'es', 'es_ES', 'Europe/Berlin', idContacto);
            
            createContacts();
            // Create sales areas
            createSalesAreas();
           
        }
    }
    
    /**
    *	@creation date: 9/2/2018
    *	@author: Alfonso Constán López - Unisys
    *   @description: Método para la creación de contactos
    *   @exception: 
    *   @throws: 
    */
    private static void createContacts() {
        
        List<contact> listcontact = [select id, AccountId from contact where id = : idContacto];
        idAccount = listcontact[0].AccountId;
    }
    
    /**
    *	@creation date: 9/2/2018
    *	@author: Alfonso Constán López - Unisys
    *   @description: Método para la creación de relaciones de ventas
    *   @exception: 
    *   @throws: 
    */
    private static void createSalesAreas() {
        List<RQO_obj_relaciondeVentas__c> listRelacionesCreado = RQO_cls_TestDataFactory.createRelaciondeVentas(200);
       
        
        List<RQO_obj_relaciondeVentas__c> listRelaciones = [select id, RQO_fld_cliente__c, RQO_fld_idRelacion__c, RQO_fld_relacionTipo__c from RQO_obj_relaciondeVentas__c where id in :listRelacionesCreado];
         
        for (integer i= 0; i< listRelaciones.size(); i++){
            listRelaciones[i].RQO_fld_cliente__c = idAccount;
            if (math.mod(i,6) == 0){
                listRelaciones[i].RQO_fld_relacionTipo__c = RQO_cls_Constantes.RELACION_VENTA_DESTINO_MERCANCIAS;
            }else{
                if (math.mod(i,6) == 1){
                    listRelaciones[i].RQO_fld_relacionTipo__c = RQO_cls_Constantes.RELACION_VENTA_DIRECCION_FACTURACION;
                }
                else{
                    if (math.mod(i,6) == 2){
                        listRelaciones[i].RQO_fld_relacionTipo__c = RQO_cls_Constantes.RELACION_VENTA_AGENTE_COMERCIAL;
                    }
                    else{
                        if (math.mod(i,6) == 3){
                            listRelaciones[i].RQO_fld_relacionTipo__c = RQO_cls_Constantes.RELACION_VENTA_CABECERA_GRUPO;
                        }
                        else{
                            if (math.mod(i,6) == 4){
                                listRelaciones[i].RQO_fld_relacionTipo__c = RQO_cls_Constantes.RELACION_VENTA_RESPONSABLE_STOCK;
                            }
                            else{
                                if (math.mod(i,6) == 5){
                                    listRelaciones[i].RQO_fld_relacionTipo__c = 'Z00000ZG';
                                }
                                
                            }
                        }                                               
                    }
                }
                
            }
            
        }
        update listRelaciones;
        
        List<RQO_obj_areadeVentas__c> listAreaVenta = [select id, RQO_fld_idRelacion__c from RQO_obj_areadeVentas__c];
        List<Account> listAccount = [select id from Account where id != : idAccount];
        listAreaVenta[0].RQO_fld_idRelacion__c = listAccount[0].id;
    //    update listAreaVenta;
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método que testea la obtención de información de un usuario
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    @isTest
    public static void testGetUserInfo() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            
            test.startTest();
            String userId = RQO_cls_UserInfo_cc.getUserInfo('/relativeurl');
            RQO_cls_WrapperUser user = (RQO_cls_WrapperUser)JSON.deserialize(userId, RQO_cls_WrapperUser.class);
            
            System.assertEquals(true, user.isPrivate());
            test.stopTest();
		}
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método que testea la obtención de cuentas hijo
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    @isTest
    public static void testGetChildAccounts() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            test.startTest();
            List<Account> listAccount = RQO_cls_UserInfo_cc.getChildAccounts();            
            
            System.assertNotEquals(0, listAccount.size());
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método que testea el establecimiento de una cuenta de usuario
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    @isTest
    public static void testSetUserAccount() {
    	User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            
            Boolean DidThrowException = false;
            
            try {
            	test.startTest();
            	RQO_cls_UserInfo_cc.setUserAccount(null);            
            	test.stopTest();
            }
            catch (Exception e) {
                DidThrowException = true;
            }
            
            System.assertEquals(true,
                                DidThrowException, 
                                'No se ha generado una excepción.');
        }
    }
       
    
}