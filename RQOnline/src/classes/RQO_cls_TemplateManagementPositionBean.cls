/**
 *	@name: RQO_cls_TemplateManagementPositionBean
 *	@version: 1.0
 *	@creation date: 20/12/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de Posicion de Plantilla.
 */
public class RQO_cls_TemplateManagementPositionBean {

    public String identificador {get; set;}
    public String grado {get; set;}
    public String gradoName {get; set;}
    public String gradoQP0 {get; set;}
    public String envase {get; set;}
    public Integer cantidad {get; set;}
    public String unidadMedida {get; set;}
    public String destino {get; set;}
    public String incoterm {get; set;}
    public String facturacion {get; set;}
    public integer fechasEntrega {get; set;}
    public List<String> envasesList {get; set;}
    
}