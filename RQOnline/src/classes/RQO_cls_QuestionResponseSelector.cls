/**
*   @name: RQO_cls_QuestionResponseSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la selección de preguntas
*/
public class RQO_cls_QuestionResponseSelector extends RQO_cls_ApplicationSelector {

    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_QuestionResponseSelector';
    
    /**
    *   @name: RQO_cls_QuestionResponseSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la lista de campos de una respuesta a una pregunta
    */
    public List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            RQO_obj_questionResponse__c.Name,
            RQO_obj_questionResponse__c.RQO_fld_nameEn__c,
            RQO_obj_questionResponse__c.RQO_fld_nameFr__c,
            RQO_obj_questionResponse__c.RQO_fld_nameDe__c,
            RQO_obj_questionResponse__c.RQO_fld_namePt__c,
            RQO_obj_questionResponse__c.RQO_fld_nameIt__c,
            RQO_obj_questionResponse__c.RQO_fld_question__c,
            RQO_obj_questionResponse__c.RQO_fld_position__c,
            RQO_obj_questionResponse__c.RQO_fld_selected__c};
    }

    /**
    *   @name: RQO_cls_QuestionResponseSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el tipo de objeto de questionResponse
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_questionResponse__c.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_QuestionResponseSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar las questionResponse
    */
    public override String getOrderBy()
    {
        return 'RQO_fld_question__c, RQO_fld_position__c';
    }

    /**
    *   @name: RQO_cls_QuestionResponseSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una questionResponse mediante un conjuntos de ids
    */
    public List<RQO_obj_questionResponse__c> selectById(Set<ID> idSet)
    {
         return (List<RQO_obj_questionResponse__c>) selectSObjectsById(idSet);
    } 
}