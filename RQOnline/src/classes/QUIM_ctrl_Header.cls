public class QUIM_ctrl_Header {
    
    public static List<ccrz__E_Menu__c> menus {get; set;}
    public static List<ccrz__E_Menu__c> menusRigth {get; set;}
    public static List<ccrz__E_MenuI18N__c> menus18 {get; set;}
    public static List<ccrz__E_MenuItem__c> items {get; set;}
    public static List<ccrz__E_MenuItemI18N__c> items18 {get; set;}
    // Parámetro GET idioma
    public static String parametroIdioma {get;set;}
    // Lengua a utilizar
    public static String language {get; set;}
    
    public QUIM_ctrl_Header() {
        System.debug('Entrando QUIM_ctrl_Header()');
        parametroIdioma = ApexPages.currentPage().getParameters().get('cclcl');
        //System.debug(System.LoggingLevel.INFO, 'parametroIdioma: ' + parametroIdioma);
        if (parametroIdioma != NULL && parametroIdioma.length()>1) {
            language = parametroIdioma;
        }
        else {
        	language = ApexPages.currentPage().getHeaders().get('Accept-Language').left(2);
        }
        //System.debug(System.LoggingLevel.INFO, 'language: ' + language);
        
        String langLike = language + '%';
        //System.debug('langLike: ' + langLike);
        try{
            
            //Menú izquierda
            menus = [SELECT Id,ccrz__URL__c FROM ccrz__E_Menu__c WHERE ccrz__LocationType__c = 'Header' ORDER BY ccrz__Sequence__c] ;
            menus18 = [SELECT Id,ccrz__Menu__c,ccrz__DisplayName__c FROM ccrz__E_MenuI18N__c WHERE ccrz__Language__c LIKE :langLike] ;
            //system.debug(menus18.size());
            if (menus18.isEmpty()){
                menus18 = [SELECT Id,ccrz__Menu__c,ccrz__DisplayName__c FROM ccrz__E_MenuI18N__c WHERE ccrz__Language__c LIKE 'en%'] ;
            }
            //system.debug(menus18.size());
            
            items = [SELECT Id,ccrz__Menu__c,ccrz__ParentMenuItem__c,ccrz__URL__c FROM ccrz__E_MenuItem__c ORDER BY ccrz__Sequence__c] ;
            items18 = [SELECT Id,ccrz__MenuItem__c,ccrz__DisplayName__c FROM ccrz__E_MenuItemI18N__c WHERE ccrz__Language__c LIKE :langLike] ;
            if (items18.isEmpty()){
                items18 = [SELECT Id,ccrz__MenuItem__c,ccrz__DisplayName__c FROM ccrz__E_MenuItemI18N__c WHERE ccrz__Language__c LIKE 'en%'] ;
            }
            
            //Menú derecha Rigth Nav
            menusRigth = [SELECT Id,ccrz__URL__c FROM ccrz__E_Menu__c WHERE ccrz__LocationType__c = 'Right Nav' ORDER BY ccrz__Sequence__c] ;
           
            
        }catch (Exception e) {
        	System.debug(System.LoggingLevel.WARN, 'EXCEPTION = ' + e);
        }
        /*
        system.debug(menus18.size());
        integer i;
        
        for(i=0;i<menus.size();i++){
            system.debug('menus='+menus[i]);
        }
        for(i=0;i<menus18.size();i++){
            system.debug('menus18='+menus18[i]);
        }
        for(i=0;i<items.size();i++){
            system.debug('items='+items[i]);
        }
        for(i=0;i<items18.size();i++){
            system.debug('items18='+items18[i]);
        }
        system.debug(menus18.size());
		*/
	}
}// class