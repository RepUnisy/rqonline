global class QUIM_Update_Masivo {
	

	public static void UP_todo(){

		list <String> Cat_name= new list<string>{'Copolímero bloque PP','Copolímero random PP','Homopolímero PP','Compuesto PP'};
		
		list <String> Cat_name2= new list<string>{'PEBD','mPELBD','PEAD','EVA', 'EBA'};
		
		ccrz__E_Spec__c Spec_generica = especificaciones('Ind. de Fluidez g/10 min');

		
		inserccion(especificaciones('Indice de Fluidez g/10min(230º/2,16 kg)'), Esp_product(Cat_name, Spec_generica));

		inserccion(especificaciones('Indice de Fluidez g/10 min (190º/2,16 kg)'), Esp_product(Cat_name2, Spec_generica));

		
	}
	
	public static ccrz__E_Spec__c especificaciones (String nombre){

		ccrz__E_Spec__c Spec_aux = [SELECT id, ccrz__DisplayName__c FROM ccrz__E_Spec__c WHERE ccrz__DisplayName__c =: nombre];
		System.debug(Spec_aux);
		return Spec_aux;	
	}
	
	// MODIFICAR LIMITES
	public static list<ccrz__E_ProductSpec__c> Esp_product (list<String> Cat_name, ccrz__E_Spec__c Spec_generica){
		List <ccrz__E_Category__c> categorias = [SELECT id FROM ccrz__E_Category__c WHERE ccrz__CategoryID__c in :Cat_name];
		
		//System.debug('categorias:  ' + categorias);

		List <ccrz__E_Product__c> Grados = [SELECT id, Name FROM ccrz__E_Product__c WHERE id in (SELECT ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__c in :categorias)];
		
		//System.debug('Grados:    ' + Grados);
		
		List <ccrz__E_ProductSpec__c> Spec_product = [SELECT id, ccrz__Spec__c, ccrz__SpecValue__c, ccrz__Product__c FROM ccrz__E_ProductSpec__c WHERE ccrz__Product__c in :Grados AND ccrz__Spec__c =: Spec_generica.id];
		
		//System.debug('Spec_product:   ' +Spec_product);
		return Spec_product;

	}

	public static void inserccion( ccrz__E_Spec__c Spec_Correcta, List <ccrz__E_ProductSpec__c> Spec_product){

		List <ccrz__E_ProductSpec__c> Spec_Insert = new list<ccrz__E_ProductSpec__c>();

		for(ccrz__E_ProductSpec__c specif : Spec_product){ 

			ccrz__E_ProductSpec__c aux = new ccrz__E_ProductSpec__c(
				ccrz__SpecValue__c = specif.ccrz__SpecValue__c,
				ccrz__Product__c = specif.ccrz__Product__c,
				ccrz__Spec__c = Spec_Correcta.id
			);

			Spec_Insert.add(aux);
		}
		//System.debug('Spec_Insert:   ' +Spec_Insert);
		
		insert Spec_Insert;
		delete Spec_product;
	}

}