/*------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    
Clase encargada de calcular valores automaticamente de la ficha de Evaluacion, como son el valor del campo Alcanzados_criterios__c y Alcanzado_objetivos__c
Test Class:     PT_test_evaluacion_handler
History
<Date>          <Author>        <Change Description>
10-05-2016      Rubén Simarro   Initial Version
------------------------------------------------------------*/
public class PT_evaluacion_handler {

    public static String calculaAlcanzadoCriterios(boolean Liderazgo,boolean Innovacion,boolean Etica,boolean Calidad,boolean Futuro_de_la_energia,String Alcanzado_conocimiento,String alcanzado_innovacion,String alcanzado_desarrollo,String alcanzado_Calidad,String Alcanzado_futuro, boolean circunstancia_excepcional, string justificacion_excepcional){      
                     
        System.debug('@@@ PT_evaluacion_handler calculaAlcanzadoCriterios()');

        integer criteriosRellenados = 0; 
        integer totalValoradosSi = 0;
        integer totalValoradosParcialmente = 0;
        integer totalValoradosNo = 0;
               
        String alcanzadosCriterios;
              
            if(Liderazgo && Alcanzado_conocimiento != null){
                   
                criteriosRellenados++;
                                     
                if (Alcanzado_conocimiento=='Si'){
                        totalValoradosSi++;
                    }
                    else if (Alcanzado_conocimiento=='Parcialmente'){ 
                        totalValoradosParcialmente++;
                    }
                    else if (Alcanzado_conocimiento=='No'){ 
                        totalValoradosNo++;
                    }
                }
               
        if(Innovacion && alcanzado_innovacion != null){
                   
                    criteriosRellenados++;
                   
                    if (alcanzado_innovacion=='Si'){
                        totalValoradosSi++;
                    }
                    else if (alcanzado_innovacion=='Parcialmente'){ 
                        totalValoradosParcialmente++;
                    }
                    else if (alcanzado_innovacion=='No'){ 
                        totalValoradosNo++;
                    }
                }
               
        if(Etica && alcanzado_desarrollo != null){
                   
                    criteriosRellenados++;
                    
                    if (alcanzado_desarrollo=='Si'){
                        totalValoradosSi++;
                    }
                    else if (alcanzado_desarrollo=='Parcialmente'){ 
                        totalValoradosParcialmente++;
                    }
                    else if (alcanzado_desarrollo=='No'){ 
                        totalValoradosNo++;
                    }
                }
            
        if(Calidad && alcanzado_Calidad != null){
                    
                    criteriosRellenados++;
                    
                    if (alcanzado_Calidad=='Si'){
                        totalValoradosSi++;
                    }
                    else if (alcanzado_Calidad=='Parcialmente'){ 
                        totalValoradosParcialmente++;
                    }
                    else if (alcanzado_Calidad=='No'){ 
                        totalValoradosNo++;
                    }
                }
              
        if(Futuro_de_la_energia && Alcanzado_futuro != null){
                   
                    criteriosRellenados++;     
                  
                    if (Alcanzado_futuro=='Si'){
                        totalValoradosSi++;
                    }
                    else if (Alcanzado_futuro=='Parcialmente'){ 
                        totalValoradosParcialmente++;
                    }
                    else if (Alcanzado_futuro=='No'){ 
                        totalValoradosNo++;
                    }
                }


        /**/
     
        /**/      
            
        if(criteriosRellenados == 5){
                
            System.debug('@@@ PT_evaluacion_handler 5 criterios evaluados: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);

            if(totalValoradosSi == 5){
                alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==4 && totalValoradosParcialmente==1){
                 alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==3 && totalValoradosParcialmente==2){
                 alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==2 && totalValoradosParcialmente==3){
                 alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==1 && totalValoradosParcialmente==4){
                 alcanzadosCriterios='Parcialmente';
            }  
            else if(totalValoradosSi ==4 && totalValoradosNo==1){
                 alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==3 && totalValoradosNo==2){
                 alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosSi ==2 && totalValoradosNo==3){
                 alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosSi ==1 && totalValoradosNo==4){
                 alcanzadosCriterios='No';
            } 
            else if(totalValoradosSi ==3 && totalValoradosNo==1 && totalValoradosParcialmente==1){
                 alcanzadosCriterios='Parcialmente';
            } 
            else if(totalValoradosSi ==2 && totalValoradosNo==2 && totalValoradosParcialmente==1){
                 alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosSi ==2 && totalValoradosNo==1 && totalValoradosParcialmente==2){
                 alcanzadosCriterios='Parcialmente';
            } 
            else if(totalValoradosSi ==1 && totalValoradosNo==3 && totalValoradosParcialmente==1){
                 alcanzadosCriterios='No';
            }
            else if(totalValoradosSi ==1 && totalValoradosParcialmente==3 && totalValoradosNo==1){
                 alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosParcialmente==5){
                 alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosParcialmente==4 && totalValoradosNo==1){
                 alcanzadosCriterios='Parcialmente';
            }  
            else if(totalValoradosParcialmente==3 && totalValoradosNo==2){
                 alcanzadosCriterios='No';
            }
             else if(totalValoradosParcialmente==2 && totalValoradosNo==3){
                 alcanzadosCriterios='No';
            }
             else if(totalValoradosParcialmente==1 && totalValoradosNo==4){
                 alcanzadosCriterios='No';
            }
             else if(totalValoradosNo==5){
                 alcanzadosCriterios='No';
            }
            else{
                System.debug('@@@ PT_evaluacion_handler casuística no contemplada: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
                alcanzadosCriterios='Si';
            }
        }          
        else if(criteriosRellenados == 4){
                         
            System.debug('@@@ PT_evaluacion_handler 4 criterios evaluados: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);

            if(totalValoradosSi == 4){
                alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==3 && totalValoradosParcialmente==1){
                 alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==2 && totalValoradosParcialmente==2){
                 alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==1 && totalValoradosParcialmente==3){
                 alcanzadosCriterios='Parcialmente';
            }   
            else if(totalValoradosSi ==3 && totalValoradosNo==1){
                 alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==2 && totalValoradosNo==2){
                 alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosSi ==1 && totalValoradosNo==3){
                 alcanzadosCriterios='No';
            } 
            else if(totalValoradosSi ==2 && totalValoradosNo==1 && totalValoradosParcialmente==1){
                 alcanzadosCriterios='Parcialmente';
            } 
            else if(totalValoradosSi ==1 && totalValoradosParcialmente==3){
                 alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosParcialmente==4){
                 alcanzadosCriterios='Parcialmente';
            } 
            else if(totalValoradosParcialmente==3 && totalValoradosNo==1){
                 alcanzadosCriterios='Parcialmente';
            }  
            else if(totalValoradosParcialmente==2 && totalValoradosNo==2){
                 alcanzadosCriterios='Parcialmente';
            }
             else if(totalValoradosParcialmente==1 && totalValoradosNo==3){
                 alcanzadosCriterios='No';
            }     
             else if(totalValoradosNo==4){
                 alcanzadosCriterios='No';
            }
            else{
                System.debug('@@@ PT_evaluacion_handler casuística no contemplada: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
                alcanzadosCriterios='Si';
            }
               
        } 
        else if(criteriosRellenados == 3){
           
            System.debug('@@@ PT_evaluacion_handler 3 criterios evaluados: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);

            if(totalValoradosSi == 3){
                alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==2 && totalValoradosParcialmente==1){
                 alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==1 && totalValoradosParcialmente==2){
                 alcanzadosCriterios='Si';
            }           
            else if(totalValoradosSi ==2 && totalValoradosNo==1){
                 alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosSi ==1 && totalValoradosNo==2){
                 alcanzadosCriterios='No';
            } 
            else if(totalValoradosSi ==1 && totalValoradosNo==1 && totalValoradosParcialmente==1){
                 alcanzadosCriterios='Parcialmente';
            } 
            else if(totalValoradosParcialmente==3){
                 alcanzadosCriterios='Parcialmente';
            } 
            else if(totalValoradosParcialmente==2 && totalValoradosNo==1){
                 alcanzadosCriterios='Parcialmente';
            }  
            else if(totalValoradosParcialmente==1 && totalValoradosNo==2){
                 alcanzadosCriterios='No';
            }  
            else if(totalValoradosNo==3){
                 alcanzadosCriterios='No';
            }
            else{
                System.debug('@@@ PT_evaluacion_handler casuística no contemplada: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
                alcanzadosCriterios='Si';
            }
             
        }     
        else if(criteriosRellenados == 2){
                  
            System.debug('@@@ PT_evaluacion_handler 2 criterios evaluados: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);

            if(totalValoradosSi == 2){
                alcanzadosCriterios='Si';
            }
            else if(totalValoradosSi ==1 && totalValoradosParcialmente==1){
                 alcanzadosCriterios='Si';
            }             
            else if(totalValoradosSi ==1 && totalValoradosNo==1){
                 alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosParcialmente == 2){
                alcanzadosCriterios='Parcialmente';
            }
            else if(totalValoradosParcialmente ==1 && totalValoradosNo==1){
                 alcanzadosCriterios='No';
            } 
            else if(totalValoradosNo==2){
                 alcanzadosCriterios='No';
            } 
            else{
                System.debug('@@@ PT_evaluacion_handler casuística no contemplada: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);        
                alcanzadosCriterios='Si';
            }
               
        }  
        else if(criteriosRellenados == 1){
           
            System.debug('@@@ PT_evaluacion_handler 1 criterio evaluado: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
            
            if(totalValoradosSi == 1){
                alcanzadosCriterios='Si';
            }
            else if(totalValoradosParcialmente ==1){
                 alcanzadosCriterios='Parcialmente';
            }             
            else if(totalValoradosNo==1){
                 alcanzadosCriterios='No';
            } 
            else{
                System.debug('@@@ PT_evaluacion_handler casuística no contemplada: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
                alcanzadosCriterios='Si';
            }
        }
        else{
            System.debug('@@@ PT_evaluacion_handler no hay ningun criterio evaluado Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
            alcanzadosCriterios=null;
        }
        
        if(circunstancia_excepcional){
            alcanzadosCriterios='Si';
        }
        
        system.debug('Excepcional  ' + alcanzadosCriterios);
        
        return alcanzadosCriterios;
    }   
   
    
    public static String calculaAlcanzadoObjetivos(boolean objetivo_incrementar_ventas,boolean objetivo_mejorar_atributos_de_imagen,boolean objetivo_otros, String Alcanzado_objetivo_imagen, String Alcanzado_objetivo_ventas){      
                     
        System.debug('@@@ PT_evaluacion_handler calculaAlcanzadoObjetivos()');

        integer objetivosRellenados = 0; 
        integer totalValoradosSi = 0;
        integer totalValoradosParcialmente = 0;
        integer totalValoradosNo = 0; 
       
        String alcanzadosObjetivos;      
           
        if(objetivo_incrementar_ventas && Alcanzado_objetivo_ventas != null ){
                   
                objetivosRellenados++;
                 //
                 System.Debug('************************Alcanzado_objetivo_ventas:   ' + Alcanzado_objetivo_ventas);
                 //                  
                    if (Alcanzado_objetivo_ventas == 'Si'){
                        totalValoradosSi++;
                    }
                    else if (Alcanzado_objetivo_ventas == 'Parcialmente'){ 
                        totalValoradosParcialmente++;
                    }
                    else if (Alcanzado_objetivo_ventas == 'No'){ 
                        totalValoradosNo++;
                    }
                }
               
        if(objetivo_mejorar_atributos_de_imagen && Alcanzado_objetivo_imagen != null){
                   
                    objetivosRellenados++;
                   //
                     System.Debug('************************Alcanzado_objetivo_imagen :   ' + Alcanzado_objetivo_imagen );
                   //
                   
                    if (Alcanzado_objetivo_imagen =='Si'){
                        totalValoradosSi++;
                    }
                    else if (Alcanzado_objetivo_imagen =='Parcialmente'){ 
                        totalValoradosParcialmente++;
                    }
                    else if (Alcanzado_objetivo_imagen =='No'){ 
                        totalValoradosNo++;
                    }
             
        }
        
                     
        if(objetivosRellenados == 2){
                  
            System.debug('@@@ PT_evaluacion_handler 2 objetivos evaluados: kkkk Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
            
            System.debug('HOLA '+ (totalValoradosSi == 2));
            
            if(totalValoradosSi == 2){
                alcanzadosObjetivos='Si';
                
            }
            else if(totalValoradosSi == 1 && totalValoradosParcialmente == 1){
                 alcanzadosObjetivos='Si';
            }             
            else if(totalValoradosSi == 1 && totalValoradosNo == 1){
                 alcanzadosObjetivos='Parcialmente';
            }
            else if(totalValoradosParcialmente == 2){
                alcanzadosObjetivos='Parcialmente';
            }
            else if(totalValoradosParcialmente == 1 && totalValoradosNo == 1){
                 alcanzadosObjetivos='No';
            } 
            else if(totalValoradosNo == 2){
                 alcanzadosObjetivos='No';
            } 
            else{
                System.debug('@@@-***** PT_evaluacion_handler casuística no contemplada: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
                alcanzadosObjetivos='Parcialmente';
            }
               
        }  
        else if(objetivosRellenados == 1){
           
            System.debug('@@@ PT_evaluacion_handler 1 objetivo evaluado: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
            
            if(totalValoradosSi == 1){
                alcanzadosObjetivos='Si';
            }
            else if(totalValoradosParcialmente ==1){
                 alcanzadosObjetivos='Parcialmente';
            }             
            else if(totalValoradosNo==1){
                 alcanzadosObjetivos='No';
            } 
            else{
                System.debug('@@@ PT_evaluacion_handler casuística no contemplada: Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
                alcanzadosObjetivos='Parcialmente';          
            }
        }
        else{              
            System.debug('@@@ PT_evaluacion_handler no hay ningun objetivo evaluado Si: '+totalValoradosSi+' Parcialmente: '+totalValoradosParcialmente+' No: '+totalValoradosNo);
            alcanzadosObjetivos=null;
        }
       
       
        System.debug('objetivosRellenados:     ' + objetivosRellenados); 
        System.debug('totalValoradosSi:  ' +  totalValoradosSi); 
        System.debug('totalValoradosParcialmente:   ' + totalValoradosParcialmente); 
        System.debug('totalValoradosNo:  ' + totalValoradosNo); 
        
        
        if(objetivo_otros){
            alcanzadosObjetivos='Si';
        }
        
        System.debug('objetivo_otros :  ' + objetivo_otros); 
        
        return alcanzadosObjetivos;      
    }     
}