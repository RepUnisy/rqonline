public with sharing class AM_CNT_Accion_Mejora_Table {
    public String idES {get;set;}
    public AM_Estacion_de_servicio__c nombreES {get;set;}
    public RecordType rtIdLimpieza {get;set;}
    public RecordType rtIdOperatividad {get;set;}
    public RecordType rtIdAccesibilidad {get;set;}
    public RecordType rtIdRapidez {get;set;}
    public RecordType rtIdAmabilidad {get;set;}
    public RecordType rtIdFidelizacion {get;set;}
    public RecordType rtIdCarteleria {get;set;}
    public RecordType rtIdOfertaProductos {get;set;}
    public List<AM_Accion_de_mejora__c> ultLimpieza {get;set;}
    public List<AM_Accion_de_mejora__c> ultOperatividad {get;set;}
    public List<AM_Accion_de_mejora__c> ultAccesibilidad {get;set;}
    public List<AM_Accion_de_mejora__c> ultRapidez {get;set;}
    public List<AM_Accion_de_mejora__c> ultAmabilidad {get;set;}
    public List<AM_Accion_de_mejora__c> ultFidelizacion {get;set;}
    public List<AM_Accion_de_mejora__c> ultCarteleria {get;set;}
    public List<AM_Accion_de_mejora__c> ultOfertaProductos {get;set;}
    public Decimal valLimpieza {get;set;}
    public Decimal valOperatividad {get;set;}
    public Decimal valAccesibilidad {get;set;}
    public Decimal valRapidez {get;set;}
    public Decimal valAmabilidad {get;set;}
    public Decimal valFidelizacion {get;set;}
    public Decimal valCarteleria {get;set;}
    public Decimal valOfertaProductos {get;set;}
    public Decimal valLimpieza2 {get;set;}
    public Decimal valOperatividad2 {get;set;}
    public Decimal valAccesibilidad2 {get;set;}
    public Decimal valRapidez2 {get;set;}
    public Decimal valAmabilidad2 {get;set;}
    public Decimal valFidelizacion2 {get;set;}
    public Decimal valCarteleria2 {get;set;}
    public Decimal valOfertaProductos2 {get;set;}
    public Date fechaImpresion {get;set;}
   
    public AM_CNT_Accion_Mejora_Table(ApexPages.StandardController controller) {
        idES = ApexPages.currentPage().getParameters().get('Id');
        getEstacionServicio();
        getValorUltimoItem();
        
    }
    public void getEstacionServicio(){
        nombreES = [SELECT AM_Nombre_ES__c,
                    Name
                FROM
                    AM_Estacion_de_servicio__c
                WHERE 
                    Id =: idES];
    }
    
    public void getRecordTypeIds() {
        rtIdLimpieza = [SELECT Id FROM RecordType WHERE Name =: '1. Limpieza'];
        rtIdOperatividad = [SELECT Id FROM RecordType WHERE Name =: '2. Operatividad'];
        rtIdAccesibilidad = [SELECT Id FROM RecordType WHERE Name =: '3. Accesibilidad y movilidad'];
        rtIdRapidez = [SELECT Id FROM RecordType WHERE Name =: '4. Rapidez y eficiencia'];
        rtIdAmabilidad = [SELECT Id FROM RecordType WHERE Name =: '5. Amabilidad'];
        rtIdFidelizacion = [SELECT Id FROM RecordType WHERE Name =: '6. Fidelización y motivación'];
        rtIdCarteleria = [SELECT Id FROM RecordType WHERE Name =: '7. Cartelería y promociones'];
        rtIdOfertaProductos = [SELECT Id FROM RecordType WHERE Name =: '8. Oferta de productos y servicios'];
    }
    
    public void getValorUltimoItem() {
        getRecordTypeIds();
        try{
            ultLimpieza = [SELECT AM_Ultima_Valoracion_Limpieza__c FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c =: idES AND RecordTypeId =: rtIdLimpieza.Id ORDER BY CreatedDate DESC LIMIT 2];
            valLimpieza = ultLimpieza.get(0).AM_Ultima_Valoracion_Limpieza__c;
            valLimpieza2 = ultLimpieza.get(1).AM_Ultima_Valoracion_Limpieza__c;
        }catch(Exception e){
            valLimpieza = valLimpieza2 = 0.0;
        }
        try{
            ultOperatividad = [SELECT AM_NU_Ultima_Valoracion_Operatividad__c FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c =: idES AND RecordTypeId =: rtIdOperatividad.Id ORDER BY CreatedDate DESC LIMIT 2];
            valOperatividad = ultOperatividad.get(0).AM_NU_Ultima_Valoracion_Operatividad__c;
            valOperatividad2 = ultOperatividad.get(1).AM_NU_Ultima_Valoracion_Operatividad__c;
        }catch(Exception e){
            valOperatividad = valOperatividad2 = 0.0;
        }
        try{        
            ultAccesibilidad = [SELECT AM_NU_Ultima_Valoracion_Accesibilidad__c FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c =: idES AND RecordTypeId =: rtIdAccesibilidad.Id ORDER BY CreatedDate DESC LIMIT 2];
            valAccesibilidad = ultAccesibilidad.get(0).AM_NU_Ultima_Valoracion_Accesibilidad__c;
            valAccesibilidad2 = ultAccesibilidad.get(1).AM_NU_Ultima_Valoracion_Accesibilidad__c;
        }catch(Exception e){
            valAccesibilidad = valAccesibilidad2 = 0.0;
        }   
        try{
            ultRapidez = [SELECT AM_NU_Ultima_Valoracion_Rapidez__c FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c =: idES AND RecordTypeId =: rtIdRapidez.Id ORDER BY CreatedDate DESC LIMIT 2];
            valRapidez = ultRapidez.get(0).AM_NU_Ultima_Valoracion_Rapidez__c;
            valRapidez2 = ultRapidez.get(1).AM_NU_Ultima_Valoracion_Rapidez__c;
        }catch(Exception e){
            valRapidez = valRapidez2 = 0.0;
        }   
        try{
            ultAmabilidad = [SELECT AM_NU_Ultima_Valoracion_Amabilidad__c FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c =: idES AND RecordTypeId =: rtIdAmabilidad.Id ORDER BY CreatedDate DESC LIMIT 2];
            valAmabilidad = ultAmabilidad.get(0).AM_NU_Ultima_Valoracion_Amabilidad__c;
            valAmabilidad2 = ultAmabilidad.get(1).AM_NU_Ultima_Valoracion_Amabilidad__c;
        }catch(Exception e){
            valAmabilidad = valAmabilidad2 = 0.0;
        }   
        try{
            ultFidelizacion = [SELECT AM_NU_Ultima_valoracion_Fidelizacion__c FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c =: idES AND RecordTypeId =: rtIdFidelizacion.Id ORDER BY CreatedDate DESC LIMIT 2];
            valFidelizacion = ultFidelizacion.get(0).AM_NU_Ultima_valoracion_Fidelizacion__c;
            valFidelizacion2 = ultFidelizacion.get(1).AM_NU_Ultima_valoracion_Fidelizacion__c;
        }catch(Exception e){
            valFidelizacion = valFidelizacion2 = 0.0;
        }
        try{
            ultCarteleria = [SELECT AM_NU_Ultima_Valoracion_Carteleria__c FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c =: idES AND RecordTypeId =: rtIdCarteleria.Id ORDER BY CreatedDate DESC LIMIT 2];
            valCarteleria = ultCarteleria.get(0).AM_NU_Ultima_Valoracion_Carteleria__c;
            valCarteleria2 = ultCarteleria.get(1).AM_NU_Ultima_Valoracion_Carteleria__c;
        }catch(Exception e){
            valCarteleria = valCarteleria2 = 0.0;
        }
        try{        
            ultOfertaProductos = [SELECT AM_NU_Ultima_Valoracion_Oferta__c FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c =: idES AND RecordTypeId =: rtIdOfertaProductos.Id ORDER BY CreatedDate DESC LIMIT 2];
            valOfertaProductos = ultOfertaProductos.get(0).AM_NU_Ultima_Valoracion_Oferta__c;
            valOfertaProductos2 = ultOfertaProductos.get(1).AM_NU_Ultima_Valoracion_Oferta__c;
        }catch(Exception e){
            valOfertaProductos = valOfertaProductos2 = 0.0;
        }
    }
    
    public List<AM_Accion_de_mejora__c> getAccionesMejora(){
        fechaImpresion = Date.today();
        return [SELECT Name, 
                        AM_Item__c, 
                        AM_SEL_Alarmas__c,
                        AM_DT_Fecha_inicio__c,
                        AM_DT_Fecha_fin_prevista__c, 
                        AM_Evidencia_de_estatus__c,
                        AM_TX_Observaciones__c,
                        AM_TX_Diagnostico__c, 
                        AM_Accion_Ellegida__c,
                        AM_Otros__c, 
                        AM_SEL_Responsable__c,
                        AM_SEL_Estatus__c,
                        AM_SEL_Prioridad__c,
                        AM_SEL_Seguimiento__c
                FROM AM_Accion_de_mejora__c 
                WHERE AM_REF_Estacion_de_servicio__c =: idES AND AM_SEL_Estatus__c =: 'En curso'
                ORDER BY AM_Item__c, AM_DT_Fecha_inicio__c ASC];
    }
}