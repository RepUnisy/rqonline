/**
 *	@name: RQO_cls_WSSOAPUtilities
 *	@version: 1.0
 *	@creation date: 12/09/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase util con métodos para cubrir las necesidades propias de los callouts SOAP de la aplicación 
 *
 */
public class RQO_cls_WSSOAPUtilities {

    /**
    * @creation date: 05/09/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	A partir de una plantilla XML dada recoge y establece en esta los valores que llegan por parámetro y que son necesarios para la petición SOAP. 
    * @param: body tipo String, cuerpo inicial de la request extraido de una plantilla. 
    * @param: paramNumber tipo Integer, número de parametros que se van a sustituir.
    * @param: orderedValues, valores ordenados que se van a sustituir en la plantilla inicial.
    * @return: Boolean, ejecución correcta
    * @exception: 
    * @throws: 
	*/
    public String getHttpSOAPServiceFinalBody(String intialRequestBody, Integer paramNumber, String[] orderedValues){
        String finalRequestBody = null;
        //Comprobamos que tenemos informados todos los campos y que el número de valores coinciden con el número de parámetros a sustituir.
        if (String.isNotEmpty(intialRequestBody) && paramNumber != null 
            && paramNumber > 0 && orderedValues != null && !orderedValues.isEmpty() && orderedValues.size() == paramNumber){
            System.debug('Entra');
            try{
                for(Integer i = 0; i<paramNumber; i++){
                    intialRequestBody = intialRequestBody.replace('|PARAM' + String.valueOf(i+1) + '|', orderedValues[i]);
                }
                finalRequestBody = intialRequestBody;
            }catch(Exception ex){
                System.debug('Exception: ' + ex.getMessage());
            }     
		}
        return finalRequestBody;
    }
}