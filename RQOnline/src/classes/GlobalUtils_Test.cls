@isTest
public class GlobalUtils_Test {

    @testSetup static void setup(){
       
    }
  
    
    static testMethod void test1(){
       
        List <CurrencyType> listaCurrencyFinal = [SELECT ConversionRate, IsoCode FROM CurrencyType]; 
        map<string, decimal> mapaCurrencys = new map<string, decimal>();
        
        for(CurrencyType divisa : listaCurrencyFinal){
            mapaCurrencys.put(divisa.IsoCode, divisa.ConversionRate);
        }
        
        Test.startTest();
       
         Decimal aux =  GlobalUtils.ConvertCurrencyComp(mapaCurrencys, 'GBP', 'USD',100.00);
         System.debug('aux:' + aux);
      
        Test.stopTest(); 
    }
}