/**
*   @name: RQO_cls_IdentificadorSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan Lopez - Unisys
*   @description: Clase de test para probar la clase RQO_cls_IdentificadorSelector
*/
public with sharing class RQO_cls_IdentificadorSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_IdentificadorSelector';
    
    /**
    *   @name: RQO_cls_IdentificadorSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan Lopez - Unisys
    *   @description: Método para obtener la lista de campos que contiene el identificador
    */
    
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_obj_identificador__c.RQO_fld_idRelacion__c,
			RQO_obj_identificador__c.RQO_fld_codigoIdentificador__c	,
            RQO_obj_identificador__c.RQO_fld_tipodeIdentificador__c,
            RQO_obj_identificador__c.RQO_fld_idExterno__c};
    }
    
    /**
    *   @name: RQO_cls_IdentificadorSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan Lopez - Unisys
    *   @description: Método para obtener el sObjectType del identificador
    */

    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_identificador__c.sObjectType;
    }
	
    /**
    *   @name: RQO_cls_IdentificadorSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan Lopez - Unisys
    *   @description: Método para obtener el order by
    */
    
    public override String getOrderBy()
	{
		return 'RQO_fld_idRelacion__c';
	}
    
    /**
    *   @name: RQO_cls_IdentificadorSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan Lopez - Unisys
    *   @description: Método para obetener los identificadores por cuenta
    */

    public List<RQO_obj_identificador__c> selectByAccount(Set<Id> idSet)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'selectByAccount';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': idSet ' + idSet);
        
        string query = newQueryFactory().setCondition('RQO_fld_idRelacion__c IN :idSet').toSOQL();
 													
 		System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);

		return (List<RQO_obj_identificador__c>) Database.query(query); 				                
    }
}