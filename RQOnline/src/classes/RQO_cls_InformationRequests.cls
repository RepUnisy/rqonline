/**
*   @name: RQO_cls_InformationRequests
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona las solicitudes de información (Information Requests)
*/
public with sharing class RQO_cls_InformationRequests extends RQO_cls_ApplicationDomain 
{  
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_InformationRequests';
	
    /**
    *   @name: RQO_cls_InformationRequests
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: constructor de la clase que recibe una lista de peticiones de información
    */  
    public RQO_cls_InformationRequests(List<RQO_obj_informationRequest__c> requests) 
    {     
        super(requests);   
    }
    
    /**
    *   @name: RQO_cls_InformationRequests
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase que implementa una interfaz "Construible"
    */  
    public class Constructor implements fflib_SObjectDomain.IConstructable 
    {     
        public fflib_SObjectDomain construct(List<SObject> sObjectList) 
        {       
            return new RQO_cls_InformationRequests(sObjectList);     
        }   
    }
    
    /**
    *   @name: RQO_cls_InformationRequests
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que agrega a una Unidad de Trabjao una Petición de Información
    */    
    public void add(fflib_ISObjectUnitOfWork uow)
    {
    	// ***** CONSTANTES ***** //
        final String DEBUG_FORMAT = CLASS_NAME + ' - add : {0} {1}';
        
    	for (RQO_obj_informationRequest__c request : (List<RQO_obj_informationRequest__c>)Records)
    	{
    		System.debug(String.format(DEBUG_FORMAT, new String[]{'request', String.valueOf(request)}));
    		uow.registerNew(request);
    	}
    }
}