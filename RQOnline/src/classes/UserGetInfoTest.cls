@IsTest 
public class UserGetInfoTest {
    
    @IsTest(SeeAllData=true) static void testUserGetInfoTest() {
        try{
            User u = [select title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country
                      FROM User WHERE id =: UserInfo.getUserId()];    
            UserGetInfo.UserGetInfoRequest req = new UserGetInfo.UserGetInfoRequest();
            req.userId = u.id;

            List<UserGetInfo.UserGetInfoRequest> requests = new List<UserGetInfo.UserGetInfoRequest>();
            requests.add(req);
            UserGetInfo.UsersGetInfoTerm(requests);            
        }catch(Exception e){
            System.debug(e.getMessage());            
        }        
    }
    

}