/*------------------------------------------------------------------------
Author:         Borja Martín Ballesteros
Company:        Indra
Description:    class
History
<Date>          <Author>                      <Description>
13-Jul-2016     Borja Martín Ballesteros       Initial version
----------------------------------------------------------------------------*/
global class ITPM_Upload_Project_Controller {    
    Apexpages.StandardController controller;
    public String projectId{get;set;}
    public Transient Blob body{get;set;}
    public String file{get;set;}
    public ITPM_Project__c project{get;set;}
    transient public String option{get;set;}
    transient public String load{get;set;}
    
    private static final String TASK_TYPE_TASK = '0';
    private static final String TASK_TYPE_PHASE = '1';
    private static final String WBS_PROJECT = '0';
    
    public ITPM_Upload_Project_Controller(ApexPages.StandardController controller) {
        projectId = ApexPages.currentPage().getParameters().get('id');
        load = ApexPages.currentPage().getParameters().get('load');
        project = [SELECT Name, ITPM_SEL_PERIODICITY__c, ITPM_DT_Project_XML_Start_Date__c, ITPM_DT_Project_XML_Finish_Date__c FROM ITPM_Project__c WHERE ID =: projectId];
        if(project.ITPM_SEL_PERIODICITY__c == null) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select a perioricity.');
            ApexPages.addMessage(myMsg);
        }
    } 
    
    public boolean getMilestones(){
        if([SELECT Count() FROM ITPM_Milestone__c WHERE ITPM_REL_PROJECT__c =: projectId] > 0)
            return true;
        return false;    
    }
    
    public void importAttachment() {
        try {
            Dom.Document document = new Dom.Document();
            try {
                document.load(body.toString() );
            }catch (System.NullPointerException e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Select the .xml file to upload');
                ApexPages.addMessage(myMsg);
                return;
            }
            catch (StringException e){       
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to convert .xml file. ');
                ApexPages.addMessage(myMsg);
                system.debug('*********** ERROR: ' + e.getStackTraceString()  + e.getTypeName() + '. ' + e.getMessage());
                return;
            }
            Dom.Xmlnode rootnode = document.getrootElement();
            String ns = rootNode.getNamespace();                
            
         //   if((option == '' || option == 'delete') && project.ITPM_SEL_PERIODICITY__c == null){
            if(project.ITPM_SEL_PERIODICITY__c == null) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select a perioricity.');
                ApexPages.addMessage(myMsg);
                return;
            }
            
            // Eliminamos los milestones antiguos
            if(option == 'delete'){                
                Delete [select id from ITPM_Milestone__c where ITPM_REL_PROJECT__c =: projectId];
                Delete [select id from ITPM_Milestone__c where ITPM_REL_PROJECT_LEVELS__c =: projectId];
                Delete [select id from ITPM_Milestone_History__c where ITPM_REL_Project__c =: projectId];
            }
            
            // Si es una actualización recuperamos los milestones para actualizar su avance
            List <ITPM_Milestone__c> lstLevelUp;
            List <ITPM_Milestone__c> lstLevelDown;
            if(option == 'update'){
                lstLevelUp = [SELECT ITPM_TX_PROJECT_MILESTONE_NAME__c, ITPM_DT_Kickoff__c, ITPM_DT_Deadline__c, ITPM_CUR_COST__c, ITPM_NU_WORK__c, ITPM_PER_COMPLETED__c FROM ITPM_Milestone__c WHERE ITPM_REL_PROJECT__c =: project.Id];
                lstLevelDown = [SELECT ITPM_TX_PROJECT_MILESTONE_NAME__c, ITPM_DT_Kickoff__c, ITPM_DT_Deadline__c, ITPM_CUR_COST__c, ITPM_NU_WORK__c, ITPM_PER_COMPLETED__c FROM ITPM_Milestone__c WHERE ITPM_REL_PROJECT_LEVELS__c =: project.Id];
            }
            // system.debug('rootnode.getChildElements() ' + rootnode.getChildElements());
            
            for (Dom.XmlNode e : rootnode.getChildElements()) {
                system.debug('task?');
                List<ITPM_Milestone__c> lstMilestones = new List<ITPM_Milestone__c>();
                for (Dom.XmlNode e1 : e.getChildElements()){                   
                    if (e1.getName() == 'Task') {
                        String taskType = getValue(e1, 'Type', ns);
                        
                        String taskName;
                        
                        if(getValue(e1, 'Name', ns) != null){
                            taskName = getValue(e1, 'Name', ns).trim();
                        }
                        else{
                            taskName = 'Aux';
                        }
                        
                        String taskWBS = getValue(e1, 'WBS', ns);
                        
                        if(taskWBS=='0'){
                            if(option != 'update'){
                                // Cogemos las fechas del project para el cálculo de la matriz                                    
                                if(taskWBS!=null && taskWBS == '0'){
                                    Date KickoffAux = getDate(getValue(e1, 'Start', ns));
                                    Date DeadlineAux = getDate(getValue(e1, 'Finish', ns));
                                    project.ITPM_DT_Project_XML_Start_Date__c = KickoffAux;
                                    project.ITPM_DT_Project_XML_Finish_Date__c = DeadlineAux;                                        
                                }
                            }else{                            
                                // Actualizamos el earnedValue y el realWork en el project
                                String actualCost = getValue(e1, 'ActualCost', ns);                                    
                              /*  if(actualCost !=null && actualCost.length()>2){
                                    actualCost = actualCost.subString(0,actualCost.length()-2) + '.' + actualCost.subString(actualCost.length()-2,actualCost.length());
                                    actualCost = actualCost.replace('..', '.');
                                }
								*/
                                Decimal MIL_PER_COMPLETED = Decimal.valueOf(getValue(e1, 'PercentComplete', ns));                                                            
                                
                                ITPM_Milestone_History__c mlHistory = new ITPM_Milestone_History__c();                                
                                mlHistory.ITPM_REL_Project__c = project.Id;
                                mlHistory.ITPM_CUR_EarnedValue__c = Decimal.valueOf(actualCost).setScale(2);
                                mlHistory.ITPM_PER_RealWork__c = MIL_PER_COMPLETED;
                                mlHistory.ITPM_DT_Date__c = setStringToDateFormat(selectedDate);
                                insert mlHistory;
                            }
                            
                        }
                        else if (taskType=='0' || (taskWBS!=null && taskWBS.split('\\.').size() == 2)){
                            // Sólo nos quedamos con cabeceras y con último nivel
                            ITPM_Milestone__c milestone;
                            Date Kickoff = getDate(getValue(e1, 'Start', ns));
                            Date Deadline = getDate(getValue(e1, 'Finish', ns));
                            Decimal MIL_PER_COMPLETED = Decimal.valueOf(getValue(e1, 'PercentComplete', ns));                                                            
                            String work = getValue(e1, 'Work', ns);
                            work = work.subString(2,work.indexOf('H'));
                            String costeAux = getValue(e1, 'Cost', ns);
                            
                            system.debug('Coste ' + costeAux);
                         //   if(costeAux!=null && costeAux.length()>2) 
                                //costeAux = costeAux.subString(0,costeAux.length()-2) + '.' + costeAux.subString(costeAux.length()-2,costeAux.length());
                            if(option != 'update'){
                                // Creamos tareas de primer y último nivel
                                milestone = new ITPM_Milestone__c();
                                if(taskType == '0')
                                    milestone.ITPM_REL_PROJECT_LEVELS__c = project.Id;                                        
                                else if(taskWBS.split('\\.').size() == 2)
                                    milestone.ITPM_REL_PROJECT__c = project.Id;
                                else
                                    break;
                                milestone.ITPM_TX_PROJECT_MILESTONE_NAME__c = taskName;
                                milestone.ITPM_DT_Kickoff__c = Kickoff;
                                milestone.ITPM_DT_Deadline__c = Deadline;
                                milestone.ITPM_PER_COMPLETED__c = MIL_PER_COMPLETED;
                                milestone.ITPM_NU_WORK__c = integer.valueOf(work)/8;
                                milestone.ITPM_CUR_COST__c = Decimal.valueOf(costeAux).setScale(2); 
                                lstMilestones.add(milestone);
                            }
                            
                            else{
                                // Actualización de avances
                                boolean exists = false;
                                milestone = new ITPM_Milestone__c();
                                
                                if(taskType=='1'){
                                    // Milestone nivel 1
                                    for(ITPM_Milestone__c ml : lstLevelUp){
                                        if(ml.ITPM_TX_PROJECT_MILESTONE_NAME__c == taskName && ml.ITPM_CUR_COST__c == Decimal.valueOf(costeAux) 
                                           && ml.ITPM_NU_WORK__c == integer.valueOf(work)/8 && ml.ITPM_DT_Kickoff__c == Kickoff  && ml.ITPM_DT_Deadline__c == Deadline){
                                               milestone = ml;
                                               exists = true;                                     
                                           }
                                    }
                                }else{
                                    // Milestone de último nivel
                                    for(ITPM_Milestone__c ml : lstLevelDown){
                                        if(ml.ITPM_TX_PROJECT_MILESTONE_NAME__c == taskName && ml.ITPM_CUR_COST__c == Decimal.valueOf(costeAux) 
                                           && ml.ITPM_NU_WORK__c == integer.valueOf(work)/8 && ml.ITPM_DT_Kickoff__c == Kickoff  && ml.ITPM_DT_Deadline__c == Deadline){
                                               milestone = ml;
                                               exists = true; 
                                           }
                                    }
                                } 
                                
                                // Si existe actualizamos avance, si no mostramos error de carga del project
                                if(exists){
                                    if(milestone.ITPM_PER_COMPLETED__c != MIL_PER_COMPLETED){
                                        milestone.ITPM_PER_COMPLETED__c = MIL_PER_COMPLETED; 
                                        lstMilestones.add(milestone);
                                        exists=false;
                                    }           
                                }else{
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Changes were detected in milestone [' + taskName + ']');
                                    ApexPages.addMessage(myMsg);
                                    return;
                                }  
                            } 
                            
                        }
                    }
                }
                if (lstMilestones != null && lstMilestones.size() > 0) {
                    upsert(lstMilestones);   
                    RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Project Execution with Project' LIMIT 1]; 
                    if(project.RecordTypeId!=rt.Id){
                        project.RecordTypeId = rt.Id;  
                    } 
                }
            }
            update project;
            
        }catch (Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'An error has ocurred. ' + e.getTypeName() + '. ' + + e.getMessage());
            ApexPages.addMessage(myMsg);
            system.debug('*********** ERROR: ' + e.getStackTraceString());
            return;
        }        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The project has been updated successfully.');
        ApexPages.addMessage(myMsg);
    }
    
    private static Date getDate(String dtStr) { //Iso 8601 datetime to date
        String dateStr = dtStr.split('T')[0];
        List<String> comps = dateStr.split('-');
        return Date.newInstance(Integer.valueOf(comps[0]), Integer.valueOf(comps[1]), Integer.valueOf(comps[2]));
    }
    
    private static String getValue(Dom.Xmlnode n, String name, String ns) {
        try {
            Dom.Xmlnode titleNode = n.getChildElement(name, ns);
            return titleNode.getText();            
        }catch (System.NullPointerException e){
            return null;
        }
    }
    
    public String selectedDate{get;set;}
    public List<SelectOption> getDatesOptions() {
        if(project.ITPM_SEL_PERIODICITY__c == null) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select a perioricity.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        List<SelectOption> dateOptions = new List<SelectOption>(); 
        Date dtStart;
        if(project.ITPM_DT_Project_XML_Start_Date__c != null) 
            dtstart = project.ITPM_DT_Project_XML_Start_Date__c.toStartOfMonth();
        Date dtEnd = project.ITPM_DT_Project_XML_Finish_Date__c;        
        List<ITPM_Milestone_History__c> listProjectHistory = [SELECT ITPM_DT_Date__c FROM ITPM_Milestone_History__c
                                                              WHERE ITPM_REL_Project__c =: projectId
                                                              ORDER BY ITPM_DT_Date__c];
        
        while(dtStart<dtEnd){
            // calculate next date
            if(project.ITPM_SEL_PERIODICITY__c == 'Monthly (30 days)'){
                dtStart = dtStart.addMonths(1);
            }else if(project.ITPM_SEL_PERIODICITY__c == 'Fortnightly (15 days)'){
                if(dtStart.day()==1)
                    dtStart = dtStart.addDays(14);
                else{
                    dtStart = dtStart.addMonths(1).toStartOfMonth();
                }
            }else if(project.ITPM_SEL_PERIODICITY__c == 'Weekly (7 days)'){
                if(dtStart.day()==1 || dtStart.day()==8 || dtStart.day()==15)
                    dtStart = dtStart.addDays(7);
                else{
                    dtStart = dtStart.addMonths(1).toStartOfMonth();
                }                         
            }
            
            boolean exists = false;
            for(ITPM_Milestone_History__c his : listProjectHistory){
                if(his.ITPM_DT_Date__c == dtStart)
                    exists = true;
            }
            if(exists)
                dateOptions.add(new SelectOption( dtStart.format(), dtStart.format() + '. Already loaded.' ));
            else
                dateOptions.add(new SelectOption( dtStart.format(), dtStart.format()));
        }                                        
        return dateOptions;
    }
    
    private Date setStringToDateFormat(String myDate) {
        list<String> myDateOnly = new list<string>();
        
        if(myDate != null){
            myDateOnly = myDate.split(' ');
        }else{
            string cadena = (Date.today().format());
            system.debug('LA CADENA ES ' + cadena);
            myDateOnly.add(cadena);
        }
        
        String[] strDate = myDateOnly[0].split('/');
        Integer myIntDate = integer.valueOf(strDate[0]);
        Integer myIntMonth = integer.valueOf(strDate[1]);
        Integer myIntYear = integer.valueOf(strDate[2]);
        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
        return d;
    }
    
    webservice static String deleteMilestone(Id projectId){
        system.debug('*********** deleteMilestone.projectId: ' + projectId);
        try{
            Delete [select id from ITPM_Milestone__c where ITPM_REL_PROJECT__c =: projectId];
            Delete [select id from ITPM_Milestone__c where ITPM_REL_PROJECT_LEVELS__c =: projectId];
            Delete [select id from ITPM_Milestone_History__c where ITPM_REL_Project__c =: projectId];        
            RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Project Execution' LIMIT 1]; 
            ITPM_Project__c project = [SELECT id, RecordTypeId FROM ITPM_Project__c where Id =: projectId LIMIT 1];
            project.RecordTypeId = rt.id;
            update project;
            return 'true';
        } catch (Exception e) {return 'ERROR: ' + e.getStackTraceString();}       
    }
    
}