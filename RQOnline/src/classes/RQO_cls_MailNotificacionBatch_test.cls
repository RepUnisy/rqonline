/**
*	@name: RQO_cls_MailNotificacionBatch_test
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar el envío de notificaciones por mail
*/
@IsTest
public class RQO_cls_MailNotificacionBatch_test {

    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		System.runAs(u){
            RQO_cls_TestDataFactory.createCSEnvioNotificaciones();
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
            RQO_cls_TestDataFactory.createNotification(200);
            RQO_cls_TestDataFactory.createNotificationConsent();
		}
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del batch con productos
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testExecuteOKProduct(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
			Id batchJobId = Database.executeBatch(new RQO_cls_MailNotificacion_batch(), 2000);            
            test.stopTest();
            
            List<RQO_obj_notification__c> notificationAuxList = [SELECT Id, RQO_fld_mailSent__c FROM RQO_obj_notification__c];
            
            System.assertEquals(true, notificationAuxList.get(1).RQO_fld_mailSent__c);
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del batch con pedidos
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testExecuteOKPedido(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<RQO_obj_notification__c> notificationList = [SELECT Id, RQO_fld_messageType__c, RQO_fld_objectType__c FROM RQO_obj_notification__c];
            for (RQO_obj_notification__c item : notificationList) {
                item.RQO_fld_messageType__c = RQO_cls_Constantes.COD_PED_TRATAMIENTO;
                item.RQO_fld_objectType__c = RQO_cls_Constantes.COD_SAP_NOT_PEDIDO;
            }
            update notificationList;
            test.startTest();
			Id batchJobId = Database.executeBatch(new RQO_cls_MailNotificacion_batch(), 2000);            
            test.stopTest();
            
            List<RQO_obj_notification__c> notificationAuxList = [SELECT Id, RQO_fld_mailSent__c FROM RQO_obj_notification__c];
            
            System.assertEquals(true, notificationAuxList.get(0).RQO_fld_mailSent__c);
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta del batch con entregas
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testExecuteOKEntrega(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<RQO_obj_notification__c> notificationList = [SELECT Id, RQO_fld_messageType__c, RQO_fld_objectType__c FROM RQO_obj_notification__c];
            for (RQO_obj_notification__c item : notificationList) {
                item.RQO_fld_messageType__c = RQO_cls_Constantes.COD_PED_CONFIRMADO;
                item.RQO_fld_objectType__c = RQO_cls_Constantes.COD_SF_NOT_ENTREGA;
            }
            update notificationList;
            test.startTest();
			Id batchJobId = Database.executeBatch(new RQO_cls_MailNotificacion_batch(), 2000);            
            test.stopTest();
            
            List<RQO_obj_notification__c> notificationAuxList = [SELECT Id, RQO_fld_mailSent__c FROM RQO_obj_notification__c];
            
            System.assertEquals(true, notificationAuxList.get(0).RQO_fld_mailSent__c);
        }
    }
    
}