/**
*   @name: RQO_cls_GestionNotificacion_cc
*   @version: 1.0
*   @creation date: 07/02/2018
*   @author: Alvaro Alonso - Unisys
*   @description: Controlador Apex para la gestión de la ventana de notificaciones de portal
*	
*/
public without sharing class RQO_cls_GestionNotificacion_cc {

    private static final String CLASS_NAME = 'RQO_cls_GestionNotificacion_cc';
    
	/**
    * @creation date: 07/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera el número de notificaciones nuevas para el contacto que están en un estado válido
    * @param: IdContacto tipo String
    * @return: Integer
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static Integer countNotificaciones (String IdContacto) {
        final String METHOD = 'countNotificaciones';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Map<String, RQO_cmt_notificationCategories__mdt> mapForContact = null;
        Integer aggR = 0;
        
        //  Vamos a recuperar los tipos de notificaciones sobre los que el usuario tiene permisos de visualización
        RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
        Map<Id, Map<String, RQO_cmt_notificationCategories__mdt>> contatcValidationMap = 
            util.getValidMessageTypeForContact(new List<Id>{idContacto});

        /*
        *   Si tengo permisos de notificaciones para el contacto vamos a buscar las notificaciones
        *   de ese tipo que aun no están visualizadas
        */
        if (contatcValidationMap != null && !contatcValidationMap.isEmpty() && contatcValidationMap.containsKey(idContacto) ){
            mapForContact = contatcValidationMap.get(idContacto);
            if ( mapForContact != null && !mapForContact.isEmpty()) {
                if (mapForContact.containsKey(RQO_cls_Constantes.COD_PED_REGISTRADO)){mapForContact.remove(RQO_cls_Constantes.COD_PED_REGISTRADO);}
                aggR = [SELECT count() FROM RQO_obj_notification__c WHERE RQO_fld_visualized__c = false AND RQO_fld_contact__c = :IdContacto 
                    		AND RQO_fld_messageType__c in : mapForContact.keySet()];
			}
        }
        
        // Se añaden las comunicaciones multimedia
        List<Contact> contacts = new RQO_cls_ContactSelector().selectById(new Set<ID> { idContacto });
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': contacts.size()' + contacts.size());
        
        if (contacts != null && contacts.size() > 0)
        {
            Contact contacto = contacts[0];
            List<String> customerProfiles = new List<String>{contacto.RQO_fld_perfilCliente__c};
            List<String> userProfiles = new List<String>{contacto.RQO_fld_perfilUsuario__c};
            List<RQO_obj_communication__c> comms = new RQO_cls_CommunicationsSelector().
                    	selectActiveMultimedia(new Set<Id> {UserInfo.getUserId()}, customerProfiles, userProfiles);
			List<RQO_obj_notification__c> notifs = 
	                	new RQO_cls_NotificationSelector().selectVisualizedByContactId(new Set<String> { contacto.Id },
	                																   RQO_cls_Constantes.COD_COM_MULTIMEDIA);
            for (RQO_obj_communication__c comm : comms) {
            	aggR++;
	            for (RQO_obj_notification__c nofif : notifs) {
	            	if (nofif.RQO_fld_externalId__c == String.valueOf(comm.Id)) {
	            		aggR--;
	            		break;
	            	}
	            }
	            
            }
        }

        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return aggR;
    }
    
}