/*------------------------------------------------------------
Author:        	Rubén Simarro
Company:       	Indra
Description:   	
clase Controladora de las Visual Force de Informes
Cualquier cambio que se haga en una pagina, hay que replicarlo en su pagina excel asociada:

PT_pagina_informeCalendario - PT_pagina_informeCalendario_excel
PT_pagina_informePatrocinios - PT_pagina_informePatrocinios_excel

Test Class:    	PT_test_informes
History
<Date>      	<Author>     	<Change Description>
10-10-2015	    Rubén Simarro 	Initial Version
05-04-2016      Rubén Simarro   Añadida funcionalidad de generar los 2 reportes en formato Excel
30-05-2016      Rubén Simarro   Añadida funcionalidad para mostrar los patrocinios vigentes en un año concreto
------------------------------------------------------------*/
public class PT_controller_informes {

    //Reporte llamado desde la aplicación
    public Reporte__c reporteEjecutado {get; set;}
    
    //Objeto que alimenta las visual force para pintar los patrocinios que intervienen en el informe
    public List<Patrocinios__c> lstPatrocinios;
       
    public String mostrarColumna29 {get;set;}
    public String mostrarColumna30 {get;set;}
    public String mostrarColumna31 {get;set;}
        
    //sql dinamica para recuperar la info de patrocinios en funcion de los campos de filtrado establecidos
    private String SOQL_patrocinios = 'select Id, Name, nombre__c, OwnerId, Owner.name, CreatedById, CreatedBy.name, Ambito_geografico__c, estado__c, formula_estado__c, Fecha_limite_de_aprobacion2__c, Notoriedad__c, Publico_objetivo__c, Fecha_de_inicio__c, Fecha_de_fin__c FROM Patrocinios__c ';
     
    //indica si el orden será ascendente o descendente y por qué campo
    private String orden;
    private String sortField;
    private String previousSortField;

    public String areaDeUsuarioEnSesion {get; set;}
    
    public PT_controller_informes(ApexPages.StandardController controller){
                        
        reporteEjecutado = [select 
                            name,  
                            recordtype.name, 
                            Responsable_del_programa__c,
                            Responsable_del_programa__r.name,                             
                            ambito_Geografico__c,
                            Estado__c, 
                            Fecha_limite_de_aprobacion__c,                        
                            Notoriedad__c,   
                            Publico_Objetivo__c,  
                            numero_mes__c,      
                            numero_anio__c,
                            mes__c,
                            anio__c, 
                            Anio_de_vigencia__c
                            from Reporte__c  
                            WHERE id = :ApexPages.currentPage().getParameters().get('Id')];
      
        areaDeUsuarioEnSesion = [Select name from userRole where id =: UserInfo.getUserRoleId()].name;
            
        //pongo esto porque en su clase de test test no permite asignar en el new reporte__c() el valor de tipo de registro recordtype.name
        if (test.isRunningTest()){

            reporteListadoPatrocinios();
            reporteCalendarioPatrocinios(); 
        }
        else{
          
            if ( reporteEjecutado.recordtype.name.equals(Label.etiqueta_nombre_informe_listado_patrocinios) ){
                reporteListadoPatrocinios();
            }   
            else if ( reporteEjecutado.recordtype.name.equals(Label.etiqueta_nombre_informe_calendario) ){
                reporteCalendarioPatrocinios();   
            }  
            else{
                System.debug('@@@ PT_controller_informes tipo de registro desconocido: '+reporteEjecutado.recordtype.name);
            }
        }
    }
     
    @TestVisible
    private void reporteListadoPatrocinios(){
                                         
        System.debug('@@@ PT_controller_informes reporteListadoPatrocinios()');
        
           try{
         
               boolean hayyawhere = false;
                                
               if ( reporteEjecutado.estado__c != null) {                                                          
                   if(hayyawhere){                                                            
                       SOQL_patrocinios = SOQL_patrocinios + 'and Estado__c ='+'\''+ reporteEjecutado.estado__c+'\'';                                                       
                   }                                                      
                   else{                                                            
                       SOQL_patrocinios = SOQL_patrocinios + ' where Estado__c ='+'\''+ reporteEjecutado.estado__c+'\'';                                                          
                       hayyawhere=true;                                                       
                   }                                                          
               }                                                       
               if (reporteEjecutado.Responsable_del_programa__r != null) {                                                           
                   if(hayYaWhere){                    
                       SOQL_patrocinios = SOQL_patrocinios + ' and CreatedById ='+'\''+ reporteEjecutado.Responsable_del_programa__c+'\'';                      
                   }                   
                   else{                   
                       SOQL_patrocinios = SOQL_patrocinios + ' where CreatedById ='+'\''+ reporteEjecutado.Responsable_del_programa__c+'\'';                       
                       hayyawhere=true;                       
                   }                 
               }                                                
               if (reporteEjecutado.ambito_geografico__c != null) {                                                           
                   if(hayYaWhere){                                                                                                  
                       SOQL_patrocinios = SOQL_patrocinios + ' and ambito_geografico__c ='+'\''+ reporteEjecutado.ambito_geografico__c+'\'';                                                                                                    
                   }                                                         
                   else{                                                                                                             
                       SOQL_patrocinios = SOQL_patrocinios + ' where ambito_geografico__c ='+'\''+ reporteEjecutado.ambito_geografico__c+'\'';                                                        
                       hayyawhere=true;                                                        
                   }                                                   
               }                                                  
               if (reporteEjecutado.notoriedad__c != null) {                                                         
                   if(hayYaWhere){                                                                                                            
                       SOQL_patrocinios = SOQL_patrocinios + ' and notoriedad__c ='+'\''+ reporteEjecutado.notoriedad__c+'\'';                                          
                   }                                                    
                   else{                                                     
                       SOQL_patrocinios = SOQL_patrocinios + ' where notoriedad__c ='+'\''+ reporteEjecutado.notoriedad__c+'\'';                                                      
                       hayyawhere=true;                                                  
                   }              
               }         
               if (reporteEjecutado.publico_objetivo__c != null) {      
                   if(hayYaWhere){               
                       SOQL_patrocinios = SOQL_patrocinios + ' and publico_objetivo__c ='+'\''+ reporteEjecutado.publico_objetivo__c+'\'';               
                   }         
                   else{          
                       SOQL_patrocinios = SOQL_patrocinios + ' where publico_objetivo__c ='+'\''+ reporteEjecutado.publico_objetivo__c+'\'';                 
                       hayyawhere=true;           
                   }            
               }       
               if (reporteEjecutado.Fecha_limite_de_aprobacion__c != null) {   
                                     
                   DateTime dateFechaLimite = reporteEjecutado.Fecha_limite_de_aprobacion__c;                                                         
                                                          
                   if(hayYaWhere){                                                     
                       SOQL_patrocinios = SOQL_patrocinios + ' and Fecha_limite_de_aprobacion2__c <= '+ dateFechaLimite.format('yyyy-MM-dd');                                                   
                   }                                           
                   else{                                                  
                       SOQL_patrocinios = SOQL_patrocinios + ' where Fecha_limite_de_aprobacion2__c <= '+ dateFechaLimite.format('yyyy-MM-dd');                                               
                       hayyawhere=true;                          
                   }                                          
               }    
               if(areaDeUsuarioEnSesion != Label.etiqueta_area_equipo_patrocinio){

                   if(hayYaWhere){                                                     
                       SOQL_patrocinios = SOQL_patrocinios + ' and area__c ='+'\''+ areaDeUsuarioEnSesion+'\'';
                   }                                            
               
                   else{                                                  
                       SOQL_patrocinios = SOQL_patrocinios + ' where area__c ='+'\''+ areaDeUsuarioEnSesion+'\'';        
                       hayyawhere=true;       
                   }  
               } 
               else{
                   areaDeUsuarioEnSesion = '';
               }
               
               if (reporteEjecutado.Anio_de_vigencia__c != null) {       
                  
                   DateTime fechaInicioAnio = Datetime.newInstance(Integer.valueOf(reporteEjecutado.Anio_de_vigencia__c), 1, 1);
                   Datetime fechaFinAnio = Datetime.newInstance(Integer.valueOf(reporteEjecutado.Anio_de_vigencia__c), 12, 31);
                      
                   if(hayYaWhere){                                                     
                       SOQL_patrocinios = SOQL_patrocinios + ' and ('
                                   +    '( Fecha_de_inicio__c >= '+ fechaInicioAnio.format('yyyy-MM-dd')+' AND Fecha_de_inicio__c <= '+fechaFinAnio.format('yyyy-MM-dd') +')'      
                                   +' OR ( Fecha_de_fin__c >= '   + fechaInicioAnio.format('yyyy-MM-dd')+' AND Fecha_de_fin__c <= '   +fechaFinAnio.format('yyyy-MM-dd') +')'
                                   +' OR ( Fecha_de_inicio__c < ' + fechaInicioAnio.format('yyyy-MM-dd')+' AND Fecha_de_fin__c >'     +fechaFinAnio.format('yyyy-MM-dd') +')'
                                   +')';
                   }                                           
                   else{                                                  
                       SOQL_patrocinios = SOQL_patrocinios + ' where ('
                                   +    '( Fecha_de_inicio__c >= '+ fechaInicioAnio.format('yyyy-MM-dd')+' AND Fecha_de_inicio__c <= '+fechaFinAnio.format('yyyy-MM-dd') +')'    
                                   +' OR ( Fecha_de_fin__c >= '   + fechaInicioAnio.format('yyyy-MM-dd')+' AND Fecha_de_fin__c <= '   +fechaFinAnio.format('yyyy-MM-dd') +')'
                                   +' OR ( Fecha_de_inicio__c < ' + fechaInicioAnio.format('yyyy-MM-dd')+' AND Fecha_de_fin__c >'     +fechaFinAnio.format('yyyy-MM-dd') +')'                                               
                                   +')';
                       hayyawhere=true;                          
                
                   }     
               }
           }
        catch(Exception e){       
            system.debug('@@@ ERROR PT_controller_informes reporteListadoPatrocinios(): '+e.getMessage());   
        }        
    }
   
    @TestVisible
    private void reporteCalendarioPatrocinios(){
         
        System.debug('@@@ PT_controller_informes reporteCalendarioPatrocinios()');

        try{              
    
            boolean hayyawhere = false;
                                   
            //añado la condicion de que solo muestre los Programas con contrato firmado
             if(hayYaWhere){                                                                                                  
                   SOQL_patrocinios = SOQL_patrocinios + ' and estado__c IN (\'Contrato firmado\',\'Programa en curso\')';                                                                                                  
               }                                                          
               else{                                                                                                             
                   SOQL_patrocinios = SOQL_patrocinios + ' where estado__c IN (\'Contrato firmado\',\'Programa en curso\')';     
                   hayyawhere=true;                                                            
               }  
                   
            if(areaDeUsuarioEnSesion != Label.etiqueta_area_equipo_patrocinio){
                 

                if(hayYaWhere){                                                     
                   SOQL_patrocinios = SOQL_patrocinios + ' and area__c ='+'\''+ areaDeUsuarioEnSesion+'\'';     
               }                                            
               else{                                                  
                   SOQL_patrocinios = SOQL_patrocinios + ' where area__c ='+'\''+ areaDeUsuarioEnSesion+'\'';        
                   hayyawhere=true;                                        
               }  
            }
            else{
                areaDeUsuarioEnSesion = '';
            }
              
             //Compruebo si el mes seleccionado es de 30 dias, para darle valor que no de error "Month or Day out of range in DATE() function" de la página PT_pagina_informeCalendario
             if(reporteEjecutado.numero_mes__c == 2  || 
                reporteEjecutado.numero_mes__c == 4 || 
                reporteEjecutado.numero_mes__c == 6  || 
                reporteEjecutado.numero_mes__c == 9 || 
                reporteEjecutado.numero_mes__c == 11){                  
                
                    mostrarColumna31 = 'false';                
           
                }
            else{
                mostrarColumna31 = 'true';
            }
                          
             //Compruebo si el mes seleccionado es FEBRERO, para darle valor que no provoque un error "Month or Day out of range in DATE() function" de la página PT_pagina_informeCalendario
             if(reporteEjecutado.numero_mes__c == 2 ){                  
                 
                 mostrarColumna29 = 'false';            
                 mostrarColumna30 = 'false';               
             }
             else{
                 mostrarColumna29 = 'true'; 
                 mostrarColumna30 = 'true';    
             }
               
              //si el mes seleccionado es FEBRERO, tengo que comprobar si es año bisiesto para mostrar columna dia 29
             //y evitar un error "Month or Day out of range in DATE() function"
             if(reporteEjecutado.numero_mes__c == 2 && date.isLeapYear(Integer.valueOf(reporteEjecutado.numero_anio__c)) ){                  
              
                 mostrarColumna29 = 'true';  
             }
                    
        }
        catch(Exception e){
            system.debug('@@@ ERROR PT_controller_informes reporteCalendarioPatrocinios(): '+e.getMessage());
            
            lstPatrocinios = new List<Patrocinios__c>();
        }   
    }
    

    public List<Patrocinios__c> getlstPatrocinios(){
                     
        try{    
            
            if(orden != null && sortField != null){
             
                lstPatrocinios = Database.Query(SOQL_patrocinios +' ORDER BY '+sortField+' '+ orden);
             
                system.debug('@@@ '+SOQL_patrocinios +' ORDER BY '+sortField+' '+ orden);
            }
            else{
                 lstPatrocinios = Database.Query(SOQL_patrocinios);
                 system.debug('@@@ '+SOQL_patrocinios);
            }
         
          return lstPatrocinios;
        }
        catch(Exception e){
            system.debug('@@@ PT_controller_informes getlstPatrocinios(): '+e.getMessage());
            
            lstPatrocinios = new List<Patrocinios__c>();
            
            return lstPatrocinios;
        }
    }
    
        public void sortByEstado() {  
        sortField = 'formula_estado__c';
        doSort();
    }

    public void sortByNombrePrograma() {  
        sortField = 'nombre__c';
         doSort();
    }
    
    public void sortByResponsable() {
        sortField = 'CreatedBy.name';
        doSort();
    }
    
    public void sortByAmbitoGeografico(){
        sortField = 'ambito_geografico__c';
        doSort();
    }
    
    public void sortByNotoriedad(){
        sortField = 'notoriedad__c';
        doSort();
    }
    
    public void sortByPublicoObjetivo(){
        sortField = 'publico_objetivo__c';
        doSort();
    }
    
    public void sortByFechaLimiteAprobacion(){
        sortField = 'Fecha_limite_de_aprobacion2__c';
        doSort();
    }
      
    public void sortByFechaInicioContrato(){
        sortField = 'Fecha_de_inicio__c';
        doSort();
    }
    
    public void sortByFechaFinContrato(){
        sortField = 'Fecha_de_fin__c';
        doSort();
    }
    
     private void doSort(){
           
        if(previousSortField == sortField){
            orden = 'DESC';
            previousSortField = null;
        }else{
            previousSortField = sortField;
            orden = 'ASC';
        }
    }
 
  
    
    //#######################   GENERACIÓN EXCEL #####################################L
    
    public pagereference exportarExcelListadoPatrocinios(){
                
        Pagereference p = new Pagereference('/apex/PT_pagina_informePatrocinios_excel');
   
        p.setredirect(false);
    
        return p;
    }    
    
    public pagereference exportarExcelCalendarioPatrocinios(){                            
                         
        Pagereference p = new Pagereference('/apex/PT_pagina_informeCalendario_excel');
   
        p.setredirect(false);
    
        return p;
    }     
    
}