/**
 *  @name: RQO_cls_GradoIntermedio_batch
 *  @version: 1.0
 *  @creation date: 11/09/2017
 *  @author: David Iglesias - Unisys
 *  @description: Clase ejecutada a traves de job programado para el volcado del objeto intermedio
 */
global class RQO_cls_GradoIntermedio_batch implements Database.Batchable<sObject>, Database.Stateful {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_GradoIntermedio_batch';
    
    // ***** VARIABLES ***** //
    private List<RQO_obj_logProcesos__c> logObjectList = null;
    private map<String, Id> orgVentasMap = null;
    
    // ***** CONSTRUCTORES ***** //
    public RQO_cls_GradoIntermedio_batch () {
        this.logObjectList = new List<RQO_obj_logProcesos__c> ();
        this.orgVentasMap = new map<String, Id> ();
    }
    
    // ***** METODO GLOBALES ***** //

    /**
    * @creation date: 11/09/2017
    * @author: David Iglesias - Unisys
    * @description: Realiza el volcado del objeto intermedio a QP0 
    * @param: Database.BatchableContext
    * @return: Database.QueryLocator    
    * @exception: 
    * @throws: 
    */
    global Database.QueryLocator start(Database.BatchableContext context) {
        
        /**
        * Constantes
        */
        final String METHOD = 'start';

        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String query = 'SELECT Id, RQO_fld_canal__c, RQO_fld_clasesdeEnvases__c, '+
                            'RQO_fld_descripciondelGrado__c, RQO_fld_grado__c, RQO_fld_jerarquiadeProducto__c, '+
                            'RQO_fld_organizaciondeVentas__c, RQO_fld_sector__c, RQO_fld_sociedad__c '+
                        'FROM RQO_obj_gradoIntermedio__c '+
                        'WHERE RQO_fld_procesado__c = false';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');   
        return Database.getQueryLocator(query);
    }
    
    /**
    * @creation date: 11/09/2017
    * @author: David Iglesias - Unisys
    * @description: Ejecucion por tramas del volcado del objeto intermedio a QP0 
    * @param: Database.BatchableContext
    * @param: List<RQO_obj_gradoIntermedio__c>
    * @return:  
    * @exception: 
    * @throws: 
    */
    global void execute(Database.BatchableContext context, List<RQO_obj_gradoIntermedio__c> scope) {
        
        /**
        * Constantes
        */
        final String METHOD = 'execute';
        
        /**
         * Variables
         */
        RQO_obj_qp0Grade__c qp0Grado = null; 
        RQO_obj_logProcesos__c logObject = null;
        RQO_obj_containerJunction__c contJunction = null;
        Set <String> envasesSet = new Set <String> ();
        Set <String> gradosDuplaSet = new Set <String> ();
        Set <String> envSetAux = null;
        List <RQO_obj_qp0Grade__c> qp0GradosUpsertList = new List <RQO_obj_qp0Grade__c> ();
        List <RQO_obj_containerJunction__c> contJunctionInsertList = new List <RQO_obj_containerJunction__c> ();
        List <String> envasesList = null;
        List <RQO_obj_gradoIntermedio__c> gradoIntermedioUpdateList = new List <RQO_obj_gradoIntermedio__c> ();
        Map <String, List<String>> envasesGradosMap = new Map <String, List<String>> ();
        Map <String, Id> envasesIdMap = new Map <String, Id> ();
        Map <String, Id> qp0GradosInsertOKMap = null;

        /**
         * Inicio
         */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        // Se limpia la lista del log de errores en cada trama
        this.logObjectList.clear();
        
        for (RQO_obj_gradoIntermedio__c item : scope) {
            if (String.isNotBlank(item.RQO_fld_clasesdeEnvases__c)) {
                envasesList = item.RQO_fld_clasesdeEnvases__c.split(RQO_cls_Constantes.SLASH);
                for(String itemAux : envasesList) {
                    if (String.isNotBlank(item.RQO_fld_organizaciondeVentas__c)) {
                        // Mapeo para el caso de poliolefinas
                        if (item.RQO_fld_organizaciondeVentas__c.equalsIgnoreCase(RQO_cls_Constantes.COD_AREA_VENTAS_10) && itemAux.equalsIgnoreCase(RQO_cls_Constantes.ENV_GC)) {
                            itemAux = itemAux + RQO_cls_Constantes.HYPHEN + RQO_cls_Constantes.COD_AREA_VENTAS_10;
                            envasesSet.add(itemAux);
                        } else {
                            envasesSet.add(itemAux);
                        }
                    } else {
                        // LOG no existe organizacion de ventas en el grado
                        logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(Label.RQO_lbl_gradoIntermedioIntegracion, RQO_cls_Constantes.LOG_DATA_ERROR, 
                                                                                Label.RQO_lbl_mensajeErrorOrganizacionVentasIntegracion + item.RQO_fld_grado__c, 
                                                                                null, item.RQO_fld_grado__c, RQO_cls_Constantes.BATCH_GRADOS);
                        if (logObject != null) {
                            this.logObjectList.add(logObject);
                        }
                        System.debug(Label.RQO_lbl_mensajeErrorOrganizacionVentasIntegracion + item.RQO_fld_grado__c);
                    }
                    
                }
                if (envasesGradosMap.get(item.RQO_fld_grado__c) != null) {
                    List <String> envasesListAux = envasesGradosMap.get(item.RQO_fld_grado__c);
                    envasesList.addAll(envasesListAux); 
                } 
                envasesGradosMap.put(item.RQO_fld_grado__c, envasesList);
                
            } else {
                envasesGradosMap.put(item.RQO_fld_grado__c, null);
            }           
        }
        
        //Obtención de las organizaciones de venta
        if (this.orgVentasMap == null || this.orgVentasMap.isEmpty()) {           
            for (RQO_obj_organizaciondeVentas__c item : [SELECT Id, RQO_fld_codeOrganization__c, RQO_fld_canal__c, RQO_fld_sector__c 
                                                           FROM RQO_obj_organizaciondeVentas__c]) {
                this.orgVentasMap.put(item.RQO_fld_codeOrganization__c+item.RQO_fld_canal__c+item.RQO_fld_sector__c, item.Id);
            }
        }
        
        // Obtención de IDs de los envases relacionados a los grados que se van a volcar
        Map <String, Id> envasesExistMap = new Map <String, Id> ();
        for (RQO_obj_container__c item : [SELECT Id, RQO_fld_codigo__c FROM RQO_obj_container__c WHERE RQO_fld_codigo__c IN :envasesSet]) {
            envasesIdMap.put(item.RQO_fld_codigo__c, item.Id);
        }
        System.debug('Mapeo de envases: '+envasesIdMap.keySet());
        
        // Obtencion de relaciones creadas en la tabla intermedia de QP0 Grades y Container
        List<RQO_obj_containerJunction__c> junctionContainerList = [SELECT Id FROM RQO_obj_containerJunction__c WHERE RQO_fld_gradoQp0__r.Name IN :envasesGradosMap.keySet()];
        
        // Borrado de los registros junction para los grados que se van a insertar/actualizar
        if (junctionContainerList != null && !junctionContainerList.isEmpty()) {
            delete junctionContainerList;
        }
    
        // Creación de objetos RQO_obj_gradoIntermedio__c para realizar upsert con el volcado de datos
        for (RQO_obj_gradoIntermedio__c item : scope) {
            
            qp0Grado = new RQO_obj_qp0Grade__c();
            qp0Grado.Name = item.RQO_fld_grado__c;
            qp0Grado.RQO_fld_sku__c = item.RQO_fld_grado__c;
            //qp0Grado.RQO_fld_canal__c = item.RQO_fld_canal__c;
     		qp0Grado.RQO_fld_descripcionDelGrado__c = item.RQO_fld_descripciondelGrado__c;
	        qp0Grado.RQO_fld_jerarquiaDeProducto__c = item.RQO_fld_jerarquiadeProducto__c;
            //qp0Grado.RQO_fld_organizaciondeVentas__c = item.RQO_fld_organizaciondeVentas__c;
            qp0Grado.RQO_fld_sector__c = item.RQO_fld_sector__c;
            qp0Grado.RQO_fld_sociedad__c = item.RQO_fld_sociedad__c;
            
            //Comprobación de duplicidad de grados
            if (!gradosDuplaSet.contains(qp0Grado.Name)) {
                // Lista para realizar la operación DML
                qp0GradosUpsertList.add(qp0Grado);
                gradosDuplaSet.add(qp0Grado.Name);
            }

            System.debug(qp0Grado.Name);
                        
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': qp0GradosUpsertList: '+qp0GradosUpsertList);
        
        // Insercion/Actualización de grados QP0
        if (!qp0GradosUpsertList.isEmpty()) {
            System.debug('Tamaño lista de QP0: '+qp0GradosUpsertList.Size());
            qp0GradosInsertOKMap = insertOrUpdateData(qp0GradosUpsertList, Label.RQO_lbl_qp0GradeIntegracion);
        }
        
        
        // Inserción de relación grado-envases
        for (RQO_obj_gradoIntermedio__c item : scope) {
            envSetAux = new Set<String> ();
            if (qp0GradosInsertOKMap.get(item.RQO_fld_grado__c) != null) {
                for (String itemAux : envasesGradosMap.get(item.RQO_fld_grado__c)) {
                    if (!envSetAux.contains(itemAux)) {
                        envSetAux.add(itemAux);
                        contJunction = new RQO_obj_containerJunction__c();
                        contJunction.RQO_fld_gradoQp0__c = qp0GradosInsertOKMap.get(item.RQO_fld_grado__c);
                        contJunction.RQO_fld_organizaciondeVentas__c = this.orgVentasMap.get(item.RQO_fld_organizaciondeVentas__c+item.RQO_fld_canal__c+item.RQO_fld_sector__c);
                        
                        // Mapeo para el caso de poliolefinas
                        if (item.RQO_fld_organizaciondeVentas__c != null && item.RQO_fld_organizaciondeVentas__c.equalsIgnoreCase(RQO_cls_Constantes.COD_AREA_VENTAS_10) 
								&& itemAux.equalsIgnoreCase(RQO_cls_Constantes.ENV_GC)) {
                            contJunction.RQO_fld_container__c = envasesIdMap.get(itemAux + RQO_cls_Constantes.HYPHEN + RQO_cls_Constantes.COD_AREA_VENTAS_10);
                        } else {
                            contJunction.RQO_fld_container__c = envasesIdMap.get(itemAux);
                        }
                        
                        contJunctionInsertList.add(contJunction);
                    }
                }
                // Flag de registro intermedio procesado
                item.RQO_fld_procesado__c = true;
                gradoIntermedioUpdateList.add(item);
            }
        }
        
        // Insercion/Actualización de Container Junction (Relacion n-m QP0-Envases)
        if (!contJunctionInsertList.isEmpty()) {
            insertOrUpdateData(contJunctionInsertList, Label.RQO_lbl_containerJunctionIntegracion);
        }
        
        // Actualización de los grados intermedios procesados
        if (gradoIntermedioUpdateList != null && !gradoIntermedioUpdateList.isEmpty()) {
            update gradoIntermedioUpdateList;
        }
        
        // Insercion de logs en el caso de existir
        if (this.logObjectList != null && !this.logObjectList.isEmpty()) {
            RQO_cls_ProcessLog.generateLogData(this.logObjectList);
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
    }
    
    global void finish(Database.BatchableContext context) {}
    
    /**
    * @creation date: 11/09/2017
    * @author: David Iglesias - Unisys
    * @description: Insercion/actualizacion de la lista de objetos RQO_obj_qp0Grade__c
    * @param: objectListDML     Lista a actualizar
    * @param: operacion         Tipo de dato a insertar/actualizar
    * @return:  List <RQO_obj_qp0Grade__c>     Lista de objetos insertados/actualizados
    * @exception: 
    * @throws: 
    */
    private Map <String, Id> insertOrUpdateData (List <sObject> objectListDML, String tipoObjeto) {
        
        /**
        * Constantes
        */
        final String METHOD = 'insertOrUpdateData';
        
        /**
        * Variables
        */
        Schema.SObjectField f = null;
        Database.UpsertResult[] srListResult = null;
        Database.SaveResult[] svListResult = null;
        Set<String> objetosOKSet = new Set<String> ();
        RQO_obj_logProcesos__c logObject = null;
        Map<String, Id> qp0GradosInsertOKMap = new Map<String, Id> ();
        
        /**
        * Inicio
        */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        
        if (tipoObjeto.equals(Label.RQO_lbl_qp0GradeIntegracion)) {
            List<RQO_obj_qp0Grade__c> qp0List = new List<RQO_obj_qp0Grade__c> ();
            for (sObject item : objectListDML) {
                RQO_obj_qp0Grade__c qp0 = (RQO_obj_qp0Grade__c) item;
                qp0List.add(qp0);
            }
            f = RQO_obj_qp0Grade__c.Fields.RQO_fld_sku__c;
            srListResult = Database.upsert(qp0List, f, false);
        } else if (tipoObjeto.equals(Label.RQO_lbl_containerJunctionIntegracion)) {
            List<RQO_obj_containerJunction__c> contJunctionList = new List<RQO_obj_containerJunction__c> ();
            for (sObject item : objectListDML) {
                RQO_obj_containerJunction__c cju = (RQO_obj_containerJunction__c) item;
                contJunctionList.add(cju);
            }
            svListResult = Database.insert(contJunctionList, false);
        }
        
        // Iteracion del resultado de la operacion DML de grados
        if (srListResult != null && !srListResult.isEmpty()) {
            for (Database.UpsertResult sr : srListResult) {
                if (sr.isSuccess()) {
                    objetosOKSet.add(sr.getId());
                    System.debug('sr.getId(): '+sr.getId());
                }
                else {
                    for(Database.Error err : sr.getErrors()) {
                        logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(tipoObjeto, String.valueOf(err.getStatusCode()), 
                                                                                String.valueOf(err.getFields()), 
                                                                                err.getMessage(), null, RQO_cls_Constantes.BATCH_GRADOS);
                        if (logObject != null) {
                            this.logObjectList.add(logObject);
                        }
                        System.debug('Error en operación DML: '+String.valueOf(err.getFields()));
                    }
                }
            }
        }
        
        // Asociacion de registros correctos mediante External ID --> SF ID
        if (tipoObjeto.equals(Label.RQO_lbl_qp0GradeIntegracion)) {
            for (sObject item : objectListDML) {
                if (objetosOKSet.contains(item.Id)) {
                    System.debug('Asociacion tipoObjeto: '+tipoObjeto+ ' - SKU: ' +item.get('RQO_fld_sku__c'));
                    qp0GradosInsertOKMap.put(String.valueOf(item.get('RQO_fld_sku__c')), item.Id);
                }
            }
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return qp0GradosInsertOKMap;
    }

}