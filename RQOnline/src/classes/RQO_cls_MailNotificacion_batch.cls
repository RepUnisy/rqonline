/**
 *	@name: RQO_cls_MailNotificacion_batch
 *	@version: 1.0
 *	@creation date: 12/12/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase para el envio de mails de notificaciones
 */
global class RQO_cls_MailNotificacion_batch implements Database.Batchable<sObject>, Database.Stateful {

    // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_MailNotificacion_batch';
	
	// ***** VARIABLES ***** //
    private List<String> messageType = null;
	private List<RQO_obj_notification__c> dailyNotificationList = null;
	private List<RQO_obj_logProcesos__c> logProcesosList = null;
	private Map<String, String> entregaIdAuxMap = null;
	private Map<String, String> posPedidoIdAuxMap = null;
	private Map<Id, String> contactIdAuxMap = null;
	
	// ***** CONSTRUCTORES ***** //
    public RQO_cls_MailNotificacion_batch () {
		this.logProcesosList = new List<RQO_obj_logProcesos__c> ();
		this.messageType = new List<String> ();
		this.dailyNotificationList = new List<RQO_obj_notification__c> ();
		this.entregaIdAuxMap = new Map<String, String> ();
		this.posPedidoIdAuxMap = new Map<String, String> ();
		this.contactIdAuxMap = new Map<Id, String> ();
		
		// Consulta de parametrización de comunicaciones
		if (RQO_cls_CustomSettingUtil.getParametrizacionEnvioNotificaciones(RQO_cls_Constantes.COD_SAP_NOT_PEDIDO).RQO_fld_activo__c) {
			this.messageType.add(RQO_cls_Constantes.COD_PED_CONFIRMADO);
			this.messageType.add(RQO_cls_Constantes.COD_PED_TRATAMIENTO);
		}
		if (RQO_cls_CustomSettingUtil.getParametrizacionEnvioNotificaciones(RQO_cls_Constantes.COD_SF_NOT_GRADO).RQO_fld_activo__c) {
			this.messageType.add(RQO_cls_Constantes.COD_PROD_NOTA_TECNICA);
			this.messageType.add(RQO_cls_Constantes.COD_PROD_CERT_GRADO);
			this.messageType.add(RQO_cls_Constantes.COD_PROD_RECOM_GRADO);
			this.messageType.add(RQO_cls_Constantes.COD_PROD_NEW_GRADO);
		}
    }
    
    
    // ***** METODO GLOBALES ***** //

	/**
	* @creation date: 12/12/2017
	* @author: David Iglesias - Unisys
	* @description:	Realiza el envio de mails de notificaciones a los contactos
	* @param: Database.BatchableContext
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
    global Database.QueryLocator start(Database.BatchableContext context) {
		
		/**
        * Constantes
        */
        final String METHOD = 'start';

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		String query = 'SELECT Id, Name, RQO_fld_contact__c, RQO_fld_externalId__c, RQO_fld_mailSent__c, RQO_fld_messageType__c, RQO_fld_objectType__c '+
                        'FROM RQO_obj_notification__c ' +
            			'WHERE RQO_fld_mailSent__c = false AND RQO_fld_messageType__c IN :messageType '+ 
						'ORDER BY RQO_fld_objectType__c';
        
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
        return Database.getQueryLocator(query);
		
	}
	
	/**
	* @creation date: 12/12/2017
	* @author: David Iglesias - Unisys
	* @description:	Ejecucion por tramas del envio de mails de notificaciones
	* @param: Database.BatchableContext
	* @param: List<RQO_obj_notification__c>
	* @return: 	
	* @exception: 
	* @throws: 
	*/
    global void execute(Database.BatchableContext context, List<RQO_obj_notification__c> scope) {
        
        /**
        * Constantes
        */
        final String METHOD = 'execute';
	
		/**
		 * Variables
		 */
		String emailTemplate = null;
		RQO_cls_IntegrationEmailBean mailBean = null;
        RQO_obj_logProcesos__c logProcesos = null;
		Map<String, List<String>> externalIdByTypeObjectMap = new Map<String, List<String>> ();
		Messaging.SendEmailResult[] resultMailList = null;
		List<String> externalIdList = null;
		List<String> idContactList = new List<String> ();
		List<Contact> contactosList = null;
		List<RQO_cls_IntegrationEmailBean> mailBeanList = new List<RQO_cls_IntegrationEmailBean> ();
		Map<String, String> gradoIdMap = null;
		Map<String, String> gradoQP0IdMap = null;
		Map<String, String> entregaIdMap = null;
		Map<String, String> posPedidoIdMap = null;
		Map<Id, String> contactIdMap = null;
		Map<String, Map<String, RQO_obj_notificationConsent__c>> contactIdNotificationConsentsMap = null;
	
		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		try {
			
			// Ordenación con relación tipo de objeto - Id del registro del objeto
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Ordenación con relación tipo de objeto - Id del registro del objeto');
			for (RQO_obj_notification__c item : scope) {
								
				if (externalIdByTypeObjectMap.get(item.RQO_fld_objectType__c) == null) {
					externalIdList = new List<String> ();
				} else {
					externalIdList = externalIdByTypeObjectMap.get(item.RQO_fld_objectType__c);
				}
			
				externalIdList.add(item.RQO_fld_externalId__c);
				externalIdByTypeObjectMap.put(item.RQO_fld_objectType__c, externalIdList);
				System.debug(CLASS_NAME + ' - ' + METHOD + 'externalIdByTypeObjectMap: '+externalIdByTypeObjectMap);
				idContactList.add(item.RQO_fld_contact__c);
			
			}
			
			// Consultas para obtener los campos de los distintos tipos de objeto y ordenación según Id externo - Id interno
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Consultas para obtener los campos de los distintos tipos de objeto y ordenación según Id externo - Id interno');
			entregaIdMap = getRelationMap(externalIdByTypeObjectMap, RQO_cls_Constantes.COD_SF_NOT_ENTREGA);
			posPedidoIdMap = getRelationMap(externalIdByTypeObjectMap, RQO_cls_Constantes.COD_SAP_NOT_PEDIDO);
			
			// Asignamos valores a variables de clase para su uso posterior en la ejecución del envío de resumen (arranque en el finish)
            if (entregaIdMap != null) {
            	this.entregaIdAuxMap.putAll(entregaIdMap);
        	}
            if (posPedidoIdMap != null) {
            	this.posPedidoIdAuxMap.putAll(posPedidoIdMap);
        	}		
			
			/*
			if (externalIdByTypeObjectMap.get(RQO_cls_Constantes.COD_SF_NOT_DOCUMENTACION) != null) {
				gradoQP0IdMap = getFieldsId(externalIdByTypeObjectMap, RQO_cls_Constantes.COD_SF_NOT_DOCUMENTACION);
				System.debug(CLASS_NAME + ' - ' + METHOD + 'gradoQP0IdMap: '+gradoQP0IdMap);
			}
			
			if (externalIdByTypeObjectMap.get(RQO_cls_Constantes.COD_SF_NOT_GRADO) != null) {
				gradoIdMap = getFieldsId(externalIdByTypeObjectMap, RQO_cls_Constantes.COD_SF_NOT_GRADO);
				System.debug(CLASS_NAME + ' - ' + METHOD + 'gradoIdMap: '+gradoIdMap);
			}*/
			
			// Consultas para obtener los datos de los usuarios receptores de los mails y obtención de la configuración de envío de cada contacto
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Consultas para obtener los datos de los usuarios receptores de los mails');	
			if (idContactList != null && !idContactList.isEmpty()) {
				contactIdMap = new Map<Id, String> ();
				
				// Consulta de idioma configurado para cada usuario
				/*for (User itemUser : [SELECT LanguageLocaleKey, Contact.Id FROM User WHERE Contact.Id IN :idContactList]) {
					contactIdMap.put(itemUser.Contact.Id, itemUser.LanguageLocaleKey);
				}*/
				for (Contact itemContact : [SELECT RQO_fld_idioma__c, Id FROM Contact WHERE Id IN :idContactList]) {
					contactIdMap.put(itemContact.Id, itemContact.RQO_fld_idioma__c);
				}
				
				// Consulta para obtener la configuración de envío de cada contacto
				contactIdNotificationConsentsMap = getConfSendContact(idContactList);
				System.debug(CLASS_NAME + ' - ' + METHOD + 'contactIdNotificationConsentsMap: '+contactIdNotificationConsentsMap);

			}
			
			// Asignamos valores a variables de clase para su uso posterior en la ejecución del envío de resumen (arranque en el finish)
			this.contactIdAuxMap.putAll(contactIdMap);
			
			// Iteración y tratado de los mails custom a enviar
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Iteración y tratado de los mails custom a enviar');
			for (RQO_obj_notification__c item : scope) {
				
				// Verificar el consentimiento del contacto para enviar el correo
				System.debug(CLASS_NAME + ' - ' + METHOD + ': Verificar el consentimiento del contacto para enviar el correo ' + item.Name);
				//List<RQO_obj_notificationConsent__c> consents = 
    			//	new RQO_cls_NotificationConsentSelector().selectByType(new Set<ID> { item.RQO_fld_contact__c }, item.RQO_fld_messageType__c);
				RQO_obj_notificationConsent__c consents = null;
				
				if (contactIdNotificationConsentsMap != null && contactIdNotificationConsentsMap.get(item.RQO_fld_contact__c) != null) {
					consents = contactIdNotificationConsentsMap.get(item.RQO_fld_contact__c).get(item.RQO_fld_messageType__c);
					System.debug(CLASS_NAME + ' - ' + METHOD + 'consents: '+consents);
				}
    		
				if (consents != null && consents.RQO_fld_emailSending__c) {
					
					// Comprobación de envío inmediato
					if (Label.RQO_lbl_emailSendingPeriodicityPromptIntegration.equalsIgnoreCase(consents.RQO_fld_emailSendingPeriodicity__c)) {
					
						mailBean = new RQO_cls_IntegrationEmailBean();
						
						mailBean.contact = item.RQO_fld_contact__c;
						
						// Mapeo de códigos del idioma del usuario con el mail template
						String languageTemplate = RQO_cls_Constantes.languageMap.get(contactIdMap.get(item.RQO_fld_contact__c));
						
						if (item.RQO_fld_objectType__c.equalsIgnoreCase(RQO_cls_Constantes.COD_SAP_NOT_PEDIDO)) {
							mailBean.idObject = posPedidoIdMap.get(item.RQO_fld_externalId__c);
							
							// Mapeo de templates de correo
							System.debug(CLASS_NAME + ' - ' + METHOD + ': Mapeo de templates de correo posPedidoIdMap');
							mailBean.templateName = RQO_cls_Constantes.MAIL_TMP_TRATAMIENTO + languageTemplate;
						} else if (item.RQO_fld_objectType__c.equalsIgnoreCase(RQO_cls_Constantes.COD_SF_NOT_DOCUMENTACION) || 
										item.RQO_fld_objectType__c.equalsIgnoreCase(RQO_cls_Constantes.COD_SF_NOT_GRADO)) {
							mailBean.idObject = item.RQO_fld_externalId__c;
							
							// Mapeo de templates de correo
							System.debug(CLASS_NAME + ' - ' + METHOD + ': Mapeo de templates de correo Grados/QP0');
							if (RQO_cls_Constantes.COD_PROD_NOTA_TECNICA.equalsIgnoreCase(item.RQO_fld_messageType__c)) {
								mailBean.templateName = RQO_cls_Constantes.MAIL_TMP_ACT_NT + languageTemplate;
							} else if (RQO_cls_Constantes.COD_PROD_CERT_GRADO.equalsIgnoreCase(item.RQO_fld_messageType__c)) {
								mailBean.templateName = RQO_cls_Constantes.MAIL_TMP_ACT_CG + languageTemplate;
							} else if (RQO_cls_Constantes.COD_PROD_RECOM_GRADO.equalsIgnoreCase(item.RQO_fld_messageType__c)) {
								mailBean.templateName = RQO_cls_Constantes.MAIL_TMP_REC_GRADE + languageTemplate;
							} else if (RQO_cls_Constantes.COD_PROD_NEW_GRADO.equalsIgnoreCase(item.RQO_fld_messageType__c)) {
								mailBean.templateName = RQO_cls_Constantes.MAIL_TMP_NEW_GRADE + languageTemplate;
							}
						} else if (item.RQO_fld_objectType__c.equalsIgnoreCase(RQO_cls_Constantes.COD_SF_NOT_ENTREGA)) {
							mailBean.idObject = entregaIdMap.get(item.RQO_fld_externalId__c);
							
							// Mapeo de templates de correo
							System.debug(CLASS_NAME + ' - ' + METHOD + ': Mapeo de templates de correo entregaIdMap');
							mailBean.templateName = RQO_cls_Constantes.MAIL_TMP_ESTADO + languageTemplate;
						}
						
						// Actualizacion de flag para mail tratado
						System.debug(CLASS_NAME + ' - ' + METHOD + ': Actualizacion de flag para mail tratado');
						item.RQO_fld_mailSent__c = true;
						
						// Mails a enviar
						System.debug(CLASS_NAME + ' - ' + METHOD + ': Añadir a mails a enviar');
						mailBeanList.add(mailBean);
						System.debug(CLASS_NAME + ' - ' + METHOD + ': Mails a enviar: '+mailBeanList);
						
					} else {
						this.dailyNotificationList.add(item);
					}
				} else {
					item.RQO_fld_mailSent__c = true;
				}
			}
			
			// Envío de mails
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Envío de mails');
			if (mailBeanList != null && !mailBeanList.isEmpty()) {
				
				System.debug(CLASS_NAME + ' - ' + METHOD + ': Numero de correos ' + mailBeanList.size());
				
				resultMailList = RQO_cls_MailUtils.sendMail(mailBeanList);
				
				// Comprobación de resultado del envío de mails
				System.debug(CLASS_NAME + ' - ' + METHOD + ': Comprobación de resultado del envío de mails');
				for (Messaging.SendEmailResult item : resultMailList) {
	                if (!item.IsSuccess()) {
	                    Messaging.SendEmailError[] errArr = item.getErrors(); 
						for (Messaging.SendEmailError itemAux : errArr) {
							logProcesos = RQO_cls_ProcessLog.instanceInitalLogBatch(String.valueOf(RQO_obj_notification__c.getSobjectType()), String.valueOf(itemAux.getStatusCode()), 
																						Label.RQO_lbl_msgErrorNotificacionInt+itemAux.getTargetObjectId(), itemAux.getMessage(), 
																						itemAux.getTargetObjectId(), RQO_cls_Constantes.SERVICE_SEND_NOTIFICATION);
							logProcesosList.add(logProcesos);
						}	
	                }
	            }
				
				// Actualización de registro de notificación
				System.debug(CLASS_NAME + ' - ' + METHOD + ': Actualización de registro de notificación');
				update scope;
			}			
		} catch (Exception e) {
			System.debug('Excepción generica: '+e.getTypeName()+' - '+e.getMessage()+' - '+e.getLineNumber());
			logProcesos = RQO_cls_ProcessLog.instanceInitalLogBatch(String.valueOf(RQO_obj_notification__c.getSobjectType()), e.getTypeName(), 
																		Label.RQO_lbl_mensajeErrorExceptionGenericoIntegracion, e.getMessage(), null, 
																		RQO_cls_Constantes.SERVICE_SEND_NOTIFICATION);
			logProcesosList.add(logProcesos);
		}

		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	
	}
	
	
	
	global void finish(Database.BatchableContext context) {
		
		/**
        * Constantes
        */
        final String METHOD = 'finish';
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		// Inserción del log de errores
		if (!logProcesosList.isEmpty()) {
			insert logProcesosList;
		}
		if (!Test.isRunningTest()) {
            Database.executeBatch(new RQO_cls_MailNotificacionResumen_batch(this.dailyNotificationList, this.contactIdAuxMap, this.posPedidoIdAuxMap, this.entregaIdAuxMap), 1);
        }
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
	}
	
	// ***** METODO PRIVADOS ***** //
	
    /**
    * @creation date: 11/01/2018
    * @author: David Iglesias - Unisys
    * @description: Consultas para obtener los campos de los distintos tipos de objeto y ordenación según Id externo - Id interno
	* @param: externalIdByTypeObjectMap			Id´s a buscar segun el tipo de objeto a tratar
	* @param: tipo								Tipo de objeto a tratar
    * @return: Map<String, String> 				Mapa de retorno
    * @exception: 
    * @throws: 
    */
	private static Map<String, String> getFieldsId (Map<String, List<String>> externalIdByTypeObjectMap, String tipo) {
		
        /**
        * Constantes
        */
        final String METHOD = 'getFieldsId';
	
		/**
		 * Variables
		 */
		Map<String, String> fieldsIdMap = new Map<String, String> ();
	
		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		if (RQO_cls_Constantes.COD_SAP_NOT_PEDIDO.equalsIgnoreCase(tipo)) {
			for (Asset posPedido : [SELECT Id, RQO_fld_idExterno__c 
										FROM Asset 
										WHERE RQO_fld_idExterno__c IN :externalIdByTypeObjectMap.get(RQO_cls_Constantes.COD_SAP_NOT_PEDIDO)]) {
				fieldsIdMap.put(posPedido.RQO_fld_idExterno__c, posPedido.Id);
			}
		} /*else if (RQO_cls_Constantes.COD_SF_NOT_DOCUMENTACION.equalsIgnoreCase(tipo)) {
			for (RQO_obj_qp0Grade__c gradoQP0 : [SELECT Id, RQO_fld_sku__c 
												FROM RQO_obj_qp0Grade__c 
												WHERE RQO_fld_sku__c IN :externalIdByTypeObjectMap.get(RQO_cls_Constantes.COD_SF_NOT_DOCUMENTACION)]) {
				fieldsIdMap.put(gradoQP0.RQO_fld_sku__c, gradoQP0.Id);
			}
		}*/ else if (RQO_cls_Constantes.COD_SF_NOT_ENTREGA.equalsIgnoreCase(tipo)) {
			
			/*
			** Se comenta debido a que ya no se informa/muestra el numero de entrega en la notificaciones, si no de el número de pedido
			for (RQO_obj_entrega__c entrega : [SELECT Id, RQO_fld_idExterno__c 
												FROM RQO_obj_entrega__c 
												WHERE RQO_fld_idExterno__c IN :externalIdByTypeObjectMap.get(RQO_cls_Constantes.COD_SF_NOT_ENTREGA)]) {
				fieldsIdMap.put(entrega.RQO_fld_idExterno__c, entrega.Id);
			}
			*/
			for (RQO_obj_posiciondeEntrega__c poscicionEntrega : [SELECT Id, RQO_fld_idRelacion__r.RQO_fld_idExterno__c
																	FROM RQO_obj_posiciondeEntrega__c 
																	WHERE RQO_fld_idRelacion__r.RQO_fld_idExterno__c IN :externalIdByTypeObjectMap.get(RQO_cls_Constantes.COD_SF_NOT_ENTREGA)]) {
				fieldsIdMap.put(poscicionEntrega.RQO_fld_idRelacion__r.RQO_fld_idExterno__c, poscicionEntrega.Id);
			}
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return fieldsIdMap;
		
	}
	
    /**
    * @creation date: 11/01/2018
    * @author: David Iglesias - Unisys
    * @description: Consultas para obtener los campos de los distintos tipos de objeto y ordenación según Id externo - Id interno
	* @param: 	idContactList													Id´s de contactos
    * @return: Map<String, Map<String, RQO_obj_notificationConsent__c>> 		Mapa de retorno
    * @exception: 
    * @throws: 
    */
	private static Map<String, Map<String, RQO_obj_notificationConsent__c>> getConfSendContact (List<String> idContactList) {
		
        /**
        * Constantes
        */
        final String METHOD = 'getConfSendContact';
	
		/**
		 * Variables
		 */
		Map<String, Map<String, RQO_obj_notificationConsent__c>> contactIdNotificationConsentsMap = new Map<String, Map<String, RQO_obj_notificationConsent__c>> ();
		Map<String, RQO_obj_notificationConsent__c> permisionMap = null;
	
		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		for (Contact itemContacto : [SELECT Id, (SELECT RQO_fld_notificationType__c, RQO_fld_emailSending__c, RQO_fld_emailSendingPeriodicity__c FROM Notification_Consents__r) 
										FROM Contact WHERE Id IN :idContactList]) {

			permisionMap = new Map<String, RQO_obj_notificationConsent__c> ();

			for (RQO_obj_notificationConsent__c itemConsent : itemContacto.Notification_Consents__r) {
				permisionMap.put(itemConsent.RQO_fld_notificationType__c, itemConsent);
			}
			contactIdNotificationConsentsMap.put(itemContacto.Id, permisionMap);
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return contactIdNotificationConsentsMap;
		
	}
	
	/**
    * @creation date: 01/02/2018
    * @author: David Iglesias - Unisys
    * @description: Consultas para obtener los campos de los distintos tipos de objeto y ordenación según Id externo - Id interno
	* @param: externalIdByTypeObjectMap			Id´s a buscar segun el tipo de objeto a tratar
	* @param: tipo								Tipo de objeto a tratar
    * @return: Map<String, String> 				Mapa de retorno
    * @exception: 
    * @throws: 
    */
	private static Map<String, String> getRelationMap (Map<String, List<String>> externalIdByTypeObjectMap, String tipo) {
		
        /**
        * Constantes
        */
        final String METHOD = 'getRelationMap';
	
		/**
		 * Variables
		 */
		Map<String, String> returnMap = null;
	
		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		if (RQO_cls_Constantes.COD_SF_NOT_ENTREGA.equalsIgnoreCase(tipo) && externalIdByTypeObjectMap.get(tipo) != null) {
			returnMap = getFieldsId(externalIdByTypeObjectMap, RQO_cls_Constantes.COD_SF_NOT_ENTREGA);
			System.debug(CLASS_NAME + ' - ' + METHOD + 'entregaIdMap: '+returnMap);
		}
		
		if (RQO_cls_Constantes.COD_SAP_NOT_PEDIDO.equalsIgnoreCase(tipo) && externalIdByTypeObjectMap.get(tipo) != null) {
			returnMap = getFieldsId(externalIdByTypeObjectMap, RQO_cls_Constantes.COD_SAP_NOT_PEDIDO);
			System.debug(CLASS_NAME + ' - ' + METHOD + 'posPedidoIdMap: '+returnMap);
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return returnMap;
		
	}
    
}