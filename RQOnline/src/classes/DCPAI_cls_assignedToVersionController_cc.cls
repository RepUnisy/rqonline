/*------------------------------------------------------------------------
 * 
 *   @name:         DCPAI_cls_assignedToVersionController_cc
 *   @version:       1 
 *   @creation date:   30/06/2017
 *   @author:       Alfonso Constán López  -  Unisys
 *   @description:     Custom controller of visualforce page "DCPAI_vf_assignedToVersion"
 * 
----------------------------------------------------------------------------*/
public class DCPAI_cls_assignedToVersionController_cc {    
    
    private List<ITPM_Version__c> listBudgetInvestment = new List<ITPM_Version__c>();
    private List<SObject> listCostItem = new List<ITPM_Budget_Item__c>();
    private Map<String, List<SObject>> mapCostItemDraft = new Map<String, List<SObject>>();
    private Map<String, List<ITPM_Version__c>> mapUBCObtenido = new Map<String, List<ITPM_Version__c>>();
    private List<ITPM_Version__c> listUBCAlmacenado = new List<ITPM_Version__c>();
    private SObject totalVersion ;
    private Map<String, String> listCost = new Map<String, String>();
    private boolean admin;
    private Integer offset = 0;
    private Integer numberPage = 1;
    private Integer recordPage = 100;
    
    //constructor
    public DCPAI_cls_assignedToVersionController_cc(ApexPages.StandardController controller) {
        initialData();
    }   
    
    private void initialData(){
        offset = recordPage * (numberPage - 1);
        admin = DCPAI_cls_GlobalClass.isAdmin();
        mapCostItemDraft = new Map<String, List<SObject>>();
        
        totalVersion = [Select SUM(ITPM_VER_FOR_Total_investment__c) totalInvest, SUM(ITPM_VER_FOR_Total_expense__c )totalExp
                         from ITPM_Version__c where DCPAI_fld_Budget_Version_CostItem__r.DCPAI_fld_Investment_Plan_Version__c = : ApexPages.currentPage().getParameters().get('id')];                                                                                                
        
        //  Obtenemos todas las iniciativas assigned
        listBudgetInvestment = [select id, 
                                Name, 
                                DCPAI_fld_Name__c,                             
                                ITPM_VER_SEL_Responsible_IT_Direction__c, 
                                DCPAI_fld_Segmentation__c,
                                DCPAI_fld_Decision_Area__c,
                                ITPM_VER_SEL_Responsible_Area__c,
                                ITPM_VER_SEL_Responsible_Subarea__c, 
                                ITPM_VER_TX_Description__c
                                from ITPM_Version__c 
                                where DCPAI_fld_Investment_Plan_Version__c = : ApexPages.currentPage().getParameters().get('id')
                                LIMIT :recordPage OFFSET :offset
                               ];

        //  Obtenemos todos los cost item relaccionados
        listCostItem = [Select ITPM_VER_SEL_Country__c,                          
                        ITPM_VER_SEL_Year_validity__c,
                        SUM(ITPM_VER_FOR_Total_investment__c) totalInv, 
                        SUM(ITPM_VER_FOR_Total_expense__c) totalExp,
                        DCPAI_fld_Budget_Version_CostItem__c,
                        DCPAI_fld_Code_of_project__c
                        from ITPM_Version__c where DCPAI_fld_Budget_Version_CostItem__r.DCPAI_fld_Investment_Plan_Version__c = : ApexPages.currentPage().getParameters().get('id')
                        GROUP BY ITPM_VER_SEL_Country__c, DCPAI_fld_Budget_Version_CostItem__c, ITPM_VER_SEL_Year_validity__c, DCPAI_fld_Code_of_project__c
                        ORDER BY DCPAI_fld_Budget_Version_CostItem__c];                   
            
        //  Obtenemos la lista de UBCs
        List<ITPM_Version__c> listaUBC = [select id,
                                              name, 
                                              ITPM_VER_SEL_Year_validity__c, 
                                              ITPM_VER_SEL_Country__c, 
                                              DCPAI_fld_Percent__c, 
                                              DCPAI_fld_UBC_Name__c,
                                              DCPAI_fld_Code_of_project__c,
                                              DCPAI_fld_Budget_Version_UBC__C
                                              from ITPM_Version__c where DCPAI_fld_Budget_Version_UBC__c IN :listBudgetInvestment 
                                              order by ITPM_VER_SEL_Year_validity__c, ITPM_VER_SEL_Country__c];
        
        system.debug('listaUBC ' + listaUBC);
        mapUBCObtenido = DCPAI_cls_GlobalClass.DCPAI_getMapUBCsVersion(listaUBC);
                
        budgetDetailMap = new Map<String, List<SObject>>();    
        
        List<SObject> listBudget = new List<SObject>();
        //  Relacción budget (version) - ubc (version)
        for (SObject bud : listCostItem ){
            String code = String.valueOf(bud.get('DCPAI_fld_Code_of_project__c'));
            if (code == 'null' || code == null){
                code = '';
            }
            String key = (String)bud.get('DCPAI_fld_Budget_Version_CostItem__c') + (String)bud.get('ITPM_VER_SEL_Country__c') + (String)bud.get('ITPM_VER_SEL_Year_validity__c')+ code;
            
            //  Obtenemos la lista de ubc perteneciente a cada budget.
            List<ITPM_Version__c> listUBC = mapUBCObtenido.get(key);
            if (listUBC != null){                
                for (ITPM_Version__c ubcAgrup : listUBC){
                    listUBCAlmacenado.add(ubcAgrup);
                    listBudget.add(bud);
                    calculate(bud, ubcAgrup);
                } 
            }
            else{
                List<ITPM_Version__c> listUbcCreada = new List<ITPM_Version__c>();                
                ITPM_Version__c ubcVacia = new ITPM_Version__c();
                listUbcCreada.add(ubcVacia);
                
                mapUBCObtenido.put(key, listUbcCreada);
                listUBCAlmacenado.add(new ITPM_Version__c());
                listBudget.add(bud);
                //calculate(bud, null);
            }
        }
        for(SObject bud: listCostItem){
            List<SObject> listaDet = new List<SObject>();
            if (budgetDetailMap.containsKey(String.valueOf(bud.get('DCPAI_fld_Budget_Version_CostItem__c')))){
                listaDet = budgetDetailMap.get(String.valueOf(bud.get('DCPAI_fld_Budget_Version_CostItem__c')));
                listaDet.add(bud);                       
            }
            else{
                listaDet.add(bud);                      
            }    
            mapCostItemDraft.put( String.valueOf(bud.get('DCPAI_fld_Budget_Version_CostItem__c')), listaDet);  
        }
        System.debug('mapCostItemDraft: ' + mapCostItemDraft);
            
}
    
    /*
     *   @creation date:     30/06/2017
     *   @author:         Alfonso Constán López  -  Unisys
     *  @description:      Versiona todos los budget, cost item y ubcs relaccionadas con el plan
     *  @param1:        bud: agrupación de costes
     *   @param2:        ubcAgrup listado de ubc versionado
  */  
    private void calculate(SObject bud, ITPM_Version__c ubcAgrup){
        
        Decimal costExpense = (Decimal)bud.get('totalExp');
        Decimal costInvestment = (Decimal) bud.get('totalInv');
        Decimal costPro = costExpense + costInvestment;
        
        costPro = (costPro * Decimal.valueOf(ubcAgrup.DCPAI_fld_Percent__c) / 100).setScale(2);

        String costProStr = costPro.format();
        listCost.put(ubcAgrup.Id, costProStr);
    }
    
    
    public List<ITPM_Version__c> listBudgetUser {
        get {
            return listBudgetInvestment;                        
        }
        set;
    }
  
    public Map<String, List<SObject>> budgetDetailMap {
        get {
            return mapCostItemDraft;
        }
        set;
    } 
    
    public Map<String, List<ITPM_Version__c>> mapUBC{
        get{
            return mapUBCObtenido;
        }
        set;
    }
    
    public String totalBudget{
        get{
            List<ITPM_Version__c> totalList = [select id, 
                                                            Name, 
                                                            DCPAI_fld_Name__c,                             
                                                            ITPM_VER_SEL_Responsible_IT_Direction__c, 
                                                            DCPAI_fld_Segmentation__c,
                                                            DCPAI_fld_Decision_Area__c,
                                                            ITPM_VER_SEL_Responsible_Area__c,
                                                            ITPM_VER_SEL_Responsible_Subarea__c, 
                                                            ITPM_VER_TX_Description__c
                                                            from ITPM_Version__c 
                                                            where DCPAI_fld_Investment_Plan_Version__c = : ApexPages.currentPage().getParameters().get('id')
                                                           ];
            return String.valueOf(totalList.size());
        }
        set;
    }
    
    public String totalInvestment {
        get {
            return (Decimal.valueOf(String.valueOf(totalVersion.get('totalInvest')))).format();
        }
        set;
    }

    public String totalExpense {
        get {
            return (Decimal.valueOf(String.valueOf(totalVersion.get('totalExp')))).format();
        }
        set;
    }

    public String totalCost {
        get {
            return (Decimal.valueOf(String.valueOf(totalVersion.get('totalInvest'))) + Decimal.valueOf(String.valueOf(totalVersion.get('totalExp')))).format();
        }
        set;
    }

    public boolean isAdmin {
        get {
            return admin;
        }
        set;
    }

    
    public  Map<String, String> listCostFinal{
        get{
            return listCost;
        }
        set;
    }
    
    public String reportUbc {
        get{
            List<Report> report = [select id from report where name = : Label.DCPAI_Report_Ubc_Csv ];
            Id idreport;
             if (report.size() != 0){
                idreport = report.get(0).id;
                return idreport + '?pv0=' + ApexPages.currentPage().getParameters().get('id');
            }
            else{
                return null;
            }
            
        }
        set;
    }  
    public String reportPA {
        get{
             List<Report> report = [select id from report where name = : Label.DCPAI_csv_PA ];
            Id idreport;
            if (report.size() != 0){
                idreport = report.get(0).id;
                return idreport + '?pv0=' + ApexPages.currentPage().getParameters().get('id');
            }
            else{
                return null;
            }
        }
        set;
    } 
    
    public Pagereference exportAll(){    
        Pagereference pageref = new Pagereference('/apex/DCPAI_vf_exportExcelVersion');
        pageref.getParameters().put('id', ApexPages.currentPage().getParameters().get('id'));
        pageref.getParameters().put('procedencia', 'version');
        return pageref;        
    }
    
    public Integer totalNumberPage  {   
        get{
            
            Decimal pag = Decimal.valueOf(totalBudget)/recordPage;
            Integer numPag = pag.intValue();          
            if (pag.scale() != 0){
                numPag ++;
            }
            return numPag;
        }
        set;    
    }   
    public Integer actualPag  {
        get{
            return numberPage;
        }
        set{
            
        }
    }

    public PageReference Next() {   
        numberPage++;
        initialData();
        return null;        
    }
    public PageReference First() {   
        numberPage = 1;
        initialData();
        return null;        
    } 
    public PageReference Previous() {   
        numberPage--;
        initialData();
        return null;        
    }   
    public PageReference Last() {   
        numberPage = totalNumberPage;
        initialData();
        return null;        
    }  

   
    public boolean disabledPrevious{
        get{
            if (numberPage == 1){
                return false;
            }
            else{
                return true;
            }
        }
    }
    public boolean disabledNext{
        get{
            if (numberPage == totalNumberPage || totalNumberPage == 0){
                return false;
            }
            else{
                return true;
            }
        }
    }

}