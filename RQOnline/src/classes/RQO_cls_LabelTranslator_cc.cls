/**
 *	@name: RQO_cls_LabelTranslator_cc
 *	@version: 1.0
 *	@creation date: 13/02/2018
 *	@author: David Iglesias - Unisys
 *	@description: Clase controladora de la visualforce RQO_vf_LabelTranslator
 */
public class RQO_cls_LabelTranslator_cc {
    
    // ***** VARIABLES ***** //
	public String label_lang {get;set;}
    public String label {get;set;}
     
    // ***** CONSTRUCTORES ***** //
    public  RQO_cls_LabelTranslator_cc(){
       Map<String, String> reqParams = ApexPages.currentPage().getParameters(); 
       label_lang = reqParams.get('label_lang');
       label = reqParams.get('label');
    }
}