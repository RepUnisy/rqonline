/**
*	@creation date: 15/2/2018
*	@author: Juan Elías - Unisys
*   @description: Clase test para cubrir la clase RQO_cls_WSCallout
*   @exception: 
*   @throws: 
*/
@IsTest
public class RQO_cls_WSCallout_test {
    
    /**
*	@name: RQO_cls_WSCallout_test
*	@version: 1.0
*	@creation date: 15/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar los métodos asociados a RQO_cls_WSCallout
*/
    
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
    }
    
    /**
*	@creation date: 15/2/2018
*	@author: Juan Elías - Unisys
*   @description: Método test para probar el método send() con respuesta correcta
*   @exception: 
*   @throws: 
*/
    
    @isTest
    static void testRecibirConfirmacionPedidoCreado(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            Map<String, String> header = new  Map<String, String>();
            header.put('key', 'value');
            RQO_cls_WSCallout wSCallout2 = new RQO_cls_WSCallout('url', 'method', 'body', header);
            Test.setMock(HttpCalloutMock.class, new RQO_cls_WSCalloutTestMockOK(200));
            test.startTest();
            RQO_cls_WSCalloutResponseBean resultado = wSCallout2.send();
            System.assertEquals('200', resultado.estado); 
            test.stopTest();
        }
    }
    
    /**
*	@creation date: 15/2/2018
*	@author: Juan Elías - Unisys
*   @description: Método test para probar el método send() con respuesta erronea
*   @exception: 
*   @throws: 
*/
    
    @isTest
    static void testRecibirConfirmacionPedidoCreadoException(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            Map<String, String> header = new  Map<String, String>();
            header.put('key', 'value');
            RQO_cls_WSCallout wSCallout2 = new RQO_cls_WSCallout('url', 'method', 'body', header);
            Test.setMock(HttpCalloutMock.class, new RQO_cls_WSCalloutTestMockOK(404));
            test.startTest();
            RQO_cls_WSCalloutResponseBean resultado = wSCallout2.send();
            System.assertEquals(null, resultado.estado);
            test.stopTest();
        }
    }  
    
    /**
*	@creation date: 15/2/2018
*	@author: Juan Elías - Unisys
*   @description: Método test para probar el método send() con respuesta erronea
*   @exception: 
*   @throws: 
*/
    
    @isTest	
    static void testRecibirConfirmacionPedidoCreadoEx(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            Map<String, String> header = new  Map<String, String>();
            RQO_cls_WSCallout wSCallout = new RQO_cls_WSCallout('url');
            test.startTest();
            RQO_cls_WSCalloutResponseBean resultado = wSCallout.send();
            System.assertEquals(null, resultado.contenido);
            test.stopTest();
        }
    }
}