/**
 * @name: RQO_cls_handlerAfterContact
 * @version: 1.0
 * @creation date: 14/01/2018
 * @author: Unisys
 * @description: Clase Handler para Trigger After Insert Update de Contact - Actualiza los consentimientos de notificaciones
*/

public class RQO_cls_handlerAfterContact {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_handlerAfterContact';
    
    /* @creation date: 14/01/2018
     * @author: Unisys
     * @description: Método handler que actualiza los consentimientos de notificaciones del contacto
     * @param: Trigger.new y Trigger.old en las variables newContacts y oldContacts
     * @return: void
     * @exception: No aplica
     * @throws: No aplica */
    public static void handlerAfterContact (Boolean isInsert, List<Contact> newContacts, Map<Id,Contact> oldContacts) {
    	
    	// ***** CONSTANTES ***** //
        final String METHOD = 'handlerAfterInsertCase';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': newContacts ' + newContacts);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': oldContacts ' + oldContacts);
        
        for (Contact newContact : newContacts)
        {
        	Contact oldContact = newContact;
        	if (oldContacts != null)
        		oldContact = oldContacts.get(newContact.Id);
	        // Si se ha modificado algún consentiiento
			if ((newContact != null && oldContact == null &&
				(newContact.RQO_fld_corpInfConsent__c || newContact.RQO_fld_commInfConsent__c || newContact.RQO_fld_techInfConsent__c || newContact.RQO_fld_surveyConsent__c)) ||
				(newContact != null && oldContact != null &&
				(newContact.RQO_fld_corpInfConsent__c != oldContact.RQO_fld_corpInfConsent__c ||
				newContact.RQO_fld_commInfConsent__c != oldContact.RQO_fld_commInfConsent__c ||
				newContact.RQO_fld_techInfConsent__c != oldContact.RQO_fld_techInfConsent__c ||
				newContact.RQO_fld_surveyConsent__c != oldContact.RQO_fld_surveyConsent__c)))
			{
	        
				List<RQO_obj_notificationConsent__c> consentsToInsert = new List<RQO_obj_notificationConsent__c>();
				List<RQO_obj_notificationConsent__c> consentsToDelete = new List<RQO_obj_notificationConsent__c>();
				List<RQO_cmt_notificationCategories__mdt> categories = new RQO_cls_NotificationCategorySelector().selectAll();
				for (RQO_cmt_notificationCategories__mdt cat : categories)
				{
					// Consultar el consentimiento
					List<RQO_obj_notificationConsent__c> consents = new RQO_cls_NotificationConsentSelector().
						selectByType(new Set<ID> { newContact.Id }, cat.RQO_fld_notificationType__c);
						
					if ((newContact.RQO_fld_corpInfConsent__c && (oldContact == null || newContact.RQO_fld_corpInfConsent__c != oldContact.RQO_fld_corpInfConsent__c) && cat.RQO_fld_notificationConsent__c == 'Corporate Information') ||
						(newContact.RQO_fld_commInfConsent__c && (oldContact == null || newContact.RQO_fld_commInfConsent__c != oldContact.RQO_fld_commInfConsent__c) && cat.RQO_fld_notificationConsent__c == 'Commercial Information') ||
						(newContact.RQO_fld_techInfConsent__c && (oldContact == null || newContact.RQO_fld_techInfConsent__c != oldContact.RQO_fld_techInfConsent__c) && cat.RQO_fld_notificationConsent__c == 'Technical Information') ||
						(newContact.RQO_fld_surveyConsent__c && (oldContact == null || newContact.RQO_fld_surveyConsent__c != oldContact.RQO_fld_surveyConsent__c) && cat.RQO_fld_notificationConsent__c == 'Surveys'))
					{
						if (consents.size() == 0)
						{
							// Crear el consentimiento
							List<SelectOption> periodicity = new RQO_cls_GeneralUtilities().getNotificationPeriodicity();
							consentsToInsert.add(new RQO_obj_notificationConsent__c(Name = newContact.Name, RQO_fld_Contact__c = newContact.Id, RQO_fld_emailSending__c = false, RQO_fld_emailSendingPeriodicity__c = periodicity[0].getValue(),  RQO_fld_notificationType__c=cat.RQO_fld_notificationType__c));
						}
					}
					else
					{
						if (consents.size() > 0)
						{
							// Borrar el consentimiento
							consentsToDelete.add(consents[0]);
						}
					}
				}
				delete consentsToDelete;
				insert consentsToInsert;
			}

        }
        
		// Enviar correo si es contacto nuevo
		if (isInsert)
		{
			RQO_cls_UserService.sendNewRegistrationEmails((new Map<Id,SObject>(newContacts)).keySet());
		}
    }
}