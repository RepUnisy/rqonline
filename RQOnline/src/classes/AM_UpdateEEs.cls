/* 
-----------------------------------------------------------------------------------------------------------------------------------------------
Author:         Luis Morena Nevado
Company:        Indra
Description:    Actualizar los datos de las estaciones de servicio con los datos precargados 
Test Class:     AM_UpdateEEs_Test.apxc
History:
<Date>                             <Author>                                            <Change Description>
27/12/2016                       Luis Morena Nevado                                   Actualización estaciones 
-----------------------------------------------------------------------------------------------------------------------------------------------
*/

public class AM_UpdateEEs{  
    public AM_UpdateEEs (){}
    
    /*---------------------------------------------------------------------------------------------------------------------------
    Author:         Luis Morena Nevado
    Company:        Indra
    Description:    Método general, encargado de actualizar las estaciones de servicio, con los datos precargados.
    IN:             N/A
    OUT:            N/A
    History
    <Date>          <Author>                                  <Description>
    27/11/2016      Luis Morena Nevado                       Initial Version
    -----------------------------------------------------------------------------------------------------------------------------*/ 
    public void updatingEEs (){   
        Boolean isUpdated = true;     
        UserRole uRole = new UserRole ();
        UserRole nUserRole = new UserRole ();
        User newUser = new User();
        UserRole deleg = new UserRole();
        UserRole deleg_n = new UserRole();
        String parentRoleUser;
        User oUser  = new User();
        String parentRole;
        String parentRoleDP0;
        String userAdminName= '';
        
        String pbltyp = '';
        String niels = '';
                    
        try{          
            // se crea un mapa con las redes disponibles
            List <AM_Red__c> redes = [SELECT Id, Name, AM_CG_TPSEDE__c, AM_CG_MARCA__c, AM_DSCR__c, AM_Nombre_Red__c  FROM AM_Red__c];
            Map<String,AM_Red__c> AM_red_Map = new Map<String, AM_Red__c>();
            String keyRed = '';
            for (AM_Red__c red:redes){
                keyRed = red.AM_CG_TPSEDE__c + red.AM_CG_MARCA__c;
                AM_red_Map.put(keyRed, red);                    
            }
            
            List <UserRole> roles = [SELECT Id, Name, ParentRoleId FROM UserRole];
            Map<String,UserRole> TR_userRoleMap = new Map<String, UserRole>();
          
            // Obtenemos el mapa de los roles de usuario...
            for (UserRole userRole:roles){
                TR_userRoleMap.put(userRole.Id, userRole);
            }
           
            List <User> usuarios = [SELECT Id, Username, LastName, FirstName, Name, Id_Usuario_AM__c, UserRoleId FROM User where Id_Usuario_AM__c != null];
            Map<String,User> AM_users_maps_IdUAm = new Map<String, User>();
            Map<String,User> AM_users_maps_Role = new Map<String, User>();
          
            // Obtenemos el mapa de los usuarios...
            for (User usuario:usuarios){
                AM_users_maps_IdUAm.put(usuario.Id_Usuario_AM__c, usuario);
            }
            
            // Obtenemos el mapa de los usuarios...
            for (User usuario:usuarios){
                AM_users_maps_Role.put(usuario.UserRoleId, usuario);
            }

            // Se realiza la actualizazión de las estaciones de servicio 
            for (List <AM_Estacion_de_servicio__c> AM_estaciones_de_servicio : [SELECT Id, Name, AM_Pbltyp__c, AM_ZzdCredCodusua__c, AM_Vkgrp__c, AM_Niels__c, AM_Sortl__c,  AM_Operativa_no_operativa__c, AM_Codigo_G__c, AM_Delegacion_Jerarquia__c, AM_Nombre_ES__c, AM_NU_Ventas_ano_anterior__c, AM_TX_Nombre_sede_comercial__c, AM_NU_KNVV_ZZGRUPOES__c, AM_Domicilio__c, AM_TX_Poblacion__c, AM_TX_Denominacion__c, AM_NU_CP__c, AM_SEL_Tipo_de_Vinculo__c, AM_TX_Nombre_Responsable_Comercial__c, AM_Telefono__c, AM_EM_Correo_del_Responsable_Comercial__c, AM_TX_Tipo_ES__c, AM_Red__c, AM_Jefe_Regional__c, AM_Delegacion__c, OwnerId FROM AM_Estacion_de_servicio__c ]){
                for (AM_Estacion_de_servicio__c eSOp : AM_estaciones_de_servicio ){
                       System.debug('Estacion de Servcio:: '+ eSOp.Name); 
                        pbltyp = eSOp.AM_Pbltyp__c;
                        niels = pbltyp + eSOp.AM_Niels__c;
                        if (AM_red_Map.containsKey(niels)){                               
                            AM_Red__c red = AM_red_Map.get(niels);
                            System.debug('Red :: '+red.AM_Nombre_Red__c);
                            if (red != null){
                                eSOp.AM_Red__c = red.AM_Nombre_Red__c;
                            }                                
                        }else{                                
                             eSOp.AM_Red__c = '';
                        }   
             
                        if ((eSOp.AM_Sortl__c == 'RA') || (eSOp.AM_Sortl__c == 'GESPEVESA') || (eSOp.AM_Sortl__c == 'Campsa RED')){
                            if (AM_users_maps_IdUAm.containsKey(eSOp.AM_Vkgrp__c)){
                                newUser = AM_users_maps_IdUAm.get(eSOp.AM_Vkgrp__c);                            
                                if (newUser != null){
                                    //asignamos cartera (propietario)
                                    eSOp.OwnerId =  newUser.Id;
                                    uRole = TR_userRoleMap.get(newUser.UserRoleId);
                                    if (uRole != null){
                                        parentRole = uRole.ParentRoleId;
                                        if (parentRole != null){
                                            nUserRole  = TR_userRoleMap.get(parentRole);
                                            if (nUserRole != null){
                                                oUser = AM_users_maps_Role.get(nUserRole.Id);                                                    
                                                if (oUser != null){
                                                    eSOp.AM_Jefe_Regional__c= oUser.Name;
                                                     System.debug('Jefe_Regional :: '+ oUser.Name);
                                                    deleg = TR_userRoleMap.get(nUserRole.ParentRoleId);
                                                    // Delegacion solo se asigna si la estación de servicio no es gespevesa
                                                    if (deleg!= null && deleg.Name != '' && eSOp.AM_Sortl__c != 'GESPEVESA'){
                                                        eSOp.AM_Delegacion_Jerarquia__c= deleg.Name;
                                                        eSOp.AM_Delegacion__c = deleg.Name;
                                                        System.debug('Delegacion :: '+ eSOp.AM_Delegacion__c);
                                                       
                                                    }else{
                                                        eSOp.AM_Delegacion_Jerarquia__c= '';
                                                        eSOp.AM_Delegacion__c = '';
                                                    }
                                                }   
                                            }
                                        }
                                      }
                                    }
                            }else{
                                // si no existe usuario de cartera, se asigna el administrador
                                 eSOp.OwnerId = Label.LAB_AM_UserAdmin;  
                            }                    
                       }
                 }
                 if (AM_estaciones_de_servicio.size() > 0) {
                       try {
                            
                                Database.SaveResult[] results = Database.Update(AM_estaciones_de_servicio, false);
                                
                            	for (Database.SaveResult sr : results) {
                                        if (sr.isSuccess()) {
                                            // Operation was successful, so get the ID of the record that was processed
                                            System.debug('Successfully updated.  ' + sr.getId());
                                        }
                                        else {
                                            // Operation failed, so get all errors                
                                            for(Database.Error err : sr.getErrors()) {
                                                System.debug('The following error has occurred.');                    
                                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                                System.debug('Affected this error: ' + err.getFields());
                                            }
                                        }
                                    }
                            
                             
                        } catch (DmlException e) {
                            System.debug('Error producido en la actualización de las Estaciones de Servicio:: ' + e.getMessage());
                        }
                 }
                }   
        }catch(System.Exception excep){
            System.debug('Error en la actualización de las estaciones de servicio: ' + excep.getMessage() + '. Causa: ' + excep.getCause());
            isUpdated = false;
        }        
    }        
}