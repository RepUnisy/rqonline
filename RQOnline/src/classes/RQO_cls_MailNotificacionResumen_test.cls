/**
*   @name: RQO_cls_MailNotificacionResumen_test
*   @version: 1.0
*   @creation date: 28/02/2018
*   @author: David Iglesias - Unisys
*   @description: Clase de test para probar el envío de notificaciones de resumen por mail
*/
@IsTest
public class RQO_cls_MailNotificacionResumen_test {

    /**
    * @creation date: 28/02/2018
    * @author: David Iglesias - Unisys
    * @description: Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacionResumen');
            RQO_cls_TestDataFactory.createCSEnvioNotificaciones();
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
            RQO_cls_TestDataFactory.createNotification(200);
            RQO_cls_TestDataFactory.createNotificationConsent();
            RQO_cls_TestDataFactory.createPosiciondeEntrega(200);
            RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_resumenDiario_es'});
        }
    }
    
    /**
    * @creation date: 26/02/2018
    * @author: David Iglesias - Unisys
    * @description: Método test para probar la funcionalidad correcta del batch
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testExecuteOK(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        String posPedidoIdExt ='';
        String entregaIdExt ='';
        Map<Id, String> contactIdMap = new Map<Id, String>();
        Map<String, String> posPedidoIdMap = new Map<String, String>();
        Map<String, String> entregaIdAuxMap = new Map<String, String>();
        
        System.runAs(u){
            List<RQO_obj_notificationConsent__c> notificationConsentAuxList = [SELECT Id, RQO_fld_emailSendingPeriodicity__c 
                                                                                FROM RQO_obj_notificationConsent__c
                                                                                WHERE RQO_fld_notificationType__c IN ('CO','TR')];
            for (RQO_obj_notificationConsent__c item : notificationConsentAuxList) {
                item.RQO_fld_emailSendingPeriodicity__c = 'Diario';
            }
            update notificationConsentAuxList;
            
            for (Contact itemContact : [SELECT RQO_fld_idioma__c, Id FROM Contact]) {
            	contactIdMap.put(itemContact.Id, itemContact.RQO_fld_idioma__c);
            }
            
            for (Asset posPedido : [SELECT Id, RQO_fld_idExterno__c FROM Asset]) {
                posPedidoIdMap.put(posPedido.RQO_fld_idExterno__c, posPedido.Id);
                posPedidoIdExt = posPedido.RQO_fld_idExterno__c;
            }
            
            for (RQO_obj_posiciondeEntrega__c poscicionEntrega : [SELECT Id, RQO_fld_idRelacion__r.RQO_fld_idExterno__c FROM RQO_obj_posiciondeEntrega__c]) {
                entregaIdAuxMap.put(poscicionEntrega.RQO_fld_idRelacion__r.RQO_fld_idExterno__c, poscicionEntrega.Id);
                entregaIdExt = poscicionEntrega.RQO_fld_idRelacion__r.RQO_fld_idExterno__c;
            }
            
            List<RQO_obj_notification__c> notificationList = [SELECT Id, RQO_fld_contact__c, RQO_fld_mailSent__c, RQO_fld_visualized__c, RQO_fld_messageType__c,
                                                              RQO_fld_objectType__c FROM RQO_obj_notification__c];
            for (Integer i = 0; i < notificationList.size(); i++) {
                RQO_obj_notification__c notificacion = notificationList.get(i);
                if (i == 0) {
                    notificacion.RQO_fld_messageType__c = 'CO';
                    notificacion.RQO_fld_externalId__c = entregaIdExt;
                    notificacion.RQO_fld_objectType__c = RQO_cls_Constantes.COD_SF_NOT_ENTREGA;
                } else {
                    notificacion.RQO_fld_messageType__c = 'TR';
                    notificacion.RQO_fld_externalId__c = posPedidoIdExt;
                    notificacion.RQO_fld_objectType__c = RQO_cls_Constantes.COD_SAP_NOT_PEDIDO;
                    break;
                }
            }
            
            test.startTest();
            Id batchJobId = Database.executeBatch(new RQO_cls_MailNotificacionResumen_batch(notificationList, contactIdMap, posPedidoIdMap, entregaIdAuxMap), 2000);            
            test.stopTest();
            

            //System.assertEquals(true, notificationAuxList.get(1).RQO_fld_mailSent__c);
        }
    }
    
}