/**
*	@name: RQO_cls_ConsumptionWS_test
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase RQO_cls_ConsumptionWS
*/
@IsTest
public class RQO_cls_ConsumptionWS_test {
    
    /**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserConsumos@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createAccount(1);
            RQO_cls_TestDataFactory.createContainerJunction(1);
            RQO_cls_TestDataFactory.createCSSapFamilies();
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
        }
    }
	
    /**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de consumos de Sap
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getSapConsumption(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            Account cuenta = [Select id from Account limit 1];
			RQO_obj_qp0Grade__c objQp0 = [Select Id, RQO_fld_sku__c from RQO_obj_qp0Grade__c];
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_CONSUMPTION, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
                RQO_cls_ConsumptionWS consumos = new RQO_cls_ConsumptionWS();
                RQO_cls_ConsumptionWS.ConsumptionCustomResponse	returnData = consumos.getSapConsumption(cuenta.Id, objQp0.RQO_fld_sku__c, 'test', null, null);
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_OK, returnData.errorCode);
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de consumos de Sap cuando se genera un error en la llamada
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getSapConsumptionNOK(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            Account cuenta = [Select id from Account limit 1];
			RQO_obj_qp0Grade__c objQp0 = [Select Id, RQO_fld_sku__c from RQO_obj_qp0Grade__c];
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_CONSUMPTION, RQO_cls_Constantes.MOCK_EXEC_NOK, '0'));
                RQO_cls_ConsumptionWS consumos = new RQO_cls_ConsumptionWS();
                RQO_cls_ConsumptionWS.ConsumptionCustomResponse	returnData = consumos.getSapConsumption(cuenta.Id, objQp0.RQO_fld_sku__c, 'test', null, null);
			test.stopTest();
            
            System.assertEquals('E', returnData.errorCode);
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la funcionalidad de obtención de consumos de Sap cuando se genera una excepción genérica
    * Va a intentar convertir a decimal una serie de datos que no son casteables
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getSapConsumptionNOKGenericException(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            Account cuenta = [Select id from Account limit 1];
			RQO_obj_qp0Grade__c objQp0 = [Select Id, RQO_fld_sku__c from RQO_obj_qp0Grade__c];
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_CONSUMPTION, RQO_cls_Constantes.MOCK_EXEC_EXCEPTION, '0'));
                RQO_cls_ConsumptionWS consumos = new RQO_cls_ConsumptionWS();
                RQO_cls_ConsumptionWS.ConsumptionCustomResponse	returnData = consumos.getSapConsumption(cuenta.Id, objQp0.RQO_fld_sku__c, 'test', null, null);
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_NOK, returnData.errorCode);
        }
    }
    
	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar el control de errores de la llamada al servicio de consumos cuando no se envía id de cliente
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getConsumosSinCliente(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            test.startTest();
                RQO_cls_ConsumptionWS consumos = new RQO_cls_ConsumptionWS();
                RQO_cls_ConsumptionWS.ConsumptionCustomResponse	returnData = consumos.getSapConsumption(null, null, null, null, null);
			test.stopTest();
            
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_NOK, returnData.errorCode);
        }
    }
    
}