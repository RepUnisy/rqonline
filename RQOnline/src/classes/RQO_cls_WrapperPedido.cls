/**
*  @name: RQO_cls_WrapperPedido
*  @version: 1.0
*  @creation date: 01/10/2017
*  @author: Alfonso Constan - Unisys
*  @description: Bean Apex para almacenar los datos de pedido obtenidos de diferentes tablas
*
*/
public class RQO_cls_WrapperPedido /*implements Comparable*/{
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase usada como modelo de selección
    */
    public class seleccionador{
        string value;
        string label;

        /**
        *   @name: seleccionador
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase seleccionador
        */
        public seleccionador(string v, string l){
            this.value = v;
            this.label = l;
        }
    }
    
    /**
    *   @name: envaseUnidadMedida
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase que relaciona envases y unidades de medida
    */
    public class envaseUnidadMedida{
        string envase = '';
        string unidadMedida = '';
        decimal pesoMaximo ;
        decimal pesoMinimo ;
        decimal multiploKgUnidad;
        boolean excluirValidacion = false;
        
        /**
        *   @name: envaseUnidadMedida
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase envaseUnidadMedida
        */
        public envaseUnidadMedida(string envase, string unidadMedida, decimal pesoMaximo, decimal pesoMinimo, decimal kgUnidad, boolean excluirValidacion){
            this.envase = envase;
            this.unidadMedida = unidadMedida;
            this.pesoMaximo = pesoMaximo;
            this.pesoMinimo = pesoMinimo;
            this.excluirValidacion = excluirValidacion;
            this.multiploKgUnidad = kgUnidad;
        }
    }
    
    private List<string> listEnvases = new List<string>();
    private List<string> listUnidadMedida = new List<string>();
    private List<string> listIdsEnvases = new List<string>();
    private List<string> listModoEnvio = new List<string>();
    Map<String, List<String>> mapModoEnvio = new Map<String, List<String>>();
    private boolean crearPedido = false;
    Map<String, id> mapaIdEnvases = new Map<String, id>();
    Map<String, id> mapaIdModosEnvio = new Map<String, id>();
    Map<id, String> mapaIdEnvasesTP = new Map<id, String>();
    Map<id, String> mapaIdModosEnvioTP = new Map<id, String>();
    
    private Map<string, string> mapaEnvases = new Map<string, string>();
    //  Información del asset
    private decimal cantidad = 0;
    private decimal cantidadUnidad = 0;
    private id idSolicitud;
    private String nameSolicitud = '';
    private String typeSolicitud;
    private id idAsset;
    private id idGrado;
    private date fechaReparto;
    private date fechaPreferenteEntrega = null;
    private string incoterm = '';
    private string grado = '';
    private string sku = '';   
    private id dirEnvio;
    private string destinoMercanciasNombre = '';
    private id direccionFacturacion;
    private boolean variasDirFacturacion ;
    private string envase = null;
    private string envaseCode = '';
    private string envaseName = '';
    private String envaseDescription = '';
    private Double pesoMaximo ;
    private Double pesoMinimo ;
    private string modoEnvio = null;
    private string modoEnvioCode = '';
    private string modoEnvioName = '';
    private string numRefCliente = '';
    private String statusCode = '';
    private String statusDescription = '';
    private String statusCssClass = '';
    private String qp0gradeName = '';
    private String qp0gradeId = '';
    private String numPedido = '';
    private String fechaSolicitado = null;
    private String fechaEntrega = null;
    private String fechaEstado = null;
    private Date fechaCreacion = null;
    private String srcFechaPreferente = null;
    private String numEntrega = null;
    private Boolean showAlbaranCMR = false;
    private Boolean showCertificadoAnalisis = false;
    private String error = '';
    private String errorFecha = '';
    private String errorCantidad = '';
    Map<String, String> mapTradEnvaseUidadMedida = new Map<String, String>();
    
    private list<seleccionador> selectEnvases = new list<seleccionador>();
    private list<seleccionador> selectModoEnvio = new list<seleccionador>();
    private Map<string, envaseUnidadMedida> mapEnvaseUndMedida = new Map<string, envaseUnidadMedida>();

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece las propiedades de un asset al wrapper de pedidos
    */
    public void setAsset(Asset asset){
        this.idSolicitud = asset.RQO_fld_requestManagement__c;
        this.nameSolicitud = (asset.RQO_fld_requestManagement__c != null) ? asset.RQO_fld_requestManagement__r.name : (asset.ParentId != null && asset.Parent.RQO_fld_requestManagement__c != null) ? asset.Parent.RQO_fld_requestManagement__r.name : null;
        this.typeSolicitud = 'PS';
        this.variasDirFacturacion = true;
        this.idAsset = asset.id;
        this.cantidad = asset.RQO_fld_cantidad__c;
        this.cantidadUnidad = asset.RQO_fld_cantidadUnidad__c;
        this.idGrado = asset.RQO_fld_grade__c;        
        this.fechaPreferenteEntrega = (asset.RQO_fld_cuDeliveryDate__c != null) ? asset.RQO_fld_cuDeliveryDate__c : (asset.ParentId != null && asset.Parent.RQO_fld_cuDeliveryDate__c != null) ? asset.Parent.RQO_fld_cuDeliveryDate__c : (asset.RQO_fld_idRelacion__c != null && asset.RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c != null) ? asset.RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c : null;
        this.srcFechaPreferente = (this.fechaPreferenteEntrega != null) ? this.fechaPreferenteEntrega.format() : null;
        //  this.fechaReparto = asset.RQO_fld_cuDeliveryDate__c;    //  Modifiar el enlace
        this.incoterm = asset.RQO_fld_incoterm__c;
        this.dirEnvio = asset.RQO_fld_direccionEnvio__c;
        this.destinoMercanciasNombre = (asset.RQO_fld_direccionEnvio__c != null) ? asset.RQO_fld_direccionEnvio__r.Name + ' ' + asset.RQO_fld_direccionEnvio__r.BillingCountryCode : (asset.RQO_fld_idRelacion__c != null && asset.RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__c != null) ? asset.RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__r.Name + ' ' + asset.RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__r.BillingCountryCode : null;
        this.direccionFacturacion = asset.RQO_fld_destinoFacturacion__c;
        this.envase = asset.RQO_fld_container__c;
        this.envaseCode = asset.RQO_fld_container__r.RQO_fld_codigo__c;
        this.envaseName = asset.RQO_fld_container__r.RQO_fld_codigo__c;	
        this.envaseDescription= (asset.RQO_fld_container__c != null) ? asset.RQO_fld_container__r.RQO_fld_codigo__c : null;
        this.modoEnvio = asset.RQO_fld_shippingMode__c;
        this.modoEnvioCode = asset.RQO_fld_shippingMode__r.RQO_fld_code__c;
        this.modoEnvioName = asset.RQO_fld_shippingMode__r.RQO_fld_code__c;
        //   listModoEnvio.add(asset.RQO_fld_sendMode__c);
        this.numRefCliente = (String.isNotEmpty(asset.RQO_fld_numRefCliente__c)) ? asset.RQO_fld_numRefCliente__c : (asset.RQO_fld_idRelacion__c != null && String.isNotEmpty(asset.RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c)) ? asset.RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c : null;
        this.statusCode = asset.RQO_fld_positionStatus__c;
        this.grado = asset.RQO_fld_gradeDescriptionData__c;
        this.qp0gradeName = asset.RQO_fld_gradeDescriptionData__c;
        this.qp0gradeId = (asset.RQO_fld_qp0Grade__c != null) ? asset.RQO_fld_qp0Grade__c : (asset.ParentId != null && asset.Parent.RQO_fld_qp0Grade__c != null) ? asset.Parent.RQO_fld_qp0Grade__c : null;
       // this.sku = (asset.RQO_fld_qp0Grade__c != null) ? ((asset.RQO_fld_qp0Grade__r.RQO_fld_sku__c != null) ? asset.RQO_fld_qp0Grade__r.RQO_fld_sku__c : 'null') : 'null';
        this.numPedido = (asset.RQO_fld_idRelacion__c != null) ? asset.RQO_fld_idRelacion__r.RQO_fld_idExterno__c : null;
        fechaSolicitado = (asset.RQO_fld_positionRequestDate__c != null) ? asset.RQO_fld_positionRequestDate__c.format() : (asset.ParentId != null && asset.Parent.RQO_fld_positionRequestDate__c != null) ?
            asset.Parent.RQO_fld_positionRequestDate__c.format() : null;
        fechaEntrega = (RQO_cls_Constantes.PED_COD_STATUS_CONFIRMADO.equalsIgnoreCase(this.statusCode) && asset.RQO_fld_fechadeReparto__c !=null) ? asset.RQO_fld_fechadeReparto__c.format() : null;
        this.statusCssClass = getDataByStatus(this.statusCode, asset, null);
        fechaCreacion = asset.CreatedDate.date();
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece las propiedades de un asset al wrapper de pedidos
    */
    public String buscarNombreEnvase(String codigoEnvase){
       // if ()
       //  Label.RQO_lbl_P7;
       
       return codigoEnvase;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece la posición de entrega
    */
    public void setPosiEntrega(RQO_obj_posiciondeEntrega__c posEntrega){
        
        this.idAsset = posEntrega.RQO_fld_posicionPedido__c;
        //this.cantidad = integer.valueof(posEntrega.RQO_fld_cantidaddeEntrega__c);        
        this.idGrado = posEntrega.RQO_fld_posicionPedido__r.RQO_fld_grade__c;       //  Cambiar a QPO Grade                
        this.fechaPreferenteEntrega = posEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c;        
        this.srcFechaPreferente = (this.fechaPreferenteEntrega != null) ? this.fechaPreferenteEntrega.format() : null;
        this.fechaReparto = posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaRealdeLlegada__c;
        this.incoterm = posEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_incoterm__c;
        
        this.dirEnvio =posEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_interlocutorWE__c;
        this.destinoMercanciasNombre = posEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__r.Name;
        this.direccionFacturacion = posEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_interlocutorRE__c;
        
        this.envase = posEntrega.RQO_fld_posicionPedido__r.RQO_fld_container__c;
        this.envaseName= (posEntrega.RQO_fld_posicionPedido__r.RQO_fld_container__c != null) ? posEntrega.RQO_fld_posicionPedido__r.RQO_fld_container__r.RQO_fld_codigo__c : null;
        this.envaseDescription= (posEntrega.RQO_fld_posicionPedido__r.RQO_fld_container__c != null) ? posEntrega.RQO_fld_posicionPedido__r.RQO_fld_container__r.RQO_fld_codigo__c : null;
        this.modoEnvio = posEntrega.RQO_fld_unidad__c;
        this.numRefCliente = posEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_numerodePedidodeCliente__c;
        this.statusCode = posEntrega.RQO_fld_idRelacion__r.RQO_fld_estadodelaEntrega__c;
        this.qp0gradeName = posEntrega.RQO_fld_posicionPedido__r.RQO_fld_gradeDescriptionData__c;
		this.qp0gradeId = (posEntrega.RQO_fld_posicionPedido__r.RQO_fld_qp0Grade__c != null) ? posEntrega.RQO_fld_posicionPedido__r.RQO_fld_qp0Grade__c : (posEntrega.RQO_fld_posicionPedido__r.ParentId != null && posEntrega.RQO_fld_posicionPedido__r.Parent.RQO_fld_qp0Grade__c != null) ? posEntrega.RQO_fld_posicionPedido__r.Parent.RQO_fld_qp0Grade__c : null; 
        
        this.numPedido = (posEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__c != null) ? posEntrega.RQO_fld_posicionPedido__r.RQO_fld_idRelacion__r.RQO_fld_idExterno__c : null;
        this.fechaSolicitado = (posEntrega.RQO_fld_posicionPedido__r.ParentId != null && posEntrega.RQO_fld_posicionPedido__r.Parent.RQO_fld_positionRequestDate__c != null) ?
            posEntrega.RQO_fld_posicionPedido__r.Parent.RQO_fld_positionRequestDate__c.format() : null;
        
        //this.fechaEntrega = (posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaRealdeLlegada__c !=null) ? posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaRealdeLlegada__c.format() : (posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaEntregaCliente__c !=null) ? posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaEntregaCliente__c.format() : null;
        
        this.fechaEntrega = (posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaRealdeLlegada__c !=null) ? posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaRealdeLlegada__c.format() : 
        	(posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaEstimadaLlegada__c !=null) ? posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaEstimadaLlegada__c.format() : 
        	(posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaEntregaCliente__c !=null) ? posEntrega.RQO_fld_idRelacion__r.RQO_fld_fechaEntregaCliente__c.format() : null;

        
		this.idSolicitud = (posEntrega.RQO_fld_posicionPedido__r.ParentId != null && posEntrega.RQO_fld_posicionPedido__r.Parent.RQO_fld_requestManagement__c != null) ?
            posEntrega.RQO_fld_posicionPedido__r.Parent.RQO_fld_requestManagement__c : null;
        
        this.nameSolicitud = (posEntrega.RQO_fld_posicionPedido__r.ParentId != null && posEntrega.RQO_fld_posicionPedido__r.Parent.RQO_fld_requestManagement__c != null) ?
            posEntrega.RQO_fld_posicionPedido__r.Parent.RQO_fld_requestManagement__r.name : null;
        
        this.statusCssClass = getDataByStatus(this.statusCode, null, posEntrega);
        
        numEntrega = (posEntrega.RQO_fld_idRelacion__c != null) ? posEntrega.RQO_fld_idRelacion__r.RQO_fld_idExterno__c : null;
        fechaCreacion = posEntrega.CreatedDate.date();
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el Id del Asset del wrapper
    */
    public void setId(Id idAsset){
        this.idAsset = idAsset;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el Id del Asset del wrapper
    */
    public Id getId(){
        return this.idAsset;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el Id de un grado
    */
    public id getIdGrado(){
        return this.idGrado;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna número de referencia de un cliente
    */
    public String getnumRefCliente(){
        return this.numRefCliente;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la cantidad pedida
    */
    public decimal getCantidad(){
        return this.cantidad;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la cantidad pedida en formato decimal
    */
    public void setCantidad(decimal cant){
        this.cantidad = cant;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la cantidad pedida en cantidad de unidades
    */
    public decimal getCantidadUnidad(){
        return this.cantidadUnidad;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la fecha preferente de entrega
    */
    public date getFechaPreferenteEntrega(){
        return this.fechaPreferenteEntrega;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la dirección de facturación
    */
    public Id getDireccionFacturacion(){
        return direccionFacturacion;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el Incoterm
    */
    public string getIncoterm(){
        return incoterm;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la dirección de envío
    */
    public Id getDireccionEnvio(){
        return dirEnvio;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el código de estado
    */
    public String getStatusCode(){
        return statusCode;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la descripción de un estado
    */
    public String getStatusDescription(){
        return statusDescription;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la descripción de un estado
    */
    public void setStatusDescription(String statusDescription){
        this.statusDescription = statusDescription;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el estado de la clase css correspondiente
    */
    public String getStatusCssClass(){
        return this.statusCssClass;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el estado de la clase css correspondiente
    */
    public String getQp0gradeName(){
        return this.qp0gradeName;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el Id de un Qp0Grade
    */
	public String getQp0gradeId(){
        return this.qp0gradeId;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el número de pedido
    */
    public String getNumPedido(){
        return this.numPedido;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el grado
    */
	public String getGrado(){
        return this.grado;
    }   
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la fecha solicitada
    */
    public String getFechaSolicitado(){
        return this.fechaSolicitado;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una fecha de entrega
    */
    public String getFechaEntrega(){
        return this.fechaEntrega;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que devuelve el envase del pedido realizado
    */
    public Id getContainer(){
        return this.envase;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que devuelve el envase del pedido realizado
    */
    public String getContainerCode(){
        return this.envaseCode;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el modo de envío
    */
    public Id getModoEnvio(){
        return this.modoEnvio;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el código de envío
    */
    public String getModoEnvioCode(){
        return this.modoEnvioCode;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el nombre del modo de envío
    */
	public String getModoEnvioName(){
        return this.modoEnvioName;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la fuente de la fecha preferente
    */
    public String getSrcFechaPreferente(){
        return this.srcFechaPreferente;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el número de entrega
    */
	public String getNumEntrega(){
        return this.numEntrega;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que la muestra del albarán
    */
    public Boolean getShowAlbaranCMR(){
        return this.showAlbaranCMR;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la muestra del certificado de análisis
    */
    public Boolean getShowCertificadoAnalisis(){
        return this.showCertificadoAnalisis;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna un nombre de solicitud
    */
	public String getNameSolicitud(){
        return this.nameSolicitud;
    }
	
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna un nombre de solicitud
    */
    public Date getFechaCreacion(){
        return this.fechaCreacion;
    }
	
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el nombre del envase
    */
	public String getEnvaseName (){
        return this.envaseName;
    }    
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece el mapa de Ids de envases
    */
    public void setMapaIdEnvases(Map<String, id> mapaIdEnvases){
        this.mapaIdEnvases = mapaIdEnvases;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece el mapa de Ids de envases de transporte
    */
    public void setMapaIdEnvasesTP(Map<id, String> mapaIdEnvasesTP){
        this.mapaIdEnvasesTP = mapaIdEnvasesTP;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece el nombre del modo de envío
    */
    public void setModoEnvioName (String modoEnvio){
        this.modoEnvioName = modoEnvio;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece el nombre del envase
    */
    public void setEnvaseName (String envaseName){
        this.envaseName = envaseName;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece el mapa de Ids de modos de envío
    */
    public void setMapaIdModosEnvio(Map<String, id> mapaIdModosEnvio){
        this.mapaIdModosEnvio = mapaIdModosEnvio;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece el mapa de Ids de modos de envío de transporte
    */
    public void setMapaIdModosEnvioTP(Map<id, String> mapaIdModosEnvioTP){
        this.mapaIdModosEnvioTP = mapaIdModosEnvioTP;
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece un mapa de traducciones
    */
    public void setMapTrad (Map<String, String> mapTradEnvaseUidadMedida ){
        this.mapTradEnvaseUidadMedida = mapTradEnvaseUidadMedida;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna los datos correspondientes a un activo según su código de estado y posición de 
    *   entrega
    */
    private String getDataByStatus(String statusCode, Asset activo, RQO_obj_posiciondeEntrega__c entrega){
        String cssClass = RQO_cls_Constantes.PED_COD_STATUS_SOLICITADO_CSS_CLASS;
        this.fechaEstado = this.fechaSolicitado;
        if (RQO_cls_Constantes.PED_COD_STATUS_REGISTRADO.equalsIgnoreCase(statusCode)){
            cssClass = RQO_cls_Constantes.PED_COD_STATUS_REGISTRADO_CSS_CLASS;
            this.fechaEstado = (activo != null && activo.RQO_fld_idRelacion__c != null && activo.RQO_fld_idRelacion__r.RQO_fld_fechadeGrabacionderegistro__c != null) ? activo.RQO_fld_idRelacion__r.RQO_fld_fechadeGrabacionderegistro__c.format() : null;
			showAlbaranCMR = false;
    		showCertificadoAnalisis = false;
        }else if (RQO_cls_Constantes.PED_COD_STATUS_CONFIRMADO.equalsIgnoreCase(statusCode)){
            cssClass = RQO_cls_Constantes.PED_COD_STATUS_CONFIRMADO_CSS_CLASS;
			showAlbaranCMR = false;
    		showCertificadoAnalisis = false;
        }else if (RQO_cls_Constantes.PED_COD_STATUS_ENTRANSITO.equalsIgnoreCase(statusCode)){
            cssClass = RQO_cls_Constantes.PED_COD_STATUS_ENTRANSITO_CSS_CLASS;
            this.fechaEstado = (entrega != null) ? entrega.RQO_fld_idRelacion__r.RQO_fld_fechaEntregaCliente__c.format() : null;
            showAlbaranCMR = true;
            showCertificadoAnalisis = entrega.RQO_fld_indicadorSujetoaLote__c;
        }else if (RQO_cls_Constantes.PED_COD_STATUS_ENTREGADO.equalsIgnoreCase(statusCode)){
            cssClass = RQO_cls_Constantes.PED_COD_STATUS_ENTREGADO_CSS_CLASS;
            this.fechaEstado = (entrega != null) ? entrega.RQO_fld_idRelacion__r.RQO_fld_fechaRealdeLlegada__c.format() : null;
            showAlbaranCMR = true;
            showCertificadoAnalisis = true;
        }else if (RQO_cls_Constantes.PED_COD_STATUS_FACTURADO.equalsIgnoreCase(statusCode)){
			showAlbaranCMR = true;
    		showCertificadoAnalisis = true;
        }
        return cssClass;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna los datos correspondientes a un activo según su código de estado y posición de 
    *   entrega
    */
    public void setTranslate(RQO_obj_translation__c trad){
        this.grado = trad.RQO_fld_translation__c;
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece la lista de envases
    */
    public void setListEnvases(List<String> listConst){
        if (listConst.size() != 0){
            listEnvases.addAll(listConst);            
        }        
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece la lista de envases en base auna lista de Ids de envases
    */
    public void setListIdsEnvases(List<Id> listIdsEnv){
        if (listIdsEnv != null && listIdsEnv.size() != 0){
            listIdsEnvases = listIdsEnv;
            crearPedido = true;
        }        
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de establecimiento de una lista de RQO_obj_shippingJunction__c
    */
    public void setShipping(List<RQO_obj_shippingJunction__c> listShipping){
        Set<String> setEnvase = New Set<String>();
        Set<String> setModoEnvio = New Set<String>();
        Map<String, List<String>> mapModoEnvioP = new Map<String, List<String>>();
        system.debug('listShipping: '+listShipping);
        //  Recorremos los envases 
        for(RQO_obj_shippingJunction__c cont : listShipping){
            
            //	Insertamos la combinatoria del shipping_juntion
            mapEnvaseUndMedida.put(String.valueOf(cont.RQO_fld_container__c) + String.valueOf(cont.RQO_fld_shippingMode__c) , new envaseUnidadMedida(cont.RQO_fld_containerCode__c, cont.RQO_fld_shippingCode__c, cont.RQO_fld_maximumWeightKg__c, cont.RQO_fld_minimumWeightKg__c, cont.RQO_fld_multiploKgUnidad__c, cont.RQO_fld_excluirValidacion__c ));
            
            if (mapModoEnvioP.containsKey(cont.RQO_fld_containerCode__c)){
                List<String> listUnidadMedida = mapModoEnvioP.get(cont.RQO_fld_containerCode__c);
                listUnidadMedida.add(cont.RQO_fld_shippingCode__c);
            }
            else{
                List<String> listUnidadMedida = new List<String>();
                listUnidadMedida.add(cont.RQO_fld_shippingCode__c);
                mapModoEnvioP.put(cont.RQO_fld_containerCode__c, listUnidadMedida);
            }
            
            if(!setEnvase.contains(cont.RQO_fld_containerCode__c)){
                setEnvase.add(cont.RQO_fld_containerCode__c);
                String tradEnvase = (mapTradEnvaseUidadMedida.containsKey(cont.RQO_fld_containerCode__c)) ? mapTradEnvaseUidadMedida.get(cont.RQO_fld_containerCode__c) : cont.RQO_fld_containerCode__c;
                selectEnvases.add(new seleccionador(cont.RQO_fld_container__c, tradEnvase));                             
            }
            if(!setModoEnvio.contains(cont.RQO_fld_shippingCode__c)){
                setModoEnvio.add(cont.RQO_fld_shippingCode__c);
                //selectModoEnvio.add(new seleccionador(cont.RQO_fld_shippingMode__c, cont.RQO_fld_shippingCode__c));
            }            
        }
        //  Almacenamos los envases y modo de envio
        this.mapModoEnvio.putAll(mapModoEnvioP);
        
        this.listEnvases.addAll(setEnvase);
        this.listModoEnvio.addAll(setModoEnvio);
        
        //  Si tiene algún modo de envio permitimos la creación de pedido
        if (this.listModoEnvio.size() != 0){
            crearPedido = true;
        }
    }
    
    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece una lista de envíos
    */
    public void settearListaEnvio(){
        this.listModoEnvio = this.mapModoEnvio.get(this.mapaIdEnvasesTP.get(this.envase));
        //codigos
        for(string aux : this.listModoEnvio){
            selectModoEnvio.add(new seleccionador(this.mapaIdModosEnvio.get(aux), aux));
            
        }
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece una lista de unidaeds de envío
    */
    public void settearListaEnvioUni( ){system.debug('aaa: '+mapModoEnvio);
        string key = (new List<String>(mapModoEnvio.keySet()))[0];   
        this.envase = (this.envase == null || this.envase == '') ? [SELECT RQO_fld_container__r.id FROM RQO_obj_shippingJunction__c where RQO_fld_container__r.RQO_fld_codigo__c = :key][0].RQO_fld_container__r.id : this.envase;
        
        if (this.mapModoEnvio.get(this.mapaIdEnvasesTP.get(this.envase)) != null){
            for(string aux : this.mapModoEnvio.get(this.mapaIdEnvasesTP.get(this.envase))){
                String tradModoEnvio = (mapTradEnvaseUidadMedida.containsKey(aux)) ? mapTradEnvaseUidadMedida.get(aux) : aux;
                selectModoEnvio.add(new seleccionador(this.mapaIdModosEnvio.get(tradModoEnvio), tradModoEnvio));
            }          
            
            //this.modoEnvio = selectModoEnvio[0].value;           
        }
    }

    /**
    *   @name: RQO_cls_WrapperPedido
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece una lista de modos de envío
    */
    public void setListModoEnvio(List<String> modoEnvio){
        if(modoEnvio.size() != 0){
            listModoEnvio.addAll(modoEnvio);
            crearPedido = true;
        }        
    }
    
	/**
    * @creation date: 09/01/2018
    * @author: Alvaro Alonso Trinidad - Unisys
    * @description: implementación del método compareTo para ordenar la lista de objetos
    * @param: compareTo tipo Object
    * @return: Integer
    * @exception: 
    * @throws: 
    */
    /*
    public Integer compareTo(Object objToCompare) {
        
        if (fechaCreacion == ((RQO_cls_WrapperPedido)objToCompare).fechaCreacion){
			String datoSolicToCompare = (!String.isEmpty(((RQO_cls_WrapperPedido)objToCompare).nameSolicitud)) ? ((RQO_cls_WrapperPedido)objToCompare).nameSolicitud : '';
            String datoSolicActual = (!String.isEmpty(nameSolicitud)) ? nameSolicitud : '';
            
            //Si voy a comparar y es la solicitud actual la que está vacía o tengo la misma solicitud en ambos objetos entonces se miran los números de pedido
            if (datoSolicActual.compareTo(datoSolicToCompare) == 0){
                String datoPedCompare = (!String.isEmpty(((RQO_cls_WrapperPedido)objToCompare).numPedido)) ? ((RQO_cls_WrapperPedido)objToCompare).numPedido : '';
                String datoPedActual = (!String.isEmpty(numPedido)) ? numPedido : '';
                if (datoPedActual.compareTo(datoPedCompare) > 0) {
                    return -1;
                } else if (datoPedActual.compareTo(datoPedCompare) < 0) {
                    return 1;
                } else {
                    return 0;
                } 
            }else if (datoSolicActual.compareTo(datoSolicToCompare) > 0){
                return -1;
            }else{
                return 1;
            }
        }else if(fechaCreacion > ((RQO_cls_WrapperPedido)objToCompare).fechaCreacion){
            return -1;
        }else{
            return 1;
        }
        return null;
	}
*/
}