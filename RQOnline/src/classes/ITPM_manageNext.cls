/*------------------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    Clase que implementa la funcionalidad del boton Siguiente
History
<Date>          <Author>        <Description>
23-Feb-2016     Rubén Simarro   Initial version
----------------------------------------------------------------------------*/
global class ITPM_manageNext {   
    /**
     * método que comprueba si hay mas elementos
     */
    webservice static integer hayMasElementos(Id idobjeto, Id proyectId){        
        system.debug('@@@ clase ITPM_manageNext method hayMasElementos() proyectID: '+ proyectId);                
      
        if(String.isEmpty(proyectId)){
             return -1;
         }
        else if(doPasarSiguiente(idobjeto, proyectId) != idobjeto){
            return 1;
        }  
        else{
            return -1;
        }          
    }
    
     webservice static integer hayMasElementosAnteriores(Id idobjeto, Id proyectId){        

         system.debug('@@@ clase ITPM_manageNext method hayMasElementosAnteriores() proyectID: '+ proyectId);                

         if(String.isEmpty(proyectId)){
             return -1;
         }
         else if(doPasarAnterior(idobjeto, proyectId) != idobjeto){
            return 1;
        }
        else{
            return -1;
        }          
    }
    
    /**
     * método que devuelve el id del objeto siguiente, si lo hay. Si nó, devuelve el actual
     */
    webservice static Id doPasarSiguiente(Id idobjeto, Id proyectId){        
        system.debug('@@@ clase ITPM_manageNext method doPasarSiguiente() ');        
        try{

            String labelProyectId = dameLabelProyectEnObjeto(idobjeto);
      
            String SOQL_query = 'select Id, name FROM '+idobjeto.getSObjectType().getDescribe().getName()+ 
                                        ' where Id > \''+idobjeto+'\' AND '+labelProyectId+' = \''+ proyectId+'\' order by createdDate ASC LIMIT 1';
                                     
            system.debug('@@@ clase ITPM_manageNext method doPasarSiguiente() SOQL query: '+SOQL_query);
            
            for(SObject aux: Database.Query(SOQL_query) ){
                         
                                            system.debug('@@@ clase ITPM_manageNext method doPasarSiguiente(): elemento siguiente '+aux.Id);
                                        
                                            return aux.Id;   
                                        }       
  
                           
            system.debug('@@@ clase ITPM_manageNext method doPasarSiguiente(): no hay mas elementos');

            return idobjeto;               
        }
        catch(Exception e){
            system.debug('@@@ '+e.getMessage());  return null;
        }
       
    }

    /**
     * método que devuelve el id del objeto anterior, si lo hay. Si nó, devuelve el actual
     */
    webservice static Id doPasarAnterior(Id idobjeto, Id proyectId){        
        system.debug('@@@ clase ITPM_manageNext method doPasarAnterior() ');        
        try{

            String labelProyectId = dameLabelProyectEnObjeto(idobjeto);
      
            String SOQL_query = 'select Id, name FROM '+idobjeto.getSObjectType().getDescribe().getName()+ 
                                        ' where Id < \''+idobjeto+'\' AND '+labelProyectId+' = \''+ proyectId+'\' order by createdDate DESC LIMIT 1';
                                     
            system.debug('@@@ clase ITPM_manageNext method doPasarAnterior() SOQL query: '+SOQL_query);

            for( SObject aux: Database.Query(SOQL_query) ){
                         
                                            system.debug('@@@ clase ITPM_manageNext method doPasarAnterior(): elemento anterior '+aux.Id);
                                        
                                            return aux.Id;   
                                        }       
  
                           
            system.debug('@@@ clase ITPM_manageNext method doPasarAnterior(): no hay mas elementos');

            return idobjeto;               
        }
        catch(Exception e){
            system.debug('@@@ '+e.getMessage());  return null;
        }
       
    }

    
    /**
     *  La finalidad de este método es devolver el nombre API correcto del campo lookup Project en los diferentes objetos
     **/
    private static String dameLabelProyectEnObjeto(Id idobjeto){
        

        if(idobjeto.getSObjectType().getDescribe().getName().equals('ITPM_Change_Request__c') ||
           idobjeto.getSObjectType().getDescribe().getName().equals('ITPM_Experience__c') ||     
           idobjeto.getSObjectType().getDescribe().getName().equals('ITPM_Issue__c') ||   
           idobjeto.getSObjectType().getDescribe().getName().equals('ITPM_Status_Report__c') ||
           idobjeto.getSObjectType().getDescribe().getName().equals('ITPM_Team_Member__c') ||
           idobjeto.getSObjectType().getDescribe().getName().equals('ITPM_Project_Success_Criteria__c') ||         
           idobjeto.getSObjectType().getDescribe().getName().equals('ITPM_Benefit__c') ){                                                         
            return 'ITPM_REL_Project__c';  
        }  
        else if( idobjeto.getSObjectType().getDescribe().getName().equals('ITPM_UBC__c') ){
            return 'ITPM_REL_Project_ITPM__c';
        }
         else if( idobjeto.getSObjectType().getDescribe().getName().equals('ITPM_Budget_Item__c') ){
            return 'ITPM_REL2_Project__c';
        }
        else{
            
            
            return 'ITPM_REL_Project__c';              
        }
      
    }
}