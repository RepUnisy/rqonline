/**
*	@name: RQO_cls_GradeTriggerHandler_trigger
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar el funcionamiento del trigger de grados
*/
@IsTest
public class RQO_cls_GradeTriggerHandler_test {

    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createContact(1);
            RQO_cls_TestDataFactory.createGrade(200);
		}
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta de inserción en el trigger
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testAfterInsert(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            
            //Creación de grados en el initialSetup()
            
            List<RQO_obj_notification__c> notificationList = [SELECT Id FROM RQO_obj_notification__c WHERE RQO_fld_messageType__c = 'GN'];
            test.stopTest();
            
            System.assertEquals(200, notificationList.size());
        }
    }
    
    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta de actualización en el trigger
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testAfterUpdate(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();

            List<RQO_obj_grade__c> gradeList = [SELECT Id, RQO_fld_recommendation__c FROM RQO_obj_grade__c];
            for(RQO_obj_grade__c item : gradeList) {
                item.RQO_fld_recommendation__c = true;
            }
            update gradeList;
            
            
            List<RQO_obj_notification__c> notificationList = [SELECT Id FROM RQO_obj_notification__c WHERE RQO_fld_messageType__c = 'GR'];
            system.debug('notificationList: '+notificationList);
            test.stopTest();
            
            System.assertEquals(200, notificationList.size());
        }
    }
 
}