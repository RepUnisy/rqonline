@istest
public class PM_cls_Juego_Datos {
    
    public static void crear_juego_datos(){
        
        
        
        //creamos un usuario
        
        profile perfil = [SELECT id FROM profile WHERE name = 'Usuario estándar force.com'];
        
        user usuarioGPS = new user(Username = 'pruebagps@pruebanorepetida.com', isActive = true, CurrencyIsoCode = 'GBP', 
                                   LastName = 'prueba', Email = 'prueba@prueba.com', Alias = 'prueba', 
                                   CommunityNickname = 'prueba@prueba.prueba', TimeZoneSidKey = 'America/Los_Angeles', 
                                   LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', ProfileId = perfil.id,
                                   LanguageLocaleKey = 'en_US');
        
        insert usuarioGPS;
        
        //Creamos estaciones de servicio
        AM_Estacion_de_servicio__c es = new AM_Estacion_de_servicio__c(AM_Nombre_ES__c = 'E.S. SANCHEZ SL', name='pruebaTest',AM_NU_E3_actual_Limpieza__c = 7.1 , AM_NU_E3_actual_Amabilidad__c = 9.2);
        insert es;
        AM_Estacion_de_servicio__c es1 = new AM_Estacion_de_servicio__c(AM_Nombre_ES__c = 'U.S. PRUEBAS', name='pruebaTest2',AM_NU_E3_actual_Limpieza__c = 5.2 , AM_NU_E3_actual_Amabilidad__c = 6.0);
        insert es1;
        AM_Estacion_de_servicio__c es2 = new AM_Estacion_de_servicio__c(AM_Nombre_ES__c = 'CORDOBAN SL', name='pruebaTest3',AM_NU_E3_actual_Limpieza__c = 3.1 , AM_NU_E3_actual_Amabilidad__c = 8.9);
        insert es2;
        AM_Estacion_de_servicio__c es3 = new AM_Estacion_de_servicio__c(AM_Nombre_ES__c = 'CORD SL', name='pruebaTest4',AM_NU_E3_actual_Limpieza__c = 1.2 , AM_NU_E3_actual_Amabilidad__c = 3.9);
        insert es3;
        
        //creamos acciones de Mejora
        AM_Accion_de_mejora__c Amejora=new AM_Accion_de_mejora__c();
        Amejora.AM_REF_Estacion_de_servicio__c=es.Id;
        Amejora.AM_DT_Fecha_fin_prevista__c= Date.newInstance(2017, 12, 17);
        Amejora.AM_DT_Fecha_inicio__c=Date.newInstance(2016, 2, 17);
        Amejora.AM_Evidencia_de_estatus__c='Evidencia';
        Amejora.AM_SEL_Estatus__c='Estatus';
        Amejora.AM_SEL_Prioridad__c='Baja';
        Amejora.AM_SEL_Responsable__c='Responsable Comercial';
        Amejora.AM_SEL_Seguimiento__c='Semanal';
        Amejora.AM_TX_Diagnostico__c='Diagnostico';
        insert(Amejora);
        
        AM_Accion_de_mejora__c Amejora2=new AM_Accion_de_mejora__c();
        Amejora2.AM_REF_Estacion_de_servicio__c=es1.Id;
        Amejora2.AM_DT_Fecha_fin_prevista__c= Date.newInstance(2017, 11, 27);
        Amejora2.AM_DT_Fecha_inicio__c=Date.newInstance(2016, 3, 20);
        Amejora2.AM_Evidencia_de_estatus__c='positiva';
        Amejora2.AM_SEL_Estatus__c='Realizada';
        Amejora2.AM_SEL_Prioridad__c='Media';
        Amejora2.AM_SEL_Responsable__c='Responsable ES';
        Amejora2.AM_SEL_Seguimiento__c='Semanal';
        Amejora2.AM_TX_Diagnostico__c='se comenta en el entrenamiento nota muy alta';
        insert(Amejora2);
        
        AM_Accion_de_mejora__c Amejora3=new AM_Accion_de_mejora__c();
        Amejora3.AM_REF_Estacion_de_servicio__c=es2.Id;
        Amejora3.AM_DT_Fecha_fin_prevista__c= Date.newInstance(2018, 12, 17);
        Amejora3.AM_DT_Fecha_inicio__c=Date.newInstance(2016, 1, 17);
        Amejora3.AM_Evidencia_de_estatus__c='Limpieza del cristal';
        Amejora3.AM_SEL_Estatus__c='En curso';
        Amejora3.AM_SEL_Prioridad__c='Alta';
        Amejora3.AM_SEL_Responsable__c='Responsable Regional';
        Amejora3.AM_SEL_Seguimiento__c='Diario';
        Amejora3.AM_TX_Diagnostico__c='Limpieza de zona de la cristalera';
        insert(Amejora3);
        
        AM_Accion_de_mejora__c Amejora4=new AM_Accion_de_mejora__c();
        Amejora4.AM_REF_Estacion_de_servicio__c=es2.Id;
        Amejora4.AM_DT_Fecha_fin_prevista__c= Date.newInstance(2018, 06, 24);
        Amejora4.AM_DT_Fecha_inicio__c=Date.newInstance(2017, 5 , 17);
        Amejora4.AM_Evidencia_de_estatus__c='Limpieza del cristal';
        Amejora4.AM_SEL_Estatus__c='En curso';
        Amejora4.AM_SEL_Prioridad__c='Media';
        Amejora4.AM_SEL_Responsable__c='Responsable Regional';
        Amejora4.AM_SEL_Seguimiento__c='Diario';
        Amejora4.AM_TX_Diagnostico__c='Limpieza de zona de la cristalera';
        insert(Amejora4);
        
        //Creamos Radar
        SurveyTaker__c radar = new SurveyTaker__c(AM_Estacion_de_servicio__c = es.Id);
        insert(radar);
        SurveyTaker__c radar2 = new SurveyTaker__c(AM_Estacion_de_servicio__c = es1.Id);
        insert(radar2);
        SurveyTaker__c radar3 = new SurveyTaker__c(AM_Estacion_de_servicio__c = es2.Id);
        insert(radar3);
        
        //Creamos Dafo
        AM_DAFO__c dafo1 = new AM_DAFO__c();
        dafo1.AM_REL_Estacion_de_servicio__c = es.Id;
        dafo1.AM_Debilidades__c='U.s. Falta de servicios como lavado /aspirado.';
        dafo1.AM_Amenazas__c='Nueva ee.s.sVentas directas.';
        dafo1.AM_Diagnostico_global_de_la_ES__c='U.s. De carretera con fuerte competencia.subsiste auras penas';
        dafo1.AM_Fortalezas__c='Marca.Solred';
        dafo1.AM_Oportunidades__c='Solred .venta cruzada';
        insert(dafo1);
        
        AM_DAFO__c dafo2 = new AM_DAFO__c();
        dafo2.AM_REL_Estacion_de_servicio__c = es1.Id;
        dafo2.AM_Debilidades__c='Accesos algo complicados';
        dafo2.AM_Amenazas__c='Entorno competitive de bajos precios';
        dafo2.AM_Diagnostico_global_de_la_ES__c='Buena ES Urbana con Bueno Servicio y bien ubicada que no ha alcanzado su tope de ventas.';
        dafo2.AM_Fortalezas__c='Buena localización y buenas instalaciones';
        dafo2.AM_Oportunidades__c='Potenciar nuevos productos y servicios como gasolina 95 premium fórmula o GLP';
        insert(dafo2);
        
        AM_DAFO__c dafo3 = new AM_DAFO__c();
        dafo3.AM_REL_Estacion_de_servicio__c = es2.Id;
        dafo3.AM_Debilidades__c='Las colas y el mal tiempo sin marquesina';
        dafo3.AM_Amenazas__c='Dodo en plaza elipticaque es mas barata';
        dafo3.AM_Diagnostico_global_de_la_ES__c='Estacion con recorrido amplio';
        dafo3.AM_Fortalezas__c='Servicio atendido y clientela habitual';
        dafo3.AM_Oportunidades__c='Mejora en plan de calidad';
        insert(dafo3);
    }
}