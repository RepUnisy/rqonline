/**
*	@name: RQO_cls_DocumentumRP2sch_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar el batch RQO_cls_DocumentumRP2_batch
*/
@IsTest
public class RQO_cls_DocumentumRP2Batch_test {
	
    /**
	* @creation date: 28/02/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRP2Batch@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createDocument(1);
        }
    }
	
    /**
	* @creation date: 28/02/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar la ejecución del batch
	* @exception: 
	* @throws: 
	*/
    static testMethod void ejecucionBatch(){
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            RQO_obj_document__c objDocumento = [Select RQO_fld_type__c from RQO_obj_document__c LIMIT 1];
            objDocumento.RQO_fld_type__c = RQO_cls_Constantes.DOC_TYPE_FICHA_SEGURIDAD;
            update objDocumento;
            
            Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>> mapServiceData = new Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>>();
            List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa> listObjRP2 = new List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>();
            RQO_cls_GetDocumentumRP2.ZeehsProdrlsa objRP2 = new RQO_cls_GetDocumentumRP2.ZeehsProdrlsa();
            objRP2.Idioma = 'es_ES';
            objRP2.Docid = 'test';
            listObjRP2.add(objRP2);
            mapServiceData.put('test', listObjRP2);
            
      		Test.startTest();
				Id batchId = Database.executeBatch(new RQO_cls_DocumentumRP2_batch(RQO_cls_Constantes.DOC_TYPE_FICHA_SEGURIDAD, 
                                                                                   new List<String>{'test'},mapServiceData), 100);
            Test.stopTest();
        }
	}
    
        /**
	* @creation date: 28/02/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar la ejecución del batch
	* @exception: 
	* @throws: 
	*/
    static testMethod void ejecucionBatchErrorInsert(){
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            RQO_obj_document__c objDocumento = [Select RQO_fld_type__c from RQO_obj_document__c LIMIT 1];
            objDocumento.RQO_fld_type__c = RQO_cls_Constantes.DOC_TYPE_FICHA_SEGURIDAD;
            update objDocumento;
            
            Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>> mapServiceData = new Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>>();
            List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa> listObjRP2 = new List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>();
            RQO_cls_GetDocumentumRP2.ZeehsProdrlsa objRP2 = new RQO_cls_GetDocumentumRP2.ZeehsProdrlsa();
            objRP2.Idioma = 'es_ES';
            //Da error de inserción al intentar guardar más de 100 caracteres
            objRP2.Docid = 'testaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
            listObjRP2.add(objRP2);
            mapServiceData.put('test', listObjRP2);
            
      		Test.startTest();
				Id batchId = Database.executeBatch(new RQO_cls_DocumentumRP2_batch(RQO_cls_Constantes.DOC_TYPE_FICHA_SEGURIDAD, 
                                                                                   new List<String>{'test'},mapServiceData), 100);
            Test.stopTest();
        }
	}

}