/**
 *	@name: RQO_cls_DocumetumRESTMockTest
 *	@version: 1.0
 *	@creation date: 18/10/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase Mock para testear los servicios REST de documentum
 *
*/
@IsTest
global class RQO_cls_DocumetumRESTMockTest implements HttpCalloutMock{
    
    private String docService;
    private String execType;
   	
    /**
    * @creation date: 18/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Constructor con parámetros de la clase
    * @param: docService tipo String, servicio de documentum que se va a testear. 
    * @param: execType tipo Stirng, tipo de ejecución (OK, NOK)
    */
    global RQO_cls_DocumetumRESTMockTest(String docService, String execType){
        this.docService = docService;
        this.execType = execType;
    }
    
    /**
    * @creation date: 18/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Implementación del método respond de la interfaz HttpCalloutMock
    * @param: req tipo HTTPRequest, request del servicio
    * @return: HTTPResponse
    */    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        //En función del servicio que estemos testeando generaremos una response concreta
        if (RQO_cls_Constantes.WS_DOCUM_GET.equalsIgnoreCase(this.docService)){
            res.setBody(this.getDirectDocumResponseBody());
        }else if(RQO_cls_Constantes.WS_DOCUM_SAP_GET.equalsIgnoreCase(this.docService)){
            res.setBody(this.getDirectDocumSapResponseBody());
        }else if (RQO_cls_Constantes.WS_DOCUM_CREATE.equalsIgnoreCase(this.docService)){
            res.setBody(this.getUploadDocResponseBody());
        }
        //En función del tipo de ejecución devolvermos un codigo de respuesta correcto o no
        if (RQO_cls_Constantes.MOCK_EXEC_OK.equalsIgnoreCase(this.execType)){
            res.setStatusCode(RQO_cls_Constantes.REST_COD_HTTP_OK);
        }else if(RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
            res.setStatusCode(RQO_cls_Constantes.REST_COD_HTTP_NOT_FOUND);
        }
        return res;
    }

    /**
    * @creation date: 18/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Recuperamos el cuerpo de la respuesta para la llamada directa contra documentum
    * @return: String
    */  
    private String getDirectDocumResponseBody(){
        String body = '';
        //Aunque la petición es rest el servicio es SOAP y por tanto la respuesta que esperamos debe tener formato XML
		if (RQO_cls_Constantes.MOCK_EXEC_OK.equalsIgnoreCase(this.execType)){
            //Response correcta de la petición
            body = '<?xml version=\'1.0\' encoding=\'UTF-8\'?>'+
                '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">'+
                '    <S:Body>'+
                '        <ns2:getResponse xmlns:ns2="http://core.services.fs.documentum.emc.com/" xmlns:ns3="http://core.datamodel.fs.documentum.emc.com/" xmlns:ns4="http://properties.core.datamodel.fs.documentum.emc.com/" xmlns:ns5="http://content.core.datamodel.fs.documentum.emc.com/" xmlns:ns6="http://rt.fs.documentum.emc.com/" xmlns:ns7="http://context.core.datamodel.fs.documentum.emc.com/" xmlns:ns8="http://profiles.core.datamodel.fs.documentum.emc.com/" xmlns:ns9="http://query.core.datamodel.fs.documentum.emc.com/">'+
                '            <return>'+
                '                <ns3:DataObjects transientId="1857080365" type="do_gupor_docagrup">'+
                '                    <ns3:Identity repositoryName="repprodocum" valueType="OBJECT_ID">'+
                '                        <ns3:ObjectId id="TestObjectId" />'+
                '                    </ns3:Identity>'+
                '                    <ns3:Properties isInternal="false">'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="object_name">'+
                '                            <ns4:Value>TEST.PDF</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:BooleanProperty" isTransient="false" name="a_is_hidden">'+
                '                            <ns4:Value>false</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:BooleanProperty" isTransient="false" name="a_archive">'+
                '                            <ns4:Value>false</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:BooleanProperty" isTransient="false" name="a_link_resolved">'+
                '                           <ns4:Value>false</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="a_content_type">'+
                '                            <ns4:Value>pdf</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:BooleanProperty" isTransient="false" name="a_full_text">'+
                '                            <ns4:Value>true</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="a_storage_type">'+
                '                            <ns4:Value>filestore_centera</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="owner_name">'+
                '                            <ns4:Value>OWNER LastName, OWNER name</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:NumberProperty" isTransient="false" name="owner_permit">'+
                '                            <ns4:Integer>7</ns4:Integer>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:NumberProperty" isTransient="false" name="group_permit">'+
                '                            <ns4:Integer>1</ns4:Integer>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:NumberProperty" isTransient="false" name="world_permit">'+
                '                            <ns4:Integer>1</ns4:Integer>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="acl_domain">'+
                '                            <ns4:Value>testDomain</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="acl_name">'+
                '                            <ns4:Value>gupor_ac_agrup</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:BooleanProperty" isTransient="false" name="a_is_template">'+
                '                            <ns4:Value>false</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:BooleanProperty" isTransient="false" name="a_is_signed">'+
                '                            <ns4:Value>false</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="negocio">'+
                '                            <ns4:Value>Química</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:BooleanProperty" isTransient="false" name="attr_is_gdcom">'+
                '                            <ns4:Value>false</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:DateProperty" isTransient="false" name="atr_fecha">'+
                '                            <ns4:Value>2000-01-01T00:00:00+01:00</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="atr_tipoob">'+
                '                            <ns4:Value>NT</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="atr_tipodoc">'+
                '                            <ns4:Value>PDF</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="atr_grado">'+
                '                            <ns4:Value>GradoName</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="atr_idioma">'+
                '                            <ns4:Value>EN</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                        <ns4:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:StringProperty" isTransient="false" name="atr_producto">'+
                '                            <ns4:Value>ProductoName</ns4:Value>'+
                '                        </ns4:Properties>'+
                '                    </ns3:Properties>'+
                '                    <ns3:Contents xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns5:BinaryContent" pageModifier="" pageNumber="0" format="pdf">'+
                '                        <ns5:renditionType>PRIMARY</ns5:renditionType>'+
                '                        <ns5:intentModifier>SET</ns5:intentModifier>'+
                '                        <ns5:Value>XXXX</ns5:Value>'+
                '                    </ns3:Contents>'+
                '                </ns3:DataObjects>'+
                '            </return>'+
                '        </ns2:getResponse>'+
                '    </S:Body>'+
                '</S:Envelope>';
        }else if(RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
            //Response incorrecta de la petición
            body = '<?xml version=\'1.0\' encoding=\'UTF-8\'?>' +
				'<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">'+
				'    <S:Body>'+
				'        <S:Fault xmlns:ns4="http://www.w3.org/2003/05/soap-envelope">'+
                '            <faultcode>S:Server</faultcode>'+
                '            <faultstring>TestError</faultstring>'+
                '        </S:Fault>'+
                '    </S:Body>'+
                '</S:Envelope>';
        }
        return body;
    }
    
	/**
    * @creation date: 19/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Recuperamos el cuerpo de la respuesta para la llamada indirecta contra documentum
    * @return: String
    */  
    private String getDirectDocumSapResponseBody(){ 
        return '<?xml version=\'1.0\' encoding=\'UTF-8\'?>'+
            '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">'+
            '    <S:Body>'+
            '        <ns6:executeResponse xmlns:ns2="http://query.core.datamodel.fs.documentum.emc.com/" xmlns:ns3="http://properties.core.datamodel.fs.documentum.emc.com/" xmlns:ns4="http://core.datamodel.fs.documentum.emc.com/" xmlns:ns5="http://context.core.datamodel.fs.documentum.emc.com/" xmlns:ns6="http://search.services.fs.documentum.emc.com/" xmlns:ns7="http://content.core.datamodel.fs.documentum.emc.com/" xmlns:ns8="http://profiles.core.datamodel.fs.documentum.emc.com/" xmlns:ns9="http://rt.fs.documentum.emc.com/">'+
            '            <return queryId="queryId-1188129178131469">'+
            '                <ns2:dataPackage>'+
            '                    <ns4:DataObjects transientId="1111155559">'+
            '                        <ns4:Identity repositoryName="repository" valueType="OBJECT_ID">'+
            '                            <ns4:ObjectId id="DocId" />'+
            '                        </ns4:Identity>'+
            '                        <ns4:Properties isInternal="false">'+
            '                            <ns3:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns3:StringProperty" isTransient="false" name="r_source">'+
            '                                <ns3:Value>repository</ns3:Value>'+
            '                            </ns3:Properties>'+
            '                            <ns3:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns3:NumberProperty" isTransient="false" name="score">'+
            '                                <ns3:Double>0.0</ns3:Double>'+
            '                            </ns3:Properties>'+
            '                            <ns3:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns3:NumberProperty" isTransient="false" name="r_seqNumber">'+
            '                                <ns3:Integer>0</ns3:Integer>'+
            '                            </ns3:Properties>'+
            '                            <ns3:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns3:NumberProperty" isTransient="false" name="r_globalSeqNumber">'+
            '                                <ns3:Integer>0</ns3:Integer>'+
            '                            </ns3:Properties>'+
            '                            <ns3:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns3:StringProperty" isTransient="false" name="r_object_id">'+
            '                                <ns3:Value>DocId</ns3:Value>'+
            '                            </ns3:Properties>'+
            '                            <ns3:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns3:DateProperty" isTransient="false" name="r_collect_date">'+
            '                                <ns3:Value>2001-01-01T00:00:00+02:00</ns3:Value>'+
            '                            </ns3:Properties>'+
            '                            <ns3:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns3:StringArrayProperty" isTransient="false" name="matching_terms" />'+
            '                        </ns4:Properties>'+
            '                    </ns4:DataObjects>'+
            '                </ns2:dataPackage>'+
            '                <ns2:queryStatus globalResultsCount="1" isCompleted="true" hasMoreResults="false">'+
            '                    <ns2:RepositoryStatusInfos isFacetResultsTruncated="false" isFacetRetrieved="false" hasMoreResults="false" resultsCount="1" hitCount="-1" status="SUCCESS" name="repdevdocum">'+
            '                        <ns2:RepositoryEvents message="Start processing" eventSubId="DEFAULT" eventId="INTERNAL" time="2017-10-19T13:44:58.580+02:00" source="repdevdocum" />'+
            '                        <ns2:RepositoryEvents message="select r_object_id from do_uuii_fichaproducto where object_name = \'ObjectName\'" eventSubId="NATIVEQUERY" eventId="INTERNAL" time="2001-01-01T00:00:00+02:00" source="repository" />'+
            '                        <ns2:RepositoryEvents message="" eventSubId="DEFAULT" eventId="FETCH" time="2001-01-01T00:00:00+02:00" source="repository" />'+
            '                        <ns2:RepositoryEvents message="1" eventSubId="DEFAULT" eventId="FOUND" time="2001-01-01T00:00:00+02:00" source="repository" />'+
            '                        <ns2:RepositoryEvents message="" eventSubId="COMPLETE" eventId="DONE" time="2001-01-01T00:00:00+02:00" source="repository" />'+
            '                    </ns2:RepositoryStatusInfos>'+
            '                </ns2:queryStatus>'+
            '            </return>'+
            '        </ns6:executeResponse>'+
            '    </S:Body>'+
            '</S:Envelope>';
    }
	
    /**
    * @creation date: 20/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Recuperamos el cuerpo de la respuesta para la llamada de carga de documentum
    * @return: String
    */ 
    private String getUploadDocResponseBody(){
    	return 	'<?xml version=\'1.0\' encoding=\'UTF-8\'?>'+
                '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">'+
                '    <S:Body>'+
                '        <ns2:createResponse xmlns:ns2="http://core.services.fs.documentum.emc.com/" xmlns:ns3="http://core.datamodel.fs.documentum.emc.com/" xmlns:ns4="http://properties.core.datamodel.fs.documentum.emc.com/" xmlns:ns5="http://content.core.datamodel.fs.documentum.emc.com/" xmlns:ns6="http://rt.fs.documentum.emc.com/" xmlns:ns7="http://context.core.datamodel.fs.documentum.emc.com/" xmlns:ns8="http://profiles.core.datamodel.fs.documentum.emc.com/" xmlns:ns9="http://query.core.datamodel.fs.documentum.emc.com/">'+
                '            <return>'+
                '                <ns3:DataObjects transientId="24374438" type="do_gupor_docagrup">'+
                '                    <ns3:Identity repositoryName="repository" valueType="OBJECT_ID">'+
                '                        <ns3:ObjectId id="DocId" />'+
                '                    </ns3:Identity>'+
                '                    <ns3:Properties isInternal="false" />'+
                '                </ns3:DataObjects>'+
                '            </return>'+
                '        </ns2:createResponse>'+
                '    </S:Body>'+
                '</S:Envelope>';   
    }
}