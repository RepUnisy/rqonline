global class TaCUserInfoLoginFlow {

        
    webService static Boolean GetUserInfo(string userId){
        
        try{
            System.debug('==> POKI1 entering GetUserInfo with UserId' + userId );
            User user = [select       ProfileId, TaCAccepted__c, TaCProfileIfAcceptTaC__c
                                from        user 
                                where id = :userId];
            return user.TaCAccepted__c;
            
        }catch(Exception e){
            System.Debug ('The following exception has occurred: ' + e.getMessage()); 
            return null;
        }
            
    }
        

}