/**
 * @name: RRSS_cls_MilestoneUtils
 * @version: 1.0
 * @creation date: 14/06/2017
 * @author: Ramon Diaz, Accenture
 * @description: Clase que valida el Milestone enviado en parámetro para los casos especificados
*/
public class RRSS_cls_MilestoneUtils {
    
    /* @creation date: 14/06/2017
     * @author: Ramon Diaz, Accenture
     * @description: Método que completa un Milestone para los casos pasados por parámetro
     *               Clase recuperada de la documentación oficial de Salesforce
     * @param: Lista de Ids de casos (caseIds), Nombre del Milestone a completar (milestoneName), Fecha de completado (complDate)
     * @return: void
     * @exception: No aplica
     * @throws: No aplica */
    public static void completeMilestone(List<Id> caseIds, List<String> milestoneName, DateTime complDate) {  
        for (string mName : milestoneName){
            System.debug ('Milestone name: ' +  mName );
        }
        for (Id caseId : caseIds){
            System.debug ('Case id: ' +  caseId );
        }        
        List<CaseMilestone> cmsToUpdate = [select Id, completionDate
            from CaseMilestone cm
            where caseId in :caseIds and cm.MilestoneType.Name in :milestoneName 
            and completionDate = null];
            System.debug('Completamos milestone : ' + cmsToUpdate.size() );
        
        if (cmsToUpdate.isEmpty() == false){
            for (CaseMilestone cm : cmsToUpdate){
                cm.completionDate = complDate;
            }
            update cmsToUpdate;
        }
    }
}