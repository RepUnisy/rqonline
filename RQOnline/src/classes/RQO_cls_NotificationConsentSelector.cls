/**
*   @name: RQO_cls_NotificationConsentSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la selección de consentimientos de notificaciones
*/
public with sharing class RQO_cls_NotificationConsentSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_NotificationConsentSelector';
	
    /**
    *   @name: RQO_cls_NotificationConsentSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase que obtiene una lista de campos del objeto de
    *   consentimientos de nptificaciones
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_obj_notificationConsent__c.Name,
			RQO_obj_notificationConsent__c.RQO_fld_Contact__c,
			RQO_obj_notificationConsent__c.RQO_fld_emailSending__c,
			RQO_obj_notificationConsent__c.RQO_fld_emailSendingPeriodicity__c,
			RQO_obj_notificationConsent__c.RQO_fld_notificationType__c };
    }

    /**
    *   @name: RQO_cls_NotificationConsentSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el tipo del objeto de consentimiento de notificaciones 
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_notificationConsent__c.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_NotificationConsentSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el campo mediainte el que realizar una ordenación 
    */
    public override String getOrderBy()
	{
		return 'RQO_fld_Contact__c';
	}

    /**
    *   @name: RQO_cls_NotificationConsentSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de consentimientos de notificaciones a través
    *   de un conjunto de ids de notificationConsents
    */
    public List<RQO_obj_notificationConsent__c> selectById(Set<ID> idSet)
    {
         return (List<RQO_obj_notificationConsent__c>) selectSObjectsById(idSet);
    }
    
    /**
    *   @name: RQO_cls_NotificationConsentSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el campo mediainte el que realizar una ordenación 
    */
   	public List<RQO_obj_notificationConsent__c> selectByContactId(Set<ID> idSet)
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectByContactId';

        string query = newQueryFactory().setCondition('RQO_fld_Contact__c IN :idSet').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
	   	return (List<RQO_obj_notificationConsent__c>) Database.query(query);
    }
    
    /**
    *   @name: RQO_cls_NotificationConsentSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de consentimientos de notificaciones
    *   en base a un conjunto de ids de notificationConsent y un tipo de consentimientos
    */
    public List<RQO_obj_notificationConsent__c> selectByType(Set<ID> idSet, string consentType)
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectByType';

        string query = newQueryFactory().setCondition('RQO_fld_Contact__c IN :idSet AND RQO_fld_notificationType__c = :consentType').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
	   	return (List<RQO_obj_notificationConsent__c>) Database.query(query);
    }
}