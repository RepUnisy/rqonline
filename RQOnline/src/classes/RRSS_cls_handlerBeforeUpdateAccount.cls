/**
 * @name: RRSS_cls_handlerBeforeUpdateAccount
 * @version: 1.0
 * @creation date: 14/06/2017
 * @author: Ramon Diaz, Accenture
 * @description: Clase Handler para Trigger Before Update de Cuentas - Valida el NIF o CIF para cuentas de RRSS
*/
public class RRSS_cls_handlerBeforeUpdateAccount {
    
    /* @creation date: 14/06/2017
     * @author: Ramon Diaz, Accenture
     * @description: Método handler que valida el NIF/CIF de la cuenta de RRSS
     * @param: Trigger.new y Trigger.old en las variables newAccounts y oldAccounts
     * @return: void
     * @exception: No aplica
     * @throws: No aplica */
    public static void handlerBeforeUpdateAccount (List<Account> newAccounts,List<Account> oldAccounts) {
        System.debug('::::::::: Parámetro newAccounts:' + newAccounts + ':::::::::');
        System.debug('::::::::: Parámetro oldAccounts:' + oldAccounts + ':::::::::');
        List<Account> accountsToValidate=[SELECT Id, RecordTypeId,rrss_NIF_CIF__c FROM Account WHERE Id IN :newAccounts];
        // Recuperamos el RecordType de cuentas personales de RRSS
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.RRSS_RecordType_Account).getRecordTypeId();
        for (Account a : newAccounts) {  
            if (a.RecordTypeId == recordTypeId) {
                String nif = a.rrss_NIF_CIF__c;
                if (!String.isBlank(nif)) {
                    // Verificamos si el NIF o CIF entrados es correcto mediante la clase ValidarNIFCIF
                    if (RRSS_cls_validarNIFCIF.validaCIF(nif) || RRSS_cls_validarNIFCIF.validaNIF(nif)) {
                        //Asignación de variable 'dummy' para validación de la clase test
                       String prueba = null;
                    } else {
                        //Añadimos un error para informar al usuario de que el NIF/CIF entrado no es correcto
                        Account errorAccount = a;
                        errorAccount.addError(Label.RRSS_Error_NIF);                 
                    }
                }
            }
                
        }
    }
}