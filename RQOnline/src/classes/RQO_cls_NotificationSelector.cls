/**
*   @name: RQO_cls_NotificationSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Unisys
*   @description: Clase que gestiona la selección de notificaciones
*/
public with sharing class RQO_cls_NotificationSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_NotificationSelector';
	
    /**
    *   @name: getSObjectFieldList
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Unisys
    *   @description: Método que obtiene una lista de campos del objeto de notificaciones
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_obj_notification__c.Name,
			RQO_obj_notification__c.RQO_fld_contact__c,
			RQO_obj_notification__c.RQO_fld_externalId__c,
			RQO_obj_notification__c.RQO_fld_objectType__c,
			RQO_obj_notification__c.RQO_fld_messageType__c,
			RQO_obj_notification__c.RQO_fld_visualized__c,
			RQO_obj_notification__c.RQO_fld_mailSent__c};
    }

    /**
    *   @name: getSObjectType
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Unisys
    *   @description: Método que retorna el tipo del objeto notificaciones 
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_notification__c.sObjectType;
    }
    
    /**
    *   @name: getOrderBy
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Unisys
    *   @description: Método que retorna el campo mediainte el que realizar una ordenación 
    */
    public override String getOrderBy()
	{
		return 'RQO_fld_contact__c';
	}

  
    /**
    *   @name: selectByContactId
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Unisys
    *   @description: Método que las notificaciones para un contacto 
    */
   	public List<RQO_obj_notification__c> selectVisualizedByContactId(Set<String> idSet, String messageType)
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectByContactId';
        
        String condition = 'RQO_fld_visualized__c = TRUE AND RQO_fld_Contact__c IN :idSet';
        if (String.isNotEmpty(messageType))
        	condition = condition + ' AND RQO_fld_messageType__c = :messageType';

        string query = newQueryFactory().setCondition(condition).toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
	   	return (List<RQO_obj_notification__c>) Database.query(query);
    }
}