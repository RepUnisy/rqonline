/**
*	@name: RQO_cls_ContactService_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: Alfonso Constan López - Unisys
*	@description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_ContactService.
*/
@isTest
public class RQO_cls_ContactService_test {
	
    /**
    *	@name: RQO_cls_ContactService_test
    *	@version: 1.0
    *	@creation date: 28/02/2018
    *	@author: Alfonso Constan López - Unisys
    *	@description: Método de test que comprueba la actualización del idioma de un usuario.
    */
    private static testMethod void whenUpdateProfileReturnsOK() {
        
        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserClienteRunAsJob@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		System.runAs(userRunAs){
            Boolean DidThrowException = false;
            try {
                
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserClienteUpdateProfile@testorg.com', 'Customer Community User', null, 'es',
                                                                    'es_ES', 'Europe/Berlin', contact[0].Id);
                
                test.startTest();
                
                List<User> users = new List<User>();
                
                users.add(u);
                RQO_cls_ContactService.updateProfile(users);
                test.stopTest();
            }
            catch (Exception e) {
                DidThrowException = true;
            }
            
            System.assertEquals(false, DidThrowException);
        }
    }
}