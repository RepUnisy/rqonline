/**
*   @name: RQO_cls_SurveyAccountSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la obtención del objeto SurveyAccount
*/
public with sharing class RQO_cls_SurveyAccountSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_SurveyAccountSelector';
	
    /**
    *   @name: RQO_cls_SurveyAccountSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la lista de campos del objeto RQO_obj_surveyAccount__c
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_obj_surveyAccount__c.RQO_fld_account__c,
			RQO_obj_surveyAccount__c.RQO_fld_survey__c,
			RQO_obj_surveyAccount__c.Name };
    }
	
    /**
    *   @name: RQO_cls_SurveyAccountSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la lista de campos del objeto RQO_obj_surveyAccount__c
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_surveyAccount__c.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_SurveyAccountSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar los objetos RQO_obj_surveyAccount__c
    */
    public override String getOrderBy()
	{
		return 'RQO_fld_survey__c';
	}
    
    /**
    *   @name: RQO_cls_SurveyAccountSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar los objetos RQO_obj_surveyAccount__c
    */
    public List<RQO_obj_surveyAccount__c> selectBySurveyId(Set<ID> idSet)
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectBySurveyId';
        
        string query = newQueryFactory().setCondition('RQO_fld_survey__c IN :idSet').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
	   	return (List<RQO_obj_surveyAccount__c>) Database.query(query);
    }
    
    /**
    *   @name: RQO_cls_SurveyAccountSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene una lista de surveys mediante un conjunto de Ids de Account
    */
    public List<RQO_obj_surveyAccount__c> selectByAccountId(Set<ID> idSet)
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'selectByAccountId';
        
        string query = newQueryFactory().setCondition('RQO_fld_account__c IN :idSet').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
	   	return (List<RQO_obj_surveyAccount__c>) Database.query(query);
    }
}