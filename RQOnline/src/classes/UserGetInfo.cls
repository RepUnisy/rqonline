global without sharing class UserGetInfo {



  @InvocableMethod
  global static List<UserGetInfoResult> UsersGetInfoTerm(List<UserGetInfoRequest> requests) {
        List<UserGetInfoResult> results = new List<UserGetInfoResult>();
        for (UserGetInfoRequest request : requests) {
            try{
            	results.add(UserGetInfoTerm(request));    
            }catch(Exception ex){
                System.debug('Error procesando las soliciud: ' + request.userId + ', error: ' + ex.getMessage());
            }           
       }
       return results;
   }
    
   public static UserGetInfoResult  UserGetInfoTerm(UserGetInfoRequest request) {
        UserGetInfoResult response = new UserGetInfoResult();
        response.termAndCondition = GetUserInfoWs(request.userId);  
        return response;
   }
      
   global class UserGetInfoRequest {           
        @InvocableVariable(required=true) 
        public ID userId;            
   }   
   global class UserGetInfoResult {           
        @InvocableVariable
        public Boolean termAndCondition;            
   } 
   
   public static boolean GetUserInfoWs(string userId){
        
        System.debug('==> POK Save before Update');
    
        //Call Web Service to update user profile
        partnerSoapSforceCom.Soap sp = new partnerSoapSforceCom.Soap();
                                  
        TaC_Custom_Setting__c settings = TaC_Custom_Setting__c.getOrgDefaults();
        Blob key = EncodingUtil.base64Decode(settings.Key__c);
        Blob user = EncodingUtil.base64Decode(settings.User__c);
        Blob pass = EncodingUtil.base64Decode(settings.Pass__c);
                
        Blob blobUserDecrypt = Crypto.decryptWithManagedIV('AES256', key, user);
        String user2 = blobUserDecrypt.toString();
                
        Blob blobPassDecrypt = Crypto.decryptWithManagedIV('AES256', key, pass);
        String pass2 = blobPassDecrypt.toString();
                
        partnerSoapSforceCom.LoginResult loginResult = sp.login(user2, pass2); 
        //partnerSoapSforceCom.LoginResult loginResult = sp.login('admin_iecisa@ieci.es.repsol.test', 'iecisa01'); 
        System.Debug('==> POK userGetInfo : ' + loginResult.serverUrl);
        System.Debug('==> POK userGetInfo : ' + loginResult.sessionId);
                
        TaCUserInfoLoginFlowWS.TaCUserInfoLoginFlow ws = new TaCUserInfoLoginFlowWS.TaCUserInfoLoginFlow();
        ws.SessionHeader = new TaCUserInfoLoginFlowWS.SessionHeader_element();
        ws.SessionHeader.sessionId = loginResult.sessionId;
                
        System.Debug ('===============> POK 1 before WS call userGetInfo'); 
        Boolean termAndCondition = ws.GetUserInfo(userId);      
        System.Debug ('===============> POK 2 after WS call userGetInfo'); 
        return termAndCondition;                
   }
       
           
}