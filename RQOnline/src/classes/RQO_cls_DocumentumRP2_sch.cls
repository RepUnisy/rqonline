/**
*	@name: RQO_cls_DocumentumRP2_sch
*	@version: 1.0
*	@creation date: 15/11/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Clase para la programación del lanzamiento del servicio de obtención diaria de documentos de RP2 y el batch de actualización de datos
*/
global class RQO_cls_DocumentumRP2_sch extends RQO_cls_ProcesoPlanificado implements Schedulable {

    private static final String JOB_NAME = RQO_cls_Constantes.SCHEDULE_CODE_RP2;
    private static final String CLASS_NAME = 'RQO_cls_DocumentumRP2_sch';

	/**
	* @creation date: 15/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Constructor por defecto
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_DocumentumRP2_sch(){
		super(JOB_NAME);
    }
    
	
	/**
	* @creation date: 15/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método execute de la clase programable. 
	* @param:	sc	tipo SchedulableContext
	* @return: 
	* @exception: 
	* @throws: 
	*/
	global void execute(SchedulableContext sc) {
        
		final String METHOD = 'execute';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Verificamos si salesforce tiene recursos disponibles para ejecutar el batch que se lanza tras recuperar los datos de RP2
		if (RQO_cls_AsyncApexJob_Helper.getActiveJobs() < RQO_cls_AsyncApexJob_Helper.MAX_JOBS && isRunnable()){
            if (!Test.isRunningTest()){
                System.debug('Encola el job');
                //Encolamos un job que se encargará de realizar el callout
                Id jobId = System.enqueueJob(new RQO_cls_EnqueueGetDocumentRP2());
            }
			schedule(true);
		}else {
            schedule(false);
		}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}

	/**
	* @creation date: 15/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Programar job para la siguiente ejecucion. 
	* @param:	blnForceNewDay	Forzado de inicio en el mismo dia
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public void schedule(Boolean blnForceNewDay) {
		final String METHOD = 'schedule';
		RQO_cls_ProcesoPlanificado.ScheduledInstance instance;
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		instance = getSchedule(getNextScheduleDatetime(blnForceNewDay));
		System.schedule(instance.name, instance.cron, new RQO_cls_DocumentumRP2_sch());
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}

	/**
	* @creation date: 15/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Lanzamiento del proceso de forma manual.
	* @return:
	* @exception:
	* @throws:
	*/
	public void lanzarProceso() {
        
		final String METHOD = 'schedule';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        if (!Test.isRunningTest()){
            //Encolamos un job que se encargará de realizar el callout
            Id jobId = System.enqueueJob(new RQO_cls_EnqueueGetDocumentRP2());
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}
	
}