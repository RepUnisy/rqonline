/**
*   @name: RQO_cls_ContactService
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Servicio de gestión de contactos
*/
public with sharing class RQO_cls_ContactService  implements RQO_cls_IContactService {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_ContactService';
	
	/**
    *   @name: RQO_cls_ContactService
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que actualiza un perfil en base a un listado de usuarios
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	public static void updateProfile(List<User> users) 
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'updateProfile';
        
        List<Contact> updateContacts = new List<Contact>();
        
        for (User user : users) {
	        List<Contact> contacts = new RQO_cls_ContactSelector().selectById(new Set<ID> { user.ContactId });
            
			if (contacts.size() > 0 && user.Contact.RQO_fld_perfilUsuario__c != contacts[0].RQO_fld_perfilUsuario__c)
			{
				contacts[0].RQO_fld_perfilUsuario__c = user.Contact.RQO_fld_perfilUsuario__c;
				System.debug(CLASS_NAME + ' - ' + METHOD + ': contact ' + contacts[0].Email + '  nuevo perfil ' + contacts[0].RQO_fld_perfilUsuario__c);
				updateContacts.add(contacts[0]);
			}
    	}
        
    	fflib_ISObjectUnitOfWork uow = RQO_cls_Application.UnitOfWork.newInstance();
        RQO_cls_Contacts contactsDom = new RQO_cls_Contacts(updateContacts);
		contactsDom.updateProfile(uow);  
        uow.commitWork();
	} 

}