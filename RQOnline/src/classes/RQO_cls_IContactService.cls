/**
*   @name: RQO_cls_IContactService
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan Lopez - Unisys
*   @description: Interfaz para implementar comportamiento de actualización de perfiles
*/
public interface RQO_cls_IContactService {
    void updateProfile(List<User> users);
}