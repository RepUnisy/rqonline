/**
*   @name: RQO_cls_seleccionador
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la selección de labels
*/
public class RQO_cls_seleccionador{
    string value;
    string label;
    
    /**
    *   @name: RQO_cls_seleccionador
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Constructor de la clase seleccionador
    */
    public RQO_cls_seleccionador(string v, string l){
        this.value = v;
        this.label = l;
    }
}