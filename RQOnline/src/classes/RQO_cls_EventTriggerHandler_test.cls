/**
*	@name: RQO_cls_RegisterEvent_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase RQO_cls_EventTriggerHandler
*/
@isTest
public class RQO_cls_EventTriggerHandler_test {
    
    /**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserEvents@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            //Creamos faqs
            RQO_cls_TestDataFactory.createDocument(1);
            RQO_cls_TestDataFactory.createFacs(1);
        }
    }
    
	/**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para comprobar el método beforeInsertExecution
    * @exception: 
    * @throws:  
    */
    @isTest
    static void beforeInsertExecution(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserEvents@testorg.com');
        System.runAs(u){
			Account cuenta = [Select id from Account LIMIT 1];
            Contact contacto = [Select id from Contact LIMIT 1];
            RQO_obj_document__c objDocument = [Select id from RQO_obj_document__c LIMIT 1];
            RQ0_obj_Facs__c facs = [Select id from RQ0_obj_Facs__c LIMIT 1];
            
            RQO_obj_event__c evento = new RQO_obj_event__c(RQO_fld_eventActionDefinition__c = '1', RQO_fld_eventActionType__c = '1', 
                                                           RQO_fld_eventClient__c = cuenta.Id,
                                                           RQO_fld_eventCompDesc__c = '1', RQO_fld_eventComponentName__c = '1', 
                                                           RQO_fld_eventContact__c = contacto.Id, 
                                                           RQO_fld_eventDocument__c = objDocument.Id, 
                                                           RQO_fld_eventFaq__c = facs.Id);
            insert evento;
            test.startTest();
            	RQO_cls_EventTriggerHandler eventHandler = RQO_cls_EventTriggerHandler.getInstance();
            	eventHandler.beforeInsertExecution(new List<RQO_obj_event__c>{evento});
            test.stopTest();
            List<RQO_obj_event__c> listaObj = [Select Id, RQO_fld_eventDocumentLocale__c from RQO_obj_event__c LIMIT 1];
			System.assert(listaObj != null);
            System.assert(!listaObj.isEmpty());
            System.assert(listaObj.size()>0);
            System.assert(String.isNotEmpty(listaObj[0].RQO_fld_eventDocumentLocale__c));
        }
    }
}