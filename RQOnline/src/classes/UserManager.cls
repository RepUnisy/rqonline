global class UserManager
{
    webService static UserManagerReturn UpdateUser(string federationId,string nombre,string apellidos,string telefono,
        string jefe , string nombreUUOO , string ubicacionFisica, string nombreSede , string pais, string unidadNegocio      
   )
    {    
        UserManagerReturn result = new UserManagerReturn();  
        result.ReturnCode = 0;        
        try
        {
            User[] users = [SELECT Id, email, FirstName,LastName,Phone,NombreUUOO__c,UbicacionFisica__c,NombreSede__c,Pais__c,UnidadNegocio__c FROM user WHERE FederationIdentifier = :federationId];   
            User user = null;  
            if (users.size()>0)
            {
                user = users[0];
            }
            else 
            {
                result.ReturnCode = 1;
                result.Message = 'No se ha encontrado el usuario: '  + federationId;
                return result;          
            }  
            if (String.isNotEmpty(nombre)){   
                if (user.FirstName != nombre){               
                    user.FirstName = nombre;
                }                
            }                                            
            if (String.isNotEmpty(apellidos)){
                if (user.LastName != apellidos){                        
                    user.LastName = apellidos;
                }                    
            }            
            if (String.isNotEmpty(telefono)){  
                if (user.Phone !=  telefono){               
                    user.Phone = telefono;
                }                    
            }    
            if (String.isNotEmpty(jefe)){
                if (jefe == '1')
                {            
                    user.Jefe__c  = true;
                }
                else{
                    user.Jefe__c = false;                
                }                   
            }   
            if (String.isNotEmpty(nombreUUOO)){ 
                if (user.NombreUUOO__c != nombreUUOO){               
                    user.NombreUUOO__c = nombreUUOO;
                }     
            }
            if (String.isNotEmpty(ubicacionFisica)){
                if (user.UbicacionFisica__c !=ubicacionFisica){              
                    user.UbicacionFisica__c = ubicacionFisica;
                }                    
            }
            if (String.isNotEmpty(nombreSede )){ 
                if (user.NombreSede__c!= nombreSede){              
                    user.NombreSede__c  = nombreSede ; 
                }                    
            }
            if (String.isNotEmpty(pais)){    
                if (user.Pais__c != pais){           
                    user.Pais__c = pais;
                }                    
            }
            if (String.isNotEmpty(unidadNegocio)){     
                if (user.UnidadNegocio__c != unidadNegocio){          
                    user.UnidadNegocio__c = unidadNegocio;     
                }        
            }                                            
            //user.IsActive = true;                                                
            update user;                     
            result.Message = 'Usuario ' + federationId + ' actualizado con Id ' + user.Id ;                      
        }
        catch(Exception ex)
        {        
            result.ReturnCode = -1;
            result.Message='Error(UpdateUser)-->' + ex.getMessage();                                
        }        
        return  result;    
    }
}