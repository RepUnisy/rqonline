/**
*  @name: RQO_cls_BusquedaGrados_cc
*  @version: 1.0
*  @creation date: 20/08/2017
*  @author: Nicolás García - Unisys
*  @description: Clase para gestionar la recuperación de los grados en las búsquedas y tablas
*
*/
public with sharing class RQO_cls_BusquedaGrados_cc {
    public static List<RQO_obj_grade__c> hacerSOQLGrados(
        String soqlAssignedGrades,
        String soqlGrades,
        String busqueda,
        String producto,
        String aplicacion,
        String segmento,
        String language,
        Boolean privado,
        Id RelatedAccount,
        List<RQO_obj_grade__c> listaGrados,
        String aux
    ) {
        List<Id> IdgradosAsignados = new List<Id>();
        List<RQO_obj_assignedGrades__c> listaGradosAsignados = Database.query(
            'SELECT Id, RQO_fld_relatedGrade__c FROM RQO_obj_assignedGrades__c ' + soqlAssignedGrades
        );
        
        System.debug('soqlAssignedGrades: ' + soqlAssignedGrades);
        System.debug('soqlAssignedGrades: ' + soqlGrades);
        
        for (RQO_obj_assignedGrades__c asig : listaGradosAsignados) {
            IdgradosAsignados.add(asig.RQO_fld_relatedGrade__c);
        }
        
        //	Añadimos los filtros
        String filtroProducto =
            (producto != null && producto != Label.RQO_lbl_null_const) ? ' AND RQO_fld_product__c = :producto ' : '';
        String filtroAplicacion =
            (aplicacion != null && aplicacion != Label.RQO_lbl_null_const) ? ' AND RQO_fld_category__c = :aplicacion' : '';
        String filtroSegmento =
            (segmento != null && segmento != Label.RQO_lbl_null_const) ? ' AND RQO_fld_category__c = :segmento ' : '';   
        String filtroQuery = '';
        
        //	Comprobamos que sea necesario filtrar por aplicación o segmento
        List<RQO_obj_grade__c> listFiltrado;
        
        if (filtroAplicacion != '' || filtroSegmento != '') {
            String filtApp = '';
            
            if (filtroAplicacion != '') {
                listFiltrado = Database.query(
                    'SELECT id FROM RQO_obj_grade__c WHERE Id IN ( SELECT RQO_fld_grade__c FROM RQO_obj_features__c WHERE id != null' + filtroAplicacion + ')');
                
                filtApp = ' AND RQO_fld_grade__c in : listFiltrado';
            }
            
            if (filtroSegmento != '') {
                listFiltrado = Database.query(
                    'SELECT id FROM RQO_obj_grade__c WHERE Id IN (SELECT RQO_fld_grade__c FROM RQO_obj_features__c WHERE id != null' + filtApp +  filtroSegmento + ')');
            }
            
            //	Creamos el bloque de la query para filtrar por segmento o aplicación
            filtroQuery = ' AND RQO_fld_grade__c in : listFiltrado';
            system.debug('listFiltrado ' + listFiltrado);        	
        }
        
        system.debug('Texto buscador: ' + aux);
        String filtroTextBuscador = '';
        
        if (aux != '%null%' && aux != '%%') {
            filtroTextBuscador =
                ' AND (RQO_fld_translation__c LIKE :aux OR RQO_fld_detailTranslation__c LIKE :aux)';
            system.debug('filtroTextBuscador ' + filtroTextBuscador);
        }
        system.debug(listFiltrado);
        String filtroPrivado = (privado)? 'RQO_fld_private__c = :privado' : 'RQO_fld_public__c = true';
        
        system.debug('ccccccccccccccccccc ' + language);
        //	Obtenemos el listado de grados a partir de las caracteristicas insertadas
        List<RQO_obj_grade__c> listGrades = Database.query(
            'SELECT id, Name FROM RQO_obj_grade__c WHERE ( ' + filtroPrivado + filtroProducto  + ' AND id IN ( SELECT RQO_fld_grade__c FROM RQO_obj_translation__c WHERE ( RQO_fld_locale__c = :language ' + filtroTextBuscador + filtroQuery + ' )))');
        
        System.debug('Prueba: ' + 'SELECT id, Name FROM RQO_obj_grade__c WHERE ( ' + filtroPrivado + filtroProducto  + ' AND id IN ( SELECT RQO_fld_grade__c FROM RQO_obj_translation__c WHERE ( RQO_fld_locale__c = :language ' + filtroTextBuscador + filtroQuery + ' )))');
        system.debug('listGrades: ' + listGrades);
        
        for (RQO_obj_grade__c grade : listGrades){
            IdgradosAsignados.add(grade.id);
        }
        system.debug('soqlGrades ' + soqlGrades) ;

        system.debug('soqlGrades ' + Database.query('SELECT Id, RQO_fld_recuentoAsignaciones__c FROM RQO_obj_grade__c ID where Id = : IdgradosAsignados')) ;
        system.debug('soqlGrades ' + Database.query('SELECT Id, RQO_fld_recuentoAsignaciones__c FROM RQO_obj_grade__c ID where Id = : IdgradosAsignados and RQO_fld_recuentoAsignaciones__c = 0')) ;
        system.debug('soqlGrades ' + Database.query('SELECT Id, Name, RQO_fld_product__c, RQO_fld_recommendation__c, RQO_fld_sku__r.RQO_fld_numContent__c, RQO_fld_new__c FROM RQO_obj_grade__c ' + soqlGrades)) ;
        return Database.query('SELECT Id, Name, RQO_fld_product__c, RQO_fld_recommendation__c, RQO_fld_sku__r.RQO_fld_numContent__c, RQO_fld_new__c FROM RQO_obj_grade__c ' + soqlGrades);
    }
    
    @AuraEnabled
    public static String getBusquedaGrados(
        String busqueda, //val
        String producto, //null
        String aplicacion, //in
        String segmento, //in
        String language, //es_ES
        Boolean privado, //false
        Id RelatedAccount //null
    ) {
        Map< Id, List<RQO_obj_grade__c> > mapaCategoriasGrados = new Map< Id, List<RQO_obj_grade__c> >();
        List<RQO_obj_grade__c> listaGrados;
        List<RQO_obj_grade__c> listaEspecificaciones;
        List<RQO_obj_assignedGrades__c> gradosAsignados;
        List<Id> IdgradosAsignados = new List<Id>();
        String aux = Label.RQO_lbl_Porcentaje + busqueda + Label.RQO_lbl_Porcentaje;
        
        System.debug('Producto: ' + producto);
        System.debug('Aplicacion: ' + aplicacion);
        System.debug('Segmento: ' + segmento);
                
        String soqlAssignedGrades = ' WHERE RQO_fld_relatedAccount__c = :RelatedAccount AND RQO_fld_relatedGrade__r.Name LIKE :aux AND RQO_fld_relatedGrade__r.RQO_fld_product__c = :producto AND RQO_fld_relatedGrade__r.RQO_fld_private__c = true';          
        String soqlGrades = ' WHERE	RQO_fld_recuentoAsignaciones__c = 0 AND Id IN :IdgradosAsignados ORDER BY Name ASC';                    					
        
        listaGrados = hacerSOQLGrados(
            soqlAssignedGrades,
            soqlGrades,
            busqueda,
            producto,
            aplicacion,
            segmento,
            language,
            privado,
            RelatedAccount,
            listaGrados,
            aux
        );        
        
        System.debug('Lista Grados debería estar ordenado por nombre: ' + listaGrados);
        
        for(RQO_obj_grade__c grado : listaGrados) {
            if (grado.RQO_fld_product__c != null) {
                if ( mapaCategoriasGrados.containsKey(grado.RQO_fld_product__c) ) {
                    mapaCategoriasGrados.get(grado.RQO_fld_product__c).add(grado);
                } else {
                    List<RQO_obj_grade__c> gradoParaMapa = new List<RQO_obj_grade__c>();
                    
                    gradoParaMapa.add(grado);
                    mapaCategoriasGrados.put(grado.RQO_fld_product__c, gradoParaMapa);
                }
            }
        }
        System.debug('mapaCategoriasGrados antes de ser serializado: ' + mapaCategoriasGrados);
        
        List<Object> listWrappers = new List<Object>();
        
        for (Id idCategoria : mapaCategoriasGrados.keySet()){
            listWrappers.add(
                getBusquedaCategoria(
                    idCategoria,
                    mapaCategoriasGrados.get(idCategoria),
                    language,
                    false,
                    privado,
                    RelatedAccount
                )
            );
        }
        
        System.debug('listWrappers: ' + listWrappers);
        return JSON.serialize(listWrappers);
    }
    
    @AuraEnabled
    //Parametros: 
    //-busqueda: Categoría sobre la que se va a buscar
    //-ordenBusqueda: especificación sobre la que se ordena
    public static Object getBusquedaCategoriaDesdeGrado(
        Id busqueda,
        List<RQO_obj_grade__c> JSONGrados,
        String language,
        boolean privado,
        Id RelatedAccount
    ) {
        if (privado == null) {
            privado = false;
        }
        
        List<RQO_obj_grade__c> categoria;
        
        if (privado) {
            categoria = new List<RQO_obj_grade__c>([SELECT Id, RQO_fld_product__c FROM RQO_obj_grade__c WHERE Id = :busqueda AND RQO_fld_private__c = true LIMIT 1]);
        } else {
            categoria = new List<RQO_obj_grade__c>([SELECT Id, RQO_fld_product__c FROM RQO_obj_grade__c WHERE Id = :busqueda AND RQO_fld_public__c = true LIMIT 1]);
        }
        
        return getBusquedaCategoria(categoria[0].RQO_fld_product__c, JSONGrados, language, true, privado, RelatedAccount);
    }
    @AuraEnabled
    //Parametros: 
    //-busqueda: Categoría sobre la que se va a buscar
    //-ordenBusqueda: especificación sobre la que se ordena
    public static Object getBusquedaCategoria(Id busqueda, List<RQO_obj_grade__c> JSONGrados, String language, Boolean gradosRelacionados, Boolean privado, Id RelatedAccount){
        
        system.debug('wwwwwwwwwwww ' + language);
        List<RQO_obj_grade__c> listGrados = new List<RQO_obj_grade__c>();
        RQO_cls_WrapperTablaObject wrapperReturn = new RQO_cls_WrapperTablaObject();
        
        Map<String, RQO_obj_category__c> mapNombreCategoria = new Map<String, RQO_obj_category__c>();
        RQO_obj_category__c cat = new RQO_obj_category__c();
        
        wrapperReturn.setCategoria(busqueda);
        
        if (gradosRelacionados) {
            wrapperReturn.setNombreCategoria(Label.RQO_lbl_gradosRelacionados);
        } else {
            for (RQO_obj_translation__c aux : [SELECT RQO_fld_translation__c FROM RQO_obj_translation__c WHERE RQO_fld_category__c = :busqueda and RQO_fld_locale__c = :language]){
                system.debug('wwwwwwwwwwww ' + aux.RQO_fld_translation__c);
                wrapperReturn.setNombreCategoria(aux.RQO_fld_translation__c);
            } 
        }
        if ( JSONGrados == null || JSONGrados.isEmpty() ){
            if (privado) {
                listGrados = new List<RQO_obj_grade__c>([SELECT Id, Name, RQO_fld_new__c, RQO_fld_sku__r.RQO_fld_numContent__c, RQO_fld_recommendation__c FROM RQO_obj_grade__c WHERE RQO_fld_product__c = :busqueda AND RQO_fld_private__c = true AND RQO_fld_recuentoAsignaciones__c = 0 ORDER BY Name ASC]);
            } else {
                listGrados = new List<RQO_obj_grade__c>([SELECT Id, Name,  RQO_fld_new__c, RQO_fld_sku__r.RQO_fld_numContent__c, RQO_fld_recommendation__c FROM RQO_obj_grade__c WHERE RQO_fld_product__c = :busqueda AND RQO_fld_public__c = true AND RQO_fld_recuentoAsignaciones__c = 0 ORDER BY Name ASC]);
            }
        } else {
            listGrados = JSONGrados;
        }
        wrapperReturn.setTraduccionesGrados(RQO_cls_Global_Class.getGradeTranslate(language, listGrados));
        List<RQO_obj_catSpecification__c> listEspecificaciones = new List<RQO_obj_catSpecification__c>([SELECT Id, RQO_fld_masterSpecification__c, RQO_fld_masterSpecification__r.Name, RQO_fld_masterSpecification__r.RQO_fld_tipologiaTexto__c FROM RQO_obj_catSpecification__c WHERE RQO_fld_category__c = :busqueda ORDER BY RQO_fld_position__c]);
       
        List<Id> listIdsMaster = new List<Id>();
        for (RQO_obj_catSpecification__c catt : listEspecificaciones) {
            listIdsMaster.add(catt.RQO_fld_masterSpecification__c);
        }
        
        List<RQO_obj_translation__c> listTranslation = [select id, RQO_fld_translation__c, RQO_fld_locale__c from RQO_obj_translation__c where RQO_fld_masterSpecification__c in : listIdsMaster AND RQO_fld_locale__c = : language];
        
        wrapperReturn.setListaEspecificaciones(listEspecificaciones);
        wrapperReturn.setListaTraduccionesEspecifi(listTranslation);
        
        wrapperReturn.setTipoDatoEspecificaciones();
        
        List<RQO_obj_specification__c> listaEspecificacionesGrado = new List<RQO_obj_specification__c>();
        listaEspecificacionesGrado = [SELECT Id, RQO_fld_grade__r.Name, RQO_fld_grade__r.RQO_fld_new__c , RQO_fld_value__c, RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.Name FROM RQO_obj_specification__c WHERE RQO_fld_grade__c IN :listGrados ORDER BY RQO_fld_catSpecification__r.RQO_fld_position__c, RQO_fld_catSpecification__r.Name, RQO_fld_value__c];
        System.debug('listaEspecificacionesGrado: ' + listaEspecificacionesGrado);
        
        for (Integer i = listaEspecificacionesGrado.size() - 1; i >= 0; i--){
            System.debug('listaEspecificacionesGrado[i].RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.Name: ' + listaEspecificacionesGrado[i].RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.Name);
            wrapperReturn.putElement(listaEspecificacionesGrado[i].RQO_fld_grade__c, listaEspecificacionesGrado[i].RQO_fld_catSpecification__c, listaEspecificacionesGrado[i].RQO_fld_value__c);
        }
        
        System.debug('wrapperReturn antes de rellenar huecos: ' + wrapperReturn);
        wrapperReturn.rellenarHuecos(listGrados);
        System.debug('wrapperReturn: ' + wrapperReturn);
        
        if (JSONGrados == null || JSONGrados.isEmpty()){
            return JSON.serialize(wrapperReturn);
        }
        else {
            return wrapperReturn;
        }
    }
    
    /**
    * @creation date: 13/02/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Obtención e inicialización de los filtros del buscador. Producto, Segmento y Aplicación
    * @param: locale. Idioma en el que se desean obtener los filtros
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static String inicializarFiltros(String locale){
        
        Map<Id, RQO_obj_category__c>  mapCategory = new Map<Id, RQO_obj_category__c>([SELECT Id FROM RQO_obj_category__c WHERE (RQO_fld_type__c = :Label.RQO_lbl_Segmentos OR RQO_fld_type__c = :Label.RQO_lbl_Aplicaciones OR (RQO_fld_type__c = :Label.RQO_lbl_productKey AND RQO_fld_parentCategory__c != ''))]);
        
        Map<String, List<RQO_obj_translation__c> > mapaFiltros = getFilterMap(locale, new List<Id>(mapCategory.keySet()));
        return JSON.serialize(mapaFiltros);
    }
    
     @AuraEnabled
    public static  Map<String, List<RQO_obj_translation__c> > getFilterMap(String locale,  List<Id> listCategory){
                
        List<RQO_obj_translation__c> listaProductos = new List<RQO_obj_translation__c>();
        List<RQO_obj_translation__c> listaSegmentos = new List<RQO_obj_translation__c>();
        List<RQO_obj_translation__c> listaAplicaciones = new List<RQO_obj_translation__c>();
        Map<String, List<RQO_obj_translation__c> > mapaFiltros = new Map<String, List<RQO_obj_translation__c> >();
        
        //	Obtenemos el listado de traducciones para todos los filtros
        for(RQO_obj_translation__c filtro : [SELECT Id, RQO_fld_translation__c, RQO_fld_locale__c, RQO_fld_category__r.RQO_fld_type__c, RQO_fld_category__c FROM RQO_obj_translation__c where RQO_fld_locale__c = :locale AND RQO_fld_category__c in : listCategory]){
            //	Clasificamos las categorias dependiendo de su tipología
            if (filtro.RQO_fld_category__r.RQO_fld_type__c == Label.RQO_lbl_Segmentos){
                listaSegmentos.add(filtro);
            } 
            else {
                if (filtro.RQO_fld_category__r.RQO_fld_type__c == Label.RQO_lbl_Aplicaciones){
                    listaAplicaciones.add(filtro);
                } 
                else {
                    listaProductos.add(filtro);
                }
            }
        }
        
        mapaFiltros.put(Label.RQO_lbl_segmentos, listaSegmentos);
        mapaFiltros.put(Label.RQO_lbl_aplicaciones, listaAplicaciones);
        mapaFiltros.put(Label.RQO_lbl_productKey, listaProductos);
        
        System.debug('mapaFiltros inicializar Filtros: ' + mapaFiltros);
        return mapaFiltros;
    }
    
    
    @AuraEnabled
    //Parametros: 
    public static String lanzarCambioPicklist(String product, String aplicacion, String locale) {
        Map<String, List<RQO_obj_category__c> > mapCategorias = new Map<String, List<RQO_obj_category__c> >();
        System.debug('product: ' + product);
        System.debug('aplicacion: ' + aplicacion);
        
        List<Id> listCategoria = new List<Id>();
        if(product != 'null'){
            mapCategorias.put(Label.RQO_lbl_Segmentos, new List<RQO_obj_category__c>());
            mapCategorias.put(Label.RQO_lbl_Aplicaciones, new List<RQO_obj_category__c>());
            
            
            for(AggregateResult feat : [SELECT RQO_fld_category__r.Id, RQO_fld_category__r.Name, RQO_fld_category__r.RQO_fld_type__c tipo
                                        FROM RQO_obj_features__c 
                                        WHERE RQO_fld_grade__c in 
                                        (SELECT id FROM RQO_obj_grade__c  WHERE RQO_fld_product__c = :product) 
                                        GROUP BY RQO_fld_category__r.Id, RQO_fld_category__r.RQO_fld_type__c, RQO_fld_category__r.Name]){
                                            
                                            Id idCat = (Id)feat.get('Id');
                                            listCategoria.add(idCat);
                                        }
        }
        else {
            if(aplicacion != 'null'){
                mapCategorias.put(Label.RQO_lbl_Segmentos, new List<RQO_obj_category__c>());
                List<Id> listaIdsEspecificaciones = new List<Id>();
                for(RQO_obj_features__c aux : [SELECT RQO_fld_grade__c FROM RQO_obj_features__c WHERE RQO_fld_category__c = :aplicacion]){
                    listaIdsEspecificaciones.add(aux.RQO_fld_grade__c);
                }
                
                for(AggregateResult feat : [SELECT RQO_fld_category__r.Id, RQO_fld_category__r.Name, RQO_fld_category__r.RQO_fld_type__c tipo 
                                            FROM RQO_obj_features__c 
                                            WHERE RQO_fld_grade__c IN 
                                            (SELECT Id FROM RQO_obj_grade__c WHERE Id IN :listaIdsEspecificaciones) 
                                            GROUP BY RQO_fld_category__r.Id, RQO_fld_category__r.RQO_fld_type__c, RQO_fld_category__r.Name]){
                                                Id idCat = (Id)feat.get('Id');
                                                listCategoria.add(idCat);
                                            }
                
            }
            else{
                mapCategorias.put(Label.RQO_lbl_Segmentos, new List<RQO_obj_category__c>());
                mapCategorias.put(Label.RQO_lbl_Aplicaciones, new List<RQO_obj_category__c>());
                
                for(AggregateResult feat : [SELECT RQO_fld_category__r.Id, RQO_fld_category__r.Name, RQO_fld_category__r.RQO_fld_type__c tipo
                                            FROM RQO_obj_features__c 
                                            GROUP BY RQO_fld_category__r.Id, RQO_fld_category__r.RQO_fld_type__c, RQO_fld_category__r.Name]){
                                                
                                                Id idCat = (Id)feat.get('Id');
                                                listCategoria.add(idCat);
                                            }                
            }
        }
        
        Map<String, List<RQO_obj_translation__c>> mapaFiltros = getFilterMap(locale, listCategoria);
        return JSON.serialize(mapaFiltros);
    }
    
    @AuraEnabled
    public static String getAllGrades(String busqueda, Boolean privado, Id relatedAccount, String language) {
        System.debug('busqueda: ' + busqueda);
        System.debug('privado: ' + privado);
        System.debug('aaaaaaaaaaaaaaaaaaaaaaaaaaaa: ' + language);
        List<RQO_obj_grade__c> allGrades = new List<RQO_obj_grade__c>();
        List<RQO_obj_translation__c> allTranslation = new List<RQO_obj_translation__c>();
        
        if(busqueda != '') {
            String aux = Label.RQO_lbl_Porcentaje +  busqueda +  Label.RQO_lbl_Porcentaje;
            
            if (privado) {
                List<RQO_obj_assignedGrades__c> listaGradosAsignados = new List<RQO_obj_assignedGrades__c>([SELECT Id, RQO_fld_relatedGrade__c FROM RQO_obj_assignedGrades__c WHERE RQO_fld_relatedAccount__c = :RelatedAccount AND RQO_fld_relatedGrade__r.Name LIKE :aux AND RQO_fld_relatedGrade__r.RQO_fld_private__c = true LIMIT 5]);
                List<Id> IdgradosAsignados = new List<Id>();
                
                System.debug('listaGradosAsignados: ' + listaGradosAsignados);
                
                for(RQO_obj_assignedGrades__c asig : listaGradosAsignados) {
                    IdgradosAsignados.add(asig.RQO_fld_relatedGrade__c);
                }
                
                allGrades = new List<RQO_obj_grade__c>([SELECT Name FROM RQO_obj_grade__c  LIMIT 5]);
                allTranslation = new List<RQO_obj_translation__c> ([SELECT RQO_fld_translation__c from RQO_obj_translation__c where RQO_fld_locale__c = : language AND RQO_fld_grade__c in (select id FROM RQO_obj_grade__c WHERE (Name LIKE :aux AND RQO_fld_private__c = true AND RQO_fld_recuentoAsignaciones__c = 0) OR Id IN :IdgradosAsignados) LIMIT 5]);
            }
            else {
                allTranslation = new List<RQO_obj_translation__c> ([SELECT RQO_fld_translation__c from RQO_obj_translation__c where RQO_fld_locale__c = : language AND RQO_fld_grade__c in (select id FROM RQO_obj_grade__c WHERE Name LIKE :aux AND RQO_fld_public__c = true AND RQO_fld_recuentoAsignaciones__c = 0) LIMIT 5]);
            }
            
            System.debug('allGrades: ' + allGrades);
        }
        
        return JSON.serialize(allTranslation);
    }
}