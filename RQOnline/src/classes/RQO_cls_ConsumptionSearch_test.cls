/**
*	@name: RQO_cls_ConsumptionSearch_test
*	@version: 1.0
*	@creation date: 27/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase RQO_cls_ConsumptionSearch_cc
*/
@IsTest
public class RQO_cls_ConsumptionSearch_test {
    
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserConsumos@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
    }
         
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar el método getMonths
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getMonths(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            test.startTest();
            	Map<String, String> mapMeses = RQO_cls_ConsumptionSearch_cc.getMonths();
			test.stopTest();
            System.assertEquals(12, mapMeses.size());
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar el método getYears
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getYears(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            test.startTest();
            	List<String> listaAnnios = RQO_cls_ConsumptionSearch_cc.getYears();
			test.stopTest();
            System.assertEquals(5, listaAnnios.size());
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar el método getFamilies
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getFamilies(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSSapFamilies();
            test.startTest();
            	Map<String, String> mapFamilies = RQO_cls_ConsumptionSearch_cc.getFamilies();
			test.stopTest();
            System.assertEquals(2, mapFamilies.size());
        }
    }    
}