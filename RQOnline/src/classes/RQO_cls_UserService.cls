public with sharing class RQO_cls_UserService {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_UserService';
	
	private static final String EMAIL_TEMPLATE_SAC = 'RQO_eplt_correoNuevoContactoSAC_';
	private static final String EMAIL_TEMPLATE_MASTER = 'RQO_eplt_correoNuevoContactoMaster_';
	private static final String DEFAULT_LOCALE = 'en';
	
	/**
	* @creation date: 11/12/2017
	* @author: Unisys
	* @description:	 Método para obtener el contacto de un usuario 
	* @param:	userId		Identificador del usuario	
	* @throws: 	
	*/
	public static Contact getContact(Id userId) 
    {
    	User user = null;
        if (userId != null)
        {
        	user = new RQO_cls_UserSelector().selectById(new Set<Id>{userId})[0];
        	List<Contact> contacts = new RQO_cls_ContactSelector().selectById(new Set<Id>{user.ContactId});
        	if (contacts.size() > 0)
        		return contacts[0];
        	else 
        		return null;
        }
        else
        	return null;
    }
	
	public static void updateLanguage(ID userId, string language) 
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'updateLanguage';
        
        List<User> updateUsers = new RQO_cls_UserSelector().selectById(new Set<ID> { userId });
    	if (updateUsers != null && updateUsers.size() > 0)
    	{
    		updateUsers[0].LanguageLocaleKey = language;
    		updateUsers[0].LocaleSidKey  = language;
    	}
        
    	fflib_ISObjectUnitOfWork uow = RQO_cls_Application.UnitOfWork.newInstance();
        RQO_cls_Users usersDom = new RQO_cls_Users(updateUsers);
		usersDom.updateUser(uow);  
        uow.commitWork();
	} 
	
	public static void unregister(ID userId) 
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'unregister';
        
        // Dar de baja el usuario del registro (Gigya)
        List<User> users = new RQO_cls_UserSelector().selectById(new Set<ID> { userId });
        System.debug(CLASS_NAME + ' - ' + METHOD + ' users: ' + users);
	  	new RQO_cls_Users(users).unregister();
	  	
	  	// Se desactiva el usuario de Salesforce
	  	fflib_ISObjectUnitOfWork uow = RQO_cls_Application.UnitOfWork.newInstance();
	  	new RQO_cls_Users(users).deactivate(uow);
	  	uow.commitWork();
	} 
	
	
	/**
	* @creation date: 11/12/2017
	* @author: Unisys
	* @description:	 Método para el envío de correo al crearse un nuevo usuario de portal 
	* @param:	userId		Identificador del nuevo contacto	
	* @throws: 	RQO_cls_CustomException		Cuando no se encuentra una plantilla para el correo
	*/
	public static void sendNewRegistrationEmail(Id contactId) 
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'sendNewRegistrationEmail';
        
    	List<User> users = 
    		new RQO_cls_UserSelector().selectByContactId(new Set<Id> { contactId });
    		
    	System.debug(CLASS_NAME + ' - ' + METHOD + ' users.size() : ' + users.size());
    		
    	if (users.size() > 0)
    	{
    		User user = users[0];
    		
    		// Indica si el cliente tiene un usuario Máster
	        Contact masterContact = null;
	        
	        List<User> accountUsers = new RQO_cls_UserSelector().selectByAccountIdWithContact(new Set<Id>{user.AccountId});
	        for (User accountUser : accountUsers)
	        {
	        	if (accountUser.Contact.RQO_fld_perfilUsuario__c == RQO_cls_Constantes.USER_PROFILE_MASTER)
	        	{
	        		masterContact = accountUser.Contact;
	        		break;
	        	}
	        }
    		
	        // Seleccionar plantilla de correo
	        EmailTemplate emailTemplate = RQO_cls_UserService.getEmailTemplate(user, masterContact);
	        
	        System.debug(CLASS_NAME + ' - ' + METHOD + ' emailTemplate : ' + emailTemplate);
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' masterContact : ' + masterContact);
	    		
	        // Seleccionar destinatario
	        List<String> emailsTo = new List<String>();
	        if (masterContact != null)
	        	emailsTo.add(masterContact.Email);
	        else
	        	emailsTo = RQO_cls_UserService.getAddresses(user);

	        // Envío de correo
	        for (String emailTo: emailsTo)
	        {
		        RQO_cls_UserService.sendEmailWithTemplate(user, emailTemplate, emailTo);
        	}
        	
        	System.debug(CLASS_NAME + ' - ' + METHOD + ' emailsTo.size() : ' + emailsTo.size());
        }
    }
    
    /**
	* @creation date: 11/12/2017
	* @author: Unisys
	* @description:	 Método para el envío de correo al crearse un nuevo usuario de portal 
	* @param:	userId		Identificador del nuevo contacto	
	* @throws: 	RQO_cls_CustomException		Cuando no se encuentra una plantilla para el correo
	*/
	public static void sendNewRegistrationEmails(Set<Id> contactIds) 
    {
		// ***** CONSTANTES ***** //
        final String METHOD = 'sendNewRegistrationEmails';
        
        if (contactIds.size() > 0)
        {
	        Map<Id, User> usersMap = new Map<Id, User>();
	        Map<Id, User> masterUsersMap = new Map<Id, User>();
	        Map<Id, String> templateNamesMap = new Map<Id, String>();
	        Map<Id, String> addressesMap = new Map<Id, String>();
	        
	        Set<Id> accountIds = new Set<Id>();
	        Set<String> templateNames = new Set<String>();
	        
	        // Recuperar usuarios de los contactos
	        List<User> users = new RQO_cls_UserSelector().selectByContactId(contactIds);
	        
	        if (users.size() > 0) {
		        for (User user: users) {
		        	usersMap.put(user.ContactId, user);
		        	accountIds.add(user.AccountId);
		        }
		        
		        // Obtener dirección de correo SAC
		        List<String> emailsTo = RQO_cls_UserService.getAddresses(users[0]);
		        
		        // Recuperar usuarios master de los contactos
		        List<User> accountUsers = new RQO_cls_UserSelector().selectByAccountIdWithContact(accountIds);
				for (User user: users) {
					for (User accountUser: accountUsers) {
			        	if (accountUser.AccountId == user.AccountId && 
			        		accountUser.Contact.RQO_fld_perfilUsuario__c == RQO_cls_Constantes.USER_PROFILE_MASTER)
			        	{
							masterUsersMap.put(user.ContactId, accountUser);
							addressesMap.put(user.ContactId, accountUser.Email);
							templateNamesMap.put(user.ContactId, EMAIL_TEMPLATE_MASTER + user.LanguageLocaleKey.substring(0,2));
							templateNames.add(EMAIL_TEMPLATE_MASTER + user.LanguageLocaleKey.substring(0,2));
							break;
			        	}
			        }	
			        masterUsersMap.put(user.ContactId, null);
			        addressesMap.put(user.ContactId, emailsTo[0]);
			        templateNamesMap.put(user.ContactId, EMAIL_TEMPLATE_SAC + user.LanguageLocaleKey.substring(0,2));
			        templateNames.add(EMAIL_TEMPLATE_SAC + user.LanguageLocaleKey.substring(0,2));
				}
				
				// Recuperar plantillas de correo
				List<EmailTemplate> lstEmailTemplates = 
					new RQO_cls_emailTemplateSelector().selectByDeveloperName(templateNames);
		
		        // Envío de correo
		        for (Id contacId : usersMap.keySet()) {
		        	for (EmailTemplate emailTemplate: lstEmailTemplates)
		        	{
		        		if (emailTemplate.DeveloperName == templateNamesMap.get(contacId))
			        		RQO_cls_UserService.sendEmailWithTemplate(usersMap.get(contacId), emailTemplate, addressesMap.get(contacId));
		        	}
		    	}
			}
        }
    }
    
    
	/**
	* @creation date: 11/12/2017
	* @author: Unisys
	* @description:	 Método para la selección de la plantilla de correo en función del idioma del usuario 
	* @param:	User		Usuario que se ha dado de alta	
	* @throws: 	RQO_cls_CustomException		Cuando no se encuentra una plantilla para el correo
	*/
	private static EmailTemplate getEmailTemplate(User user, Contact masterContact) 
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'getEmailTemplate';
        
        String templateName = '';
        if (masterContact != null)
        	templateName = EMAIL_TEMPLATE_MASTER;
        else
        	templateName = EMAIL_TEMPLATE_SAC;
        
		String locale = user.LanguageLocaleKey.substring(0,2);
        System.debug(CLASS_NAME + ' - ' + METHOD + ' locale : ' + locale);
        EmailTemplate emailTemplate;
		List<EmailTemplate> lstEmailTemplates = 
			new RQO_cls_emailTemplateSelector().selectByDeveloperName(new Set<String> { templateName + locale });
		if (lstEmailTemplates.size() > 0)
		{
			emailTemplate = lstEmailTemplates[0];
		}
		else
		{
			lstEmailTemplates = 
				new RQO_cls_emailTemplateSelector().selectByDeveloperName(new Set<String> { templateName + DEFAULT_LOCALE });
			if (lstEmailTemplates.size() > 0)
				emailTemplate = lstEmailTemplates[0];
			else
				throw new RQO_cls_CustomException('Plantilla de correo inexistente');
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ' emailTemplate.DeveloperName : ' + emailTemplate.DeveloperName);
		System.debug(CLASS_NAME + ' - ' + METHOD + ' emailTemplate.HtmlValue : ' + emailTemplate.HtmlValue);
		
		return emailTemplate;
    }
    
    public static List<String> getAddresses(User user) 
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'getAddresses';
        
        List<String> emailsTo = new List<string>();
        
    	List<RQO_cmt_informationMails__mdt> emails = 
	       	new RQO_cls_informationMailsSelector().selectMail(null, 
	       													  null, 
	       													  new Set<String> { RQO_cls_Constantes.CONTACT_HELP_AND_SUPPORT });
		if (emails.size() > 0)
			emailsTo.add(emails[0].RQO_fld_email__c);
		else
			throw new RQO_cls_CustomException('Dirección de correo inexistente');

	    return emailsTo;
    }
    
    private static void sendEmailWithTemplate(User user, EmailTemplate emailTemplate, string email) 
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'sendEmailWithTemplate';
        
        List<Messaging.SingleEmailMessage> allMails = new List<Messaging.SingleEmailMessage>();
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new List<String>{email});
		mail.subject = emailTemplate.Subject;
		string body = emailTemplate.HtmlValue;
    	body = body.replace('{!User.FirstName}', user.Contact.FirstName);
    	body = body.replace('{!User.LastName}', user.Contact.LastName);
		body = body.replace('{!User.Account}', user.AccountId);	
        System.debug(CLASS_NAME + ' - ' + METHOD + ' body : ' + body);
        mail.setHtmlBody(body);
		mail.setSaveAsActivity(false);
		// Specify the address used when the recipients reply to the email. 
		mail.setReplyTo('noreply@repsol.com');
		// Specify the name used as the display name.
		mail.setSenderDisplayName('Repsol Química Omline');
			
		allMails.add(mail);
		
		Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(allMails); 
		if (resultMail[0].success) {
		    System.debug(CLASS_NAME + ' - ' + METHOD + ' Envío de correo : ' + email);
		} else {
			System.debug(CLASS_NAME + ' - ' + METHOD + ' Error : ' + resultMail[0].errors[0].message);
		}
    }
}