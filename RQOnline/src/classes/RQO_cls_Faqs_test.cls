/**
*	@name: RQO_cls_Faqs_test
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: Juan E. Laiseca - Unisys
*	@description: Clase de test para probar las Faqs
*/
@IsTest
private class RQO_cls_Faqs_test {
    
    /**
* @creation date: 26/02/2018
* @author: Juan E. Laiseca - Unisys
* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
* @exception: 
* @throws: 
*/
    @testSetup 
    static void initialSetup() {
        RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null, 'es', 'es_ES', 'Europe/Berlin');
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserInitial@testorg.com');
        System.runAs(u){
        	RQO_cls_TestDataFactory.createFacs(200);
        }
    }
    
    /**
* @creation date: 22/02/2018
* @author: Juan E. Laiseca - Unisys
* @description:	Método test para probar la funcionalidad correcta dentro de las Faqs
* @exception: 
* @throws:  
*/
    @isTest 
    static void test1GetFaqsCategorias(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            list<String> listaCategoriasEsperada = new list<String>();
            
            List<Schema.PicklistEntry> ple = RQ0_obj_Facs__c.RQO_fld_topic__c.getDescribe().getPicklistValues();
            for(Schema.PicklistEntry f : ple){
                listaCategoriasEsperada.add(f.getLabel());
            }
            
            Test.startTest();
            List<String> listaCategorias = RQO_cls_Faqs_cc.getFaqsCategorias('', '', '');
            System.assertEquals(listaCategoriasEsperada, listaCategorias);
            test.stopTest();
        }
    }
    
    /**
* @creation date: 22/02/2018
* @author: Juan E. Laiseca - Unisys
* @description:	Método test para probar la funcionalidad correcta dentro de las Faqs
* @exception: 
* @throws:  
*/    
    @isTest static void test2GetFaqsCategorias(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<String> listaCategoriasRes = new list<String>();
            list<String> listaCategoriasEsperada = new list<String>();
            list<String> listaCategorias = new list<String>();
            List<RQ0_obj_Facs__c> preguntas = new List<RQ0_obj_Facs__c>();
            set<String> setCategorias = new set<String>();
            map<string, string> mapaLabels = new map<string, string>();
            String lenguaje = 'es_ES';
            String palabra = 'proveedor';
            
            Schema.DescribeFieldResult fieldResult = RQ0_obj_Facs__c.RQO_fld_topic__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for(Schema.PicklistEntry f : ple){
                mapaLabels.put(f.getValue(), f.getLabel());
            }
            
            Test.startTest();
            
            //CATEGORIAS 2.1
            preguntas = [Select RQO_fld_topic__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'cliente' OR  RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra) ORDER BY RQO_fld_topic__c ASC];
            listaCategoriasEsperada = categoriasEsperadas(preguntas, setCategorias, listaCategorias, mapaLabels);
            
            listaCategoriasRes = RQO_cls_Faqs_cc.getFaqsCategorias('es_ES', 'proveedor', 'cliente');      
            System.assertEquals(listaCategoriasEsperada, listaCategoriasRes);
            
            //CATEGORIAS 2.2   
            preguntas.clear();
            preguntas = [Select RQO_fld_topic__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra) ORDER BY RQO_fld_topic__c ASC];
            
            setCategorias.clear();
            listaCategorias.clear();
            listaCategoriasEsperada = categoriasEsperadas(preguntas, setCategorias, listaCategorias, mapaLabels);
            
            listaCategoriasRes.clear();
            listaCategoriasRes = RQO_cls_Faqs_cc.getFaqsCategorias('es_ES', 'proveedor', 'noCliente');
            System.assertEquals(listaCategoriasEsperada, listaCategoriasRes);
            
            //CATEGORIAS 2.3
            preguntas.clear();
            preguntas = [Select RQO_fld_topic__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra) ORDER BY RQO_fld_topic__c ASC];
            
            setCategorias.clear();
            listaCategorias.clear();
            listaCategoriasEsperada = categoriasEsperadas(preguntas, setCategorias, listaCategorias, mapaLabels);
            
            listaCategoriasRes.clear();
            listaCategoriasRes = RQO_cls_Faqs_cc.getFaqsCategorias('es_ES', 'proveedor', 'publico');
            System.assertEquals(listaCategoriasEsperada, listaCategoriasRes);
            
            test.stopTest();
        }
    }
    
    /**
* @creation date: 22/02/2018
* @author: Juan E. Laiseca - Unisys
* @description:	Método test para probar la funcionalidad correcta dentro de las Faqs
* @exception: 
* @throws:  
*/
    @isTest static void test3GetFaqsPreguntas(){
        User user = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(user){
            List<Account> account = RQO_cls_TestDataFactory.createAccount(1);
            List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
            User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserFaqs@testorg.com', 'Customer Community User', null, 'es',
                                                                'es_ES', 'Europe/Berlin', contact[0].Id);
        }
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFaqs@testorg.com');
        System.runAs(u){
            List<RQ0_obj_Facs__c> preguntasRes = new List<RQ0_obj_Facs__c>();
            List<RQ0_obj_Facs__c> preguntasEsperada = new List<RQ0_obj_Facs__c>();
            map<string, string> mapaLabels = new map<string, string>();
            String lenguaje = 'es_ES';
            String topic = 'Corporate information'; 
            String palabra = ''; 
            String tipoUsuario = 'cliente';
           	
            List<Schema.PicklistEntry> ple = RQ0_obj_Facs__c.RQO_fld_topic__c.getDescribe().getPicklistValues();
            
            for(Schema.PicklistEntry f : ple){
                mapaLabels.put(f.getLabel(), f.getValue());
            }
            
            topic = mapaLabels.get(topic);
            
            preguntasEsperada = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'cliente' OR  RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic];
            
            Test.startTest();
            
            //PREGUNTAS 1.1
            String topicRes = 'Corporate information';
            preguntasRes = RQO_cls_Faqs_cc.getFaqsPreguntas(lenguaje, topicRes, palabra, tipoUsuario); 
            System.assertEquals(preguntasEsperada, preguntasRes);
            
            
            //PREGUNTAS 1.2 
            preguntasEsperada.clear();
            preguntasEsperada = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic];
            tipoUsuario = 'noCliente';
            preguntasRes = RQO_cls_Faqs_cc.getFaqsPreguntas(lenguaje, topicRes, palabra, tipoUsuario);
            //System.assertEquals(preguntasEsperada, preguntasRes);
            
            //PREGUNTAS 1.3
            preguntasEsperada.clear();
            preguntasEsperada = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic];
            tipoUsuario = 'publico';
            preguntasRes = RQO_cls_Faqs_cc.getFaqsPreguntas(lenguaje, topicRes, palabra, tipoUsuario);
            System.assertEquals(preguntasEsperada, preguntasRes);
            
            //PREGUNTAS 2.1 
            preguntasEsperada.clear();
            palabra = 'proveedor';
            preguntasEsperada = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'cliente' OR  RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra)];
            tipoUsuario = 'cliente';
            preguntasRes = RQO_cls_Faqs_cc.getFaqsPreguntas(lenguaje, topicRes, palabra, tipoUsuario);
            System.assertEquals(preguntasEsperada, preguntasRes); 
            
            //PREGUNTAS 2.2
            preguntasEsperada.clear();
            preguntasEsperada = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'noCliente' OR RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra)];
            tipoUsuario = 'noCliente';
            preguntasRes = RQO_cls_Faqs_cc.getFaqsPreguntas(lenguaje, topicRes, palabra, tipoUsuario);
            //System.assertEquals(preguntasEsperada, preguntasRes);
            
            //PREGUNTAS 2.3
            preguntasEsperada.clear();
            preguntasEsperada = [SELECT RQO_fld_question__c, RQO_fld_answer__c FROM RQ0_obj_Facs__c WHERE (RQO_fld_profile__c = 'publico') AND RQO_fld_locale__c = :lenguaje AND RQO_fld_topic__c = :topic AND (RQO_fld_question__c LIKE :palabra OR RQO_fld_answer__c LIKE :palabra)];
            tipoUsuario = 'publico';
            preguntasRes = RQO_cls_Faqs_cc.getFaqsPreguntas(lenguaje, topicRes, palabra, tipoUsuario);
            System.assertEquals(preguntasEsperada, preguntasRes);
            
            test.stopTest();
            
        }
    }
    
    /**
* @creation date: 22/02/2018
* @author: Juan E. Laiseca - Unisys
* @description:	Método test para probar la funcionalidad correcta dentro de las Faqs
* @exception: 
* @throws:  
*/
    private static list<String> categoriasEsperadas(List<RQ0_obj_Facs__c> preguntas, set<String> setCategorias, list<String> listaCategoriasEsperada, map<string, string> mapaLabels){
        for(RQ0_obj_Facs__c aux : preguntas){
            setCategorias.add(aux.RQO_fld_topic__c);
        }
        
        for(string aux : setCategorias){
            listaCategoriasEsperada.add(mapaLabels.get(aux)); 
        }
        return listaCategoriasEsperada;
    }
}