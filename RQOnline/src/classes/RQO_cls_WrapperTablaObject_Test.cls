/**
*	@name: RQO_cls_WrapperTablaObject_Test
*	@version: 1.0
*	@creation date: 30/10/2017
*	@author: Nicolás García - Unisys
*	@description: Clase de test para probar la clase RQO_cls_WrapperTablaObject
*/
@IsTest
public class RQO_cls_WrapperTablaObject_Test {
    
    /**
*	@name: initialSetup
*	@version: 1.0
*	@creation date: 20/02/2018
*	@author: Juan Elías Laiseca - Unisys
*	@description: Setup testing method for required initial dataset
*/
    @testSetup 
    static void initialSetup() {
        RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es',
                                             'es_ES','Europe/Berlin');     
        
    }
    
    /**
* @creation date: 30/10/2017
* @author: Nicolás García - Unisys
* @description:	Método que ejecuta toda la lógica de la clase RQO_cls_WrapperTablaObject
* @exception: 
* @throws: 
*/
    
    @isTest
    static void testWrapperTablaObject() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            List<RQO_obj_category__c> listCategory = RQO_cls_TestDataFactory.createCategory(200);
            listCategory[0].Name = 'categoria1';
            listCategory[0].RQO_fld_imagenCategoria__c = 'imagenCategoria' + '.jpg';
            listCategory[0].RQO_fld_sortCategories__c = 1;
            listCategory[0].RQO_fld_type__c = Label.RQO_lbl_productKey;
            update listCategory[0];    
            
            List<RQO_obj_masterSpecification__c> listaMasterSpecification = RQO_cls_TestDataFactory.createMasterSpecification(200);
            listaMasterSpecification[0].Name = 'espec1';
            listaMasterSpecification[0].RQO_fld_unitMeasure__c = 'kg';
            listaMasterSpecification[0].RQO_fld_tipologiaTexto__c = 'Texto';
            update listaMasterSpecification[0];
            listaMasterSpecification[1].Name = 'espec2';
            listaMasterSpecification[1].RQO_fld_unitMeasure__c = '1';
            listaMasterSpecification[1].RQO_fld_tipologiaTexto__c = 'Numero';
            update listaMasterSpecification[1];
            
            List<RQO_obj_qp0Grade__c> listQp0Grade = RQO_cls_TestDataFactory.createQp0Grade(200);
            listQp0Grade[0].name = 'qp0name';
            listQp0Grade[0].RQO_fld_sku__c = 'canal';
            listQp0Grade[0].RQO_fld_descripcionDelGrado__c = 'gradeDescription';
            listQp0Grade[0].RQO_fld_jerarquiaDeProducto__c = 'jerarquiaProd';
            listQp0Grade[0].RQO_fld_sector__c = 'sector';
            listQp0Grade[0].RQO_fld_sociedad__c = 'sociedad';
            update listQp0Grade[0];
            
            List<RQO_obj_grade__c> listGrade = RQO_cls_TestDataFactory.createGrade(200);
            listGrade[0].Name = 'gradeName';
            listGrade[0].RQO_fld_sku__c = listQp0Grade[0].Id;
            listGrade[0].RQO_fld_blocked__c = false;
            listGrade[0].RQO_fld_codeDocumentum__c = 'a';
            listGrade[0].RQO_fld_new__c = true;
            listGrade[0].RQO_fld_private__c = true;
            listGrade[0].RQO_fld_product__c = listCategory[0].Id;
            listGrade[0].RQO_fld_public__c = true;
            listGrade[0].RQO_fld_recommendation__c = true;
            update listGrade[0];
            
            listGrade[1].Name = 'gradeName';
            listGrade[1].RQO_fld_sku__c = listQp0Grade[0].Id;
            listGrade[1].RQO_fld_blocked__c = false;
            listGrade[1].RQO_fld_codeDocumentum__c = 'a';
            listGrade[1].RQO_fld_new__c = true;
            listGrade[1].RQO_fld_private__c = true;
            listGrade[1].RQO_fld_product__c = listCategory[0].Id;
            listGrade[1].RQO_fld_public__c = true;
            listGrade[1].RQO_fld_recommendation__c = true;
            update listGrade[1];
            
            listGrade[2].Name = 'gradeName';
            listGrade[2].RQO_fld_sku__c = listQp0Grade[0].Id;
            listGrade[2].RQO_fld_blocked__c = false;
            listGrade[2].RQO_fld_codeDocumentum__c = 'a';
            listGrade[2].RQO_fld_new__c = true;
            listGrade[2].RQO_fld_private__c = true;
            listGrade[2].RQO_fld_product__c = listCategory[0].Id;
            listGrade[2].RQO_fld_public__c = true;
            listGrade[2].RQO_fld_recommendation__c = true;
            update listGrade[2];
            
            List<RQO_obj_translation__c> listaTranslation = RQO_cls_TestDataFactory.createTranslation(200);
            listaTranslation[0].RQO_fld_locale__c = 'es_ES';
            listaTranslation[0].RQO_fld_category__c = listCategory[0].Id;
            listaTranslation[0].RQO_fld_detailTranslation__c = 'Traduccione';
            listaTranslation[0].RQO_fld_translation__c = 'Traduccionmas';      
            
            List<RQO_obj_catSpecification__c> listaCatSpecification = RQO_cls_TestDataFactory.createCatSpecification(200);
            listaCatSpecification[0].RQO_fld_position__c = 1;
            update listaCatSpecification[0];
            listaCatSpecification[1].RQO_fld_position__c = 2;
            update listaCatSpecification[1];
            
            List<RQO_obj_catSpecification__c> listCategorySpec = new List<RQO_obj_catSpecification__c>();
            listCategorySpec.add(listaCatSpecification[0]); 
            listCategorySpec.add(listaCatSpecification[1]); 
            
            RQO_cls_TestDataFactory.createSpecification(200);                    
            RQO_cls_WrapperTablaObject wrapper = new RQO_cls_WrapperTablaObject();
            test.startTest();
            wrapper.setListaEspecificaciones(listCategorySpec);
            wrapper.setTipoDatoEspecificaciones();
            System.assertEquals(2, wrapper.tipoDatosCategorias.size(), 'No corresponden los Tipos de datos de las especificaciones');
            wrapper.getValoresTabla();
            System.assertEquals(0, wrapper.getValoresTabla().size(), 'No corresponden los valores de los datos');
            wrapper.setNombreCategoria(listCategory[0].Id);
            wrapper.setNombreCategoria(listCategory[0].Name);
            wrapper.putElement(listGrade[0].Id, listaCatSpecification[0].Name, 'mayo'); 
            wrapper.putElement(listGrade[0].Id, listaCatSpecification[1].Name, '37');
            wrapper.putElement(listGrade[1].Id, listaCatSpecification[0].Name, 'marzo'); 
            wrapper.putElement(listGrade[1].Id, listaCatSpecification[1].Name, '45'); 
            wrapper.getElement(listGrade[0].Id, listaCatSpecification[0].Name); 
            wrapper.getElement(listGrade[0].Id, 'error');
            wrapper.setTraduccionesGrados(new Map<Id, RQO_obj_translation__c>());
            wrapper.rellenarHuecos(listGrade);
            System.assertEquals(0, wrapper.traduccionesGrados.size(), 'No corresponden las traducciones de los Grados');
            test.stopTest();
        } 
    }
}