/**
 *	@name: RQO_cls_NotificacionBean
 *	@version: 1.0
 *	@creation date: 18/12/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de Notificaciones.
 */
public class RQO_cls_NotificacionBean implements Comparable{
	
    public String identificador {get; set;}
    public String identificadorRedirect {get; set;} //Identificador auxiliar para las redirecciones por pantalla
    public Date dateRedirect {get; set;} //Fecha auxiliar para las redirecciones por pantalla
    public String tipoObjeto {get; set;}
    public String tipoObjetoKey {get; set;}
    public String literal {get; set;}
    public Boolean isNew {get; set;}
    public DateTime fechaCreacion {get; set;}
    public String auxIconCode {get; set;} //Los icones se muestran en función de la categoría pero en algunos casos dentro de la misma categoría se pueden 
    										//utilizar varios iconos, con la categoría más este este campo solventamos esta situación
    
    /**
    * @creation date: 19/02/2018
    * @author: Alvaro Alonso Trinidad - Unisys
    * @description: implementación del método compareTo para ordenar la lista de objetos
    * @param: compareTo tipo Object
    * @return: Integer
    * @exception: 
    * @throws: 
    */
    public Integer compareTo(Object compareTo) {
        RQO_cls_NotificacionBean compareToEmp = (RQO_cls_NotificacionBean)compareTo;
        if (fechaCreacion == compareToEmp.fechaCreacion) return 0;
        if (fechaCreacion > compareToEmp.fechaCreacion) return -1;
        return 1;
    }
}