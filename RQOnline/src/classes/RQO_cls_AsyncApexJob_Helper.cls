/**
 *	@name: RQO_cls_AsyncApexJob_Helper
 *	@version: 1.0
 *	@creation date: 12/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase para la verificacion del encolamiento de procesos
 */
public class RQO_cls_AsyncApexJob_Helper {
    
    // ***** CONSTANTES ***** //
    public static final Integer MAX_BATCH = 5;
    public static final Integer MAX_JOBS = 50;
    private static final String CLASS_NAME = 'RQO_cls_AsyncApexJob_Helper';
    
    // ***** METODOS PUBLICOS ***** //
    
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtencion de procesos Batch activos. 
	* @param:	
	* @return: Integer	Cantidad de batch activos
	* @exception: 
	* @throws: 
	*/
	public static Integer getActiveBatch(){
    	return [SELECT Count() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status IN('Holding','Queued','Preparing','Processing')];
    }
    
    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Obtencion de Jobs activos. 
	* @param:	
	* @return: Integer	Cantidad de jobs activos
	* @exception: 
	* @throws: 
	*/
    public static Integer getActiveJobs(){
        return [SELECT Count() FROM AsyncApexJob WHERE JobType = 'Queueable' AND Status IN('Holding','Queued','Preparing','Processing')];    
    }

    /**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Nos indica si el Batch en cuestion esta activo. 
	* @param:	nombreClase	- Batch a verificar
	* @return:	Boolean	
	* @exception: 
	* @throws: 
	*/
	public static Boolean isBatchActive(String nombreClase) {
        
        /**
		 * Constantes
		 */
		final String METHOD = 'isBatchActive';
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

		Integer numero = [SELECT count() 
                            FROM AsyncApexJob 
                            WHERE JobType = 'BatchApex' 
                            	AND Status IN('Holding','Queued','Preparing','Processing') 
                           		AND ApexClassId IN (Select Id FROM ApexClass where Name = :nombreClase)];
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');

		return (numero > 0);
	}
}