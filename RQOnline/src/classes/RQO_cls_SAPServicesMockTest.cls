/**
 *	@name: RQO_cls_SAPServicesMockTest
 *	@version: 1.0
 *	@creation date: 18/12/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase Mock para testear los métodos del servicio SOAP de SAP que agrupa varias integraciones contra este sistema
*/
global class RQO_cls_SAPServicesMockTest implements WebServiceMock {
    private String serviceName;
    private String execType;
    private String execErrorNumberCode;
    private String messageV1;
    private String messageV2;
    private String messageV3;
    private String messageV4;
    
    /**
    * @creation date: 18/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Constructor con parámetros de la clase
    * @param: serviceName tipo String, nombre de la operación del servicio que se va a testear. 
    * @param: execType tipo String, tipo de ejecución OK / NOK. 
    * @param: execErrorNumberCode tipo String, número de error para la ejecución erronea. 
    */
    global RQO_cls_SAPServicesMockTest(String serviceName, String execType, String execErrorNumberCode){
        this.serviceName = serviceName;
		this.execType = execType;
        this.execErrorNumberCode = execErrorNumberCode;
    }
	
    /**
    * @creation date: 06/03/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Sobrecarga del constructor con parámetros de la clase para especificar mensajes de error en la gestión genérica de errores
    * @param: serviceName tipo String, nombre de la operación del servicio que se va a testear. 
    * @param: execType tipo String, tipo de ejecución OK / NOK. 
    * @param: execErrorNumberCode tipo String, número de error para la ejecución erronea. 
    * @param: messageV1 tipo String, mensaje de error 1. 
    * @param: messageV2 tipo String, mensaje de error 2. 
    * @param: messageV3 tipo String, mensaje de error 3. 
    * @param: messageV4 tipo String, mensaje de error 4. 
    */
    global RQO_cls_SAPServicesMockTest(String serviceName, String execType, String execErrorNumberCode, String messageV1, String messageV2, String messageV3, String messageV4){
        this.serviceName = serviceName;
		this.execType = execType;
        this.execErrorNumberCode = execErrorNumberCode;
		this.messageV1 = messageV1;
    	this.messageV2 = messageV2;
    	this.messageV3 = messageV3;
    	this.messageV4 = messageV4;
    }
    
	/**
    * @creation date: 18/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Implementación del método doIvoke de la interfaz WebServiceMock
    * @param: stub tipo Object
    * @param: request tipo Object
    * @param: response tipo Map<String, Object>
    * @param: endpoint tipo String
    * @param: soapAction tipo String
    * @param: requestName tipo String
    * @param: responseNS tipo String
    * @param: responseName tipo String
    * @param: responseType tipo String
    * @param: responseName tipo String
    * @return:
    */  
    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
        if (RQO_cls_Constantes.WS_GET_AGREEMENT.equalsIgnoreCase(serviceName)){//Acuerdos
            RQO_cls_SAPServicesWS.ZqoCargaAcuerdosResponse_element customResponse = this.getAcuerdosGetResponse();
            response.put('response_x', customResponse);
		}else if (RQO_cls_Constantes.WS_GET_SEND_ORDER.equalsIgnoreCase(serviceName)){//Envío solicitud
            RQO_cls_SAPServicesWS.ZqoMfGrabarSolSalesforceResponse_element customResponse = this.getSendOrderGetResponse();
            response.put('response_x', customResponse);
		}else if (RQO_cls_Constantes.WS_GET_CONSUMPTION.equalsIgnoreCase(serviceName)){//Obtención de consumos
            RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element customResponse = this.getConsumosResponse();
            response.put('response_x', customResponse);
		}else if (RQO_cls_Constantes.WS_GET_INVOICES.equalsIgnoreCase(serviceName)){//Obtención facturas
            RQO_cls_SAPServicesWS.ZqoCargaFacturasResponse_element customResponse = this.getFacturasResponse();
            response.put('response_x', customResponse);
		}else if (RQO_cls_Constantes.WS_GET_INVOICE_DOC.equalsIgnoreCase(serviceName)){//Descarga factura
           	RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element customResponse = this.getFacturaDoc();
            response.put('response_x', customResponse);
		}else if (RQO_cls_Constantes.WS_GET_ALBARAN_DOC.equalsIgnoreCase(serviceName)){//Descarga albaran
            RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element customResponse = this.getAlbaranDocResponse();
            response.put('response_x', customResponse);
		}else if (RQO_cls_Constantes.WS_GET_CERTIFICADO_ANALISIS_DOC.equalsIgnoreCase(serviceName)){//Descarga certificado analisis
            RQO_cls_SAPServicesWS.ZqoMfCertificadoAnalisisResponse_element customResponse = this.getCertificadoAnalisisDocResponse();
            response.put('response_x', customResponse);
		}

    }
    
	/**
    * @creation date: 18/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Generamos respuesta del servicio de acuerdos
    * @param:
    * @return: RQO_cls_SAPServicesWS.ZqoCargaAcuerdosResponse_element
    */  
    private RQO_cls_SAPServicesWS.ZqoCargaAcuerdosResponse_element getAcuerdosGetResponse(){
        RQO_cls_SAPServicesWS.ZqoCargaAcuerdosResponse_element responseElement = new RQO_cls_SAPServicesWS.ZqoCargaAcuerdosResponse_element();
        if (RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, this.execErrorNumberCode, 'Error');
          }else{
            RQO_cls_SAPServicesWS.ZqoTtGacuerdos acuerdos = new RQO_cls_SAPServicesWS.ZqoTtGacuerdos();
            List<RQO_cls_SAPServicesWS.ZqoStGacuerdos> item = new List<RQO_cls_SAPServicesWS.ZqoStGacuerdos>();
            RQO_cls_SAPServicesWS.ZqoStGacuerdos objItem = new RQO_cls_SAPServicesWS.ZqoStGacuerdos();
            objItem.Guid = '';
            objItem.GradoSap = 'test0';
            objItem.DenomGrado = '';
            objItem.Prodh = '';
            objItem.Organizacion = '';
            objItem.Canal = '';
            objItem.Sector = '';
            objItem.Sociedad = '';
            objItem.Envases = '';
            objItem.Comercializable = '';
            item.add(objItem);
            Acuerdos.Item = item;
            responseElement.EtAcuerdos = acuerdos;
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_OK, null, 'Success');
		}
        return responseElement;
    }
    
    /**
    * @creation date: 18/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Generamos respuesta del servicio de envío de solicitud
    * @param:
    * @return: RQO_cls_SendOrderWS.ZqoMfGrabarSolSalesforceResponse_element
    */  
    private RQO_cls_SAPServicesWS.ZqoMfGrabarSolSalesforceResponse_element getSendOrderGetResponse(){
        RQO_cls_SAPServicesWS.ZqoMfGrabarSolSalesforceResponse_element responseElement = new RQO_cls_SAPServicesWS.ZqoMfGrabarSolSalesforceResponse_element();
        if (RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, this.execErrorNumberCode, 'Error');
        }else{
			responseElement.EvError = 'Test';            
			responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_OK, null, 'OK $1 $2 $3 $4');
        }
        return responseElement;
    }
    
    
	/**
    * @creation date: 26/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Generamos respuesta del servicio de recuperación de consumos
    * @param:
    * @return: RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element
    */  
    private RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element getConsumosResponse(){
        RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element responseElement = new RQO_cls_SAPServicesWS.ZqoCargaConsumosResponse_element();
        if (RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, this.execErrorNumberCode, 'Error');
        }else{
            RQO_cls_SAPServicesWS.ZqoTtConsumos consumos = new RQO_cls_SAPServicesWS.ZqoTtConsumos();
            List<RQO_cls_SAPServicesWS.ZqoStConsumos> item = new List<RQO_cls_SAPServicesWS.ZqoStConsumos>();
            RQO_cls_SAPServicesWS.ZqoStConsumos objItem = new RQO_cls_SAPServicesWS.ZqoStConsumos();
            objItem.Material = null;
            objItem.UnidadNegocio = null;
            objItem.Familia = 'ESTIRENO';
            objItem.Ejercicio = '2017';
            objItem.Periodo = 'Enero';
            objItem.Cantidad = '100';
            objItem.Unidad = '1';
            objItem.Importe = '100';
            objItem.Moneda = 'EUR';
            item.add(objItem);
            //Acumula al existente
            objItem = new RQO_cls_SAPServicesWS.ZqoStConsumos();
            objItem.Material = null;
            objItem.UnidadNegocio = null;
            objItem.Familia = 'ESTIRENO';
            objItem.Ejercicio = '2017';
            objItem.Periodo = 'Enero';
            objItem.Cantidad = '100';
            objItem.Unidad = '1';
            objItem.Importe = '100';
            objItem.Moneda = 'EUR';
            //Nuevo mes
			objItem = new RQO_cls_SAPServicesWS.ZqoStConsumos();
            objItem.Material = null;
            objItem.UnidadNegocio = null;
            objItem.Familia = 'ESTIRENO';
            objItem.Ejercicio = '2017';
            objItem.Periodo = 'Febrero';
            objItem.Cantidad = '100';
            objItem.Unidad = '1';
            objItem.Importe = '100';
            objItem.Moneda = 'EUR';
            item.add(objItem);
            
			//Familia nueva
            objItem = new RQO_cls_SAPServicesWS.ZqoStConsumos();
            objItem.Material = null;
            objItem.UnidadNegocio = null;
            objItem.Familia = 'FERTILIZANTES';
            objItem.Ejercicio = '2017';
            objItem.Periodo = 'Enero';
            objItem.Cantidad = '100';
            objItem.Unidad = '1';
            objItem.Importe = '100';
            objItem.Moneda = 'EUR';
            item.add(objItem);
			//Meses no consecutivos
			objItem = new RQO_cls_SAPServicesWS.ZqoStConsumos();
            objItem.Material = null;
            objItem.UnidadNegocio = null;
            objItem.Familia = 'FERTILIZANTES';
            objItem.Ejercicio = '2016';
            objItem.Periodo = 'Marzo';
            objItem.Cantidad = '100';
            objItem.Unidad = '1';
            objItem.Importe = '100';
            objItem.Moneda = 'EUR';
            item.add(objItem);
			//Acumulamos en años diferentes
            objItem = new RQO_cls_SAPServicesWS.ZqoStConsumos();
            objItem.Material = null;
            objItem.UnidadNegocio = null;
            objItem.Familia = 'FERTILIZANTES';
            objItem.Ejercicio = '2016';
            objItem.Periodo = 'Enero';
            objItem.Cantidad = '100';
            objItem.Unidad = '1';
            objItem.Importe = '100';
            objItem.Moneda = 'EUR';
            item.add(objItem);
			objItem = new RQO_cls_SAPServicesWS.ZqoStConsumos();
            objItem.Material = null;
            objItem.UnidadNegocio = null;
            objItem.Familia = 'FERTILIZANTES';
            objItem.Ejercicio = '2016';
            objItem.Periodo = 'Febrero';
            objItem.Cantidad = '100';
            objItem.Unidad = '1';
            objItem.Importe = '100';
            objItem.Moneda = 'EUR';
            item.add(objItem);
            //Generamos dato para unos meses después
			objItem = new RQO_cls_SAPServicesWS.ZqoStConsumos();
            objItem.Material = null;
            objItem.UnidadNegocio = null;
            objItem.Familia = 'ESTIRENO';
            objItem.Ejercicio = '2017';
            objItem.Periodo = 'Junio';
            objItem.Cantidad = '100';
            objItem.Unidad = '1';
            objItem.Importe = '100';
            objItem.Moneda = 'EUR';
            item.add(objItem);
            
            if (RQO_cls_Constantes.MOCK_EXEC_EXCEPTION.equalsIgnoreCase(this.execType)){
                objItem = new RQO_cls_SAPServicesWS.ZqoStConsumos();
                objItem.Material = null;
                objItem.UnidadNegocio = null;
                objItem.Familia = 'ESTIRENO';
                objItem.Ejercicio = '2017';
                objItem.Periodo = 'Febrero';
                objItem.Cantidad = 'TEST';
                objItem.Unidad = 'TEST';
                objItem.Importe = 'TEST';
                objItem.Moneda = 'EUR';
                item.add(objItem);
            }
            
            consumos.Item = item;
            responseElement.EtConsumos = consumos;
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_OK, null, 'Success');
        }
        return responseElement;
    } 
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Generamos respuesta del servicio de recuperación de facturas
    * @param:
    * @return: RQO_cls_SAPServicesWS.ZqoCargaFacturasResponse_element
    */  
    private RQO_cls_SAPServicesWS.ZqoCargaFacturasResponse_element getFacturasResponse(){
        RQO_cls_SAPServicesWS.ZqoCargaFacturasResponse_element responseElement = new RQO_cls_SAPServicesWS.ZqoCargaFacturasResponse_element();
        if (RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, this.execErrorNumberCode, 'Error');
        }else{
            RQO_cls_SAPServicesWS.ZqoTtFacturaso facturas = new RQO_cls_SAPServicesWS.ZqoTtFacturaso();
            List<RQO_cls_SAPServicesWS.ZqoStFacturaso> item = new List<RQO_cls_SAPServicesWS.ZqoStFacturaso>();
            RQO_cls_SAPServicesWS.ZqoStFacturaso objItem = new RQO_cls_SAPServicesWS.ZqoStFacturaso();
            objItem.Factura = '';
            objItem.PosFactura = '';
            objItem.Entrega = '';
            objItem.PosEntrega = '';
            objItem.Pedido = '';
            objItem.PedidoRefCliente = '';
            objItem.DocfactContable = '';
            objItem.DestMercancias = 'test0';
            objItem.DestFacturacion = '';
            objItem.IndVat = '';
            objItem.Solicitante = '';
            objItem.Fecfactura = '';
            objItem.FechaVenci = '';
            objItem.Clasefact = '';
            objItem.TipoFact = '';
            objItem.StatusFact = '';
            objItem.ImporteBruto = '';
            objItem.ImporteTotal = '';
            objItem.Moneda = '';
            objItem.Organizacion = '';
            objItem.Canal = '';
            objItem.Sector = '';
            objItem.Sociedad = '';
            objItem.IdArchivado = '';
            objItem.Codmaterial = 'test0';
            objItem.Cantidad = '';
            objItem.Umctd = '';
            objItem.ImportNetoPos = '';
            objItem.Impdescpos = '';
            objItem.PrecioUnitario = '';
            item.add(objItem);
            facturas.Item = item;
            responseElement.EtFacturas = facturas;
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_OK, null, 'Success');
        }
        return responseElement;
    }

	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Generamos respuesta del servicio de recuperación de documento de facturas
    * @param:
    * @return: RQO_cls_SAPServicesWS.ZqoCargaFacturasResponse_element
    */  
    private RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element getFacturaDoc(){
		RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element responseElement = new RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element();
        if (RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, this.execErrorNumberCode, 'Error');
        }else{
			responseElement.EvGuidDoc = 'test';
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_OK, null, 'Success');
        }
        if (RQO_cls_Constantes.MOCK_EXEC_EXCEPTION.equalsIgnoreCase(this.execType)){
            responseElement = null;
        }
        return responseElement;
    }

	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Generamos respuesta del servicio de recuperación de documento de albaran
    * @param:
    * @return: RQO_cls_SAPServicesWS.ZqoCargaFacturasResponse_element
    */  
    private RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element getAlbaranDocResponse(){
		RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element responseElement = new RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element();
        if (RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, this.execErrorNumberCode, 'Error');
        }else{
			RQO_cls_SAPServicesWS.ZqoItablaio tablaIo = new RQO_cls_SAPServicesWS.ZqoItablaio();
			List<RQO_cls_SAPServicesWS.Ytcar255> listItem = new List<RQO_cls_SAPServicesWS.Ytcar255>();
            RQO_cls_SAPServicesWS.Ytcar255 obj255 = new RQO_cls_SAPServicesWS.Ytcar255();
            obj255.Ycar255 = 'Test';
            listItem.add(obj255);
            tablaIo.item = listItem;
            responseElement.ItablaIo = tablaIo;
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_OK, null, 'Success');
        }
        if (RQO_cls_Constantes.MOCK_EXEC_EXCEPTION.equalsIgnoreCase(this.execType)){
            responseElement = null;
        }
        return responseElement;
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Generamos respuesta del servicio de recuperación de documento de certificado de analisis
    * @param:
    * @return: RQO_cls_SAPServicesWS.ZqoCargaFacturasResponse_element
    */  
    private RQO_cls_SAPServicesWS.ZqoMfCertificadoAnalisisResponse_element getCertificadoAnalisisDocResponse(){
		RQO_cls_SAPServicesWS.ZqoMfCertificadoAnalisisResponse_element responseElement = new RQO_cls_SAPServicesWS.ZqoMfCertificadoAnalisisResponse_element();
        if (RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR, this.execErrorNumberCode, 'Error');
        }else{
			RQO_cls_SAPServicesWS.ZqoItablaio tablaIo = new RQO_cls_SAPServicesWS.ZqoItablaio();
			List<RQO_cls_SAPServicesWS.Ytcar255> listItem = new List<RQO_cls_SAPServicesWS.Ytcar255>();
            RQO_cls_SAPServicesWS.Ytcar255 obj255 = new RQO_cls_SAPServicesWS.Ytcar255();
            obj255.Ycar255 = 'Test';
            listItem.add(obj255);
            tablaIo.item = listItem;
            responseElement.ItablaIo = tablaIo;
            responseElement.EtReturn = this.getGenericResponse(RQO_cls_Constantes.REQ_WS_RESPONSE_OK, null, 'Success');
        }
		if (RQO_cls_Constantes.MOCK_EXEC_EXCEPTION.equalsIgnoreCase(this.execType)){
            responseElement = null;
        }
        
        return responseElement;
    }
    
	/**
    * @creation date: 18/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Genera la parte de respuesta que es genérica para todos los servicios de SAP
    * @param:
    * @return: RQO_cls_SAPServicesWS.Bapiret2T
    */
    private RQO_cls_SAPServicesWS.Bapiret2T getGenericResponse(String responseExecCode, String responseNumberCode, String messagge){
        RQO_cls_SAPServicesWS.Bapiret2T etReturn = new RQO_cls_SAPServicesWS.Bapiret2T();
        RQO_cls_SAPServicesWS.Bapiret2[] item = new List<RQO_cls_SAPServicesWS.Bapiret2>();
        RQO_cls_SAPServicesWS.Bapiret2 objResponse = new RQO_cls_SAPServicesWS.Bapiret2();
        objResponse.Type_x = responseExecCode;
        objResponse.Number_x = responseNumberCode;
        objResponse.Message = messagge;
        objResponse.MessageV1 = (String.isNotBlank(this.messageV1)) ? messageV1 : 'test1';
        objResponse.MessageV2 = (String.isNotBlank(this.messageV2)) ? messageV2 : 'test2';
        objResponse.MessageV3 = (String.isNotBlank(this.messageV3)) ? messageV3 : 'test3';
        objResponse.MessageV4 = (String.isNotBlank(this.messageV4)) ? messageV4 : 'test4';
        item.add(objResponse);
        etReturn.item = item;
        return etReturn;
    }
}