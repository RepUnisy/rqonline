/*------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    Clase controladora de la visualforce page que muestra el catálogo de todos los MASTER RISK
Test Class:     ITPM_Controller_Manage_risk_test
History
<Date>          <Author>        <Change Description>
08-Ago-2016     Rubén Simarro   Initial Version
------------------------------------------------------------*/
global with sharing class ITPM_Controller_Manage_risk {
       
    public Id  matrixRiskId{get; set;}
    
    public List<ITPM_Matrix_risk_row__c> lstFilasMatriz{get; set;} 
    
    public String PSC1name{get; set;} 
    public String PSC2name{get; set;} 
    public String PSC3name{get; set;} 
    public String PSC4name{get; set;} 
    public String PSC5name{get; set;} 
    public String PSC6name{get; set;} 
    public String PSC7name{get; set;} 
    public String PSC8name{get; set;} 
    public String PSC9name{get; set;} 
    public String PSC10name{get; set;} 
    
    public String PSC1description{get; set;} 
    public String PSC2description{get; set;} 
    public String PSC3description{get; set;} 
    public String PSC4description{get; set;} 
    public String PSC5description{get; set;} 
    public String PSC6description{get; set;} 
    public String PSC7description{get; set;} 
    public String PSC8description{get; set;} 
    public String PSC9description{get; set;} 
    public String PSC10description{get; set;} 
    
    public boolean PSC1exist{get; set;} 
    public boolean PSC2exist{get; set;} 
    public boolean PSC3exist{get; set;} 
    public boolean PSC4exist{get; set;} 
    public boolean PSC5exist{get; set;} 
    public boolean PSC6exist{get; set;} 
    public boolean PSC7exist{get; set;} 
    public boolean PSC8exist{get; set;} 
    public boolean PSC9exist{get; set;} 
    public boolean PSC10exist{get; set;} 
       
    public ITPM_Controller_Manage_risk(ApexPages.StandardController controller){
        
           matrixRiskId = ApexPages.currentPage().getParameters().get('Id');
        
          lstFilasMatriz = [SELECT ITPM_Master_risk__r.Id, 
                            ITPM_Master_risk__r.Name,
                            ITPM_Master_risk__r.ITPM_Risk_Name__c,
                            ITPM_CEP1__c, 
                            ITPM_CEP2__c, 
                            ITPM_CEP3__c, 
                            ITPM_CEP4__c, 
                            ITPM_CEP5__c,
                            ITPM_CEP6__c, 
                            ITPM_CEP7__c,
                            ITPM_CEP8__c,
                            ITPM_CEP9__c,
                            ITPM_CEP10__c,
                            ITPM_CEP1__r.ITPM_PER_Weighing__c,
                            ITPM_CEP2__r.ITPM_PER_Weighing__c,
                            ITPM_CEP3__r.ITPM_PER_Weighing__c,
                            ITPM_CEP4__r.ITPM_PER_Weighing__c,
                            ITPM_CEP5__r.ITPM_PER_Weighing__c,
                            ITPM_CEP6__r.ITPM_PER_Weighing__c,
                            ITPM_CEP7__r.ITPM_PER_Weighing__c,   
                            ITPM_CEP8__r.ITPM_PER_Weighing__c,   
                            ITPM_CEP9__r.ITPM_PER_Weighing__c,   
                            ITPM_CEP10__r.ITPM_PER_Weighing__c,   
                            ITPM_value_CEP1__c,
                            ITPM_value_CEP2__c,
                            ITPM_value_CEP3__c,
                            ITPM_value_CEP4__c,
                            ITPM_value_CEP5__c,
                            ITPM_value_CEP6__c,
                            ITPM_value_CEP7__c,
                            ITPM_value_CEP8__c,
                            ITPM_value_CEP9__c,
                            ITPM_value_CEP10__c,
                            ITPM_FOR_Impact__c,
                            ITPM_Probability__c,
                            ITPM_FOR_Risk_calculated__c,
                            ITPM_PSC1_selected__c,
                            ITPM_PSC2_selected__c,
                            ITPM_PSC3_selected__c,
                            ITPM_PSC4_selected__c,
                            ITPM_PSC5_selected__c,
                            ITPM_PSC6_selected__c,
                            ITPM_PSC7_selected__c,
                            ITPM_PSC8_selected__c,
                            ITPM_PSC9_selected__c,
                            ITPM_PSC10_selected__c
                            FROM 
                            ITPM_Matrix_risk_row__c where ITPM_Matrix_Risk__c=: ApexPages.currentPage().getParameters().get('id') 
                            ORDER BY ITPM_Master_risk__r.Name ASC];
     

        for(ITPM_Project_Success_Criteria__c PSC : [select Id, ITPM_TX_Criterion_name__c, ITPM_TX_Criterion_description__c, ITPM_PER_Weighing__c, ITPM_SEL_Matrix_column_position__c 
                                                    from ITPM_Project_Success_Criteria__c
                                                    where ITPM_REL_Project__c =:[SELECT ITPM_Matrix_risk_REL_Project__c FROM ITPM_Matrix_Risk__c WHERE ID =: ApexPages.currentPage().getParameters().get('id')].ITPM_Matrix_risk_REL_Project__c 
                                                   AND CreatedDate <=:[SELECT CreatedDate FROM ITPM_Matrix_Risk__c WHERE ID =: ApexPages.currentPage().getParameters().get('id')].CreatedDate ] ){
                      
                                                       if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC1')){    
                               PSC1name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';         
                              PSC1description= PSC.ITPM_TX_Criterion_description__c;
                              PSC1exist = true;
                           }  
                           else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC2')){    
                               PSC2name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';             
                               PSC2description= PSC.ITPM_TX_Criterion_description__c;
                                PSC2exist = true;
                          }  
                           else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC3')){    
                               PSC3name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                              
                               PSC3description= PSC.ITPM_TX_Criterion_description__c;
                                PSC3exist = true;
                           }  
                           else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC4')){    
                               PSC4name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                   
                               PSC4description= PSC.ITPM_TX_Criterion_description__c;
                               PSC4exist = true;
                           }  
                           else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC5')){    
                               PSC5name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                        
                               PSC5description= PSC.ITPM_TX_Criterion_description__c;
                               PSC5exist = true;
                           }  
                           else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC6')){  
                               PSC6name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';               
                               PSC6description= PSC.ITPM_TX_Criterion_description__c;
                               PSC6exist = true;
                           }  
                           else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC7')){    
                               PSC7name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                          
                               PSC7description= PSC.ITPM_TX_Criterion_description__c;
                               PSC7exist = true;
                           } 
                           else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC8')){    
                               PSC8name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                     
                               PSC8description= PSC.ITPM_TX_Criterion_description__c;
                               PSC8exist = true;
                           }  
                           else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC9')){    
                               PSC9name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';               
                               PSC9description= PSC.ITPM_TX_Criterion_description__c;
                               PSC9exist = true;
                           }  
                           else if(PSC.ITPM_SEL_Matrix_column_position__c.equals('PSC10')){    
                               PSC10name= PSC.ITPM_TX_Criterion_name__c + ' ('+PSC.ITPM_PER_Weighing__c+' %) ';                    
                               PSC10description= PSC.ITPM_TX_Criterion_description__c;
                               PSC10exist = true;
                           }  
            
         }  
             

    }
          
    public List<ITPM_Matrix_risk_row__c> getlstFilasMatriz(){
        return lstFilasMatriz;
    }
    
      public void saveManageRisk(){
     
        try{       
            
            for(ITPM_Matrix_risk_row__c listAux : lstFilasMatriz){
                   
                if(!listAux.ITPM_PSC1_selected__c){
                    listAux.ITPM_value_CEP1__c=null;
                }   
                if(!listAux.ITPM_PSC2_selected__c){
                    listAux.ITPM_value_CEP2__c=null;
                }   
                if(!listAux.ITPM_PSC3_selected__c){
                    listAux.ITPM_value_CEP3__c=null;
                } 
                if(!listAux.ITPM_PSC4_selected__c){
                    listAux.ITPM_value_CEP4__c=null;
                } 
                if(!listAux.ITPM_PSC5_selected__c){
                    listAux.ITPM_value_CEP5__c=null;
                }  
                if(!listAux.ITPM_PSC6_selected__c){
                    listAux.ITPM_value_CEP6__c=null;
                }   
                if(!listAux.ITPM_PSC7_selected__c){
                    listAux.ITPM_value_CEP7__c=null;
                }  
                if(!listAux.ITPM_PSC8_selected__c){
                    listAux.ITPM_value_CEP8__c=null;
                }  
                if(!listAux.ITPM_PSC9_selected__c){
                    listAux.ITPM_value_CEP9__c=null;
                }   
                if(!listAux.ITPM_PSC10_selected__c){
                    listAux.ITPM_value_CEP10__c=null;
                } 

                if(listAux.ITPM_PSC1_selected__c || 
                   listAux.ITPM_PSC2_selected__c ||
                   listAux.ITPM_PSC3_selected__c ||
                   listAux.ITPM_PSC4_selected__c ||
                   listAux.ITPM_PSC5_selected__c ||
                   listAux.ITPM_PSC6_selected__c ||
                   listAux.ITPM_PSC7_selected__c ||
                   listAux.ITPM_PSC8_selected__c ||
                   listAux.ITPM_PSC9_selected__c ||
                   listAux.ITPM_PSC10_selected__c){
                   
                       listAux.ITPM_Apply_risk__c = true;       
                }  
                else{           
                    listAux.ITPM_Apply_risk__c = false;
                }
            }
            
            
            update lstFilasMatriz;
           
            
            system.debug('@@@ ITPM_Controller_Manage_risk saveManageRisk()');
        }
        catch (Exception ex) {                                 
            system.debug('@@@ ITPM_Controller_Manage_risk saveManageRisk() ERROR: '+ex.getMessage());
        }  
    }
    
}