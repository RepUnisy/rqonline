global class QUIM_REP_hk_UserInterface extends ccrz.cc_hk_UserInterface{
    
    global override String metaContent(){
        
        StaticResource static_resource = [SELECT Id, SystemModStamp, Name
                                          FROM StaticResource 
                                          WHERE Name = 'QUIM_img_favicon'
                                          LIMIT 1];
        
        String url_file_ref  = 'https://'+URL.getSalesforceBaseUrl().getHost();
        if((URL.getCurrentRequestUrl().toExternalForm()).contains('PortalQuimica')){
            url_file_ref +=  '/PortalQuimica';
        }
            url_file_ref = url_file_ref + '/resource/' +String.valueOf(((DateTime)static_resource.get('SystemModStamp')).getTime())
            + '/' 
            + static_resource.get('Name');
        
        return '<link rel="shortcut icon" href="'+url_file_ref+'" type="image/x-icon"/>' +
             '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />\n'+
             '<meta name="robots" content="index, follow">\n';
    }


}