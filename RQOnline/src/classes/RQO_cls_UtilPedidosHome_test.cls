/**
*	@name: RQO_cls_UtilPedidosHome_test
*	@version: 1.0
*	@creation date: 27/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar los métodos asociados a UtilPedidosHome
*/
@IsTest
public class RQO_cls_UtilPedidosHome_test {
    
	/**
    *	@name: RQO_cls_UtilPedidosHome_test
    *	@version: 1.0
    *	@creation date: 27/2/2018
    *	@author: Juan Elías - Unisys
    *	@description: Clase que genera el conjunto de datos inicial necesario para la ejecución del test
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        
    }
    
    /**
	* @creation date: 27/02/2018
    * @author: Juan Elías - Unisys
    * @description: Método para probar el contador de registros por estado que aparece en la home del portal de química
	* @exception: 
	* @throws: 
	*/
    
    @isTest
    static void testUploadDocToRepository(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String esperado;
            List<Contact> contactos = RQO_cls_TestDataFactory.createContact(200);
            
            Id clientId = contactos[0].Id;
            RQO_cls_UtilPedidosHome utilPedidosHome = new RQO_cls_UtilPedidosHome();
            test.startTest();
            String resultado = utilPedidosHome.getCountPedidos(clientId);
            System.assertNotEquals(esperado, resultado);
            test.stopTest();
        }
    }
}