/**
*   @name: RQO_cls_UtilPedidosHome
*   @version: 1.0
*   @creation date: 29/01/2017
*   @author: Alvaro Alonso - Unisys
*   @description: Clase Apex de utilidades para recuperar los datos de pedido que se muestran en la home
*	@testClass: 
*/
public class RQO_cls_UtilPedidosHome {

    private static final String CLASS_NAME = 'RQO_cls_UtilPedidosHome';

	/**
    * @creation date: 29/01/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método para recuperar el contador de registros por estado que aparece en la home del portal de química
    * @param: clientId tipo ID
    * @return: String con el objeto RQO_cls_ContadorPedidosBean serializado
    * @exception: 
    * @throws: 
    */
    public String getCountPedidos(Id clientId){
        final String METHOD = 'getCountPedidos';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
       
        String response = '';
        Id positionRecordtypeId = null;
        //Instanciamos el objeto e inicialmente rellenamos los datos de ejecución a error
        RQO_cls_ContadorPedidosBean objCount = new RQO_cls_ContadorPedidosBean();
        objCount.executionCode = RQO_cls_Constantes.COD_EXEC_NOK;
        objCount.executionMessage = RQO_cls_Constantes.COD_EXEC_NOK;
        if (clientId != null){
            try{
                positionRecordtypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get(RQO_cls_Constantes.RTID_ASSET_ORDER_POSITION).getRecordTypeId();
                //Contador de posiciones de pedido en estado registrado
                objCount.contadorRegistrado = this.getCountOrder(clientId, RQO_cls_Constantes.PED_COD_STATUS_REGISTRADO, positionRecordtypeId);
                //Contador de posiciones de pedido en estado en curso
                objCount.contadorEnCurso = this.getCountOrder(clientId, RQO_cls_Constantes.PED_COD_STATUS_EN_TRATAMIENTO, positionRecordtypeId);
                //Contador de posiciones de entrega en estado en confirmado
                objCount.contadorConfirmado = this.getCountOrderDelivery(clientId, RQO_cls_Constantes.PED_COD_STATUS_CONFIRMADO);
                //Contador de posiciones de entrega en estado en transito
                objCount.contadorEnTransito = this.getCountOrderDelivery(clientId, RQO_cls_Constantes.PED_COD_STATUS_ENTRANSITO);
                objCount.executionCode = RQO_cls_Constantes.COD_EXEC_OK;
                objCount.executionMessage = RQO_cls_Constantes.COD_EXEC_OK;
            }catch(Exception ex){
                //Capturamos la excepción y rellenamos el objeto con el error para que no falle la ventana al completo y podamos mostrar en nuestro componente
                //el error concreto de ser necesario.
                System.debug('Excepción: ' + ex.getStackTraceString());
                objCount.executionMessage = ex.getMessage();
            }
        }
        //Serializamos el objeto
        response = JSON.serialize(objCount);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return response;
    }
	
    /**
    * @creation date: 29/01/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera el número de registros que cumplen las condiciones que llegan por parámetro sobre el objeto asset 
    * @param: clientId tipo ID
    * @param: status tipo String
    * @param: recordtypeId tipo ID
    * @return: String
    * @exception: 
    * @throws: 
    */
    private Integer getCountOrder(Id clientId, String status, Id recordtypeId){
        final String METHOD = 'getCountOrder';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String query = this.getAssetCountQuery(clientId, status, recordtypeId);
        Integer responseData = database.countQuery(query);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return responseData;
    }
    
    /**
    * @creation date: 29/01/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera el número de registros que cumplen las condiciones que llegan por parámetro sobre el objeto asset 
    * @param: clientId tipo ID
    * @return: String
    * @exception: 
    * @throws: 
    */
    private Integer getCountOrderDelivery(Id clientId, String status){
		final String METHOD = 'getCountOrderDelivery';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String query = this.getDeliveryCountQuery(clientId, status);
        Integer responseData = database.countQuery(query);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return responseData;
    }
    
	/**
    * @creation date: 29/01/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera el string con la query que se va a realizar sobre asset
    * @param: clientId tipo ID
    * @param: status tipo String
    * @param: recordtypeId tipo ID
    * @return: String
    * @exception: 
    * @throws: 
    */
    private String getAssetCountQuery(Id clientId, String status, Id recordtypeId){
        final String METHOD = 'getAssetCountQuery';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String query = 'Select count() from Asset ';
        String strWhere = '';
        if (String.isNotEmpty(recordtypeId)){
            strWhere += ' where RecordtypeId = \'' + String.escapeSingleQuotes(String.valueOf(recordtypeId)) + '\'';
        }
        if (String.isNotEmpty(clientId)){
            if (String.isEmpty(strWhere)){strWhere += ' where ';}else{strWhere += ' and ';}
            strWhere += '  AccountId = \'' + String.escapeSingleQuotes(String.valueOf(clientId)) + '\'';
        }
        //Solo se recuperan posiciones de pedido que no tengan posiciones de entrega asociadas. 
        //El campo RQO_fld_deliveryChildsNumber__c es un rollup que verifica el número de hijos (posiciones de entrega) que tiene asociadas la posición.
        if (String.isEmpty(strWhere)){strWhere += ' where ';}else{strWhere += ' and ';}
        strWhere += ' RQO_fld_deliveryChildsNumber__c = 0 ';
            
        if (String.isNotEmpty(status)){
            if (String.isEmpty(strWhere)){strWhere += ' where ';}else{strWhere += ' and ';}
            strWhere += '  RQO_fld_positionStatus__c = \'' + String.escapeSingleQuotes(status) + '\'';
        }
        query += strWhere;
        return query;
    }
    
	/**
    * @creation date: 29/01/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera el string con la query que se va a realizar sobre RQO_obj_posiciondeEntrega__c (posiciones de entrega)
    * @param: clientId tipo ID
    * @param: status tipo String
    * @return: String
    * @exception: 
    * @throws: 
    */
    private String getDeliveryCountQuery(Id clientId, String status){
        final String METHOD = 'getDeliveryCountQuery';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String query = 'Select count() from RQO_obj_posiciondeEntrega__c ';
        String strWhere = '';
        if (String.isNotEmpty(clientId)){
            strWhere = ' where RQO_fld_posicionPedido__r.AccountId = \'' + String.escapeSingleQuotes(String.valueOf(clientId)) + '\'';
        }
        //Solo las que tiene posición de pedido asociada. Deberían ser todas
        if (String.isEmpty(strWhere)){strWhere += ' where ';}else{strWhere += ' and ';}
        strWhere += ' RQO_fld_posicionPedido__c <> \'\' ';
        
        if (String.isNotEmpty(status)){
            if (String.isEmpty(strWhere)){strWhere += ' where ';}else{strWhere += ' and ';}
            strWhere += '  RQO_fld_idRelacion__r.RQO_fld_estadodelaEntrega__c = \'' + String.escapeSingleQuotes(status) + '\'';
        }
        query += strWhere;
        return query;
    }
}