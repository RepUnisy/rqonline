/**
 *	@name: RQO_cls_PedidoBean
 *	@version: 1.0
 *	@creation date: 16/10/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de Pedidos.
 */
public class RQO_cls_PedidoBean {

    public String externalId {get; set;}
    public String tipoPedido {get; set;}
    public String solicitante {get; set;}
    public String moneda {get; set;}
    public String incoterm {get; set;}
    public String paisReceptorFiscal {get; set;}
    public String fechaPreferenteEntrega {get; set;}
    public String fechaGrabacionRegistro {get; set;}
    public String interlocutorWE {get; set;}
    public String interlocutorRE {get; set;}
    public String interlocutorSB {get; set;}
    public String interlocutorZA {get; set;}
    public String interlocutorZC {get; set;}
    public String numPedidoCliente {get; set;}
    public String isDelete {get; set;}
    public List <RQO_cls_PosicionPedidoBean> posicionePedidosList {get; set;}
    
}