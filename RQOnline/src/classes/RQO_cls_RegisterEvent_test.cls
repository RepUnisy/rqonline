/**
*	@name: RQO_cls_RegisterEvent_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase RQO_cls_RegisterEvent_cc
*/
@IsTest
public class RQO_cls_RegisterEvent_test {
    
    /**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
		
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserEvents@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            List<Contact> contacto = RQO_cls_TestDataFactory.createContact(1);
            User usuarioPortal = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserEventsPortal@testorg.com', 'Customer Community User', null,
                                                                            'es', 'es_ES', 'Europe/Berlin', contacto[0].ID);
            
        }       
    }
    
    
	/**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para comprobar el método createEvent
    * @exception: 
    * @throws:  
    */
    @isTest
    static void createEvent(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserEventsPortal@testorg.com');
        System.runAs(u){
            test.startTest();
            	RQO_cls_RegisterEvent_cc.createEvent('1', '1', '1', '1', null, null, null, null, null, null, null, null, null, null, null, null);
            test.stopTest();
            List<RQO_obj_event__c> listaObj = [Select Id from RQO_obj_event__c LIMIT 1];
            System.assert(listaObj != null);
            System.assert(!listaObj.isEmpty());
            System.assert(listaObj.size()>0);
        }
    }    
}