/**
*	@name: RQO_cls_CancelRequestByDateSch_test
*	@version: 1.0
*	@creation date: 06/03/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar el batch RQO_cls_CancelRequestByDate_sch
*/
@IsTest
public class RQO_cls_CancelRequestByDateSch_test {

    /**
    * @creation date: 06/03/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserCancelRequestBatch@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSprocesoPlanificado(RQO_cls_Constantes.SCHEDULE_CODE_CANCEL_REQUEST);
        }
    }
    
	/**
	* @creation date: 06/03/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar la ejecución de la clase programable
	* @exception: 
	* @throws: 
	*/
    static testMethod void ejecucionProgramada(){
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserCancelRequestBatch@testorg.com');
        System.runAs(u){
            String CRON_EXP = '0 0 0 15 3 ? 2022';
      		Test.startTest();
				RQO_cls_CancelRequestByDate_sch ccScheduler = new RQO_cls_CancelRequestByDate_sch();
            	system.schedule('test', CRON_EXP, ccScheduler);
            	ccScheduler.lanzarProceso();
            Test.stopTest();
			System.assertNotEquals(null, ccScheduler);
        }
	}
    
}