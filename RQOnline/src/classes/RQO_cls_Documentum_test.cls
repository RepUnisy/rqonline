/**
*	@name: RQO_cls_Documentum_test
*	@version: 1.0
*	@creation date: 25/10/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar los servicios de documentum y las clases asociadas
*/
@IsTest
public class RQO_cls_Documentum_test {
    
    /**
	* @creation date: 25/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserDocumentum@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSDocumentumRepository();
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_DOCUM_GET, RQO_cls_Constantes.WS_DOCUM_SAP_GET, RQO_cls_Constantes.WS_DOCUM_CREATE});
            REP_cs_activacionTrigger__c actTri = new REP_cs_activacionTrigger__c();
            actTri.REP_fld_triggerActive__c = false;
            actTri.REP_fld_triggerName__c = 'RQO_trg_GradeTrigger';
            actTri.Name = 'RQO_trg_GradeTrigger';
            insert actTri;
            RQO_cls_TestDataFactory.createDocument(200);
		}
    }
    
    /**
	* @creation date: 25/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar la funcionalidad de llamada directa correcta a documetum con id de documentum
	* @exception: 
	* @throws: 
	*/
    static testMethod void descargableItemDirectCall(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            
            RQO_obj_grade__c gradoinicial = [Select Id, Name, RQO_fld_sku__c, RQO_fld_blocked__c, RQO_fld_codeDocumentum__c, RQO_fld_new__c, RQO_fld_private__c, RQO_fld_product__c, 
                RQO_fld_public__c, RQO_fld_recommendation__c from RQO_obj_grade__c limit 1];
            
            List<RQO_obj_document__c> listObjDoc = [Select RQO_fld_documentId__c, RQO_fld_grade__c, RQO_fld_locale__c, RQO_fld_type__c 
                from RQO_obj_document__c where RQO_fld_grade__c = : gradoinicial.Id];
            
            test.startTest();
            //Resositorio no existente
            String[] nokResponse = RQO_cls_ProductoItemDescargable_cc.getDocDataValues(listObjDoc[0].RQO_fld_documentId__c, 'testRepository', null);
            System.assertEquals(nokResponse[0], RQO_cls_Constantes.DOCUMENTUM_ERROR_GENERIC_NOK);
            //Peticion correcta
            Test.setMock(HttpCalloutMock.class, new RQO_cls_DocumetumRESTMockTest(RQO_cls_Constantes.WS_DOCUM_GET, RQO_cls_Constantes.MOCK_EXEC_OK));
            String[] okResponse = RQO_cls_ProductoItemDescargable_cc.getDocDataValues(listObjDoc[0].RQO_fld_documentId__c, RQO_cls_Constantes.DOC_CS_REPOSITORY, null);
            System.assertEquals(okResponse[0], String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK));
            test.stopTest();
        }
	}

    /**
	* @creation date: 25/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar la funcionalidad de llamada directa incorrecta a documetum con id de documentum
	* @exception: 
	* @throws: 
	*/
    static testMethod void descargableItemDirectCallError(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            
            RQO_obj_grade__c gradoinicial = [Select Id, Name, RQO_fld_sku__c, RQO_fld_blocked__c, RQO_fld_codeDocumentum__c, RQO_fld_new__c, RQO_fld_private__c, RQO_fld_product__c, 
                RQO_fld_public__c, RQO_fld_recommendation__c from RQO_obj_grade__c limit 1];
            List<RQO_obj_document__c> listObjDoc = [Select RQO_fld_documentId__c, RQO_fld_grade__c, RQO_fld_locale__c, RQO_fld_type__c 
                from RQO_obj_document__c where RQO_fld_grade__c = : gradoinicial.Id];
            test.startTest();
                //Peticion incorrecta
                Test.setMock(HttpCalloutMock.class, new RQO_cls_DocumetumRESTMockTest(RQO_cls_Constantes.WS_DOCUM_GET, RQO_cls_Constantes.MOCK_EXEC_NOK));
                String[] nokResponse =RQO_cls_ProductoItemDescargable_cc.getDocDataValues(listObjDoc[0].RQO_fld_documentId__c, RQO_cls_Constantes.DOC_CS_REPOSITORY, null);
                System.assertEquals(nokResponse[0], String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_NOT_FOUND));    
            test.stopTest();
        }
	}

    /**
	* @creation date: 25/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar la funcionalidad de llamada indirecta correcta a documetum con id de SAP y no de documentum
	* @exception: 
	* @throws: 
	*/
	static testMethod void descargableItemIndirectCall(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){  
            RQO_obj_grade__c gradoinicial = [Select Id, Name, RQO_fld_sku__c, RQO_fld_blocked__c, RQO_fld_codeDocumentum__c, RQO_fld_new__c, RQO_fld_private__c, RQO_fld_product__c, 
                RQO_fld_public__c, RQO_fld_recommendation__c from RQO_obj_grade__c limit 1];
            
            List<RQO_obj_document__c> listObjDoc = [Select RQO_fld_documentId__c, RQO_fld_grade__c, RQO_fld_locale__c, RQO_fld_type__c 
                from RQO_obj_document__c where RQO_fld_grade__c = : gradoinicial.Id];
            test.startTest();
                //Peticion correcta
                Test.setMock(HttpCalloutMock.class, new RQO_cls_DocumetumRESTMockTest(RQO_cls_Constantes.WS_DOCUM_SAP_GET, RQO_cls_Constantes.MOCK_EXEC_OK));
                String[] okResponse =RQO_cls_ProductoItemDescargable_cc.getDocDataValues(listObjDoc[0].RQO_fld_documentId__c, RQO_cls_Constantes.DOC_CS_REPOSITORY, RQO_cls_Constantes.DOCUMENTUM_RECOVERY_TYPE_VISOR);
                System.assertEquals(okResponse[0], String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK));    
            test.stopTest();
        }
	}

    /**
	* @creation date: 25/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar la funcionalidad de carga de documentos en documetum
	* @exception: 
	* @throws: 
	*/
    
    static testMethod void uploadDoc(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            RQO_obj_grade__c gradoinicial = [Select Id, Name, RQO_fld_sku__c, RQO_fld_blocked__c, RQO_fld_codeDocumentum__c, RQO_fld_new__c, RQO_fld_private__c, RQO_fld_product__c, 
                RQO_fld_public__c, RQO_fld_recommendation__c from RQO_obj_grade__c limit 1];  
            List<RQO_obj_document__c> listObjDoc = [Select RQO_fld_documentId__c, RQO_fld_grade__c, RQO_fld_locale__c, RQO_fld_type__c 
                from RQO_obj_document__c where RQO_fld_grade__c = : gradoinicial.Id];
            test.startTest();
            Test.setMock(HttpCalloutMock.class, new RQO_cls_DocumetumRESTMockTest(RQO_cls_Constantes.WS_SAP_SERVICES, RQO_cls_Constantes.MOCK_EXEC_OK));
            RQO_cls_UploadDocument_cc.uploadDocToRepository(gradoinicial.Id, 'DocName', listObjDoc[0].RQO_fld_type__c, 'Base64Body', 'es_ES', listObjDoc[0].Id);
            RQO_obj_document__c docFinal = [Select RQO_fld_documentId__c from RQO_obj_document__c where Id = : listObjDoc[0].Id];
            System.assertNotEquals(null, docFinal.RQO_fld_documentId__c);    
            test.stopTest();
        }
    }
    
    /**
	* @creation date: 9/2/2018
	* @author: Juan Elías - Unisys
	* @description:	Método test para probar RQO_cls_EnqueueGetDocumentRP2.
	* @exception: 
	* @throws: 
	*/
    @isTest 
    static void testGetDocumentumRP2(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            List<String> piNameToCreate = new List<String>();
            piNameToCreate.add(RQO_cls_Constantes.WS_DOCUM_RP2_GET);
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(piNameToCreate);
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_RP2ServiceMockTest(true, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
                RQO_cls_Documentum serDoc = new RQO_cls_Documentum();
                serDoc.getDocumentumRP2(RQO_cls_Constantes.DOCUMENTUM_RP2_QUIMICA_CODE, Date.today(), Date.today());
            test.stopTest();
            System.assertNotEquals(null,serDoc.getMapDataService());
        }
    }
    
    /**
	* @creation date: 25/10/2017
	* @author: Juan Elías - Unisys
	* @description:	Método test para probar RQO_cls_EnqueueGetDocumentRP2.
	* @exception: 
	* @throws: 
	*/
    @isTest 
    static void testFalseGetDocumentumRP2(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            List<String> piNameToCreate = new List<String>();
            piNameToCreate.add(RQO_cls_Constantes.WS_DOCUM_RP2_GET);
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(piNameToCreate);
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_RP2ServiceMockTest(true, RQO_cls_Constantes.MOCK_EXEC_NOK, '0'));
                RQO_cls_Documentum serDoc = new RQO_cls_Documentum();
                serDoc.getDocumentumRP2(RQO_cls_Constantes.DOCUMENTUM_RP2_QUIMICA_CODE, Date.today(), Date.today());
            test.stopTest();
            System.assertEquals(null,serDoc.getMapDataService());
        }
    }
    
	/**
	* @creation date: 27/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método test para probar el método getInvoiceDoc
	* @exception: 
	* @throws: 
	*/
	static testMethod void getInvoiceDoc(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){  
            test.startTest();
                Test.setMock(HttpCalloutMock.class, new RQO_cls_DocumetumRESTMockTest(RQO_cls_Constantes.WS_DOCUM_SAP_GET, RQO_cls_Constantes.MOCK_EXEC_OK));
            	RQO_cls_Documentum docum = new RQO_cls_Documentum();
                RQO_cls_Documentum.RQO_cls_documentumResponse response = docum.getInvoiceDoc(RQO_cls_Constantes.DOC_CS_REPOSITORY, 'test', null);
                System.assertEquals(String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK), response.errorCode);    
            test.stopTest();
        }
	}
    
}