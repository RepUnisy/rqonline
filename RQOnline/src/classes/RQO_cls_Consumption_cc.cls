/**
*	@name: RQO_cls_Consumption
*	@version: 1.0
*	@creation date: 07/11/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Controlador Apex para recuperar los consumos de SAP
*	@testClass: RQO_cls_Consumption_test
*/
public with sharing class RQO_cls_Consumption_cc {
	
    private static final String CLASS_NAME = 'RQO_cls_ConsumptionWS';
	
    /**
    * @creation date: 07/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método invocable desde la vista para obtener los consumos de un cliente en SAP
    * @param: clientId, tipo Id. Id de la cuenta en salesforce
    * @param: gradeSKU, tipo String. El dato de grado que ha seteado el cliente en el buscador de consumos
    * @param: family, tipo String. Familia de grados sobre la que se quieren recuperar los consumos
    * @param: fechaInicio, tipo Date.
    * @param: fechaFin, tipo Date.
    * @return: String con el objeto RQO_cls_ConsumptionWS.ConsumptionCustomResponse con los datos de respuesta
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static String getSapConsumption(String clientId, String gradeSKU, String family, DateTime fechaInicio, DateTime fechaFin){
        final String METHOD = 'getSapConsumption';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        System.debug('Prueba: ' + family);
        Date fechaInicioDate = (fechaInicio != null) ? fechaInicio.date() : null;
        Date fechaFinDate = (fechaFin != null) ? fechaFin.date() : null;
        List<RQO_cls_ConsumptionAuxObj> listaConsumos = null;
        RQO_cls_ConsumptionWS consum = new RQO_cls_ConsumptionWS();
        String gradeSKUServices = '';
        if (String.isNotEmpty(gradeSKU)){
            gradeSKUServices = gradeSKU.left(gradeSKU.length() - 2);
        }

        RQO_cls_ConsumptionWS.ConsumptionCustomResponse responseData = consum.getSapConsumption(clientId, gradeSKUServices, family, fechaInicioDate, fechaFinDate);
        String serializedResponse = JSON.serialize(responseData);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        System.debug('AA: ' + serializedResponse);
        return serializedResponse;
    }
}