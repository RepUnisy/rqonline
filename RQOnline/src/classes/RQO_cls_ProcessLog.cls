/**
 *	@name: RQO_cls_ProcessLog
 *	@version: 1.0
 *	@creation date: 29/09/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase de utilidades para el objeto de log
 *
*/
public with sharing class RQO_cls_ProcessLog {
    
   /**
	* @creation date: 17/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método que instancia y completa los datos iniciales de un objeto de tipo log para los web services
	* @param: 	origin	 - Quien realiza la petición 
	* @param: 	destiny	 - Contra quien se realiza la petición
	* @return:	RQO_obj_logProcesos__c
	* @exception: 
	* @throws: 
	*/
	public static RQO_obj_logProcesos__c instanceInitalLogService(String origin, String destiny, String processDescription){
        RQO_obj_logProcesos__c objLog = new RQO_obj_logProcesos__c();
        objLog.RQO_fld_processType__c = RQO_cls_Constantes.LOG_TYPE_SERVICE;
        objLog.RQO_fld_fechaPeticion__c = System.now();
        objLog.RQO_fld_origin__c = origin;
        objLog.RQO_fld_destiny__c = destiny;
        objLog.RQO_fld_descripciondeProceso__c = processDescription;
        return objLog;
    }
    
    /**
	* @creation date: 17/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método que completa los datos finales de un objeto de tipo log para los web services
	* @param: 	objLog	 - Tipo RQO_obj_logProcesos__c, pasamos por referencia el objeto que queremos comppletar
	* @param: 	errorCode	 - Código de error
	* @param: 	errorMessage	 - Mensaje de error
	* @param: 	exceptionMessage	 - Mensaje de excepción
	* @return:
	* @exception: 
	* @throws: 
	*/
	public static void completeLog(RQO_obj_logProcesos__c objLog, String errorCode, String errorMessage, String exceptionMessage){
        objLog.RQO_fld_estado__c = (String.isNotEmpty(errorCode)) ? getStatusCodeByErrorCode(errorCode) : '';
        objLog.RQO_fld_errorCode__c = errorCode;
        objLog.RQO_fld_errorMessage__c = (String.isNotEmpty(errorMessage) && errorMessage.length() > 10000) ? errorMessage.substring(0, 10000) : errorMessage;
        objLog.RQO_fld_exceptionMessage__c = exceptionMessage;
    }
    
	/**
	* @creation date: 10/10/2017
	* @author: David Iglesias - Unisys
	* @description:	Método que instancia y completa un objeto de tipo log para los procesos Batch
	* @param: 	tipoObjeto	 - Tipo de objeto tratado
	* @param: 	errorCode	 - Código de error
	* @param: 	msgError	 - Mensaje custom de error
	* @param: 	message		 - Mensaje de excepción producida
	* @param: 	idExterno	 - Id externo del objeto tratado
	* @return:	RQO_obj_logProcesos__c
	* @exception: 
	* @throws: 
	*/
    public static RQO_obj_logProcesos__c instanceInitalLogBatch(String tipoObjeto, String errorCode, String msgError, String message, String idExterno, String processDescription){
        
        RQO_obj_logProcesos__c objLog = new RQO_obj_logProcesos__c();
        objLog.RQO_fld_fechaPeticion__c = System.now();
        objLog.RQO_fld_fechaRespuesta__c = System.now();
        objLog.RQO_fld_processType__c = RQO_cls_Constantes.LOG_TYPE_BATCH;
		objLog.RQO_fld_objType__c = (tipoObjeto!=null)?tipoObjeto:RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_errorCode__c = (errorCode!=null)?errorCode:RQO_cls_Constantes.LOG_NO_APPLY;
		objLog.RQO_fld_errorMessage__c = (msgError!=null)?msgError:RQO_cls_Constantes.LOG_NO_APPLY;
		objLog.RQO_fld_exceptionMessage__c = (message!=null)?message:RQO_cls_Constantes.LOG_NO_APPLY;
		objLog.RQO_fld_objExternalId__c = idExterno;
        objLog.RQO_fld_body__c = RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_destiny__c = RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_header__c = RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_host__c = RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_numeroIntentos__c = 0;
        objLog.RQO_fld_origin__c = RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_retry__c = false;
        objLog.RQO_fld_response__c = RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_descripciondeProceso__c = processDescription;
        
        return objLog;
    }
    
    /**
	* @creation date: 17/10/2017
	* @author: David Iglesias - Unisys
	* @description:	Método que instancia y completa un objeto de tipo log para las peticiones recibidas REST
	* @param: 	tipoObjeto	 	- Tipo de objeto tratado
	* @param: 	errorCode	 	- Código de error
	* @param: 	msgError	 	- Mensaje custom de error
	* @param: 	message		 	- Mensaje de excepción producida
	* @param: 	idExterno	 	- Id externo del objeto tratado
    * @param: 	fechaPeticion	- Fecha de la petición
    * @param: 	header	 		- Cabecera de la petición recibida
    * @param: 	body	 		- Cuerpo de la petición recibida
	* @return:	RQO_obj_logProcesos__c
	* @exception: 
	* @throws: 
	*/
    public static RQO_obj_logProcesos__c instanceInitalLogRest(String tipoObjeto, String errorCode, String msgError, String message, String idExterno,
                                                               		Datetime fechaPeticion, String header, String body, String processDescription){
        RQO_obj_logProcesos__c objLog = new RQO_obj_logProcesos__c();
        objLog.RQO_fld_fechaPeticion__c = fechaPeticion;
        objLog.RQO_fld_fechaRespuesta__c = fechaPeticion;
        objLog.RQO_fld_processType__c = RQO_cls_Constantes.LOG_TYPE_SERVICE;
		objLog.RQO_fld_objType__c = (tipoObjeto!=null)?tipoObjeto:RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_errorCode__c = (errorCode!=null)?errorCode:RQO_cls_Constantes.LOG_NO_APPLY;
		objLog.RQO_fld_errorMessage__c = (msgError!=null)?msgError:RQO_cls_Constantes.LOG_NO_APPLY;
		objLog.RQO_fld_exceptionMessage__c = (message!=null)?message:RQO_cls_Constantes.LOG_NO_APPLY;
		objLog.RQO_fld_objExternalId__c = idExterno;
        objLog.RQO_fld_body__c = body;
        objLog.RQO_fld_destiny__c = RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE;
        objLog.RQO_fld_header__c = header;
        objLog.RQO_fld_host__c = RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_numeroIntentos__c = 0;
        objLog.RQO_fld_origin__c = RQO_cls_Constantes.LOG_ORIGIN_SAP;
        objLog.RQO_fld_retry__c = false;
        objLog.RQO_fld_response__c = RQO_cls_Constantes.LOG_NO_APPLY;
		objLog.RQO_fld_descripciondeProceso__c = processDescription;
        
        return objLog;
    }
    
	/**
	* @creation date: 16/11/2017
	* @author: Álvaro Alonso - Unisys
	* @description:	Método que instancia y completa un objeto de tipo log para un error genérico	
    * @param: 	errorCode	 	- Código de error
    * @param: 	msgError	 	- Mensaje custom de error
    * @param: 	message		 	- Mensaje de excepción producida
	* @return:	RQO_obj_logProcesos__c
	* @exception: 
	* @throws: 
	*/
    public static RQO_obj_logProcesos__c instanceGenericLog(String errorCode, String msgError, String message){
        RQO_obj_logProcesos__c objLog = new RQO_obj_logProcesos__c();
        objLog.RQO_fld_processType__c = RQO_cls_Constantes.LOG_TYPE_GENERIC;
		objLog.RQO_fld_objType__c = RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_errorCode__c = (errorCode!=null)?errorCode:RQO_cls_Constantes.LOG_NO_APPLY;
		objLog.RQO_fld_errorMessage__c = (msgError!=null)?msgError:RQO_cls_Constantes.LOG_NO_APPLY;
		objLog.RQO_fld_exceptionMessage__c = (message!=null)?message:RQO_cls_Constantes.LOG_NO_APPLY;
        objLog.RQO_fld_numeroIntentos__c = 0;
        objLog.RQO_fld_retry__c = false;
        return objLog;
    }
    
	/**
	* @creation date: 29/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método que genera un registro de log a partir de una lista de objetos de tipo log
	* @param: logList, tipo List<RQO_obj_logProcesos__c>
	* @return: 
	* @exception: 
	* @throws: 
	*/
    public static void generateLogData(List<RQO_obj_logProcesos__c> logList){
        
       	Database.UpsertResult[] srListResult = Database.upsert(logList, false);
        //Iteracion del resultado de la operacion DML
        for (Database.UpsertResult sr : srListResult) {
            if (sr.isSuccess()) {
                System.debug('sr.getId(): '+sr.getId());
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Campos afectados por el error: ' + err.getFields());
                }
            }
        }

    }
    
    /**
	* @creation date: 29/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método que genera un registro de log para los webservices
	* @param: header tipo String, cabera del servicio.
	* @param: body tipo String, cuerpo de la request
	* @param: response tipo String, respuesta del servicio. 
	* @param: origin tipo String, sistema que realiza la petición. 
	* @param: destiny tipo string, sistema al que se enevía la petición.
	* @param: status tipo String, estado. 
	* @param:initialDate tipo Datetime, fecha de inicio del proceso. 
	* @param: endDate tipo Datetime, fecha de fin del proceso. 
	* @param: host tipo String, el endpoint. 
	* @param: isRetry tipo Boolean, indica si el proceso es susceptible de reintento.
	* @param: errorMessage tipo String, mensaje de error. 
	* @param: exceptionMessage tipo string, mensaje de la excepción
	* @return: Id del registro de log generado
	* @exception: 
	* @throws: 
	*/
    public Id generateLogServiceType(String header, String body, String response, String origin, String destiny, 
                                     String status, Datetime initialDate, Datetime endDate, String host, Boolean isRetry, 
                                     String errorCode, String errorMessage, String exceptionMessage){
                              
		RQO_obj_logProcesos__c logObj = new RQO_obj_logProcesos__c(RQO_fld_processType__c = RQO_cls_Constantes.LOG_TYPE_SERVICE, 
                                                                    RQO_fld_header__c = header, RQO_fld_body__c = body, 
                                                                    RQO_fld_response__c = response, RQO_fld_estado__c = status, 
																	RQO_fld_fechaPeticion__c = initialDate, RQO_fld_fechaRespuesta__c =  endDate, 
                                                                    RQO_fld_errorMessage__c = errorMessage, RQO_fld_exceptionMessage__c = exceptionMessage,
                                                                    RQO_fld_errorCode__c = errorCode, RQO_fld_host__c = host, RQO_fld_retry__c = isRetry);
        insert logObj;
        return logObj.Id;
	}
       
    /**
	* @creation date: 29/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método que recupera el estado que hay que establecer en el registro de log en función del códgo de error que nos llega como parámetro.
	* @param: errorCode tipo String, el código de error
	* @return: Id del registro de log generado
	* @exception: 
	* @throws: 
	*/
    public static String getStatusCodeByErrorCode(String errorCode){
        if (String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK).equalsIgnoreCase(errorCode) || RQO_cls_Constantes.SOAP_COD_OK.equalsIgnoreCase(errorCode)){
            return RQO_cls_Constantes.PETICION_ESTADO_OK;
        }else if (String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_TIME_OUT).equalsIgnoreCase(errorCode)){
            return RQO_cls_Constantes.PETICION_RESULTADO_ERR_TIME_OUT;
        }else if (String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_INTERNAL_SERVER_ERROR).equalsIgnoreCase(errorCode)){
            return RQO_cls_Constantes.PETICION_RESULTADO_ERR_INTERNAL_SERVER_ERROR;
        }else if (String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_NOT_FOUND).equalsIgnoreCase(errorCode)){
            return RQO_cls_Constantes.PETICION_RESULTADO_ERR_NOT_FOUND;
        }
        return RQO_cls_Constantes.PETICION_ESTADO_GENERIC;
    }

}