/**
*	@name: RQO_cls_LCDocDataAux
*	@version: 1.0
*	@creation date: 28/09/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Objeto wrapper con los datos auxiliares de documentos para facilitar la gestión y descarga de estos
*	@testClass: RQO_cls_DetalleProducto4_test
*/
public class RQO_cls_LCDocDataAux implements Comparable{
    // ***** PROPIEDADES ***** //
    @AuraEnabled
    public String descriptionCode{get;set;}
    
    @AuraEnabled
    public String description{get;set;}
    
    @AuraEnabled
	public String idDoc{get;set;}
    
    @AuraEnabled
	public String docRecoveryType{get;set;}
    
    @AuraEnabled
	public String docRepository{get;set;}
   
    @AuraEnabled
	public String docDescriptionType{get;set;}
    
	@AuraEnabled
	public Id sfDocId{get;set;}

    public String idiomaUsuario{get;set;}
    
    /**
    * @creation date: 24/01/2018
    * @author: Alvaro Alonso Trinidad - Unisys
    * @description: implementación del método compareTo para ordenar la lista de objetos
    * @param: compareTo tipo Object
    * @return: Integer
    * @exception: 
    * @throws: 
    */
    public Integer compareTo(Object objToCompare) {
        RQO_cls_LCDocDataAux compareToEmp = (RQO_cls_LCDocDataAux)objToCompare;
        if (idiomaUsuario.equalsIgnoreCase(descriptionCode)){return -1;}
        if (description.compareTo(compareToEmp.description) > 0){return 1;}
        if (description.compareTo(compareToEmp.description) < 0){return -1;}
        return 0;
	}
}