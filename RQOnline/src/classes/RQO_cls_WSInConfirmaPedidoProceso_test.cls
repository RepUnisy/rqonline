/**
*	@name: RQO_cls_WSInConfirmaPedidoProceso_test
*	@version: 1.0
*	@creation date: 14/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar los métodos asociados a WSInConfirmaPedidoProceso
*/
@IsTest
public class RQO_cls_WSInConfirmaPedidoProceso_test {
	
    /**
    *	@creation date: 14/2/2018
    *	@author: Juan Elías - Unisys
    *   @description: Método de inicialización del conjunto de datos necesario para el test
    *   @exception: 
    *   @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
            List<RQO_obj_container__c> listContainer = RQO_cls_TestDataFactory.createContainer(200);
            listContainer[0].RQO_fld_codigo__c = 'P7';
            update listContainer[0];
            RecordType rtActual = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ACCOUNT_QUIMICA limit 1];
            List<Account> listaCuentas = RQO_cls_TestDataFactory.createAccount(1);
            listaCuentas[0].Name = 'cuentaAgreement';
            listaCuentas[0].RecordtypeId = rtActual.Id;
            listaCuentas[0].RQO_fld_idExterno__c = 'acExtId';
            listaCuentas[0].RQO_fld_pais__c = 'ES';
            listaCuentas[0].Phone = '914443322';
            update listaCuentas[0];
            RQO_cls_TestDataFactory.createContact(1);
            List<RQO_obj_qp0Grade__c> listQp0Grade = RQO_cls_TestDataFactory.createQp0Grade(1);
            listQp0Grade[0].RQO_fld_sku__c = 'PP099X2M';
            listQp0Grade[0].name = 'gradeQp0Agreement';
            listQp0Grade[0].RQO_fld_descripcionDelGrado__c = 'Qp0 grade test';
            update listQp0Grade[0];
            RecordType rtActualSec = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ASSET_REQUEST_POSITION limit 1];
            List<Asset> listAsset = RQO_cls_TestDataFactory.createAsset(200);
            listAsset[0].RecordTypeId = rtActualSec.Id;
            update listAsset[0];
        }
    }
    
    /**
    *	@creation date: 14/2/2018
    *	@author: Juan Elías - Unisys
    *   @description: Método test para probar el servicio WSInConfirmaPedidoProceso
    *   @exception: 
    *   @throws: 
    */
    @isTest
    static void testRecibirConfirmacionPedidoCreado(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String esperado;
            Datetime now = System.now();
            Map<String, String> headers;
            Account cliente = [SELECT id, RQO_fld_idExterno__c FROM Account limit 1];
            cliente.RQO_fld_idExterno__c = '0000083921';
            update cliente;
            String body = '{"pedidosList":[{"externalId":"0013973248","tipoPedido":"TA","solicitante":"0000083921","moneda":"EUR","incoterm":"","paisReceptorFiscal":"FR","fechaPreferenteEntrega":"20160223","fechaGrabacionRegistro":"20160210","interlocutorWE":"83921F47","interlocutorRE":"0000083921","interlocutorSB":"","interlocutorZA":"VF9","interlocutorZC":"FIT7","numPedidoCliente":"","isDelete":"","posicionePedidosList":[{"externalId":"0013973248000010","pedido":"0013973248","posicionPedido":"000010","centro":"03","material":"PP099X2MP7","cantidad":"10000","unidadMedida":"KG","fechaReparto":"20160223","textoInhibidor":"","estado":"P","idPosicionSolicitud":"","descripcionGrado":"ISPLEN PP099X2M","fechaConfirmacion":"","isDelete":"","entregasList":[{"externalId":"0024421021","mascEdicSegmFecha":"YMIGDLV","claseEntrega":"LF","numExtEntrega":"","fechaSalidaMercancias":"20160212","fechaEntregaClienteAlbaran":"20160223","fechaMovimientoMercancias":"20160223","fechaRealLlegada":"","factura":"0395326797","estadoEntrega":"F","isDelete":"","fechaEstimadaLLegada":"","posicionesentregaList":[{"externalId":"0024421021000010","entrega":"0024421021","posicionEntrega":"000010","indSujetoLote":"X","cantidadEntrega":"10.000","unidad":"KG","idArchivado":"","fechaRegistro":"20160212","isDelete":"","posicionPedido":"0013973248000010","posicionesLoteList":[{"externalId":"0024421021000010900001","posicionEntrega":"0024421021000010","posicionLote":"900001","cantidadSalidaMercancias":"10.000","unidad":"KG","isDelete":""}]}]}]}]}]}'; 
            QueueableContext context;

            test.startTest();
            RQO_cls_WSInConfirmaPedidoProceso WSInConfirmaPedidoProceso = new RQO_cls_WSInConfirmaPedidoProceso(now, headers, body);
            WSInConfirmaPedidoProceso.execute(context);
            Asset asset = [SELECT id, RQO_fld_material__c, RQO_fld_unidaddeMedida__c, RQO_fld_posiciondePedido__c, RQO_fld_positionStatus__c, RQO_fld_descripciondeGrado__c FROM Asset limit 1];
            System.assertEquals( esperado, asset.RQO_fld_positionStatus__c);
            body = '{"pedidosList":[{"externalId":"0013973248","tipoPedido":"TA","solicitante":"0000083921","moneda":"EUR","incoterm":"","paisReceptorFiscal":"FR","fechaPreferenteEntrega":null,"fechaGrabacionRegistro":null,"interlocutorWE":"83921F47","interlocutorRE":"0000083921","interlocutorSB":"","interlocutorZA":"VF9","interlocutorZC":"FIT7","numPedidoCliente":"","isDelete":"","posicionePedidosList":[{"externalId":"0013973248000010","pedido":"0013973248","posicionPedido":"000010","centro":"03","material":"PP099X2MP7","cantidad":"10000","unidadMedida":"KG","fechaReparto":null,"textoInhibidor":"","estado":"P","idPosicionSolicitud":"","descripcionGrado":"ISPLEN PP099X2M","fechaConfirmacion":null,"isDelete":"","entregasList":[{"externalId":"0024421021","mascEdicSegmFecha":"YMIGDLV","claseEntrega":"LF","numExtEntrega":"","fechaSalidaMercancias":null,"fechaEntregaClienteAlbaran":null,"fechaMovimientoMercancias":null,"fechaRealLlegada":null,"factura":"0395326797","estadoEntrega":"F","isDelete":"","fechaEstimadaLLegada":"","posicionesentregaList":[{"externalId":"0024421021000010","entrega":"0024421021","posicionEntrega":"000010","indSujetoLote":"X","cantidadEntrega":"10.000","unidad":"KG","idArchivado":"","fechaRegistro":null,"isDelete":"","posicionPedido":"0013973248000010","posicionesLoteList":[{"externalId":"0024421021000010900001","posicionEntrega":"0024421021000010","posicionLote":"900001","cantidadSalidaMercancias":"10.000","unidad":"KG","isDelete":""}]}]}]}]}]}'; 
            WSInConfirmaPedidoProceso = new RQO_cls_WSInConfirmaPedidoProceso(now, headers, body);
            WSInConfirmaPedidoProceso.execute(context);
            RQO_obj_pedido__c pedido = [SELECT id, RQO_fld_tipodePedido__c FROM RQO_obj_pedido__c limit 1];
            System.assertEquals('test', pedido.RQO_fld_tipodePedido__c);
            body = '{"pedidosList":[{"externalId":"0013973248","tipoPedido":"TA","solicitante":"0000083921","moneda":"EUR","incoterm":"","paisReceptorFiscal":"FR","fechaPreferenteEntrega":"*","fechaGrabacionRegistro":"*","interlocutorWE":"83921F47","interlocutorRE":"0000083921","interlocutorSB":"","interlocutorZA":"VF9","interlocutorZC":"FIT7","numPedidoCliente":"","isDelete":"","posicionePedidosList":[{"externalId":"0013973248000010","pedido":"0013973248","posicionPedido":"000010","centro":"03","material":"PP099X2MP7","cantidad":"10000","unidadMedida":"KG","fechaReparto":"*","textoInhibidor":"","estado":"P","idPosicionSolicitud":"","descripcionGrado":"ISPLEN PP099X2M","fechaConfirmacion":"*","isDelete":"","entregasList":[{"externalId":"0024421021","mascEdicSegmFecha":"YMIGDLV","claseEntrega":"LF","numExtEntrega":"","fechaSalidaMercancias":"*","fechaEntregaClienteAlbaran":"*","fechaMovimientoMercancias":"*","fechaRealLlegada":"*","factura":"0395326797","estadoEntrega":"F","isDelete":"","fechaEstimadaLLegada":"","posicionesentregaList":[{"externalId":"0024421021000010","entrega":"0024421021","posicionEntrega":"000010","indSujetoLote":"X","cantidadEntrega":"10.000","unidad":"KG","idArchivado":"","fechaRegistro":"*","isDelete":"","posicionPedido":"0013973248000010","posicionesLoteList":[{"externalId":"0024421021000010900001","posicionEntrega":"0024421021000010","posicionLote":"900001","cantidadSalidaMercancias":"10.000","unidad":"KG","isDelete":""}]}]}]}]}]}';      
            WSInConfirmaPedidoProceso = new RQO_cls_WSInConfirmaPedidoProceso(now, headers, body);
            WSInConfirmaPedidoProceso.execute(context);
            RQO_obj_entrega__c entrega = [SELECT id, RQO_fld_idExterno__c, RQO_fld_mascaraSegmento__c, RQO_fld_clasedeEntrega__c FROM RQO_obj_entrega__c limit 1];
            System.assertEquals('LF', entrega.RQO_fld_clasedeEntrega__c);
            test.stopTest();
        }
    }
}