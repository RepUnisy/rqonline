/**
*   @name: RQO_cls_WrapperUser
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Wrapper de usuarios
*/
public without sharing class RQO_cls_WrapperUser {
    
    public String name  {get; set;}
    public String language  {get; set;}
    public List<Contact> clientes {get; set;}
    private boolean isPrivate = false;
    private boolean esCliente {get; set;}
    private string perfilUsuario {get; set;}
    private Map<Id, Account> mapaClientes = new Map<Id, Account>();
    private List<RQO_obj_relaciondeVentas__c> relaciones;
    
    private Map<String, List<RQO_obj_relaciondeVentas__c>> mapSuEquipo = new Map<String, List<RQO_obj_relaciondeVentas__c>>();
    private List<RQO_obj_relaciondeVentas__c> listDirMercancias;
    private List<seleccionador> listDirMercanciasSelect = new List<seleccionador>();
    private List<seleccionador> listDirFacturacionSelect = new List<seleccionador>();
    private List<seleccionador> paisReceptorFiscal = new List<seleccionador>();
    private List<RQO_obj_relaciondeVentas__c> listDirFacturacion;
    private List<RQO_obj_relaciondeVentas__c> listCabeceraGrupo = new List<RQO_obj_relaciondeVentas__c>();
    private List<String> listIncoterm = new List<String>();
    private List<String> listDirEnvio = new List<string>();
    
    private Id idContacto;
    private String perfilCliente;
    private Set<string> permissionUrls;
    private Set<string> permissionApiNames;
    private boolean hasResponsableStock = false;
    private Account cliente;
    private Id idCliente;
    private String idExternoCliente;
    
    /**
    *   @name: RQO_cls_WrapperUser
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase usada como modelo de un seleccionador
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public class seleccionador {
        string value;
        string label;
        
        public seleccionador(string v, string l) {
            this.value = v;
            this.label = l;
        }
    }
    
    /**
    *   @name: RQO_cls_WrapperUser
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Constructor de la clase RQO_cls_WrapperUser
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public RQO_cls_WrapperUser(){
        
    }
    
    /**
    *   @name: RQO_cls_WrapperUser
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece un contacto
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public void setContacto(Contact contacto) {
                
        if (contacto == null) {
            isPrivate = false;
        } else {
            isPrivate = true;
            idContacto = contacto.id;        
            perfilCliente = (contacto.RQO_fld_perfilCliente__c != null) ? contacto.RQO_fld_perfilCliente__c : ''; 
            perfilUsuario = (contacto.RQO_fld_perfilUsuario__c != null) ? contacto.RQO_fld_perfilUsuario__c : ''; 
        }
    }
    
    /**
    *   @name: RQO_cls_WrapperUser
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que genera la dirección de un cliente en base a sus propiedades (nombre, calle, ciudad, pais...)
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private String generateDirCliente (String name, String name3, String calle, String city, String pais) {
        String direccion = (name != null ? name + ', ' : '');
        direccion = direccion + (name3 != null ? name3 + ', ' : '');
        direccion = direccion + (calle != null ? calle + ', ' : '');
        direccion = direccion + (city != null ? city + ', ' : '');
        direccion = direccion + (pais != null ? pais : '');
        return direccion;
    }
    
    /**
    * @creation date: 29/01/2018
    * @author: 	Alfonso Constán López - Unisys
    * @description: Inicialización del cliente
    * @param:
    * @return: 
    * @exception: 
    * @throws: 
    */
    public void setCliente(Account clienteP){
        cliente = clienteP;
        idCliente = clienteP.Id;
        
        esCliente = (clienteP.RQO_fld_esCliente__c == true) ? true : false;
        
        if (clienteP.RQO_fld_idExterno__c != null && clienteP.RQO_fld_idExterno__c != ''){
            idExternoCliente = clienteP.RQO_fld_idExterno__c;
        }        
        
        String dirCliente = generateDirCliente(cliente.Name, cliente.RQO_fld_name3__c, cliente.RQO_fld_calleNumero__c, cliente.RQO_fld_poblacion__c, cliente.RQO_fld_pais__c); 
        listDirMercanciasSelect.add(new seleccionador(cliente.Id, dirCliente));
        listDirFacturacionSelect.add(new seleccionador(cliente.Id, dirCliente));
        
        //	Obtenemos sus relaciones de venta        
        List<RQO_obj_relaciondeVentas__c> listRelaciones = [select id, 
                                                            RQO_fld_idRelacion__r.Name,
                                                            RQO_fld_idRelacion__r.RQO_fld_organizaciondeVentas__r.RQO_fld_nombreOrg__c,
                                                            RQO_fld_idRelacion__r.RQO_fld_incoterm__c,
                                                            RQO_fld_relacionTipo__c,
                                                            RQO_fld_cliente__c,
                                                            RQO_fld_cliente__r.RQO_fld_idExterno__c,
                                                            RQO_fld_cliente__r.Name, 
                                                            RQO_fld_cliente__r.RQO_fld_telefono__c,
                                                            RQO_fld_cliente__r.RQO_fld_mail__c,
                                                            RQO_fld_cliente__r.RQO_fld_name3__c,
                                                            RQO_fld_calleNumero__c,                                                              
                                                            RQO_fld_city__c,
                                                            RQO_fld_country__c
                                                            from RQO_obj_relaciondeVentas__c 
                                                            where RQO_fld_idRelacion__r.RQO_fld_idRelacion__c = :idCliente 
                                                            AND RQO_fld_isDelete__c = false   
                                                            AND RQO_fld_idRelacion__r.RQO_fld_isDelete__c = false
                                                            AND RQO_fld_cliente__r.RQO_fld_isDelete__c = false
                                                            order by RQO_fld_cliente__r.Name];		
        
        //	LIstado de facturacion
        listDirFacturacion = new List<RQO_obj_relaciondeVentas__c> ();
        listDirMercancias = new List<RQO_obj_relaciondeVentas__c> ();
        relaciones = new List<RQO_obj_relaciondeVentas__c> ();
        Set<String> setIncoterm = New Set<String>();
        Set<String> setRelaciones = new Set<String>();
        
        //	Obtenemos los valores de las relaciones con su traduccion
        Map<String, String> mapRelacionTipo = new Map<String, String>();
        List<Schema.PicklistEntry> relacionTipo =  RQO_obj_relaciondeVentas__c.RQO_fld_relacionTipo__c.getDescribe().getPicklistValues();        
        for( Schema.PicklistEntry f : relacionTipo){
            mapRelacionTipo.put(f.getValue(), f.getLabel());
        }
                
        //	Recorremos las distintas relaciones de ventas, para diseccionarlas y agruparlas por tipo
        for(RQO_obj_relaciondeVentas__c rv : listRelaciones){
            //	Insertamos los incoterm
            addSetIncoterm(rv.RQO_fld_idRelacion__r.RQO_fld_incoterm__c, setIncoterm );
            
            if (rv.RQO_fld_relacionTipo__c == RQO_cls_Constantes.RELACION_VENTA_DESTINO_MERCANCIAS){
                addDestinoMercancias(rv);
            }
            else{
                if (rv.RQO_fld_relacionTipo__c == RQO_cls_Constantes.RELACION_VENTA_DIRECCION_FACTURACION){
                    addDirFacturacion(rv);
                }
                else{
                    if (rv.RQO_fld_relacionTipo__c == RQO_cls_Constantes.RELACION_VENTA_AGENTE_COMERCIAL || rv.RQO_fld_relacionTipo__c == RQO_cls_Constantes.RELACION_VENTA_ASISTENCIA_TECNICA || rv.RQO_fld_relacionTipo__c == RQO_cls_Constantes.RELACION_VENTA_TECNICO_COMERCIAL){
                        addSuEquipo(rv, mapRelacionTipo);
                    }
                    else{
                        if(rv.RQO_fld_relacionTipo__c == RQO_cls_Constantes.RELACION_VENTA_CABECERA_GRUPO){
                            addCabeceraGrupo(rv);
                        }
                        else{
                            if(rv.RQO_fld_relacionTipo__c == RQO_cls_Constantes.RELACION_VENTA_RESPONSABLE_STOCK){
                                hasResponsableStock();
                            }
                        }
                    }  
                }
            }                                        
        } 
        
        //	Añadimos el incoterm FCA para que aparezca siempre
        addSetIncoterm(Label.RQO_lbl_incotermFCA, setIncoterm );
        listIncoterm.addAll(setIncoterm);
        
    }
    
    /**
    *   @name: RQO_cls_WrapperUser
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que establece los permisos de las URLs
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public void setContacto(Set<string> permissionUrls){
    	permissionApiNames = new RQO_cls_UserProfileSelector().selectPermissionsApiNameByProfile(perfilUsuario);
    	this.permissionUrls = permissionUrls;
    }
    
    /**
    *   @name: RQO_cls_WrapperUser
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el estado del flag isPrivate
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public boolean isPrivate(){
        return isPrivate;
    }
    
    /**
    * @creation date: 29/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método que retorna un perfil de usuario
    * @param:
    * @return: 
    * @exception: 
    * @throws: 
    */
    public string perfilUsuario(){
        return perfilUsuario;
    }
    
    /**
    * @creation date: 29/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método que retorna un perfil del cliente
    * @param:
    * @return: 
    * @exception: 
    * @throws: 
    */
   	public string perfilCliente(){
        return perfilCliente;
    }
    
    /**
    * @creation date: 29/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método que retorna si se tiene acceso al perfil mediante una url pasada por parámetro
    * @param:
    * @return: 
    * @exception: 
    * @throws: 
    */
    public Boolean hasAccess(string url)
    {
		return permissionUrls.contains(url);
    }
    
    /**
    * @creation date: 29/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método que establece el Id del Cliente
    * @param:
    * @return: 
    * @exception: 
    * @throws: 
    */
    public void setIdCliente(Id idCliente){
    	this.idCliente = idCliente;
    }
    
    /**
    * @creation date: 29/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método que establece el id del ExternoCliente
    * @param:
    * @return: 
    * @exception: 
    * @throws: 
    */
    public void setIdExternoCliente(string idExternoCliente){
        this.idExternoCliente = idExternoCliente;
    }
    
    /**
    * @creation date: 29/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Inserción del país receptor fiscal
    * @param:
    * @return: 
    * @exception: 
    * @throws: 
    */
    public void setPaisReceptor(){
        
        Set<String> setIdentificador = new Set<String>();
        List<RQO_obj_identificador__c> listPais = [select id, RQO_fld_tipodeIdentificador__c from RQO_obj_identificador__c where RQO_fld_idRelacion__c = : idCliente];
        for (RQO_obj_identificador__c ident : listPais){
            
            String code = ident.RQO_fld_tipodeIdentificador__c.substring(0, 2);
            if (!setIdentificador.contains(code) && string.isNotBlank(code)){
                
                setIdentificador.add(code);
                paisReceptorFiscal.add(new seleccionador(code, code));
            }
        }        
    }
    
    /**
    * @creation date: 29/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Inserción de incoterm en caso de no existir
    * @param: incoterm. nuevo incoterm
    * @param: setIncoterm. listado de incoterm
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void addSetIncoterm(String incoterm, Set<String> setIncoterm ){        
        //Comprobamos si existe el incoterm
        if (!setIncoterm.contains(incoterm) && incoterm != ''){
            setIncoterm.add(incoterm);
        }                
    }
    
    
    /**
    * @creation date: 29/09/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Inicialización del equipo repsol del usuario logueado
    * @param: rv. información de la relación de venta
    * @param: mapRelacionTipo. Traducción de la relacion de venta
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void addSuEquipo (RQO_obj_relaciondeVentas__c rv, Map<String, String> mapRelacionTipo){
        
        if( rv.RQO_fld_idRelacion__r.RQO_fld_organizaciondeVentas__c != null ){
            
            //	Obtenemos el código de la organización de venta
            String tradOrganizacionVenta = rv.RQO_fld_idRelacion__r.RQO_fld_organizaciondeVentas__r.RQO_fld_nombreOrg__c;
            try{
                //	Creamos un texto con el nombre de la label de forma dinámica
                String labelArea = 'RQO_lbl_areaVenta' + rv.RQO_fld_idRelacion__r.RQO_fld_organizaciondeVentas__r.RQO_fld_nombreOrg__c;
                //	Obtenemos la traducción de la label
                tradOrganizacionVenta = RQO_cls_GeneralUtilities.translate(labelArea, UserInfo.getLocale()).trim();
            }
            catch (Exception e){
                system.debug('Exception: Traduccion label ' +tradOrganizacionVenta);
            }
            
            //	Comprobamos si existe la organización como clave del mapa
            if (mapSuEquipo.containsKey(tradOrganizacionVenta)){
                //	Obtenemos el listado de relaciones para el area de venta buscado
                List<RQO_obj_relaciondeVentas__c> listRel = mapSuEquipo.get(tradOrganizacionVenta);
                //	Obtenemos la traducción de la relación y actualizamos el relacion tipo con su traducción
                rv.RQO_fld_relacionTipo__c = mapRelacionTipo.containsKey(rv.RQO_fld_relacionTipo__c) ? mapRelacionTipo.get(rv.RQO_fld_relacionTipo__c) : rv.RQO_fld_relacionTipo__c;
                //	Introducimos la relación en la lista
                listRel.add(rv);
            }
            else{
                //	En caso de no existir el area de ventas en el mapa creamos un nuevo listado
                List<RQO_obj_relaciondeVentas__c> listRel = new List<RQO_obj_relaciondeVentas__c>();
                //	Obtenemos la traducción de la relación y actualizamos el relacion tipo con su traducción
                rv.RQO_fld_relacionTipo__c = mapRelacionTipo.containsKey(rv.RQO_fld_relacionTipo__c) ? mapRelacionTipo.get(rv.RQO_fld_relacionTipo__c) : rv.RQO_fld_relacionTipo__c;
                //	Introducimos la relación en la lista creada
                listRel.add(rv);       
                //	Introducimos la lista creada en el mapa de areas
                mapSuEquipo.put(tradOrganizacionVenta, listRel);                                
            }
        }  
    }
    
    /**
    * @creation date: 29/09/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Inserción en el listado de direcciones de facturación
    * @param: rv. información de la relación de venta
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void addDirFacturacion (RQO_obj_relaciondeVentas__c rv){
        
        //	Obtenemos el nombre del cliente perteneciente la relación
        String nombreCliente = generateDirCliente(rv.RQO_fld_cliente__r.Name, rv.RQO_fld_cliente__r.RQO_fld_name3__c, rv.RQO_fld_calleNumero__c, rv.RQO_fld_city__c, rv.RQO_fld_country__c); 
        
        
            
        Set<RQO_obj_relaciondeVentas__c> setDirEnvio = new Set<RQO_obj_relaciondeVentas__c>(listDirFacturacion);
        if (!setDirEnvio.contains(rv) && rv.RQO_fld_cliente__r.RQO_fld_idExterno__c != RQO_cls_Constantes.CLIENTE_EXCLUYENTE_0000070005 ){
            //	Introducimos en el listado de facturacion (para picklist) el código y el nombre del cliente
            listDirFacturacionSelect.add(new seleccionador(rv.RQO_fld_cliente__r.Id, nombreCliente));
            listDirFacturacion.add(rv);
        }        
    }
    
    /**
    * @creation date: 29/09/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Inserción en el listado de destinos de mercancías
    * @param: rv. información de la relación de venta
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void addDestinoMercancias (RQO_obj_relaciondeVentas__c rv){
        
        //	Obtenemos el nombre del cliente perteneciente la relación
        String nombreCliente = generateDirCliente(rv.RQO_fld_cliente__r.Name, rv.RQO_fld_cliente__r.RQO_fld_name3__c, rv.RQO_fld_calleNumero__c, rv.RQO_fld_city__c, rv.RQO_fld_country__c); 
        
        Set<String> setDirEnvio = new Set<String>(listDirEnvio);
        if (!setDirEnvio.contains(nombreCliente)){
            //	Introducimos en el listado de facturacion (para picklist) el código y el nombre del cliente
            listDirMercanciasSelect.add(new seleccionador(rv.RQO_fld_cliente__r.Id, nombreCliente));
            
            listDirMercancias.add(rv);
            listDirEnvio.add(nombreCliente);
        }
    }
    
    /**
    * @creation date: 29/09/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Inserción en el listado de cabeceras de grupo
    * @param: rv. información de la relación de venta
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void addCabeceraGrupo (RQO_obj_relaciondeVentas__c rv){
        listCabeceraGrupo.add(rv);
    }
    
    /**
    * @creation date: 29/09/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Comprobación si el cliente tiene responsable de stock
    * @param: 
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void hasResponsableStock (){
        if (!hasResponsableStock){
            hasResponsableStock = true;
        }
    }
    

}