/**
    *	@name: RQO_cls_GestionPedido_test
    *	@version: 1.0
    *	@creation date: 23/01/2018
    *	@author: Alvaro Alonso - Unisys
    *	@description: Clase de test para probar la funcionalidad del controlador APEX RQO_cls_GestionPedido_cc
    */
@IsTest
public class constanTest {
    
        /**
    * @creation date: 23/01/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparte entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserGestionPedido@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
    }
    

    
    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear la actualización de posiciones
    * @exception: 
    * @throws: 
    */    
    @IsTest
    static void testActualizarPosiciones(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            String respuesta = RQO_cls_CustomSettingUtil.getGigyaApiKeyAutorizados();
             System.assertEquals('', respuesta);
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear la actualización de posiciones
    * @exception: 
    * @throws: 
    */    
    @IsTest
    static void testGetGigyaUserKey(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            String respuesta = RQO_cls_CustomSettingUtil.GetGigyaUserKey();
             System.assertEquals('', respuesta);
            test.stopTest();
        }
    }

            /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear la actualización de posiciones
    * @exception: 
    * @throws: 
    */    
    @IsTest
    static void testGetGigyaSecret(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            String respuesta = RQO_cls_CustomSettingUtil.GetGigyaSecret();
             System.assertEquals('', respuesta);
            test.stopTest();
        }
    }
    
                /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear la actualización de posiciones
    * @exception: 
    * @throws: 
    */    
    @IsTest
    static void testGetSalesforceKey(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            String respuesta = RQO_cls_CustomSettingUtil.GetSalesforceKey();
             System.assertEquals('', respuesta);
            test.stopTest();
        }
    }
    
                    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear la actualización de posiciones
    * @exception: 
    * @throws: 
    */    
    @IsTest
    static void testGetSalesforceSecret(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            String respuesta = RQO_cls_CustomSettingUtil.GetSalesforceSecret();
             System.assertEquals('', respuesta);
            test.stopTest();
        }
    }

                    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear la actualización de posiciones
    * @exception: 
    * @throws: 
    */    
    @IsTest
    static void testGetSalesforceUser(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            String respuesta = RQO_cls_CustomSettingUtil.GetSalesforceUser();
             System.assertEquals('', respuesta);
            test.stopTest();
        }
    }
    
                        /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear la actualización de posiciones
    * @exception: 
    * @throws: 
    */    
    @IsTest
    static void testGetSalesforcePassword(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            String respuesta = RQO_cls_CustomSettingUtil.GetSalesforcePassword();
             System.assertEquals('', respuesta);
            test.stopTest();
        }
    }
    
    
                        /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description:	Método test para testear la actualización de posiciones
    * @exception: 
    * @throws: 
    */    
    @IsTest
    static void testgetSalesforceLoginUrl(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            String respuesta = RQO_cls_CustomSettingUtil.getSalesforceLoginUrl();
             System.assertEquals('', respuesta);
            test.stopTest();
        }
    }
    
    
    
    /*************************************************************************************************************************************/
    /*************************************************************************************************************************************/
        

}