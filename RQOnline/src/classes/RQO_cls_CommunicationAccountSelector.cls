/**
*   @name: RQO_cls_CommunicationAccountSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de obtención de CommunicationAccounts
*/
public with sharing class RQO_cls_CommunicationAccountSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_CommunicationAccountSelector';
	
    /**
    *   @name: RQO_cls_CommunicationAccountSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que devuelve la lista de campos de RQO_obj_communicationAccount__c
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_obj_communicationAccount__c.RQO_fld_account__c,
			RQO_obj_communicationAccount__c.RQO_fld_communication__c,
			RQO_obj_communicationAccount__c.Name };
    }

    /**
    *   @name: RQO_cls_CommunicationAccountSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que devuelve el sObjectType de RQO_obj_communicationAccount__c
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_communicationAccount__c.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_CommunicationAccountSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que devuelve nombre del campo mediante le que se ordenan los CommunicationAccounts
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public override String getOrderBy()
	{
		return 'RQO_fld_communication__c';
	}

    /**
    *   @name: RQO_cls_CommunicationAccountSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que devuelve una lista de communicationAccount mediante el conjunto de ids
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<RQO_obj_communicationAccount__c> selectByCommunicationId(Set<ID> idSet)
    {
        /**
        * Constantes
        */
        final String METHOD = 'selectByCommunicationId';
        
        string query = newQueryFactory().setCondition('RQO_fld_communication__c IN :idSet').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
	   	return (List<RQO_obj_communicationAccount__c>) Database.query(query);
    }
}