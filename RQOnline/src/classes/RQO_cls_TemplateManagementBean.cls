/**
 *	@name: RQO_cls_TemplateManagementBean
 *	@version: 1.0
 *	@creation date: 20/12/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de Plantillas.
 */
public class RQO_cls_TemplateManagementBean {
	
    public String identificador {get; set;}
    public String nombre {get; set;}
    public Integer gradosCount {get; set;}
    public Integer destinosCount {get; set;}
    public List<RQO_cls_TemplateManagementPositionBean> posicionPlantillaList {get; set;}
    
}