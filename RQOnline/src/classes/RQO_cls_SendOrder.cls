/**
*   @name: RQO_cls_SendOrder
*   @version: 1.0
*   @creation date: 29/09/2017
*   @author: Alvaro Alonso - Unisys
*   @description: Clase que gestiona el envío al servicio de SAP de los datos asociados a una solicitud de pedido 
*	@testClass: RQO_cls_SendOrderTest
*/
public without sharing class RQO_cls_SendOrder{
    
    private static final String CLASS_NAME = 'RQO_cls_SendOrder';
    private List<RQO_obj_logProcesos__c> listProcessLog;
    private RQO_obj_logProcesos__c objLog;
    
    private boolean esCancelacion = false;
    private boolean esCreacion = false;    
    private SendOrderCustomResponse customResp = null;
    private Map<Id, String> mapCancelacion = null;
    
    /**
    * @creation date: 29/09/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Método general que recupera y envía los datos de posiciones de solicitud a SAP ya sean cancelaciones o creaciones
    * @param: orderId tipo String, id de la solicitud
    * @param: listIdPositions tipo List<Id>, id de posiciones de solicitud
    * @param: processType tipo String, tipo de proceso (Creación o cancelación)
    * @return: 
    * @exception: 
    * @throws: 
    */
    public SendOrderCustomResponse sendPositions(Id orderId, List<Id> listIdPositions, String processType) {
        final String METHOD = 'sendPositions';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        customResp = new SendOrderCustomResponse(RQO_cls_Constantes.SOAP_COD_OK, RQO_cls_Constantes.SOAP_COD_OK);
        if (RQO_cls_Constantes.REQ_WS_PROCESSTYPE_CANCEL.equalsIgnoreCase(processType)){esCancelacion=true;}else{esCreacion=true;}
        //Inicializamos los objetos de log
        this.instanceInitalLog();
        //Recuperamos todos los datos necesarios a enviar a través de las posiciones de solicitud (asset) de la solicitud
        List<Asset> positions = [Select Id,  RQO_fld_cuRequest__c, RQO_fld_sendMode__c, RQO_fld_incoterm__c, RQO_fld_cuDeliveryDate__c, RQO_fld_cantidad__c,
                                      RQO_fld_direccionEnvio__c, RQO_fld_direccionEnvio__r.RQO_fld_idExterno__c,
                                 	  RQO_fld_numRefCliente__c, 
                                      RQO_fld_accBillingAddress__c, RQO_fld_accBillingAddress__r.RQO_fld_idExterno__c,
                                 	  RQO_fld_requestManagement__r.RQO_fld_tipoPedido__c, 
                                 	  RQO_fld_destinoFacturacion__c, RQO_fld_destinoFacturacion__r.RQO_fld_idExterno__c,
                                      AccountId, Account.RQO_fld_idExterno__c,
                                      ContactId, Contact.RQO_fld_idExterno__c,
                                      RQO_fld_grade__c, RQO_fld_grade__r.RQO_fld_sku__r.RQO_fld_sku__c,
                                      RQO_fld_requestManagement__c, RQO_fld_requestManagement__r.RQO_fld_stockManager__c,
                                      RQO_fld_requestManagement__r.RQO_fld_stockManager__r.RQO_fld_idExterno__c, 
                                      RQO_fld_requestManagement__r.RQO_fld_type__c,
                                      RQO_fld_requestManagement__r.RQO_fld_taxReceivingCountry__c,
                                 	  RQO_fld_requestManagement__r.name,
                                      RQO_fld_container__c, 
                                      RQO_fld_container__r.RQO_fld_codigo__c, 
                                 	  ParentId, RQO_fld_positionStatus__c
                                      from Asset where Id = : listIdPositions];
        
        if (positions != null && !positions.isEmpty()){
            try{
                //Si estamos en cancelación verificamos que efectivamente se pueda cancelar
                If (esCancelacion){this.verifyCancelledPositionStatus(positions);}
                //Llamamos al método que realiza la llamada
                System.debug('Vamos a llamar');
                RQO_cls_SAPServicesWS.ZqoMfGrabarSolSalesforceResponse_element response = this.doCallOut(positions);
                //Gestionamos la respuesta del servicio
                System.debug('Vamos a gestionar la respuesta');
                this.manageResponse(response, null, orderId);  
            }catch(System.CalloutException ex) {
                System.debug('Excepción CO: ' + ex.getStackTraceString());
                this.completeErrorResponse(Label.RQO_lbl_GenericExceptionMsg, ex.getMessage() + ' ' + ex.getStackTraceString());
            }catch(Exception ex){
                System.debug('Excepción: ' + ex.getStackTraceString());
                this.completeErrorResponse(Label.RQO_lbl_GenericExceptionMsg, ex.getMessage() + ' ' + ex.getStackTraceString());
            }
        }else{
            this.completeErrorResponse(Label.RQO_lbl_GenericExceptionMsg, null);
        }
        //Si estamos creando y ha funcionado la integración atualizamos la solicitud. 
        if (esCreacion && RQO_cls_Constantes.SOAP_COD_OK.equalsIgnoreCase(customResp.errorCode)){this.updateOrder(orderId);}
        //Actualizamos la lista de posiciones si la respuesta del servicio ha sido un ok
        if(RQO_cls_Constantes.SOAP_COD_OK.equalsIgnoreCase(customResp.errorCode)){this.updatePositionsData(positions);}
        RQO_cls_ProcessLog.generateLogData(listProcessLog);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');

        return customResp;
    }
    
    /**
    * @creation date: 24/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Método general que completa los datos de error tanto del log como de respuesta
    * @param: errorMessage tipo String, mensaje de error
    * @param: exceptionMessage tipo String, mensaje de excepción
    * @return: 
    * @exception: 
    * @throws: 
    */
    private void completeErrorResponse(String errorMessage, String exceptionMessage){
		final String METHOD = 'completeErrorResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
        customResp.errorMessage = errorMessage;
        RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, errorMessage, exceptionMessage);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    
    /**
    * @creation date: 24/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Método que realiza la llamada a SAP
    * @param: positions tipo List<Asset>, lista con los datos de posiciones de solicitud almacenados en Salesforce
    * @return: RQO_cls_SAPServicesWS.ZqoMfGrabarSolSalesforceResponse_element, objeto de respuesta del servicio
    * @exception: 
    * @throws: 
    */
    private RQO_cls_SAPServicesWS.ZqoMfGrabarSolSalesforceResponse_element doCallOut(List<Asset> positions){
		final String METHOD = 'doCallOut';        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        RQO_cls_SAPServicesWS.ZqoTtSalesforce solicitud = this.createRequest(positions);
        RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE objCall = new RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE();
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return objCall.ZqoMfGrabarSolSalesforce(solicitud, objLog);
    }

    /**
    * @creation date: 24/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Método general para cargar los datos de posiciones de solicitud en el objeto de request del web service
    * @param: positions tipo List<Asset>, lista de activos con las posiciones de la solicitud
    * @return: RQO_cls_SAPServicesWS.ZqoTtSalesforce, objeto que se manda en la request
    * @exception: 
    * @throws: 
    */
    private RQO_cls_SAPServicesWS.ZqoTtSalesforce createRequest(List<Asset> positions){
        final String METHOD = 'createRequest';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<RQO_cls_SAPServicesWS.ZqoStSalesforce> listItem = new List<RQO_cls_SAPServicesWS.ZqoStSalesforce>();
        RQO_cls_SAPServicesWS.ZqoStSalesforce objSoapOrder = null;
        DateTime dateTimeToday = Datetime.now();
        //Recorremos la lista de posiciones de solicitud
        for(Asset a : positions){
            objSoapOrder = new RQO_cls_SAPServicesWS.ZqoStSalesforce();
            //Cargamos la lista de items con los datos de cada una de las posiciones de solicitud
            this.getObjectSoapOrder(objSoapOrder, a, dateTimeToday.date(), dateTimeToday.time());
            listItem.add(objSoapOrder);
        }
        RQO_cls_SAPServicesWS.ZqoTtSalesforce requestDataSolicitud = new RQO_cls_SAPServicesWS.ZqoTtSalesforce();
        requestDataSolicitud.item = listItem;
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return requestDataSolicitud;
    }
    
    /**
    * @creation date: 24/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Método para cargar en la lista de objetos de la request los datos de las posiciones de solicitud
    * @param: objSoapOrder tipo RQO_cls_SAPServicesWS.ZqoStSalesforce, objeto de la lista a rellenar pasado por referencia
    * @param: datoActual tipo Asset, posición de solicitud sobre la que recoger los datos
    * @param fechaActual tipo Date
    * @param: tiempoActual tipo Time
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void getObjectSoapOrder(RQO_cls_SAPServicesWS.ZqoStSalesforce objSoapOrder, Asset datoActual, Date fechaActual, Time tiempoActual){
		final String METHOD = 'getObjectSoapOrder';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        objSoapOrder.Cliente = datoActual.Account.RQO_fld_idExterno__c; //External id cuenta (Id de Cliente en SAP)
       	objSoapOrder.Numsolic = datoActual.RQO_fld_requestManagement__c; //Id solicitud en Salesforce
        objSoapOrder.NumsolicUsu = (datoActual.RQO_fld_requestManagement__c != null) ? datoActual.RQO_fld_requestManagement__r.name : null; //Name solicitud en Salesforce
       	objSoapOrder.Possolic = datoActual.ID; //Id posición de solicitud en Salesforce (id del activo)
        objSoapOrder.Bstkd = datoActual.RQO_fld_numRefCliente__c; //Número de Compra o Número de pedido Cliente
        objSoapOrder.KunnrWe = (datoActual.RQO_fld_direccionEnvio__c != null) ? datoActual.RQO_fld_direccionEnvio__r.RQO_fld_idExterno__c : null; //Destino mercancias
        objSoapOrder.KunnrRe = (datoActual.RQO_fld_destinoFacturacion__c != null) ? datoActual.RQO_fld_destinoFacturacion__r.RQO_fld_idExterno__c : null; //Destino factura
        objSoapOrder.Waerk =  RQO_cls_Constantes.COD_CURRENCY_EURO;//Moneda (Por defecto en euros)
        String envase = '';
        String gradoMant = (datoActual.RQO_fld_grade__c != null) ? datoActual.RQO_fld_grade__r.RQO_fld_sku__r.RQO_fld_sku__c : null; //Grado (Nombre del grado);
        if (datoActual.RQO_fld_container__c != null){
            envase = datoActual.RQO_fld_container__r.RQO_fld_codigo__c;
            envase = (String.isNotEmpty(envase)) ? (envase.length() > 2) ? envase.substring(0, 2) : envase : ''; 
            gradoMant += envase;
        }
        objSoapOrder.Matnr = gradoMant;
        objSoapOrder.Envase =  envase; //Código de envase
        objSoapOrder.Kwmeng = datoActual.RQO_fld_cantidad__c; //Cantidad kg
        objSoapOrder.Inco1 = datoActual.RQO_fld_incoterm__c; //Inconterm(3)
        objSoapOrder.Vdatu = (datoActual.RQO_fld_cuDeliveryDate__c != null ) ? String.valueOf(datoActual.RQO_fld_cuDeliveryDate__c) : null; //Fecha de entrega seleccionada por el cliente
        objSoapOrder.Auart = (datoActual.RQO_fld_requestManagement__c !=null) ? datoActual.RQO_fld_requestManagement__r.RQO_fld_tipoPedido__c : null;//Tipo de pedido (Estandar/Consigna)
        objSoapOrder.StcegL = (datoActual.RQO_fld_requestManagement__c !=null) ? datoActual.RQO_fld_requestManagement__r.RQO_fld_taxReceivingCountry__c : null; //Pais receptor fiscal
        
        objSoapOrder.KunnrSb = (datoActual.RQO_fld_requestManagement__c !=null && datoActual.RQO_fld_requestManagement__r.RQO_fld_stockManager__c != null) 
                ?   datoActual.RQO_fld_requestManagement__r.RQO_fld_stockManager__r.RQO_fld_idExterno__c : null; //Responsable de stock
		objSoapOrder.Camion = RQO_cls_Constantes.REQ_WS_CAMION_SI;//Camion(S/N)
        objSoapOrder.Vsbed = datoActual.RQO_fld_sendMode__c; //Modo de envío
       	RQO_obj_parametrizacionIntegraciones__c serviceParam  = RQO_obj_parametrizacionIntegraciones__c.getInstance(RQO_cls_Constantes.WS_SAP_SERVICES);
        objSoapOrder.Usrenvio = (serviceParam !=null && String.isNotEmpty(serviceParam.RQO_fld_sendUserAux__c)) ? serviceParam.RQO_fld_sendUserAux__c : null;//Usuario envío
        
        objSoapOrder.Fechaenvio = String.valueOf(fechaActual); //Fecha actual
        objSoapOrder.Horaenvio = String.valueOf(tiempoActual).subString(0, 8); //Hora actual
        objSoapOrder.Usrcreacion = datoActual.ContactId;//Usuario creación
        objSoapOrder.Estado = (!esCancelacion) ? RQO_cls_Constantes.REQ_WS_CREATE_REQUEST_SAP : RQO_cls_Constantes.REQ_WS_CANCEL_REQUEST_SAP; //Creación o cancelación de ordenes de solicitud
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
    * @creation date: 17/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Instancia la lista de logs y el objeto log con los datos iniciales
    * @param:
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void instanceInitalLog(){
		final String METHOD = 'instanceInitalLog';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        if(listProcessLog == null){listProcessLog = new List<RQO_obj_logProcesos__c>();}
        String serviceName = (esCancelacion) ? RQO_cls_Constantes.SERVICE_SEND_CANCELED_POSITIONS : RQO_cls_Constantes.SERVICE_SEND_ORDER;
        objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_SAP, serviceName);
        listProcessLog.add(objLog);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
    * @creation date: 24/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Gestiona la respuesta del servicio y en función de esta actualiza el log y el estado de la solicitud
    * @param: response tipo RQO_cls_SAPServicesWS.ZqoMfGrabarSolSalesforceResponse_element. Respuesta del servicio
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void manageResponse(RQO_cls_SAPServicesWS.ZqoMfGrabarSolSalesforceResponse_element response, List<Asset> positions, Id orderId){
		final String METHOD = 'manageResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Verificamos que tenemos response con datos, en caso contrario será error genérico.
        if (response != null && response.EtReturn != null && response.EtReturn.item != null && !response.EtReturn.item.isEmpty()){
            this.manageGenericResponse(response.EtReturn.item);
        }else{
            customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = Label.RQO_lbl_GenericExceptionMsg;
        }
        //Cargamos los datos en el objeto log
        RQO_cls_ProcessLog.completeLog(objLog, customResp.errorCode, customResp.errorMessage, null);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }

	/**
    * @creation date: 11/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Gestion genérica del objeto de los servicio que retorna los mensajes de respuesta
    * @param: listObjMessResponse tipo RQO_cls_SAPServicesWS.Bapiret2. Lista de mensajes de respuesta
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void manageGenericResponse(List<RQO_cls_SAPServicesWS.Bapiret2> listObjMessResponse){
		final String METHOD = 'manageGenericResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        for(RQO_cls_SAPServicesWS.Bapiret2 objAux : listObjMessResponse){
            System.debug('ObjAux: ' + objAux);
            //Recuperamos el mensaje de error.
            if (String.isNotEmpty(objAux.Message)){
                customResp.errorMessage = (String.isNotEmpty(objAux.Type_x)) ? objAux.Type_x + RQO_cls_Constantes.COLON + objAux.Message : objAux.Message;
                if (String.isNotEmpty(objAux.MessageV1)){customResp.errorMessage = customResp.errorMessage.replace('&1', objAux.MessageV1);}
                if (String.isNotEmpty(objAux.MessageV2)){customResp.errorMessage = customResp.errorMessage.replace('&2', objAux.MessageV2);}
                if (String.isNotEmpty(objAux.MessageV3)){customResp.errorMessage = customResp.errorMessage.replace('&3', objAux.MessageV3);}
                if (String.isNotEmpty(objAux.MessageV4)){customResp.errorMessage = customResp.errorMessage.replace('&4', objAux.MessageV4);}
                customResp.errorMessage += RQO_cls_Constantes.SEMICOLON;
            }
            //La cancelación puede ser masiva y en ese caso en los mensajes de error nos devuelven aquellas posiciones de solicitud que no han funcionado por
            // no pasar las validaciones (por tener pedido generado por ejemplo). Aquellos ids que nos devuelvan no se actualizan. Se almacenan en un mapa y se 
            // comprueban en la actualización
            //Si estoy cancelando, devuelve un error y el número de error es 001 (no pasa la validación) guardamos en el mapa
            if (esCancelacion && RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR.equalsIgnoreCase(objAux.Type_x) 
                && RQO_cls_Constantes.REQ_WS_RESPONSE_INV_NO_NUMBER.equalsIgnoreCase(objAux.Number_x) && 
                String.isNotEmpty(objAux.MessageV1) && objAux.MessageV1 instanceof Id){
                    if (this.mapCancelacion == null){this.mapCancelacion = new Map<Id, String>();}
                    this.mapCancelacion.put(Id.valueOf(objAux.MessageV1), objAux.MessageV1);
			}else if (RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR.equalsIgnoreCase(objAux.Type_x)){
                customResp.errorCode = RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR;
                break;
            }
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }

    /**
    * @creation date: 24/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Actualiza la solicitud con el estado que se le pase como parámetro. También actualiza todas las poisiciones de solicitud a "Enviado" 
    * cuando le pasamos la lista rellena
    * @param: orderStatus tipo String. Estado en el que se quiere poner la solicitud
    * @return: void
    * @exception: 
    * @throws: 
    */
     private void updateOrder(Id orderId){
         final String METHOD = 'updateOrder';
         System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
         
         RQO_obj_requestManagement__c reqToUp = new RQO_obj_requestManagement__c(Id = orderId, RQO_fld_requestStatus__c = RQO_cls_Constantes.REQ_STATUS_SENT, RQO_fld_sentToQp0__c = true);
         update reqToUp;
         System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
    * @creation date: 24/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Actualiza la posiciones de solicitud
    * @param: orderStatus tipo String. Estado en el que se quiere poner la solicitud
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void updatePositionsData(List<Asset> positions){
        final String METHOD = 'updatePositionsData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        //Si tengo posiciones las actualizo con su estado
        Date fechaActual = Date.today();
        List<Asset> listaActualizar = null;
		boolean actualizar = false;
        for(Asset activo : positions){
			actualizar = false;
            
			//Si estoy cancelando y el id no existe en el mapa que marca que ha dado error en SAP lo pongo cancelado y actualizo
			if(esCancelacion && (this.mapCancelacion == null || this.mapCancelacion.isEmpty() || !this.mapCancelacion.containsKey(activo.Id))){
				System.debug('AA_Entra por aquí');
				activo.RQO_fld_positionStatus__c = RQO_cls_Constantes.REQ_STATUS_CANCEL;
				actualizar = true;
			}else if (!esCancelacion){ //Si no estoy cancelando, es decir, estoy mandando la solicitud a SAP se cambia la fecha de solicitud a la fecha actual y
                					//marca la posición como enviada
				activo.RQO_fld_positionRequestDate__c = fechaActual;
                activo.RQO_fld_positionStatus__c = RQO_cls_Constantes.REQ_STATUS_SENT;
				actualizar = true;
			}
			if (actualizar){ //Si es necesario actualizar se añade a la lista
				if (listaActualizar==null){listaActualizar = new List<Asset>();}
				listaActualizar.add(activo);
			}
			
        }
		if (listaActualizar != null && !listaActualizar.isEmpty()){update listaActualizar;}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
	/**
    * @creation date: 15/01/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método que verifica si la lista de posiciones de solicitud que pretenden cancelar aun están en un estado válido para la cancelación
    * @param: positions tipo List<Asset>. Posiciones de solicitud
    * @return: boolean
    * @exception: 
    * @throws: 
    */
    private Boolean verifyCancelledPositionStatus(List<Asset> positions){
        final String METHOD = 'verifyCancelledPositionStatus';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Boolean verificated = true;
        //Verificamos si alguna de las solicitud que se van a enviar a cancelar ya no son susceptibles de cancelación. Una solicitud no es susceptible de cierre si 
        //se ha generado pedido asociado o su estado es diferente de solicitado
        for (Asset a : positions){
            if (a.ParentId != null || !RQO_cls_Constantes.PED_COD_STATUS_SOLICITADO.equalsIgnoreCase(a.RQO_fld_positionStatus__c)){
                customResp = new SendOrderCustomResponse(RQO_cls_Constantes.SOAP_COD_NO_VERIFIED, RQO_cls_Constantes.SOAP_COD_NO_VERIFIED);
            }
        }
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return verificated;
    }
    
    
    /**
    * @creation date: 25/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Objeto wrapper para devolver los datos de la llamada al servicio
    * @param:
    * @return: void
    * @exception: 
    * @throws: 
    */
    public class SendOrderCustomResponse{
        public String errorCode{get;set;}
        public String errorMessage{get;set;}
        public SendOrderCustomResponse(String errorCode, String errorMessage){
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
        }
    }
}