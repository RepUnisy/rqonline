/**
*   @name: RQO_cls_Users_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_SurveyResponses.
*/
@isTest
public class RQO_cls_Users_test {
	/**
    *   @name: RQO_cls_Users_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    */
	@testSetup static void initialSetup() {
		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(u){
			RQO_cls_TestDataFactory.createCSGigya();
			RQO_cls_TestDataFactory.createCSSalesforce();
		}
	}
	
   	/**
    *   @name: RQO_cls_Users_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la actualización del idioma de un usuario.
    */
    private static testMethod void whenunregisterReturnsOk() {

        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserClienteRunAsJob@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(userRunAs){
            Boolean DidThrowException = false;
            
            String exceptionMessage = '';
            
            try {
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 'es',
                                                       'es_ES', 'Europe/Berlin', contact[0].Id);
                                                       
                List<User> users = new List<User>();
                users.add(u);
    
                test.startTest();
                Test.setMock(HttpCalloutMock.class, new RQO_cls_Users_test.MockHttpResponseGenerator());
                fflib_ISObjectUnitOfWork uow = RQO_cls_Application.UnitOfWork.newInstance();
                ((RQO_cls_Users) new RQO_cls_Users.Constructor().construct(users)).updateUser(uow);
                ((RQO_cls_Users) new RQO_cls_Users.Constructor().construct(users)).deactivate(uow);
                ((RQO_cls_Users) new RQO_cls_Users.Constructor().construct(users)).unregister();
                test.stopTest();
            }
            catch (Exception e) {
                exceptionMessage = e.getMessage();
                DidThrowException = true;
            }
            
            System.assertEquals(false,
                                DidThrowException, 
                                'Se ha generado una excepción no esperada. ' + exceptionMessage);
        }
    }
    
    /**
    *   @name: MockHttpResponseGenerator
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase de test que chequea mediante un mock una respuesta HTTP
    */
	public class MockHttpResponseGenerator implements HttpCalloutMock {
        /**
        *   @name: MockHttpResponseGenerator
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Método que implementa el método de la interfaz para gestionar respuestas
        */
	    public HTTPResponse respond(HTTPRequest req) {
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody(' {' +
							  '"results": [' +
							  '  {' +
							  '    "data": {' +
							  '      "Service": {' +
							  '        "Quimica": "true"' +
							  '      }' +
							  '    }' +
							  '  }],' +
							  '"objectsCount": 1,' +
							  '"totalCount": 1,' +
							  '"statusCode": 200,' +
							  '"errorCode": 0,' +
							  '"statusReason": "OK",' +
							  '"callId": "ed334ae1f5f0437a97fd9e03069f91d9",' +
							  '"time": "2018-02-28T09:39:06.972Z"' +
							'}');
	        res.setStatusCode(200);
	        return res;
	    }
	}
	
}