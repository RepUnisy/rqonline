/**
 *	@name: RQO_cls_CustomSettingUtil
 *	@version: 1.0
 *	@creation date: 08/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase de utilidad para obtener los registros de la configuracion en custom setting
 */
public class RQO_cls_CustomSettingUtil {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_CustomSettingUtil';
	
    /**
	 * Variables de clase
	 */
	private static RQO_obj_parametrizacionIntegraciones__c parametrizacionIntegraciones = null;
    private static RQO_cs_procesoPlanificado__c procesoPlanificado = null;
    private static Map<String, RQO_cs_parametrizacionObjBulk__c> parametrizacionObjBulk = null;
    private static RQO_cs_tiempoBorradoRegistros__c tiempoBorradoRegistros = null;
    private static Map<String, RQO_cs_parametrizacionRTAccount__c> parametrizacionRTAccount = null;
    private static Map<String, RQO_cs_sapRP2Locale__c> parametrizacionSAPRP2Locale = null;
    private static Map<String, String> mapSAPRP2Language = null;
    private static RQO_cs_envioNotificaciones__c envioNotificaciones = null;
    private static REP_cs_activacionTrigger__c parametrizacionTrigger = null;
    
    
	// ***** METODOS PUBLICOS ***** //
    
	/**
	* @creation date: 08/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de devolver los valores de la Custom Setting: 	Parametrizacion Integraciones
	* @return: RQO_obj_parametrizacionIntegraciones__c
	* @exception: 
	* @throws: 
	*/
	public static RQO_obj_parametrizacionIntegraciones__c getParametrizacionIntegraciones(String servicio) {
		if (parametrizacionIntegraciones == null) {
			parametrizacionIntegraciones = RQO_obj_parametrizacionIntegraciones__c.getValues(servicio);
		}
		return parametrizacionIntegraciones;
	}
    
    /**
	* @creation date: 27/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de devolver los valores de la Custom Setting: Procesos Planificados
	* @param: nombreJob		Nombre del job
	* @return: RQO_cs_procesoPlanificado__c
	* @exception: 
	* @throws: 
	*/
	public static RQO_cs_procesoPlanificado__c getProcesoPlanificado(String nombreJob) {
		if (procesoPlanificado == null) {
			procesoPlanificado = RQO_cs_procesoPlanificado__c.getValues(nombreJob);
		}
		return procesoPlanificado;
	}
    
	/**
	* @creation date: 08/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de devolver los valores de la Custom Setting: 	parametrizacion Obj Bulk
	* @return: Map<String, RQO_cs_parametrizacionObjBulk__c>
	* @exception: 
	* @throws: 
	*/
	public static Map<String, RQO_cs_parametrizacionObjBulk__c> getParametrizacionObjBulk() {
		if (parametrizacionObjBulk == null) {
			parametrizacionObjBulk = RQO_cs_parametrizacionObjBulk__c.getAll();
		}
		return parametrizacionObjBulk;
	}
    
	/**
	* @creation date: 21/12/2017
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de devolver los valores de la Custom Setting: 	Envío de notificaciones
	* @return: Map<String, RQO_cs_envioNotificaciones__c>
	* @exception: 
	* @throws: 
	*/
	public static RQO_cs_envioNotificaciones__c getParametrizacionEnvioNotificaciones(String tipo) {
        envioNotificaciones = RQO_cs_envioNotificaciones__c.getValues(tipo);
		return envioNotificaciones;
	}
    
    /**
	* @creation date: 27/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de devolver los valores de la Custom Setting: 	Tiempo Borrado Registros
	* @param: apli		Nombre de la aplicación
	* @return: RQO_cs_tiempoBorradoRegistros__c
	* @exception: 
	* @throws: 
	*/
	public static RQO_cs_tiempoBorradoRegistros__c getTiempoBorradoRegistros(String apli) {
		if (tiempoBorradoRegistros == null) {
			tiempoBorradoRegistros = RQO_cs_tiempoBorradoRegistros__c.getValues(apli);
		}
		return tiempoBorradoRegistros;
	}
    
	/**
	* @creation date: 04/10/2017
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de devolver los valores de la Custom Setting: 	Parametrización Record Type Account
	* @return: Map<String, RQO_cs_parametrizacionRTAccount__c>
	* @exception: 
	* @throws: 
	*/
	public static Map<String, RQO_cs_parametrizacionRTAccount__c> getParametrizacionRTAccount() {
		if (parametrizacionRTAccount == null) {
			parametrizacionRTAccount = RQO_cs_parametrizacionRTAccount__c.getAll();
		}
		return parametrizacionRTAccount;
	}
  	
    /**
	* @creation date: 15/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Metodo encargado de devolver los valores de la Custom Setting: 	Sap Docum RP2 Locale
	* @return: Map<String, RQO_cs_parametrizacionRTAccount__c>
	* @exception: 
	* @throws: 
	*/
	public static Map<String, RQO_cs_sapRP2Locale__c> getParametrizacionSAPRP2Locale() {
		if (parametrizacionSAPRP2Locale == null) {
			parametrizacionSAPRP2Locale = RQO_cs_sapRP2Locale__c.getAll();
		}
		return parametrizacionSAPRP2Locale;
	}
	
    /**
	* @creation date: 15/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Devuelve en un mapa el idioma de SAP (Name) y el idioma de Salesforce (RQO_fld_sfFieldLocale__c) de la Custom Setting: 	Sap Docum RP2 Locale
	* @return: Map<String, String>
	* @exception: 
	* @throws: 
	*/
	public static Map<String, String> getMapSAPRP2Language() {
        if (mapSAPRP2Language == null){
            mapSAPRP2Language = new Map<String, String>();
            if (parametrizacionSAPRP2Locale == null) {
                parametrizacionSAPRP2Locale = RQO_cs_sapRP2Locale__c.getAll();
            }
            RQO_cs_sapRP2Locale__c objLocale = null;
            for(String keyAux: parametrizacionSAPRP2Locale.keySet()){
                objLocale = parametrizacionSAPRP2Locale.get(keyAux);
                mapSAPRP2Language.put(objLocale.name, objLocale.RQO_fld_sfFieldLocale__c);
            }
        }
		return mapSAPRP2Language;
	}
	
	/**
	* @creation date: 15/12/2017
	* @author: Unisys
	* @description:	Devuelve la API key de Gigya relativa al servicio de Quimica, una vez desencriptada
	* @return: String
	* @exception: 
	* @throws: 
	*/
	public static string getGigyaApiKeyAutorizados ()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'getGigyaApiKeyAutorizados';
		
    	RQO_cs_gigyaIntegracion__c gigyaInt = RQO_cs_gigyaIntegracion__c.getInstance();
    	if (gigyaInt.RQO_fld_apiKeyAutorizados__c != null)
    	{
	    	Blob encryptedData = EncodingUtil.base64Decode(gigyaInt.RQO_fld_apiKeyAutorizados__c);
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' gigyaInt.RQO_fld_apiKeyAutorizados__c: ' + gigyaInt.RQO_fld_apiKeyAutorizados__c);
	    	
			RQO_cs_gigyaKeyEncriptacion__c gigyaKey = RQO_cs_gigyaKeyEncriptacion__c.getInstance();
			Blob cryptoKey = EncodingUtil.base64Decode(gigyaKey.RQO_fld_keyEncriptacion__c);
	    	
	    	return Crypto.decryptWithManagedIV('AES256', cryptoKey, encryptedData).toString();
    	}
    	else
    		return '';
    }
    
    /**
	* @creation date: 15/12/2017
	* @author: Unisys
	* @description:	Devuelve la user key de Gigya relativa al servicio de Quimica, una vez desencriptada
	* @return: String
	* @exception: 
	* @throws: 
	*/
	public static string GetGigyaUserKey ()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'GetGigyaUserKey';
		
    	RQO_cs_gigyaIntegracion__c gigyaInt = RQO_cs_gigyaIntegracion__c.getInstance();
    	if (gigyaInt.RQO_fld_userKey__c != null)
    	{
	    	Blob encryptedData = EncodingUtil.base64Decode(gigyaInt.RQO_fld_userKey__c);
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' gigyaInt.RQO_fld_userKey__c: ' + gigyaInt.RQO_fld_userKey__c);
	    	
			RQO_cs_gigyaKeyEncriptacion__c gigyaKey = RQO_cs_gigyaKeyEncriptacion__c.getInstance();
			Blob cryptoKey = EncodingUtil.base64Decode(gigyaKey.RQO_fld_keyEncriptacion__c);
	    	
	    	return Crypto.decryptWithManagedIV('AES256', cryptoKey, encryptedData).toString();
    	}
    	else
    		return '';
    }
    
    /**
	* @creation date: 15/12/2017
	* @author: Unisys
	* @description:	Devuelve la secret de Gigya relativa al servicio de Quimica, una vez desencriptada
	* @return: String
	* @exception: 
	* @throws: 
	*/
	public static string GetGigyaSecret ()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'GetGigyaSecret';
		
    	RQO_cs_gigyaIntegracion__c gigyaInt = RQO_cs_gigyaIntegracion__c.getInstance();
    	if (gigyaInt.RQO_fld_secret__c != null)
    	{
	    	Blob encryptedData = EncodingUtil.base64Decode(gigyaInt.RQO_fld_secret__c);
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' gigyaInt.RQO_fld_secret__c: ' + gigyaInt.RQO_fld_secret__c);
	    	
			RQO_cs_gigyaKeyEncriptacion__c gigyaKey = RQO_cs_gigyaKeyEncriptacion__c.getInstance();
			Blob cryptoKey = EncodingUtil.base64Decode(gigyaKey.RQO_fld_keyEncriptacion__c);
	    	
	    	return Crypto.decryptWithManagedIV('AES256', cryptoKey, encryptedData).toString();
    	}
    	else
    		return '';
    }
    
    /**
	* @creation date: 15/12/2017
	* @author: Unisys
	* @description:	Devuelve la api Key de Salesforce relativa a la connected app, una vez desencriptada
	* @return: String
	* @exception: 
	* @throws: 
	*/
	public static string GetSalesforceKey ()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'GetSalesforceKey';
		
		RQO_cs_salesforceIntegracion__c salesforceInt = RQO_cs_salesforceIntegracion__c.getInstance();
		if (salesforceInt.RQO_fld_apiKey__c != null)
		{
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' salesforceInt: ' + salesforceInt);
	    	Blob encryptedData = EncodingUtil.base64Decode(salesforceInt.RQO_fld_apiKey__c);
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' salesforceInt.RQO_fld_apiKey__c: ' + salesforceInt.RQO_fld_apiKey__c);
	    	
	    	RQO_cs_salesforceKeyEncriptacion__c salesforceKey = RQO_cs_salesforceKeyEncriptacion__c.getInstance();
			System.debug(CLASS_NAME + ' - ' + METHOD + ' salesforceKey: ' + salesforceKey);
			Blob cryptoKey = EncodingUtil.base64Decode(salesforceKey.RQO_fld_keyEncriptacion__c);
			System.debug(CLASS_NAME + ' - ' + METHOD + ' cryptoKey: ' + cryptoKey);

	    	return Crypto.decryptWithManagedIV('AES256', cryptoKey, encryptedData).toString();
		}
		else
			return '';
    }
    
    /**
	* @creation date: 15/12/2017
	* @author: Unisys
	* @description:	Devuelve la secret de Salesforce relativa a la connected app, una vez desencriptada
	* @return: String
	* @exception: 
	* @throws: 
	*/
	public static string GetSalesforceSecret ()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'GetSalesforceSecret';
		
		RQO_cs_salesforceIntegracion__c salesforceInt = RQO_cs_salesforceIntegracion__c.getInstance();
		if (salesforceInt.RQO_fld_secret__c != null)
		{
	    	Blob encryptedData = EncodingUtil.base64Decode(salesforceInt.RQO_fld_secret__c);
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' salesforceInt.RQO_fld_secret__c: ' + salesforceInt.RQO_fld_secret__c);
	    	
			RQO_cs_salesforceKeyEncriptacion__c salesforceKey = RQO_cs_salesforceKeyEncriptacion__c.getInstance();
			Blob cryptoKey = EncodingUtil.base64Decode(salesforceKey.RQO_fld_keyEncriptacion__c);
	    	
	    	return Crypto.decryptWithManagedIV('AES256', cryptoKey, encryptedData).toString();
		}
		else
			return '';
    }
    
    /**
	* @creation date: 15/12/2017
	* @author: Unisys
	* @description:	Devuelve el usuario de integraciión de Salesforce, una vez desencriptada
	* @return: String
	* @exception: 
	* @throws: 
	*/
	public static string GetSalesforceUser ()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'GetSalesforceUser';
		
		RQO_cs_salesforceIntegracion__c salesforceInt = RQO_cs_salesforceIntegracion__c.getInstance();
		if (salesforceInt.RQO_fld_user__c != null)
		{
	    	Blob encryptedData = EncodingUtil.base64Decode(salesforceInt.RQO_fld_user__c);
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' salesforceInt.RQO_fld_user__c: ' + salesforceInt.RQO_fld_user__c);
	    	
			RQO_cs_salesforceKeyEncriptacion__c salesforceKey = RQO_cs_salesforceKeyEncriptacion__c.getInstance();
			Blob cryptoKey = EncodingUtil.base64Decode(salesforceKey.RQO_fld_keyEncriptacion__c);
	    	
	    	return Crypto.decryptWithManagedIV('AES256', cryptoKey, encryptedData).toString();
		}
		else
			return '';
    }
    
    /**
	* @creation date: 15/12/2017
	* @author: Unisys
	* @description:	Devuelve la password de Salesforce, una vez desencriptada
	* @return: String
	* @exception: 
	* @throws: 
	*/
	public static string GetSalesforcePassword ()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'GetSalesforcePassword';
		
		RQO_cs_salesforceIntegracion__c salesforceInt = RQO_cs_salesforceIntegracion__c.getInstance();
		if (salesforceInt.RQO_fld_password__c != null)
		{
	    	Blob encryptedData = EncodingUtil.base64Decode(salesforceInt.RQO_fld_password__c);
	    	
	    	System.debug(CLASS_NAME + ' - ' + METHOD + ' salesforceInt.RQO_fld_password__c: ' + salesforceInt.RQO_fld_password__c);
	    	
			RQO_cs_salesforceKeyEncriptacion__c salesforceKey = RQO_cs_salesforceKeyEncriptacion__c.getInstance();
			Blob cryptoKey = EncodingUtil.base64Decode(salesforceKey.RQO_fld_keyEncriptacion__c);
	    	
	    	return Crypto.decryptWithManagedIV('AES256', cryptoKey, encryptedData).toString();
		}
		else
			return '';
    }
    
	/**
	* @creation date: 15/12/2017
	* @author: Unisys
	* @description:	Devuelve la url de la api de Salesforce
	* @return: String
	* @exception: 
	* @throws: 
	*/
	public static string getSalesforceLoginUrl ()
    {
    	// ***** CONSTANTES ***** //
		final String METHOD = 'getSalesforceLoginUrl';
		
		RQO_cs_salesforceIntegracion__c salesforceInt = RQO_cs_salesforceIntegracion__c.getInstance();
    	if (salesforceInt.RQO_fld_apiUrl__c != null)
    		return salesforceInt.RQO_fld_apiUrl__c;
    	else
    		return '';
    }
    
    /**
	* @creation date: 24/01/2018
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de devolver los valores de la Custom Setting: 	REP_cs_activacionTrigger__c
	* @param: apli		Nombre del trigger
	* @return: REP_cs_activacionTrigger__c
	* @exception: 
	* @throws: 
	*/
	public static REP_cs_activacionTrigger__c getParametrizacionTrigger(String triggerName) {
		if (parametrizacionTrigger == null) {
			parametrizacionTrigger = REP_cs_activacionTrigger__c.getValues(triggerName);
		}
		return parametrizacionTrigger;
	}
	
    /**
	* @creation date: 15/02/2018
	* @author: Alvaro Iglesias - Unisys
	* @description:	Metodo encargado de devolver los valores del Custom MetadataType: RQO_cmt_dateFilterData__mdt
	* @param: filterType tipo String, tipo del filtro
	* @return: RQO_cmt_dateFilterData__mdt
	* @exception: 
	* @throws: 
	*/
	public static RQO_cmt_dateFilterData__mdt getParametrizacionDateFilterData(String filterType) {
        final String METHOD = 'getParametrizacionDateFilterData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        RQO_cmt_dateFilterData__mdt dateData = null;
        //Recupero el registro para el tipo de filtro
        for(RQO_cmt_dateFilterData__mdt obj : [Select RQO_fld_type__c, RQO_fld_numeroMeses__c from RQO_cmt_dateFilterData__mdt 
                                               where RQO_fld_type__c = : filterType LIMIT 1]){
            dateData = obj;
        }    
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
		return dateData;
	}
    
	/**
	* @creation date: 15/02/2018
	* @author: Alvaro Iglesias - Unisys
	* @description:	Metodo encargado de devolver los valores del Custom MetadataType: RQO_cmt_dateFilterData__mdt
	* @return: RQO_cmt_dateFilterData__mdt
	* @exception: 
	* @throws: 
	*/
	public static RQO_cmt_dateFilterData__mdt getParametrizacionDateFilterData() {
        final String METHOD = 'getParametrizacionDateFilterData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        RQO_cmt_dateFilterData__mdt dateData = null;
        //Recupero el registro para el tipo de filtro
        for(RQO_cmt_dateFilterData__mdt obj : [Select RQO_fld_type__c, RQO_fld_numeroMeses__c from RQO_cmt_dateFilterData__mdt]){
            dateData = obj;
        }    
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
		return dateData;
	}
}