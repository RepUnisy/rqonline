/*------------------------------------------------------------------------
 * 
 *   @name:         DCPAI_cls_exportExcelController
 *   @version:       1 
 *   @creation date:   30/06/2017
 *   @author:       Alfonso Constán López  -  Unisys
 *   @description:     Test class to manage the coverage code for the class DCPAI_cls_exportExcel
 * 
----------------------------------------------------------------------------*/
public class DCPAI_cls_exportExcelController {
    
    private List<ITPM_UBC_Project__c> listUBCAlmacenado = new List<ITPM_UBC_Project__c>();
    private List<ITPM_Version__c> listUBCAlmacenadoVersion = new List<ITPM_Version__c>();
    private String idPlan = '';
    private String procedencia = '';
    private Map<String, List<ITPM_UBC_Project__c>> mapUBCObtenido = new Map<String, List<ITPM_UBC_Project__c>>();
    private Map<String, List<ITPM_Version__c>> mapUBCObtenidoVersion = new Map<String, List<ITPM_Version__c>>();
    private List<String> listCost = new List<String>();
    private List<String> listExpense = new List<String>();
    private List<String> listInvestment = new List<String>();
    
    
    public DCPAI_cls_exportExcelController(ApexPages.StandardController controller) {
        idPlan = ApexPages.currentPage().getParameters().get('id');
        procedencia = ApexPages.currentPage().getParameters().get('procedencia');
        if (procedencia == 'plan'){
            inicializarUBCs();
            inicializarCostItem();
        }
        else{
            inicializarUBCsVersion();
            inicializarCostItemVersion();
        }        
    }
    
    private void inicializarUBCs(){
        //  Obtenemos las UBCs
        List<ITPM_UBC_Project__c> listaUBC = [select id,
                                              name, 
                                              ITPM_SEL_Year__c, 
                                              ITPM_SEL_Delivery_Country__c, 
                                              ITPM_Percent__c, 
                                              ITPM_REL_UBC__r.Name, 
                                              ITPM_REL_UBC__r.ITPM_TX_Description__c,
                                              ITPM_FOR_Business_Area_Name__c,
                                              ITPM_FOR_Business_Subarea_Name__c,
                                              ITPM_FOR_Business_Unit_Name__c,
                                              ITPM_FOR_Business_Country_Name__c,
                                              DCPAI_fld_Code_of_project__c,
                                              DCPAI_fld_Budget_Investment__c, 
                                              DCPAI_fld_Budget_Investment__r.Name,
                                              DCPAI_fld_Budget_Investment__r.ITPM_BI_TX_Budget_Investment_name__c, 
                                              DCPAI_fld_Budget_Investment__r.ITPM_BIA_TXA_Description__c,
                                              DCPAI_fld_Budget_Investment__r.DCPAI_fld_Segmentation__c, 
                                              DCPAI_fld_Budget_Investment__r.ITPM_BIA_SEL_IT_Subarea__c, 
                                              DCPAI_fld_Budget_Investment__r.ITPM_BIA_SEL_IT_Area__c,
                                              DCPAI_fld_Budget_Investment__r.ITPM_BI_SEL_Responsible_IT_Direction__c,
                                              DCPAI_fld_Budget_Investment__r.ITPM_BIA_REL_Responsible__r.Name,
                                              DCPAI_fld_Budget_Investment__r.DCPAI_fld_Decision_Area__c,
                                              DCPAI_fld_Budget_Investment__r.DCPAI_fld_Program__r.Name
                                              from ITPM_UBC_Project__c where DCPAI_fld_Budget_Investment__c != null
                                              order by ITPM_SEL_Year__c, ITPM_SEL_Delivery_Country__c
                                             ];
        
        system.debug('Listado de UBCs ' + listaUBC);
        
        // Creamos un mapa para introducir las UBCs
         mapUBCObtenido = DCPAI_cls_GlobalClass.DCPAI_getMapUBCs(listaUBC);
       
    }    
    private void inicializarCostItem(){
        List<SObject> listBudget = new List<SObject>();
        system.debug('controlamos '); 
        system.debug('aaaaaaa ' + idPlan + Label.DCPAI_CostItem_Status_Assigned + idPlan);
        List<SObject> budgetDetailListObtenida = [Select ITPM_SEL_Country__c, ITPM_SEL_Year__c, 
                                                  DCPAI_fld_Investment_Nature__c, 
                                                  DCPAI_fld_Code__c,
                                                  DCPAI_fld_Budget_Investment__c,
                                                  SUM(ITPM_FOR_Total_investment__c) totalInvestment, 
                                                  SUM(ITPM_FOR_Total_expense__c) totalExpense,
                                                  DCPAI_fld_Waiting_to_Assign__c
                                                  from ITPM_Budget_Item__c where DCPAI_fld_Investment_Plan__c = :idPlan and DCPAI_fld_Assing_Status__c =: Label.DCPAI_CostItem_Status_Assigned and DCPAI_fld_Waiting_to_Assign__c = false 
                                                  GROUP BY DCPAI_fld_Budget_Investment__c, DCPAI_fld_Investment_Nature__c, ITPM_SEL_Country__c, ITPM_SEL_Year__c, DCPAI_fld_Waiting_to_Assign__c, DCPAI_fld_Code__c
                                                  ORDER BY DCPAI_fld_Budget_Investment__c ];
        
        system.debug('budgetDetailListObtenida ' + budgetDetailListObtenida);
        for (SObject bud : budgetDetailListObtenida ){
            String code = String.valueOf(bud.get('DCPAI_fld_Code__c'));
            if (code == null){
                code = '';
            }
            List<ITPM_UBC_Project__c> listUBC = mapUBCObtenido.get((String)bud.get('DCPAI_fld_Budget_Investment__c') + (String)bud.get('ITPM_SEL_Country__c') + (String)bud.get('ITPM_SEL_Year__c') + code);
            if (listUBC != null){
                
                for (ITPM_UBC_Project__c ubcAgrup : listUBC){
                    listUBCAlmacenado.add(ubcAgrup);
                    listBudget.add(bud);
                    calculate(bud, ubcAgrup, null);
                } 
            }
            else{
  //              listUBCAlmacenado.add(new ITPM_UBC_Project__c());
  //              listBudget.add(bud);
  //              calculate(bud, null, 1);
            }
            
        }
    }  
        
    private void inicializarUBCsVersion(){
        //  Obtenemos las UBCs
        List<ITPM_Version__c> listaUBC = [select id,
                                              name, 
                                              ITPM_VER_SEL_Year_validity__c, 
                                              ITPM_VER_SEL_Country__c, 
                                              DCPAI_fld_Percent__c, 
                                              DCPAI_fld_UBC_Name__c, 
                                              DCPAI_fld_Code_of_project__c,
                                              DCPAI_fld_Budget_Version_UBC__c, 
                                              DCPAI_fld_Budget_Version_UBC__r.Name,
                                              DCPAI_fld_Budget_Version_UBC__r.DCPAI_fld_Name__c, 
                                              DCPAI_fld_Budget_Version_UBC__r.ITPM_VER_TX_Description__c,
                                              DCPAI_fld_UBC_Description__c,
                                              DCPAI_fld_Budget_Version_UBC__r.DCPAI_fld_Segmentation__c, 
                                              DCPAI_fld_Budget_Version_UBC__r.ITPM_VER_SEL_Responsible_Subarea__c, 
                                              DCPAI_fld_Budget_Version_UBC__r.ITPM_VER_SEL_Responsible_Area__c,
                                              DCPAI_fld_Budget_Version_UBC__r.ITPM_VER_SEL_Responsible_IT_Direction__c,
                                              DCPAI_fld_Budget_Version_UBC__r.ITPM_VER_SEL_Responsible__r.Name,
                                              DCPAI_fld_Budget_Version_UBC__r.DCPAI_fld_Decision_Area__c,
                                              DCPAI_fld_Business_Area_Name__c,
                                              DCPAI_fld_Business_Subarea_Name__c,
                                              DCPAI_fld_Business_Unit_Name__c,
                                              DCPAI_fld_Business_Country__c
                                              from ITPM_Version__c where DCPAI_fld_Budget_Version_UBC__r.DCPAI_fld_Investment_Plan_Version__c != null      
                                             ];
        
        system.debug('Listado de UBCs ' + listaUBC);
        
        // Creamos un mapa para introducir las UBCs
         mapUBCObtenidoVersion = DCPAI_cls_GlobalClass.DCPAI_getMapUBCsVersion(listaUBC);
       
    }    
    private void inicializarCostItemVersion(){
      
        
        List<SObject> budgetDetailListObtenida = [Select ITPM_VER_SEL_Country__c, 
                                                  ITPM_VER_SEL_Year_validity__c,
                                                  DCPAI_fld_Code_of_project__c,
                                                  DCPAI_fld_Budget_Version_CostItem__c,
                                                  SUM(ITPM_VER_FOR_Total_investment__c) totalInvestment, 
                                                  SUM(ITPM_VER_FOR_Total_expense__c) totalExpense
                                                  from ITPM_Version__c where DCPAI_fld_Budget_Version_CostItem__r.DCPAI_fld_Investment_Plan_Version__c = :idPlan
                                                  GROUP BY DCPAI_fld_Budget_Version_CostItem__c, ITPM_VER_SEL_Country__c, ITPM_VER_SEL_Year_validity__c, DCPAI_fld_Code_of_project__c
                                                  ORDER BY DCPAI_fld_Budget_Version_CostItem__c ];
        
        for (SObject bud : budgetDetailListObtenida ){
            String code = String.valueOf(bud.get('DCPAI_fld_Code_of_project__c'));
            if (code == null){
                code = '';
            }
            List<ITPM_Version__c> listUBC = mapUBCObtenidoVersion.get((String)bud.get('DCPAI_fld_Budget_Version_CostItem__c') + (String)bud.get('ITPM_VER_SEL_Country__c') + (String)bud.get('ITPM_VER_SEL_Year_validity__c') + code);
            if (listUBC != null){
                for (ITPM_Version__c ubcAgrupVersion : listUBC){
                    listUBCAlmacenadoVersion.add(ubcAgrupVersion);
                    calculate(bud, null, ubcAgrupVersion);
                }
            }
        }
    }    
    
    public List<ITPM_UBC_Project__c> listUBCFinal{
        get{
            return listUBCAlmacenado;
        }
        set;
    }
    
    public List<ITPM_Version__c> listUBCFinalVersion{
        get{
            return listUBCAlmacenadoVersion;
        }
        set;
    }    
    
    public String title{
        get{
            return Label.DCPAI_Investment_Plan;  
        }
        set;
    }
    
    public List<String> listExpenseFinal{
        get{
            return listExpense;
        }
        set;
    }
    public List<String> listInvestmentFinal{
        get{
            return listInvestment;
        }
        set;
    }
    public List<String> listCostFinal{
        get{
            return listCost;
        }
        set;
    }

    private void calculate(SObject bud, ITPM_UBC_Project__c ubcAgrup, ITPM_Version__c ubcversion){
        
        Decimal costExpense = (Decimal)bud.get('totalExpense');
        Decimal costInvestment = (Decimal) bud.get('totalInvestment');
        Decimal costPro = costExpense + costInvestment;
        
        if(ubcversion == null){
            costExpense = costExpense * ubcAgrup.ITPM_Percent__c / 100;
            costInvestment = costInvestment * ubcAgrup.ITPM_Percent__c / 100;
            costPro = costPro * ubcAgrup.ITPM_Percent__c / 100;
        }
        else{
            costExpense = costExpense * Decimal.valueOf(ubcversion.DCPAI_fld_Percent__c) / 100;
            costInvestment = costInvestment * Decimal.valueOf(ubcversion.DCPAI_fld_Percent__c) / 100;
            costPro = costPro * Decimal.valueOf(ubcversion.DCPAI_fld_Percent__c) / 100;
        }
        
        listExpense.add(costExpense.format());
        listInvestment.add(costInvestment.format());
        listCost.add(costPro.format());
    }            
    
    public List<String> listaVacia {
        get{
            listaVacia = new List<String>();
            String elemento1 = 'elem1';
            listaVacia.add(elemento1);
            return listaVacia;
        }
        set;
    }    
}