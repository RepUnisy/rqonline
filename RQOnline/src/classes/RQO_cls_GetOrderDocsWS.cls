/**
 *	@name: RQO_cls_GetOrderDocsWS
 *	@version: 1.0
 *	@creation date: 29/11/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase genérica de acceso a las diferentes operaciones de los servicios SAP para obtener documentos asociados a un pedido
 *	@testClass: RQO_cls_GetOrderDocsWS_test
*/
public without sharing class RQO_cls_GetOrderDocsWS {
    
    private static final String CLASS_NAME = 'RQO_cls_GetOrderDocsWS';
    
    private List<RQO_obj_logProcesos__c> listProcessLog;
    private RQO_obj_logProcesos__c objLog;
    private OrderDocCustomResponse customResp = null;
	private String invoiceEspecificError = null;
	/**
    * @creation date: 29/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método para obtener el GUID de SAP de la factura solicitada
    * @param: cliente tipo String. Id externo del cliente
	* @param: fechaFactura tipo String. Fecha de la factura a buscar
	* @param: numFactura tipo String. Número de la factura
    * @return: OrderDocCustomResponse, objeto con los datos de respuesta
    * @exception: 
    * @throws: 
    */
    public OrderDocCustomResponse getInvoice(String cliente, String fechaFactura, String numFactura){
		final String METHOD = 'getInvoice';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        customResp = new OrderDocCustomResponse(RQO_cls_Constantes.SOAP_COD_OK, RQO_cls_Constantes.SOAP_COD_OK);
        //Inicializamos los objetos de log
		this.instanceInitalLog(RQO_cls_Constantes.SERVICE_GET_INVOICE);
        try{
            //Llamamos al método genérico que monta la request y realiza el callout
            RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element returnData = this.getInvoiceData(cliente, fechaFactura, numFactura);
            System.debug('Return data: ' + returnData);
            if(returnData != null){
                //Si tengo datos de vuelta llamamos al método de gestión de respuesta
                this.manageInvoiceResponse(returnData);
            }
        }catch(System.CalloutException ex) {
            System.debug('CallOut Exception: ' + ex.getMessage());
            customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = ex.getMessage();
            //Si da una excepción de tipo callout genero el log correspondiente
            RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage());
        }catch(Exception ex){
            System.debug('Exception: ' + ex.getMessage());
			customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = ex.getMessage();
            //Si da una excepción genérica genero el log correspondiente
            RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage());
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return customResp;
    }
    
	/**
	* @creation date: 29/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método genérico que monta la request y realiza el callout contra SAP parsa obtener la factura
    * @param: cliente tipo String. Id externo del cliente
	* @param: fechaFactura tipo String. Fecha de la factura a buscar
	* @param: numFactura tipo String. Número de la factura
	* @return: RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element
	* @exception: 
	* @throws: 
	*/
    private RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element getInvoiceData(String cliente, String fechaFactura, String numFactura){
		final String METHOD = 'getInvoiceData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String IvCodCliente = cliente;
        String IvFechaFactura = fechaFactura;
        String IvNumFactura = numFactura;
        RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE objCall = new RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE();
        //Realizamos el callout
        RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element responseData = objCall.ZqoConsultaFactura(IvCodCliente, IvFechaFactura, IvNumFactura, objLog);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return responseData;
        
    }
    
	/**
    * @creation date: 29/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Gestiona la respuesta del servicio de facturas y en función de esta actualiza el log 
    * @param: response tipo RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element. Respuesta del servicio
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void manageInvoiceResponse(RQO_cls_SAPServicesWS.ZqoConsultaFacturaResponse_element response){
		final String METHOD = 'manageInvoiceResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Boolean noData = false;
		//Verificamos que tenemos response con datos, en caso contrario será error genérico.
        if (response != null){
            if (response.EtReturn != null && response.EtReturn.item != null && !response.EtReturn.item.isEmpty()){
            	this.manageGenericResponse(response.EtReturn.item);
            }else{
                noData = true;
            }
            if (String.isNotEmpty(response.EvGuidDoc)){
                //Vamos a por la respuesta
                customResp.errorCode = RQO_cls_Constantes.SOAP_COD_OK; 
                customResp.docGuidData = response.EvGuidDoc;
                noData = false;
            }
            if (noData){customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NO_DATA; customResp.errorMessage = Label.RQO_lbl_documentumErrorSapNoResults;}
        }else{
            customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = Label.RQO_lbl_GenericExceptionMsg;
        }
        //Cargamos los datos en el objeto log
        RQO_cls_ProcessLog.completeLog(objLog, customResp.errorCode, customResp.errorMessage, null);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    
	/**
    * @creation date: 05/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método para obtener el documento del albarán
    * @param: cliente tipo String. Id externo del cliente
    * @return: OrderDocCustomResponse, objeto con los datos de respuesta
    * @exception: 
    * @throws: 
    */
    public OrderDocCustomResponse getDeliveryNote(String cliente, String numDocumento){
		final String METHOD = 'getDeliveryNote';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        customResp = new OrderDocCustomResponse(RQO_cls_Constantes.SOAP_COD_OK, RQO_cls_Constantes.SOAP_COD_OK);
        //Inicializamos los objetos de log
		this.instanceInitalLog(RQO_cls_Constantes.SERVICE_GET_DELIVERY_NOTE);
        try{
            System.debug('Entro');
            //Llamamos al método genérico que monta la request y realiza el callout
            RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element returnData = this.getDeliveryNoteData(cliente, numDocumento);
            if(returnData != null){
                //Si tengo datos de vuelta llamamos al método de gestión de respuesta
                this.manageDeliveryNoteResponse(returnData);
            }
        }catch(System.CalloutException ex) {
            System.debug('CallOut Exception: ' + ex.getMessage());
			customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = ex.getMessage();
            //Si da una excepción de tipo callout genero el log correspondiente
            RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage());
        }catch(Exception ex){
            System.debug('Exception: ' + ex.getMessage());
			customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = ex.getMessage();
            //Si da una excepción genérica genero el log correspondiente
            RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage());
        }
        RQO_cls_ProcessLog.generateLogData(listProcessLog);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return customResp;
    }
    
    
    
	/**
	* @creation date: 05/12/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método genérico que monta la request y realiza el callout contra SAP parsa obtener el albarán
    * @param: cliente tipo String. Id externo del cliente
	* @return: RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element
	* @exception: 
	* @throws: 
	*/
    private RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element getDeliveryNoteData(String cliente, String numDocumento){
		final String METHOD = 'getDeliveryNoteData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		RQO_cls_SAPServicesWS.ZqoScertalbcmr ivCalbcmr = new RQO_cls_SAPServicesWS.ZqoScertalbcmr();
		ivCalbcmr.Zvbeln = numDocumento;
        ivCalbcmr.Zkunnr = cliente;
        System.debug(ivCalbcmr.Zvbeln);
        System.debug(ivCalbcmr.Zkunnr);
        RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE objCall = new RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE();
        //Realizamos el callout
        RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element returnData = objCall.ZqoMfCertanalAlbcmr(IvCalbcmr, objLog);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return returnData;
    }
    
    
    /**
    * @creation date: 05/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Gestiona la respuesta del servicio de albarán y en función de esta actualiza el log 
    * @param: response tipo RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element. Respuesta del servicio
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void manageDeliveryNoteResponse(RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element response){
		final String METHOD = 'manageDeliveryNoteResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Verificamos que tenemos response con datos, en caso contrario será error genérico.
        if (response != null && response.EtReturn != null && response.EtReturn.item != null && !response.EtReturn.item.isEmpty()){
            this.manageGenericResponse(response.EtReturn.item);
        }else{
            customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = Label.RQO_lbl_GenericExceptionMsg;
        }
        if (RQO_cls_Constantes.SOAP_COD_OK.equalsIgnoreCase(customResp.errorCode) && response != null){
            //Vamos a por la respuesta
			this.manageDocDataResponse(response.ItablaIo);
        }
        //Cargamos los datos en el objeto log
        RQO_cls_ProcessLog.completeLog(objLog, customResp.errorCode, customResp.errorMessage, null);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }

    
    
    /**
    * @creation date: 05/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método para obtener el documento de cetificado de analisis
    * @param: cliente tipo String. Id externo del cliente
    * @return: OrderDocCustomResponse, objeto con los datos de respuesta
    * @exception: 
    * @throws: 
    */
    public OrderDocCustomResponse getAnilisysCertificate(String cliente, String numDocumento, String idioma){
		final String METHOD = 'getAnilisysCertificate';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        customResp = new OrderDocCustomResponse(RQO_cls_Constantes.SOAP_COD_OK, RQO_cls_Constantes.SOAP_COD_OK);
        //Inicializamos los objetos de log
		this.instanceInitalLog(RQO_cls_Constantes.SERVICE_GET_ANALYSIS_CETIFICATE);
        try{
            //Llamamos al método genérico que monta la request y realiza el callout
            RQO_cls_SAPServicesWS.ZqoMfCertificadoAnalisisResponse_element returnData = this.getAnalysisCertificateData(cliente, numDocumento, idioma);
            if(returnData != null){
                //Si tengo datos de vuelta llamamos al método de gestión de respuesta
                this.manageAnalysisCertificateResponse(returnData);
            }
        }catch(System.CalloutException ex) {
            System.debug('CallOut Exception: ' + ex.getMessage());
			customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = ex.getMessage();
            //Si da una excepción de tipo callout genero el log correspondiente
            RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage());
        }catch(Exception ex){
            System.debug('Exception: ' + ex.getMessage());
			customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = ex.getMessage();
            //Si da una excepción genérica genero el log correspondiente
            RQO_cls_ProcessLog.completeLog(objLog, RQO_cls_Constantes.SOAP_COD_NOK, Label.RQO_lbl_GenericExceptionMsg, ex.getMessage());
        }
        RQO_cls_ProcessLog.generateLogData(listProcessLog);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return customResp;
    }
    
    
    
	/**
	* @creation date: 05/12/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método genérico que monta la request y realiza el callout contra SAP parsa obtener el certificado de analisis
    * @param: cliente tipo String. Id externo del cliente
	* @return: RQO_cls_SAPServicesWS.ZqoMfCertificadoAnalisisResponse_element
	* @exception: 
	* @throws: 
	*/
    private RQO_cls_SAPServicesWS.ZqoMfCertificadoAnalisisResponse_element getAnalysisCertificateData(String cliente, String numDocumento, String idioma){
		final String METHOD = 'getAnalysisCertificateData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		RQO_cls_SAPServicesWS.ZqoScertanal ivCertanal = new RQO_cls_SAPServicesWS.ZqoScertanal();
		ivCertanal.Zvbeln = numDocumento; //Número de doc
        ivCertanal.Zspras = idioma; //Idioma
        ivCertanal.Zkunnr = cliente; //Cliente
        RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE objCall = new RQO_cls_SAPServicesWS.ZQO_BNDG_QONLINE();
        //Realizamos el callout
        RQO_cls_SAPServicesWS.ZqoMfCertificadoAnalisisResponse_element returnData = objCall.ZqoMfCertificadoAnalisis(ivCertanal, objLog);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return returnData;
    }
    
    
    /**
    * @creation date: 05/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Gestiona la respuesta del servicio de certificado de analisis y en función de esta actualiza el log 
    * @param: response tipo RQO_cls_SAPServicesWS.ZqoMfCertanalAlbcmrResponse_element. Respuesta del servicio
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void manageAnalysisCertificateResponse(RQO_cls_SAPServicesWS.ZqoMfCertificadoAnalisisResponse_element response){
		final String METHOD = 'manageAnalysisCertificateResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        //Verificamos que tenemos response con datos, en caso contrario será error genérico.
        if (response != null && response.EtReturn != null && response.EtReturn.item != null && !response.EtReturn.item.isEmpty()){
            this.manageGenericResponse(response.EtReturn.item);
        }else{
            customResp.errorCode = RQO_cls_Constantes.SOAP_COD_NOK;
            customResp.errorMessage = Label.RQO_lbl_GenericExceptionMsg;
        }
        if (RQO_cls_Constantes.SOAP_COD_OK.equalsIgnoreCase(customResp.errorCode) && response != null){
            //Vamos a por la respuesta
			this.manageDocDataResponse(response.ItablaIo);
        }
        //Cargamos los datos en el objeto log
        RQO_cls_ProcessLog.completeLog(objLog, customResp.errorCode, customResp.errorMessage, null);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }

	/**
    * @creation date: 05/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera los datos del documento de albarán y certificado de analisis en función de la respuesta del servicio que corresponda
    * @param: response tipo RQO_cls_SAPServicesWS.ZqoItablaio.
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void manageDocDataResponse(RQO_cls_SAPServicesWS.ZqoItablaio itablaIo){
		final String METHOD = 'manageDocDataResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        String docData = '';
        if (itablaIo !=null && itablaIo.item != null && !itablaIo.item.isEmpty()){
            for (RQO_cls_SAPServicesWS.Ytcar255 objYt255: itablaIo.item){
                docData += objYt255.Ycar255;
            }
			customResp.docData = docData;
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    
	/**
    * @creation date: 05/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Gestion genérica del objeto de los servicio que retorna los mensajes de respuesta
    * @param: listObjMessResponse tipo RQO_cls_SAPServicesWS.Bapiret2. Lista de mensajes de respuesta
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void manageGenericResponse(List<RQO_cls_SAPServicesWS.Bapiret2> listObjMessResponse){
		final String METHOD = 'manageGenericResponse';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        for(RQO_cls_SAPServicesWS.Bapiret2 objAux : listObjMessResponse){
            //Recuperamos el mensaje de error.
            if (String.isNotEmpty(objAux.Message)){
                customResp.errorMessage = (String.isNotEmpty(objAux.Type_x)) ? objAux.Type_x + RQO_cls_Constantes.COLON + objAux.Message : objAux.Message;
                if (String.isNotEmpty(objAux.MessageV1)){customResp.errorMessage = customResp.errorMessage.replace('&1', objAux.MessageV1);}
                if (String.isNotEmpty(objAux.MessageV2)){customResp.errorMessage = customResp.errorMessage.replace('&2', objAux.MessageV2);}
                if (String.isNotEmpty(objAux.MessageV3)){customResp.errorMessage = customResp.errorMessage.replace('&3', objAux.MessageV3);}
                if (String.isNotEmpty(objAux.MessageV4)){customResp.errorMessage = customResp.errorMessage.replace('&4', objAux.MessageV4);}
                customResp.errorMessage += RQO_cls_Constantes.SEMICOLON;
            }
            if (RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR.equalsIgnoreCase(objAux.Type_x)){
                customResp.errorCode = RQO_cls_Constantes.REQ_WS_RESPONSE_ERROR;
                customResp.errorNumber = objAux.Number_x;
                if (RQO_cls_Constantes.REQ_WS_RESPONSE_NO_DATA_ERROR.equalsIgnoreCase(objAux.Number_x)){
                    customResp.noDataError = true;
                }
                break;
            }
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
	/**
    * @creation date: 29/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Instancia la lista de logs y el objeto log con los datos iniciales
    * @param:
    * @return: void
    * @exception: 
    * @throws: 
    */
    private void instanceInitalLog(String service){
		final String METHOD = 'instanceInitalLog';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        if(listProcessLog == null){listProcessLog = new List<RQO_obj_logProcesos__c>();}
        objLog = RQO_cls_ProcessLog.instanceInitalLogService(RQO_cls_Constantes.LOG_ORIGIN_SALESFORCE, RQO_cls_Constantes.LOG_ORIGIN_SAP, service);
        listProcessLog.add(objLog);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
	/**
    * @creation date: 29/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description: Objeto wrapper para devolver los datos de la llamada al servicio
    * @param:
    * @return: void
    * @exception: 
    * @throws: 
    */
    public class OrderDocCustomResponse{
        public String errorCode{get;set;}
        public String errorMessage{get;set;}
        public String errorNumber{get;set;}
        public Boolean noDataError{get;set;}
        public String docGuidData{get;set;}
        public String docData{get;set;}
        
        public OrderDocCustomResponse(String errorCode, String errorMessage){
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
        }
    }
}