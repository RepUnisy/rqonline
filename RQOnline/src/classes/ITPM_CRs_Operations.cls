/*---------------------------------------------------------------------------
Author:         Javier Franco
Company:        Indra
Description:    Clase para distintas operaciones en relación con CRs.
Test Class:     ITPM_ChangeRequests_Test
History
<Date>          <Author>            <Change Description>
15-09-2016      Javier Franco       Versión inicial.
----------------------------------------------------------------------------*/

public class ITPM_CRs_Operations {
    
    /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Método que valida si se puede crear una CR de una categoría
                    dependiendo de las que ya existan abiertas.
    IN:             changeRequest: Objeto CR a insertar.
    OUT:            'OK' si todo es correcto o el error encontrado.
    History
    <Date>          <Author>            <Description>
    15-09-2016      Javier Franco       Creación del Método
    ----------------------------------------------------------------------------*/
    
    public static String validateInsertCR (ITPM_Change_Request__c changeRequest) {
        
        // Comprobamos la categoría del CR para evitar que existan 2 AP en el tiempo.
        
        Id apRecordTypeId = Schema.SObjectType.ITPM_Change_Request__c.getRecordTypeInfosByName().get('AP').getRecordTypeId();
        Id projectId = changeRequest.ITPM_REL_PROJECT__c;
        Id changeRequestId = changeRequest.Id;
        
        if (!changeRequest.RecordTypeId.equals(apRecordTypeId))
            return ('OK');
        
        Set<String> categoria = new Set<String>{'Time & Cost'};
        if (!String.isBlank(changeRequest.ITPM_SEL_Change_Subcategory__c)) {
            if (changeRequest.ITPM_SEL_Change_Subcategory__c.equals('Time & Cost')) {
                categoria.add('Time');
                categoria.add('Cost');
            }
            else
                categoria.add(changeRequest.ITPM_SEL_Change_Subcategory__c);
        }
        
        // Si existe algún CR en estado no finalizado de dicha subcategoría, bloqueamos la creación/modificación.
        
        List<ITPM_Change_Request__c> checkCRList = [
            SELECT Id, ITPM_REL_PROJECT__c, ITPM_SEL_Change_Subcategory__c, ITPM_SEL_Status__c
            FROM ITPM_Change_Request__c 
            WHERE 
                ITPM_SEL_Change_Subcategory__c IN :categoria
                AND ITPM_REL_PROJECT__c = :projectId
                AND ITPM_SEL_Status__c = 'Draft'
                AND Id <> :changeRequestId
                AND RecordTypeId = :apRecordTypeId
            LIMIT 1];
        
        if (checkCRList.size() > 0 && changeRequest.RecordTypeId.equals(apRecordTypeId))
            return ('There is another CR of the same subcategory in progress.');
        else
            return ('OK');
    }
    
    /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Método que valida si se puede crear/modificar/eliminar un BI.
    IN:             budgetItems: Budget items a insertar/modificar/eliminar.
    OUT:            'OK' si todo es correcto o el error encontrado.
    History
    <Date>          <Author>            <Description>
    15-09-2016      Javier Franco       Creación del Método
    13-10-2016      Javier Franco       Se comentan todas las validaciones.
    ----------------------------------------------------------------------------*/
    
    public static String validateAlterBIs (List<ITPM_Budget_Item__c> budgetItems) {
        
        // Comprobamos el estado de la CR si tiene asociado o bien si existe alguna
        // CR abierta para el proyecto actual.
        
        List <Id> idsCR = new List <Id>();
        List <Id> idsProyectos = new List <Id>();
        List <ITPM_Change_Request__c> listaCRs = new List <ITPM_Change_Request__c>();
        
        for (ITPM_Budget_Item__c bi : budgetItems) {
            if (!String.isBlank(bi.ITPM_REL_Change_Request__c))
                idsCR.add(bi.ITPM_REL_Change_Request__c);
            
            if (!String.isBlank(bi.ITPM_REL2_Project__c))
                idsProyectos.add(bi.ITPM_REL2_Project__c);
        }
        
        // Comprobamos si alguna de las CRs asociadas está terminada, en cuyo caso no 
        // se pueden modificar ningun Budget Item.
        /*
        if (idsCR.size() > 0) {
            listaCRs = [
                SELECT Id, ITPM_SEL_Status__c, ITPM_SEL_Change_Subcategory__c
                FROM ITPM_Change_Request__c
                WHERE 
                    Id IN :idsCR
                    AND ITPM_SEL_Change_Subcategory__c IN ('Cost', 'Time & Cost')
                    AND ITPM_SEL_Status__c <> 'Draft'
                LIMIT 1
            ];
            
            if (listaCRs.size() > 0)
                return ('You cannot alter any Cost Items associated to finished Change Requests.');
        }
        
        // Comprobamos si alguno de los proyectos tiene alguna CR en curso, en cuyo caso
        // no se pueden modificar ningún Budget Item del proyecto.
        
        Id apRecordTypeId = Schema.SObjectType.ITPM_Change_Request__c.getRecordTypeInfosByName().get('AP').getRecordTypeId();
        
        if (idsProyectos.size() > 0) {
            listaCRs = [
                SELECT Id, ITPM_SEL_Status__c, ITPM_SEL_Change_Subcategory__c
                FROM ITPM_Change_Request__c
                WHERE 
                    ITPM_REL_Project__c IN :idsProyectos
                    AND ITPM_SEL_Change_Subcategory__c IN ('Cost', 'Time & Cost')
                    AND ITPM_SEL_Status__c = 'Draft'
                    AND RecordTypeId = :apRecordTypeId
                LIMIT 1
            ];
            
            if (listaCRs.size() > 0)
                return ('You cannot alter Cost Items as there is an open Change Request involving costs in progress.');
        }
        */
        
        return ('OK');
    }
    
    /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Método que clona los Budget Items de un proyecto asociándolos
                    a una CR recién creada de tipo "Cost" o "Time & Cost".
    IN:             listaCRs: Lista de objetos CR a insertar y a los que se
                    asociarán los BIs del proyecto correspondiente.
    OUT:            Objetos Budget Items insertados en BD.
    History
    <Date>          <Author>            <Description>
    15-09-2016      Javier Franco       Creación del Método
    ----------------------------------------------------------------------------*/
    
    public static void cloneBudgetItems (List <ITPM_Change_Request__c> listaCRs) {
        
        // Recorremos la lista de CRs para copiar los BIs del proyecto correspondiente.
        Id apRecordTypeId = Schema.SObjectType.ITPM_Change_Request__c.getRecordTypeInfosByName().get('AP').getRecordTypeId();
        
        List <ITPM_Budget_Item__c> BIs = new List<ITPM_Budget_Item__c>();
        List <ITPM_Budget_Item__c> listaClones = new List<ITPM_Budget_Item__c>();
        ITPM_Budget_Item__c registroClon;
        
        for (ITPM_Change_Request__c cr : listaCRs) {
            // Si es de la categoría esperada, procedemos a crear los BIs clones de los del proyecto.
            if (cr.RecordTypeId.equals(apRecordTypeId) && 
                (cr.ITPM_SEL_Change_Subcategory__c.equals('Time & Cost') || cr.ITPM_SEL_Change_Subcategory__c.equals('Cost'))) {
                
                Id projectId = cr.ITPM_REL_PROJECT__c;
                Id budgetItemId = null;
                
                if (!String.isBlank(projectId)) {
                    
                    try {
                        // Se clonan todos los campos de los Budget Items para después introducir las modificaciones necesarias.
                        String strPlan = 'Plan';
                        BIs = Database.query(getCreatableFieldsSOQL('ITPM_Budget_Item__c', 'ITPM_REL2_Project__c = :projectId AND ITPM_CI_SEL_Version__c =: strPlan'));
                        for (ITPM_Budget_Item__c bi : BIs) {
                            budgetItemId = bi.Id;
                            
                            registroClon = bi.clone(false, true, true, true);
                            registroClon.ITPM_CHK_Historico__c = true;
                            registroClon.ITPM_REL_Change_Request__c = cr.Id;
                            registroClon.ITPM_REL2_Project__c = null;
                            registroClon.ITPM_EXT_Key__c = null;
                            listaClones.add(registroClon);
                        }
                    }
                    catch (Exception clonadoError) {
                        System.debug('@@@ Error al clonar Budget Item ' + budgetItemId + ' asociado al proyecto: ' + projectId + ': ' + clonadoError.getMessage());
                    }
                    
                } // FIN Hay proyecto asociado a la CR.
                
            } // FIN Comprobación de la categoría de la CR.
            
        } // FIN Recorrido de las CRs pasadas.
        
        // Si se ha creado algún BI, procedemos a insertarlos/actualizarlos en el sistema controlando posibles errores.
        
        try {
            if (listaClones.size() > 0)
                insert (listaClones);
        }
        catch (Exception err) {
            System.debug('@@@ Error al insertar los Budget Items asociados a Change Requests: ' + err.getMessage());
        }
    }
    
      /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Método que borra los Budget Items de CRs al producirse un
                    cambio en la categoría del mismo.
    IN:             listaCRs: Lista de objetos CR a procesar.
    OUT:            Objetos Budget Items borrados de BD.
    History
    <Date>          <Author>            <Description>
    06-10-2016      Javier Franco       Creación del Método
    ----------------------------------------------------------------------------*/  
    
    public static void deleteBudgetItems (List <ITPM_Change_Request__c> listaCRs) {
        
        List <Id> idsCRs = new List <Id>();
        for (ITPM_Change_Request__c cr : listaCRs) {
            if (cr.ITPM_SEL_Change_Subcategory__c.equals('Time'))
                idsCRs.add(cr.Id);
        }
        
        List <ITPM_Budget_Item__c> BIs = [
            SELECT Id, ITPM_BLN_Clone__c
            FROM ITPM_Budget_Item__c 
            WHERE ITPM_REL_Change_Request__c IN :idsCRs
        ];
        
        for (ITPM_Budget_Item__c markDelete : BIs) {
            markDelete.ITPM_BLN_Clone__c = true;
        }
        
        if (BIs != null) {
            if (!BIs.isEmpty()) {
                delete(BIs);
            }
        }
    }
    
    /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Método para generar la consulta con todos los campos
                    de un objeto para poder clonarlo.
    IN:             objectName:        Nombre del objeto SFDC.
                    whereClause:       Clausula WHERE para filtrar la consulta.
    OUT:            Cadena con la query a realizar.
    History
    <Date>          <Author>            <Description>
    15-09-2016      Javier Franco       Creación del Método
    ----------------------------------------------------------------------------*/
    
    private static String getCreatableFieldsSOQL(String objectName, String whereClause) {
        
        String selects = '';
        if (String.isBlank(whereClause))
            return null;
        
        // Get a map of field name and field token
        Map <String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list <String> selectFields = new list <String>();
        
        if (fMap != null) {
            for (Schema.SObjectField ft : fMap.values()) { // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                
                if (fd.isCreateable() || fd.isUpdateable()) {
                    if (!fd.getName().equals('ITPM_REL_Project__c') && !fd.getName().equals('ITPM_REL_Change_Request__c'))
                        selectFields.add(fd.getName());
                }
            }
        }
        
        if (!selectFields.isEmpty()) {
            for (String s: selectFields)
                selects += s + ',';
            
            if (selects.endsWith(',')) 
                selects = selects.substring(0, selects.lastIndexOf(','));
        }
        
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
    }       
       
}