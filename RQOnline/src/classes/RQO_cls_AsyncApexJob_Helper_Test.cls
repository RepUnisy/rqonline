/**
*	@name: RQO_cls_AsyncApexJob_Helper_Test
*	@version: 1.0
*	@creation date: 19/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar los métodos asociados a RQO_cls_FacturasGeneral
*/

@isTest
public class RQO_cls_AsyncApexJob_Helper_Test {
    /**
    *	@name: RQO_cls_AsyncApexJob_Helper_Test
    *	@version: 1.0
    *	@creation date: 19/2/2018
    *	@author: Juan Elías - Unisys
    *	@description: Método initial setup
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        
    }
    
    /**
*	@creation date: 19/2/2018
*	@author: Juan Elías - Unisys
*   @description: Método test para probar el método getActiveBatch()
*   @exception: 
*   @throws: 
*/
    
    @isTest
    static void testGetActiveBatch(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            
            Integer esperado = [SELECT Count() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status IN('Holding','Queued','Preparing','Processing')];
            test.startTest();
            Integer resultado = RQO_cls_AsyncApexJob_Helper.getActiveBatch();
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
*	@creation date: 19/2/2018
*	@author: Juan Elías - Unisys
*   @description: Método test para probar el método getActiveJobs()
*   @exception: 
*   @throws: 
*/
    
    @isTest
    static void testGetActiveJobs(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            
            Integer esperado = [SELECT Count() FROM AsyncApexJob WHERE JobType = 'Queueable' AND Status IN('Holding','Queued','Preparing','Processing')];
            test.startTest();
            Integer resultado = RQO_cls_AsyncApexJob_Helper.getActiveJobs();
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
*	@creation date: 19/2/2018
*	@author: Juan Elías - Unisys
*   @description: Método test para probar el método isBatchActive()
*   @exception: 
*   @throws: 
*/
    
    @isTest
    static void testIsBatchActive(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            
            Boolean esperado = false;
            test.startTest();
            Boolean resultado = RQO_cls_AsyncApexJob_Helper.isBatchActive('Assert');
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
}