/*------------------------------------------------------------------------
Author:         Borja Matín Ballesteros
Company:        Indra
Description:    trigger
History
<Date>          <Author>                      <Description>
13-Jul-2016     Borja Matín Ballesteros       Initial version
----------------------------------------------------------------------------*/
public class ITPM_Trigger_Logic {    
    public ITPM_Trigger_Logic() {}    
    //Lógica del Trigger de la clase Milestone History
    public static List<ITPM_Milestone_History__c> ITPM_Milestone_History_Tr_Logic(List<ITPM_Milestone_History__c> lstmlhist, ITPM_Milestone_History__c mlhis){
        lstmlhist = [SELECT Id, ITPM_REL_Project__r.Name 
                                            FROM ITPM_Milestone_History__c  
                                            WHERE ITPM_REL_Project__c =: mlhis.ITPM_REL_Project__c
                                            AND ITPM_DT_Date__c =: mlhis.ITPM_DT_Date__c];             
        
        
        ITPM_Project__c project = [SELECT Name FROM ITPM_Project__c WHERE ID =: mlhis.ITPM_REL_Project__c LIMIT 1];
        mlHis.Name = project.Name  + '-' + mlhis.ITPM_DT_Date__c.format();
        return lstmlhist;
    }          
}