/*------------------------------------------------------------
Author:        	Rubén Simarro
Company:       	Indra
Description:   	
Clase encargada de enviar un email al equipo de patrocinio notificando los programas pendientes de aprobación
Test Class:    	PT_test_scheduled_NotiPendAproba
History
<Date>      	<Author>     	<Change Description>
18-04-2016	    Rubén Simarro 	Initial Version
------------------------------------------------------------*/
global class PT_scheduled_NotificarPendientesAprob implements Schedulable {

    global void execute(SchedulableContext SC) {
               
        String mensajeLog=' ';
        
        try{                        
             mensajeLog = mensajeLog+'\n@@ '+Datetime.now().format()+' Proceso automático notificación programas pendientes aprobación @@';   
             
            Patrocinios__c[] patrocinios = [select Id, name, nombre__c, Fecha_limite_de_aprobacion2__c, estado__c 
                                             From Patrocinios__c 
                                             where estado__c in ('Programa no aprobado','Inicial')];
            
            for(Patrocinios__c patAux: patrocinios ){
            
                mensajeLog = mensajeLog+'\n--------------------------------------------------';                      
                mensajeLog = mensajeLog+'\nId Patrocinio: '+ patAux.Name+ '\nNombre del programa: '+patAux.Nombre__c+' \nFecha límite de aprobación: '+patAux.Fecha_limite_de_aprobacion2__c+' \nEstado: '+patAux.estado__c;   
                mensajeLog = mensajeLog+'\nlink acceso: '+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+patAux.id;
            }                
        }      
        catch(Exception e){                           
            mensajeLog = mensajeLog+'\n@@@ ERROR clase PT_scheduled_NotificarPendientesAprob: '+e.getMessage();          
        }  
         system.debug(mensajeLog);      

        PT_button_enviarMail.enviarEmail('Notificación programas pendientes aprobación', mensajeLog);
    }  
}