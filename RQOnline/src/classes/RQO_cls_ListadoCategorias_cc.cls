/**
 *	@name: RQO_cls_ListadoCategorias_cc
 *	@version: 1.0
 *	@creation date: 05/09/2017
 *	@author: Nicolás García - Unisys
 *	@description: Clase que se utiliza para la obtención de las Categorías en el catálogo
 *	@testClass: RQO_cls_ListadoCategorias_test
*/
public with sharing class RQO_cls_ListadoCategorias_cc {
    /**
	* @creation date: 05/09/2017
	* @author: Nicolás García - Unisys
	* @description:	Método para obtener la información de las categorías desde el Catálogo
	* @param: 'lenguaje' en el que queremos obtener las traducciones de las Categorías
    * @return: Mapa que contiene como clave la traducción de la categoría y como valor la información de la categoría
	* @exception: 
	* @throws: 
	*/
    @AuraEnabled
    public static String getMapCategorias(String lenguaje) {
        Map<String, List<RQO_obj_translation__c> > mapCategorias = new Map<String, List<RQO_obj_translation__c> >();
        try{
		system.debug('Entra en la clase: RQO_cls_ListadoCategorias');
        System.debug('lenguaje: ' + lenguaje);
        
        mapCategorias = new Map<String, List<RQO_obj_translation__c> >();
        Map<Id, String> mapInfoTraduccionesPadres = new Map<Id, String>();
        
        for(RQO_obj_translation__c categoria : [SELECT Id, RQO_fld_translation__c, RQO_fld_category__r.Id FROM RQO_obj_translation__c WHERE RQO_fld_locale__c = :lenguaje AND RQO_fld_category__c IN :[SELECT Id FROM RQO_obj_category__c WHERE RQO_fld_type__c = :Label.RQO_lbl_productKey AND RQO_fld_parentCategory__c = ''] ORDER BY RQO_fld_category__r.RQO_fld_sortCategories__c DESC]){
            mapCategorias.put(categoria.RQO_fld_translation__c, new List<RQO_obj_translation__c> ());
            System.debug('***primerFOR*****mapCategorias: ' + mapCategorias);
            mapInfoTraduccionesPadres.put(categoria.RQO_fld_category__r.Id, categoria.RQO_fld_translation__c);
            System.debug('***PrimerFOR*****mapInfoTraduccionesPadres: ' + mapInfoTraduccionesPadres);

        }
        
        for(RQO_obj_translation__c categoria : [SELECT Id, RQO_fld_translation__c, RQO_fld_category__r.RQO_fld_parentCategory__r.Id, RQO_fld_category__r.RQO_fld_imagenCategoria__c FROM RQO_obj_translation__c WHERE RQO_fld_locale__c = :lenguaje AND RQO_fld_category__c IN :[SELECT Id FROM RQO_obj_category__c WHERE RQO_fld_type__c = :Label.RQO_lbl_productKey AND RQO_fld_parentCategory__c != ''] ORDER BY RQO_fld_category__r.RQO_fld_sortCategories__c]){
                            System.debug('********mapCategorias: ' + mapCategorias);
                System.debug('********mapInfoTraduccionesPadres: ' + mapInfoTraduccionesPadres);
            System.debug('********RQO_fld_parentCategory__r.Id: ' + categoria.RQO_fld_category__r.RQO_fld_parentCategory__r.Id);
            if(mapCategorias.containsKey(mapInfoTraduccionesPadres.get(categoria.RQO_fld_category__r.RQO_fld_parentCategory__r.Id))){

                System.debug('Contiene al padre en el mapa.');
                mapCategorias.get(mapInfoTraduccionesPadres.get(categoria.RQO_fld_category__r.RQO_fld_parentCategory__r.Id)).add(categoria);
            }
        }          
        System.debug('mapCategorias: ' + mapCategorias);
        }catch(Exception ex){
            return ex.getMessage();
        }
        return JSON.serialize(mapCategorias);
    }   
}