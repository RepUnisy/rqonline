/**
*   @name: RQO_cls_InformationRequests
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: David Iglesias - Unisys
*   @description: Bean de integración de correos
*/
public class RQO_cls_IntegrationEmailBean {

    // ***** PROPIEDADES ***** //
	public String templateName { get; set; }
	public String contact { get; set; }
	public String idObject { get; set; }
    
}