/**
*	@name: RQO_cls_GradoIntermedio_test
*	@version: 1.0
*	@creation date: 14/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar la migración de grados
*/
@IsTest
public class RQO_cls_GradoIntermedio_test {

    /**
	* @creation date: 14/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup static void initialSetup() {
        
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserGrado@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createGradointermedio(200);
            RQO_cls_TestDataFactory.createOrganizaciondeVentas(1);		
            RQO_cls_TestDataFactory.createContainerJunction(1);
        }
    }
    
    /**
	* @creation date: 25/10/2017
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta en la migración de grados
	* @exception: 
	* @throws: 
	*/
    static testMethod void crearGradoRelacionesOK(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            test.startTest();
            //Lanzamiento del proceso
			Id batchJobId = Database.executeBatch(new RQO_cls_GradoIntermedio_batch(), 2000);
            test.stopTest();
            
			List <RQO_obj_qp0Grade__c> qp0GradeList = [SELECT Id FROM RQO_obj_qp0Grade__c];
            
			// Comprobaciones
			List<AsyncApexJob> jobs = [SELECT Status FROM AsyncApexJob WHERE Id = :batchJobId];
			System.assertEquals('Completed', jobs[0].Status);
			System.assertEquals(201, qp0GradeList.size());
        }
	}
	
	/**
	* @creation date: 20/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta en la migración de grados con container Junction existentes
	* @exception: 
	* @throws: 
	*/
    static testMethod void crearGradoOrgVentasKO(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            test.startTest();
			// Preparación de datos especificos
			RQO_obj_gradoIntermedio__c gradoIntermedio = [SELECT id FROM RQO_obj_gradoIntermedio__c LIMIT 1];
			gradoIntermedio.RQO_fld_organizaciondeVentas__c = '';
			update gradoIntermedio;
			
            //Lanzamiento del proceso
			Id batchJobId = Database.executeBatch(new RQO_cls_GradoIntermedio_batch(), 2000);
            test.stopTest();
            
			List <RQO_obj_logProcesos__c> logList = [SELECT Id FROM RQO_obj_logProcesos__c];
            
			// Comprobaciones
			List<AsyncApexJob> jobs = [SELECT Status FROM AsyncApexJob WHERE Id = :batchJobId];
			System.assertEquals('Completed', jobs[0].Status);
			System.assertEquals(1, logList.size());
        }
	}
    
    /**
	* @creation date: 20/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad erronea con generación de log al no existir organización de ventas
	* @exception: 
	* @throws: 
	*/
    static testMethod void crearGradoRelacionesJunctionOK(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            test.startTest();
			// Preparación de datos especificos
			RQO_obj_qp0Grade__c qp0Grade = [SELECT id FROM RQO_obj_qp0Grade__c LIMIT 1];
			qp0Grade.Name = 'test0';
			update qp0Grade;
			
            //Lanzamiento del proceso
			Id batchJobId = Database.executeBatch(new RQO_cls_GradoIntermedio_batch(), 2000);
            test.stopTest();
            
			List <RQO_obj_qp0Grade__c> qp0GradeList = [SELECT Id FROM RQO_obj_qp0Grade__c];
            
			// Comprobaciones
			List<AsyncApexJob> jobs = [SELECT Status FROM AsyncApexJob WHERE Id = :batchJobId];
			System.assertEquals('Completed', jobs[0].Status);
			System.assertEquals(201, qp0GradeList.size());
        }
	}
        
}