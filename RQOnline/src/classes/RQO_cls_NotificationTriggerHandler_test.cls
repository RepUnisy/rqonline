/**
*	@name: RQO_cls_NotificationTriggerHandler_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase RQO_cls_NotificationTriggerHandler
*/
@isTest
public class RQO_cls_NotificationTriggerHandler_test {

	/**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserNotifications@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
           RQO_cls_TestDataFactory.createNotification(6);
        }
    }
    
	/**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para comprobar el método createEvent
    * @exception: 
    * @throws:  
    */
    @isTest
    static void beforeInsertExecution(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserEventsPortal@testorg.com');
        System.runAs(u){
            RQO_obj_notification__c objNotif = [Select RQO_fld_contact__c,RQO_fld_mailSent__c, RQO_fld_visualized__c,RQO_fld_messageType__c, RQO_fld_objectType__c
                                                from RQO_obj_notification__c LIMIT 1];
            test.startTest();
            
            	RQO_cls_NotificationTriggerHandler notification = RQO_cls_NotificationTriggerHandler.getInstance();
            	notification.beforeInsertExecution(new List<RQO_obj_notification__c>{objNotif});
            
            test.stopTest();
            objNotif = [Select RQO_fld_contact__c,RQO_fld_mailSent__c, RQO_fld_visualized__c,RQO_fld_messageType__c, RQO_fld_objectType__c
                                                from RQO_obj_notification__c LIMIT 1];
            System.assert(!objNotif.RQO_fld_visualized__c);
        }
    }
    
	/**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para comprobar el método afterInsertExecution
    * @exception: 
    * @throws:  
    */
    @isTest
    static void afterInsertExecution(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserEventsPortal@testorg.com');
        System.runAs(u){
            Account cuenta = [Select Id, RQO_fld_idExterno__c from Account LIMIT 1];
            List<RQO_obj_notification__c> listaNotificacion = [Select RQO_fld_contact__c,RQO_fld_mailSent__c, RQO_fld_visualized__c,RQO_fld_messageType__c, RQO_fld_objectType__c
                                                from RQO_obj_notification__c LIMIT 6];
            //Acualizamos cada registro con un tipo de tipo cuenta
            listaNotificacion[0].RQO_fld_messageType__c = RQO_cls_Constantes.COD_EQU_COMERCIAL;
            listaNotificacion[1].RQO_fld_messageType__c = RQO_cls_Constantes.COD_EQU_GESTOR_COMERCIAL;
            listaNotificacion[2].RQO_fld_messageType__c = RQO_cls_Constantes.COD_EQU_ATENCION_COMERCIAL;
            listaNotificacion[3].RQO_fld_messageType__c = RQO_cls_Constantes.COD_FAC_NEW;
            listaNotificacion[4].RQO_fld_messageType__c = RQO_cls_Constantes.COD_FAC_PROX_VENCIMIENTO;
            listaNotificacion[5].RQO_fld_messageType__c = RQO_cls_Constantes.COD_FAC_VENCIDA;
            for (Integer i = 0; i<listaNotificacion.size(); i++){
                listaNotificacion[i].RQO_fld_contact__c = cuenta.RQO_fld_idExterno__c;
            }
            update listaNotificacion;
            test.startTest();
            
            	RQO_cls_NotificationTriggerHandler notification = RQO_cls_NotificationTriggerHandler.getInstance();
            	notification.afterInsertExecution(listaNotificacion);
            
            test.stopTest();
            Contact contact = [Select Id from Contact LIMIT 1];
            List<RQO_obj_notification__c> listaNotific = [Select Id from RQO_obj_notification__c where RQO_fld_contact__c =:contact.Id LIMIT 1];
            System.assert(listaNotific != null);
            System.assert(!listaNotific.isEmpty());
        }
    }
    
}