/**
*   @name: RQO_cls_NotificationConsentSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la selección de organizaciones
*/
public class RQO_cls_OrganizationSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_CommunicationsSelector';
	
	/**
	*   @name: RQO_cls_NotificationConsentSelector
	*   @version: 1.0
	*   @creation date: 31/10/2017
	*   @author: Alfonso Constan López - Unisys
	*   @description: Método que obtiene la lista de campos de un objeto Organization
	*/
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			Organization.Name,
			Organization.Id,
			Organization.OrganizationType,
			Organization.InstanceName,
			Organization.IsSandbox };
    }

	/**
	*   @name: RQO_cls_NotificationConsentSelector
	*   @version: 1.0
	*   @creation date: 31/10/2017
	*   @author: Alfonso Constan López - Unisys
	*   @description: Método que obtiene la lista de campos de un objeto Organization
	*/
    public Schema.SObjectType getSObjectType()
    {
        return Organization.sObjectType;
    }
    
   	/**
	*   @name: RQO_cls_NotificationConsentSelector
	*   @version: 1.0
	*   @creation date: 31/10/2017
	*   @author: Alfonso Constan López - Unisys
	*   @description: Método que obtiene el campo por el cual ordernar las organizaciones
	*/
    public override String getOrderBy()
	{
		return 'OrganizationType';
	}

	/**
	*   @name: RQO_cls_NotificationConsentSelector
	*   @version: 1.0
	*   @creation date: 31/10/2017
	*   @author: Alfonso Constan López - Unisys
	*   @description: Método que obtene toda la lista de notificaciones
	*/
    public List<Organization> selectAll()
    {
    	return (List<Organization>) Database.query(newQueryFactory().toSOQL());
    }
}