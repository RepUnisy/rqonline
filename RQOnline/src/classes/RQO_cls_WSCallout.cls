/**
 *	@name: RQO_cls_WSCallout
 *	@version: 1.0
 *	@creation date: 07/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase para preparar y realizar las llamadas a servicios web externos
 */
public class RQO_cls_WSCallout {

	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_WSCallout';
	private static final String DEFAULT_METHOD = 'GET';
	private static final String HEADER_PARAM_ACCEPT_NAME = 'accept';
	private static final String HEADER_PARAM_CONTENT_TYPE_NAME = 'content-type';
	private static final String HEADER_PARAM_JSON_VALUE = 'application/json';
	private static final String HEADER_PARAM_ENTORNO_NAME = 'entorno';

	// ***** PROPIEDADES ***** //
	public String interfaz { get; set; }
	public String url { get; set; }
	public String method { get; set; }
	public String body { get; set; }
	public Map<String, String> header { get; set; }

	// ***** CONSTRUCTORES ***** //
	/**
	* @creation date: 07/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Constructor para inicializar la URL
	* @param: String	URL
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_WSCallout(String url) {
		this.url = url;
		this.method = DEFAULT_METHOD;
		this.header = new Map<String, String>();
		this.body = '';
	}	

	/**
	* @creation date: 07/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Constructor para inicializar la URL, metodo y contenido
	* @param: String	URL
	* @param: String	Metodo
	* @param: String	Contenido
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_WSCallout(String url, String method, String body) {
		this(url);
		if ( method != null && !method.equals('') ) {
			this.method = method;
		}
		this.body = body;
	}

	/**
	* @creation date: 07/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Constructor para incluir url, metodo y contenido
	* @param: String	Metodo de envio
	* @param: AP_WSCalloutConfigBean	Configuracion de la llamada
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_WSCallout(String url, String method, String body, Map<String, String> header) {
		this(url, method, body);
		this.header = header;
	}

	// ***** METODO PUBLICOS ***** //

	/**
	* @creation date: 07/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Realiza una peticion simple y devuelve la respuesta obtenida 
	* @param: 
	* @return: RQO_cls_WSCalloutResponseBean	Resultado de la peticion (respuesta sincrona)
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_WSCalloutResponseBean send() {
		/**
		 * Constantes
		 */
		final String METHOD = 'send';

		/**
		 * Variables
		 */
		RQO_cls_WSCalloutResultBean result;
		RQO_cls_WSCalloutResponseBean calloutResponse = new RQO_cls_WSCalloutResponseBean();
		HttpResponse response;
		HttpRequest request;
		Http h;
		Datetime dtRequest;
		Datetime dtResponse;
		String status;
		String strRequestBody;
		String strResponseBody;
		RQO_cls_WSCalloutResult internalResult;

		/**
		 * Inicio
		 */
		System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

		// Inicializacion del resultado
		result = new RQO_cls_WSCalloutResultBean();

		try {
			response = null;

			// Inicializacion del estado
			status = RQO_cls_Constantes.PETICION_ESTADO_PENDIENTE;

			/*
			 * Inicializar y preparar la peticion
			 */
            System.debug(CLASS_NAME + ' - ' + METHOD + ': Configurando la peticion');
			request = new HttpRequest();
            System.debug(CLASS_NAME + ' - ' + METHOD + ': URL: ' + this.url);
			request.setEndpoint(this.url);
            System.debug(CLASS_NAME + ' - ' + METHOD + ': Method: ' + this.method);
			request.setMethod(this.method);
			if (!String.isBlank(this.body))
			{
	            System.debug(CLASS_NAME + ' - ' + METHOD + ': Body: ' + this.body);
				request.setBody(this.body);
			}
			//System.debug(CLASS_NAME + ' - ' + METHOD + ': Timeout: ' + RQO_cls_CustomSettingUtil.getParametrizacionIntegraciones(RQO_cls_Constantes.WS_PED_SAP).RQO_fld_Timeout__c);
            //request.setTimeout((RQO_cls_CustomSettingUtil.getParametrizacionIntegraciones(RQO_cls_Constantes.WS_PED_SAP).RQO_fld_Timeout__c).intValue());

			/*
			 * Parametros de la cabecera
			 */
			// Parametros Repsol
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Configurando la cabecera');
			for ( String paramName : header.keySet() ) {
				System.debug(CLASS_NAME + ' - ' + METHOD + ': (E) ' + paramName + ':' + header.get(paramName));
                request.setHeader(paramName, header.get(paramName));
			}

			/*
			 * Ejecutar callout
			 */
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Realizando la llamada');
			try {
				h = new Http();
				dtRequest = System.now();
				response = h.send(request);
				dtResponse = System.now();

				result.statusCode = response.getStatusCode();

				System.debug(CLASS_NAME + ' - ' + METHOD + ': Response status: ' + response.getStatusCode());
                System.debug(CLASS_NAME + ' - ' + METHOD + ': Response body: ' + response.getBody());

				// Comprobar estado respuesta
				if (response.getStatusCode() == RQO_cls_Constantes.REST_COD_HTTP_OK ||
					response.getStatusCode() == RQO_cls_Constantes.REST_COD_HTTP_CREATED ) {
                    System.debug(CLASS_NAME + ' - ' + METHOD + ': Comunicacion OK');

					// Transforma el contenido del body
					calloutResponse.estado = String.valueOf(response.getStatusCode());
					calloutResponse.msgError = response.getStatus();
					calloutResponse.contenido = response.getBody();
					
					// Preparar resultado
					result.resultOK = true;
					result.code = calloutResponse.estado;
					result.errorMessage = calloutResponse.msgError;
					
				} else {
                    System.debug(CLASS_NAME + ' - ' + METHOD + ': Error en la comunicacion');
						
					// Preparar resultado
					status = RQO_cls_Constantes.PETICION_ESTADO_ERROR_COMUNICACION;
					result.resultOK = false;
					result.code = RQO_cls_Constantes.PETICION_RESULTADO_ERR_HTTP;
					result.errorMessage = 'Error HTTP ' + response.getStatusCode();
				}
			} catch (CalloutException e) {
                System.debug(CLASS_NAME + ' - ' + METHOD + ': Error en la llamada al servicio REST. Message: ' + e.getMessage());

				if ( dtResponse == null ) {
					dtResponse = System.now();
				}

				// Preparar resultado
				status = RQO_cls_Constantes.PETICION_ESTADO_ERROR_COMUNICACION;
				result.resultOK = false;
				if(e.getMessage().equals('Read timed out')) {
					result.errorMessage = 'Timeout producido';
					result.code = RQO_cls_Constantes.PETICION_RESULTADO_ERR_TIME_OUT;
				} else {
					result.errorMessage = e.getMessage();
					result.code = RQO_cls_Constantes.PETICION_RESULTADO_ERR_CALLOUT;
				}
			}
		} catch (Exception e) {
            System.debug(CLASS_NAME + ' - ' + METHOD + ': ERROR -> Line: ' + e.getLineNumber() + ' | Message: ' + e.getMessage());

			// Preparar resultdo
			result.resultOK = false;
			result.errorMessage = e.getMessage();
			result.code = RQO_cls_Constantes.PETICION_RESULTADO_ERR_EXCEPTION;
			status = RQO_cls_Constantes.PETICION_ESTADO_ERROR_RESULTADO;
		}

		// Preprando resultado para su almacenamiento posterior
		internalResult = new RQO_cls_WSCalloutResult();
		internalResult.dtRequest = dtRequest == null ? System.now() : dtRequest;
		internalResult.dtResponse = dtResponse;
		internalResult.status = status;
		internalResult.host = url;
		internalResult.header = header;
		internalResult.requestMessage = request.getBody();
		internalResult.responseMessage = (response != null) ? response.getBody() : null;
		internalResult.result = result;
		
		// Se procesan los resultados
		// Se inserta el log para cada uno de los resultados
		//AP_LogServicioWebUtil.recordLog(internalResult.dtRequest, internalResult.dtResponse, internalResult.status, internalResult.header,
		//								internalResult.requestMessage, internalResult.responseMessage, internalResult.result, internalResult.host);

		// Se envia el email en caso de error
		if ( !internalResult.result.resultOk ) {
			sendError(internalResult);
		}

		System.debug(CLASS_NAME + RQO_cls_Constantes.HYPHEN + METHOD + RQO_cls_Constantes.COLON + ' FIN');
		return calloutResponse;
	}

	// ***** METODOS PRIVADOS ***** //

    /**
	* @creation date: 07/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de enviar un email notificando de un error 
	* @param: RQO_cls_WScalloutResult
	* @return: 
	* @exception: 
	* @throws: 
	*/
	private void sendError(RQO_cls_WScalloutResult result) {
		
		/**
		 * Constantes
		 */
		final String METHOD = 'sendError';
		
		/**
		 * Variables
		 */
		RQO_cls_IntegrationEmailBean emailData;

		/**
		 * Inicio
		 */
		System.debug(CLASS_NAME + RQO_cls_Constantes.HYPHEN + METHOD + RQO_cls_Constantes.COLON + ' INICIO');

		// Preparar datos
		//emailData = new RQO_cls_IntegrationEmailBean();
		//emailData.environment = RQO_cls_CustomSettingUtil.getParametrizacionIntegraciones(RQO_cls_Constantes.WS_PED_SAP).RQO_fld_Entorno__c;
		//if ( result.result.code.equals(RQO_cls_Constantes.PETICION_RESULTADO_ERR_HTTP) ||
		//     result.result.code.equals(RQO_cls_Constantes.PETICION_RESULTADO_ERR_TIME_OUT) ||
		//	 result.result.code.equals(RQO_cls_Constantes.PETICION_RESULTADO_ERR_CALLOUT) ) {
		//	emailData.type = RQO_cls_Constantes.ERR_CONNECTION;
		//} else {
		//	emailData.type = RQO_cls_Constantes.ERR_MESSAGE;
		//}
		//emailData.requestDate = result.dtRequest.format('dd/MM/yyyy HH:mm:ss');
		//emailData.requestHeader = result.header;
		//emailData.requestBody = result.requestMessage;
		//emailData.responseDate = result.dtResponse != null ? result.dtResponse.format('dd/MM/yyyy HH:mm:ss') : null;
		//emailData.responseHeader = null;
		//emailData.responseBody = result.responseMessage;

		// Enviar email
		//AP_IntegrationEmail.sendError(emailData);
		
		System.debug(CLASS_NAME + RQO_cls_Constantes.HYPHEN + METHOD + RQO_cls_Constantes.COLON + ' FIN');
	}
}