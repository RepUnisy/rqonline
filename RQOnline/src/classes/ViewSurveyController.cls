/* Controller associated with pages rendering the survey.
* Used by SurveyPage, ResultsPage, TakeSurvey
*/
global virtual with sharing class ViewSurveyController {
    public String  qQuestion                      {get; set;}
    public Boolean qRequired                      {get; set;}
    public String  qChoices                       {get; set;}
    public String surveyName                        {get; set;}
    public String surveyHeader                {get; set;}
    public String surveyId                          {get; 
                                                     set{
                                                         this.surveyId = value;
                                                         init();
                                                     }
                                                    }   
    public String renderSurveyPreview           {get; set;}  
    public String questionName                    {get; set;}  
    public String questionType                    {get; set;}
    public Boolean questionRequired             {get; set;}
    public List<SelectOption> singleOptions   {get; set;} 
    public List<question> allQuestions        {get; set;}
    public List<question> allQuestionsFinal        {get; set;}
    public List<String> responses                   {get; set;}
    public Integer allQuestionsSize               {get; set;}
    public String  templateURL                {get; set;}
    public String  surveyThankYouText         {get; set;}
    public String surveyContainerCss {get; set;}
    public String  surveyThankYouURL          {get; set;}
    public String  caseId                     {get; set;}
    public String  esId                     {get; set;} //anãdido para referencia con ES
    public String contactId {get;set;}
    public String anonymousAnswer {get;set;}
    public List<SelectOption> anonymousOrUser {get;set;}
    public Boolean isInternal {get;set;}
    public String baseURL {get;set;}
    
    public String userId{get;set;}
    public String userName{get;set;}
    public String surveyTakerId {get;set;}
    public Boolean thankYouRendered{get;set;}
    public List<String> newOrder {get;set;}
    
    //pagination [DD]28/10/2016
    // public integer lastPage{get;set;}
    
    public Integer pageNumber  ;
    public integer counter = 0;  //keeps track of the offset
    
    public integer list_size=10; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list   
    
    
    /* Retrieves the list of questions, the survey name, after retrieving the 
necessary parameters from the url.
*/
    //------------------------------------------------------------------------------// 
    public ViewSurveyController(ApexPages.StandardController stdController) {
                    System.debug(':::::::::::::::::::::::::stdController');
        // Get url parameters
        surveyId = Apexpages.currentPage().getParameters().get('id');
        caseId   = Apexpages.currentPage().getParameters().get('caId');
        contactId = Apexpages.currentPage().getParameters().get('cId'); 
        esId = Apexpages.currentPage().getParameters().get('esId');  //anãdido para referencia con ES
        
        
        if(caseId ==null || caseId.length()<15){
            caseId = 'none';
        }
        if(contactId ==null || contactId.length()<15){
            contactId = 'none';
        }           
        // By default the preview is not showing up
        renderSurveyPreview = 'false';
        
        //[DD]28102016 Pagination 
        total_size = [select count() from Survey_Question__c where Survey__c=:surveyId ]; // set the total size in the constructor
        
        init();
    } 
    
    public ViewSurveyController(viewShareSurveyComponentController controller)
    {
                    System.debug(':::::::::::::::::::::::::ViewSurveyController');
        surveyId = Apexpages.currentPage().getParameters().get('id');
        caseId   = Apexpages.currentPage().getParameters().get('caId');
        contactId = Apexpages.currentPage().getParameters().get('cId'); 
        if(caseId ==null || caseId.length()<15){
            caseId = 'none';
        }
        if(contactId ==null || contactId.length()<15){
            contactId = 'none';
        }       
        // By default the preview is not showing up
        renderSurveyPreview = 'false';
        init();
    }
    
    
    public void init()
    {
                    System.debug(':::::::::::::::::::::::::init');
        if (surveyId != null){ 
            // Retrieve all necessary information to be displayed on the page
            allQuestions = new List<question>();
            getAQuestionFinal();
            setupQuestionList();
            setSurveyNameAndThankYou(surveyId);
            anonymousOrUser = new List<SelectOption>();
            //   anonymousOrUser.add(new SelectOption('Anonymous',System.Label.LABS_SF_Anonymous));
            anonymousOrUser.add(new SelectOption('User','User ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName()));
            anonymousAnswer = 'User';
            
            
            isInternal =true;
            newOrder = new List<String>();
            String urlBase = URL.getSalesforceBaseUrl().toExternalForm();
            baseURL = urlBase;
            
            userId = UserInfo.getUserId();
            userName = UserInfo.getName();
            
            String profileId = UserInfo.getProfileId();
            try
            {
                Profile p = [select Id, UserType from Profile where Id=:profileId];
                if (p.UserType == 'Guest')
                {
                    isInternal = false;
                }
                else
                {
                    isInternal = true;
                }
            }
            catch (Exception e){
                isInternal = false;
            }
            
            
            thankYouRendered=false;
            
            
        }       
    }
    
    
    
    
    
    //----------------------------------------------------------------------------//    
    
    /* Called during the setup of the page. 
Retrieve questions and responses from DB and inserts them in 2 lists. */
    public Integer setupQuestionList(){
                    System.debug(':::::::::::::::::::::::::setupQuestionList');
        
        getAQuestion();
        
        return allQuestions.size();
    }
    
    
    /** Sets the survey's name variable
*  param: sID   The survey ID as specified in the DB
*/
    public void setSurveyNameAndThankYou(String sId){
                    System.debug(':::::::::::::::::::::::::setSurveyNameAndThankYou');
        Survey__c s = [SELECT Name, Id, URL__c, Thank_You_Text__c, thankYouText__c, thankYouLink__c, Survey_Header__c, Survey_Container_CSS__c FROM Survey__c WHERE Id =:sId];
        surveyName = s.Name;
        surveyHeader = s.Survey_Header__c;
        templateURL = s.URL__c+'id='+sId;//+'&cId={!Contact.Id}'+'&caId='+'{!Case.id}';
        surveyThankYouText = s.Thank_You_Text__c;
        if (surveyThankYouText == null)
        {
            surveyThankYouText = System.Label.LABS_SF_Survey_Submitted_Thank_you;
        }
        surveyThankYouURL = s.thankYouLink__c;
        surveyContainerCss = s.Survey_Container_CSS__c;
    }
    
    //------------------------------------------------------------------------------//   
    public Pagereference updateSurveyName(){
                    System.debug(':::::::::::::::::::::::::updateSurveyName');
        Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:surveyId];
        s.Name = surveyName;
        try{
            update s;
        }catch (Exception e){
            Apexpages.addMessages(e);
        }
        return null;
    } 
    
    //------------------------------------------------------------------------------//      
    public Pagereference updateSurveyThankYouAndLink(){
                    System.debug(':::::::::::::::::::::::::updateSurveyThankYouAndLink');
        Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:surveyId];
        s.thankYouText__c = surveyThankYouText;
        s.thankYouLink__c = surveyThankYouURL;
        try{
            update s;
        }catch(Exception e){
            Apexpages.addMessages(e);
        }
        return null;
    }
    
    //------------------------------------------------------------------------------//    
    /** When requested from the page - when the user clicks on 'Update Order' -
this function will reorganize the list so that it is displayed in the new order
*/
    public Pagereference refreshQuestionList(){
                    System.debug(':::::::::::::::::::::::::refreshQuestionList');
        setupQuestionList();
        return null;
    }
    
    
    
    //------------------------------------------------------------------------------//      
    
    
    
    //------------------------------------------------------------------------------//    
    private static boolean checkRequired(String response, Survey_Question__c question){
                    System.debug(':::::::::::::::::::::::::checkRequired');
        if(question.Required__c == true){
            if(response == null || response =='NO RESPONSE')
                return false;
        }
        return true;
    } 
    
    /** Redirects the page that displays the detailed results of the survey, 
from all users who took the survey.
*/
    public PageReference resultPage() {
                    System.debug(':::::::::::::::::::::::::resultPage');
        return new PageReference('/apex/ResultsPage?id='+surveyId);
    }
    
    
    //------------------------------------------------------------------------------//  
    
    //------------------------------------------------------------------------------//  
    
    /** 
*/
    public List<String> getResponses() {
                    System.debug(':::::::::::::::::::::::::getResponses');
        List<SurveyQuestionResponse__c> qr = [Select Survey_Question__c, SurveyTaker__c, Response__c, Name From SurveyQuestionResponse__c limit 100];
        List<String> resp = new List<String>();
        for (SurveyQuestionResponse__c r : qr) {
            resp.add(r.Response__c);
        }
        
        return resp;
    }  
    
    
    
    /** Fills up the List of questions to be displayed on the Visualforce page
*/   
    public List<question> getAQuestion() {
                    System.debug(':::::::::::::::::::::::::getAQuestion');
        qQuestion = '';
        qChoices ='';
        system.debug(':::::getQuetsion:::::'+counter);  
        //counter += list_size;
        List<Survey_Question__c> allQuestionsObject = 
            [Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, 
             s.OrderNumber__c, s.Name, s.Choices__c 
             From Survey_Question__c s 
             WHERE s.Survey__c =: surveyId ORDER BY s.OrderNumber__c
             limit :list_size//anadido para la paginacion
             offset :counter]; //anadido para la paginacion
        System.debug(allQuestionsObject);
        System.debug(':::::::::Offset:::::::'+counter);
        UpdateAnswer();
        allQuestions = new List<question>();
        
        Double old_OrderNumber = 0;
        Double new_OrderNumber;
        Double difference = 0;
        /* Make sure that the order number follow each other (after deleting a question, orders might not do so) */
        for (Survey_Question__c q : allQuestionsObject){ 
            new_OrderNumber = q.OrderNumber__c;
            difference = new_OrderNumber - old_OrderNumber - 1;
            if (difference > 0) {
                Double dd = double.valueOf(difference);
                Integer newOrderInt = dd.intValue();
                //    q.OrderNumber__c -= Integer.valueOf(newOrderInt); 
            }
            old_OrderNumber = q.OrderNumber__c;
            question theQ = new question(q);
            allQuestions.add(theQ);
        }
        allQuestionsSize = allQuestions.size();
        previousAnswer();
        return allQuestions;
    }

    //acrescentado en 12/12/2016
    public List<question> getAQuestionFinal() {
                    System.debug(':::::::::::::::::::::::::getAQuestionFinal');
 
        //counter += list_size;
        List<Survey_Question__c> allQuestionsObject = 
            [Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, 
             s.OrderNumber__c, s.Name, s.Choices__c 
             From Survey_Question__c s 
             WHERE s.Survey__c =: surveyId ORDER BY s.OrderNumber__c]; //anadido para la paginacion
        allQuestionsFinal = new List<question>();
        
        Double old_OrderNumber = 0;
        Double new_OrderNumber;
        Double difference = 0;
        /* Make sure that the order number follow each other (after deleting a question, orders might not do so) */
        for (Survey_Question__c q : allQuestionsObject){ 
            new_OrderNumber = q.OrderNumber__c;
            difference = new_OrderNumber - old_OrderNumber - 1;
            if (difference > 0) {
                Double dd = double.valueOf(difference);
                Integer newOrderInt = dd.intValue();
                //    q.OrderNumber__c -= Integer.valueOf(newOrderInt); 
            }
            old_OrderNumber = q.OrderNumber__c;
            question theQ = new question(q);
            allQuestionsFinal.add(theQ);
        }
        return allQuestionsFinal;
    }
    private void previousAnswer(){
        //if(!allQuestions.isEmpty() && !allQuestionsFinal.isEmpty() ){
            for(integer i=0; i< allQuestions.size(); i++){
                for(question qf : allQuestionsFinal){
                    if(allQuestions.get(i).id == qf.id)
                        allQuestions.set(i,qf);

                }
            }
       // }

    }   
    private void UpdateAnswer(){
        //if(!allQuestions.isEmpty() && !allQuestionsFinal.isEmpty() ){
            for(integer i=0; i< allQuestionsFinal.size(); i++){
                for(question qf : allQuestions){
                    if(allQuestionsFinal.get(i).id == qf.id)
                        allQuestionsFinal.set(i,qf);

                }
            }
       // }

    } 
    
   
   
   public list<string> listaRequiredCuestiones(){
     //  getPageNumber();
        //List <SurveyQuestionResponse__c> sqrList = new List<SurveyQuestionResponse__c>();
        //    UpdateAnswer();
            
             list<string> listMessage = new list<string>();
            
       for (question q : allQuestions) {
       System.debug(':::::::::::q::::::'+q);
          if (q.renderSelectRadio == 'true') {
                    System.debug('::::::::::listaRequiredCuestiones::::::::::::::renderSelectRadio');
                    if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                     System.debug('::::::::::renderSelectRadio  INSIDE IF::::::::::::::');
                      listMessage.add(''+q.ordernumber);
                        
                                
                 }
               }else if (q.renderFreeText == 'true') {
                        System.debug('::::::::::listaRequiredCuestiones:::::::::::::::renderFreeText');
                 if (q.required && q.choices == '') {
                   System.debug('::::::::::renderFreeText INSIDE IF::::::::::::::');
                      listMessage.add(''+q.ordernumber);
                    }
               }else if (q.renderSelectCheckboxes == 'true') {
                    System.debug('::::::::listaRequiredCuestiones:::::::::renderSelectCheckboxes');
                      if (q.required && (q.selectedOptions == null || q.selectedOptions.size() == 0)) {
                       System.debug('::::::::::renderSelectCheckboxes INSIDE IF::::::::::::::');
                            listMessage.add(''+q.ordernumber);
                        }
                   }  else if (q.renderSelectRow == 'true') {
                    System.debug(':::::::::::::::::::::::renderSelectRow');
                    if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                        System.debug(':::::::::::::::::::::::renderSelectRow  INSIDE IF::::::::::::::');
                         listMessage.add(''+q.ordernumber);
                     }
                  }
                 
              } 
           System.debug(':::::::::::listMessage.size()::::'+listMessage.size());
           
            
      return listMessage;
      
   }
   
    public pagereference submitResults()
    {
                    System.debug(':::::::::::::::::::::::::submitResults');
       // try 
            List <SurveyQuestionResponse__c> sqrList = new List<SurveyQuestionResponse__c>();
            UpdateAnswer();
            System.debug('Here 1'+allQuestionsFinal.size());
            for (question q : allQuestionsFinal) {
                System.debug('Here 2::'+q);
               
                SurveyQuestionResponse__c sqr = new SurveyQuestionResponse__c();
                if (q.renderSelectRadio == 'true') {
                    System.debug('::::::::::::::::::::::::renderSelectRadio');
                    if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                       
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Por favor, rellene los campos obligatorios. Ver cuestion:'+q.ordernumber));
                        return null;
                    }
                    
                    if (q.selectedOption == null || q.selectedOption == '') {
                        sqr.Response__c = '';
                    } else {
                        sqr.Response__c = q.singleOptions.get(Integer.valueOf(q.selectedOption)).getLabel();
                    }
                    sqr.Survey_Question__c = q.Id;
                    sqrList.add(sqr);
                } else if (q.renderFreeText == 'true') {
                    System.debug(':::::::::::::::::::::::::renderFreeText');
                    if (q.required && q.choices == '') {
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Por favor, rellene los campos obligatorios. Ver cuestion: '+q.ordernumber));
                        return null;
                    }
                    System.debug('*****Select Radio ' + q.choices);
                    
                    sqr.Response__c = q.choices;
                    sqr.Survey_Question__c = q.Id;
                    sqrList.add(sqr);
                } else if (q.renderSelectCheckboxes == 'true') {
                    System.debug(':::::::::::::::::renderSelectCheckboxes');
                    if (q.required && (q.selectedOptions == null || q.selectedOptions.size() == 0)) {
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Por favor, rellene los campos obligatorios. Ver cuestion: '+q.ordernumber));
                        return null;
                    }
                    
                    for (String opt : q.selectedOptions) {
                        sqr = new SurveyQuestionResponse__c();
                        if (opt == '' || opt == null) {
                            sqr.Response__c = '';
                        } else {
                            sqr.Response__c = q.multiOptions.get(Integer.valueOf(opt)).getLabel();
                        }
                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    }
                } else if (q.renderSelectRow == 'true') {
                    System.debug(':::::::::::::::::::::::renderSelectRow');
                    if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Por favor, rellene los campos obligatorios. Ver cuestion: '+q.ordernumber));
                        return null;
                    }
                    
                    if (q.selectedOption == null || q.selectedOption == '') {
                        sqr.Response__c = '';
                    } else {
                        sqr.Response__c = q.rowOptions.get(Integer.valueOf(q.selectedOption)).getLabel();
                    }
                    sqr.Survey_Question__c = q.Id;
                    sqrList.add(sqr);
                }
            }
            //System.debug('AddSurveyTaker:'+AddSurveyTaker());
            if(AddSurveyTaker())
            {
                System.debug('Here 3');
                for (SurveyQuestionResponse__c sqr : sqrList)
                {
                    sqr.SurveyTaker__c = surveyTakerId;
                }
                System.debug('Here 4');
                insert sqrList;
                thankYouRendered=true;
            }
        /*}catch(Exception e){
            if(isInternal) {
                Apexpages.addMessages(e);
            }else{
                System.debug('Exception: ' + e.getMessage());
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Some error occured while saving response'));
            }
        }*/
          PageReference newpage = new PageReference ('/'+esId);
           return newpage;
   
    }
    
    
    private Boolean AddSurveyTaker()
    {
                    System.debug(':::::::::::::::::::::::::AddSurveyTaker');
        String userId;
        
        if (surveyId == null)
        {
            return false;
        }
        if(caseId.toUpperCase() =='NONE'|| caseId.length()<5)
            caseId = null;    
        if(contactId.toUpperCase() =='NONE'|| contactId.length()<5)
            contactId = null;         
        if (anonymousAnswer != 'Anonymous')
        {
            userId = UserInfo.getUserId();
        }
        else
        {
            userId = null;
        }
        
        if(anonymousAnswer != 'Anonymous' && (contactId != null || caseId != null))
        {
            List<SurveyTaker__c> check = [Select Contact__c, Survey__c, Case__c, User__c From SurveyTaker__c Where Contact__c=:contactId and Survey__c=:surveyId and Case__c = :caseId and User__c=:UserId];
            if(check != null && check.size()>0){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.LABS_SF_You_have_already_taken_this_survey));
                
                return false;       
            }
        }
        
        
        SurveyTaker__c st = new SurveyTaker__c();
        st.Contact__c = contactId;
        st.Survey__c = surveyId;
        st.Taken__c = 'false';
        st.Case__c = caseId;
        st.User__c = userId;
        
        //anãdido para obtenere la ES en lo url
        list<SurveyTaker__c> lst = [Select id, Survey__c, AM_Estacion_de_servicio__c From SurveyTaker__c Where AM_Estacion_de_servicio__c=:esId and Survey__c=:surveyId];
        if(lst.size()> 0) {
            delete lst;
        }
        if(esId != '' && esId != null )
            st.AM_Estacion_de_servicio__c = esId;
        
        insert st;  
        surveyTakerId = st.Id;
        return true;    
    }
    
    // ----------------------------------------------------------------------------------//
    // -------------------------------- Pagination -------------------------------------//
    public Survey_Question__c[] getNumbers() {
                    System.debug(':::::::::::::::::::::::::getNumbers');
        try {
            Survey_Question__c[] numbers = [select OrderNumber__c 
                                            from Survey_Question__c 
                                            order by OrderNumber__c 
                                            limit :list_size 
                                            offset :counter];
            
            return numbers;
        } catch (QueryException e) {
            ApexPages.addMessages(e);   
            return null;
        }
    } 
    
    public PageReference Beginning() { //user clicked beginning
                    System.debug(':::::::::::::::::::::::::Beginning');
        counter = 0;
          getAQuestion();
        getPageNumber();
        System.debug('::::::::::::getPageNumber:::'+getPageNumber());
        return null;
    }
    
    public PageReference Previous() { //user clicked previous button
                    System.debug(':::::::::::::::::::::::::Previous');
         If(getPageNumber() == 1){
             return null;
         } else {              
            counter -= list_size;
          
             getAQuestion();
             //previousAnswer();
            //getPageNumber();
            return null;
         }
    }
    
    public PageReference Next() { //user clicked next button
                    System.debug(':::::::::::::::::::::::::Next');
      
      list<string>  listMessages = listaRequiredCuestiones();
      System.debug('::::::::::::::listMessages::::::::::::'+listMessages);
        if(!listMessages.isEmpty()){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Por favor, rellene los campos obligatorios.Ver cuestion(s):'+string.join(listMessages,',' )));
            return null;
        }else{
         If(getPageNumber() == getTotalPages())   
         {return null;} else {   
        counter += list_size;
        
        getAQuestion();
       
        System.debug('::::::::::::::::::::::::getPageNumber()::::'+getPageNumber());
        return null;}
        }
        
    }
    
    public PageReference End() { //user clicked end
                    System.debug(':::::::::::::::::::::::::End');
       //added to required fields
        list<string>  listMessages = listaRequiredCuestiones();
      System.debug('::::::::::::::listMessages::::::::::::'+listMessages);
        if(!listMessages.isEmpty()){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Por favor, rellene los campos obligatorios.Ver cuestion(s):'+ string.join(listMessages,',')));
            return null;
        }else{             
                    
                    
        if(math.mod(total_size, list_size) == 0)
            counter = total_size-list_size;
        else
            counter = (total_size - math.mod(total_size, list_size));
        system.debug('::::.Counter END::::::'+counter);
        system.debug('::::.Counter total_size::::::'+total_size);
        system.debug('::::.Counter MOD::::::'+ math.mod(total_size, list_size));
         getAQuestion();
        getPageNumber();
        return null;
       } 
    }
    
    public Boolean getDisablePrevious() { 
                    System.debug(':::::::::::::::::::::::::getDisablePrevious');
                     System.debug('::::::::::::::::: getDisablePrevious counter::::::'+counter);
            return false;          
        //this will disable the previous and beginning buttons
       //if (counter>0) return false; else return true;
    
       getPageNumber();
           System.debug('::::::::: pageNumber : 572:::::::'+pageNumber );
        if(counter> 0 )
        return false; else return true;
       
    }
    
    public Boolean getDisableNext() { //this will disable the next and end buttons
                    System.debug(':::::::::::::::::::::::::getDisableNext');
                 return false; 
     getPageNumber();
      if (counter + list_size < total_size) return false; else return true;
     
    }
    
    public Integer getTotal_size() {
                    System.debug(':::::::::::::::::::::::::getTotal_size:::'+total_size);
        return total_size;
    }
    
    public Integer getPageNumber() {
                    System.debug(':::::::::::::::::::::::::getPageNumber:::');
       // getAQuestion();
        System.debug(':::::::::PAgeNumber Counter::::'+counter);
        System.debug(':::::::::PAgeNumber size::::'+list_size);
        System.debug(':::::::::PAgeNumber All::::'+(counter/list_size + 1));
        pageNumber = (counter/list_size + 1);
        
        system.debug('::::::::::::pageNumber::::::::::::'+pageNumber);
        system.debug('::::::::::::getTotalPages::::::::::::'+getTotalPages());
        
        return (pageNumber);
        
        
    }
    
    
    public Integer getTotalPages() {
                    System.debug(':::::::::::::::::::::::::getTotalPages');
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        } else {
            return (total_size/list_size);
        }
    }
    
    public Integer getlastPage() {
                    System.debug(':::::::::::::::::::::::::getlastPage');
        getPageNumber();
        system.debug(':::::::::::::::::::::::::lastpage: TotalPAges::::::::::::::::'+getTotalPages());
        return (getTotalPages());
        
    }
    
    
    
    //----------------------------------------------------End Pagination ------------------------------------------//   
    
    
    
}