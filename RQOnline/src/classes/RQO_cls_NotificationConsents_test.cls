/**
*   @name: RQO_cls_NotificationConsents_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_NotificationConsents.
*/
@isTest
public class RQO_cls_NotificationConsents_test {
    
    /**
    *   @name: RQO_cls_NotificationConsents_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la actualización de consentimientos.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private static testMethod void whenUpdateLanguageReturnsOK() {
        
        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserClienteRunAsJob@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        
        System.runAs(userRunAs){
        
            test.startTest();
    
            Boolean DidThrowException = false;
            
            try {
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                RQO_obj_notificationConsent__c consent = new RQO_obj_notificationConsent__c(RQO_fld_Contact__c = contact[0].Id,
                                                                                            RQO_fld_notificationType__c = RQO_cls_Constantes.COD_COM_MULTIMEDIA);
                List<RQO_obj_notificationConsent__c> consents = new List<RQO_obj_notificationConsent__c>();
                consents.add(consent);
                ((RQO_cls_NotificationConsents) new RQO_cls_NotificationConsents.Constructor().construct(consents)).updateConsents();
            }
            catch (Exception e) {
                DidThrowException = true;
            }
            
            System.assertEquals(false,
                                DidThrowException, 
                                'Se ha generado una excepción no esperada.');
            
            test.stopTest();
        }
    }
}