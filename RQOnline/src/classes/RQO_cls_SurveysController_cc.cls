/**
*   @name: RQO_cls_SurveysController_cc
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Controlador de Surveys
*/
public with sharing class RQO_cls_SurveysController_cc {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_SurveysController';
    
    /**
    *   @name: RQO_cls_SurveysController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que selecciona la última Home activa
    */
    @AuraEnabled
    public static RQO_obj_surveyQuestion__c selectLastActiveHome(string perfilCliente, string perfilUsuario, string cookies) 
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectLastActiveHome';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': perfilCliente ' + perfilCliente);
        System.debug(CLASS_NAME + ' - ' + METHOD + ': perfilUsuario ' + perfilUsuario);
    
        List<RQO_obj_surveyQuestion__c> active = 
            new RQO_cls_SurveyQuestionSelector().selectActive(new Set<ID> { UserInfo.getUserId() },
                                                              perfilCliente, 
                                                              perfilUsuario, 
                                                              true);
        
        // Eliminamos las preguntas anónimas respondidas por el usuario                                               
        for (Integer i = (active.size()-1); i>=0; i--)
        {
            RQO_obj_surveyQuestion__c question = active[i];
            if (question != null && cookies != null && cookies.contains(question.Id))
                active.remove(i);
        }
                                            
        active = RQO_cls_SurveysController_cc.translateSurveyQuestions(active);       
                                                              
        if (active != null && active.size() > 0)
            return active [0];
        else
            return null;
    }
    
    /**
    *   @name: RQO_cls_SurveysController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que selecciona la última Home activa
    */
    @AuraEnabled
    public static RQO_obj_survey__c selectSurveyById(ID idSurvey) 
    {       
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectSurveyById';
        
        List<RQO_obj_survey__c> surveys = new RQO_cls_SurveySelector().selectById(new Set<ID> { idSurvey });

        if (surveys != null && surveys.size() > 0)
            return RQO_cls_SurveysController_cc.translateSurvey(surveys[0]);
        else
            return null;
    }
    
    /**
    *   @name: RQO_cls_SurveysController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la lista de RQO_obj_surveyQuestion__c mediante una idSurvey
    */
    @AuraEnabled
    public static List<RQO_obj_surveyQuestion__c> selectQuestions(ID idSurvey) 
    {       
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectQuestions';
        
        List<RQO_obj_surveyQuestion__c> questions = new RQO_cls_SurveyQuestionSelector().selectBySurveyId(new Set<ID> { idSurvey });

        if (questions != null && questions.size() > 0)
            return RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
        else
            return null;
    }
    
    /**
    *   @name: RQO_cls_SurveysController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que agrega una respuesta en base a ids de Survey, Question y Response
    */
    @AuraEnabled
    public static void addResponse(string name, ID idSurvey, ID idQuestion, ID idResponse) 
    {       
        // ***** CONSTANTES ***** //
        final String METHOD = 'addResponse';
        
        List<RQO_obj_surveyResponse__c> responses = new List<RQO_obj_surveyResponse__c>();
        RQO_obj_surveyResponse__c response = new RQO_obj_surveyResponse__c (
            Name = name,
            RQO_fld_question__c = idQuestion,
            RQO_fld_response__c = idResponse,
            RQO_fld_user__c = UserInfo.getUserId()
            );
        responses.add(response);
        
        RQO_obj_survey__c survey = RQO_cls_SurveysController_cc.selectSurveyById(idSurvey);
        
        if (survey.RQO_fld_anonymous__c)
            new RQO_cls_SurveyResponses(responses).addAnonymousResponse();
        else
            new RQO_cls_SurveyResponses(responses).addResponse();
    }
    
    /**
    *   @name: RQO_cls_SurveysController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que agrega una respuesta una Id de Survey y a un JSON de Responses
    */
    @AuraEnabled
    public static void addResponses(ID surveyId, string jsonResponses) 
    {       
        // ***** CONSTANTES ***** //
        final String METHOD = 'addResponses';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': guardar respuestas del usuario ' + jsonResponses);
        
        List<RQO_obj_questionResponse__c> questionResponses = (List<RQO_obj_questionResponse__c>)
            JSON.deserialize(jsonResponses, List<RQO_obj_questionResponse__c>.class);
            
        System.debug(questionResponses);

        List<RQO_obj_surveyResponse__c> responses = new List<RQO_obj_surveyResponse__c>();
        for (RQO_obj_questionResponse__c questionResponse : questionResponses)
        {
            System.debug(CLASS_NAME + ' - ' + METHOD + ': ' + questionResponse.Name + ' ' + questionResponse.RQO_fld_selected__c);
            if (questionResponse.RQO_fld_selected__c)
            {
                RQO_obj_surveyResponse__c response = new RQO_obj_surveyResponse__c (
                    Name = questionResponse.Name,
                    RQO_fld_question__c = questionResponse.RQO_fld_question__c,
                    RQO_fld_response__c = questionResponse.Id,
                    RQO_fld_user__c = UserInfo.getUserId()
                    );
                responses.add(response);
            }
        }
        
        if (responses.size() > 0)
        {
            RQO_obj_survey__c survey = RQO_cls_SurveysController_cc.selectSurveyById(surveyId);
            
            System.debug(CLASS_NAME + ' - ' + METHOD + ': ' + responses.size() + ' respuestas para la encuesta ' + survey.Name);
            
            if (survey.RQO_fld_anonymous__c)
                new RQO_cls_SurveyResponses(responses).addAnonymousResponse();
            else
                new RQO_cls_SurveyResponses(responses).addResponse();
        }
    }
    
    /**
    *   @name: RQO_cls_SurveysController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la traducción de una Survey
    */
    public static RQO_obj_survey__c translateSurvey(RQO_obj_survey__c survey)
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'translateSurvey';
        
        if (UserInfo.getLanguage() == 'en_US')
            survey.Name = survey.RQO_fld_nameEn__c;
        else if (UserInfo.getLanguage() == 'fr')
            if (!String.isEmpty(survey.RQO_fld_nameFr__c))
                survey.Name = survey.RQO_fld_nameFr__c;
            else
                survey.Name = survey.RQO_fld_nameEn__c;
        else if (UserInfo.getLanguage() == 'de')
            if (!String.isEmpty(survey.RQO_fld_nameDe__c))
                survey.Name = survey.RQO_fld_nameDe__c;
            else
                survey.Name = survey.RQO_fld_nameEn__c;
        else if (UserInfo.getLanguage() == 'pt_BR')
            if (!String.isEmpty(survey.RQO_fld_namePt__c))
                survey.Name = survey.RQO_fld_namePt__c;
            else
                survey.Name = survey.RQO_fld_nameEn__c;
        else if (UserInfo.getLanguage() == 'it')
            if (!String.isEmpty(survey.RQO_fld_nameIt__c))
                survey.Name = survey.RQO_fld_nameIt__c;
            else
                survey.Name = survey.RQO_fld_nameEn__c;
            
        return survey;
    } 
    
    /**
    *   @name: RQO_cls_SurveysController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lsita de traducciones de SurveyQuestions
    */
    public static List<RQO_obj_surveyQuestion__c> translateSurveyQuestions(List<RQO_obj_surveyQuestion__c> questions)
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'translateSurveyQuestions';
        
        for (RQO_obj_surveyQuestion__c question : questions)
        {
            if (UserInfo.getLanguage() == 'en_US')
                question.Name = question.RQO_fld_nameEn__c;
            else if (UserInfo.getLanguage() == 'fr')
                if (!String.isEmpty(question.RQO_fld_nameFr__c))
                    question.Name = question.RQO_fld_nameFr__c;
                else
                    question.Name = question.RQO_fld_nameEn__c;
            else if (UserInfo.getLanguage() == 'de')
                if (!String.isEmpty(question.RQO_fld_nameDe__c))
                    question.Name = question.RQO_fld_nameDe__c;
                else
                    question.Name = question.RQO_fld_nameEn__c;
            else if (UserInfo.getLanguage() == 'pt_BR')
                if (!String.isEmpty(question.RQO_fld_namePt__c))
                    question.Name = question.RQO_fld_namePt__c;
                else
                    question.Name = question.RQO_fld_nameEn__c;
            else if (UserInfo.getLanguage() == 'it')
                if (!String.isEmpty(question.RQO_fld_nameIt__c))
                    question.Name = question.RQO_fld_nameIt__c;
                else
                    question.Name = question.RQO_fld_nameEn__c;
                    
            for (RQO_obj_questionResponse__c response : question.Question_Responses__r)
            {
                if (UserInfo.getLanguage() == 'en_US')
                    response.Name = response.RQO_fld_nameEn__c;
                else if (UserInfo.getLanguage() == 'fr')
                    if (!String.isEmpty(response.RQO_fld_nameFr__c))
                        response.Name = response.RQO_fld_nameFr__c;
                    else
                        response.Name = response.RQO_fld_nameEn__c;
                else if (UserInfo.getLanguage() == 'de')
                    if (!String.isEmpty(response.RQO_fld_nameDe__c))
                        response.Name = response.RQO_fld_nameDe__c;
                    else
                        response.Name = response.RQO_fld_nameEn__c;
                else if (UserInfo.getLanguage() == 'pt_BR')
                    if (!String.isEmpty(response.RQO_fld_namePt__c))
                        response.Name = response.RQO_fld_namePt__c;
                    else
                        response.Name = response.RQO_fld_nameEn__c;
                else if (UserInfo.getLanguage() == 'it')
                    if (!String.isEmpty(response.RQO_fld_nameIt__c))
                        response.Name = response.RQO_fld_nameIt__c;
                    else
                        response.Name = response.RQO_fld_nameEn__c;
            }
        }
        
        return questions;
    } 
    
}