/*------------------------------------------------------------------------
Author:         Borja Matín Ballesteros
Company:        Indra
Description:    class
History
<Date>          <Author>                      <Description>
13-Jul-2016     Borja Matín Ballesteros       Initial version
----------------------------------------------------------------------------*/
public with sharing class ITPM_ListasVistaExtension {
    public String idActual {get; set;}
    
    public ITPM_ListasVistaExtension(ApexPages.StandardController controller) {
        idActual = ApexPages.currentPage().getParameters().get('id');
    }
    
    // IDEA
    public String getOportunidades(){
        String retorno = '';
        AggregateResult[] lstOportunidades= [SELECT COUNT(Id) FROM ITPM_Opportunity__c WHERE ITPM_Idea__c = : idActual];
        for (AggregateResult ar : lstOportunidades){
            if (integer.valueof(ar.get('expr0')) > 1){
                retorno = 'There are ' + ar.get('expr0') + ' opportunities created';
            }else if (integer.valueof(ar.get('expr0')) == 1){
                retorno = 'There is ' + ar.get('expr0') + ' opportunity created';
            }else{
                retorno = '';
            }
        }
        return retorno;
    }
       
        
    public String getProj_PV(){
        String retorno = '';
        AggregateResult[] lstProyectos = [SELECT COUNT(Id) FROM ITPM_Project__c WHERE ITPM_Project_REL_Conceptualization__r.ITPM_Conceptu_REL_Opportunity__c = : idActual];
        for (AggregateResult ar : lstProyectos ){
            if (integer.valueof(ar.get('expr0')) > 1){
                retorno = 'There are ' + ar.get('expr0') + ' projects created';
            }else if (integer.valueof(ar.get('expr0')) == 1){
                retorno = 'There is ' + ar.get('expr0') + ' project created';
            }else{
                retorno = '';
            }
        }
        return retorno;
    }
    
    //PdV/Concepts
    public String getPdvConcepts(){
        String retorno = '';
        AggregateResult[] lstPropuestas = [SELECT COUNT(Id) FROM ITPM_Conceptualization__c WHERE ITPM_Conceptu_REL_Opportunity__c = : idActual];
        for (AggregateResult ar : lstPropuestas){
            if (integer.valueof(ar.get('expr0')) > 1){
                retorno = 'There are ' + ar.get('expr0') + ' PdV/Concepts created';
            }else if (integer.valueof(ar.get('expr0')) == 1){
                retorno = 'There is ' + ar.get('expr0') + ' PdV/Concepts created';
            }else{
                retorno =  '';
            }
        }
        return retorno;
    }
    
    // PDV
    public String getProjects(){
        String retorno = '';
        AggregateResult[] lstProyectos = [SELECT COUNT(Id) FROM ITPM_Project__c WHERE ITPM_Project_REL_Conceptualization__c =: idActual AND ITPM_Project_Phase__c !=: 'Rejected'];
        for (AggregateResult ar : lstProyectos ){
            if (integer.valueof(ar.get('expr0')) > 1){
                retorno = 'There are ' + ar.get('expr0') + ' projects created';
            }else if (integer.valueof(ar.get('expr0')) == 1){
                retorno = 'There is ' + ar.get('expr0') + ' project created';
            }else{
                retorno = '';
            }
        }
        return retorno;        
    }
    
    public String getProjectsRejecteds(){
        String retorno = '';
        AggregateResult[] lstProyectos = [SELECT COUNT(Id) FROM ITPM_Project__c WHERE ITPM_Project_REL_Conceptualization__c =: idActual AND ITPM_Project_Phase__c =: 'Rejected'];        
        for (AggregateResult ar : lstProyectos ){
            if (integer.valueof(ar.get('expr0')) > 0){
                retorno = 'The created project has been rejected';
            }else{
                retorno = '';
            }
        }
        return retorno;
    }
    
    public String getPrograms(){
        String retorno = '';
        AggregateResult[] lstProgramas = [SELECT COUNT(Id) FROM ITPM_Program__c WHERE ITPM_REL_Conceptualization__c = : idActual];
        for (AggregateResult ar : lstProgramas ){
            if (integer.valueof(ar.get('expr0')) > 1){
                retorno = 'There are ' + ar.get('expr0') + ' programs created';
            }else if (integer.valueof(ar.get('expr0')) == 1){
                retorno = 'There is ' + ar.get('expr0') + ' program created';
            }else{
                retorno = '';
            }
        }
        return retorno;
    }
     
}