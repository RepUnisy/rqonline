/**
 *	@name: RQO_cls_ProductoItemDescargable_cc
 *	@version: 1.0
 *	@creation date: 18/09/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Controlador Apex para recuperar los datos de documentos a través de los servicios de Documentum
 *	@testClass: RQO_cls_Documentum_test
*/
public with sharing class RQO_cls_ProductoItemDescargable_cc {

     /**
	* @creation date: 18/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera los datos del documento solicitado 
	* @Param: idDocumentum tipo String, id del documento a recuperar
	* @param: repository tipo String.
	* @param: docType tipo String, tipo de documento.
	* @return: List<String> con la respuesta de la llamada a docuemtum
	* @exception: 
	* @throws: 
	*/
    @AuraEnabled
    public static String[] getDocDataValues(String idDocumentum, String repository, String docType){
        RQO_cls_Documentum docum = new RQO_cls_Documentum();
        RQO_cls_Documentum.RQO_cls_documentumResponse dataReturn =null;
        //Si el tipo de documento es de llamada indirecta (tenemos id de SAP no de documetum)
        if (String.isNotEmpty(docType) && RQO_cls_Constantes.DOCUMENTUM_RECOVERY_TYPE_VISOR.equalsIgnoreCase(docType)){
            dataReturn = docum.documentIndirectCall(repository, idDocumentum);
        }else{
            //Si tenemos id de documentum la llamada es directa
			dataReturn = docum.documetumOp(RQO_cls_Constantes.WS_DOCUM_GET, repository, idDocumentum, null, null, null, null, null, true);
        }
        System.debug('Datos: ' + dataReturn);
        return new List<String>{dataReturn.errorCode, dataReturn.errorMessage, dataReturn.docData, dataReturn.docName, dataReturn.docType};
    }
}