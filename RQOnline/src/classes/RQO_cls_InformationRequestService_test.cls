/**
*   @name: RQO_cls_InformationRequestService_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description:  Clase de test para comprobar el funcionamiento de la clase RQO_cls_InformationRequestService.
*/
@IsTest
public class RQO_cls_InformationRequestService_test {
	
	// ***** CONSTANTES ***** //
	private static final Id FAQ_RECORD_TYPE = 
  Schema.SObjectType.RQO_obj_informationRequest__c.getRecordTypeInfosByName().get('FAQ').getRecordTypeId();
  private static final Id CONTACT_RECORD_TYPE = 
  Schema.SObjectType.RQO_obj_informationRequest__c.getRecordTypeInfosByName().get('Contact Form').getRecordTypeId();

    /**
    *   @name: RQO_cls_InformationRequestService_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @testSetup static void initialSetup() {
      User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES','Europe/Berlin');

      System.runAs(u){
			// Crear email templates RQO_eplt_correoContacto_en RQO_eplt_correoFaqs_en
         RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_correoContacto_en', 'RQO_eplt_correoFaqs_en'}); 
        }
    }

    /**
    *   @name: RQO_cls_InformationRequestService_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico que cubre una casuística transaccional de una llamada al dominio
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @IsTest
    private static void whenRegisterFaqCallsDomainAndCommits()
    {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            fflib_ApexMocks mocks = new fflib_ApexMocks();
            
            // Given
            fflib_SObjectUnitOfWork mockUow = 
            (fflib_SObjectUnitOfWork) mocks.mock(fflib_SObjectUnitOfWork.class);
            RQO_cls_informationRequests mockDomain = 
            (RQO_cls_informationRequests) mocks.mock(RQO_cls_informationRequests.class);
            
            // Given - Configure mock responses
            Id informationRequestId = 
            fflib_IDGenerator.generate(RQO_obj_informationRequest__c.SObjectType);
            mocks.startStubbing();
            mocks.when(mockDomain.SObjectType()).thenReturn(RQO_obj_informationRequest__c.SObjectType);
            mocks.stopStubbing();
            
            // Given - inject mocks
            RQO_cls_Application.UnitOfWork.setMock(mockUow);
            RQO_cls_Application.Domain.setMock(mockDomain);
            
            // When
            RQO_cls_informationRequestService.registerFaq('test@test.com', 'Test Message');
            
            // Then
            ((RQO_cls_informationRequests) mocks.verify(mockDomain, 0)).add(mockUow);
            ((fflib_SObjectUnitOfWork) mocks.verify(mockUow, 1)).commitWork();
        }
    }

    /**
    *   @name: RQO_cls_InformationRequestService_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico que cubre una casuística transaccional de una llamada al dominio
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @IsTest
    private static void whenRegisterContactCallsDomainAndCommits()
    {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');

        System.runAs(u) {
            fflib_ApexMocks mocks = new fflib_ApexMocks();

                // Given
            fflib_SObjectUnitOfWork mockUow = 
            (fflib_SObjectUnitOfWork) mocks.mock(fflib_SObjectUnitOfWork.class);
            RQO_cls_informationRequests mockDomain = 
            (RQO_cls_informationRequests) mocks.mock(RQO_cls_informationRequests.class);

                // Given - Configure mock responses
            Id informationRequestId = 
            fflib_IDGenerator.generate(RQO_obj_informationRequest__c.SObjectType);
            mocks.startStubbing();
            mocks.when(mockDomain.SObjectType()).thenReturn(RQO_obj_informationRequest__c.SObjectType);
            mocks.stopStubbing();

                // Given - inject mocks
            RQO_cls_Application.UnitOfWork.setMock(mockUow);
            RQO_cls_Application.Domain.setMock(mockDomain);

                // When
            RQO_cls_informationRequestService.registerContact('test@test.com', 'Test Surname', 'Mr',
              '+34', '910000000', 'Test Company',
              'Agent', 'test@test.com', 'ES',
              'Technical Information', 'Olefins',
              'Test Subject', 'Test Message');

                // Then
            ((fflib_SObjectUnitOfWork) mocks.verify(mockUow, 1)).commitWork();
        }
    }

    /**
    *   @name: RQO_cls_InformationRequestService_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico que cubre una casuística para cuando se envía un correo
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @IsTest
    private static void whensendEmailSendsEmails()
    {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');

        System.runAs(u) {
            fflib_ApexMocks mocks = new fflib_ApexMocks();

                // Given
            RQO_cls_InformationRequestSelector mockSelector = 
            (RQO_cls_InformationRequestSelector) mocks.mock(RQO_cls_InformationRequestSelector.class);

                // Given - Configure mock responses

            Id informationRequestId = 
            fflib_IDGenerator.generate(RQO_obj_informationRequest__c.SObjectType);
            List<RQO_obj_informationRequest__c> infoRequests = 
            new List<RQO_obj_informationRequest__c> {
                new RQO_obj_informationRequest__c(
                    Id=informationRequestId,
                    Name = 'test@test.com',
                    RQO_fld_email__c = 'test@test.com',
                    RQO_fld_subject__c = 'Test Subject',
                    RQO_fld_message__c = 'Test Message',
                    RecordTypeId = FAQ_RECORD_TYPE
                    )
            };
            mocks.startStubbing();
            mocks.when(mockSelector.SObjectType()).thenReturn(RQO_obj_informationRequest__c.SObjectType);
            mocks.when(mockSelector.selectById(new Set<Id> {informationRequestId})).thenReturn(infoRequests);
            mocks.stopStubbing();

                // Given - inject mocks
            RQO_cls_Application.Selector.setMock(mockSelector);

                // When
            RQO_cls_informationRequestService.sendEmail(true, informationRequestId, null, null);

                // Then
            ((RQO_cls_InformationRequestSelector) mocks.verify(mockSelector, 0)).
            selectById(new Set<Id> {informationRequestId});
        }
    }

    /**
    *   @name: RQO_cls_InformationRequestService_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que cubre la casuística para cuando se obtiene un template de Email. 
    *   Se comprueba si se retorna el template
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @IsTest
    private static void whenGetEmailTemplateReturnsTemplate()
    {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');

        System.runAs(u) {
            fflib_ApexMocks mocks = new fflib_ApexMocks();

                // Given
            RQO_cls_emailTemplateSelector mockSelector = 
            (RQO_cls_emailTemplateSelector) mocks.mock(RQO_cls_emailTemplateSelector.class);

                // Given - Configure mock responses
            Id informationRequestId = 
            fflib_IDGenerator.generate(RQO_obj_informationRequest__c.SObjectType);
            Id templateId = 
            fflib_IDGenerator.generate(EmailTemplate.SObjectType);
            List<EmailTemplate> templates = 
            new List<EmailTemplate> {
                new EmailTemplate(
                    Id=templateId,
                    Name = 'Test Template',
                    DeveloperName = 'Test Developer Name',
                    Body = 'Body',
                    Subject = 'Subject',
                    HtmlValue = '<html/>'
                    )
            };
            mocks.startStubbing();
            mocks.when(mockSelector.SObjectType()).thenReturn(EmailTemplate.SObjectType);
            mocks.when(true).thenReturn(templates);
            mocks.stopStubbing();

                // Given - inject mocks
            RQO_cls_Application.Selector.setMock(mockSelector);

                // When
            RQO_cls_informationRequestService.getEmailTemplate(
                new RQO_obj_informationRequest__c(
                    Id=informationRequestId,
                    Name = 'test@test.com',
                    RQO_fld_email__c = 'test@test.com',
                    RQO_fld_subject__c = 'Test Subject',
                    RQO_fld_message__c = 'Test Message',
                    RecordTypeId = FAQ_RECORD_TYPE
                    )
                );

                // Then
            ((RQO_cls_emailTemplateSelector) mocks.verify(mockSelector, 0)).
            selectByDeveloperName(new Set<String> { 'Test Developer Name' + 'es' });
        }
    }

}