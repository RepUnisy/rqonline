/**
 *	@name: RQO_cls_BorradoRegistros_sch
 *	@version: 1.0
 *	@creation date: 27/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase encargada de planificar y borrar los logs registrados en el sistema.
 */
global class RQO_cls_BorradoRegistros_sch extends RQO_cls_ProcesoPlanificado implements Schedulable { 

	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_BorradoRegistros_sch';
	private static final String JOB_NAME = 'BorradoHistoricoRegistros';

	// ***** CONSTRUCTORES ***** //
	/**
	* @creation date: 27/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Constructor por defecto
	* @param:
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_BorradoRegistros_sch() {
		super(JOB_NAME);
	}

	// ***** METODOS GLOBALES ***** //
	/**
	* @creation date: 18/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Executes the scheduled Apex job. 
	* @param:	context		Contains the job ID
	* @return: 
	* @exception: 
	* @throws: 
	*/
	global void execute(SchedulableContext context) {	
		
		/**
        * Constantes
        */
		final String METHOD = 'execute';	
		
		/**
		 * Inicio
		 */
		System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

		// Comprobar si se puede lanzar
		if (RQO_cls_AsyncApexJob_Helper.getActiveJobs() < RQO_cls_AsyncApexJob_Helper.MAX_JOBS && 
			RQO_cls_AsyncApexJob_Helper.getActiveBatch() < RQO_cls_AsyncApexJob_Helper.MAX_BATCH && isRunnable()) {
			// Programar par el dia siguiente           
			schedule(true);
		} else {
			// Programar para intervalo siguiente
			schedule(false);
		}

		lanzarProceso();

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}
	
	// ***** METODOS PUBLICOS ***** //

	/**
	* @creation date: 27/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Lanzamiento del proceso de borrado. 
	* @param:	
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public void lanzarProceso() {
		
		/**
        * Constantes
        */
		final String METHOD = 'lanzarProceso';	
		
		/**
		 * Inicio
		 */
		System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		//Obtención de custom settings con periodos establecidos
        RQO_cs_tiempoBorradoRegistros__c tbo = RQO_cls_CustomSettingUtil.getTiempoBorradoRegistros('RQO');

		 /**
		 * Tratamiento de objeto RQO_obj_auxBulkObject__c.
		 */
        if (String.isNotBlank(tbo.RQO_fld_auxBulkObject__c)) {
            Datetime fechaAuxBulkObj = Datetime.now().addDays(-Integer.valueOf(tbo.RQO_fld_auxBulkObject__c));
            List<RQO_obj_auxBulkObject__c> listaABO =[SELECT Id 
                                                        FROM RQO_obj_auxBulkObject__c 
                                                        WHERE CreatedDate < :fechaAuxBulkObj AND 
                                                            RQO_fld_verificado__c = true LIMIT 1];
    system.debug('listaABO: '+listaABO);
            system.debug('fechaAuxBulkObj: '+fechaAuxBulkObj);
            // Si existen objetos para borrar se llama al metodo
            for(RQO_obj_auxBulkObject__c resABO : listaABO) {
                borrarAuxBulkObject(fechaAuxBulkObj);
            }            
        } else {
            System.debug(CLASS_NAME + ' - ' + METHOD + ': No existe configuración para el borrado de RQO_obj_auxBulkObject__c');
        }
		
		/**
		 * Tratamiento de objeto RQO_obj_logProcesos__c.
		 */
        if (String.isNotBlank(tbo.RQO_fld_logProcesos__c)) {
            Datetime fechaLogProceso = Datetime.now().addDays(-Integer.valueOf(tbo.RQO_fld_logProcesos__c));
            List<RQO_obj_logProcesos__c> listaLP =[SELECT Id 
                                                        FROM RQO_obj_logProcesos__c 
                                                        WHERE CreatedDate < :fechaLogProceso LIMIT 1];
    
            // Si existen objetos para borrar se llama al metodo
            for(RQO_obj_logProcesos__c resLPR : listaLP) {
                borrarLogProcesos(fechaLogProceso);
            }            
        } else {
            System.debug(CLASS_NAME + ' - ' + METHOD + ': No existe configuración para el borrado de RQO_obj_logProcesos__c');
        }
		
		/**
		 * Tratamiento de objeto RQO_obj_gradoIntermedio__c.
		 */
        if (String.isNotBlank(tbo.RQO_fld_gradosIntermedios__c)) {
            Datetime fechaGradoIntermedio = Datetime.now().addDays(-Integer.valueOf(tbo.RQO_fld_gradosIntermedios__c));
            List<RQO_obj_gradoIntermedio__c> listaGI =[SELECT Id 
															FROM RQO_obj_gradoIntermedio__c 
															WHERE CreatedDate < :fechaGradoIntermedio LIMIT 1];
    
            // Si existen objetos para borrar se llama al metodo
            for(RQO_obj_gradoIntermedio__c resGRI : listaGI) {
                borrarGradosIntermedios(fechaGradoIntermedio);
            }            
        } else {
            System.debug(CLASS_NAME + ' - ' + METHOD + ': No existe configuración para el borrado de RQO_obj_gradoIntermedio__c');
        }
		 
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');

	}

	/**
	* @creation date: 27/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Programar job para la siguiente ejecucion. 
	* @param:	blnForceNewDay	Forzado de inicio en el mismo dia
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public void schedule(Boolean blnForceNewDay) {
		
		/**
		 * Variables 
		 */
		RQO_cls_ProcesoPlanificado.ScheduledInstance instance;

		/**
		 * Inicio
		 */
		instance = getSchedule(getNextScheduleDatetime(blnForceNewDay));

		System.schedule(instance.name, instance.cron, new RQO_cls_BorradoRegistros_sch());
	}
	
	// ***** METODOS PRIVADOS ***** //

	/**
	* @creation date: 27/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Busqueda y borrado de registros. 
	* @param:	fechaAuxBulkObj	Fecha de borrado
	* @return: 
	* @exception: 
	* @throws: 
	*/
	@future
	private static void borrarAuxBulkObject(Datetime fechaAuxBulkObj) {
		
		/**
        * Constantes
        */
		final String METHOD = 'borrarAuxBulkObject';
		
		/**
		 * Variables 
		 */
		Map<Id,RQO_obj_auxBulkObject__c> auxBulkObjectMap = null;
		
		/**
		 * Inicio
		 */
		System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

		auxBulkObjectMap = new Map<Id,RQO_obj_auxBulkObject__c> ([SELECT Id 
																	FROM RQO_obj_auxBulkObject__c 
																	WHERE CreatedDate < :fechaAuxBulkObj AND 
																		RQO_fld_verificado__c = true LIMIT 10000]);
																		
		deleteObjetos(auxBulkObjectMap.keySet(), 'RQO_obj_auxBulkObject__c');

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}
	
	/**
	* @creation date: 27/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Busqueda y borrado de registros. 
	* @param:	fechaAuxBulkObj	Fecha de borrado
	* @return: 
	* @exception: 
	* @throws: 
	*/
	@future
	private static void borrarLogProcesos(Datetime fechaLogProceso) {
		
		/**
        * Constantes
        */
		final String METHOD = 'borrarLogProcesos';
		
		/**
		 * Variables 
		 */
		Map<Id,RQO_obj_logProcesos__c> logProcesosMap = null;
		
		/**
		 * Inicio
		 */
		System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

		logProcesosMap = new Map<Id,RQO_obj_logProcesos__c> ([SELECT Id 
																	FROM RQO_obj_logProcesos__c 
																	WHERE CreatedDate < :fechaLogProceso LIMIT 10000]);
																		
		deleteObjetos(logProcesosMap.keySet(), 'RQO_obj_logProcesos__c');

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}
	
	/**
	* @creation date: 24/10/2017
	* @author: David Iglesias - Unisys
	* @description:	Busqueda y borrado de registros. 
	* @param:	fechaAuxBulkObj	Fecha de borrado
	* @return: 
	* @exception: 
	* @throws: 
	*/
	@future
	private static void borrarGradosIntermedios(Datetime fechaGradoIntermedio) {
		
		/**
        * Constantes
        */
		final String METHOD = 'borrarGradosIntermedios';
		
		/**
		 * Variables 
		 */
		Map<Id,RQO_obj_gradoIntermedio__c> gradosIntermediosMap = null;
		
		/**
		 * Inicio
		 */
		System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

		gradosIntermediosMap = new Map<Id,RQO_obj_gradoIntermedio__c> ([SELECT Id 
																			FROM RQO_obj_gradoIntermedio__c 
																			WHERE CreatedDate < :fechaGradoIntermedio AND 
																				RQO_fld_procesado__c = true LIMIT 10000]);
																		
		deleteObjetos(gradosIntermediosMap.keySet(), 'RQO_obj_gradoIntermedio__c');

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}

	/**
	* @creation date: 27/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Borrado de registros
	* @param:	ids		Lista de ids de objetos a borrar
	* @param:	objeto	Tipo de objetos a borrar
	* @return: 	
	* @exception: 
	* @throws: 
	*/
	private static void deleteObjetos(Set<Id> ids, String objeto) {

		/**
        * Constantes
        */
		final String METHOD = 'deleteObjetos';
		
		/**
		 * Inicio
		 */
		System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		System.debug(CLASS_NAME + ' - ' + METHOD + ': Borrando lista de objetos: '+objeto);
		
		// Borrado de objetos
		Database.DeleteResult[] drList = Database.delete(new List<Id>(ids), false);

		// Iteracion de resultados devueltos en el borrado
		for(Database.DeleteResult dr : drList) {
			if (!dr.isSuccess()) {
				// Operacion falla, Se pintan los errores               
				for(Database.Error err : dr.getErrors()) {
					System.debug('Ocurrio el siguiente error: '+err.getStatusCode() + ': ' + err.getMessage());                    
					System.debug('Campos afectados por el error: ' + err.getFields());
				}
			}
		}
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}

}