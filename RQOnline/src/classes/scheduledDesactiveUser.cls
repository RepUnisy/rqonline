global class scheduledDesactiveUser implements Schedulable {
   
    public static integer daysFromDesactive = -75; 
    public static integer daysFromEmail  = -60;    
        
    global void execute(SchedulableContext sc) {
        desactiveUsers();
    }
    @future
    global static void desactiveUsers(){        
        Datetime myDT = DateTime.now();
        Datetime myDTMax = myDT.addDays(daysFromDesactive);    
        Date myDateMax = date.newinstance(myDTMax.year(), myDTMax.month(), myDTMax.day());
        Datetime myDTWaring = myDT.addDays(daysFromEmail);
        Date myDateWaring = date.newinstance(myDTWaring.year(), myDTWaring.month(), myDTWaring.day());
        //SI LO USUARIOS ESTAN 60 DIAS SE LES ENVIARA UN CORREO
        //List<User> users2 = [SELECT Id,isActive,email,LastLoginDate FROM User WHERE  id = '005b00000029GdA' ];
        //User userr = users2.get(0);
        //System.debug('FECYHA: ' + string.valueOfGmt(userr.LastLoginDate) );                
        List<User> users = [SELECT Id,isActive,email,LanguageLocaleKey FROM User WHERE  DAY_ONLY(LastLoginDate) = :myDateWaring and isActive=True and LanguageLocaleKey='es' ];
        List<User> usersEng = [SELECT Id,isActive,email,LanguageLocaleKey FROM User WHERE  DAY_ONLY(LastLoginDate) = :myDateWaring and isActive=True and LanguageLocaleKey!='es'];
        
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName  = 'Ayuda Red Social'];
     
        System.debug('OrgWideEmailAddress: ' +owea.get(0).Id);
        System.debug('Fecha de email: ' + string.valueOfGmt(myDateWaring) );
        System.debug('Numero de usuarios es para enviar el correo: ' + users.size());
        System.debug('Numero de usuarios distinto es para enviar el correo: ' + usersEng.size());

        sendMail(users, 'Aviso desactivación usuario chatter','<html><head></head><body>Hola,<br/><br/>Hemos detectado que no has entrado en Chatter en los últimos 60 días. Con el fin de ser más eficiente en nuestros procesos, queremos avisarte que no accedes en los próximos 15 días a la red social, procederemos a desactivar tu usuario.<br/><br/>Si deseas nuevamente volver a formar parte de nuestra comunidad de Chatter, simplemente accede a través del siguiente <a href="https://repsol.my.salesforce.com">link</a> y  tu usuario será activado de nuevo. <br/> <br/>Para cualquier duda o consulta estamos disponibles en el buzón AYUDA RED SOCIAL ayudaredsocial@repsol.com.</body></html> ',owea.get(0).Id);        
        sendMail(usersEng ,'Notice of disabling your Chatter user','<html><head></head><body>Hi,<br/><br/> We have detected that you have not logged in Chatter in the last 60 days. In order to be more efficient in our processes, we inform you that if you do not log in the next 15 days to the social network, we will disable your user. <br/> <br/>If you want to be part of our Chatter community again you only have to access through the following <a href="https://repsol.my.salesforce.com">link</a> and your user will be activated again. <br/><br/> For any question we are at your disposal in the following mailbox, SOCIAL SUPPORT NETWORK  ayudaredsocial@repsol.com.  </body></html>',owea.get(0).Id);                
     
        //SI LOS USUARIOS ESTAN MAS DE 75 DIAS SE DESACTIVA
        users = [SELECT Id,isActive FROM User WHERE  DAY_ONLY(LastLoginDate) = :myDateMax and isActive=True];
         System.debug('Numero de usuarios para desactivar: ' + users.size());
        for (User user : users){
            System.debug('scheduledDesactiveUser: Desactivamos el usuario ' + user.Id);
            user.isActive = false;
        }  
        try
        {
            Database.SaveResult[] results = database.update(users, false);                    
        }
        Catch (Exception ex){
            System.debug('desactiveUsers --> update users : ' + ex.getMessage());
        }
        
    }
    
    public static void sendMail(List<User> users, string subject,string body, Id emailFrom){
        List<Messaging.SingleEmailMessage > allMails = new List<Messaging.SingleEmailMessage >();
        for (User user : users){
            System.debug('Usurio a enviar correo: ' +  user.Email);
            String email = user.Email;
            if (!String.isEmpty(email))
            {        
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {email});                            
                mail.setSubject(subject);
                //mail.setPlainTextBody(body);
                mail.setHtmlBody(body);
                mail.setOrgWideEmailAddressId(emailFrom);
                
                allMails.add(mail);  
                System.debug('scheduledDesactiveUser: Enviamos email a ' + email);                    
            }
        }
        if (allMails.size()>0)
        {
            
            Messaging.sendEmail(allMails);  
        }        
    }       
}