public class PT_controller_contraprestacionesPopUp {
   
    public Patrocinios__c patrocinio {get; set;}
   
    public PT_controller_contraprestacionesPopUp(ApexPages.StandardController controller){
                    
        system.debug('@@@ PT_controller_contraprestacionesPopUp con Id patrocinio: '+ ApexPages.currentPage().getParameters().get('Id'));
      
  
        patrocinio = [select
                      id,   
                      Name, 
                      contraprestacion_reutilizacion__c,                    
                      contraprestacion_campania__c,           
                      contraprestacion_presencia_en_espacios__c,                      
                      contraprestacion_invitaciones_entradas__c,
                      contraprestacion_desgravaciones_fiscales__c,  
                      contraprestacion_uso_instalaciones__c,
                      contraprestacion_otros__c
                      from Patrocinios__c 
                      where id = :ApexPages.currentPage().getParameters().get('Id')];
              
    }
}