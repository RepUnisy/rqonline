/**
*	@name: RQO_cls_cabecerasPaginas_Test
*	@version: 1.0
*	@creation date: 27/10/2017
*	@author: Nicolás García - Unisys
*	@description: Clase de test para probar la clase RQO_cls_CabecerasPaginas_cc
*/
@IsTest
private class RQO_cls_CabecerasPaginas_test {
    /**
    *	@name: initialSetup
    *	@version: 1.0
    *	@creation date: 20/02/2018
    *	@author: Julio Maroto - Unisys
    *	@description: Setup testing method for required initial dataset
	*/
    @testSetup 
    static void initialSetup() {
        // Create user
        createUser();
        
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createTranslation(200);
            RQO_cls_TestDataFactory.createQP0Grade(200);
            RQO_cls_TestDataFactory.createGrade(200);
        }
    }
    
    /**
    *	@name: createUser
    *	@version: 1.0
    *	@creation date: 20/02/2018
    *	@author: Julio Maroto - Unisys
    *	@description: Testing method for user context creation
    */
    private static void createUser() {
        RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null, 'es', 'es_ES','Europe/Berlin');
    }
    
    /**
    *	@name: testCabecerasPaginas
    *	@version: 1.0
    *	@creation date: 20/02/2018
    *	@author: Julio Maroto - Unisys
    *	@description: Method for testing page headers
	*/
    @isTest 
    static void testCabecerasPaginas() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u) {
            RQO_cls_TestDataFactory.createCScabecerasPaginas('nombrePrueba', 'imagen1', 'nombre1');
            RQO_cls_TestDataFactory.createCScabecerasPaginas(Label.RQO_lbl_default_const, 'imagen1', 'nombre1');
            List<RQO_obj_grade__c> gradeList = [SELECT Id FROM RQO_obj_grade__c LIMIT 1];
    		List<RQO_obj_translation__c> listTranslation = [SELECT Id FROM RQO_obj_translation__c LIMIT 1];
            listTranslation[0].RQO_fld_grade__c = gradeList[0].Id;
            listTranslation[0].RQO_fld_locale__c = 'en_US';
            update listTranslation[0];
            List<String> listaCustomSetting = new List<String>(RQO_cls_CabecerasPaginas_cc.getPageNameGrade('nombrePrueba', gradeList[0].Id, 'en_US'));            
            listaCustomSetting = RQO_cls_CabecerasPaginas_cc.getPageName('nombrePrueba');
            System.assertEquals('nombre1', listaCustomSetting[0], 'El valor devuelto del nombre de cabecera de la Custom setting no es correcto');
            System.assertEquals('imagen1', listaCustomSetting[1], 'El valor devuelto de la imagen de la Custom setting no es correcto');
            
            listaCustomSetting = RQO_cls_CabecerasPaginas_cc.getPageNameGrade('PruebaError', gradeList[0].Id, 'en_US');
            listaCustomSetting = RQO_cls_CabecerasPaginas_cc.getPageName('PruebaError');
            System.assertEquals('nombre1', listaCustomSetting[0], 'El valor devuelto del nombre de cabecera de la Custom setting no es correcto');
            System.assertEquals('imagen1', listaCustomSetting[1], 'El valor devuelto de la imagen de la Custom setting no es correcto');
        } 
    }
}