/**
 *	@name: RQO_cls_GradeTriggerHandler
 *	@version: 1.0
 *	@creation date: 23/01/2018
 *	@author: David Iglesias - Unisys
 *	@description: Clase controladora del trigger para RQO_obj_grade__c
 */
public class RQO_cls_GradeTriggerHandler {

    // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_GradeTriggerHandler';
    private static RQO_cls_GradeTriggerHandler handler;
	private List<String> contactsList = null;

	// ***** CONSTRUCTORES ***** //
	public static RQO_cls_GradeTriggerHandler getHandler() {
		if (handler == null) {
			handler = new RQO_cls_GradeTriggerHandler();
		}
		return handler;
	}
    
	// ***** METODOS PUBLICOS ***** //
	      
    /**
	* @creation date: 23/01/2018
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de procesar las inserciones del objeto RQO_obj_grade__c
	* @param:	listNew		Lista de objetos posterior a la inserción
	* @param:	mapNew		Mapa de objetos posterior a la inserción
	* @return: 	
	* @exception: 
	* @throws: 
	*/
    public void afterInsert (List<RQO_obj_grade__c > listNew, Map<Id, RQO_obj_grade__c > mapNew) {
		
        /**
         * Constantes
         */
        final String METHOD = 'afterInsert';
	
		/**
		 * Variables
		 */
		RQO_obj_notification__c notification = null;
		List<RQO_obj_notification__c> notificationList = new List<RQO_obj_notification__c> ();

        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');

		// Obtención de los contactos que van a ser comunicados
		if (this.contactsList == null) {
			this.contactsList = RQO_cls_GeneralUtilities.getContactRQO();
		}
        
		// Se recorren los grados insertados
        for (RQO_obj_grade__c itemGrade : listNew) {
			// Se recorren todos los contactos existentes en BBDD
			for (String itemContact : this.contactsList) {
				notification = new RQO_obj_notification__c();
				notification.RQO_fld_contact__c = itemContact;
				notification.RQO_fld_externalId__c = itemGrade.Id;
				notification.RQO_fld_objectType__c = RQO_cls_Constantes.COD_SF_NOT_GRADO;
				notification.RQO_fld_messageType__c = RQO_cls_Constantes.COD_PROD_NEW_GRADO;
				
				// Lista de notificaciones a insertar
				notificationList.add(notification);
			}            
        }
		
		// Inserción de los registros de notificaciones
		if (!notificationList.isEmpty()) {
			insert notificationList;
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');

    }
    
    /**
	* @creation date: 23/01/2018
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de procesar las actualizaciones del objeto RQO_obj_grade__c
	* @param:	listNew		Lista de objetos posterior a la inserción
	* @param:	listOld		Lista de objetos anterior a la inserción
	* @param:	mapNew		Mapa de objetos posterior a la inserción
	* @param:	mapOld		Mapa de objetos anterior a la inserción
	* @return: 	
	* @exception: 
	* @throws: 
	*/
    public void afterUpdate (List<RQO_obj_grade__c> listNew, List<RQO_obj_grade__c> listOld, Map<Id, RQO_obj_grade__c> mapNew, Map<Id, RQO_obj_grade__c> mapOld) {
		
        /**
         * Constantes
         */
        final String METHOD = 'afterUpdate';
	
		/**
		 * Variables
		 */
		RQO_obj_notification__c notification = null;
		List<RQO_obj_notification__c> notificationList = new List<RQO_obj_notification__c> ();

        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		// Obtención de los contactos que van a ser comunicados
		if (this.contactsList == null) {
			this.contactsList = RQO_cls_GeneralUtilities.getContactRQO();
		}
		
		// Se recorren los grados actualizados
		for (String itemKeyMap : mapNew.keySet()) {
			// Se comprueba que el grado se marca como recomendado y anteriormente no lo era
			if (mapNew.get(itemKeyMap).RQO_fld_recommendation__c && !mapOld.get(itemKeyMap).RQO_fld_recommendation__c) {
				// Se recorren todos los contactos existentes en BBDD
				for (String itemContact : this.contactsList) {
					notification = new RQO_obj_notification__c();
					notification.RQO_fld_contact__c = itemContact;
					notification.RQO_fld_externalId__c = itemKeyMap;
					notification.RQO_fld_objectType__c = RQO_cls_Constantes.COD_SF_NOT_GRADO;
					notification.RQO_fld_messageType__c = RQO_cls_Constantes.COD_PROD_RECOM_GRADO;
					
					// Lista de notificaciones a insertar
					notificationList.add(notification);
				}
			}
		}
		
		// Inserción de los registros de notificaciones
		if (!notificationList.isEmpty()) {
			insert notificationList;
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');

    }
    
}