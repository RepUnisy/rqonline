/**
 *	@name: RQO_cls_WSCalloutResultBean
 *	@version: 1.0
 *	@creation date: 07/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase para almacenar el resultado del proceso de la llamada a un servicio web
 *
 */
public class RQO_cls_WSCalloutResultBean {

	// ***** PROPIEDADES ***** //
	public Boolean resultOK { get; set; }
	public Integer statusCode { get; set; }
	public String code { get; set; }
	public String errorMessage { get; set; }


	// ***** CONSTRUCTORES ***** //
	/**
	* @creation date: 07/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Constructor por defecto
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_WSCalloutResultBean() {
		this.resultOK = false;
		this.statusCode = null;
		this.code = null;
		this.errorMessage = null;
	}

}