/*------------------------------------------------------------
Author:          Agustín Andrés
Company:         Unisys
Description:     
Clase encargada de actualizar 
Test Class:      
History
<Date>          <Author>            <Change Description>
17-01-2017      Agustín Andrés      Initial Version
------------------------------------------------------------*/
public class PT_actualizaPatrocinios {

    @future
    public static void actualizaPatrocinios(id idUsuario, string Rol){
        
        list<Patrocinios__c> listaPatrocinios = [SELECT id, Solicitante__c, CreatedById FROM Patrocinios__c WHERE CreatedById = :idUsuario];
        
        for(Patrocinios__c p : listaPatrocinios){
            p.Area_Calculada__c = Rol;
        }
        
        if (Test.isRunningTest()) {
            System.runAs(new User(Id = Userinfo.getUserId())) {
                update listaPatrocinios;
            } 
        } 
        else {
            update listaPatrocinios;
        }
    }    
}