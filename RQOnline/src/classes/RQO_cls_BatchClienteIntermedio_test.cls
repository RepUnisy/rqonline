/**
*	@name: RQO_cls_BatchClienteIntermedio_test
*	@version: 1.0
*	@creation date: 25/10/2017
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar la migración de clientes
*/
@IsTest
public class RQO_cls_BatchClienteIntermedio_test {
    
    /**
	* @creation date: 25/10/2017
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
         User u = RQO_cls_TestDataFactory.userCreation('sfTestUserCliente@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionObjBulk('Account', new List<String> {'RQO_fld_agrupacion__c','RQO_fld_tipodeInterlocutor__c','Name','LastName_NoUsable',
                'RQO_fld_guiddeInterlocutorComercial__c','RQO_fld_idDireccion__c',
                'RQO_fld_guidDireccion__c','RQO_fld_calleNumero__c','RQO_fld_poblacion__c',
                'RQO_fld_codigoPostal__c','BillingCountryCode', 'RQO_fld_region__c', 'RQO_fld_husoHorario__c',
                'RQO_fld_zonadeTransporte__c','RecordType','RQO_fld_name3__c', 'RQO_fld_name3__c', 'RQO_fld_name3__c', 'RQO_fld_name3__c', 'RQO_fld_name3__c', 
                'RQO_fld_name3__c', 'RQO_fld_name3__c', 'RQO_fld_name3__c', 'RQO_fld_name3__c', 'RQO_fld_name3__c'});
            
            RQO_cls_TestDataFactory.createCSParametrizacionObjBulk('RQO_obj_identificador__c', new List<String> {'RQO_fld_tipodeIdentificador__c'});
            
            RQO_cls_TestDataFactory.createCSParametrizacionObjBulk('RQO_obj_areadeVentas__c', new List<String> {'RQO_fld_organizaciondeVentas__c','RQO_fld_canal__c','RQO_fld_sector__c',
                'RQO_fld_incoterm__c','RQO_fld_oficinadeVentas__c','RQO_fld_grupodeVendedores__c','RQO_fld_zonadeVentas__c',
                'RQO_fld_grupodeClientes__c','RQO_fld_grupodeClientes2__c','RQO_fld_grupodeClientes3__c',
                'RQO_fld_grupodeClientes4__c','RQO_fld_grupodeClientes5__c'});
            
            RQO_cls_TestDataFactory.createCSParametrizacionObjBulk('RQO_obj_relaciondeVentas__c', new List<String> {'RQO_fld_cliente__c','RQO_fld_relacionTipo__c'});
            
            RQO_cls_TestDataFactory.createCSParametrizacionObjBulk('Contact', new List<String> {'RQO_fld_funcion__c','RQO_fld_departamento__c','FirstName','LastName'});
            
            RQO_cls_TestDataFactory.createCSParametrizacionObjBulk('RQO_obj_datodeContacto__c', new List<String> {'RQO_fld_pais__c','RQO_fld_principal__c','RQO_fld_value__c',
                'Record Type'});
            
            RQO_cls_TestDataFactory.createCSParametrizacionObjBulk('RQO_obj_datosdeConsentimiento__c', new List<String> {'RQO_fld_consentimiento__c','RQO_fld_canal__c','RQO_fld_forma__c',
                'RQO_fld_fecha__c'});
            
            RQO_cls_TestDataFactory.createCSParametrizacionObjBulk('RQO_obj_atributodeMarketing__c', new List<String> {'RQO_fld_value__c','RQO_fld_valueNeutral__c','RQO_fld_description__c',
                'RQO_fld_charact__c'});
            
            RQO_cls_TestDataFactory.createCSRTAccount();
            RQO_cls_TestDataFactory.createOrganizaciondeVentas(1);
            RQO_cls_TestDataFactory.createAuxBulkObject(200);								
        }
    }
    
    /**
	* @creation date: 25/10/2017
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta en la migración de clientes y sus relaciones
	* @exception: 
	* @throws: 
	*/
    @isTest
    static void crearClienteRelacionesOK(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserCliente@testorg.com');
        System.runAs(u){
            RQO_obj_auxBulkObject__c auxObj = [SELECT Id FROM RQO_obj_auxBulkObject__c LIMIT 1];
            auxObj.RQO_fld_columna17__c = 'item';
            auxObj.RQO_fld_columna18__c = 'item';
            auxObj.RQO_fld_columna19__c = 'item';
            auxObj.RQO_fld_columna20__c = 'item';
            auxObj.RQO_fld_columna21__c = 'item';
            auxObj.RQO_fld_columna22__c = 'item';
            auxObj.RQO_fld_columna23__c = 'item';
            auxObj.RQO_fld_columna24__c = 'item';
            auxObj.RQO_fld_columna25__c = 'item';
            update auxObj;
            test.startTest();
            //Lanzamiento del proceso
			Id batchJobId = Database.executeBatch(new RQO_cls_ClienteIntermedio_batch(), 2000);
            test.stopTest();
            
			List <Account> accList = [SELECT Id FROM Account];
            
			// Comprobaciones
			List<AsyncApexJob> jobs = [SELECT Status FROM AsyncApexJob WHERE Id = :batchJobId];
			System.assertEquals('Completed', jobs[0].Status);
			System.assertEquals(200, accList.size());
        }
	}
    
    /**
	* @creation date: 25/10/2017
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad incorrecta en caso de formato erroneo de fecha en un campo
	* @exception: 
	* @throws: 
	*/
    @isTest
    static void crearClienteFormatoDateKO(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserCliente@testorg.com');
        System.runAs(u){
            test.startTest();
            
            RQO_obj_auxBulkObject__c aux = [SELECT Id FROM RQO_obj_auxBulkObject__c WHERE RQO_fld_tipo__c='08'];
            aux.RQO_fld_columna4__c = '234';
            update aux;
            
            //Lanzamiento del proceso
			Id batchJobId = Database.executeBatch(new RQO_cls_ClienteIntermedio_batch(), 2000);
            											
            test.stopTest();
            
			List <RQO_obj_logProcesos__c> auxLogList = [SELECT Id, RQO_fld_errorMessage__c FROM RQO_obj_logProcesos__c];
            system.debug('XXX --> '+auxLogList);
			// Comprobaciones
			System.assertEquals(Label.RQO_lbl_mensajeErrorFechaIntegracion + aux.RQO_fld_columna4__c, auxLogList[0].RQO_fld_errorMessage__c);
        }
	}
    
    /**
	* @creation date: 25/10/2017
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad incorrecta en caso de tipo de objeto erroneo
	* @exception: 
	* @throws: 
	*/
    @isTest
    static void errorTipoObjeto(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserCliente@testorg.com');
        System.runAs(u){
            
            RQO_obj_auxBulkObject__c aux = [SELECT Id FROM RQO_obj_auxBulkObject__c WHERE RQO_fld_tipo__c='08'];
            aux.RQO_fld_tipo__c = '07';
            update aux;
            system.debug('XXX '+aux);
            
            test.startTest();
            //Lanzamiento del proceso
			Id batchJobId = Database.executeBatch(new RQO_cls_ClienteIntermedio_batch(), 2000);
            test.stopTest();
            
			List <RQO_obj_logProcesos__c> auxLogList = [SELECT Id, RQO_fld_errorMessage__c FROM RQO_obj_logProcesos__c];
            
			// Comprobaciones
			System.assertEquals(Label.RQO_lbl_mensaleErrorTipoObjetoIntegracion + aux.RQO_fld_tipo__c, auxLogList[0].RQO_fld_errorMessage__c);
        }
	}

}