/**
 *	@name: RQO_cls_PosicionLoteBean
 *	@version: 1.0
 *	@creation date: 16/10/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de Posiciones de Lote.
 */
public class RQO_cls_PosicionLoteBean {

    public String externalId {get; set;}
    public String posicionEntrega {get; set;}
    public String posicionLote {get; set;}
    public String cantidadSalidaMercancias {get; set;}
    public String unidad {get; set;}
    public String isDelete {get; set;}
    
}