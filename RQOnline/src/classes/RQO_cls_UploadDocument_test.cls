/**
*	@name: RQO_cls_UploadDocument_test
*	@version: 1.0
*	@creation date: 12/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar los métodos asociados a UploadDocument
*/
@IsTest
public class RQO_cls_UploadDocument_test {
    
	/**
    *	@creation date: 12/2/2018
    *	@author: Juan Elías - Unisys
    *   @description:	Método que genera el conjunto de datos inicial necesario para el test
    *   @exception: 
    *   @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
            RQO_cls_TestDataFactory.createCSDocumentumRepository();
        }
        
    }
    
    /**
    *	@creation date: 12/2/2018
    *	@author: Juan Elías - Unisys
    *   @description:	Método test para probar UploadDocToRepository
    *   @exception: 
    *   @throws: 
    */
    @isTest
    static void testUploadDocToRepository(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String locale, documentSFId;
            RQO_cls_TestDataFactory.createContact(20);
            List<RQO_obj_grade__c> grade = RQO_cls_TestDataFactory.createGrade(1);
            
            String idGrade = grade[0].Id;
            String tipoDoc = 'txt';
            String docBody = '';
            String docName = 'Test.txt';
            test.startTest();
            RQO_cls_UploadDocument_cc.uploadDocToRepository(idGrade, docName, tipoDoc, docBody, locale, documentSFId);
            System.assertEquals(false, grade.isEmpty());
            test.stopTest();
        }
    }
    
    /**
    *	@creation date: 12/2/2018
    *	@author: Juan Elías - Unisys
    *   @description:	Método test para probar LoadPicklistDocType
    *   @exception: 
    *   @throws: 
    */
    @isTest
    static void testLoadPicklistDocType(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
            Map<String, String> esperado = util.getDocumentType();
            RQO_cs_DocumentUtilities__c objAux = null;
            for(String key : esperado.keySet()){
                objAux = RQO_cs_DocumentUtilities__c.getInstance(key);
                if (objAux == null || !objAux.RQO_fld_allowUpload__c){
                    esperado.remove(key);
                }
            }
            test.startTest();
            Map<String, String> resultado = RQO_cls_UploadDocument_cc.loadPicklistDocType();
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
    /**
    *	@creation date: 12/2/2018
    *	@author: Juan Elías - Unisys
    *   @description:	Método test para probar LoadPicklistDocLocale
    *   @exception: 
    *   @throws: 
    */
    @isTest
    static void testLoadPicklistDocLocale(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            Map<String, String> esperado;
            RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
            esperado = util.getDocumentLocale();
            test.startTest();
            Map<String, String> resultado = RQO_cls_UploadDocument_cc.loadPicklistDocLocale();
            System.assertEquals(esperado, resultado);            
            test.stopTest();
        }
    }
    
    /**
    *	@creation date: 12/2/2018
    *	@author: Juan Elías - Unisys
    *   @description:	Método test para probar ValidateDocExist
    *   @exception: 
    *   @throws: 
    */
    @isTest
    static void testValidateDocExist(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String esperado;
            String idGrade, docType, locale;
            test.startTest();
            String resultado = RQO_cls_UploadDocument_cc.validateDocExist(idGrade, docType, locale);
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
}