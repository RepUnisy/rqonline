@IsTest
public class RQO_cls_MiPerfilController_test {
	
    /**
     * Método inicial de carga de datos que se comparten entre todos los testMethods.
     */
	@testSetup static void initialSetup() {
		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(u){
			// Crear email templates RQO_eplt_correoNuevoContactoSAC_ RQO_eplt_correoNuevoContactoMaster_
			RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_correoNuevoContactoSAC_en', 'RQO_eplt_correoNuevoContactoMaster_en'}); 
		}
	}
	
	@isTest
    public static void WhenSelectAccountByIdReturnsData() {
        User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            List<Account> account = RQO_cls_TestDataFactory.createAccount(1);
            
            Account accountResult = RQO_cls_MiPerfilController_cc.selectAccountById(account[0].Id);
            
            System.assertNotEquals(null,
                                accountResult,
                                'No se devuelven los datos esperados.');
            }
 	}
    
	@isTest
    public static void WhenSelectContactByIdDoesNotReturn() {
        User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Id contactId = 
                fflib_IDGenerator.generate(Contact.SObjectType);
            
            Contact contactResult = RQO_cls_MiPerfilController_cc.selectContactById(contactId);
            
            System.assertEquals(null,
                                contactResult,
                                'Se devuelven datos no esperados.');
            }
 	}
 	
 	@isTest
    public static void WhenSelectPermissionsReturnsPermissions() {
        User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            string profile = RQO_cls_MiPerfilController_cc.selectAllProfiles()[0];
            
            List<RQO_cmt_userProfilePermission__mdt> permissions = RQO_cls_MiPerfilController_cc.selectPermissions(profile);
            
            System.assertNotEquals(0,
                                    permissions.size(),
                                    'No se han devuelto permisos para un perfil existente.');
		}
 	}
 	
 	@isTest
    public static void WhenSelectAllProfilesReturnsProfiles() {
        User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Set<string> values = new RQO_cls_UserProfileSelector().selectProfiles();
            List<String> profiles = new List<String>();
            profiles.addAll(values);
            
            System.assertEquals(profiles,
                                    RQO_cls_MiPerfilController_cc.selectAllProfiles(),
                                    'No se devuelven los perfiles esperados.');
		}
    }
    
    @isTest
    public static void WhenSelectProfilesReturnsProfiles() {
        User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Map<String, String> options = new Map<String, String>();
	    	Schema.DescribeFieldResult fieldResult = RQO_cmt_userProfilePermission__mdt.RQO_fld_userProfile__c.getDescribe();
	        List<Schema.PicklistEntry> profiles = fieldResult.getPicklistValues();
	        for( Schema.PicklistEntry profile : profiles){
	            options.put(profile.getValue(), profile.getLabel());
	        }
            
            System.assertEquals(options,
                                    RQO_cls_MiPerfilController_cc.selectProfiles(),
                                    'No se devuelven los perfiles esperados.');
		}
    }
    
    @isTest
    public static void WhenSelectAllPermissionsReturnsPermissions() {
        User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
        	Set<string> values = new RQO_cls_UserProfileSelector().selectPermissionsLabels();
	    	List<String> permissions = new List<String>();
			permissions.addAll(values);
            
            System.assertEquals(permissions,
                                    RQO_cls_MiPerfilController_cc.selectAllPermissions(),
                                    'No se devuelven los permisos esperados.');
            }
    }
    
    @isTest
    public static void WhenSelectPermissionsByProfileReturnsPermissions() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            string profile = RQO_cls_MiPerfilController_cc.selectAllProfiles()[0];
            
            List<String> permissions = RQO_cls_MiPerfilController_cc.selectPermissionsByProfile(profile);
            
            System.assertNotEquals(0,
                                    permissions.size(),
                                    'No se han devuelto permisos para un perfil existente.');
            }
 	}
 	
	@isTest
    public static void WhenSelectUsersByAccountIdDoesNotReturn() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Id accountId = 
                fflib_IDGenerator.generate(Account.SObjectType);
            
            List<User> usersResult = RQO_cls_MiPerfilController_cc.selectUsersByAccountId(accountId);
            
            System.assertEquals(0,
                                usersResult.size(),
                                'Se devuelven datos no esperados.');
            }
 	}
 	
	@isTest
    public static void WhenSelectUsersByNameDoesNotReturn() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Id accountId = 
                fflib_IDGenerator.generate(Account.SObjectType);
            
            List<User> usersResult = RQO_cls_MiPerfilController_cc.selectUsersByName(accountId, null);
            
            System.assertEquals(0,
                                usersResult.size(),
                                'Se devuelven datos no esperados.');
            }
 	}
 	
 	@isTest
    public static void WhenRegisterContactThrowsException() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Boolean DidThrowException = false;
            
            try {
                
                Id accountId = 
                    fflib_IDGenerator.generate(Account.SObjectType);
                
                RQO_cls_MiPerfilController_cc.registerContact(String.valueOf(accountId), 'Nombre', 'Primer Apellido', 'Segundo Apellido',
                                                              'test@test.test', '666666666', RQO_cls_MiPerfilController_cc.selectAllProfiles()[0]);
            }
            catch (AuraHandledException e) {
                DidThrowException = true;
            }
            
            System.assertEquals(DidThrowException,
                                true, 
                                'No se ha generado el error al enviar una cuenta incorrecta.');
            }
    }
    
	@isTest
    public static void WhenRegisterContactDoesNotThrowException() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Boolean DidThrowException = false;
            
            try {
                
                List<Account> account = RQO_cls_TestDataFactory.createAccount(1);
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 'es',
                                                       'es_ES', 'Europe/Berlin', contact[0].Id);
                System.runAs(u){
                    RQO_cls_MiPerfilController_cc.registerContact(String.valueOf(account[0].Id), 'Nombre', 'Primer Apellido', 'Segundo Apellido',
                                                              'test@test.test', '666666666', 'Máster');
                }
            }
            catch (AuraHandledException e) {
                System.debug('WhenRegisterContactDoesNotThrowException: ' + e.getMessage());
                DidThrowException = true;
            }
            
            System.assertEquals(DidThrowException,
                                true, 
                                'Se ha generado una excepción no esperada.');
            
            }
    }
    
    @isTest
    public static void WhenUpdateProfileDoesNotThrowException() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Boolean DidThrowException = false;
            
            try {
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 
                                                                    'es','es_ES', 'Europe/Berlin', contact[0].Id);
                //User u = RQO_cls_TestDataFactory.userCreation('sfTestUserUnregister@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
                
                RQO_cls_MiPerfilController_cc.updateProfile(new List<User>{u});
            }
            catch (AuraHandledException e) {
                DidThrowException = true;
            }
            
            System.assertEquals(DidThrowException,
                                false, 
                                'Se ha generado una excepción no esperada.');
            }
    }
    
    @isTest
    public static void WhenUpdateLanguageUpdatesLanguage() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            RQO_cls_MiPerfilController_cc.updateLanguage('de');
            
            System.assertEquals(UserInfo.getLanguage(),
                                'de', 
                                'No se ha actualizado el idioma correctamente.');
    	}
	}
    
   	@isTest
    public static void WhenGetNotificationPeriodicityReturnsPeriodicities() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            List<String> values = new List<String>();
            List<SelectOption> options = new RQO_cls_GeneralUtilities().getNotificationPeriodicity();
            for (SelectOption option: options)
                values.add(option.getValue());
            
            System.assertEquals(values,
                                RQO_cls_MiPerfilController_cc.getNotificationPeriodicity(), 
                                'No se devuelven las periodicidades correctas.');
          }  
    	
    }
    
    @isTest
    public static void WhenGetNotificationPeriodicitiesReturnsPeriodicities() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Map<String,String> values = new Map<String,String>();
	    	List<SelectOption> options = new RQO_cls_GeneralUtilities().getNotificationPeriodicity();
	    	for (SelectOption option: options)
	    		values.put(option.getValue(), option.getLabel());
            
            System.assertEquals(values,
                                RQO_cls_MiPerfilController_cc.getNotificationPeriodicities(), 
                                'No se devuelven las periodicidades correctas.');
          }  
    	
    }
    
    @isTest
    public static void WhenGetNotificationConsentsReturns() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
            
            List<SelectOption> types = new RQO_cls_GeneralUtilities().getNotificationTypes();
    
            System.assertNotEquals(0,
                                    RQO_cls_MiPerfilController_cc.getNotificationConsents(contact[0].Id).size(), 
                                    'No devuelve los consentimientos correctos.');
		} 
    }
    
    @isTest
    public static void WhenUpdateNotificationConsentsWithNoConsents() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
            contact[0].RQO_fld_surveyConsent__c = true;
            update contact[0];
            
            List<RQO_obj_notificationConsent__c> previousConsents = 
                new RQO_cls_NotificationConsentSelector().selectByContactId(new Set<ID> { contact[0].Id });
                
            RQO_cls_MiPerfilController_cc.updateNotificationConsents('[]');
            
            List<RQO_obj_notificationConsent__c> afterConsents = 
                new RQO_cls_NotificationConsentSelector().selectByContactId(new Set<ID> { contact[0].Id });
    
            System.assertEquals(previousConsents.size(),
                                   afterConsents.size(), 
                                    'Se han modificado consentimientos no enviados.');
            }
    }
    
    @isTest
    public static void WhenunregisterUserThrowsException() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
        System.runAs(usuario){
            Boolean DidThrowException = false;
            
            try {
                
                //User u = RQO_cls_TestDataFactory.userCreation('sfTestUserUnregister@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
                
                RQO_cls_MiPerfilController_cc.unregisterUser(usuario.Id);
            }
            catch (AuraHandledException e) {
                DidThrowException = true;
            }
            
            System.assertEquals(DidThrowException,
                                true, 
                                'No se ha generado la excepción esperada.');
            }
    }
}