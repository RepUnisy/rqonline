/**
 * @name: RRSS_cls_handlerAfterInsertCase
 * @version: 1.0
 * @creation date: 14/06/2017
 * @author: Ramon Diaz, Accenture
 * @description: Clase Handler para Trigger After Insert de Casos - Asigna un Entitlement genérico a casos de RRSS
*/

public class RRSS_cls_handlerAfterInsertCase {
    
    /* @creation date: 14/06/2017
     * @author: Ramon Diaz, Accenture
     * @description: Método handler que asigna al caso de RRSS el Entitlement 'Asignación RRSS Genérica'
     * @param: Trigger.new y Trigger.old en las variables newCases y oldCases
     * @return: void
     * @exception: No aplica
     * @throws: No aplica */
    public static void handlerAfterInsertCase (List<Case> newCases,List<Case> oldCases) {
        System.debug('::::::::: Parámetro newCases:' + newCases + ':::::::::');
        System.debug('::::::::: Parámetro oldCases:' + oldCases + ':::::::::');
        List<Entitlement> totalEntitlements = [Select Id from Entitlement WHERE Name = :Label.RRSS_Entitlement];
        if (totalEntitlements.size()> 0) {
            //Recuperamos el Id del Entitlement por defecto para asignarlo al caso
            Id defaultEnt = totalEntitlements.get(0).Id;
            List<Case> casesToUpdate = new List<Case>();
            for (Case c : [Select Id, AccountId, RecordTypeId, EntitlementId FROM Case WHERE Id IN : newCases]) {
                // Verificamos que el caso sea de RRSS mediante el RecordType
                Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.RRSS_RecordType).getRecordTypeId();
                // Añadimos el caso a la lista de casos a actualizar
                if (c.RecordTypeId == recordTypeId && c.EntitlementId == null) {
                    c.EntitlementId = defaultEnt;
                    casesToUpdate.add(c);
                }
            }
            // Una vez identificados los casos de RRSS que se intentan insertar y después de agregarles el Entitlement
            // Los actualizamos
            if (casesToUpdate.size() > 0) {
                update casesToUpdate;
            }
        }
    }
}