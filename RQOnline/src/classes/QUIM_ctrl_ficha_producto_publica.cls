global with sharing class QUIM_ctrl_ficha_producto_publica {
    // ID del Grado pasado por parámetro GET
    public static String idGrado {get; set;}
    // the query para obtener los objetos
    private static String soql {get; set;}
    // El grado[CC Product] a mostrar
    public static ccrz__E_Product__c grado {get; set;}
    // String para mostrar las aplicaciones (Recomendado para) | No muestra el texto directamente en la VF :S
    public static String aplicacionesRec {get; set;}
    // Productos[CC Category] del grado
    public static List<ccrz__E_Category__c> productos {get; set;}
    public static List<ccrz__E_CategoryI18N__c> productosI18N {get; set;}
    // Aplicaciones[CC Category] del grado
    public static List<ccrz__E_Category__c> aplicacionesGrado {get; set;}
    public static List<ccrz__E_CategoryI18N__c> aplicacionesI18N {get; set;}
    // Segmentos Industriales[CC Category] del grado
    public static List<ccrz__E_Category__c> segIndustialsGrado {get; set;}
    
    // Map de las especificaciones del grado (IDentificador__c[key] y ccrz__SpecValue__c[value])
    public static List<Map<String, String>> specsGrado {get; set;}
    // paso lista a JSON [para paso por JS]
    public static String specsGradoJSON {get; set;}
    
    // the collection of grados relacionados
    public static List<ccrz__E_Product__c> gradosRelated {get; set;}
    // paso lista a JSON [para paso por JS]
    public static String gradosRelJSON {get;set;}
    
    // listado de las especificiones de los grados. En el mismo orden.
    public static List<Map<String, String>> specsRelatedMap {get; set;}
    // paso lista a JSON [para paso por JS]
    public static String specsRelatedMapJSON {get;set;}
    
    global static String blank{get{return '';}private set;}
    
    public static String base64Value {get;Set;}  
    public static String tipoDocumento {get;Set;}
    
    //MDG
    public static List<ccrz__E_ProductMedia__c> prodMedia {get; set;}
    public static List<String> idiFichtec {get; set;}
    public static List<String> idiCertCump {get; set;}
    public static List<String> idiFichSeg {get; set;}
    public static String idFichSeg {get; set;}
    public static List<ccrz__E_ProductMedia__c> ProdMediaRelated {get; set;}
    public static List<String> idProdMediaRelated {get; set;}
    public static ccrz__E_CategoryI18N__c nombreI18n {get; set;}
    // Parámetro GET idioma
    public static String parametroIdioma {get;set;}
    // Lengua a utilizar
    public static String language {get; set;}
    // init the controller
    public QUIM_ctrl_ficha_producto_publica() {
        System.debug('Entrando Constructor QUIM_ctrl_ficha_producto_publica()');
        idGrado = ApexPages.currentPage().getParameters().get('id');
        System.debug('IdGrado: '+idGrado);
        parametroIdioma = ApexPages.currentPage().getParameters().get('cclcl');
        System.debug(System.LoggingLevel.INFO, 'parametroIdioma: ' + parametroIdioma);
        if (parametroIdioma != NULL && parametroIdioma.length()>1) {
            language = parametroIdioma;
        }
        else {
          language = ApexPages.currentPage().getHeaders().get('Accept-Language').left(2);
        }
        System.debug(System.LoggingLevel.INFO, 'language: ' + language);
        
        // CONTROLA que al pasar un ID incorrecto o vacío no pete la página
        // ****************************************************************
        if (idGrado != null) {
            try {
                //MDG Obtengo todos los product category del grado que son documentos y los divido segun el documento al que pertenecen    
                prodMedia = [SELECT id,Tipo_documento__c,Id_Documento__c,ccrz__Locale__c FROM ccrz__E_ProductMedia__c WHERE ccrz__Product__c = :idGrado];
                //System.debug('prodMedia' +prodMedia);
            }
            catch (Exception e) {
                System.debug(System.LoggingLevel.ERROR, 'FAIL@prodMedia: ' + e);
            }
            if(prodMedia!=null){
                idiFichtec = new List<String>();
                idiCertCump = new List<String>();
                idiFichSeg = new List<String>();
                for(integer i=0; i<prodMedia.size();i++){
                    if(prodMedia[i].Tipo_documento__c == 'Ficha Técnica'){
                        idiFichtec.add(prodMedia[i].ccrz__Locale__c);
                    }/*else if(prodMedia[i].Tipo_documento__c == 'Certificado de Cumplimiento'){
                        idiCertCump.add(prodMedia[i].ccrz__Locale__c);
                    }*/else if (prodMedia[i].Tipo_documento__c == 'Ficha Seguridad'){
                        idiFichSeg.add(prodMedia[i].ccrz__Locale__c);

                        if(prodMedia[i].Id_Documento__c != null){
                            idFichSeg = prodMedia[i].Id_Documento__c;
                        }
                    }
                }
            }
            System.debug('idFichSeg++++' +idFichSeg);
            System.debug('idiFichtec' +idiFichtec);
            //System.debug('idiCertCump' +idiCertCump);
            //System.debug('idiFichSeg' +idiFichSeg);
            //System.debug('idFichSeg: ' + idFichSeg);
            // Query para obtener el grado [CC Product]
            //soql = 'SELECT Name, ID, ccrz__sku__c, ccrz__LongDesc__c, Id_Ficha_Seguridad__c  FROM ccrz__E_Product__c WHERE ID = \''+ idGrado +'\'';
            soql = 'SELECT Name, ID, ccrz__sku__c, ccrz__LongDesc__c  FROM ccrz__E_Product__c WHERE ID = \''+ idGrado +'\'';
            runQueryGrado();
                     
            // Obtener los productos [CC Category]
            obtenerProductos();
            // Obtener las aplicaciones [CC Category]
            obtenerAplicaciones();
            // Obtener los segmentos industriales [CC Category]
            obtenerSegIndustriales();

            // mapea las Spec's del grado (IDentificador__c[key] y ccrz__SpecValue__c[value])
            mapearSpecs();
            
            //system.debug('aplicacionesI18N  ' + aplicacionesI18N);
            //system.debug('aplicacionesI18N SIZE: ' + aplicacionesI18N.size());
            aplicacionesRec='';
            for (integer i=0 ; i<aplicacionesI18N.size() ; i++){
                aplicacionesRec += aplicacionesI18N[i].Name + ', ';
            }
            if (aplicacionesRec.length() > 2) {
                aplicacionesRec = aplicacionesRec.substring(0, aplicacionesRec.length()-2);
                aplicacionesRec += '.';
            }
            
            // Query para obtener los grados relacionados [al menos un mismo producto [CC Category] y tiene al menos una misma aplicación (segmento)]
            // filtra por producto [CC Category] y elimina el grado [CC Product] que estamos mostrando
            soql = 'SELECT Id, Name, ccrz__LongDesc__c, Recomendado__c, Nuevo__c FROM ccrz__E_Product__c WHERE Id IN (SELECT ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Product__c != \''+ idGrado +'\'';
            if (productos != NULL && productos.size() > 0) {
                if (productos.size() == 1) {
                    soql += ' AND ccrz__Category__c = \''+ productos.get(0).ID +'\')';
                }
                else {
                    soql += ' AND (ccrz__Category__c = \''+ productos.get(0).ID +'\'';
                    for (Integer i = 1; i<productos.size(); i++) {
                        soql += 'OR ccrz__Category__c = \''+ productos.get(i).ID +'\'';
                    }
                }
            }
            //System.debug('APLICACIONES2 aplicacionesGrado: ' + aplicacionesGrado);
            if (aplicacionesGrado != NULL && aplicacionesGrado.size() > 0) {
                soql += 'AND Id IN (SELECT ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Product__c != \''+ idGrado +'\'';
                if (aplicacionesGrado.size() == 1) {
                    soql += ' AND ccrz__Category__c = \''+ aplicacionesGrado.get(0).ID +'\')';
                }
                else {
                    soql += ' AND (ccrz__Category__c = \''+ aplicacionesGrado.get(0).ID +'\'';
                    for (Integer i = 1; i<aplicacionesGrado.size(); i++) {
                        soql += 'OR ccrz__Category__c = \''+ aplicacionesGrado.get(i).ID +'\'';
                    }
                    soql += ')) ORDER BY Recomendado__c DESC';
                }
            }
            runQueryGradosRelated();
            //System.debug(System.LoggingLevel.INFO, 'gradosRelated: ' + gradosRelated);
            mapearSpecsRelated();
        }// if
    }
    
    // Obtiene el grado [CC Product]
    public static void runQueryGrado() { 
        try {
            grado = new ccrz__E_Product__c();
            grado = Database.query(soql);
            //System.debug(System.LoggingLevel.INFO, 'grado: ' + grado);
            String langLike = language + '%';
            //System.debug(System.LoggingLevel.INFO, 'langLike: ' + langLike);
            ccrz__E_ProductItemI18N__c gradoI18N = new ccrz__E_ProductItemI18N__c();
            gradoI18N = [SELECT Name, ccrz__LongDesc__c, ccrz__Product__c FROM ccrz__E_ProductItemI18N__c WHERE ccrz__Product__c = :grado.Id AND (ccrz__Locale__c LIKE :langLike OR ccrz__Locale__c = '') LIMIT 1];
            //System.debug(System.LoggingLevel.INFO, 'gradoI18N: ' + gradoI18N);
            if (gradoI18N.Name != NULL) {
                grado.Name = gradoI18N.Name;
            }
            if (gradoI18N.ccrz__LongDesc__c != NULL) {
                grado.ccrz__LongDesc__c = gradoI18N.ccrz__LongDesc__c;
            }
            
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ runQueryGrado(): ' + e);
        }
    }
    // Obtiene los productos [CC Category]
    public static void obtenerProductos() {
        try {
            productos = new List<ccrz__E_Category__c> ();
            //*************** Obteniendo Productos PARENT_CATEGORY **********************
            ccrz__E_Category__c idProductosParent = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'AgrupacionProductos' LIMIT 1];
            List<ccrz__E_Category__c> idsPadresMiddle = [SELECT Id FROM ccrz__E_Category__c WHERE ccrz__ParentCategory__c = :idProductosParent.Id];
            //System.debug('PRODUCTOS idsPadresMiddle: ' + idsPadresMiddle);
            productos = [SELECT Id, Name FROM ccrz__E_Category__c WHERE Id IN (SELECT ccrz__Category__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Product__c = :idGrado) AND ccrz__ParentCategory__c IN :idsPadresMiddle];
            //System.debug('PRODUCTOS productos: ' + productos);
            String langLike = language + '%';
            //System.debug('langLike: ' + langLike);
            productosI18N = [SELECT Id, Name, ccrz__Locale__c, ccrz__ShortDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :productos AND ccrz__Locale__c LIKE :langLike];
            //System.debug('productosI18N: ' + productosI18N);
            if (productosI18N.isEmpty()) {
                productosI18N = [SELECT Id, ccrz__CategoryI18NId__c, Name, ccrz__Locale__c, ccrz__ShortDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :productos AND ccrz__Locale__c LIKE 'en%'];
            }
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ obtenerProductos(): ' + e);
        }
    }
    // Obtiene las Aplicaciones [CC Category]
    public static void obtenerAplicaciones() {
        try {
            aplicacionesGrado = new List<ccrz__E_Category__c> ();
            //*************** Obteniendo Aplicaciones PARENT_CATEGORY ********************************
            ccrz__E_Category__c idAplicacionesParent = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'Aplicaciones' LIMIT 1];
            //System.debug('APLICACIONES idAplicacionesParent: ' + idAplicacionesParent);
            aplicacionesGrado = [SELECT Id, Name FROM ccrz__E_Category__c WHERE Id IN (SELECT ccrz__Category__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Product__c = :idGrado) AND ccrz__ParentCategory__c = :idAplicacionesParent.Id];
            //System.debug('APLICACIONES1 aplicacionesGrado: ' + aplicacionesGrado);
            String langLike = language + '%';
            //System.debug('langLike: ' + langLike);
            aplicacionesI18N = [SELECT Id, Name FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :aplicacionesGrado AND ccrz__Locale__c LIKE :langLike];
            //System.debug('aplicacionesI18N: ' + aplicacionesI18N);
            if (aplicacionesI18N.isEmpty()) {
                aplicacionesI18N = [SELECT Id, Name FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :aplicacionesGrado AND ccrz__Locale__c LIKE 'en%'];
            }
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ obtenerAplicaciones(): ' + e);
        }
    }
    // Obtiene los Segmentos Industriales [CC Category]
    public static void obtenerSegIndustriales() {
        try {
            segIndustialsGrado = new List<ccrz__E_Category__c> ();
            //*************** Obteniendo Segmentos Industriales PARENT_CATEGORY **********************
            ccrz__E_Category__c idSegmentosParent = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'Segmentos' LIMIT 1];
            //System.debug('PRUEBA idSegmentosParent: ' + idSegmentosParent.Id);
            segIndustialsGrado = [SELECT Id, Name FROM ccrz__E_Category__c WHERE Id IN (SELECT ccrz__Category__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Product__c = :idGrado) AND ccrz__ParentCategory__c = :idSegmentosParent.Id ORDER BY Id];
            //System.debug('PRUEBA segIndustialsGrado: ' + segIndustialsGrado);
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ obtenerSegIndustriales(): ' + e);
        }
    }

    // Mapea las Spec's del grado (IDentificador__c[key] y ccrz__SpecValue__c[value])
    public static void mapearSpecs() {
        try {
            //System.debug('dentro mapearSpecs()');
            specsGrado = new List<Map<String, String>> ();

            Map <String, String> mapaSpecs = new Map<String, String> ();
            
            String langLike = language + '%';
            List<ccrz__E_ProductSpec__c> productSpecGrado = new List<ccrz__E_ProductSpec__c> ();
            productSpecGrado = [SELECT Id, ccrz__Product__c, ccrz__Spec__c, ccrz__SpecValue__c, ccrz__Spec__r.IDentificador__c FROM ccrz__E_ProductSpec__c WHERE ccrz__Product__c = :idGrado AND (Locale__c LIKE :langLike OR Locale__c = '')];

            for (ccrz__E_ProductSpec__c productSpec : productSpecGrado) {
                mapaSpecs.put(productSpec.ccrz__Spec__r.IDentificador__c, productSpec.ccrz__SpecValue__c);
            }
            //System.debug('NEW mapaSpecs: ' + mapaSpecs);
            
            specsGrado.add(mapaSpecs);
            //System.debug('ADD specsGrado: ' + specsGrado);
            // paso lista a JSON [para paso por JS]
            specsGradoJSON = JSON.serialize(specsGrado);
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ mapearSpecs(): ' + e);
        }
    }
    
    // Obtiene los grados relacionados
    public static void runQueryGradosRelated() { 
        try {
            gradosRelated = new List<ccrz__E_Product__c> ();
            gradosRelated = Database.query(soql);
            //system.debug('gradosRelated soql: '+soql);
            //System.debug('gradosRelated: '+gradosRelated);
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ runQueryGradosRelated(): ' + e);
        }
    }
    
    // Mapea las Spec's de los grados relacionados (IDentificador__c[key] y ccrz__SpecValue__c[value])
    public static void mapearSpecsRelated() { 
        try {
            specsRelatedMap = new List<Map<String, String>> ();
            
            // ID's de todos los grados relacionados para usarla en la Query
            List<Id> idsgradosRel = new List<Id> ();
            for(ccrz__E_Product__c gradoRel : gradosRelated) {
                idsgradosRel.add(gradoRel.Id);
            }
            String langLike = language + '%';
            //System.debug(System.LoggingLevel.INFO, 'langLike: ' + langLike);
            // ProductSpecs de Todos los grados Relacionados
            List<ccrz__E_ProductSpec__c> productSpecsGrados = new List<ccrz__E_ProductSpec__c> ();
            productSpecsGrados = [SELECT Id, ccrz__Product__c, ccrz__Spec__c, ccrz__SpecValue__c, ccrz__Spec__r.IDentificador__c FROM ccrz__E_ProductSpec__c WHERE ccrz__Product__c IN :idsgradosRel AND (Locale__c LIKE :langLike OR Locale__c = '')];
            // ProductItem'sI18N de Todos los grados Relacionados
            List<ccrz__E_ProductItemI18N__c> gradosResI18N = new List<ccrz__E_ProductItemI18N__c> ();
            gradosResI18N = [SELECT Name, ccrz__LongDesc__c, ccrz__Product__c FROM ccrz__E_ProductItemI18N__c WHERE ccrz__Product__c IN :idsgradosRel AND (ccrz__Locale__c LIKE :langLike OR ccrz__Locale__c = '')];
            //System.debug(System.LoggingLevel.INFO, 'gradosResI18N: ' + gradosResI18N);

            for(ccrz__E_Product__c gradoRel : gradosRelated) {

                Map<String, String> specsMap = new Map<String, String>();
                // Rellenando el mapa de especificaciones del grado.
                for (ccrz__E_ProductSpec__c productSpec : productSpecsGrados) {
                    if (gradoRel.Id == productSpec.ccrz__Product__c) {
                        specsMap.put(productSpec.ccrz__Spec__r.IDentificador__c, productSpec.ccrz__SpecValue__c);
                    }
                }
                // Cambiando el nombre y descripción al idioma correspondiente
                for (ccrz__E_ProductItemI18N__c gradoI18N : gradosResI18N) {
                    if (gradoRel.Id == gradoI18N.ccrz__Product__c) {
                        //System.debug(System.LoggingLevel.INFO, 'ANTES gradoRel.Name: ' + gradoRel.Name);
                        gradoRel.Name = gradoI18N.Name;
                        //System.debug(System.LoggingLevel.INFO, 'DESPUES gradoRel.Name: ' + gradoRel.Name);
                        gradoRel.ccrz__LongDesc__c = gradoI18N.ccrz__LongDesc__c; 
                    }
                }
                //System.debug('SETTED specsMap' + specsMap);
                // Añadiendo el mapa de especs del grado a la lista en el mismo orden de gradosRelated
                specsRelatedMap.add(specsMap);
            }
            //System.debug('OJO specsRelatedMap: ' + specsRelatedMap);
            // paso lista a JSON [para paso por JS]
            gradosRelJSON = JSON.serialize(gradosRelated);
            // paso lista a JSON [para paso por JS]
            specsRelatedMapJSON = JSON.serialize(specsRelatedMap);
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ mapearSpecsRelated(): ' + e);
        }
    }
    
    //MDG
    @RemoteAction
    global static String[] obtenerIdiomas(String idGrad, String modal) {
         
        QUIM_documentum llamadaDocumentum = new QUIM_documentum();
        
        return llamadaDocumentum.obtenerIdiomas(idGrad, modal);
    }  
    
    @RemoteAction
    global static String[] documentum(String idGrad, String documento, String idioma) {
         QUIM_documentum llamadaDocumentum = new QUIM_documentum();
        
        return llamadaDocumentum.fetchDocument(idGrad, documento, idioma);
    }
} // class