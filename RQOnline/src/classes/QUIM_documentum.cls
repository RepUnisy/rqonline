global class QUIM_documentum {

    // Obtiene el documento de Documentum
    global String[] fetchDocument(String idGrad, String documento, String idioma) {
        
        String tipoDocumento;
        String base64Value;
        String nombreDocumento;
        
        //modificación para obtención del id de Product Media
        ////Primero saber el tipo de documento a extraer:
        String tipo;
        if(documento == 'certCump' || documento == 'certCumpModalDown'){
            tipo = 'Certificado de Cumplimiento';
        }else if(documento == 'fichaTec'|| documento == 'fichaTecModalDown'){
             tipo = 'Ficha Técnica';
        }else if(documento == 'data-file'|| documento == 'fichaSeg'){
            tipo = 'Ficha Seguridad';
        }
        List<ccrz__E_ProductMedia__c> documentoSeleccionado = [SELECT Id_Documento__c FROM ccrz__E_ProductMedia__c WHERE ccrz__Locale__c = :idioma and ccrz__Product__c = :idGrad and Tipo_documento__c= :tipo];
        System.debug('documentoSeleccionado: '+documentoSeleccionado);
        System.debug('documento: '+documento);
        String idDocumentum;
        if (!documentoSeleccionado.isEmpty()) {
            idDocumentum = documentoSeleccionado[0].Id_Documento__c;
        }
        else {
            idDocumentum = 'ffffffffffffffff';  // Para los Test's
        }
        System.debug('idDocumentum: '+idDocumentum);
        //List<String> documentos = new List<String>();
       // ccrz__E_Product__c targetGrado = [SELECT Id, Id_Cert_Cumpl_Esp__c, Id_Cert_Cumpl_Ing__c, Id_Ficha_Tecnica_Esp__c, Id_Ficha_Tecnica_Ing__c FROM ccrz__E_Product__c WHERE Id = :idGrad];
        String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:core="http://core.services.fs.documentum.emc.com/" xmlns:core1="http://core.datamodel.fs.documentum.emc.com/" xmlns:prop="http://properties.core.datamodel.fs.documentum.emc.com/" xmlns:prof="http://profiles.core.datamodel.fs.documentum.emc.com/">'+
            '<soapenv:Header>'+
                '<ServiceContext token="temporary/127.0.0.1-1416830746831-1726605532208714733" xmlns="http://context.core.datamodel.fs.documentum.emc.com/" xmlns:ns2="http://properties.core.datamodel.fs.documentum.emc.com/" xmlns:ns3="http://profiles.core.datamodel.fs.documentum.emc.com/" xmlns:ns4="http://query.core.datamodel.fs.documentum.emc.com/" xmlns:ns5="http://content.core.datamodel.fs.documentum.emc.com/" xmlns:ns6="http://core.datamodel.fs.documentum.emc.com/">'+
                    '<Identities password="'+Label.QUIM_LBL_Pass+'" repositoryName="'+Label.QUIM_RepDocumentum+'" userName="'+Label.QUIM_LBL_Usuario+'" xsi:type="RepositoryIdentity" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"></Identities>'+
                '</ServiceContext>'+
            '</soapenv:Header>'+
            '<soapenv:Body>'+
                '<core:get>'+
                    '<forObjects isInternal="false">'+
                        '<core1:Identities valueType="OBJECT_ID" repositoryName="'+Label.QUIM_RepDocumentum+'">'+     
                    '<core1:ObjectId id="'+idDocumentum+'"/>'+
                     '</core1:Identities>'+
                    ' </forObjects>'+  
                        ' <options>'+            
                            '<core1:Profiles xsi:type="prof:ContentProfile" pageModifierFilter="ANY" pageNumber="0" pageFilter="ANY" formatFilter="ANY" urlReturnPolicy="NEVER" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>'+           
                            '<core1:Profiles xsi:type="prof:ContentTransferProfile" allowAsyncContentTransfer="false" allowCachedContentTransfer="false" transferMode="BASE64" isProcessOLELinks="false" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>'+           
                         '</options>'+    
            '</core:get>'+
            '</soapenv:Body>'+
            '</soapenv:Envelope>';       
        
        //Se obtiene el endPoint utilizado para el servicio WEB 
        //String endPoint = 'https://dsoa.repsol.com:8243/services/PORT_QUIMICA_DOCUM_object_service_FE_ps/get';
        String endPoint = '';
        if(!Test.isRunningTest()) {
            endPoint = QUIM_EndPoint_Documentum__c.getValues('documentum').value__c;
        }
        system.debug('**ENDPOINT ' + endpoint);
        String VFUsernameToken = Label.QUIM_VFUsernameToken;
        String VFPasswordText = Label.QUIM_VFPasswordText; 
        Blob headerValue = Blob.valueOf(VFUsernameToken + ':' + VFPasswordText);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        
        //Llamada al servicio WEB 
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint);
        req.setHeader('SoapAction', 'urn:core.services.fs.documentum.emc.com:get');
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');        
        req.setHeader('Authorization', authorizationHeader);                      
        req.setBody(body);
        req.setMethod('GET');
        Http http = new Http();
        HttpResponse result;
        if(Test.isRunningTest()){
            QUIM_MOCK_form_search nuevomock = new QUIM_MOCK_form_search();
            result=nuevomock.getMock();
        }else{
            result = http.send(req);
        }
        system.debug('gelireq'+ body);
        system.debug('geli'+ result);
        //system.debug(LoggingLevel.INFO, '** INFO PORT_QUIMICA_DOCUM_object_service_FE_ps: resultado --> ' + result);
        system.debug(LoggingLevel.INFO, '** INFO PORT_QUIMICA_DOCUM_object_service_FE_ps: resultado --> ' + result.getBody());
        system.debug(LoggingLevel.INFO, '** INFO PORT_QUIMICA_DOCUM_object_service_FE_ps: endPoint --> ' + endPoint);
        //system.debug(LoggingLevel.INFO, '** INFO PORT_QUIMICA_DOCUM_object_service_FE_ps: estado de la respuesta devuelta --> ' + result.getXmlStreamReader());
        
        List<String> orgInfo=new List<String>();
        Dom.Document doc = new Dom.Document();        
        doc.load(result.getBody());        
        //Retrieve the root element for this document.        
        Dom.XMLNode Envelope = doc.getRootElement();        
        Dom.XMLNode child= Envelope.getChildElements()[0];       //getResponse 
        Dom.XMLNode child1= child.getChildElements()[0]; 
        Dom.XMLNode child2= child1.getChildElements()[0]; 
        Dom.XMLNode child3= child2.getChildElements()[0];   //Contents
        Dom.XMLNode child4= child3.getChildElements()[2];  //Value
        Dom.XmlNode properties = child3.getChildElements()[1];
        for(Dom.XmlNode node: properties.getChildElements()) {
            system.debug('names == '+ node.getAttributeValue('name',null));
            if(node.getAttributeValue('name',null)=='atr_tipoob') {
                Dom.XmlNode contentType = node;
                tipoDocumento = contentType.getChildElements()[0].getText().toLowerCase();
                system.debug('tipoDocumento ========= '+tipoDocumento);
            }
            if(node.getAttributeValue('name',null)=='atr_grado') {//devolvemos el nombre
                Dom.XmlNode contentType = node;
                nombreDocumento = contentType.getChildElements()[0].getText();
                //system.debug('tipoDocumento ========= '+tipoDocumento);
            }
        }
        base64Value = child4.getChildElements()[2].getText(); 
        String[] retorno = new List<String>();
        tipoDocumento = 'pdf';
        retorno.add(tipoDocumento);
        retorno.add(base64Value);
        retorno.add(nombreDocumento);
        return retorno;
 
    }
   
    
    // Devuelve los idiomas para alimentar el combo de los modales de descarga de documentos
    global String[] obtenerIdiomas(String idGrad, String modal) {
        ////Primero saber el tipo de documento a extraer:
        String tipo;
        ccrz__E_Product__c idSeguridad;
        if(modal == '#fichaTecModal'){
            tipo = 'Ficha Técnica';
        }else if(modal == '#fichaSegModal'){
            tipo = 'Ficha Seguridad';
        }else{
            tipo = 'Certificado de Cumplimiento';
        }
        //System.debug('tipo: '+tipo);
        List <ccrz__E_ProductMedia__c> prodMediaRelated = new List <ccrz__E_ProductMedia__c>();
        List <String> idProdMediaRelated = new List <String>();
        prodMediaRelated = [SELECT id,Tipo_documento__c,ccrz__Locale__c,Id_Documento__c FROM ccrz__E_ProductMedia__c WHERE ccrz__Product__c = :idGrad and Tipo_documento__c= :tipo];
        //System.debug('prodMediaRelated: '+prodMediaRelated);  
        if(modal=='#fichaSegModal' && !prodMediaRelated.isEmpty()){
            idSeguridad = [SELECT Id_Ficha_Seguridad__c from ccrz__E_Product__c where id =:idGrad];
            idProdMediaRelated.add(idSeguridad.Id_Ficha_Seguridad__c );
            for(integer i=0;i<prodMediaRelated.size();i++){   
                idProdMediaRelated.add(prodMediaRelated[i].ccrz__Locale__c);
            } 
        }else{
            for(integer i=0;i<prodMediaRelated.size();i++){             
                idProdMediaRelated.add(prodMediaRelated[i].ccrz__Locale__c);
            }
        }
        
        //System.debug('idProdMediaRelated: '+idProdMediaRelated);
        return idProdMediaRelated;
    }
    
    // Botón "Subir a Documentum" del objeto CC Product Media
    webservice static String UploadBotton(Id Adjunto){
        String idDocumento;
        String idioma;
        String tipoDoc;
        String extensionDocumento;
        Map<String,String> datosAdjuntos = new Map<String,String>(); 
        
        List<Attachment> adjuntos = [SELECT Id, Body,Name,ContentType from Attachment WHERE ParentId = :Adjunto];
        ccrz__E_ProductMedia__c productParent = [SELECT Id,ccrz__Locale__c,Tipo_documento__c,ccrz__Product__c FROM ccrz__E_ProductMedia__c Where Id = :Adjunto];
        ccrz__E_Product__c producto = [SELECT Id,Name FROM ccrz__E_Product__c WHERE Id = :productParent.ccrz__Product__c];
        // resultado = ''+adjuntos.size();      
        if(adjuntos.size()>1){
            return idDocumento = 'Invalido';
        }else{ 
        for(Attachment att:adjuntos){
            String extension= att.Name;
            Integer extensionIndex = extension.lastIndexOf('.');
            extensionDocumento = extension.substring(extensionIndex+1, extension.length());
            System.debug('extensionDocumento'+extensionDocumento);
            datosAdjuntos.put('Name',att.Name);
            datosAdjuntos.put('Value',EncodingUtil.base64Encode(att.Body));
            datosAdjuntos.put('ContentType',extensionDocumento);
            if(productParent.Tipo_documento__c == 'Certificado de Cumplimiento'){
                tipoDoc='CC';
            }else{
                tipoDoc='NT';
            }
            datosAdjuntos.put('TipoDocumento',tipoDoc);
            // idioma = obtenerLocaleIdiomas(productParent.Idioma_del_documento__c);
            datosAdjuntos.put('idioma',productParent.ccrz__Locale__c);
            datosAdjuntos.put('NameGrado',producto.Name);
            idDocumento = UploadDocumentum(datosAdjuntos);
            
        }
            if(adjuntos.size()>0){
                productParent.Id_Documento__c = idDocumento;
                //Actualizamos el product Media para que tenga su idDocumento
                update productParent;
                //Borramos el adjunto de Product Media
                delete adjuntos;  
                return idDocumento;
            }else{
                return '0';
            }
       }
        
    }
    
    global static String UploadDocumentum(Map<String,String> datosAdjuntos){
        String idDocumento = '';
        Datetime fecha = datetime.now();
        String fechaformateada = fecha.format('MM/dd/yyyy HH:mm:ss');
        String body = '<?xml version="1.0" ?>'+
                        '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">'+
                            '<S:Header>'+
                                '<ServiceContext xmlns="http://context.core.datamodel.fs.documentum.emc.com/" xmlns:ns2="http://properties.core.datamodel.fs.documentum.emc.com/" xmlns:ns3="http://profiles.core.datamodel.fs.documentum.emc.com/" xmlns:ns4="http://query.core.datamodel.fs.documentum.emc.com/" xmlns:ns5="http://content.core.datamodel.fs.documentum.emc.com/" xmlns:ns6="http://core.datamodel.fs.documentum.emc.com/" token="temporary/127.0.0.1-1369031710007-440497215183974583">'+
                                    '<Identities xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" password="'+Label.QUIM_LBL_Pass+'" repositoryName="'+Label.QUIM_RepDocumentum+'" userName="'+Label.QUIM_LBL_Usuario+'" xsi:type="RepositoryIdentity"> </Identities>'+
                                 '</ServiceContext>'+
                            '</S:Header>'+
                            '<S:Body>'+
                                '<ns8:create xmlns:ns2="http://properties.core.datamodel.fs.documentum.emc.com/" xmlns:ns3="http://core.datamodel.fs.documentum.emc.com/" xmlns:ns4="http://content.core.datamodel.fs.documentum.emc.com/" xmlns:ns5="http://profiles.core.datamodel.fs.documentum.emc.com/" xmlns:ns6="http://query.core.datamodel.fs.documentum.emc.com/" xmlns:ns7="http://rt.fs.documentum.emc.com/" xmlns:ns8="http://core.services.fs.documentum.emc.com/" xmlns:ser="http://adobe.com/idp/services">'+
                                    '<dataPackage>'+
                                        '<ns3:DataObjects transientId="24374438" type="do_gupor_docagrup"><ns3:Identity repositoryName="'+Label.QUIM_RepDocumentum+'" valueType="UNDEFINED"/>'+
                                            '<ns3:Properties isInternal="false">'+
                                                '<ns2:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:StringProperty" isTransient="false" name="object_name">'+
                                                    '<ns2:Value>'+datosAdjuntos.get('Name')+'</ns2:Value>'+
                                                '</ns2:Properties>'+
                                                '<ns2:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:StringProperty" isTransient="false" name="atr_fecha">'+
                                                    '<ns2:Value>'+fechaformateada+'</ns2:Value>'+
                                                '</ns2:Properties>'+
                                                '<ns2:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:StringProperty" isTransient="false" name="atr_tipoob">'+
                                                    '<ns2:Value>'+datosAdjuntos.get('ContentType')+'</ns2:Value>'+
                                //'<ns2:Value>docx</ns2:Value>'+
                                                '</ns2:Properties>'+
                                                '<ns2:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:StringProperty" isTransient="false" name="atr_tipodoc">'+
                                                    '<ns2:Value>'+datosAdjuntos.get('TipoDocumento')+'</ns2:Value>'+
                                 //'<ns2:Value>NT</ns2:Value>'+
                                                '</ns2:Properties>'+
                                                '<ns2:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:StringProperty" isTransient="false" name="atr_grado">'+
                                                    '<ns2:Value>'+datosAdjuntos.get('NameGrado')+'</ns2:Value>'+
                                                '</ns2:Properties>'+        
                                                '<ns2:Properties xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:StringProperty" isTransient="false" name="atr_idioma">'+
                                                    '<ns2:Value>'+datosAdjuntos.get('idioma')+'</ns2:Value>'+
                                                '</ns2:Properties>'+
                                            '</ns3:Properties>'+
                                            '<ns3:Contents xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns4:BinaryContent" pageNumber="0" format="doc">'+
                                                '<ns4:renditionType xsi:nil="true"/>'+
                                                '<ns4:Value>'+datosAdjuntos.get('Value')+'</ns4:Value>'+
                                            '</ns3:Contents>'+
                                        '</ns3:DataObjects>'+
                                    '</dataPackage>'+
                                    '<options>'+          
                                    '</options>'+
                                '</ns8:create>'+
                            '</S:Body>'+
                        '</S:Envelope>';
        String endPoint = '';
        if(!Test.isRunningTest()){
            endPoint = QUIM_EndPoint_Documentum__c.getValues('documentum').value__c;
        }
        system.debug('**ENDPOINT ' + endpoint);
        String VFUsernameToken = Label.QUIM_VFUsernameToken;
        String VFPasswordText = Label.QUIM_VFPasswordText; 
        Blob headerValue = Blob.valueOf(VFUsernameToken + ':' + VFPasswordText);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        
        //Llamada al servicio WEB 
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint);
        req.setHeader('SoapAction', 'urn:core.services.fs.documentum.emc.com:create');
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');        
        req.setHeader('Authorization', authorizationHeader);                      
        req.setBody(body);
        req.setMethod('GET');
        Http http = new Http();
        HttpResponse result;                           
        if(Test.isRunningTest()){
            QUIM_MOCK_form_search nuevomock = new QUIM_MOCK_form_search();
            result=nuevomock.getMock();
        }else{
            req.setTimeout(120000);
            result = http.send(req);
        }
        system.debug(LoggingLevel.INFO, '** INFO PORT_QUIMICA_DOCUM_object_service_FE_ps: resultado --> ' + result.getBody());
        system.debug(LoggingLevel.INFO, '** INFO PORT_QUIMICA_DOCUM_object_service_FE_ps: endPoint --> ' + endPoint);
        
        //Obtenemos el idDocumento
        Dom.Document doc = new Dom.Document();        
        doc.load(result.getBody());       
        System.debug('holaaaaaaaaaaaaaaaaaaaaaaaaaaa++'+result.getBody()) ;
        //Retrieve the root element for this document.        
        Dom.XMLNode Envelope = doc.getRootElement();  
        Dom.XMLNode child= Envelope.getChildElements()[0]; //getResponse 
        Dom.XMLNode child1= child.getChildElements()[0]; //Response
        Dom.XMLNode child2= child1.getChildElements()[0];  //Return
        Dom.XMLNode child3= child2.getChildElements()[0];   //DataObject
        Dom.XMLNode child4= child3.getChildElements()[0];  //Identitylue
        Dom.XMLNode objectId= child4.getChildElements()[0];  //objectId
        
        idDocumento = objectId.getAttribute(objectId.getAttributeKeyAt(0),'');     
        
        return idDocumento;
    }
} //class