/**
*   @name: RQO_cls_SurveyQuestionSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la obtención de SurveyQuestion
*/
public with sharing class RQO_cls_SurveyQuestionSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_SurveyQuestionSelector';
	
    /**
    *   @name: RQO_cls_SurveyQuestionSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la lista de campos del tipo SurveyQuestion
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_obj_surveyQuestion__c.Name,
			RQO_obj_surveyQuestion__c.RQO_fld_nameEn__c,
			RQO_obj_surveyQuestion__c.RQO_fld_nameFr__c,
			RQO_obj_surveyQuestion__c.RQO_fld_nameDe__c,
			RQO_obj_surveyQuestion__c.RQO_fld_namePt__c,
			RQO_obj_surveyQuestion__c.RQO_fld_nameIt__c,
			RQO_obj_surveyQuestion__c.RQO_fld_survey__c,
			RQO_obj_surveyQuestion__c.RQO_fld_position__c,
			RQO_obj_surveyQuestion__c.RQO_fld_description__c };
    }
	
    /**
    *   @name: RQO_cls_SurveyQuestionSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el sObjectType de SurveyQuestion
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_surveyQuestion__c.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_SurveyQuestionSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el campo por el cual ordenar los RQO_cls_SurveyQuestionSelector
    */
    public override String getOrderBy()
	{
		return 'RQO_fld_Survey__r.RQO_fld_startDate__c, RQO_fld_position__c';
	}
	
    /**
    *   @name: RQO_cls_SurveyQuestionSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la lista de RQO_obj_surveyQuestion__c a través de un conjunto
    *   de Ids de surveryQuestion
    */
    public List<RQO_obj_surveyQuestion__c> selectById(Set<ID> idSet)
    {
         return (List<RQO_obj_surveyQuestion__c>) selectSObjectsById(idSet);
    }
    
    /**
    *   @name: RQO_cls_SurveyQuestionSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la lista de RQO_obj_surveyQuestion__c a través de un conjunto de
    *   Survey Ids
    */
    public List<RQO_obj_surveyQuestion__c> selectBySurveyId(Set<ID> idSet)
    {
    	/**
        * Constantes
        */
        final String METHOD = 'selectBySurveyId';
        
        // Factoría para la Survey Question
    	fflib_QueryFactory surveyQuestionQueryFactory = newQueryFactory();

    	// Añadimos una subselect mediante la factoría de Question Responses
	    fflib_QueryFactory questionResponsesQueryFactory =
	        new RQO_cls_QuestionResponseSelector().
	            addQueryFactorySubselect(surveyQuestionQueryFactory);
	            
	    string query = surveyQuestionQueryFactory.setCondition('RQO_fld_Survey__r.Id IN :idSet').toSOQL();
    	
    	System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
    	
  		return (List<RQO_obj_surveyQuestion__c>) Database.query(query);
    }
    
    /**
    *   @name: RQO_cls_SurveyQuestionSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de RQO_obj_surveyQuestion__c activos
    */
    public List<RQO_obj_surveyQuestion__c> selectActive(Set<ID> idUserSet, string customerProfile, string userProfile, boolean home)
    {
    	/**
        * Constantes
        */
        final String METHOD = 'selectActive';
        
        // Recuperar clientes de los usuarios
        List<User> users = new RQO_cls_UserSelector().selectById(idUserSet);
       	Set<Id> accountIds = new Set<Id>();
    	for (User user : users) {
		   accountIds.add(user.AccountId);
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': accountIds ' + accountIds);
		
		// Recuperar encuestas para los clientes
		List<RQO_obj_surveyAccount__c> surveys = new RQO_cls_SurveyAccountSelector().selectByAccountId(accountIds);
		Set<Id> surveyIds = new Set<Id>();
    	for (RQO_obj_surveyAccount__c survey : surveys) {
		   surveyIds.add(survey.RQO_fld_survey__c);
		}
		
        // Factoría para la Survey Question
    	fflib_QueryFactory surveyQuestionQueryFactory = newQueryFactory();

    	// Añadimos una subselect mediante la factoría de Question Responses
	    fflib_QueryFactory questionResponsesQueryFactory =
	        new RQO_cls_QuestionResponseSelector().
	            addQueryFactorySubselect(surveyQuestionQueryFactory);
	    
	    // Filtro por fechas y home
	    string condition = 'RQO_fld_Survey__r.RQO_fld_home__c = ' + home + ' AND ' +
	    				   'RQO_fld_Survey__r.RQO_fld_startDate__c  <= TODAY AND RQO_fld_Survey__r.RQO_fld_endDate__c  >= TODAY';
        
        // Filtro por perfil de usuario
        System.debug(CLASS_NAME + ' - ' + METHOD + ': userProfile ' + userProfile);
        
        if (String.isNotEmpty(userProfile))
        	condition = condition + ' AND (RQO_fld_Survey__r.RQO_fld_userProfile__c = \'' + userProfile + '\' OR RQO_fld_Survey__r.RQO_fld_userProfile__c = \'Todos\')';

		// Filtro por perfil de cliente
		System.debug(CLASS_NAME + ' - ' + METHOD + ': customerProfile ' + customerProfile);

        if (String.isNotEmpty(customerProfile))
        	condition = condition + ' AND (RQO_fld_Survey__r.RQO_fld_customerProfile__c = \'' + customerProfile + '\' OR RQO_fld_Survey__r.RQO_fld_customerProfile__c = \'Todos\')';
        
        // Filtro por clientes
        System.debug(CLASS_NAME + ' - ' + METHOD + ': surveyIds ' + surveyIds);
        
        condition = condition + ' AND (RQO_fld_Survey__r.RQO_fld_accountsNumber__c = 0 OR RQO_fld_Survey__r.Id IN : surveyIds)';
        
        // Filtro por preguntas ya respondidas
        if (idUserSet != null)
        {
        	List<RQO_obj_surveyResponse__c> responses = new RQO_cls_SurveyResponseSelector().selectByUserId(idUserSet);
        	Map<Id, RQO_obj_surveyResponse__c> responsesMap = new Map<Id, RQO_obj_surveyResponse__c>();
        	for (RQO_obj_surveyResponse__c response : responses) {
			   responsesMap.put(response.RQO_fld_question__c, response);
			}
			Set<Id> responseIds = responsesMap.keySet();
			
			System.debug(CLASS_NAME + ' - ' + METHOD + ': responseIds ' + responseIds);
			
        	condition = condition + ' AND Id NOT IN : responseIds';
        }
        	
    	string query = surveyQuestionQueryFactory.setCondition(condition).toSOQL();
    	
    	System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
    	
  		return (List<RQO_obj_surveyQuestion__c>) Database.query(query);
    }
      
}