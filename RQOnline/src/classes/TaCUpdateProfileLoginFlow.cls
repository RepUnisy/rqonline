global class TaCUpdateProfileLoginFlow{
        
    webService static void DoUpdateProfile(Id userId,string language, boolean updateProfile) {

    // Add proper error handling to prevent update if target profile not found and an underestanble error message is
    // returned and possibly emailed to System Administrator
        try{
            System.debug('==> POKI1 entering DoUpdateProfile with UserId' + userId );
            User userToUpdate = [select       ProfileId, TaCAccepted__c, TaCProfileIfAcceptTaC__c
                                from        user 
                                where id = :UserId];
                                
            Profile originProfile = [select id,name from profile where id =:userToUpdate.ProfileId ];                                
 
            System.debug('==> POKI2 retrieved UserId, ProfileId, TaCProfileIfAcceptTaC__c: ' + userToUpdate.Id 
                                + ' ' + userToUpdate.ProfileId + ' ' + userToUpdate.TaCProfileIfAcceptTaC__c );
 
            Profile targetProfile = [select id from profile where Name =:userToUpdate.TaCProfileIfAcceptTaC__c ];

              //  Add some logic to protect users with System Administrator profile to be update
            System.debug('==> POKI3 Retrieved Profile Id ' + targetProfile.Id + ' now updating A......');
            if ((updateProfile) && (targetProfile != null)){
                userToUpdate.ProfileId =  targetProfile.Id; 
            }                
            userToUpdate.TaCAccepted__c = true;
            userToUpdate.TaC_Aceptation_Date__c = Datetime.now();
            userToUpdate.LanguageLocaleKey = language;
 
            
            userToUpdate.UserPreferencesHideS1BrowserUI = false;
            
            System.debug('==> POKI3 Retrieved Profile Id ' + targetProfile.Id + ' now updating B......');

            update userToUpdate;
            System.debug('==> POKI4 after update');      
        }catch(Exception e){
            System.Debug ('The following exception has occurred: ' + e.getMessage()); 
        }
    }
}