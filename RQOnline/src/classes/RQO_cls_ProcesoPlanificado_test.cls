/**
*	@name: RQO_cls_ProcesoPlanificado_test
*	@version: 1.0
*	@creation date: 26/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar la programación de procesos
*/
@IsTest
public class RQO_cls_ProcesoPlanificado_test {

    /**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
    }
    

	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue(){
        User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            System.assertEquals(true, result);	
        }  
    }
    
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue2(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_horaInicio__c = null;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            System.assertEquals(true, result);
		}
    }
    
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue3(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_horaFin__c = null;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            System.assertEquals(true, result);
		}
    }
    
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue4(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_horaInicio__c = '01:30';
            proceso.RQO_fld_horaFin__c = '23:59';
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            System.assertEquals(true, result);
		}
    }
	
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue5(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_sabado__c = false;
            proceso.RQO_fld_lunes__c = false;
            proceso.RQO_fld_martes__c = false;
            proceso.RQO_fld_miercoles__c = false;
            proceso.RQO_fld_jueves__c = false;
            proceso.RQO_fld_viernes__c = false;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            Datetime dt = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
            String dayOfWeek=dt.format('EEEE');
            if (dayOfWeek.equalsIgnoreCase('Sunday')) {
                System.assertEquals(true, result);
            } else {
                 System.assertEquals(false, result);
            }
		}
    }
	
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue6(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_domingo__c =  false;
            proceso.RQO_fld_lunes__c = false;
            proceso.RQO_fld_martes__c = false;
            proceso.RQO_fld_miercoles__c = false;
            proceso.RQO_fld_jueves__c = false;
            proceso.RQO_fld_viernes__c = false;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            Datetime dt = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
            String dayOfWeek=dt.format('EEEE');
            if (dayOfWeek.equalsIgnoreCase('Saturday')) {
                System.assertEquals(true, result);
            } else {
                 System.assertEquals(false, result);
            }
		}
    }
	
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue7(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_domingo__c =  false;
            proceso.RQO_fld_sabado__c = false;
            proceso.RQO_fld_martes__c = false;
            proceso.RQO_fld_miercoles__c = false;
            proceso.RQO_fld_jueves__c = false;
            proceso.RQO_fld_viernes__c = false;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            Datetime dt = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
            String dayOfWeek=dt.format('EEEE');
            if (dayOfWeek.equalsIgnoreCase('Monday')) {
                System.assertEquals(true, result);
            } else {
                 System.assertEquals(false, result);
            }
		}
    }
	
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue8(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_domingo__c =  false;
            proceso.RQO_fld_sabado__c = false;
            proceso.RQO_fld_lunes__c = false;
            proceso.RQO_fld_miercoles__c = false;
            proceso.RQO_fld_jueves__c = false;
            proceso.RQO_fld_viernes__c = false;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            Datetime dt = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
            String dayOfWeek=dt.format('EEEE');
            if (dayOfWeek.equalsIgnoreCase('Tuesday')) {
                System.assertEquals(true, result);
            } else {
                 System.assertEquals(false, result);
            }
		}
    }
	
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue9(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_domingo__c =  false;
            proceso.RQO_fld_sabado__c = false;
            proceso.RQO_fld_lunes__c = false;
            proceso.RQO_fld_martes__c = false;
            proceso.RQO_fld_jueves__c = false;
            proceso.RQO_fld_viernes__c = false;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            Datetime dt = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
            String dayOfWeek=dt.format('EEEE');
            if (dayOfWeek.equalsIgnoreCase('Wednesday')) {
                System.assertEquals(true, result);
            } else {
                 System.assertEquals(false, result);
            }
		}
    }
	
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue10(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_domingo__c =  false;
            proceso.RQO_fld_sabado__c = false;
            proceso.RQO_fld_lunes__c = false;
            proceso.RQO_fld_martes__c = false;
            proceso.RQO_fld_miercoles__c = false;
            proceso.RQO_fld_viernes__c = false;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            Datetime dt = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
            String dayOfWeek=dt.format('EEEE');
            if (dayOfWeek.equalsIgnoreCase('Thursday')) {
                System.assertEquals(true, result);
            } else {
                 System.assertEquals(false, result);
        	}
		}
    }
	
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva true por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableTrue11(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_domingo__c =  false;
            proceso.RQO_fld_sabado__c = false;
            proceso.RQO_fld_lunes__c = false;
            proceso.RQO_fld_martes__c = false;
            proceso.RQO_fld_miercoles__c = false;
            proceso.RQO_fld_jueves__c = false;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            Datetime dt = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
            String dayOfWeek=dt.format('EEEE');
            if (dayOfWeek.equalsIgnoreCase('Friday')) {
                System.assertEquals(true, result);
            } else {
                 System.assertEquals(false, result);
            }
		}
    }
    
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva false
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableFalse(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
		System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_horaInicio__c = '23:30';
            proceso.RQO_fld_horaFin__c = '23:59';
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            System.assertEquals(false, result);
		}
    }
	
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método isRunnable() para que devuelva false por otro camino
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testIsRunnableFalse1(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
		System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            proceso.RQO_fld_domingo__c =  false;
            proceso.RQO_fld_sabado__c = false;
            proceso.RQO_fld_lunes__c = false;
            proceso.RQO_fld_martes__c = false;
            proceso.RQO_fld_miercoles__c = false;
            proceso.RQO_fld_jueves__c = false;
            proceso.RQO_fld_viernes__c = false;
            update proceso;
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            Boolean result = procesoPlanificado.isRunnable();
            System.assertEquals(false, result);
		}
    }
    
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método getNextScheduleDatetime(Boolean blnForceNewDay), pasandole true
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetNextScheduleDatetimeTrue(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
		System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            DateTime result = procesoPlanificado.getNextScheduleDatetime(true);
            System.assertNotEquals(null, result);
		}
    }
    
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método getNextScheduleDatetime(Boolean blnForceNewDay), pasandole false
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetNextScheduleDatetimeFalse(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
		System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            DateTime result = procesoPlanificado.getNextScheduleDatetime(false);
            System.assertNotEquals(null, result);
		}
    }

	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método formatTime(String strTime)
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testFormatTime(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
		System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            Time result = RQO_cls_ProcesoPlanificado.formatTime('16:30');
            System.assertNotEquals(null, result);
		}
    }
    
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método getNextSchedule()
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetNextSchedule(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
		System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            RQO_cls_ProcesoPlanificado.ScheduledInstance result = procesoPlanificado.getNextSchedule();
            System.assertNotEquals(null, result);
		}
    }
    
	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Test para probar el método getSchedule(Date d)
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetScheduleDate(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
		System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            RQO_cls_ProcesoPlanificado.ScheduledInstance result = procesoPlanificado.getSchedule(Date.today());
            System.assertNotEquals(null, result);
		}
    }

	/**
	* @creation date: 26/02/2018
	* @author: David Iglesias - Unisys

	* @description:	Test para probar el método getSchedule(DateTime d)
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testGetScheduleDateTime(){
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
		System.runAs(usuario){
            RQO_cs_procesoPlanificado__c proceso = RQO_cls_TestDataFactory.createCSprocesoPlanificado('MailNotificacion');
            RQO_cls_MailNotificacion_sch procesoPlanificado = new RQO_cls_MailNotificacion_sch();
            RQO_cls_ProcesoPlanificado.ScheduledInstance result = procesoPlanificado.getSchedule(DateTime.now());
            System.assertNotEquals(null, result);
		}
    }
    
}