/**
*	@name: RQO_cls_ListadoCategorias_test
*	@version: 1.0
*	@creation date: 27/02/2017
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar la clase RQO_cls_ListadoCategorias_cc
*/
@IsTest
public class RQO_cls_ListadoCategorias_test {
    /**
* @creation date: 27/02/2017
* @author: Juan Elías - Unisys
* @description:	Método que ejecuta toda la lógica de la clase para cubrirla
* @exception: 
* @throws: 
*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        
    }
    
    @isTest
    static void testListadoCategorias() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCategory(10);
            List<RQO_obj_category__c> listCategory = [SELECT Id, RQO_fld_type__c, RQO_fld_parentCategory__c FROM RQO_obj_category__c LIMIT 2];
            RQO_cls_TestDataFactory.createTranslation(10);
            
            List<RQO_obj_translation__c> listTranslation = [SELECT Id, RQO_fld_locale__c, RQO_fld_category__c FROM RQO_obj_translation__c LIMIT 2];
            listTranslation[0].RQO_fld_locale__c = 'es_ES';
            listTranslation[0].RQO_fld_category__c = listCategory[0].Id;
            update listTranslation[0];
            listTranslation[1].RQO_fld_locale__c = 'es_ES';
            listTranslation[1].RQO_fld_category__c = listCategory[1].id;
            update listTranslation[1];
            
            test.startTest();
            Map<String, List<RQO_obj_translation__c> > mapCategorias = new Map<String, List<RQO_obj_translation__c> >();
            mapCategorias = (Map<String, List<RQO_obj_translation__c> >)JSON.deserialize(RQO_cls_ListadoCategorias_cc.getMapCategorias('es_ES'), Map<String, List<RQO_obj_translation__c>>.class);
            System.assertEquals(1, mapCategorias.size(), 'No está cogiendo la categoría insertada');
            test.stopTest();
        }
    }
    
}