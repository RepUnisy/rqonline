/*------------------------------------------------------------------------
* 
* 	@name: 				DCPAI_cls_AssignToIP
* 	@version: 			1 
* 	@creation date: 	30/06/2017
* 	@author: 			Alfonso Constán López	-	Unisys
* 	@description: 		Class to assign cost item to investment plan
* 
----------------------------------------------------------------------------*/
global without sharing class DCPAI_cls_AssignToIP {
    
    @InvocableMethod
    public static void DCPAI_AssignToIP_CostItem (List<ITPM_Budget_Item__c> costItem) {
        system.debug('Entra en la clase: class DCPAI_cls_AssignToIP' );
        List<Id> listId = new List<Id>();
        for(ITPM_Budget_Item__c ciP : costItem){
            listId.add(ciP.id);
        }       
        
        List<ITPM_Budget_Item__c> updateCostItem = new List<ITPM_Budget_Item__c>();
        // Obtenemos el cost item modificado
        if (costItem.size() != 0){
            // obtenemos la información del cost item
            List<ITPM_Budget_Item__c> ListCiBuscado = [Select id, DCPAI_fld_Investment_Plan__c, ITPM_SEL_Year__c from ITPM_Budget_Item__c where id in : listId];         
            for(ITPM_Budget_Item__c ciBuscado : ListCiBuscado){
                if(ciBuscado.ITPM_SEL_Year__c != null && ciBuscado.ITPM_SEL_Year__c != ''){
                    //	Buscamos el plan de inversion correspondiente al año seleccionado. Solo existirá 1 plan por año
                    List<DCPAI_obj_Investment_Plan__c> ip = [select id from DCPAI_obj_Investment_Plan__c where DCPAI_Year__c =: ciBuscado.ITPM_SEL_Year__c limit 1];
                    if (ip.size() != 0){
                        system.debug('Existe un plan de inversión ' + ip);                  
                        ciBuscado.DCPAI_fld_Investment_Plan__c = ip.get(0).id;
                    }
                    else{
                        system.debug('No existe un plan de inversión');
                        ciBuscado.DCPAI_fld_Investment_Plan__c = null;
                    }
                }
                else{
                    ciBuscado.DCPAI_fld_Investment_Plan__c = null;
                }
                
                //	Una vez modificado, el cost item pasa a estado inprogress
                ciBuscado.DCPAI_fld_Assing_Status__c = Label.DCPAI_Status_InProgress;
                system.debug('El cost item para actualizar: ' + ciBuscado);
                updateCostItem.add(ciBuscado);
                
            }
        }
        if (updateCostItem.size() != 0){
            update updateCostItem;
        }
    }    

}