@isTest
public with sharing class TRAD_TEST_eventTriggerAfterInsert {
	@testSetup
    static void dataTest(){
        Account acc = new Account();
        acc.Name = 'Account Name';
        insert acc;
        
        Contact cont = new Contact();
        cont.FirstName = 'First Name';
        cont.LastName = 'Last Name';
        cont.AccountId = acc.Id;
        insert cont;
    }
    
    @isTest
    static void testTrigger(){
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Contact cont = [SELECT Id, AccountId FROM Contact LIMIT 1];
        Test.startTest();
        
        Event newEventWAccount = new Event();
        newEventWAccount.Subject = 'Subject';
        newEventWAccount.WhatId = acc.Id;
        newEventWAccount.DurationInMinutes = 60;
        newEventWAccount.ActivityDateTime = DateTime.Now();
        insert newEventWAccount;
        
        Event newEventWOAccount = new Event();
        newEventWOAccount.Subject = 'Subject';
        newEventWOAccount.WhoId = cont.Id;
        newEventWOAccount.DurationInMinutes = 60;
        newEventWOAccount.ActivityDateTime = DateTime.Now();
        insert newEventWOAccount;
        
        Test.stopTest();
    }    
}