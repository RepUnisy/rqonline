/**
 * rsimarro Clase que contiene la funcionalidad del botón Siguiente en Auditoria__c
 */
global class PT_button_siguiente {

    webservice static integer hayMasElementos(Id idobjeto){
        
        system.debug('@@@ clase PT_button_siguiente method hayMasElementos() ');
           
        if(doPasarSiguiente(idobjeto) != idobjeto){          

            return 1;
        }
        else{
            return -1;
        }
          
    }
    
    /**
     * método que devuelve el id del objeto siguiente, si lo hay. Si nó, devuelve el actual
     */
    webservice static Id doPasarSiguiente(Id idobjeto){
        
         system.debug('@@@ clase PT_button_siguiente method doPasarSiguiente() ');
        
        try{
      
            for(SObject aux: Database.Query('select Id FROM '+idobjeto.getSObjectType().getDescribe().getName()+' where Id > \''+idobjeto+'\' LIMIT 1') ){
        	             
                                            system.debug('@@@ clase PT_button_siguiente method doPasarSiguiente(): elemento siguiente '+aux.Id);
                                        
                                            return aux.Id;   
                                        }       
  
                           
            system.debug('@@@ clase PT_button_siguiente method doPasarSiguiente(): no hay mas elementos');

            return idobjeto;               
        }
        catch(Exception e){
            system.debug('@@@ '+e.getMessage());
            return Label.error_generico;
        }
       
    }
}