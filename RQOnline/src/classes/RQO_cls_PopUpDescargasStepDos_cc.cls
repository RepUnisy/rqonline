/**
*	@name: RQO_cls_PopUpDescargasStepDos_cc
*	@version: 1.0
*	@creation date: 05/12/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Controlador apex para la descarga de doc umentos de tipo Albarán y Certificado de Analisis
*
*/
public class RQO_cls_PopUpDescargasStepDos_cc {
	
    /**
    * @creation date: 07/12/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Obtiene el documento a descargar 
    * @param: idCliente tipo String.
    * @param: numDocumento tipo String
    * @param: idioma tipo String
    * @param: tipo tipo String, id del grado.    
    * @return: Map<String, List<RQO_cls_LCDocDataAux>>, mapa con el tipo de documento y una lista de los diferentes idiomas e ids de documento almacenados en un objeto 
    * RQO_cls_LCDocDataAux
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static String[] getDocumentData(String idCliente, String numDocumento, String tipo){
        RQO_cls_GetOrderDocsWS getDocObj = new RQO_cls_GetOrderDocsWS();
		RQO_cls_GetOrderDocsWS.OrderDocCustomResponse dataReturn = null;
        Account cuenta = [Select RQO_fld_idExterno__c from Account where Id = : idCliente];
        //Si es albarán
        if (RQO_cls_Constantes.PED_COD_DOWNLOAD_ALBARAN_CMR.equalsIgnoreCase(tipo)){
            dataReturn = getDocObj.getDeliveryNote(cuenta.RQO_fld_idExterno__c, numDocumento);
        }else{
            //Si es certificado de analisis
            dataReturn = getDocObj.getAnilisysCertificate(cuenta.RQO_fld_idExterno__c, numDocumento, RQO_cls_Constantes.COD_SAP_IDIOMA_ESPANOL);
        }

        return new List<String>{dataReturn.errorCode, dataReturn.errorMessage, dataReturn.docData, numDocumento};
    }

}