/*------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    Clase controladora de la Matriz de Riesgos asociados a proyectos
Test Class:     ITPM_controller_Risk_PSC_Test
History
<Date>          <Author>        <Change Description>
30-Jun-2016     Rubén Simarro   Initial Version
------------------------------------------------------------*/
global class ITPM_Button_Clone_Matrix {

 
    //metodo que genera e inserta en BD un duplicado del objeto ITPM_Matrix_Risk__c y de todas las filas
    //ITPM_Matrix_risk_row__c que lo forman
    //si la operacion va bien, devuelve el ID de la nueva Matriz
    webservice static String cloneMatrixRisk(Id matrix_risk_id){
       
        try{
            system.debug('@@@@@ ITPM_Button_Clone_Matrix CLONE  @@@@@@@@'); 
            
            ITPM_Matrix_Risk__c newMatrix = new ITPM_Matrix_Risk__c(ITPM_Matrix_risk_BOL_Is_cloned__c=true, ITPM_Matrix_risk_REL_Project__c= [SELECT ITPM_Matrix_risk_REL_Project__c FROM ITPM_Matrix_Risk__c WHERE id=: matrix_risk_id].ITPM_Matrix_risk_REL_Project__c);
            insert newMatrix;
                 
            List <ITPM_Matrix_risk_row__c> listNewMatrixRiskFile = new List<ITPM_Matrix_risk_row__c>();

            FOR(ITPM_Matrix_risk_row__c IdListMAtrixRow : [SELECT id from ITPM_Matrix_risk_row__c WHERE ITPM_Matrix_Risk__c =: matrix_risk_id]){
                          
                String soql = getAtributosObject(IdListMAtrixRow.Id.getSObjectType().getDescribe().getName(),'id=\''+IdListMAtrixRow.Id+'\'');

                ITPM_Matrix_risk_row__c matrixRiskFile = (ITPM_Matrix_risk_row__c)Database.query(soql);   
               
                //clone(preserveId, isDeepClone, preserveReadonlyTimestamps, preserveAutonumber)
                ITPM_Matrix_risk_row__c newMatrixRiskFile = matrixRiskFile.clone(false,true,false,false);              
                newMatrixRiskFile.ITPM_Matrix_Risk__c = newMatrix.Id;
                
                listNewMatrixRiskFile.add(newMatrixRiskFile);
            }
            insert listNewMatrixRiskFile;

            return  newMatrix.Id;                   
        }        
        catch(Exception e){
        
            system.debug('@@@ ERROR ITPM_Button_Clone_Matrix cloneMatrixRisk(): '+e.getMessage());
           
            return matrix_risk_id;
        }
        
    }
      
    //metodo que devuelve una SOQL dinamica con todos los campos creados por el usuario
    @TestVisible
    private static String getAtributosObject(String objectName, String whereClause){
         
        try{       
          
            String selects = '';
                    
            if(Schema.getGlobalDescribe().get(objectName.toLowerCase()) == null){ 
                system.debug('@@@ ERROR getAtributosObject(): debe indicar un nombre de objeto válido: '+objectName);
                return null; 
            }
            else{
            
                Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
                                                         
                if (fMap != null && whereClause != null && whereClause != ''){
                               
                     List<String> selectFields = new list<string>();
                    
                        for (Schema.SObjectField ft : fMap.values()){ //recorrecmos todos los field tokens (ft)
                            Schema.DescribeFieldResult fd = ft.getDescribe();
                            if (fd.isCreateable()){ 
                                selectFields.add(fd.getName());
                            }
                        }
               
                        if (!selectFields.isEmpty()){
                            for (string s:selectFields){
                                selects += s + ',';
                            }
                            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
                             
                        }
            
                     String soql = 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
            
                    system.debug('@@@ getAtributosObject(): '+ soql);
                  
                    return soql;                
                }
                else{            
                    system.debug('@@@ ERROR getAtributosObject(): debe indicar el parámetro WHERE no vacío y bien formado.');
                    return null; 
                }                                         
            }          
        }      
        catch(Exception e){  
             
            system.debug('@@@ ERROR getAtributosObject(): '+ e.getMessage());       
            return null;          
        }     
    }
    
}