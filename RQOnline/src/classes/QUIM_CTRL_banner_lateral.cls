public class QUIM_CTRL_banner_lateral{
    public string lang {get; set;}
    public string page {get; set;}
    public string product {get; set;}
    
    public QUIM_CTRL_banner_lateral(){
        
        lang = (String) ApexPages.currentPage().getParameters().get('cclcl');
        
        page = (String) ApexPages.currentPage().getParameters().get('pageKey');

        product = (String) ApexPages.currentPage().getParameters().get('product');

    }
}