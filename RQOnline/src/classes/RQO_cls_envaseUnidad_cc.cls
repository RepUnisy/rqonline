/**
*   @name: RQO_cls_envaseUnidad_cc
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que relaciona un envase con su unidad de medida y gestiona esa relacion
*/
public without sharing class RQO_cls_envaseUnidad_cc {
    /**
    *   @name: RQO_cls_envaseUnidad_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase que relaciona un envase con su unidad de medida
    */
    public class envaseUnidadMedida{
        string envase = '';
        string unidadMedida = '';
        decimal pesoMaximo ;
        decimal pesoMinimo ;
        decimal multiploKgUnidad;
        boolean excluirValidacion = false;
        
        public envaseUnidadMedida(string envase, string unidadMedida, decimal pesoMaximo, decimal pesoMinimo, decimal kgUnidad, boolean excluirValidacion){
            this.envase = envase;
            this.unidadMedida = unidadMedida;
            this.pesoMaximo = pesoMaximo;
            this.pesoMinimo = pesoMinimo;
            this.excluirValidacion = excluirValidacion;
            this.multiploKgUnidad = kgUnidad;
        }
    }
    
    
    /**
    * @creation date: 20/12/2017
    * @author: Alfonso Constán - Unisys
    * @description: Método para la obtención traducciones de envases y unidad de medida
    * @param: 
    * @return: String. Mapa serializada traduccciones
    * @exception: 
    * @throws: 
    */  
    @AuraEnabled
    public static String getEnvaseUnidadMedida(String language) {
        if (language == null){
            language = 'en_US';
        }
        
        Map<String, List<RQO_cls_seleccionador>>  mapConbinatoria = new Map<String, List<RQO_cls_seleccionador>>();
        
        RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
        //  Obtenemos un mapa con las unidades de medida y envases. Clave el código y valor su traducción
        Map<String, String>  mapTradEnvaseUidadMedida = util.traduccionesEnvasesUnidadMedida(language);
        
        List<RQO_obj_container__c>  listEnvases = [select id, RQO_fld_codigo__c, (select id, RQO_fld_shippingCode__c, RQO_fld_shippingMode__c from Shipping_Junction__r) from RQO_obj_container__c ];
        
        for (RQO_obj_container__c env : listEnvases) {
            List<RQO_cls_seleccionador> listUnidad = new List<RQO_cls_seleccionador>();
            
            //  Recorremos sus unidades de medida
            for (RQO_obj_shippingJunction__c unidaMedi : env.Shipping_Junction__r) {
                //  Obtenemos la trducción si existiera. En caso de no existir introducimos el código
                String tradUnidad = mapTradEnvaseUidadMedida.containsKey(unidaMedi.RQO_fld_shippingCode__c) ? mapTradEnvaseUidadMedida.get(unidaMedi.RQO_fld_shippingCode__c) : unidaMedi.RQO_fld_shippingCode__c;
                //  Creamos un seleccionador, donde introducimos el id y la traduccion
                RQO_cls_seleccionador tradUnidadSelect = new RQO_cls_seleccionador(unidaMedi.RQO_fld_shippingMode__c, tradUnidad);
                listUnidad.add(tradUnidadSelect);
            }
            
            //  Obtenemos la trducción si existiera. En caso de no existir introducimos el código
            String tradEnvase = mapTradEnvaseUidadMedida.containsKey(env.RQO_fld_codigo__c) ? mapTradEnvaseUidadMedida.get(env.RQO_fld_codigo__c) : env.RQO_fld_codigo__c;
            //  Creamos un seleccionador, donde introducimos el id y la traduccion
            RQO_cls_seleccionador tradEnvaseSelec = new RQO_cls_seleccionador(env.id, tradEnvase);
            
            if ( !listUnidad.isEmpty() ) {                
                mapConbinatoria.put(JSON.serialize(tradEnvaseSelec), listUnidad);
            }            
        }
        return JSON.serialize(mapConbinatoria);        
    }
    
    /**
    *   @name: RQO_cls_envaseUnidad_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase que obtiene todos los Shipping Junction
    */
    @AuraEnabled
    public static String getAllShippingJuntion(){
        
        Map<string, envaseUnidadMedida> mapEnvaseUndMedida = new Map<string, envaseUnidadMedida>();
        
        List<RQO_obj_shippingJunction__c> listShipping = [select id,
                                                          RQO_fld_containerCode__c,
                                                          RQO_fld_shippingCode__c,
                                                          RQO_fld_container__c,
                                                          RQO_fld_shippingMode__c,
                                                          RQO_fld_maximumWeightKg__c,
                                                          RQO_fld_minimumWeightKg__c,
                                                          RQO_fld_multiploKgUnidad__c,
                                                          RQO_fld_excluirValidacion__c 
                                                          from RQO_obj_shippingJunction__c];
        
        //  Recorremos los envases 
        for (RQO_obj_shippingJunction__c cont : listShipping) {
            //  Insertamos la combinatoria del shipping_juntion
            mapEnvaseUndMedida.put(String.valueOf(cont.RQO_fld_container__c) + String.valueOf(cont.RQO_fld_shippingMode__c) , new envaseUnidadMedida(cont.RQO_fld_containerCode__c, cont.RQO_fld_shippingCode__c, cont.RQO_fld_maximumWeightKg__c, cont.RQO_fld_minimumWeightKg__c, cont.RQO_fld_multiploKgUnidad__c, cont.RQO_fld_excluirValidacion__c ));
            
        }
        return JSON.serialize(mapEnvaseUndMedida);
    }
  
    
}