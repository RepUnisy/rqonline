/**
*   @name: RQO_cls_BorradoRegistros_Test
*   @version: 1.0
*   @creation date: 21/02/2018
*   @author: David Iglesias - Unisys
*   @description: Clase de test para probar la programación del borrado fisico de registros
*/
@IsTest
public class RQO_cls_BorradoRegistros_Test {

    /**
    * @creation date: 22/02/2018
    * @author: David Iglesias - Unisys
    * @description: Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSprocesoPlanificado('BorradoHistoricoRegistros');

            RQO_cls_TestDataFactory.createCStiempoBorradoRegistros('RQO');
            List<RQO_obj_auxBulkObject__c> listaAuxBulkObjectList = RQO_cls_TestDataFactory.createAuxBulkObject(200);
            Test.setCreatedDate(listaAuxBulkObjectList.get(0).Id, System.now().addDays(-60));
            RQO_obj_auxBulkObject__c item = [select Id, CreatedDate FROM RQO_obj_auxBulkObject__c WHERE ID =:listaAuxBulkObjectList.get(0).Id];
            system.debug('item: '+item);
            List<RQO_obj_logProcesos__c> listaLogProcesos = RQO_cls_TestDataFactory.createLogProcesos(200);
            Test.setCreatedDate(listaLogProcesos.get(0).Id, System.now().addDays(-60));
            List<RQO_obj_gradoIntermedio__c> listaGradoIntermedio = RQO_cls_TestDataFactory.createGradoIntermedio(200);
            Test.setCreatedDate(listaGradoIntermedio.get(0).Id, System.now().addDays(-60));
        }
    }
    
    /**
    * @creation date: 22/02/2018
    * @author: David Iglesias - Unisys
    * @description: Método test para probar la funcionalidad correcta dentro de la franja horaria de permiso
    * @exception: 
    * @throws:  
    */
    @isTest
    static void testEjecucionEnHora(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_obj_auxBulkObject__c item = [select Id, CreatedDate FROM RQO_obj_auxBulkObject__c Limit 1];
            item.RQO_fld_verificado__c = true;
            update item;
            RQO_cls_BorradoRegistros_sch scheduler = new RQO_cls_BorradoRegistros_sch();
            RQO_cls_ProcesoPlanificado.ScheduledInstance instance = scheduler.getNextSchedule();
            System.schedule(instance.name + 'Test', instance.cron, new RQO_cls_BorradoRegistros_sch());
            
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 22/02/2018
    * @author: David Iglesias - Unisys
    * @description: Método test para probar la funcionalidad correcta fuera de la franja horaria de permiso
    * @exception: 
    * @throws: 
    */
    static testMethod void testEjecucionFueraHora(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserCliente@testorg.com');
        System.runAs(u){
            
            String CRON_EXP = '0 0 0 15 3 ? 2022';
            
            RQO_cs_procesoPlanificado__c cs = [SELECT Id,RQO_fld_domingo__c,RQO_fld_jueves__c,RQO_fld_lunes__c,RQO_fld_martes__c,RQO_fld_miercoles__c,
                                                    RQO_fld_sabado__c,RQO_fld_viernes__c 
                                                FROM RQO_cs_procesoPlanificado__c 
                                                WHERE Name = 'BorradoHistoricoRegistros'];
            
            cs.RQO_fld_lunes__c = false;
            cs.RQO_fld_martes__c = false;
            cs.RQO_fld_miercoles__c = false;
            cs.RQO_fld_jueves__c = false;
            cs.RQO_fld_viernes__c = false;
            cs.RQO_fld_sabado__c = false;
            cs.RQO_fld_domingo__c = false;
            
            update cs;
            
            test.startTest();

            RQO_cls_BorradoRegistros_sch scheduler = new RQO_cls_BorradoRegistros_sch();
            RQO_cls_ProcesoPlanificado.ScheduledInstance instance = scheduler.getNextSchedule();
            System.schedule(instance.name + 'Test', instance.cron, new RQO_cls_BorradoRegistros_sch());

            test.stopTest();
        }
    }
}