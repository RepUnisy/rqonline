/**
*   @name: RQO_cls_ContactSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la selección u obtención de contactos
*/
public with sharing class RQO_cls_ContactSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_ContactSelector';
	
    /**
    *   @name: RQO_cls_ContactSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la lista de campos del objeto Contact
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			Contact.Name,
			Contact.FirstName,
			Contact.LastName,
			Contact.Email,
			Contact.MobilePhone,
			Contact.RQO_fld_perfilCliente__c,
			Contact.RQO_fld_perfilUsuario__c,
			Contact.RQO_fld_Pais__c,
			Contact.RQO_fld_Idioma__c,
            Contact.RQO_fld_corpInfConsent__c,
            Contact.RQO_fld_commInfConsent__c,
            Contact.RQO_fld_surveyConsent__c,
            Contact.RQO_fld_techInfConsent__c,
			Contact.Account.Name };
    }
	
    /**
    *   @name: RQO_cls_ContactSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el sObjectType del objeto Contact
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public Schema.SObjectType getSObjectType()
    {
        return Contact.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_ContactSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public override String getOrderBy()
	{
		return 'Name';
	}
	
    /**
    *   @name: RQO_cls_ContactSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de objetos Contact mediante un conjunto de ids de contacto
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<Contact> selectById(Set<ID> idSet)
    {
         return (List<Contact>) selectSObjectsById(idSet);
    }
    
    /**
    *   @name: RQO_cls_ContactSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de objetos Contact mediante un conjunto de ids de cuentas
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<Contact> selectByAccountId(Set<ID> idSet)
    {
 		List<Contact> contacts =  (List<Contact>) Database.query(newQueryFactory().setCondition('AccountId in :idSet').toSOQL()); 

		return contacts;							                
    }
}