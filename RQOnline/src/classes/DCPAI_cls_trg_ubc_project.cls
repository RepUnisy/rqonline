/*------------------------------------------------------------------------ 
Author:         Alfonso Constán López 
Company:        Unisys
Description:    trigger
History
<Date>          <Author>                <Description>
06-Jul-2017     Alfonso Constán López   Initial version
----------------------------------------------------------------------------*/

public class DCPAI_cls_trg_ubc_project{
    public static void validateCreationModification(Boolean isAdmin, List<ITPM_UBC_Project__c> triggernew) {
        if(!isAdmin){
            ITPM_UBC_Project__c ubc = triggernew[0];
            List<ITPM_Budget_Item__c> listaCostItems = new List<ITPM_Budget_Item__c>([SELECT Id, DCPAI_fld_Assing_Status__c FROM ITPM_Budget_Item__c WHERE DCPAI_fld_Budget_Investment__c = :ubc.DCPAI_fld_Budget_Investment__c AND ITPM_SEL_Country__c = :ubc.ITPM_SEL_Delivery_Country__c AND ITPM_SEL_Year__c = :ubc.ITPM_SEL_Year__c AND DCPAI_fld_Code__c = :ubc.DCPAI_fld_Code_of_project__c]);
            if(listaCostItems.size()>0){
                if(listaCostItems[0].DCPAI_fld_Assing_Status__c == Label.DCPAI_CostItem_Status_Assigned){
                    ubc.adderror('You can\'t create or change UBCs that are assigned');
                }
            }
        }
    }
}