@IsTest public with sharing class TaCTest{
	@IsTest(SeeAllData=true) public static void testSave() {
      Tac tac = new TaC();
        tac.currentUser = new user();
        tac.TACAcceptedCheckBox = true;
        
        PageReference page = new PageReference('/home/home.jsp');
        
        TaC_Custom_Setting__c settings = TaC_Custom_Setting__c.getOrgDefaults();
		Blob key = EncodingUtil.base64Decode(settings.Key__c);
        Blob user = EncodingUtil.base64Decode(settings.User__c);
        Blob pass = EncodingUtil.base64Decode(settings.Pass__c);
            
        Blob blobUserDecrypt = Crypto.decryptWithManagedIV('AES256', key, user);
		String user2 = blobUserDecrypt.toString();
        System.debug(user2);
        
        Blob blobPassDecrypt = Crypto.decryptWithManagedIV('AES256', key, pass);
		String pass2 = blobPassDecrypt.toString();
        System.debug(pass2);
        
        System.assertEquals(tac.Save(),null);
    }
}