/**
*	@name: RQO_cls_FacturasGeneral_test
*	@version: 1.0
*	@creation date: 27/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase RQO_cls_FacturasGeneral
*/
@isTest
public class RQO_cls_FacturasGeneral_test {

    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserFacturas@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la ejecución correcta del servicio de obtención de facturas
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getFacturas(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFacturas@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createQp0Grade(1);
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_INVOICES, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
                List <RQO_cls_FacturaBean> listaFacturas = RQO_cls_FacturasGeneral.getFacturasListBySearch('test', 'test', null, new List<String>{'TESTMATERIAL'}, 
																	'TestFact', null, null, null, String.valueOf(Date.today()), String.valueOf(Date.today()), null, null, null, null, null, null);            
			test.stopTest();
            
            System.assertEquals(1, listaFacturas.size());
        }
    }
	
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la ejecución incorrecta del servicio de obtención de facturas
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getFacturasNOK(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFacturas@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createQp0Grade(1);
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_INVOICES, RQO_cls_Constantes.MOCK_EXEC_NOK, '0'));
				List <RQO_cls_FacturaBean> listaFacturas = RQO_cls_FacturasGeneral.getFacturasListBySearch('test', 'test', null, new List<String>{'TESTMATERIAL'}, 
																	'TestFact', null, null, null, String.valueOf(Date.today()), String.valueOf(Date.today()), null, null, null, null, null, null);                     
			test.stopTest();
            
            System.assertEquals(true, listaFacturas[0].error);
        }
    }
    
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar la gestión de errores en la clase cuando no mandamos id de solicitante
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getFacturasNoSolicitante(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFacturas@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createQp0Grade(1);
            test.startTest();
                List <RQO_cls_FacturaBean> listaFacturas = RQO_cls_FacturasGeneral.getFacturasListBySearch(null, null, null, null, null, null, null, null, null, 
                                                                                                           null, null, null, null, null, null, null);            
			test.stopTest();
            
            System.assertEquals(1, listaFacturas.size());
        }
    }
    
	/**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para probar el bean de factura pedido
    * @exception: 
    * @throws:  
    */
    @isTest
    static void facturaBean(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserFacturas@testorg.com');
        System.runAs(u){
            test.startTest();
                List<RQO_cls_FacturaBean> listaFactura = new List<RQO_cls_FacturaBean>();
                RQO_cls_FacturaBean factBean = new RQO_cls_FacturaBean();
            	List<RQO_cls_FacturaBean.RQO_cls_DetalleFacturaWrapper> listFacWrraper = new List<RQO_cls_FacturaBean.RQO_cls_DetalleFacturaWrapper>();
				RQO_cls_FacturaBean.RQO_cls_DetalleFacturaWrapper facWrapper = new RQO_cls_FacturaBean.RQO_cls_DetalleFacturaWrapper();
                    facWrapper.numRefCliente = 'test';
                    facWrapper.numPedido ='test';
                    facWrapper.numAlbaran ='test';
                    facWrapper.grado ='test';
                    facWrapper.cantidad ='test';
                    facWrapper.tipoCantidad ='test';
                    facWrapper.precioTonelada ='test';
                    facWrapper.precioToneladaFormateado ='test';
                    facWrapper.importe ='test';
                    facWrapper.importeNetoFormateado ='test';
                    facWrapper.destino ='test';
            		listFacWrraper.add(facWrapper);
				factBean.numFactura = 'test';
                factBean.fechaEmision = date.today();
                factBean.fechaVencimiento =  date.today();
                factBean.importe = 'test';
                factBean.importeFormateado = 'test';
                factBean.tipoMoneda = 'test';
                factBean.tipo = 'test';
                factBean.estado = 'test';
                factBean.error = false;
                factBean.mensajeError = 'test';
                factBean.idArchivado = 'test';
                factBean.detallesList = listFacWrraper;
            	listaFactura.add(factBean);
            
            	RQO_cls_FacturaBean factBean2 = new RQO_cls_FacturaBean();
				factBean2.numFactura = 'test';
                factBean2.fechaEmision = date.today();
                factBean2.fechaVencimiento =  date.today();
                factBean2.importe = 'test';
                factBean2.importeFormateado = 'test';
                factBean2.tipoMoneda = 'test';
                factBean2.tipo = 'test';
                factBean2.estado = 'test';
                factBean2.error = false;
                factBean2.mensajeError = 'test';
                factBean2.idArchivado = 'test';                
            	listaFactura.add(factBean2);
				listaFactura.sort();
            
                RQO_cls_Constantes.FAC_ORDER_BY_FECHA_EMISION = true;
                listaFactura.sort();
            	RQO_cls_Constantes.FAC_ORDER_BY_FECHA_EMISION = false;
                RQO_cls_Constantes.FAC_ORDER_BY_FECHA_VENCIMIENTO = true;
                listaFactura.sort();
			test.stopTest();
            
            System.assert(listaFactura != null);
        }
    }
    
}