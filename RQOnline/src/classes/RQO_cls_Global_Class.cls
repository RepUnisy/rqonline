/**
*   @name: RQO_cls_Global_Class
*   @version: 1.0
*   @creation date: 09/10/2017
*   @author: Alfonso Constán López - Unisys
*   @description: 
*
*/
global class RQO_cls_Global_Class {
    
    
    /**
    * @creation date: 05/09/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Obtiene las traducciones de los grados
    * @param: language: idioma de 
    * @return: 
    * @exception: 
    * @throws: 
    */
    public static Map<Id, RQO_obj_translation__c> getGradeTranslate(String language, List<Id> listGrade){
        Map<Id, RQO_obj_translation__c> mapTraducciones = new Map<Id, RQO_obj_translation__c>();
        
        //	Obtenemos las traducciones
        List<RQO_obj_translation__c> listTranslation = [select id, RQO_fld_translation__c, RQO_fld_detailTranslation__c, RQO_fld_grade__c from RQO_obj_translation__c where RQO_fld_grade__c in: listGrade and RQO_fld_locale__c = :language];
        
        for (RQO_obj_translation__c trad : listTranslation){
            //	Comprobamos que no se haya introducido ya una traduccion
            if (! mapTraducciones.containsKey(trad.RQO_fld_grade__c)){
                mapTraducciones.put(trad.RQO_fld_grade__c, trad);
            }            
        }        
        return mapTraducciones;
    } 
    /**
    * @creation date: 16/10/2017
    * @author: Nicolás García Muñoz - Unisys
    * @description: Obtiene las traducciones de los grados pasando como parámetro una lista de Grados
    * @param: language: idioma de 
    * @return: 
    * @exception: 
    * @throws: 
    */
    public static Map<Id, RQO_obj_translation__c> getGradeTranslate(String language, List<RQO_obj_grade__c> listGrade){
        System.debug('language: ' + language + ' listGrade: ' + listGrade);
        Map<Id, RQO_obj_translation__c> mapTraducciones = new Map<Id, RQO_obj_translation__c>();
        
        //	Obtenemos las traducciones
        List<RQO_obj_translation__c> listTranslation = [select id, RQO_fld_translation__c, RQO_fld_detailTranslation__c, RQO_fld_grade__c from RQO_obj_translation__c where RQO_fld_grade__c in: listGrade and RQO_fld_locale__c = :language];
        
        for (RQO_obj_translation__c trad : listTranslation){
            //	Comprobamos que no se haya introducido ya una traduccion
            if (! mapTraducciones.containsKey(trad.RQO_fld_grade__c)){
                mapTraducciones.put(trad.RQO_fld_grade__c, trad);
            }            
        }        
        return mapTraducciones;
    }    
}