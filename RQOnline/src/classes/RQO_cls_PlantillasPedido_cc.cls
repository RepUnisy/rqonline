/**
*   @name: RQO_cls_PlantillasPedido_cc
*   @version: 1.0
*   @creation date: 15/12/2017
*   @author: Alfonso Constán López - Unisys
*   @description: Controlador Apex para la gestión de plantillas de pedido
*
*/
public without sharing class RQO_cls_PlantillasPedido_cc {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_PlantillasPedido_cc';
    
    // ***** METODO PUBLICOS ***** //
    
    /**
* @creation date: 20/12/2017
* @author: David Iglesias - Unisys
* @description: Método para el borrado fisico de un registro RQO_obj_templatePosition__c
* @param: idPlantilla		Id de la posición de plantilla a borrar
* @return: 
* @exception: 
* @throws: 
*/
    @AuraEnabled
    public static void deleteTemplatePositionManagement (String idTemplatePosition) {
        
        /**
* Constantes
*/
        final String METHOD = 'deleteTemplatePositionManagement';
        
        /**
* Variables
*/
        RQO_obj_templatePosition__c templatePosition = null;
        
        /**
* Inicio
*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        templatePosition = new RQO_obj_templatePosition__c(Id = idTemplatePosition);
        delete templatePosition;
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');		
    }
    
    /**
* @creation date: 20/12/2017
* @author: David Iglesias - Unisys
* @description: Método para el borrado fisico de un registro RQO_obj_templateManagement__c
* @param: idPlantilla		Id de plantilla a borrar
* @return: 
* @exception: 
* @throws: 
*/
    @AuraEnabled
    public static String deteleTemplateManagement (String idTemplate) {
        
        /**
* Constantes
*/
        final String METHOD = 'deteleTemplateManagement';
        
        /**
* Variables
*/
        RQO_obj_templateManagement__c templateManagement = null;
        
        /**
* Inicio
*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        
        templateManagement = new RQO_obj_templateManagement__c(Id = idTemplate);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        delete templateManagement;
        return idTemplate;
        
        
    }
    
    /**
* @creation date: 03/01/2017
* @author: Alfonso Constán López - Unisys
* @description: Método para la creación de una plantilla
* @param: templateName		Nombre de la plantilla
* @param: idContact			Id del contacto
* @return: 
* @exception: 
* @throws: 
*/
    @AuraEnabled
    public static void createTemplate (String templateName, Id idAccount, Id idContact){
        RQO_obj_templateManagement__c template = new RQO_obj_templateManagement__c(Name = templateName,
                                                                                   RQO_fld_contact__c = idContact,
                                                                                   RQO_fld_account__c = idAccount );
        insert template;
    }
    
    /**
* @creation date: 20/12/2017
* @author: David Iglesias - Unisys
* @description: Método para la modificación del nombre de la plantilla
* @param: idTemplate		Identificador de la plantilla a modificar
* @param: templateName		Nombre de la plantilla a modificar
* @return: Boolean			Resultado de la modificación
* @exception: 
* @throws: 
*/
    @AuraEnabled
    public static Boolean createOrModifyTemplate (String idTemplate, String templateName) {
        
        /**
* Constantes
*/
        final String METHOD = 'createOrModifyTemplate';
        
        /**
* Variables
*/
        Integer count = 0;
        Boolean modified = false;
        RQO_obj_templateManagement__c templateManagement = null;
        
        /**
* Inicio
*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        try {
            
            // Comprobación de existencia de otra plantilla con el mismo nombre
            count = database.countQuery('SELECT count() from RQO_obj_templateManagement__c WHERE Name = :templateName');
            System.debug('****Count: ' + count);
            if (count == 0) {
                modified = true;
                templateManagement = new RQO_obj_templateManagement__c();
                templateManagement.Id = (String.isNotBlank(idTemplate))?idTemplate:null;
                templateManagement.Name = templateName;
                
                // Actualización del registro
                upsert templateManagement;
            }
            
        } catch (Exception e) {
            System.debug('Excepción generica: '+e.getTypeName()+' - '+e.getMessage());
            modified = false;
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return modified;
    }
    
    /**
    * @creation date: 20/12/2017
    * @author: David Iglesias - Unisys
    * @description: Método para la obtención de plantillas asociadas a un usuario
    * @param: idContact		Identificador del contacto
    * @return: String		Lista serializada de plantillas asociadas al usuario
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static String getTemplates (String idAccount, String idContact) {
        
        /**
        * Constantes
        */
        final String METHOD = 'getTemplates';
        
        /**
        * Variables
        */
        RQO_cls_TemplateManagementBean templateManagementBean = null;
        RQO_cls_TemplateManagementPositionBean templateManagementPositionBean = null;
        List<RQO_cls_TemplateManagementBean> templateManagementBeanList = new List<RQO_cls_TemplateManagementBean> ();
        Set<Id> gradosQP0Set = new Set<Id> ();
        Set<String> templateSet = new Set<String> ();
        Set<String> gradosCountSet = null;
        Set<String> destinosCountSet = null;
        Map<String,RQO_cls_TemplateManagementBean> templateManagementMap = new Map<String,RQO_cls_TemplateManagementBean> ();
        Map<String,Set<String>> gradosCountByTemplateManagementMap = new Map<String,Set<String>> ();
        Map<String,Set<String>> destinosCountByTemplateManagementMap = new Map<String,Set<String>> ();
        Map<Id, List<Id>> containerByQP0GradeMap = null;
        
        /**
        * Inicio
        */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        try {
            List<RQO_obj_templateManagement__c> listTemplateVacio = [select id, Name from RQO_obj_templateManagement__c where RQO_fld_account__c = : idAccount AND RQO_fld_contact__c = :idContact AND id not in (select RQO_fld_templateManagement__c from RQO_obj_templatePosition__c)];
            for (RQO_obj_templateManagement__c temp : listTemplateVacio){
                templateManagementBean = new RQO_cls_TemplateManagementBean ();
                templateManagementBean.nombre = temp.Name;
                templateManagementBean.identificador = temp.id;
                templateManagementBean.posicionPlantillaList = new List<RQO_cls_TemplateManagementPositionBean> ();
                templateManagementMap.put(temp.Name, templateManagementBean);
                
                gradosCountByTemplateManagementMap.put(temp.Name, new Set<String> ());
                destinosCountByTemplateManagementMap.put(temp.Name, new Set<String> ());
            }
            
            
            
            // Obtención de plantillas por contacto dado
            for (RQO_obj_templatePosition__c item : [SELECT Id, RQO_fld_cantidad__c, RQO_fld_container__c, RQO_fld_destinoFacturacion__c, 
                                                     RQO_fld_destinoMercancias__c, RQO_fld_grade__c, RQO_fld_grade__r.RQO_fld_sku__r.Id, RQO_fld_grade__r.RQO_fld_sku__r.RQO_fld_descripcionDelGrado__c, RQO_fld_incoterm__c, 
                                                     RQO_fld_nFechasEntrega__c, RQO_fld_shippingMode__c, RQO_fld_templateManagement__c, RQO_fld_templateManagement__r.Name
                                                     FROM RQO_obj_templatePosition__c
                                                     WHERE RQO_fld_templateManagement__r.RQO_fld_contact__c = :idContact AND
                                                     RQO_fld_templateManagement__r.RQO_fld_account__c = :idAccount
                                                     ORDER BY RQO_fld_templateManagement__r.Name]) {
                                                         
                                                         // Si no existe el registro de plantilla se genera nuevo
                                                         if (!templateSet.contains(item.RQO_fld_templateManagement__r.Name)) {
                                                             templateManagementBean = new RQO_cls_TemplateManagementBean ();
                                                             templateManagementBean.nombre = item.RQO_fld_templateManagement__r.Name;
                                                             templateManagementBean.identificador = item.RQO_fld_templateManagement__c;
                                                             templateManagementBean.posicionPlantillaList = new List<RQO_cls_TemplateManagementPositionBean> ();
                                                             
                                                             gradosCountSet = new Set<String> ();
                                                             destinosCountSet = new Set<String> ();
                                                             
                                                             templateSet.add(item.RQO_fld_templateManagement__r.Name);
                                                             system.debug('TemplateSet: '+templateSet);
                                                         }
                                                         System.debug('*****************Item: ' + item);
                                                         // Generación del registro de posiciones de plantilla
                                                         templateManagementPositionBean = new RQO_cls_TemplateManagementPositionBean ();
                                                         templateManagementPositionBean.identificador = item.Id;
                                                         templateManagementPositionBean.grado = item.RQO_fld_grade__c;
                                                         templateManagementPositionBean.gradoQP0 = item.RQO_fld_grade__r.RQO_fld_sku__r.Id;
                                                         templateManagementPositionBean.gradoName = item.RQO_fld_grade__r.RQO_fld_sku__r.RQO_fld_descripcionDelGrado__c;
                                                         templateManagementPositionBean.envase = item.RQO_fld_container__c;
                                                         templateManagementPositionBean.cantidad = (item.RQO_fld_cantidad__c == null ? 0 : item.RQO_fld_cantidad__c.intValue());
                                                         templateManagementPositionBean.unidadMedida = item.RQO_fld_shippingMode__c;
                                                         templateManagementPositionBean.destino = item.RQO_fld_destinoMercancias__c;
                                                         templateManagementPositionBean.incoterm = item.RQO_fld_incoterm__c;
                                                         templateManagementPositionBean.facturacion = item.RQO_fld_destinoFacturacion__c;
                                                         templateManagementPositionBean.fechasEntrega = item.RQO_fld_nFechasEntrega__c.intValue();
                                                         
                                                         templateManagementBean.posicionPlantillaList.add(templateManagementPositionBean);
                                                         
                                                         templateManagementMap.put(item.RQO_fld_templateManagement__r.Name, templateManagementBean);
                                                         gradosQP0Set.add(item.RQO_fld_grade__r.RQO_fld_sku__r.Id);
                                                         
                                                         // Conteo de grados asociados
                                                         gradosCountSet.add(item.RQO_fld_grade__c);
                                                         gradosCountByTemplateManagementMap.put(item.RQO_fld_templateManagement__r.Name, gradosCountSet);
                                                         
                                                         // Conteo de destinos asociados
                                                         destinosCountSet.add(item.RQO_fld_destinoMercancias__c);
                                                         destinosCountByTemplateManagementMap.put(item.RQO_fld_templateManagement__r.Name, destinosCountSet);
                                                         
                                                     }
            
            // Búsqueda y ordenación de envases a partir del grado asociado a la posición de plantilla
            containerByQP0GradeMap = getContainerByQP0Grade(gradosQP0Set);
            
            // Mapeo de los envases para los grados en el objeto templateManagementPositionBean
            for (RQO_cls_TemplateManagementBean item : templateManagementMap.values()) {
                for (RQO_cls_TemplateManagementPositionBean itemAux : item.posicionPlantillaList) {
                    itemAux.envasesList = containerByQP0GradeMap.get(itemAux.gradoQP0);
                }
                
                // Información de conteo en el objeto padre
                item.gradosCount = gradosCountByTemplateManagementMap.get(item.nombre).size();
                item.destinosCount = destinosCountByTemplateManagementMap.get(item.nombre).size();
            }
            
        } catch (Exception e) {
            System.debug('Excepción generica: '+e.getTypeName()+' - '+e.getMessage()+' - '+e.getLineNumber());
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return JSON.serialize(templateManagementMap.values());
        
    }
    
    /**
* @creation date: 20/12/2017
* @author: Alfonso Constán - Unisys
* @description: Método para la obtención traducciones de envases y unidad de medida
* @param: 
* @return: String		Mapa serializada traduccciones
* @exception: 
* @throws: 
*/	
    @AuraEnabled
    public static String getEnvaseUnidadMedida(String language){
        Map<String, List<RQO_cls_seleccionador>>  mapConbinatoria = new Map<String, List<RQO_cls_seleccionador>>();
        
        RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
        //	Obtenemos un mapa con las unidades de medida y envases. Clave el código y valor su traducción
        Map<String, String>  mapTradEnvaseUidadMedida = util.traduccionesEnvasesUnidadMedida(language);
        
        List<RQO_obj_container__c>  listEnvases = [select id, RQO_fld_codigo__c, (select id, RQO_fld_shippingCode__c, RQO_fld_shippingMode__c from Shipping_Junction__r) from RQO_obj_container__c ];
        
        for (RQO_obj_container__c env : listEnvases){
            
            List<RQO_cls_seleccionador> listUnidad = new List<RQO_cls_seleccionador>();
            //	Recorremos sus unidades de medida
            for (RQO_obj_shippingJunction__c unidaMedi : env.Shipping_Junction__r){
                
                //	Obtenemos la trducción si existiera. En caso de no existir introducimos el código
                String tradUnidad = mapTradEnvaseUidadMedida.containsKey(unidaMedi.RQO_fld_shippingCode__c) ? mapTradEnvaseUidadMedida.get(unidaMedi.RQO_fld_shippingCode__c) : unidaMedi.RQO_fld_shippingCode__c;
                //	Creamos un seleccionador, donde introducimos el id y la traduccion
                RQO_cls_seleccionador tradUnidadSelect = new RQO_cls_seleccionador(unidaMedi.RQO_fld_shippingMode__c, tradUnidad);
                listUnidad.add(tradUnidadSelect);
            }
            
            //	Obtenemos la trducción si existiera. En caso de no existir introducimos el código
            String tradEnvase = mapTradEnvaseUidadMedida.containsKey(env.RQO_fld_codigo__c) ? mapTradEnvaseUidadMedida.get(env.RQO_fld_codigo__c) : env.RQO_fld_codigo__c;
            //	Creamos un seleccionador, donde introducimos el id y la traduccion
            RQO_cls_seleccionador tradEnvaseSelec = new RQO_cls_seleccionador(env.id, tradEnvase);
            
            if (!listUnidad.isEmpty()){                
                mapConbinatoria.put(JSON.serialize(tradEnvaseSelec), listUnidad);
            }            
        }
        return JSON.serialize(mapConbinatoria);        
    }
    
    /**
* @creation date: 03/01/2017
* @author: Alfonso Constán López - Unisys
* @description: añadimos una posición de plantilla
* @param: idTemplate				Id de la plantilla
* @param: positionTemplateBean		Contenido de la posición de plantilla
* @return: 
* @exception: 
* @throws: 
*/
    @AuraEnabled
    public static void addPositionTemplate (String idTemplate, String positionTemplateBean) {
        
        RQO_cls_TemplateManagementPositionBean templatePosition = (RQO_cls_TemplateManagementPositionBean)JSON.deserialize(positionTemplateBean, RQO_cls_TemplateManagementPositionBean.class);       
        //	Creamos la posición de la plantilla
        RQO_obj_templatePosition__c tempPosition = new RQO_obj_templatePosition__c(RQO_fld_templateManagement__c = idTemplate,
                                                                                   RQO_fld_cantidad__c = templatePosition.cantidad, 
                                                                                   RQO_fld_grade__c = templatePosition.grado,
                                                                                   RQO_fld_container__c = templatePosition.envase,
                                                                                   RQO_fld_shippingMode__c = templatePosition.unidadMedida,
                                                                                   RQO_fld_incoterm__c = templatePosition.incoterm,
                                                                                   RQO_fld_destinoMercancias__c = templatePosition.destino,
                                                                                   RQO_fld_destinoFacturacion__c = templatePosition.facturacion);
        insert tempPosition;
    }
    
    /**
* @creation date: 26/12/2017
* @author: David Iglesias - Unisys
* @description: Método para la actualizacion de posiciones asociadas a plantillas
* @param: idTemplate				Identificador de la plantilla
* @param: positionTemplateBeanList		Lista de posiciones de plantilla a actualizar
* @return: String		Lista serializada de plantillas asociadas al usuario
* @exception: 
* @throws: 
*/
    @AuraEnabled
    public static void updatePositionTemplate (String jsonPositionTemplata) {
        
        List<RQO_cls_TemplateManagementPositionBean> positionTemplateBeanList = (List<RQO_cls_TemplateManagementPositionBean>)JSON.deserialize(jsonPositionTemplata, List<RQO_cls_TemplateManagementPositionBean>.class);
        
/**
* Constantes
*/
        final String METHOD = 'updatePositionTemplate';
        
        /**
* Variables
*/
        RQO_obj_templatePosition__c templateManagementPosition = null;
        List<RQO_obj_templatePosition__c> templateManagementPositionList = new List<RQO_obj_templatePosition__c> ();
        
        /**
* Inicio
*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        for (RQO_cls_TemplateManagementPositionBean item : positionTemplateBeanList) {
            templateManagementPosition = new RQO_obj_templatePosition__c();
            templateManagementPosition.Id = item.identificador;
            templateManagementPosition.RQO_fld_cantidad__c = item.cantidad;
            templateManagementPosition.RQO_fld_container__c = item.envase;
            templateManagementPosition.RQO_fld_destinoFacturacion__c = item.facturacion;
            templateManagementPosition.RQO_fld_destinoMercancias__c = item.destino;
            templateManagementPosition.RQO_fld_grade__c = item.grado;
            templateManagementPosition.RQO_fld_incoterm__c = item.incoterm;
            templateManagementPosition.RQO_fld_nFechasEntrega__c = item.fechasEntrega;
            templateManagementPosition.RQO_fld_shippingMode__c = item.unidadMedida;
            
            templateManagementPositionList.add(templateManagementPosition);
        }
        
        if (!templateManagementPositionList.isEmpty()) {
            update templateManagementPositionList;
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
* @creation date: 03/01/2017
* @author: Alfonso Constán López - Unisys
* @description: añadimos una posición de plantilla
* @param: idTemplate				Id de la plantilla
* @param: positionTemplateBean		Contenido de la posición de plantilla
* @return: 
* @exception: 
* @throws: 
*/
    @AuraEnabled
    public static Boolean addPositionTemplateList (String idTemplate, String listWrapperPedido){
        List<RQO_cls_WrapperPedido> listWrapper = (List<RQO_cls_WrapperPedido>)JSON.deserialize(listWrapperPedido, List<RQO_cls_WrapperPedido>.class);
        //	Creamos la lista de plantillas que serán insertadas
        List<RQO_obj_templatePosition__c> listTemplate = new List<RQO_obj_templatePosition__c>();
        
        for (RQO_cls_WrapperPedido temp : listWrapper){
            
            //	Creamos la posición de la plantilla
            RQO_obj_templatePosition__c tempPosition = new RQO_obj_templatePosition__c(RQO_fld_templateManagement__c = idTemplate,
                                                                                       RQO_fld_cantidad__c = temp.getCantidadUnidad(), 
                                                                                       RQO_fld_grade__c = temp.getIdGrado(),
                                                                                       RQO_fld_container__c = temp.getContainer(),
                                                                                       RQO_fld_shippingMode__c = temp.getModoEnvio(),
                                                                                       RQO_fld_incoterm__c = temp.getIncoterm(),
                                                                                       RQO_fld_destinoMercancias__c = temp.getDireccionEnvio(),
                                                                                       RQO_fld_destinoFacturacion__c = temp.getDireccionFacturacion());
            listTemplate.add(tempPosition);
        }
        
        system.debug('listTemplate ' + listTemplate);
        insert listTemplate;
        return true;
    }
    
    /**
* @creation date: 03/01/2017
* @author: Alfonso Constán López - Unisys
* @description: Inserción de posiciones de solicitud a partir de las posiciones de plantilla
* @param: listPosition				Listado de posiciones de plantilla
* @param: AccountId					Id del cliente
* @param: AccountId					Id del contacto
* @return: 	Número de posiciones de solicitud insertadas
* @exception: 
* @throws: 
*/
    @AuraEnabled
    public static integer addRequest(String listPosition, Id AccountId, Id ContactId){
        
        List<RQO_cls_TemplateManagementPositionBean> listTemplatePosition = (List<RQO_cls_TemplateManagementPositionBean>)JSON.deserialize(listPosition, List<RQO_cls_TemplateManagementPositionBean>.class);
        //	Obtenemos el id de la solicitud abierta
        Id idRequest = RQO_cls_GestionPedido_cc.getOpenRequest();
        
        List<Asset> listAsset = new List<Asset>() ;
        //	Recorremos las posiciones de plantilla para crear las posiciones de solicitud
        for(RQO_cls_TemplateManagementPositionBean position : listTemplatePosition) {
            //	Creamos una posición por cada número de fechas de entrega indicado
            for( integer i=0; i< position.fechasEntrega; i++) {                
                Asset asset = new Asset(Name = system.today().format(),
                                        AccountId = AccountId,
                                        ContactId = ContactId,
                                        RQO_fld_requestManagement__c = idRequest,
                                        RQO_fld_grade__c = position.grado,
                                        RQO_fld_qp0Grade__c = position.gradoQP0,
                                        RQO_fld_cantidadUnidad__c = position.cantidad,
                                        RQO_fld_container__c = position.envase,
                                        RQO_fld_shippingMode__c = position.unidadMedida,   
                                        RQO_fld_incoterm__c = position.incoterm,
                                        RQO_fld_destinoFacturacion__c = position.facturacion,
                                        RQO_fld_direccionEnvio__c =  position.destino
                                       );
                listAsset.add(Asset) ;
            } 
        } 
        insert listAsset;        
        return listAsset.size();
    }
    
    
    
    // ***** METODO PRIVADOS ***** //
    
    /**
* @creation date: 21/12/2017
* @author: David Iglesias - Unisys
* @description: Método para la obtención envases por grados QP0
* @param: gradosQP0Set			Grados QP0 para realizar la búsqueda
* @return: Map<Id, List<Id>> 	Mapa con el binomio Grado QP0 - Envase		
* @exception: 
* @throws: 
*/
    private static Map<Id, List<Id>> getContainerByQP0Grade (Set<Id> gradosQP0Set) {
        
        /**
* Constantes
*/
        final String METHOD = 'getContainerByGrade';
        
        /**
* Variables
*/
        Map<Id, List<Id>> resultMap = new Map<Id, List<Id>> ();
        List<Id> containerList = null;
        
        /**
* Inicio
*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        
        for (RQO_obj_containerJunction__c item : [SELECT RQO_fld_gradoQp0__c, RQO_fld_container__c 
                                                  FROM RQO_obj_containerJunction__c 
                                                  WHERE RQO_fld_gradoQp0__c IN :gradosQP0Set]) {
                                                      
                                                      if (resultMap.get(item.RQO_fld_gradoQp0__c) == null) {
                                                          containerList = new List<Id> ();
                                                      } else {
                                                          containerList = resultMap.get(item.RQO_fld_gradoQp0__c);
                                                      }
                                                      containerList.add(item.RQO_fld_container__c);
                                                      resultMap.put(item.RQO_fld_gradoQp0__c, containerList);
                                                  }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return resultMap;
        
    }
    
    
}