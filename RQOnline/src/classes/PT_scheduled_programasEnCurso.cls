/*------------------------------------------------------------
Author:        	Rubén Simarro
Company:       	Indra
Description:   	
Description:   	
Clase encargada de comprobar si en la fecha actual comienza el periodo de vigencia de algun programa de patrocinio
y de ser así, cambia automáticamente el estado del patrocinio a "Programa en curso"

Test Class:    	PT_test_scheduled_prograEnCurso
History
<Date>      	<Author>     	<Change Description>
10-10-2015	    Rubén Simarro 	Initial Version
22-04-2016      Rubén Simarro   Se optimiza la consulta añadiendo al where la condicion
------------------------------------------------------------*/
global class PT_scheduled_programasEnCurso implements Schedulable {

    global void execute(SchedulableContext SC) {
                                      
         try{              
            system.debug('@@'+DateTime.now().format()+' INICIO proceso automático comprobación inicio vigencias Patrocinios');   
   
             Patrocinios__c[] patrocinios = [select Id, name, nombre__c, estado__c, fecha_de_inicio__c 
                                             From Patrocinios__c 
                                             where estado__c =: 'Contrato firmado' and fecha_de_inicio__c=TODAY];
                        
             for(Patrocinios__c patAux: patrocinios){
                 patAux.estado__c='Programa en curso';
             }
             update(patrocinios);
                     	
            system.debug('@@----------------------------------------');
            system.debug('@@ FIN proceso automático comprobación inicio vigencias Patrocinios @@@@@');          
        }
        catch(Exception e){    
            system.debug('@@ ERROR PT_scheduled_programasEnCurso'+e.getMessage());
        }    
    }
   
}