/**
 *	@name: RQO_cls_DetalleProducto4_cc
 *	@version: 1.0
 *	@creation date: 21/09/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Controlador Apex para el componente que se encarga de cargar los tipos de documentos que tiene un grado
 *	@testClass: RQO_cls_DetalleProducto4_test
*/
public with sharing class RQO_cls_DetalleProducto4_cc {
	
    /**
	* @creation date: 21/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Obtiene los documentos susceptibles de descarga agrupados por tipo de documento en un mapa 
	* @param: idGrado tipo String, id del grado.
	* @return: Map<String, List<RQO_cls_LCDocDataAux>>, mapa con el tipo de documento y una lista de los diferentes idiomas e ids de documento almacenados en un objeto 
	* RQO_cls_LCDocDataAux
	* @exception: 
	* @throws: 
	*/    
    @AuraEnabled
    public static Map<String, List<RQO_cls_LCDocDataAux>> doInitDataProd4Apex(String idGrade, String idioma){
        Map<String, List<RQO_cls_LCDocDataAux>> returnMap = null;
        List<RQO_cls_LCDocDataAux> listData = null;
        RQO_cls_LCDocDataAux objAux = null;        
        RQO_cls_GeneralUtilities utilities= null;
        Map<String, string> mapLocale = null;
        String description = '';
        //Verificamos si el usuario esta logado
        Boolean loggedUser = (!RQO_cls_Constantes.USER_GUEST.equalsIgnoreCase(UserInfo.getUserType())) ? true : false;
        Boolean directDownload=true;
        RQO_cs_DocumentUtilities__c objCSDocum = null;
        for (RQO_obj_document__c objDoc : [select RQO_fld_documentId__c, RQO_fld_grade__c, RQO_fld_locale__c, RQO_fld_type__c from RQO_obj_document__c where RQO_fld_grade__c = : idGrade]){
			listData = null;
			objAux = null;
            //Recuperamos del custom settings las caracteristicas parametrizadas para cada documentos
            objCSDocum = RQO_cs_DocumentUtilities__c.getInstance(objDoc.RQO_fld_type__c);
			//Si es usuario loggado muestro todos los tipos de documento, si no está loggado solo muestro los que tenga visibilidad pública 
			//en el custom settings
            if(loggedUser || (objCSDocum != null && objCSDocum.RQO_fld_isPublicVisibility__c)){
				if (utilities == null){utilities = new RQO_cls_GeneralUtilities();}
                if (mapLocale == null){mapLocale = utilities.getDocumentLocale();}
				//Comprobamos si tiene descarga directa
            	directDownload=(objCSDocum != null && !objCSDocum.RQO_fld_directDownload__c) ? false : true;
                description = (mapLocale != null && !mapLocale.isEmpty() && mapLocale.containsKey(objDoc.RQO_fld_locale__c)) ? mapLocale.get(objDoc.RQO_fld_locale__c) : objDoc.RQO_fld_locale__c;
                //Si el mapa ya contiene la key actual recuperamos la lista de objetos para esa key. 
                //Si no existe instaciamos una nueva lista y se la asignamos al mapa
                if (returnMap != null && !returnMap.isEmpty() && returnMap.containsKey(objDoc.RQO_fld_type__c)){
                    listData = returnMap.get(objDoc.RQO_fld_type__c);
                }else{
                    if(returnMap == null){returnMap = new Map<String, List<RQO_cls_LCDocDataAux>>();}
                    listData = new List<RQO_cls_LCDocDataAux>();
                    returnMap.put(objDoc.RQO_fld_type__c, listData);
                }
                //Instanciamos el objeto que va en la lista, completamos los datos y lo añadimos a la lista actual que puede ser nueva o tener datos
				objAux = new RQO_cls_LCDocDataAux();
                completeObjectAux(objAux, objDoc.RQO_fld_locale__c, description, objDoc.RQO_fld_documentId__c, directDownload, objDoc.RQO_fld_type__c, objDoc.Id, idioma);
                listData.add(objAux);
                listData.sort();
                System.debug(listData);
			}
        }
        System.debug('ReturnMap: ' + returnMap);
        return returnMap;
    }
	
    /**
	* @creation date: 20/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	completa los datos de cada objeto RQO_cls_LCDocDataAux 
	* @param: objAux tipo RQO_cls_LCDocDataAux, el objeto que se va a completar.
	* @param: locale tipo String, idioma.
    * @param: description tipo String.
    * @param: docId tipo String.
    * @param: directDownload tipo String.
    * @param: docDescriptionType tipo String, tipo de documento (Ficha de seguridad, nota técnica...)
	* @return:
	* @exception: 
	* @throws: 
	*/  
    private static void completeObjectAux(RQO_cls_LCDocDataAux objAux, String locale, String description, String docId, Boolean directDownload, 
                                          String docDescriptionType, Id sfDocId, String idioma){
        objAux.descriptionCode = locale;
        objAux.description = description;
        objAux.idDoc = docId;
        objAux.docRecoveryType = (directDownload) ? RQO_cls_Constantes.DOCUMENTUM_RECOVERY_TYPE_DIRECT : RQO_cls_Constantes.DOCUMENTUM_RECOVERY_TYPE_VISOR;
        //Nombre de repositorio definido en el custom settings
        objAux.docRepository = RQO_cls_Constantes.DOC_CS_REPOSITORY; 
        objAux.docDescriptionType = docDescriptionType;
		objAux.sfDocId = sfDocId;
		objAux.idiomaUsuario = idioma;
    }
    
     /**
	* @creation date: 25/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Obtiene la traducción de todos los tipos de documentos para presentar por pantalla
	* @param: 
	* @return: Map<String, String>, mapa con el código de tipo de documento y su traducción
	* @exception: 
	* @throws: 
	*/    
    @AuraEnabled
    public static Map<String, String> getDocTypeTittleTranslate(){
        RQO_cls_GeneralUtilities utilities = new RQO_cls_GeneralUtilities();
        return utilities.getDocumentType();
    }
}