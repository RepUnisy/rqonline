/**
 * @name: RRSS_cls_EmailEscaladoNegocio_cc
 * @version: 1.0
 * @creation date: 26/04/2017
 * @author: Ramon Diaz, Accenture
 * @description: Controlador para componente de VF custom que extrae los Social Post relacionados de un caso
 */

public class RRSS_cls_EmailEscaladoNegocio_cc {
    public Id caseId {get;set;}
    
    /* @creation date: 26/04/2017
	 * @author: Ramon Diaz, Accenture
	 * @description: Método que retorna una lista de Social Posts relacionados con el caso
	 * @param: No
	 * @return: Lista de Social posts
	 * @exception: No aplica
	 * @throws: No aplica */
    public List<SocialPost> getSocialPosts()
    {
        List<SocialPost> socialposts;
        socialposts = [SELECT Handle, ParentId, Name, Posted,Content FROM SocialPost WHERE ParentId =: caseId ORDER BY Posted];
        return socialposts;
    }
}