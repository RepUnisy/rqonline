/**
 * @name: RRSS_cls_validarNIFCIF
 * @version: 1.1
 * @creation date: 14/06/2017
 * @author: Ramon Diaz, Accenture
 * @description: Clase para validar un NIF o CIF en un Account
*/
public class RRSS_cls_validarNIFCIF 
{
    private static final String letras = 'TRWAGMYFPDXBNJZSQVHLCKE';
    private static final String letrasNIF = 'ABCDEFGHJNPQRSUVW';
    private static final String cCtrlNIF = 'JABCDEFGHI';
    public Boolean nifcifValido;
    
    /* @creation date: 14/06/2017
	 * @author: Ramon Diaz, Accenture
	 * @description: Función que comprueba si una cadena introducida como parámetro corresponde a un DNI/NIE válido.
	 * @param: nie, cadena a validar.
	 * @return: True en caso de que la cadena corresponda a un DNI/NIE válido. False en caso contrario.
	 * @exception: resultado = false
	 * @throws: No aplica */
    public static boolean validaNIF (String nie)  
    {
        System.debug('::::::::: Clase RRSS_cls_validarNIFCIF / Método validaNIF / Parámetro nie =' + String.valueOf(nie));
        boolean resultado = false;
        String primera, letra;
        Integer numero;
        
        if (nie.length() == 9)
        {
            try
            {
                nie = nie.toUpperCase();
                    
                primera = nie.substring(0, 1);
                
                if (primera == 'X') nie = nie.replaceFirst('X','0');
                else if (primera == 'Y') nie = nie.replaceFirst('Y','1');
                else if (primera == 'Z') nie = nie.replaceFirst('Z','2');
                
                Pattern MyPattern = Pattern.compile('[0-9]{8}['+letras+']');
                Matcher MyMatcher = MyPattern.matcher(nie);
                
                if (MyMatcher.matches())
                {
                    numero = Integer.valueOf(nie.substring (0,nie.length()-1));
                    letra = letras.substring(Math.mod(numero,23), Math.mod(numero,23)+1);
                      
                    if (letra.equals(nie.substring(8,9))) resultado =  true;
                }
            }
            catch (Exception e) 
            {
                resultado = false;
            }
        }
                
        system.debug('P: ' + primera + ' N: ' + numero + ' L: ' + letra);
        system.debug(resultado);
        return resultado;
    }
    
    
    /* PROCECIMIENTO DE VALIDACIÓN DEL NIF
    1- Se suman las posiciones pares de los 7 dígitos centrales, es decir, no se tiene en cuenta la letra inicial ni el código de control. (Suma = A)
    2- Por cada uno de los dígitos de las posiciones impares, se multiplica el dígito por 2 y se suman las cifras del resultado 
  (p.e. si el dígito es 6, el resultado sería 6 x 2 = 12 -> 1 + 2 = 3). Se repite la operación con todos los dígitos en posiciones impares y 
  se suman los resultados. (Suma = B)
    3- Sumar el resultado de los 2 pasos anteriores. (A + B = C)
    4- El último dígito de la suma anterior (C) se lo restamos a 10, cuyo resultado sería el código de control (p.e. si C = 14, el último dígito 
  es 4, por lo que tendríamos 10 - 4 = 6). Si el último dígito de la suma del paso anterior es 0 (p.e. C = 30), no se realiza resta y 
  se toma el 0 como código de control.

  Si el código de control es un número, este sería el resultado de la última operación. Si se trata de una letra, se utilizaría la siguiente 
  relación:
        número obtenido ->   1   2   3   4   5   6   7   8   9   0
      código de control ->   A   B   C   D   E   F   G   H   I   J
     */

    
    /* @creation date: 14/06/2017
	 * @author: Ramon Diaz, Accenture
	 * @description: Función que comprueba si una cadena introducida como parámetro corresponde a un NIF/CIF válido.
	 * @param: nif, cadena a validar.
	 * @return: True en caso de que la cadena corresponda a un NIF/CIF válido. False en caso contrario.
	 * @exception: resultado = false
	 * @throws: No aplica */
    public static boolean validaCIF (String nif)
    {
        System.debug('::::::::: Clase RRSS_cls_validarNIFCIF / Método validaCIF / Parámetro nif =' + String.valueOf(nif));
        boolean resultado = false;
        String primera, ultima, numero, letra;
        Integer A = 0, B = 0, C = 0, D = 0;
        
        if (nif != null)
        {
            if (nif.length() == 9)
            {
                try
                {
                    nif = nif.toUpperCase();
                    
                    Pattern MyPattern = Pattern.compile('['+letrasNIF+'][0-9]{7}[0-9,A-J]');
                    Matcher MyMatcher = MyPattern.matcher(nif);
                    
                    if (MyMatcher.matches())
                    {
                        primera = nif.substring(0,1);
                        numero = nif.substring(1,8);
                        ultima = nif.substring(8,9);
                        
                        system.debug('P: ' + primera + ' N: ' + numero + ' U: ' + ultima);
                        
                        //A es la suma de los números en las posiciones pares
                        A = Integer.valueOf(numero.substring(1,2)) + Integer.valueOf(numero.substring(3,4)) + Integer.valueOf(numero.substring(5,6));
                        
                        //B es la suma de los dígitos de los números multiplicados por dos de las posiciones impares
                        Integer B1 = Integer.valueOf(numero.substring(0,1))*2;
                        if (String.valueOf(B1).length() == 2)
                            B += Integer.valueOf(String.valueOf(B1).substring(0,1)) + Integer.valueOf(String.valueOf(B1).substring(1,2));
                        else
                            B += Integer.valueOf(String.valueOf(B1).substring(0,1));
        
                        Integer B2 = Integer.valueOf(numero.substring(2,3))*2;
                        if (String.valueOf(B2).length() == 2)
                            B += Integer.valueOf(String.valueOf(B2).substring(0,1)) + Integer.valueOf(String.valueOf(B2).substring(1,2));
                        else
                            B += Integer.valueOf(String.valueOf(B2).substring(0,1));
                        
                        Integer B3 = Integer.valueOf(numero.substring(4,5))*2;
                        if (String.valueOf(B3).length() == 2)
                            B += Integer.valueOf(String.valueOf(B3).substring(0,1)) + Integer.valueOf(String.valueOf(B3).substring(1,2));
                        else
                            B += Integer.valueOf(String.valueOf(B3).substring(0,1));
                        
                        Integer B4 = Integer.valueOf(numero.substring(6,7))*2;
                        if (String.valueOf(B4).length() == 2)
                            B += Integer.valueOf(String.valueOf(B4).substring(0,1)) + Integer.valueOf(String.valueOf(B4).substring(1,2));
                        else
                            B += Integer.valueOf(String.valueOf(B4).substring(0,1));
                        
                        //C es la suma de A y B
                        C = A + B;
                        if (String.valueOf(C).length() == 2)
                        {
                            D = 10 - Integer.valueOf(String.valueOf(C).substring(1,2));
                            if (D == 10) D = 0;
                        }  
                        else
                        {
                            D = 10 - Integer.valueOf(String.valueOf(C).substring(0,1));
                            if (D == 10) D = 0;
                        }  
                        
                        //system.debug('B1=' + B1 + ' B2=' + B2 + ' B3=' + B3 + ' B4=' + B4);
                        system.debug('A=' + A + ' B=' + B + ' C=' + C + ' D=' + D);
                        
                        //Comprobamos si D coincide con el dígito/letra de control
                        if (ultima.isNumeric())
                        {
                            if (Integer.valueOf(ultima) == D)  resultado = true;   
                        }
                        else
                        {
                            letra = cCtrlNIF.substring(D,D+1);
                            
                            system.debug(letra);
                            
                            if (letra.equals(nif.substring(8,9))) resultado =  true;
                        }
                    }
                }
                catch (Exception e)
                {
                    resultado = false;
                }
            }
        }
        system.debug('Resultado CIF' + resultado);
        return resultado;
    }
}