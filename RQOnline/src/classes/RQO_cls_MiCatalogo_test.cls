/**
*	@name: RQO_cls_MiCatalogo_test
*	@version: 1.0
*	@creation date: 9/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar los servicios de asociados a micatálogo
*/

@IsTest
public class RQO_cls_MiCatalogo_test {
    
    private static String grado;
    private static String contacto;
    
    /**
    *	@creation date: 9/2/2018
    *	@author: Juan Elías - Unisys
    *   @description: Método inicial de carga de datos que se comparte entre todos los testMethods
    *   @exception: 
    *   @throws: 
    */
    @testSetup
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
            
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
            RecordType rtActual = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ACCOUNT_QUIMICA LIMIT 1];
            List<Account> listaCuentas = RQO_cls_TestDataFactory.createAccount(1);
            listaCuentas[0].Name = 'cuentaAgreement';
            listaCuentas[0].RecordtypeId = rtActual.Id;
            listaCuentas[0].RQO_fld_idExterno__c = 'acExtId';
            listaCuentas[0].RQO_fld_calleNumero__c = 'StreetNumber';
            listaCuentas[0].RQO_fld_poblacion__c = 'City';
            listaCuentas[0].RQO_fld_pais__c = 'ES';
            listaCuentas[0].Phone = '914443322';
            update listaCuentas[0];
            
            List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
            contact[0].FirstName = 'contactAgreement';
            contact[0].LastName = 'contactLastName';
            update contact[0];
            
            RecordType rtActualsec = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ASSET_REQUEST_POSITION];
            List<Asset> asset = RQO_cls_TestDataFactory.createAsset(200);
            asset[0].RecordTypeId = rtActualsec.Id;
            asset[0].Name = 'assetSendOrder';
            update asset[0];
            
            List<RQO_obj_myCatalogue__c> myCatalogue = RQO_cls_TestDataFactory.createMyCatalogue(200);
            List<RQO_obj_specification__c> spefi = RQO_cls_TestDataFactory.createSpecification(200);
        }
    }
    
    /**
    *	@creation date: 9/2/2018
    *	@author: Juan Elías - Unisys
    *   @description: Método test para probar la obtención de "Mi catalogo" tanto la parte almacenada en Salesforce como los acuerdos recuperados de SAP.
    *   @exception: 
    *   @throws: 
    */
    @IsTest
    static void testGetMiCatalogo(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            
            test.startTest();
            //	Obtenemos un catálogo
            List<RQO_obj_myCatalogue__c> listCatalogo = [select id, RQO_fld_grade__c, RQO_fld_contact__c from RQO_obj_myCatalogue__c limit 1];
            List<RQO_obj_specification__c> listSpecification = [select id, RQO_fld_grade__c from RQO_obj_specification__c limit 1];
            Id idGradeSpecifi = listSpecification.size() > 0 ? listSpecification[0].RQO_fld_grade__c : null;
            for (RQO_obj_myCatalogue__c mc : listCatalogo){
                mc.RQO_fld_grade__c = idGradeSpecifi;
            }
            update listCatalogo;
            
            RQO_cls_MiCatalogo_cc.getMiCatalogo(listCatalogo[0].RQO_fld_contact__c, 'ES_es');    
            
            test.stopTest();
        }
    }
        
    /**
    *	@creation date: 9/2/2018
    *	@author: Juan Elías - Unisys
    *   @description: Método test para comprobar volver a proponer del catálogo
    *   @exception: 
    *   @throws: 
    */
    @IsTest
    static void testVolverProponer(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<RQO_obj_myCatalogue__c> listCatalogo = [select id, RQO_fld_grade__c, RQO_fld_contact__c, RQO_fld_contact__r.AccountId from RQO_obj_myCatalogue__c limit 1];   
            Id account = listCatalogo.size() > 0 ? listCatalogo[0].RQO_fld_contact__r.AccountId : null;
            Id contact =  listCatalogo.size() > 0 ? listCatalogo[0].RQO_fld_contact__c : null;
            
            test.startTest();
            
            //Petición con retorno ok del servicio
            Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_AGREEMENT, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
            
            List<Account> listaCuentas = [SELECT Id FROM Account LIMIT 1];
            List<Contact> contactData = [SELECT Id FROM Contact LIMIT 1];
            RQO_cls_MiCatalogo_cc.volverProponer(account, contact, 'test');
            List<RQO_obj_myCatalogue__c> listMiCatalogo = [Select Id from RQO_obj_myCatalogue__c where RQO_fld_grade__r.Name = 'gradeAgreement2'];
            System.assertNotEquals(listMiCatalogo, null);
            
            test.stopTest();
        }
        
    }
    
    /**
    *	@creation date: 9/2/2018
    *	@author: Juan Elías - Unisys
    *   @description: Método test para probar la obtención de "Mi catalogo" tanto la parte almacenada en Salesforce como los acuerdos recuperados de SAP
    *   @exception: 
    *   @throws: 
    */
    @IsTest
    static void testIsMiCatalogoGrado(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            List<RQO_obj_myCatalogue__c> listCatalogo = [select id, RQO_fld_grade__c, RQO_fld_contact__c from RQO_obj_myCatalogue__c limit 1];
            boolean resultado = RQO_cls_MiCatalogo_cc.isMiCatalogoGrado(listCatalogo[0].RQO_fld_grade__c, listCatalogo[0].RQO_fld_contact__c);
            boolean esperado = true;
            System.assertEquals(esperado, resultado);
            
            //	Eliminamos el catálogo para comprobar que el grado no pertenece al catálogo
           	List<RQO_obj_myCatalogue__c> listCatalogoEliminar = [select id from RQO_obj_myCatalogue__c where RQO_fld_grade__c = :listCatalogo[0].RQO_fld_grade__c AND RQO_fld_contact__c = :listCatalogo[0].RQO_fld_contact__c];
         	delete listCatalogoEliminar;
            
            resultado = RQO_cls_MiCatalogo_cc.isMiCatalogoGrado(listCatalogo[0].RQO_fld_grade__c, listCatalogo[0].RQO_fld_contact__c);
            esperado = false;
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
    /**
*	@creation date: 9/2/2018
*	@author: Juan Elías - Unisys
*   @description:	Método test para probar la inserción de "Mi catalogo"
*   @exception: 
*   @throws: 
*/
    @IsTest
    static void testInsertMiCatalogoGrado(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();            
            List<RQO_obj_myCatalogue__c> listCatalogo = [select id, RQO_fld_grade__c, RQO_fld_contact__c from RQO_obj_myCatalogue__c limit 1];
            boolean resultado = RQO_cls_MiCatalogo_cc.insertMiCatalogoGrado(listCatalogo[0].RQO_fld_grade__c, listCatalogo[0].RQO_fld_contact__c);
            System.assertEquals(true, resultado);
            test.stopTest();
        }
    }
    
    /**
*	@creation date: 9/2/2018
*	@author: Juan Elías - Unisys
*   @description:	Método test para probar el borrado de "Mi catalogo"
*   @exception: 
*   @throws: 
*/
    @IsTest
    static void testDeleteMiCatalogo(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<RQO_obj_myCatalogue__c> listCatalogo = [select id, RQO_fld_grade__c, RQO_fld_contact__c from RQO_obj_myCatalogue__c limit 1];
            test.startTest();
            boolean resultado = RQO_cls_MiCatalogo_cc.deleteMiCatalogo(listCatalogo);
            System.assertEquals(true, resultado);
            test.stopTest();
        }
    }
    
    /**
        *	@creation date: 9/2/2018
        *	@author: Alfonso Constán López - Unisys
        *   @description:	Método para comprobar la eliminación de un grado de mi catálogo
        *   @exception: 
        *   @throws: 
        */
    @IsTest
    static void testDeleteMiCatalogoGrado(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
                        
            List<RQO_obj_myCatalogue__c> listCatalogo = [select id, RQO_fld_grade__c, RQO_fld_contact__c from RQO_obj_myCatalogue__c limit 1];
            Id grado = listCatalogo.size()>0 ? listCatalogo[0].RQO_fld_grade__c : null;
            Id contacto = listCatalogo.size()>0 ? listCatalogo[0].RQO_fld_contact__c : null;
            
            test.startTest();
            boolean resultado = RQO_cls_MiCatalogo_cc.deleteMiCatalogoGrado(grado, contacto);
            boolean esperado = true;
            System.assertEquals(true, resultado);
            test.stopTest();
        }
    }
    
}