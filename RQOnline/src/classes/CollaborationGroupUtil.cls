global class CollaborationGroupUtil {
    

    /*global static void addMember(String groupId, String userId){
        List<CollaborationGroupMember> findMembers = [Select id from CollaborationGroupMember 
        where CollaborationGroupId = :groupId and MemberId = :userId];
        if(findMembers.size() ==0){
         CollaborationGroupMember cand =
         new CollaborationGroupMember(CollaborationGroupId=groupId,MemberId = userId);
         insert cand;
        }                 
    }*/
    /*
    global static void addMember(List<UserToCollaboration> asignaciones){
        List<CollaborationGroupMember> findMembersAsign = new List<CollaborationGroupMember>();
        List<CollaborationGroupMember> collaborationGroupMemberMap = [Select id,CollaborationGroupId,MemberId from CollaborationGroupMember];
        for (UserToCollaboration asignacion : asignaciones){
            boolean existe = false;
            for (CollaborationGroupMember collaborationGroupMember : collaborationGroupMemberMap){
                if ((collaborationGroupMember.CollaborationGroupId == asignacion.getGroupId()) && (collaborationGroupMember.MemberId == asignacion.getUserId())){         
                    existe = true;                   
                }
            }       
            if(existe==false){
                CollaborationGroupMember cand =
                new CollaborationGroupMember(CollaborationGroupId=asignacion.getGroupId(),MemberId = asignacion.getUserId());
                findMembersAsign.add(cand);                                                 
            }
        }  
        try
        {                  
            Database.SaveResult[] srList = Database.insert(findMembersAsign,false);
            mostrarLog(srList);
            
        }catch(Exception ex)
        {            
            System.debug('Error: ' + ex.getMessage());
        }         
    }*/ 



    global static void addMember(List<UserToCollaboration> asignaciones){
        List<CollaborationGroupMember> findMembersAsign = new List<CollaborationGroupMember>();        
        for (UserToCollaboration asignacion : asignaciones){
            CollaborationGroupMember cand =
            new CollaborationGroupMember(CollaborationGroupId=asignacion.getGroupId(),MemberId = asignacion.getUserId());
            findMembersAsign.add(cand);                                                 
        }  
        try
        {                  
            Database.SaveResult[] srList = Database.insert(findMembersAsign,false);
            mostrarLog(srList);
            
        }catch(Exception ex)
        {            
            System.debug('Error: ' + ex.getMessage());
        }         
    } 
    
    private static void mostrarLog( Database.SaveResult[] srList)
    {
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {           
                        
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug(LoggingLevel.ERROR ,'Successfully inserted permision. Permision ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                                  
                    System.debug(LoggingLevel.ERROR ,  err.getStatusCode() + ': ' + err.getMessage());                  
                    System.debug(LoggingLevel.ERROR , 'Permisions fields that affected this error: ' + err.getFields());
                }
            }
        }    
    }   
}