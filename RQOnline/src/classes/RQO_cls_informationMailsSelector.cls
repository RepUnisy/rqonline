/**
*   @name: RQO_cls_informationMailsSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la obtención de un template de email
*/
public with sharing class RQO_cls_informationMailsSelector extends RQO_cls_ApplicationSelector {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_informationMailsSelector';
    
    /**
    *   @name: RQO_cls_informationMailsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna la lista de campos del custom metadata type RQO_cmt_informationMails__mdt
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<Schema.SObjectField> getSObjectFieldList()
    {
        return new List<Schema.SObjectField> {
            RQO_cmt_informationMails__mdt.RQO_fld_customerProfile__c,
            RQO_cmt_informationMails__mdt.RQO_fld_informationProduct__c,
            RQO_cmt_informationMails__mdt.RQO_fld_informationTopic__c,
            RQO_cmt_informationMails__mdt.RQO_fld_email__c
            };
    }

    /**
    *   @name: RQO_cls_informationMailsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el sObjectType de RQO_cmt_informationMails__mdt
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_cmt_informationMails__mdt.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_informationMailsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual se deb ordenar un conjunto de RQO_cmt_informationMails__mdt
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public override String getOrderBy()
    {
        return 'RQO_fld_customerProfile__c';
    }

    /**
    *   @name: RQO_cls_informationMailsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que selecciona un correo y devuelve una lista de RQO_cmt_informationMails__mdt
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<RQO_cmt_informationMails__mdt> selectMail(Set<String> profiles, Set<String> products, Set<String> topics)
    {
        // ***** CONSTANTES ***** //
        final String METHOD = 'selectMail';
        
        string condition = '';
        
        if (profiles != null && profiles.size() > 0)
            condition = condition + 'RQO_fld_customerProfile__c in :profiles ';
        if (products != null && products.size() > 0)
        {
            if (String.isNotEmpty(condition))
                condition = condition + 'AND ';
            condition = condition + 'RQO_fld_informationProduct__c in :products ';
        }
        if (topics != null && topics.size() > 0)
        {
            if (String.isNotEmpty(condition))
                condition = condition + 'AND ';
            condition = condition + 'RQO_fld_informationTopic__c in :topics ';
        }

        string query = newQueryFactory().setCondition(condition).toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        List<RQO_cmt_informationMails__mdt> mails = (List<RQO_cmt_informationMails__mdt>) Database.query(query); 
        return mails;                                           
    }
}