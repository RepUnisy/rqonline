/**
*	@name: RQO_cls_EnqueueGetDocumentRP2
*	@version: 1.0
*	@creation date: 15/11/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Clase encolable que ejecuta la llamada al servicio de recuperación de datos RP2 y carga los registros de documentos asociados
*	a grados mediante un batch
*	@testclass: RQO_cls_EnqueueGetDocumentRP2_test
*/
public with sharing class RQO_cls_EnqueueGetDocumentRP2 implements Queueable, Database.AllowsCallouts{
    
    /**
    * @creation date: 15/11/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Método execute de la clase encolable.
    * @param: Param1 - context tipo QueueableContext
    * @return: 
    * @exception: 
    * @throws: 
    */
    public void execute(QueueableContext context) {
        System.debug('Ejecutando el job');
        this.manageScheduleLogic();
    }
	
    /**
	* @creation date: 15/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Se encarga de realizar la lógica de la clase. Llama al handler de documentum para recuperar los datos de RP2 y ejecuta el batch para realizar
	* la actualización
	* @return:
	* @exception:
	* @throws:
	*/	
    private void manageScheduleLogic(){
        System.debug('Vamos a gestionar');
        //La fecha de inicio se calcula en el método getStartDate, la fecha de fin es la fecha actual
        Date fechaInicio  = this.getStartDate();
        Date fechaFin = Date.today();
        //Llamamos al handler de Documentum que se encargará de obtener los datos de RP2
        RQO_cls_Documentum serDoc = new RQO_cls_Documentum();
        serDoc.getDocumentumRP2(RQO_cls_Constantes.DOCUMENTUM_RP2_QUIMICA_CODE, fechaInicio, fechaFin);
        System.debug('*********************fechaInicio: ' + fechaInicio + 'fechaFin: ' + fechaFin);
        System.debug('Acabamos de consultar');
        //Con los datos recuperados (si los tenemos) lanzamos la actualización de documentos
        List<string> gradeList = serDoc.getGradeList();
        if(gradeList != null && !gradeList.isEmpty()){
            Id batchId = Database.executeBatch(new RQO_cls_DocumentumRP2_batch(RQO_cls_Constantes.DOC_TYPE_FICHA_SEGURIDAD, gradeList,serDoc.getMapDataService()), 100);
        }
    }
	
    /**
	* @creation date: 15/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Recupera la fecha de inicio que se manda al servicio de RP2
	* la actualización
	* @return:
	* @exception:
	* @throws:
	*/
    private Date getStartDate(){
        Date fechaInicio = null;
       	//Intento recuperar la última ejecución correcta de esta carga en el registro de logs para mi tipo de ejecución. 
       	//Si no encuentro ejecución correcta estoy lanzando por primera vez (o reintentando esa primera ejecución por que ha dado error), es decir, 
       	//tengo que lanzar la carga inicial de documentos (solo Empresa "RQA").
       	//Si encuentro registro de log correcto se toma la fecha de creación del registro más un día como fecha inicial para el servicio, es decir, tomamos como 
       	//partida la última ejecución correcta del proceso. Como norma debería recuperar siempre el día anterior.
        for (RQO_obj_logProcesos__c logProces : [Select CreatedDate, RQO_fld_processType__c, RQO_fld_descripciondeProceso__c from RQO_obj_logProcesos__c  
                                                  where RQO_fld_processType__c = : RQO_cls_Constantes.LOG_TYPE_BATCH 
                                                  and RQO_fld_descripciondeProceso__c = : RQO_cls_Constantes.BATCH_DOCUMENTOS_RP2 and RQO_fld_parentLog__c = '' 
                                                  and RQO_fld_errorCode__c = : RQO_cls_Constantes.BATCH_LOG_RESULT_OK order by createdDate desc LIMIT 1]){
			fechaInicio = logProces.CreatedDate.Date().addDays(1);
		}
        return fechaInicio;
    }
}