/**
 *	@name: RQO_cls_PosicionPedidoBean
 *	@version: 1.0
 *	@creation date: 16/10/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de Posiciones de Pedido.
 */
public class RQO_cls_PosicionPedidoBean {

    public String externalId {get; set;}
    public String pedido {get; set;}
    public String posicionPedido {get; set;}
    public String centro {get; set;}
    public String material {get; set;}
    public String cantidad {get; set;}
    public String unidadMedida {get; set;}
    public String fechaReparto {get; set;}
    public String textoInhibidor {get; set;}
    public String estado {get; set;}
    public String idPosicionSolicitud {get; set;}
    public String descripcionGrado {get; set;}
    public String fechaConfirmacion {get; set;}
    public String isDelete {get; set;}
    //public String identificadorSolicitud {get; set;}
    public List <RQO_cls_EntregaBean> entregasList {get; set;}
    
}