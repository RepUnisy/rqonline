/*------------------------------------------------------------
Author:        	Rubén Simarro
Company:       	Indra
Description:   	Clase que implementa la funcionalidad del boton enviar email

Test Class:    	PT_test_button_enviarMail
History
<Date>      	<Author>     	<Change Description>
25-11-2015	    Rubén Simarro 	Initial Version
------------------------------------------------------------*/
global class PT_button_enviarMail {
        
    public PT_button_enviarMail(){         
    }
         
    webservice static void enviarEmailDirecciones(String asunto, String mensaje, List<String> direcciones){
    
        sendEmail(asunto,mensaje, direcciones);
     }  
    
   webservice static void enviarEmail(String asunto, String mensaje){
    
       sendEmail(asunto,mensaje, new List<String>{Label.etiqueta_boton_enviar_email_direccion}); 
   }  
    
  
    private static void sendEmail(String asunto, String mensaje, List<String> direcciones){
                                        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();            
        mail.setUseSignature(false);
        mail.setToAddresses(direcciones);
        mail.setSubject(asunto);     		        		           
        mail.setHtmlBody(mensaje.replace('\n','<br>'));
           
       system.debug('@@ SALESFORCE envío de email @@');
       system.debug('@@@ Direcciones: '+ direcciones);  
       system.debug('@@@ Asunto: '+ asunto);  
       
       try{
       
           Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
                                                           
             if(resultMail[0].isSuccess()) {
            
                 system.debug('@@@ email enviado correctamente @@@');       
             }
       }                   
       catch(System.EmailException emlEx) {                                    
             system.debug('@@@ ERROR PT_button_enviarMail:'+emlEx.getMessage());         
       }          
    }
}