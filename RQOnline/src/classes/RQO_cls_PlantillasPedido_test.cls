/**
*   @name: RQO_cls_PlantillasPedido_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test de la clase RQO_cls_PlantillasPedido
*/
@IsTest
public class RQO_cls_PlantillasPedido_test {
    
    @testSetup static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
        }
    }
    
    /**
    * @creation date: 15/02/2018
    * @author:Alfonso Constán - Unisys
    * @description: Test de la inserción de posiciones de templates
    */
    @isTest static void testaddPositionTemplateList(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u){
            
            List<RQO_obj_templateManagement__c> listTemplatePosition = RQO_cls_TestDataFactory.createTemplateManagement(200);
            String idTemplate = listTemplatePosition.size()>0 ? listTemplatePosition[0].id : null;

            List<RQO_cls_WrapperPedido> listWrapper = new List<RQO_cls_WrapperPedido> ();
            listWrapper.add(new RQO_cls_WrapperPedido());
            String wrapperPedido = JSON.serialize(listWrapper);
            
            test.startTest();
            Boolean resultado = RQO_cls_PlantillasPedido_cc.addPositionTemplateList(idTemplate, wrapperPedido);
           
            System.assertEquals(true, resultado);
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 15/02/2018
    * @author:Alfonso Constán - Unisys
    * @description: Test de la obtención de los envases y unidad de medida
    */
    @isTest static void testGetTemplates(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u){
            
            List<RQO_obj_templatePosition__c> listTemplatePosition = RQO_cls_TestDataFactory.createTemplatePosition(200);
            List<RQO_obj_templateManagement__c> listTemplate = [select id, RQO_fld_account__c, RQO_fld_contact__c 
                                                                from RQO_obj_templateManagement__c
                                                                where id = :listTemplatePosition[0].RQO_fld_templateManagement__c ];
            
            String idAccount = listTemplate.size() > 0 ? listTemplate[0].RQO_fld_account__c : null;
            String idContact = listTemplate.size() > 0 ? listTemplate[0].RQO_fld_contact__c : null;
            
             
            test.startTest();
            
            String resultado = RQO_cls_PlantillasPedido_cc.getTemplates(idAccount, idContact);
            
            List<RQO_cls_TemplateManagementBean> positionTemplateBeanList = (List<RQO_cls_TemplateManagementBean>)JSON.deserialize(resultado,List<RQO_cls_TemplateManagementBean>.class);
            
            System.assertEquals(1, positionTemplateBeanList.size());
            test.stopTest();
                       
        }
    }
    
    
    /**
    * @creation date: 15/02/2018
    * @author:Alfonso Constán - Unisys
    * @description: Test de la obtención de los envases y unidad de medida
    */
    @isTest static void testCreateOrModifyTemplate(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            Boolean esperado = false;
            Boolean resultado;
            
            test.startTest();
            resultado = RQO_cls_PlantillasPedido_cc.createOrModifyTemplate('', 'cuentaAgreement');
            System.assertEquals(esperado, resultado);
            
            resultado = RQO_cls_PlantillasPedido_cc.createOrModifyTemplate('', '');
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 15/02/2018
    * @author:Alfonso Constán - Unisys
    * @description: Test de la obtención de los envases y unidad de medida
    */
    @isTest 
    static void testGetEnvaseUnidadMedida(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            
            List<RQO_obj_shippingJunction__c> listShippingJuntion = RQO_cls_TestDataFactory.createShippingJunction(200);
            String inicio = '';    
            
            test.startTest();
            String resultado = RQO_cls_PlantillasPedido_cc.getEnvaseUnidadMedida('es_ES');
            System.assertNotEquals(inicio, resultado);
            
            test.stopTest();            
        }
    }
    
    /**
    * @creation date: 15/02/2018
    * @author:Alfonso Constán - Unisys
    * @description:Actualización de una posición de pedido
    */
    @isTest 
    static void testAddRequest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String listPosition = '[{"unidadMedida":"null", "fechasEntrega":0}]';
            
            List<Contact> listContact = RQO_cls_TestDataFactory.createContact(200);
            Id idAccount = listContact.size()!= 0 ? listContact[0].AccountId : null;
            Id idContact = listContact.size()!= 0 ? listContact[0].Id : null;            
            
            
            test.startTest();
            integer esperado = 0;
            System.debug('**********ESPERADO*********: ' + esperado);
            integer resultado;
            resultado = RQO_cls_PlantillasPedido_cc.addRequest(listPosition, idAccount, idContact);
            System.assertEquals(esperado, resultado);
            test.stopTest();            
        }
    }
    
    /**
    * @creation date: 15/02/2018
    * @author:Alfonso Constán - Unisys
    * @description:Actualización de una posición de pedido
    */
    @isTest 
    static void testUpdatePositionTemplate(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            
            List<RQO_obj_templatePosition__c> listTemplatePosition = RQO_cls_TestDataFactory.createTemplatePosition(200);
            //	Obtenemos el id de la posición de plantilla
            Id idTemplatePosition = listTemplatePosition.size() != 0 ? listTemplatePosition[0].Id : null;
            
            RQO_cls_TemplateManagementPositionBean item = new RQO_cls_TemplateManagementPositionBean();
            item.identificador = idTemplatePosition;
            item.cantidad = 99;
            item.envase = null;
            item.facturacion = null;
            item.destino = null;
            item.grado = null;
            item.incoterm = 'FCA';
            item.fechasEntrega = 1;
            item.unidadMedida = null;
            
            List<RQO_cls_TemplateManagementPositionBean> positionTemplateBeanList = new List<RQO_cls_TemplateManagementPositionBean> ();
            positionTemplateBeanList.add(item);
            
            test.startTest();
            RQO_cls_PlantillasPedido_cc.updatePositionTemplate(JSON.serialize(positionTemplateBeanList));
            
            List<RQO_obj_templatePosition__c> listTemplatePost = [select id, RQO_fld_cantidad__c from RQO_obj_templatePosition__c where id =:idTemplatePosition];
            Decimal cantidadFinal = listTemplatePost.size() > 0 ? listTemplatePost[0].RQO_fld_cantidad__c : 0;
            System.assertEquals(99, cantidadFinal);
            test.stopTest();   
        }
    }
    
    /**
    * @creation date: 15/02/2018
    * @author:Alfonso Constán - Unisys
    * @description: Eliminación de una template
    */
    @isTest 
    static void testDeleteTemplatePositionManagement(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<RQO_obj_templatePosition__c> listTemplatePosition = RQO_cls_TestDataFactory.createTemplatePosition(200);
            Id idTemplatePosition = listTemplatePosition.size() != 0 ? listTemplatePosition[0].Id : null;
            
            test.startTest();
            RQO_cls_PlantillasPedido_cc.deleteTemplatePositionManagement(idTemplatePosition);
            
            //	Buscamos la template que hemos borrado, para asegurar que no existe
            List<RQO_obj_templatePosition__c> listSinBorrar = [select id from RQO_obj_templatePosition__c where id = : idTemplatePosition];
            System.assertEquals(0, listSinBorrar.size());
            test.stopTest();   
        }
    }
    
    /**
    * @creation date: 15/02/2018
    * @author:Alfonso Constán - Unisys
    * @description: Eliminación de una template
    */
    @isTest 
    static void testDeteleTemplateManagement(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<RQO_obj_templateManagement__c> listTemplateManagement = RQO_cls_TestDataFactory.createTemplateManagement(200);
            Id idTemplateManagement = listTemplateManagement.size()!= 0 ? listTemplateManagement[0].Id : null;
            
            test.startTest();
            RQO_cls_PlantillasPedido_cc.deteleTemplateManagement(idTemplateManagement);
            
            //	Buscamos la template que hemos borrado, para asegurar que no existe
            List<RQO_obj_templateManagement__c> listSinBorrar = [select id from RQO_obj_templateManagement__c where id = : idTemplateManagement];
            System.assertEquals(0, listSinBorrar.size());
            test.stopTest();   
        }
    }    
    
    /**
	* @creation date: 15/02/2018
	* @author:Alfonso Constán - Unisys
	* @description: Método para crear una nueva plantilla
	*/
    @isTest 
    static void testCreateTemplate(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<Contact> listContact = RQO_cls_TestDataFactory.createContact(200);
            Id idAccount;
            Id idContact;
            //	Obtenemos la información de un contacto.
            for(Contact cont : listContact){
                idAccount = cont.AccountId;
                idContact = cont.Id;
            }
            List<RQO_obj_templateManagement__c> listTemplatePre = [select id from RQO_obj_templateManagement__c where RQO_fld_contact__c =:idContact];
            
            test.startTest();
            RQO_cls_PlantillasPedido_cc.createTemplate('name', idAccount, idContact);
            
            List<RQO_obj_templateManagement__c> listTemplatePost = [select id from RQO_obj_templateManagement__c where RQO_fld_contact__c =:idContact];
            
            System.assertEquals(listTemplatePre.size() + 1, listTemplatePost.size());
            test.stopTest();   
            
        }
    }
    
    /**
	* @creation date: 15/02/2018
	* @author:Alfonso Constán - Unisys
	* @description: Método para testear la funcionalidad de inserción de posición a una plantilla
	*/
    @isTest 
    static void testAddPositionTemplate(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            List<RQO_obj_templateManagement__c> listTemplateManagement = RQO_cls_TestDataFactory.createTemplateManagement(200);
            
            String jsonPositionTemplateBean = '{"unidadMedida":null,"incoterm":null,"identificador":"a3q9E0000004qJdQAI","gradoQP0":null,"gradoName":null,"grado":null,"fechasEntrega":1,"facturacion":null,"envasesList":null,"envase":null,"destino":null,"cantidad":0}';
            Id idTemplateManagement = listTemplateManagement.size()!= 0 ? listTemplateManagement[0].Id : null;
            
            List<RQO_obj_templatePosition__c> listTemplatePositionPre = [select id from RQO_obj_templatePosition__c where RQO_fld_templateManagement__c = : idTemplateManagement];
            
            test.startTest();
            
            RQO_cls_PlantillasPedido_cc.addPositionTemplate(idTemplateManagement, jsonPositionTemplateBean);
            List<RQO_obj_templatePosition__c> listTemplatePositionPost = [select id from RQO_obj_templatePosition__c where RQO_fld_templateManagement__c = : idTemplateManagement];
            
            System.assertEquals(listTemplatePositionPre.size() + 1, listTemplatePositionPost.size());
            test.stopTest();    
        }
    }
}