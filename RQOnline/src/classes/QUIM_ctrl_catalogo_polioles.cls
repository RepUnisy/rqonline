global with sharing class QUIM_ctrl_catalogo_polioles{
    
    // the query
    public static String soql {get;set;}
    public static Boolean queryDesc {get; set;}
    public static String nameDesc {get; set;}
    public static String soqlCategory {get;set;}
    public static String soqlProduct_Category {get;set;}
    // the collection of grados to display
    public static List<ccrz__E_Product__c> grados {get; set;}
    public static List<ccrz__E_Category__c> categorias {get; set;}
    public static String categoriasJSON {get;set;}
    //Categorias filtradas
    public static List<ccrz__E_Category__c> categoriasPadre {get; set;}
    public static List<ccrz__E_Category__c> categoriasHijas {get; set;}
    public static List<ccrz__E_Category__c> categoriasAplicaciones {get; set;}   
    public static List<ccrz__E_Category__c> categoriasSegmentos {get; set;}
    public static List<ccrz__E_ProductCategory__c> productosCategorias {get; set;}
    public static String productosCategoriasJSON {get; set;}
    public static List<ccrz__E_ProductCategory__c> productosCategoriasPadreHija {get; set;} 
    public static List<ccrz__E_ProductCategory__c> productosCategoriasHijaGrados {get; set;} 
    public static List<ccrz__E_ProductCategory__c> productosCategoriasSegmentos {get; set;} 
    public static List<ccrz__E_ProductCategory__c> productosCategoriasAplicaciones {get; set;} 
    // the resulti
    public static List<ccrz__E_Product__c> gradosRes {get; set;}
    // paso lista a JSON [para paso por JS]
    public static String gradosResJSON {get;set;}
    public static String apliResJSON {get;set;}
    public static String segResJSON {get;set;}
    
    // Lista de todas las Especificaciones existentes
    //public static List<ccrz__E_Spec__c> todasSpecs {get;set;}
    // listado de las especificiones de los grados. En el mismo orden.
    public static List<Map<String, String>> specsRelatedMap {get; set;}
    // paso lista a JSON [para paso por JS]
    public static String specsRelatedMapJSON {get;set;}
        
    public static String aux {get;set;}
    //public static String consulta {get;set;}
    public static String parametro {get;set;}
    public static String parametroApp {get;set;}
    //public static List<String> industriaCB {get; set;}
    //public static String base64Value {get;Set;}  
    //public static String tipoDocumento {get;Set;}
    //public static String nombreDocumento {get;Set;}
    // Parámetro GET idioma
    public static String parametroIdioma {get;set;}
    // Lengua a utilizar
    public static String language {get; set;}
    
    // format the soql for display on the visualforce page
    public String debugSoql {
        get { return soql; }
        set;
    }
    
    // init the controller
    public QUIM_ctrl_catalogo_polioles() {
        try{
            parametro = ApexPages.currentPage().getParameters().get('product');
            parametroApp = ApexPages.currentPage().getParameters().get('application');
            //soql = 'SELECT Name, ccrz__AlternateName__c, Id FROM ccrz__E_Product__c WHERE ccrz__Storefront__c = \'PortalQuimica\'';
            
            //categorias = [SELECT Id,Name FROM ccrz__E_Category__c] ;
            //Dividir la obtención de las categorias y de las product categorias para que nunca superen las 1000
            categoriasPadre = [SELECT Id,Name FROM ccrz__E_Category__c where ccrz__ParentCategory__r.Name ='AgrupacionProductos']; //Esto me devuelve las 4 agrupaciones
            categoriasHijas = [SELECT Id,Name FROM ccrz__E_Category__c where ccrz__ParentCategory__c IN :categoriasPadre]; //Esto los 13 productos
            categoriasSegmentos =  [SELECT Id,Name FROM ccrz__E_Category__c where ccrz__ParentCategory__r.Name = 'Segmentos']; //con esto saco los segmentos
            categoriasAplicaciones =  [SELECT Id,Name FROM ccrz__E_Category__c where ccrz__ParentCategory__r.Name = 'Aplicaciones']; //con esto saco las aplicaciones
            categorias = new List<ccrz__E_Category__c>();
            categorias.addAll(categoriasPadre);
            categorias.addAll(categoriasHijas);
            categorias.addAll(categoriasSegmentos);
            categorias.addAll(categoriasAplicaciones);
            // Paso a JSON
            categoriasJSON = JSON.serialize(categorias);
            
            //productosCategorias = [SELECT Id,ccrz__Category__c,ccrz__Product__c FROM ccrz__E_ProductCategory__c];
            //Product category por separado
            productosCategoriasPadreHija = [SELECT Id,ccrz__Category__c,ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__c IN :categoriasPadre]; //Relacion agrupaciones-productos
            productosCategoriasHijaGrados = [SELECT Id,ccrz__Category__c,ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__c IN :categoriasHijas]; //Agrupaciones producto-grado
            productosCategoriasSegmentos = [SELECT Id,ccrz__Category__c,ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__c IN :categoriasSegmentos]; //Agrupaciones producto-segmento
            productosCategoriasAplicaciones = [SELECT Id,ccrz__Category__c,ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__c IN :categoriasAplicaciones]; //Agrupaciones producto-segmento
            productosCategorias = new List<ccrz__E_ProductCategory__c>();
            productosCategorias.addAll(productosCategoriasPadreHija);
            productosCategorias.addAll(productosCategoriasHijaGrados);
            productosCategorias.addAll(productosCategoriasSegmentos);
            productosCategorias.addAll(productosCategoriasAplicaciones);
            // Paso a JSON
            productosCategoriasJSON = JSON.serialize(productosCategorias);
            
            //grados = Database.query(soql);
            //productosCategorias = Database.query(soqlProduct_Category);
            //categorias = Database.query(soqlCategory);
            parametroIdioma = ApexPages.currentPage().getParameters().get('cclcl');
            System.debug(System.LoggingLevel.INFO, 'parametroIdioma: ' + parametroIdioma);
            if (parametroIdioma != NULL && parametroIdioma.length()>1) {
                language = parametroIdioma;
            }
            else {
                language = ApexPages.currentPage().getHeaders().get('Accept-Language').left(2); 
            }
            System.debug(System.LoggingLevel.INFO, 'language: ' + language);
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ QUIM_ctrl_catalogo_polioles(): ' + e);
        }
    }
     
    // runs the actual query
    public static void runQuery() {
        try {
            system.debug('db: Inicio runquery polioles');
            gradosRes = new List<ccrz__E_Product__c>();
            List<ccrz__E_ProductItemI18N__c> gradosResI18N = new List<ccrz__E_ProductItemI18N__c>();
            System.debug('queryDesc: ' + queryDesc);
            if (queryDesc) {
                List<ccrz__E_Product__c> gradosAux = Database.query(soql);
                System.debug('gradosAux' + gradosAux);
                // Obtengo los grados en el lenguaje que corresponda
                String langLike = language + '%';
                List<ccrz__E_ProductItemI18N__c> gradosI18NBus = [SELECT Name, ccrz__LongDesc__c, ccrz__Product__c FROM ccrz__E_ProductItemI18N__c WHERE ccrz__Product__c IN :gradosAux AND (ccrz__Locale__c Like :langLike OR ccrz__Locale__c = '')];
                System.debug('gradosI18NBus: ' + gradosI18NBus);
                for(ccrz__E_ProductItemI18N__c gradoAux : gradosI18NBus) {
                    if (gradoAux.ccrz__LongDesc__c == NULL) {
                        if(gradoAux.Name.containsIgnoreCase(nameDesc)) { 
                            gradosResI18N.add(gradoAux);
                            //  gradosRes.add(gradoAux);
                        }
                    }
                    else {
                        if(gradoAux.ccrz__LongDesc__c.containsIgnoreCase(nameDesc) || gradoAux.Name.containsIgnoreCase(nameDesc)) {
                            gradosResI18N.add(gradoAux);
                            // gradosRes.add(gradoAux);
                        }
                    }
                }//foreach
                for(ccrz__E_Product__c grado : gradosAux) {
                    for(ccrz__E_ProductItemI18N__c i18n: gradosResI18N){
                        if(grado.Id == i18N.ccrz__Product__c){
                            gradosRes.add(grado);
                        }
                    } 
                }
                
            }
            else {
                gradosRes = Database.query(soql);
            }
            
            mapearSpecsRelated();
            // paso lista a JSON [para paso por JS]
            gradosResJSON = JSON.serialize(gradosRes);
            List<String> apliRes = new List<String>();
            for(ccrz__E_ProductCategory__c pCA : [SELECT Id,ccrz__Category__r.ccrz__ParentCategory__r.Name,ccrz__Category__r.Name,ccrz__Product__c FROM ccrz__E_ProductCategory__c
                                                  WHERE ccrz__Product__c IN :gradosRes AND ccrz__Category__r.ccrz__ParentCategory__r.Name = 'Aplicaciones'])
            {
                apliRes.add(pCA.ccrz__Category__r.Name);
            }
            system.debug('apliRes= ' + apliRes.size());
            apliResJSON = JSON.serialize(apliRes);
            // Segmentos industriales de los grados Relacionados para alimentar combo Segmento Industrial
            List<String> segRes = new List<String>();
            for(ccrz__E_ProductCategory__c pCS : [SELECT Id,ccrz__Category__r.ccrz__ParentCategory__r.Name,ccrz__Category__r.Name,ccrz__Product__c FROM ccrz__E_ProductCategory__c
                                                  WHERE ccrz__Product__c IN :gradosRes AND ccrz__Category__r.ccrz__ParentCategory__r.Name = 'Segmentos'])
            {
                segRes.add(pCS.ccrz__Category__r.Name);
            }
            system.debug('segRes= ' + segRes.size());
            segResJSON = JSON.serialize(segRes);
            
            system.debug('db: fin runquery polioles');
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ runQuery(): ' + e);
        }
    }
    
    // Mapea las Spec's de los grados relacionados (IDentificador__c[key] y ccrz__SpecValue__c[value])
    public static void mapearSpecsRelated() { 
        try {
            specsRelatedMap = new List<Map<String, String>> ();
          
            List<Id> idsgradosRel = new List<Id> ();
            for(ccrz__E_Product__c gradoRel : gradosRes) {
                idsgradosRel.add(gradoRel.Id);
            }

            String langLike = language + '%';
            System.debug(System.LoggingLevel.INFO, 'langLike: ' + langLike);
            // ProductSpecs de Todos los grados Relacionados
            List<ccrz__E_ProductSpec__c> productSpecsGrados = new List<ccrz__E_ProductSpec__c> ();
            productSpecsGrados = [SELECT Id, ccrz__Product__c, ccrz__Spec__c, ccrz__SpecValue__c,ccrz__Spec__r.IDentificador__c From ccrz__E_ProductSpec__c WHERE ccrz__Product__c IN :idsgradosRel AND (Locale__c Like :langLike OR Locale__c = '')];
            // ProductItem'sI18N de Todos los grados Relacionados
            List<ccrz__E_ProductItemI18N__c> gradosResI18N = new List<ccrz__E_ProductItemI18N__c> ();
            gradosResI18N = [SELECT Name, ccrz__LongDesc__c, ccrz__Product__c FROM ccrz__E_ProductItemI18N__c WHERE ccrz__Product__c IN :idsgradosRel AND (ccrz__Locale__c Like :langLike OR ccrz__Locale__c = '')];
            System.debug(System.LoggingLevel.INFO, 'gradosResI18N: ' + gradosResI18N);
 
            for(ccrz__E_Product__c gradoRel : gradosRes) {
                Map<String, String> specsMap = new Map<String, String>();
                
                for(ccrz__E_ProductSpec__c prdEsp : productSpecsGrados){
                    if (gradoRel.Id == prdEsp.ccrz__Product__c) {
                        specsMap.put(prdEsp.ccrz__Spec__r.IDentificador__c, prdEsp.ccrz__SpecValue__c);
                    }
                }
                // Cambiando el nombre y descripción al idioma correspondiente
                for (ccrz__E_ProductItemI18N__c gradoI18N : gradosResI18N) {
                    if (gradoRel.Id == gradoI18N.ccrz__Product__c) {
                        //System.debug(System.LoggingLevel.INFO, 'ANTES gradoRel.Name: ' + gradoRel.Name);
                        gradoRel.Name = gradoI18N.Name;
                        //System.debug(System.LoggingLevel.INFO, 'DESPUES gradoRel.Name: ' + gradoRel.Name);
                        gradoRel.ccrz__LongDesc__c = gradoI18N.ccrz__LongDesc__c; 
                    }
                }
                // Añadir mapa de especificaciones al listado.
                specsRelatedMap.add(specsMap);
            }
            //System.debug('specsRelatedMap ANTES JSON: ' + specsRelatedMap);
            // paso lista a JSON [para paso por JS]
            specsRelatedMapJSON = JSON.serialize(specsRelatedMap);
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ mapearSpecsRelated(): ' + e);
        }
    }
    
    public static void runQuery2() {
        try {
            categorias = Database.query(soqlCategory);
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, 'FAIL @ runQuery2(): ' + e);
        }
    }
    
    // runs the search with parameters passed via Javascript
    /*@RemoteAction
    global static List<ccrz__E_Category__c> recuperaCategorias(Integer a) {
        try{
            runQuery2();
        }catch(Exception e){
            System.debug(System.LoggingLevel.ERROR, e.getmessage());
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
        } 
        
        return categorias;
    }*/
     
    
    @RemoteAction
    global static List<String> runSearch(String name, String[] industria, String aplicacion, String producto, String idioma) {
        /*for(String x: industriaCB){
            System.debug(x);
        }*/
        //system.debug('longitud de array checkboxes = ' + industriaCB.size());
        aux= 'Entrando en runsearch';
        System.debug('Entrando en @RemoteAction runSearch()');
        System.debug('name: ' + name);
        System.debug('industria: ' + industria);
        System.debug('aplicacion: ' + aplicacion);
        System.debug('producto: ' + producto);
        System.debug('idioma: ' + idioma);
        language = idioma;
        parametroIdioma = idioma;
        System.debug(System.LoggingLevel.INFO, 'runSearch() parametroIdioma: ' + parametroIdioma);
        Boolean firstFilter = true;
        queryDesc = false;
        try{
            soql = 'SELECT Id, Name, ccrz__LongDesc__c, Recomendado__c, Nuevo__c FROM ccrz__E_Product__c WHERE ccrz__Storefront__c = \'PortalQuimica\'';
            firstFilter = false;
            if (!name.equals('')){
                //soql += ' AND ccrz__E_Product__c.Name LIKE \'%'+String.escapeSingleQuotes(name)+'%\'';
                //firstFilter=false;
                nameDesc = name;
                queryDesc = true;
            }
            
            if (!industria[0].equals('Todos')){
                for (String indus : industria) {
                    List<ccrz__E_ProductCategory__c> indAux = [SELECT ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__c IN (SELECT Id FROM ccrz__E_Category__c WHERE Name = :indus)];
                    System.debug('indAux: ' + indAux);
                    if(indAux.size()>0){
                        
                        if(!firstFilter){
                            soql += ' AND ';
                        }
                        
                        for(Integer i=0;i<indAux.size();i++){
                            if (!firstFilter){
                                if(i==0){
                                    soql += ' (ccrz__E_Product__c.Id = ' + '\'' + indAux[i].ccrz__Product__c + '\'';
                                }else{
                                    soql += ' OR ccrz__E_Product__c.Id = ' + '\'' + indAux[i].ccrz__Product__c + '\'';
                                }
                            }else{
                                if(i==0){
                                    soql += ' WHERE (ccrz__E_Product__c.Id = ' + '\'' + indAux[i].ccrz__Product__c + '\'';
                                }else{
                                    soql += ' OR ccrz__E_Product__c.Id = ' + '\'' + indAux[i].ccrz__Product__c + '\'';
                                }
                            }   
                        }         
                        soql += ')';
                        
                        firstFilter = false;
                    }else{
                        if(!firstFilter){
                            soql += 'AND ccrz__E_Product__c.Name =\'\' ';
                        }else{
                            soql += ' WHERE ccrz__E_Product__c.Name =\'\'';
                        }
                    }   
                }
            }
            
            
            if (!aplicacion.equals('Todos')){
                List<ccrz__E_ProductCategory__c> apliAux = [SELECT ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__c IN (SELECT Id FROM ccrz__E_Category__c WHERE Name = :aplicacion)];
                System.debug('apliAux: ' + apliAux);
                if(apliAux.size()>0){
                    
                    if(!firstFilter){
                        soql += ' AND ';
                    }
                   
                    for(Integer i=0;i<apliAux.size();i++){
                        if (!firstFilter){
                            if(i==0){
                                soql += ' (ccrz__E_Product__c.Id = ' + '\'' + apliAux[i].ccrz__Product__c + '\'';
                            }else{
                                soql += ' OR ccrz__E_Product__c.Id = ' + '\'' + apliAux[i].ccrz__Product__c + '\'';
                            }
                        }else{
                            if(i==0){
                                soql += ' WHERE (ccrz__E_Product__c.Id = ' + '\'' + apliAux[i].ccrz__Product__c + '\'';
                            }else{
                                soql += ' OR ccrz__E_Product__c.Id = ' + '\'' + apliAux[i].ccrz__Product__c + '\'';
                            }
                        }   
                    }         
                    soql += ')';
                    
                    
                }else{
                        if(!firstFilter){
                            soql += 'AND ccrz__E_Product__c.Name =\'\' ';
                        }else{
                            soql += ' WHERE ccrz__E_Product__c.Name =\'\'';
                        }
                    }   
                firstFilter = false;
            }
            
            
            if (!producto.equals('Todos')){
                List<ccrz__E_ProductCategory__c> prodAux = [SELECT ccrz__Product__c FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__c IN (SELECT Id FROM ccrz__E_Category__c WHERE Name = :producto)];
                //System.debug('prodAux: ' + prodAux);
                if(prodAux.size()>0){
                    
                    if(!firstFilter){
                        soql += ' AND ';
                    }
                    
                    for(Integer i=0;i<prodAux.size();i++){
                        if (!firstFilter){
                            if(i==0){
                                soql += ' (ccrz__E_Product__c.Id = ' + '\'' + prodAux[i].ccrz__Product__c + '\'';
                            }else{
                                soql += ' OR ccrz__E_Product__c.Id = ' + '\'' + prodAux[i].ccrz__Product__c + '\'';
                            }
                            
                        }else{
                            if(i==0){
                                soql += ' WHERE (ccrz__E_Product__c.Id = ' + '\'' + prodAux[i].ccrz__Product__c + '\'';
                            }else{
                                soql += ' OR ccrz__E_Product__c.Id = ' + '\'' + prodAux[i].ccrz__Product__c + '\'';
                            }
                        }   
                    }         
                    soql += ')';
                    
                    firstFilter = false;
                }else{
                    if(!firstFilter){
                        soql += 'AND ccrz__E_Product__c.Name =\'\' ';
                    }else{
                        soql += ' WHERE ccrz__E_Product__c.Name =\'\'';
                    }
                }
                
            }
            
            System.debug('soql');
            System.debug(soql);
            runQuery();            
        }
        //MDG catch entero
        catch(Exception e){
            System.debug(System.LoggingLevel.ERROR, e.getmessage());
        }   
        //System.debug(gradosRes.size());
        //return gradosRes;
        
        List<String> results = new List<String> ();
        results.add(gradosResJSON);
        results.add(specsRelatedMapJSON);
        results.add(apliResJSON);
        results.add(segResJSON);
        System.debug('results[0]: ' + results[0]);
        System.debug('results[1]: ' + results[1]);
        System.debug('results[2]: ' + results[2]);
        System.debug('results[3]: ' + results[3]);
        
        return results;
    }
    
    //MDG
    @RemoteAction
    global static String[] obtenerIdiomas(String idGrad, String modal) {
         /* *** PRUEBA Clase QUIM_documentum ***
        ////Primero saber el tipo de documento a extraer:
        String tipo;
        if(modal == '#fichaTecModal'){
            tipo = 'Ficha Técnica';
        }else if(modal == '#fichaSegModal'){
             tipo = 'Ficha Seguridad';
        }else{
            tipo = 'Certificado de Cumplimiento';
        }
        //System.debug('tipo: '+tipo);
        List <ccrz__E_ProductMedia__c> prodMediaRelated = new List <ccrz__E_ProductMedia__c>();
        List <String> idProdMediaRelated = new List <String>();
        prodMediaRelated = [SELECT id,Tipo_documento__c,Idioma_del_documento__c,Id_Documento__c FROM ccrz__E_ProductMedia__c WHERE ccrz__Product__c = :idGrad AND Tipo_documento__c= :tipo];
        //System.debug('prodMediaRelated: '+prodMediaRelated);  
        if(modal=='#fichaSegModal' && !prodMediaRelated.isEmpty()){
            idProdMediaRelated.add(prodMediaRelated[0].Id_Documento__c);
            for(integer i=0;i<prodMediaRelated.size();i++){   
                idProdMediaRelated.add(prodMediaRelated[i].Idioma_del_documento__c);
            } 
        }else{
            for(integer i=0;i<prodMediaRelated.size();i++){             
                idProdMediaRelated.add(prodMediaRelated[i].Idioma_del_documento__c);
            }
        }
        
        //System.debug('idProdMediaRelated: '+idProdMediaRelated);
        
        return idProdMediaRelated;*/
        
        QUIM_documentum llamadaDocumentum = new QUIM_documentum();
        
        return llamadaDocumentum.obtenerIdiomas(idGrad, modal);
    }
    
    @RemoteAction
    global static String[] documentum(String idGrad, String documento, String idioma) {
        
        QUIM_documentum llamadaDocumentum = new QUIM_documentum();
        
        return llamadaDocumentum.fetchDocument(idGrad, documento, idioma);
 
    }
    // use apex describe to build the picklist values
    public List<List<String>> industrias {
        get {
            if (industrias == null) {
                try {
                    industrias = new List<List<String>>();
                    //*************** Obteniendo Segmentos Industriales PARENT_CATEGORY **********************
                    ccrz__E_Category__c idSegmentosParent = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'Segmentos' LIMIT 1];
                    //System.debug('PRUEBA idSegmentosParent: ' + idSegmentosParent.Id);
                    List<ccrz__E_Category__c> todasIndustrias = [SELECT Id, Name FROM ccrz__E_Category__c WHERE ccrz__ParentCategory__c = :idSegmentosParent.Id ORDER BY Name];
                    //System.debug('PRUEBA todasIndustrias: ' + todasIndustrias);
                    String langLike = language + '%';
                    System.debug('langLike: ' + langLike);
                    List<ccrz__E_CategoryI18N__c> industriasI18N = new List<ccrz__E_CategoryI18N__c>();
                    industriasI18N = [SELECT Id, ccrz__Category__c, Name, ccrz__Locale__c, ccrz__ShortDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :todasIndustrias AND ccrz__Locale__c LIKE :langLike];
                    System.debug('industriasI18N: ' + industriasI18N);
                    if (industriasI18N.isEmpty()) {
                        industriasI18N = [SELECT Id, ccrz__Category__c, Name, ccrz__Locale__c, ccrz__ShortDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :todasIndustrias AND ccrz__Locale__c LIKE 'en%'];
                    }
                    for (ccrz__E_Category__c indus : todasIndustrias) {
                        for (ccrz__E_CategoryI18N__c indusI18N : industriasI18N) {
                            if (indus.Id == indusI18N.ccrz__Category__c) {
                                List<String> auxNombreI18Name = new List<String>();
                                auxNombreI18Name.add(indus.Name);
                                auxNombreI18Name.add(indusI18N.Name);
                                industrias.add(auxNombreI18Name);
                            }
                        }
                    }
                    System.debug(System.LoggingLevel.INFO, 'industrias: ' + industrias);
                }
                catch (Exception e) {
                    System.debug(System.LoggingLevel.ERROR, 'EXCEPTION industrias: ' + e);
                }
            }
            return industrias;          
        }
        set;
    }
    
    public List<List<String>> aplicaciones {
        get {
            if (aplicaciones == null) {
                try {
                    aplicaciones = new List<List<String>>();
                    //*************** Obteniendo Aplicaciones PARENT_CATEGORY ********************************
                    ccrz__E_Category__c idAplicacionesParent = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'Aplicaciones' LIMIT 1];
                    //System.debug('APLICACIONES idAplicacionesParent: ' + idAplicacionesParent);
                    List<ccrz__E_Category__c> todasAplicaciones = [SELECT Id, Name FROM ccrz__E_Category__c WHERE ccrz__ParentCategory__c = :idAplicacionesParent.Id ORDER BY Name];
                    //System.debug('APLICACIONES todasAplicaciones: ' + todasAplicaciones);
                    String langLike = language + '%';
                    System.debug('langLike: ' + langLike);
                    List<ccrz__E_CategoryI18N__c> aplicacionesI18N = new List<ccrz__E_CategoryI18N__c>();
                    aplicacionesI18N = [SELECT Id, ccrz__Category__c, Name, ccrz__Locale__c, ccrz__ShortDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :todasAplicaciones AND ccrz__Locale__c LIKE :langLike];
                    System.debug('aplicacionesI18N: ' + aplicacionesI18N);
                    if (aplicacionesI18N.isEmpty()) {
                        aplicacionesI18N = [SELECT Id, ccrz__Category__c, Name, ccrz__Locale__c, ccrz__ShortDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :todasAplicaciones AND ccrz__Locale__c LIKE 'en%'];
                    }
                    for (ccrz__E_Category__c app : todasAplicaciones) {
                        for (ccrz__E_CategoryI18N__c appI18N : aplicacionesI18N) {
                            if (app.Id == appI18N.ccrz__Category__c) {
                                List<String> auxNombreI18Name = new List<String>();
                                auxNombreI18Name.add(app.Name);
                                auxNombreI18Name.add(appI18N.Name);
                                aplicaciones.add(auxNombreI18Name);
                            }
                        }
                    }
                    System.debug(System.LoggingLevel.INFO, 'aplicaciones: ' + aplicaciones);
                }
                catch (Exception e) {
                    System.debug(System.LoggingLevel.ERROR, 'EXCEPTION aplicaciones: ' + e);
                }
            }
            return aplicaciones;           
        }
        set;
    }
    
    public List<List<String>> productos {
        get {
            if (productos == null) {
                try {
                    productos = new List<List<String>>();
                    //*************** Obteniendo Productos PARENT_CATEGORY **********************
                    ccrz__E_Category__c idProductosParent = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'AgrupacionProductos' LIMIT 1];
                    List<ccrz__E_Category__c> idsPadresMiddle = [SELECT Id FROM ccrz__E_Category__c WHERE ccrz__ParentCategory__c = :idProductosParent.Id];
                    //System.debug('PRODUCTOS idsPadresMiddle: ' + idsPadresMiddle);
                    List<ccrz__E_Category__c> todosProductos = [SELECT Id, Name FROM ccrz__E_Category__c WHERE ccrz__ParentCategory__c IN :idsPadresMiddle ORDER BY Name];
                    //System.debug('PRODUCTOS todosProductos: ' + todosProductos);
                    String langLike = language + '%';
                    System.debug('langLike: ' + langLike);
                    List<ccrz__E_CategoryI18N__c> productosI18N = new List<ccrz__E_CategoryI18N__c>();
                    productosI18N = [SELECT Id, ccrz__Category__c, Name, ccrz__Locale__c, ccrz__ShortDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :todosProductos AND ccrz__Locale__c LIKE :langLike];
                    System.debug('productosI18N: ' + productosI18N);
                    if (productosI18N.isEmpty()) {
                        productosI18N = [SELECT Id, ccrz__Category__c, Name, ccrz__Locale__c, ccrz__ShortDesc__c FROM ccrz__E_CategoryI18N__c WHERE ccrz__Category__c IN :todosProductos AND ccrz__Locale__c LIKE 'en%'];
                    }
                    for (ccrz__E_Category__c product : todosProductos) {
                        for (ccrz__E_CategoryI18N__c productI18N : productosI18N) {
                            if (product.Id == productI18N.ccrz__Category__c) {
                                List<String> auxNombreI18Name = new List<String>();
                                auxNombreI18Name.add(product.Name);
                                auxNombreI18Name.add(productI18N.Name);
                                productos.add(auxNombreI18Name);
                            }
                        }
                    }
                    System.debug(System.LoggingLevel.INFO, 'productos: ' + productos);
                }
                catch(Exception e) {
                    System.debug(System.LoggingLevel.ERROR, 'EXCEPTION productos: ' + e);
                }
            }
            return productos;           
        }
        set;
    }
    
} // class