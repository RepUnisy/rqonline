/**
 *	@name: RQO_cls_RegisterEvent_cc
 *	@version: 1.0
 *	@creation date: 16/11/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Controlador Apex para el registro de eventos en la tabla RQO_obj_event
 *	@testClass: RQO_cls_RegisterEvent_test
*/
public with sharing class RQO_cls_RegisterEvent_cc {
	
    private static final String CLASS_NAME = 'RQO_cls_RegisterEvent_cc';
    
	/**
	* @creation date: 21/09/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método invocable desde el componente lightning para generar los eventos
	* @param: idGrado tipo String, id del grado.
	* @return: String
	* @exception: 
	* @throws: 
	*/ 
    @AuraEnabled
    public static void createEvent(String componentName, String componentDescription, String actionType, String actionDescription, String gradeId, String documentId, 
                                    String pedidoId, String comunicacionId, String faqId, String numFactura, String idCategoria, String searchData,
                                  	String idioma, String requestId, String browser, String device){
		final String METHOD = 'createEvent';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
                                        
        if(!RQO_cls_Constantes.USER_GUEST.equalsIgnoreCase(UserInfo.getUserType())){
			RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
			util.generateEvent(componentName, componentDescription, actionType, actionDescription, gradeId, documentId, pedidoId, comunicacionId, faqId, 
                               numFactura, idCategoria, searchData, idioma, requestId, browser, device);
        }
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
}