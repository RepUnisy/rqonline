/**
*   @name: RQO_cls_CommunicationsSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de que gestiona la selección u obtención de comunicaciones
*/
public with sharing class RQO_cls_CommunicationsSelector extends RQO_cls_ApplicationSelector {
    
    // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_CommunicationsSelector';
    
    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la lista de campos de RQO_obj_communication__c
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<Schema.SObjectField> getSObjectFieldList()
    {
        /**
        * Constantes
        */
        final String METHOD = 'getSObjectFieldList';
        
        return new List<Schema.SObjectField> {
            RQO_obj_communication__c.Name,
            RQO_obj_communication__c.RQO_fld_shortDescription__c,
            RQO_obj_communication__c.RQO_fld_customerProfile__c,
            RQO_obj_communication__c.RQO_fld_userProfile__c,
            RQO_obj_communication__c.RQO_fld_startDate__c,
            RQO_obj_communication__c.RQO_fld_endDate__c,
            RQO_obj_communication__c.RQO_fld_status__c,
            RQO_obj_communication__c.RQO_fld_alreadySent__c,
            RQO_obj_communication__c.RQO_fld_template__c,
            RQO_obj_communication__c.RQO_fld_accountsNumber__c,
            RQO_obj_communication__c.RecordTypeId };
    }

    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la lista de campos de RQO_obj_communication__c
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public Schema.SObjectType getSObjectType()
    {
        /**
        * Constantes
        */
        final String METHOD = 'getSObjectType';
        
        return RQO_obj_communication__c.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar los CommunicationSelector
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public override String getOrderBy()
    {
        return 'RQO_fld_startDate__c, RQO_fld_accountsNumber__c DESC';
    }

    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar los CommunicationSelector
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<RQO_obj_communication__c> selectById(Set<ID> idSet)
    {
        /**
        * Constantes
        */
        final String METHOD = 'selectById';
        
         return (List<RQO_obj_communication__c>) selectSObjectsById(idSet);
    }
    
    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna el resultado de realizar una consulta que localiza las comunicaciones de correos
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public Database.QueryLocator queryLocatorEmailCommunications()
    {
        /**
        * Constantes
        */
        final String METHOD = 'queryLocatorEmailCommunications';
        
        // Comunicaciones seleccionadas para correo, publicadas en el pasado y que no se hayan envíado.
        string query = newQueryFactory().setCondition('RQO_fld_emailSending__c = true AND RQO_fld_alreadySent__c = false AND ' + 
                                                      'RQO_fld_startDate__c <= TODAY AND RQO_fld_endDate__c >= TODAY').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        return Database.getQueryLocator(query);
    }
    
    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de objetos de comunicaión en base a un id con un template relacionado
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<RQO_obj_communication__c> selectByIdWithTemplate(Set<ID> idSet)
    {
        /**
        * Constantes
        */
        final String METHOD = 'selectByIdWithTemplate';
        
        fflib_QueryFactory commFactory = newQueryFactory();   
        new RQO_cls_CommunicationTemplatesSelector().configureQueryFactoryFields(commFactory,     
                RQO_obj_communication__c.RQO_fld_template__c.getDescribe().getRelationshipName());
        
        string query = commFactory.setCondition('RQO_fld_template__r.RQO_fld_active__c = true AND Id in :idSet').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);

        return (List<RQO_obj_communication__c>) Database.query(query);
    }
    
    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de comunicaciones basadas en multimedia activos
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<RQO_obj_communication__c> selectActiveMultimedia(Set<Id> idUserSet,
                                                                 List<String> customerProfiles, 
                                                                 List<String> userProfiles) 
    {  
        return this.selectActiveMultimedia(idUserSet, customerProfiles, userProfiles, null, null);
    }
    
    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de comunicaciones multimedia activas
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<RQO_obj_communication__c> selectActiveMultimedia(Set<Id> idUserSet,
                                                                 List<String> customerProfiles, 
                                                                 List<String> userProfiles,
                                                                 Date startDate,
                                                                 Date endDate) 
    {   
        /**
        * Constantes
        */
        final String METHOD = 'selectActiveMultimedia';
        
        fflib_QueryFactory communicationQueryFactory = newQueryFactory();
        
        // Recuperar clientes de los usuarios
        List<User> users = new RQO_cls_UserSelector().selectById(idUserSet);
        Set<Id> accountIds = new Set<Id>();
        for (User user : users) {
           accountIds.add(user.AccountId);
        }
        
        // Añadimos una subselect mediante la factoría de Communication Account
        fflib_QueryFactory communicationAccountQueryFactory =
            new RQO_cls_CommunicationAccountSelector().
                addQueryFactorySubselect(communicationQueryFactory);
                
        communicationAccountQueryFactory.setCondition('RQO_fld_account__c IN : accountIds').toSOQL();
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': startDate ' + startDate);
        
        String startDateCondition = '';
        if (startDate != null)
            startDateCondition = 'AND RQO_fld_startDate__c >= :startDate ';
            
        System.debug(CLASS_NAME + ' - ' + METHOD + ': endDate ' + endDate);
        
        String endDateCondition = '';
        if (endDate != null)
            endDateCondition = 'AND RQO_fld_startDate__c <= :endDate ';
            
        String customerProfilesCondition = '';
        if (customerProfiles != null)
            customerProfilesCondition = 'AND (RQO_fld_customerProfile__c INCLUDES(\''+ String.join( customerProfiles, '\',\'') + '\') OR RQO_fld_customerProfile__c INCLUDES(\'' + RQO_cls_Constantes.PICKLIST_TODOS_VALUE + '\')) ';
        
        String userProfilesCondition = '';
        if (userProfiles != null)
            userProfilesCondition = 'AND (RQO_fld_userProfile__c INCLUDES(\''+ String.join( userProfiles, '\',\'') + '\') OR RQO_fld_userProfile__c INCLUDES(\'' + RQO_cls_Constantes.PICKLIST_TODOS_VALUE + '\')) ';

        String condition = 
               'RQO_fld_startDate__c <= TODAY AND RQO_fld_endDate__c >= TODAY ' +
               startDateCondition +
               endDateCondition +
               customerProfilesCondition + 
               userProfilesCondition + 
               'AND RQO_fld_bannerType__c = NULL';
               
        string query = communicationQueryFactory.setCondition(condition).toSOQL();
            
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        List<RQO_obj_communication__c> comms = (List<RQO_obj_communication__c>) Database.query(query);
        
        List<RQO_obj_communication__c> commsSelected = new List<RQO_obj_communication__c>();
        
        for (RQO_obj_communication__c comm : comms)
        {
            if (comm.RQO_fld_accountsNumber__c == 0 || comm.Communication_Contacts__r.size() > 0)
                commsSelected.add(comm);
        }
        
        return commsSelected;
    }
    
    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de ContentVersiones basadas en adjuntos activos
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<ContentVersion> selectActiveWithAttachments(Set<Id> idUserSet,
                                                            List<String> customerProfiles, 
                                                            List<String> userProfiles,
                                                            string bannerType,
                                                            String language)
    {   
        /**
        * Constantes
        */
        final String METHOD = 'selectActiveWithAttachments';
        
        fflib_QueryFactory communicationQueryFactory = newQueryFactory();
        
        // Recuperar clientes de los usuarios
        List<User> users = new RQO_cls_UserSelector().selectById(idUserSet);
        Set<Id> accountIds = new Set<Id>();
        for (User user : users) {
           accountIds.add(user.AccountId);
        }
        
        // Añadimos una subselect mediante la factoría de Communication Account
        fflib_QueryFactory communicationAccountQueryFactory =
            new RQO_cls_CommunicationAccountSelector().
                addQueryFactorySubselect(communicationQueryFactory);
                
        communicationAccountQueryFactory.setCondition('RQO_fld_account__c IN : accountIds').toSOQL();

        String customerProfilesCondition = '';
        if (customerProfiles != null)
            customerProfilesCondition = 'AND (RQO_fld_customerProfile__c INCLUDES(\''+ String.join( customerProfiles, '\',\'') + '\') OR RQO_fld_customerProfile__c INCLUDES(\'' + RQO_cls_Constantes.PICKLIST_TODOS_VALUE + '\')) ';
        
        String userProfilesCondition = '';
        if (userProfiles != null)
            userProfilesCondition = 'AND (RQO_fld_userProfile__c INCLUDES(\''+ String.join( userProfiles, '\',\'') + '\') OR RQO_fld_userProfile__c INCLUDES(\'' + RQO_cls_Constantes.PICKLIST_TODOS_VALUE + '\')) ';
        
        String languageCondition = '';
        if (String.isNotEmpty(language))
            languageCondition = 'AND (RQO_fld_language__c = :language OR RQO_fld_language__c = NULL) ';
        
        String condition = 
               'RQO_fld_startDate__c <= TODAY AND RQO_fld_endDate__c >= TODAY ' +
               customerProfilesCondition + 
               userProfilesCondition + 
               languageCondition + 
               'AND RQO_fld_bannerType__c = \'' + bannerType + '\' ';
               
        string query = communicationQueryFactory.setCondition(condition).toSOQL();
            
        System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
        
        List<RQO_obj_communication__c> banners = (List<RQO_obj_communication__c>) 
                                                    Database.query(query); 
                                                    
        System.debug(CLASS_NAME + ' - ' + METHOD + ': banners.size() ' + banners.size());            
                                                        
        List<ContentVersion> contents = new List<ContentVersion>();
        
        RQO_obj_communication__c bannerSelected = null;
        if (banners.size() > 0) {
            
            for (RQO_obj_communication__c banner : banners)
            {
                System.debug(CLASS_NAME + ' - ' + METHOD + ': banner.RQO_fld_accountsNumber__c ' + banner.RQO_fld_accountsNumber__c);
                System.debug(CLASS_NAME + ' - ' + METHOD + ': banner.Communication_Contacts__r ' + banner.Communication_Contacts__r.size());    

                if (banner.RQO_fld_accountsNumber__c == 0 || 
                    (banner.RQO_fld_accountsNumber__c > 0 && banner.Communication_Contacts__r.size() > 0))
                {
                    bannerSelected = banner;
                    break;
                }
            }
            if (bannerSelected != null)
            {
                System.debug(CLASS_NAME + ' - ' + METHOD + ': recuperar contents de ' + bannerSelected.Id);
                List<ContentDocumentLink> cds = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :bannerSelected.Id];
                System.debug(CLASS_NAME + ' - ' + METHOD + ': ' + cds.size() + ' contenidos de ' + bannerSelected.Id);
                Set<Id> contentIds = new Set<Id>();
                for (ContentDocumentLink cd : cds)
                	contentIds.add(cd.ContentDocumentId);
                System.debug(CLASS_NAME + ' - ' + METHOD + ': contentIds ' + contentIds);
                contents = [SELECT Id FROM ContentVersion WHERE contentdocumentid IN :contentIds AND isLatest=true];
                System.debug(CLASS_NAME + ' - ' + METHOD + ': contents ' + contents);
            }
            
        }
        return contents;
    }
    
    /**
    *   @name: RQO_cls_CommunicationsSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de ContentVersiones basadas en adjuntos
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public List<ContentVersion> selectAttachments(ID commId) 
    {   
        /**
        * Constantes
        */
        final String METHOD = 'selectAttachments';
        
        List<RQO_obj_communication__c> comms = selectById(new Set<Id> { commId }); 
                                                        
        List<ContentVersion> contents = new List<ContentVersion>();
        if (comms.size() > 0) {
            RQO_obj_communication__c comm = comms[0];
            System.debug(CLASS_NAME + ' - ' + METHOD + ': recuperar contents de ' + comm.Id);
            List<ContentDocumentLink> cds = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :comm.Id];
            System.debug(CLASS_NAME + ' - ' + METHOD + ': ' + cds.size() + ' contenidos de ' + comm.Id);
            Set<Id> contentIds = new Set<Id>();
            for (ContentDocumentLink cd : cds)
            	contentIds.add(cd.ContentDocumentId);
            System.debug(CLASS_NAME + ' - ' + METHOD + ': contentIds ' + contentIds);
            contents = [SELECT Id, Title, FileType, FileExtension FROM ContentVersion WHERE contentdocumentid in : contentIds AND isLatest=true];
            System.debug(CLASS_NAME + ' - ' + METHOD + ': contents ' + contents);
        }
        return contents;
    }
}