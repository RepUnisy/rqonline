/**
*   @name: RQO_cls_BatchEmailSending_Test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_BatchEmailSending.
*/
@isTest
public class RQO_cls_BatchEmailSending_Test {
	
	/**
    *   @name: RQO_cls_BatchEmailSending_Test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    */
	@testSetup static void initialSetup() {
		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(u){
			// Crear comunicaciones para hoy
			RQO_cls_TestDataFactory.createCoommunications();
			// Crear email templates RQO_eplt_CorreoComunicaciones RQO_eplt_correoEncuestas
			RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_CorreoComunicaciones', 'RQO_eplt_correoEncuestas'}); 
		}
		// Crear encuenta con una pregunta y una respuesta para hoy
		RQO_cls_TestDataFactory.createSurveys();
	}
	
   	/**
    *   @name: RQO_cls_BatchEmailSending_Test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba el envío de correo de una comunicación creada para el día de hoy.
    */
    private static testMethod void whenSendCommunicationEmailsFinishesOK() {

		User u = RQO_cls_TestDataFactory.userCreation('sfTestUserClienteRunAsJob@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(u){
        	test.startTest();
            //Lanzamiento del proceso
			Id commBatchJobId = Database.executeBatch(new RQO_cls_BatchEmailSending(RQO_obj_communication__c.sObjectType), 2000);
			Id surveyBatchJobId = Database.executeBatch(new RQO_cls_BatchEmailSending(RQO_obj_survey__c.sObjectType), 2000);
            test.stopTest();
            
			// Comprobaciones
			List<AsyncApexJob> jobs = [SELECT Status FROM AsyncApexJob WHERE Id = :commBatchJobId];
			System.assertEquals('Completed', jobs[0].Status);
			RQO_obj_communication__c comm = [SELECT Name, RQO_fld_alreadySent__c FROM RQO_obj_communication__c WHERE Name = 'Test Multimedia Comm'];
			System.assertEquals(true, comm.RQO_fld_alreadySent__c, 'La comunicación no se ha enviado por email');

			// Comprobaciones
			jobs = [SELECT Status FROM AsyncApexJob WHERE Id = :surveyBatchJobId];
			System.assertEquals('Completed', jobs[0].Status);
			RQO_obj_survey__c survey = [SELECT Name, RQO_fld_alreadySent__c FROM RQO_obj_survey__c WHERE Name = 'Test Survey'];
			System.assertEquals(true, survey.RQO_fld_alreadySent__c, 'La encuesta no se ha enviado por email');
        }
    }
}