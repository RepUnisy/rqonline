/**
*   @name: RQO_cls_BusquedaGrados_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Nicolás García - Unisys
*   @description: Clase de test para probar la clase RQO_cls_BusquedaGrados_cc
*/
 
@isTest
public class RQO_cls_BusquedaGrados_test {
    private static List<RQO_obj_category__c> categoryList;
    private static List<RQO_obj_masterSpecification__c> masterSpecificationList;
    private static List<RQO_obj_catSpecification__c> categorySpecList;
    private static List<RQO_obj_qp0Grade__c> qp0GradeList;
    private static List<RQO_obj_grade__c> gradeList;
    private static List<RQO_obj_translation__c> translationGradeList;
    private static List<RQO_obj_specification__c> specificationList;
    
    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    public static void initialSetup() {
        // Create user
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            // 
        //	createFeatureList();   
            
            // Create categories List
            createCategoryList();
    
            // Create Qp0 Grade List
            createQpoGradeList();
    
            // Create Grade List
            createGradeList();
            
            // Create Translation Category List
            createTranslationCategoryList();
            
            // Create Category Specification List
            createCatSpecificationList();
    
            // Create Translation
            createTranslationGradeList();
            
            // Create assigned grades
            createAssignedGrades();
		}
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea una lista de categorías
    * @exception: 
    * @throws: 
    */
    private static void createCategoryList() {
        categoryList = RQO_cls_TestDataFactory.createCategory(4);
                
        categoryList[1].RQO_fld_parentCategory__c = null;
        categoryList[1].RQO_fld_type__c = Label.RQO_lbl_Segmentos;
        categoryList[2].RQO_fld_parentCategory__c = null;
        categoryList[2].RQO_fld_type__c = Label.RQO_lbl_Aplicaciones;  
        categoryList[3].RQO_fld_parentCategory__c = null;
        categoryList[3].RQO_fld_type__c = Label.RQO_lbl_Segmentos;

        update categoryList;
    }
	
    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea un usuario necesario para la ejecución del test
    * @exception: 
    * @throws: 
    */
    private static void createUser() {
        RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es',
            'es_ES','Europe/Berlin');
    }

    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea una lista de categorías traducidas
    * @exception: 
    * @throws: 
    */
    private static void createTranslationCategoryList() {
        List<RQO_obj_translation__c> translationCategoryList = RQO_cls_TestDataFactory.createTranslation(200);
       
      //  translationCategoryList[0].RQO_fld_category__c = categoryList[0].Id;
        
        //update translationCategoryList[0];
    }
	
    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea una lista de especificaciones de categorías
    * @exception: 
    * @throws: 
    */
    private static void createCatSpecificationList() {
        categorySpecList = RQO_cls_TestDataFactory.createCatSpecification(2);
        
        update categorySpecList;
    }
	
    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea una lista de Qp0Grades
    * @exception: 
    * @throws: 
    */
    private static void createQpoGradeList() {
        qp0GradeList = RQO_cls_TestDataFactory.createQp0Grade(4);
    }

    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea una lista de grados
    * @exception: 
    * @throws: 
    */
    private static void createGradeList() {
        gradeList = RQO_cls_TestDataFactory.createGrade(3);

        gradeList[0].RQO_fld_sku__c = qp0GradeList[0].Id;
        gradeList[0].RQO_fld_product__c = categoryList[0].Id;

        gradeList[1].RQO_fld_sku__c = qp0GradeList[0].Id;
        gradeList[1].RQO_fld_product__c = categoryList[0].Id;

        gradeList[2].RQO_fld_sku__c = qp0GradeList[0].Id;
        gradeList[2].RQO_fld_product__c = categoryList[0].Id;

        update gradeList;
    }
	
    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea una lista de traducciones de grado
    * @exception: 
    * @throws: 
    */
    private static void createTranslationGradeList() {
        translationGradeList = RQO_cls_TestDataFactory.createTranslation(3);
        
        translationGradeList[0].RQO_fld_grade__c = gradeList[0].Id;
        translationGradeList[1].RQO_fld_grade__c = gradeList[0].Id;
        translationGradeList[2].RQO_fld_grade__c = gradeList[0].Id;
        
        update translationGradeList;
    }
	
    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea una lista de features
    * @exception: 
    * @throws: 
    */
    private static void createFeatureList() {
        
        RQO_cls_TestDataFactory.createFeatures(200);
    }
	
    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea una lista de especificaciones
    * @exception: 
    * @throws: 
    */
    private static void createSpecificationList() {
        specificationList = RQO_cls_TestDataFactory.createSpecification(4);

        specificationList[0].RQO_fld_grade__c = gradeList[0].Id;
        specificationList[0].RQO_fld_catSpecification__c = categorySpecList[0].Id;
        specificationList[0].RQO_fld_value__c = 'mayo';

        specificationList[1].RQO_fld_grade__c = gradeList[0].Id;
        specificationList[1].RQO_fld_catSpecification__c = categorySpecList[1].Id;
        specificationList[1].RQO_fld_value__c = '37';

        specificationList[2].RQO_fld_grade__c = gradeList[1].Id;
        specificationList[2].RQO_fld_catSpecification__c = categorySpecList[0].Id;
        specificationList[2].RQO_fld_value__c = 'marzo';

        specificationList[3].RQO_fld_grade__c = gradeList[1].Id;
        specificationList[3].RQO_fld_catSpecification__c = categorySpecList[1].Id;
        specificationList[3].RQO_fld_value__c = '45';
        
        update specificationList;  
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Nicolás García - Unisys
    * @description:	Método auxiliar que crea una lista de grados asignados
    * @exception: 
    * @throws: 
    */
    private static void createAssignedGrades() {
        RQO_cls_TestDataFactory.createAssignedGrade(10);
    }

    /**
    * @creation date: 31/10/2017
    * @author: Nicolás García - Unisys
    * @description: Método que ejecuta toda la lógica de la clase para cubrirla
    * @exception: 
    * @throws: 
	*/
    @isTest
    public static void testBusquedaGrados() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            Test.startTest();
            
            RQO_obj_category__c categoryInserted = getCategoryByType(Label.RQO_lbl_productKey);
            RQO_obj_category__c categoryInserted2 = getCategoryByType(Label.RQO_lbl_segmentos);
            RQO_obj_category__c categoryInserted3 = getCategoryByType(Label.RQO_lbl_aplicaciones);
            RQO_obj_category__c categoryInserted4 = getCategoryByType(Label.RQO_lbl_segmentos);
            
            RQO_obj_grade__c gradeInserted = [
                SELECT
                Id
                FROM
                RQO_obj_grade__c
                WHERE
                Name LIKE '%test%'
                LIMIT 1
            ];
            
            // Revisar: Con el cambio de API Names se añade el último parámetro de entrada (RelatedAccount)
            // para que compile
            RQO_cls_BusquedaGrados_cc.getBusquedaGrados('test1', 'es', 'in', 'in', 'test', true, null);
            RQO_cls_BusquedaGrados_cc.getBusquedaGrados('test1', 'es', 'in', 'in', 'test', false, null);
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 31/10/2017
    * @author: Nicolás García - Unisys
    * @description: Método auxiliar que obtiene una categoría según su tipo
    * @exception: 
    * @throws: 
	*/
    private static RQO_obj_category__c getCategoryByType(String type) {
        return [
            SELECT
                Id
            FROM
                RQO_obj_category__c
            WHERE
                RQO_fld_type__c = :type
            LIMIT 1
        ];
    }
    
    /**
    * @creation date: 31/10/2017
    * @author: Nicolás García - Unisys
    * @description: Método que testea la casuística de no inicialización de la inicalización de filtros
    * @exception: 
    * @throws: 
	*/
    @isTest
    public static void testInicializarFiltrosNotInitialize() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            String invalidFilter = 'UNKNOWN';
            
            String JSONfiltros = RQO_cls_BusquedaGrados_cc.inicializarFiltros(invalidFilter);
            
            Map<String, List<RQO_obj_category__c> > mapaFiltros = (Map<String, List<RQO_obj_category__c> >) JSON.deserialize(JSONfiltros, Map<String, List<RQO_obj_category__c> >.class);
            
            System.assertEquals(3, mapaFiltros.size(), 'No se inicializan los filtros correctamente.');
        }
    }
    
    /**
    * @creation date: 31/10/2017
    * @author: Nicolás García - Unisys
    * @description: Método que testea la búsqueda de la categoría de un grado
    * @exception: 
    * @throws: 
	*/
    @isTest
    public static void testBusquedaCategoriaDesdeGrado() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            List<RQO_obj_grade__c> gradeList = [
                SELECT
                Id
                FROM
                RQO_obj_grade__c
                WHERE
                Name LIKE '%test%'
                LIMIT 1
            ];
            
            // Revisar: Con el cambio de API Names se añade el último parámetro de entrada (RelatedAccount)
            // para que compile
            gradeList[0].RQO_fld_private__c = true;
            update gradeList[0];
            
            RQO_cls_BusquedaGrados_cc.getBusquedaCategoriaDesdeGrado(gradeList[0].Id, new List<RQO_obj_grade__c>(),
                                                                     'test', true, null);
            
            gradeList[0].RQO_fld_private__c = false;
            gradeList[0].RQO_fld_public__c = true;
            update gradeList[0];
            
            RQO_cls_BusquedaGrados_cc.getBusquedaCategoriaDesdeGrado(gradeList[0].Id, new List<RQO_obj_grade__c>(),
                                                                     'test', false, null); 
        }
    }
    
    /**
    * @creation date: 31/10/2017
    * @author: Nicolás García - Unisys
    * @description: Método que testea el lanzamiento del cambio de una picklist
    * @exception: 
    * @throws: 
	*/
    @isTest
    public static void testLanzarCambioPickList() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            
            RQO_obj_category__c categoryInserted = getCategoryByType(Label.RQO_lbl_productKey);
            RQO_obj_category__c categoryInserted3 = getCategoryByType(Label.RQO_lbl_Aplicaciones);
            
            String retorno = RQO_cls_BusquedaGrados_cc.lanzarCambioPicklist(categoryInserted.Id, categoryInserted3.Id, 'test');
            
            Map<String, List<RQO_obj_category__c> > mapaPicklists = 
                (Map<String, List<RQO_obj_category__c> >) JSON.deserialize(retorno, Map<String, List<RQO_obj_category__c> >.class);
            
            System.assertEquals(3, mapaPicklists.size(), 'No se inicializan las picklists correctamente.');
            
            retorno = RQO_cls_BusquedaGrados_cc.lanzarCambioPicklist( 'null', categoryInserted3.Id, 'test');
            mapaPicklists = (Map<String, List<RQO_obj_category__c> >)JSON.deserialize(retorno, Map<String, List<RQO_obj_category__c> >.class);
            
            System.assertEquals(3, mapaPicklists.size(), 'No se inicializan las picklists correctamente.');
            
            retorno = RQO_cls_BusquedaGrados_cc.lanzarCambioPicklist( 'null', 'null', 'test');
            mapaPicklists = (Map<String, List<RQO_obj_category__c> >) JSON.deserialize(retorno, Map<String, List<RQO_obj_category__c> >.class);
            
            System.assertEquals(3, mapaPicklists.size(), 'No se inicializan las picklists correctamente.');
        }
    }
    
    /**
    * @creation date: 31/10/2017
    * @author: Nicolás García - Unisys
    * @description: Método que testea la obtención de todos los grados
    * @exception: 
    * @throws: 
	*/
    @isTest
    public static void testGetAllGrades() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            List<RQO_obj_assignedGrades__c> assignedGrades =  [
                SELECT
                Id,
                RQO_fld_relatedAccount__c,
                RQO_fld_relatedGrade__c
                FROM
                RQO_obj_assignedGrades__c
                LIMIT 1];
            
            List<RQO_obj_grade__c> gradeListToUpdate = new List<RQO_obj_grade__c>();
            
            for (RQO_obj_assignedGrades__c assignedGrade : assignedGrades) {
                Id gradeId = assignedGrade.RQO_fld_relatedGrade__c;
                RQO_obj_grade__c grade = [SELECT Id FROM RQO_obj_grade__c WHERE Id =: gradeId];
                grade.RQO_fld_private__c = true;
                
                gradeListToUpdate.add(grade);
            }
            
            update gradeListToUpdate;
            
            RQO_obj_category__c categoryInserted = getCategoryByType(Label.RQO_lbl_productKey);
            RQO_obj_category__c categoryInserted3 = getCategoryByType(Label.RQO_lbl_aplicaciones);
            
            //String retorno = RQO_cls_BusquedaGrados_cc.lanzarCambioPicklist(categoryInserted.Id, categoryInserted3.Id, 'test');
            List<RQO_obj_translation__c> listaTraducciones = new List<RQO_obj_translation__c>();
            
            String retorno;
            //  Revisar: Con el cambio de API Names se añade el último parámetro de entrada (RelatedAccount) para que compile
            retorno = RQO_cls_BusquedaGrados_cc.getAllGrades('test', true, assignedGrades[0].RQO_fld_relatedAccount__c, 'test');
            listaTraducciones = (List<RQO_obj_translation__c>)JSON.deserialize(retorno, List<RQO_obj_translation__c>.class);
            System.assertEquals(false, listaTraducciones.isEmpty(), 'Error. No se están devolviendo grados.');
            
            List<RQO_obj_assignedGrades__c> assignedGradeListToDelete = [SELECT Id FROM RQO_obj_assignedGrades__c];
            DELETE assignedGradeListToDelete;
            
            //Revisar: Con el cambio de API Names se añade el último parámetro de entrada (RelatedAccount) para que compile
            retorno = RQO_cls_BusquedaGrados_cc.getAllGrades('test', false, assignedGrades[0].RQO_fld_relatedAccount__c, 'test');
            listaTraducciones = (List<RQO_obj_translation__c>)JSON.deserialize(retorno, List<RQO_obj_translation__c>.class);
            System.assertEquals(false, listaTraducciones.isEmpty(), 'Error. No se están devolviendo grados.');
        }
    }
        
	/**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getBusquedaGrados(){
         User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
            Account cuenta = [Select Id from Account LIMIT 1];
            List<RQO_obj_grade__c> listGrade = [Select id, RQO_fld_private__c from RQO_obj_grade__c LIMIT 200];
            for (Integer i = 0; i<listGrade.size();i++){
                listGrade[i].RQO_fld_private__c = true;
            }
            update listGrade;
            
			List<RQO_obj_assignedGrades__c> listaGradosAsigando =[SELECT Id, RQO_fld_relatedGrade__c,RQO_fld_relatedGrade__r.RQO_fld_product__c,
                                                              RQO_fld_relatedGrade__r.RQO_fld_private__c
                                                              FROM RQO_obj_assignedGrades__c WHERE RQO_fld_relatedAccount__c = :cuenta.Id];
            delete listaGradosAsigando;
            
            RQO_obj_category__c categoria = [Select Id from RQO_obj_category__c where RQO_fld_type__c = 'Aplicaciones' LIMIT 1];
			RQO_obj_category__c categoriaInsert = new RQO_obj_category__c(RQO_fld_type__c = 'Segmentos');
			insert categoriaInsert;
			
			List<RQO_obj_features__c> listFeatures = new List<RQO_obj_features__c>();
			RQO_obj_features__c features = new RQO_obj_features__c (RQO_fld_grade__c = listGrade.get(0).Id, RQO_fld_category__c = categoria.Id);
			RQO_obj_features__c features2 = new RQO_obj_features__c (RQO_fld_grade__c = listGrade.get(0).Id, RQO_fld_category__c = categoriaInsert.Id);
			
			listFeatures.add(features);
            listFeatures.add(features2);
			insert listFeatures;
            
            
            Test.startTest();
            	String response = RQO_cls_BusquedaGrados_cc.getBusquedaGrados('test', null, categoria.Id, categoriaInsert.Id, 'test', false, cuenta.Id);
			Test.stopTest();
            
            System.assertNotEquals(null, response);
        }
    }
    
}