/**
*   @name: RQO_cls_GigyaSearchResponse
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona las búsquedas y respuestas con Gigya
*/
public class RQO_cls_GigyaSearchResponse {
    /**
    *   @name: RQO_cls_GigyaSearchResponse
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que consume un objeto JSON
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }

    /**
    *   @name: DocID
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase de gestión de un Id de Documento
    */
    public class DocID {
        public String Aviacion {get;set;} 
        public String Tipo {get;set;} 
        public String GLP {get;set;} 
        public String Pers {get;set;} 
        public String Solred {get;set;} 
        public String VVDD {get;set;}

        /**
        *   @name: DocID
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Método constructor de la clase DocID
        *   @return: 
        *   @exception: 
        *   @throws: 
        */
        public DocID(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'Aviacion') {
                            Aviacion = parser.getText();
                        } else if (text == 'Tipo') {
                            Tipo = parser.getText();
                        } else if (text == 'GLP') {
                            GLP = parser.getText();
                        } else if (text == 'Pers') {
                            Pers = parser.getText();
                        } else if (text == 'Solred') {
                            Solred = parser.getText();
                        } else if (text == 'VVDD') {
                            VVDD = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'DocID consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    /**
    *   @name: Service
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase genérica de Servicios
    */
    public class Service {
        public string Quimica {get;set;} 
        
        /**
        *   @name: Service
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase Service
        *   @return: 
        *   @exception: 
        *   @throws: 
        */
        public Service(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'Quimica') {
                            Quimica = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Service consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    /**
    *   @name: Data
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase genérica de Datos que envuelve un servicio, codCliente y DocId principalmente
    */
    public class Data {
        public CodCliente CodCliente {get;set;} 
        public Service Service {get;set;} 
        public DocID DocID {get;set;} 
        public LegalTerms LegalTerms {get;set;} 
        public LegalTerms LegalTermsDate {get;set;} 

        /**
        *   @name: Data
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase Data
        *   @return: 
        *   @exception: 
        *   @throws: 
        */
        public Data(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'CodCliente') {
                            CodCliente = new CodCliente(parser);
                        } else if (text == 'Service') {
                            Service = new Service(parser);
                        } else if (text == 'DocID') {
                            DocID = new DocID(parser);
                        } else if (text == 'LegalTerms') {
                            LegalTerms = new LegalTerms(parser);
                        } else if (text == 'LegalTermsDate') {
                            LegalTermsDate = new LegalTerms(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    /**
    *   @name: GigyaSearchResponse
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase genérica de búsqueda y respuesta Gigya
    */
    public class GigyaSearchResponse {
        public List<Results> results {get;set;} 
        public Integer objectsCount {get;set;} 
        public Integer totalCount {get;set;} 
        public Integer statusCode {get;set;} 
        public Integer errorCode {get;set;} 
        public String statusReason {get;set;} 
        public String callId {get;set;} 
        public String timeStamp {get;set;} 

        /**
        *   @name: GigyaSearchResponse
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase GigyaSearchResponse
        *   @return: 
        *   @exception: 
        *   @throws: 
        */
        public GigyaSearchResponse(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'results') {
                            results = new List<Results>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                results.add(new Results(parser));
                            }
                        } else if (text == 'objectsCount') {
                            objectsCount = parser.getIntegerValue();
                        } else if (text == 'totalCount') {
                            totalCount = parser.getIntegerValue();
                        } else if (text == 'statusCode') {
                            statusCode = parser.getIntegerValue();
                        } else if (text == 'errorCode') {
                            errorCode = parser.getIntegerValue();
                        } else if (text == 'statusReason') {
                            statusReason = parser.getText();
                        } else if (text == 'callId') {
                            callId = parser.getText();
                        } else if (text == 'time') {
                            timeStamp = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'RQO_cls_GigyaSearchResponse consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    /**
    *   @name: Profile
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase de gestión de perfiles
    */
    public class Profile {
        public String lastName {get;set;} 
        public String hometown {get;set;} 
        public String email {get;set;} 
        public String locale {get;set;} 
        public String firstName {get;set;} 
        public Phones phones {get;set;} 
        public String country {get;set;} 

        /**
        *   @name: Profile
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase Profile
        *   @return: 
        *   @exception: 
        *   @throws: 
        */
        public Profile(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'lastName') {
                            lastName = parser.getText();
                        } else if (text == 'hometown') {
                            hometown = parser.getText();
                        } else if (text == 'email') {
                            email = parser.getText();
                        } else if (text == 'locale') {
                            locale = parser.getText();
                        } else if (text == 'firstName') {
                            firstName = parser.getText();
                        } else if (text == 'phones') {
                            phones = new Phones(parser);
                        } else if (text == 'country') {
                            country = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Profile consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    /**
    *   @name: LegalTerms
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase de gestión de términos legales
    */
    public class LegalTerms {
        public String GLP {get;set;} 

        /**
        *   @name: LegalTerms
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase LegalTerms
        *   @return: 
        *   @exception: 
        *   @throws: 
        */
        public LegalTerms(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'GLP') {
                            GLP = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'LegalTerms consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    /**
    *   @name: Phones
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase de gestión de teléfonos
    */
    public class Phones {
        public String default_Z {get;set;} // in json: default

        /**
        *   @name: Phones
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase Phones
        *   @return: 
        *   @exception: 
        *   @throws: 
        */
        public Phones(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'default') {
                            default_Z = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Phones consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    /**
    *   @name: Results
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase de gestión de resultados
    */
    public class Results {
        public Long lastUpdatedTimestamp {get;set;} 
        public Data data {get;set;} 
        public String socialProviders {get;set;} 
        public Integer iRank {get;set;} 
        public String created {get;set;} 
        public Long lastLoginTimestamp {get;set;} 
        public String verified {get;set;} 
        public String oldestDataUpdated {get;set;} 
        public Profile profile {get;set;} 
        public Boolean isVerified {get;set;} 
        public Long createdTimestamp {get;set;} 
        public String lastUpdated {get;set;} 
        public Boolean isRegistered {get;set;} 
        public String regSource {get;set;} 
        public Boolean isActive {get;set;} 
        public Long oldestDataUpdatedTimestamp {get;set;} 
        public String lastLogin {get;set;} 
        public String UID {get;set;} 
        public String registered {get;set;} 
        public Long verifiedTimestamp {get;set;} 
        public Long registeredTimestamp {get;set;} 
        public String loginProvider {get;set;} 

        /**
        *   @name: Results
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase Results
        *   @return: 
        *   @exception: 
        *   @throws: 
        */
        public Results(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'lastUpdatedTimestamp') {
                            lastUpdatedTimestamp = parser.getLongValue();
                        } else if (text == 'data') {
                            data = new Data(parser);
                        } else if (text == 'socialProviders') {
                            socialProviders = parser.getText();
                        } else if (text == 'iRank') {
                            iRank = parser.getIntegerValue();
                        } else if (text == 'created') {
                            created = parser.getText();
                        } else if (text == 'lastLoginTimestamp') {
                            lastLoginTimestamp = parser.getLongValue();
                        } else if (text == 'verified') {
                            verified = parser.getText();
                        } else if (text == 'oldestDataUpdated') {
                            oldestDataUpdated = parser.getText();
                        } else if (text == 'profile') {
                            profile = new Profile(parser);
                        } else if (text == 'isVerified') {
                            isVerified = parser.getBooleanValue();
                        } else if (text == 'createdTimestamp') {
                            createdTimestamp = parser.getLongValue();
                        } else if (text == 'lastUpdated') {
                            lastUpdated = parser.getText();
                        } else if (text == 'isRegistered') {
                            isRegistered = parser.getBooleanValue();
                        } else if (text == 'regSource') {
                            regSource = parser.getText();
                        } else if (text == 'isActive') {
                            isActive = parser.getBooleanValue();
                        } else if (text == 'oldestDataUpdatedTimestamp') {
                            oldestDataUpdatedTimestamp = parser.getLongValue();
                        } else if (text == 'lastLogin') {
                            lastLogin = parser.getText();
                        } else if (text == 'UID') {
                            UID = parser.getText();
                        } else if (text == 'registered') {
                            registered = parser.getText();
                        } else if (text == 'verifiedTimestamp') {
                            verifiedTimestamp = parser.getLongValue();
                        } else if (text == 'registeredTimestamp') {
                            registeredTimestamp = parser.getLongValue();
                        } else if (text == 'loginProvider') {
                            loginProvider = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Results consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    /**
    *   @name: CodCliente
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase de gestión de códigos de cliente
    */
    public class CodCliente {
        public String Aviacion {get;set;} 
        public String GLP {get;set;} 
        public String Solred {get;set;} 
        public String VVDD {get;set;} 
        public String Quimica {get;set;} 

        /**
        *   @name: CodCliente
        *   @version: 1.0
        *   @creation date: 31/10/2017
        *   @author: Alfonso Constan López - Unisys
        *   @description: Constructor de la clase CodCliente
        *   @return: 
        *   @exception: 
        *   @throws: 
        */
        public CodCliente(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'Aviacion') {
                            Aviacion = parser.getText();
                        } else if (text == 'GLP') {
                            GLP = parser.getText();
                        } else if (text == 'Solred') {
                            Solred = parser.getText();
                        } else if (text == 'VVDD') {
                            VVDD = parser.getText();
                        } else if (text == 'Quimica') {
                            Quimica = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CodCliente consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    /**
    *   @name: RQO_cls_GigyaSearchResponse
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna GigyaSearchResponse recibido por parámetro en formato JSON
    */
    public static GigyaSearchResponse parse(String json) {
        return new GigyaSearchResponse(System.JSON.createParser(json));
    }
}