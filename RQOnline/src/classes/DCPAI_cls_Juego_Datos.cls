/*------------------------------------------------------------------------
* 
*   @name:              DCPAI_cls_Juego_Datos
*   @version:           1 
*   @creation date:     30/06/2017
*   @author:            Alfonso Constán López   -   Unisys
*   @description:       Juego de datos para las clases test
* 
----------------------------------------------------------------------------*/
public class DCPAI_cls_Juego_Datos {
    
    public static void crear_juego_datos(){
        List<ITPM_Budget_Investment__c> insertBudget = new List<ITPM_Budget_Investment__c>();
        List<ITPM_Budget_Item__c> insertCostItem = new List<ITPM_Budget_Item__c> ();
        List<ITPM_UBC_Project__c> insertUBC = new List<ITPM_UBC_Project__c>();
        List<ITPM_Version__c> insertVersion = new List<ITPM_Version__c>();
        List<ITPM_Version__c> insertVersionPlan = new List<ITPM_Version__c>();
        
        //creamos un usuario
        profile perfil = [SELECT id FROM profile WHERE name = 'Usuario estándar force.com'];
        RecordType rec = [select Id, Name from RecordType where Name = :Label.DCPAI_Version_RecordType_IP and SobjectType = 'ITPM_Budget_Item__c'];
        
        User usuarioGPS = new user(Username = 'pruebagps@pruebanorepetida.com', isActive = true, CurrencyIsoCode = 'GBP', 
                                   LastName = 'prueba', Email = 'prueba@prueba.com', Alias = 'prueba', 
                                   CommunityNickname = 'prueba@prueba.prueba', TimeZoneSidKey = 'America/Los_Angeles', 
                                   LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', ProfileId = perfil.id,
                                   LanguageLocaleKey = 'en_US');        
        insert usuarioGPS;
        
        DCPAI_IdConsecutivoPA__c consecutivoPA1 = new DCPAI_IdConsecutivoPA__c( DCPAI_Nombre__c = 'PA-2' , DCPAI_Consecutivo__c = '123');        
        insert consecutivoPA1;
        
        //  Creamos master ubc
        ITPM_UBC__c Master_Ubc = new ITPM_UBC__c(Name = 'masterUBC');
        insert Master_Ubc;
        
        //creamos el investment plan 2018 (solo es posible 1 plan cada año)
        DCPAI_obj_Investment_Plan__c inv_Plan1 = new DCPAI_obj_Investment_Plan__c(DCPAI_fld_Status__c = 'Not Blocked', DCPAI_Year__c = '2018',  DCPAI_fld_Type__c = 'UPA 15', DCPAI_fld_Last_Type__c = 'UPA 16'); 
        DCPAI_obj_Investment_Plan__c inv_Plan1_1 = new DCPAI_obj_Investment_Plan__c(DCPAI_fld_Status__c = 'Not Blocked', DCPAI_Year__c = '2020',  DCPAI_fld_Type__c = 'UPA 35', DCPAI_fld_Last_Type__c = 'UPA 36');       
        insert inv_Plan1;
        insert inv_Plan1_1;
        
        //  insertamos el plan de version
        ITPM_Version__c versionPlan = new ITPM_Version__c(
            Name = 'versionPlan1',
            ITPM_VER_SEL_Year_validity__c = '2018'
        );
        insert versionPlan;
        
        Integer nJuegoDatos = 201;
        for (Integer i = 0; i < nJuegoDatos ; i ++){
            //  Insertamos un conjunto de budget investment
            ITPM_Budget_Investment__c Budget_inv1 = new ITPM_Budget_Investment__c(
                ITPM_BIA_REL_Responsible__c = usuarioGPS.id,
                ITPM_BI_TX_Budget_Investment_name__c = 'Budget prueba',
                ITPM_BIA_TXA_Description__c = 'texto descripcion test', 
                ITPM_BI_SEL_Responsible_IT_Direction__c = 'D. TI SERVICIOS Y OPERACIONES GLOBAL', 
                ITPM_BIA_SEL_IT_Area__c = 'TI ATENCION A USUARIOS', 
                ITPM_BIA_SEL_IT_Subarea__c = 'TI MESA DE AYUDA',
                DCPAI_fld_Decision_Area__c = 'Upstream',
                DCPAI_fld_Segmentation__c = 'Business Deployment' );                        
            insertBudget.add(Budget_inv1);
            
            //  Insertamos budget version
            ITPM_Version__c versionBudget = new ITPM_Version__c(
                DCPAI_fld_Investment_Plan_Version__c = versionPlan.Id
            );
            insertVersionPlan.add(versionBudget);
        }
        insert insertVersionPlan;
        insert insertBudget;
        
        //  Insertamos un juego de datos para cada budget generado
        for(ITPM_Budget_Investment__c Budget_inv1 : insertBudget){
            
            //  Insertamos UBCS
            ITPM_UBC_Project__c UBCSBudget1 = new ITPM_UBC_Project__c(
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                ITPM_SEL_Delivery_Country__c = 'Spain',
                ITPM_SEL_Year__c = '2018',
                ITPM_REL_UBC__c = Master_Ubc.Id,
                ITPM_Percent__c = 100,
                ITPM_REL_Project__c  = null);
            insertUBC.add(UBCSBudget1);
            
            insertUBC[0].OwnerId = usuarioGPS.Id;
            
            ITPM_UBC_Project__c UBCSBudget2 = new ITPM_UBC_Project__c(
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                ITPM_SEL_Delivery_Country__c = 'Germany',
                DCPAI_fld_Code_of_project__c = 'PI-2033344',
                ITPM_SEL_Year__c = '2018',
                ITPM_REL_UBC__c = Master_Ubc.Id,
                ITPM_Percent__c = 70,
                ITPM_REL_Project__c  = null);
            insertUBC.add(UBCSBudget2);
            
            ITPM_UBC_Project__c UBCSBudget2_1 = new ITPM_UBC_Project__c(
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                ITPM_SEL_Delivery_Country__c = 'France',
                DCPAI_fld_Code_of_project__c = '',
                ITPM_SEL_Year__c = '2018',
                ITPM_REL_UBC__c = Master_Ubc.Id,
                ITPM_Percent__c = 30,
                ITPM_REL_Project__c  = null);
            insertUBC.add(UBCSBudget2_1);
            
            ITPM_UBC_Project__c UBCSBudget3 = new ITPM_UBC_Project__c(
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                ITPM_SEL_Delivery_Country__c = 'France',
                DCPAI_fld_Code_of_project__c = '',
                ITPM_SEL_Year__c = '2018',
                ITPM_REL_UBC__c = Master_Ubc.Id,
                ITPM_Percent__c = 50,
                ITPM_REL_Project__c  = null);       
            insertUBC.add(UBCSBudget3);
            
            
            ITPM_Budget_Item__c costItem_inv1 =  new ITPM_Budget_Item__c(
                RecordTypeId = rec.Id,
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                DCPAI_fld_Investment_Plan__c = inv_Plan1.id,
                ITPM_SEL_Budget_Type__c = 'Investment',
                ITPM_SEL_Country__c = 'Spain',
                ITPM_SEL_Year__c = '2018',
                DCPAI_fld_Code__c = '',
                DCPAI_fld_Investment_Nature__c = 'Project',
                ITPM_CUR_February_investment__c = 15,
                ITPM_CUR_March_investment__c = 50,
                ITPM_CUR_December_investment__c = 1,
                DCPAI_fld_Assing_Status__c = Label.DCPAI_CostItem_Status_Assigned,
                DCPAI_fld_Waiting_to_Assign__c = false);
            insertCostItem.add(costItem_inv1);

            ITPM_Budget_Item__c costItem_inv1_1 =  new ITPM_Budget_Item__c(
                RecordTypeId = rec.Id,
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                DCPAI_fld_Investment_Plan__c = inv_Plan1_1.id,
                ITPM_SEL_Budget_Type__c = 'Investment',
                ITPM_SEL_Country__c = 'Portugal',
                ITPM_SEL_Year__c = '2020',
                DCPAI_fld_Code__c = '',
                DCPAI_fld_Investment_Nature__c = 'Project',
                ITPM_CUR_February_investment__c = 15,
                ITPM_CUR_March_investment__c = 50,
                ITPM_CUR_December_investment__c = 1,
                DCPAI_fld_Assing_Status__c = Label.DCPAI_CostItem_Status_Assigned,
                DCPAI_fld_Waiting_to_Assign__c = false);
            insertCostItem.add(costItem_inv1_1);
            
            ITPM_Budget_Item__c costItem_inv2 =  new ITPM_Budget_Item__c(
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                DCPAI_fld_Investment_Plan__c = inv_Plan1.id,
                ITPM_SEL_Budget_Type__c = 'Investment',
                ITPM_SEL_Country__c = 'France',
                ITPM_SEL_Year__c = '2018',
                DCPAI_fld_Code__c = '',
                ITPM_CUR_June_investment__c = 15,
                ITPM_CUR_July_investment__c = 50,
                ITPM_CUR_February_investment__c = 1,
                DCPAI_fld_Assing_Status__c = Label.DCPAI_CostItem_Status_Assigned,
                DCPAI_fld_Waiting_to_Assign__c = false);
            insertCostItem.add(costItem_inv2);
            
            ITPM_Budget_Item__c costItem_inv3 =  new ITPM_Budget_Item__c(
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                ITPM_SEL_Budget_Type__c = 'Expense',
                ITPM_SEL_Country__c = 'Andorra',
                ITPM_SEL_Year__c = '2019',
                DCPAI_fld_Code__c = '',
                ITPM_CUR_March_Expense__c = 15,
                ITPM_CUR_May_expense__c = 50,
                ITPM_CUR_August_expense__c = 1,
                DCPAI_fld_Assing_Status__c = Label.DCPAI_CostItem_Status_Assigned,
                DCPAI_fld_Waiting_to_Assign__c = false
            );
            insertCostItem.add(costItem_inv3);
            
            ITPM_Budget_Item__c costItem_inv4 =  new ITPM_Budget_Item__c(
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                DCPAI_fld_Investment_Plan__c = inv_Plan1.id,
                ITPM_SEL_Budget_Type__c = 'Investment',
                ITPM_SEL_Country__c = 'Peru',
                ITPM_SEL_Year__c = '2018',
                DCPAI_fld_Code__c = 'PI-2033344',
                ITPM_CUR_June_investment__c = 15,
                ITPM_CUR_July_investment__c = 50,
                ITPM_CUR_February_investment__c = 1,
                DCPAI_fld_Assing_Status__c = Label.DCPAI_CostItem_Status_Assigned,
                DCPAI_fld_Waiting_to_Assign__c = true);        
            insertCostItem.add(costItem_inv4);    
            
            ITPM_Budget_Item__c costItem_inv5 =  new ITPM_Budget_Item__c(
                DCPAI_fld_Budget_Investment__c = Budget_inv1.id,
                DCPAI_fld_Investment_Plan__c = inv_Plan1.id,
                ITPM_SEL_Budget_Type__c = 'Investment',
                ITPM_SEL_Country__c = 'Peru',
                ITPM_SEL_Year__c = '2015',
                ITPM_CUR_June_investment__c = 15,
                ITPM_CUR_July_investment__c = 50,
                ITPM_CUR_February_investment__c = 1,
                DCPAI_fld_Assing_Status__c = Label.DCPAI_CostItem_Status_Assigned,
                DCPAI_fld_Waiting_to_Assign__c = true);        
            insertCostItem.add(costItem_inv5);                
            
        }
        insert insertUBC;
        insert insertCostItem;   
        inv_Plan1_1.DCPAI_fld_Status__c = 'Blocked';
        update inv_Plan1_1;
        
        for (ITPM_Version__c versionBudget : insertVersionPlan){
            ITPM_Version__c versionCostItem = new ITPM_Version__c(
                DCPAI_fld_Budget_Version_CostItem__c = versionBudget.Id,
                ITPM_VER_SEL_Country__c = 'Spain',
                ITPM_VER_SEL_Year_validity__c = '2018'
            );    
            insertVersion.add(versionCostItem);
            
            ITPM_Version__c versionUBC = new ITPM_Version__c(
                DCPAI_fld_Budget_Version_UBC__C = versionBudget.Id,
                ITPM_VER_SEL_Country__c = 'Spain',
                ITPM_VER_SEL_Year_validity__c = '2018',
                DCPAI_fld_Percent__c = '100'
            );
            insertVersion.add(versionUBC);
            
            ITPM_Version__c versionCostItem2 = new ITPM_Version__c(
                DCPAI_fld_Budget_Version_CostItem__c = versionBudget.Id,
                ITPM_VER_SEL_Country__c = 'Spain',
                DCPAI_fld_Code_of_project__c = 'PI-1234',
                ITPM_VER_SEL_Year_validity__c = '2018'
            );    
            insertVersion.add(versionCostItem2);
            
            ITPM_Version__c versionUBC2 = new ITPM_Version__c(
                DCPAI_fld_Budget_Version_UBC__C = versionBudget.Id,
                ITPM_VER_SEL_Country__c = 'Spain',
                ITPM_VER_SEL_Year_validity__c = '2018',
                DCPAI_fld_Code_of_project__c = 'PI-1234',
                DCPAI_fld_Percent__c = '100'
            );
            insertVersion.add(versionUBC2);
        }
        insert insertVersion;
        //  Insertamos otro plan único para el 2019
        DCPAI_obj_Investment_Plan__c inv_Plan2 = new DCPAI_obj_Investment_Plan__c(DCPAI_fld_Status__c = 'Not Blocked', DCPAI_Year__c = '2019',  DCPAI_fld_Type__c = 'UPA 25', DCPAI_fld_Last_Type__c = 'UPA 26');
        insert inv_Plan2;        
        
        List<ITPM_Budget_Item__c> listDelete = [select id from ITPM_Budget_Item__c where ITPM_SEL_Country__c = 'Peru' and ITPM_SEL_Year__c = '2015'];
        delete listDelete;
    }
    
    
}