/**
*   @name: RQO_cls_SurveyResponseSelector
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona las SurveyResponse
*/
public with sharing class RQO_cls_SurveyResponseSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_SurveyResponseSelector';
	
    /**
    *   @name: RQO_cls_SurveyResponseSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene la lista de campos de RQO_obj_surveyResponse__c
    */
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			RQO_obj_surveyResponse__c.Name,
			RQO_obj_surveyResponse__c.RQO_fld_question__c,
			RQO_obj_surveyResponse__c.RQO_fld_response__c,
			RQO_obj_surveyResponse__c.RQO_fld_user__c };
    }

    /**
    *   @name: RQO_cls_SurveyResponseSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el sObjectType de RQO_obj_surveyResponse__c
    */
    public Schema.SObjectType getSObjectType()
    {
        return RQO_obj_surveyResponse__c.sObjectType;
    }
    
    /**
    *   @name: RQO_cls_SurveyResponseSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que obtiene el campo por el cual ordenar los RQO_obj_surveyResponse__c
    */
    public override String getOrderBy()
	{
		return 'RQO_fld_question__c, RQO_fld_response__c';
	}

    /**
    *   @name: RQO_cls_SurveyResponseSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que retorna una lista de RQO_obj_surveyResponse__c en base a un conjunto
    *   de Ids de usuario
    */
    public List<RQO_obj_surveyResponse__c> selectByUserId(Set<ID> idUserSet)
    {
    	/**
        * Constantes
        */
        final String METHOD = 'selectByUserId';
        
        
        // Query Factory for this Selector (Survey Question)
    	fflib_QueryFactory surveyResponseQueryFactory = newQueryFactory();
    	
    	string query = surveyResponseQueryFactory.setCondition('RQO_fld_user__c in :idUserSet').toSOQL();

    	System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
    	
  		return (List<RQO_obj_surveyResponse__c>) Database.query(query);
    }
}