/**
*   @name: RQO_cls_MiPerfilController_cc
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona los consentimientos de notificaciones
*/
public with sharing class RQO_cls_NotificationConsents extends RQO_cls_ApplicationDomain 
{   
	// // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_NotificationConsents';
	
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método constructor de la clase que recibe una lista de
    *   consentimientos de notificaciones
    */
    public RQO_cls_NotificationConsents(List<RQO_obj_notificationConsent__c> consents) 
    {     
        super(consents);   
    }   
	
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase Constructor que implementa un constructor personalizado
    */
    public class Constructor implements fflib_SObjectDomain.IConstructable 
    {     
        public fflib_SObjectDomain construct(List<SObject>sObjectList) 
        {       
            return new RQO_cls_NotificationConsents(sObjectList);     
        }   
    }  
    
    /**
    *   @name: RQO_cls_MiPerfilController_cc
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que actualiza consentimientos
    */
    public void updateConsents()
    {
    	 /**
		 * Constantes
		 */
		final String METHOD = 'UpdateConsents';
		
		upsert Records;
    }

}