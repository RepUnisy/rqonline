/**
*   @name: RQO_cls_Consumption_test
*   @version: 1.0
*   @creation date: 27/02/2018
*   @author: Alvaro Alonso - Unisys
*   @description: Clase de test para probar la clase RQO_cls_Consumption_cc
*/
@IsTest
public class RQO_cls_Consumption_test {
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserConsumos@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createAccount(1);
            RQO_cls_TestDataFactory.createContainerJunction(1);
            RQO_cls_TestDataFactory.createCSSapFamilies();
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_SAP_SERVICES});
        }
    }
    
    /**
    * @creation date: 27/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Método test para probar la funcionalidad de obtención de consumos de Sap desde el controlador apex
    * @exception: 
    * @throws:  
    */
    @isTest
    static void getSapConsumption(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserConsumos@testorg.com');
        System.runAs(u){
            Account cuenta = [Select id from Account limit 1];
            RQO_obj_qp0Grade__c objQp0 = [Select Id, RQO_fld_sku__c from RQO_obj_qp0Grade__c];
            test.startTest();
                Test.setMock(WebServiceMock.class, new RQO_cls_SAPServicesMockTest(RQO_cls_Constantes.WS_GET_CONSUMPTION, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
                String response = RQO_cls_Consumption_cc.getSapConsumption(cuenta.Id, objQp0.RQO_fld_sku__c, 'test', null, null);
            test.stopTest();
            RQO_cls_ConsumptionWS.ConsumptionCustomResponse responseData 
                            = (RQO_cls_ConsumptionWS.ConsumptionCustomResponse)JSON.deserialize(response, RQO_cls_ConsumptionWS.ConsumptionCustomResponse.class);
            System.assertEquals(RQO_cls_Constantes.SOAP_COD_OK, responseData.errorCode);
        }
    }
}