public with sharing class ReportFinderUtil {

  public ReportFinderUtil(){}
  
  public String findReportId(String reportName){
    
    String ret = '';
    List <Report> myReport = [select Id,Name From Report Where Name=:reportName];
      if (myReport.size() != 0){
          ret =  myReport.get(0).Id;
      }
    
    return ret;
  }
  
  
}