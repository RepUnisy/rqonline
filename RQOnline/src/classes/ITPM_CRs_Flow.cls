/*------------------------------------------------------------------------
Author:         Javier Franco
Company:        Indra
Description:    Clase que implementa la funcionalidad de los botones 
                "Authorize CRs" y "Reject CRs".
Test Class:     ITPM_CRs_Test
History
<Date>          <Author>        <Description>
16-09-2016      Javier Franco   Initial version
----------------------------------------------------------------------------*/

global class ITPM_CRs_Flow {
    
    // Lista de Change Requests sobre la que se va a proceder.
    private static List<ITPM_Change_Request__c> CRs;
    
    /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Método que valida si se puede utilizar los botones de
                    Authorize y Reject con las CRs pasadas como argumento.
    IN:             selectedIds: Ids de CRs a validar.
    OUT:            'OK' si todo es correcto o el error encontrado.
    History
    <Date>          <Author>            <Description>
    15-09-2016      Javier Franco       Creación del Método
    ----------------------------------------------------------------------------*/
    
    webservice static String validateAuthorize (String selectedIds) {
        
        // Es correcto si se trata de un único AP activo.
        
        obtainSelectedCRs(selectedIds);
        
        if (CRs == null) {
            return ('Unexpected error acquiring CRs information. Please contact your administrator.');
        }
        else if (CRs.size() > 1) {
            return ('You only can authorize/reject one AP Change Request at once.');
        }
        else if (CRs[0].ITPM_SEL_Status__c.equals('Approved')) {
            return ('You cannot alter an approved Change Request.');
        }
        else if (CRs[0].ITPM_SEL_Status__c.equals('Rejected')) {
            return ('You cannot alter a rejected Change Request.');
        }
        else if (CRs[0].ITPM_SEL_Status__c.equals('Closed')) {
            return ('You cannot alter a closed Change Request.');
        }
        else if (CRs[0].RecordTypeId <> Schema.SObjectType.ITPM_Change_Request__c.getRecordTypeInfosByName().get('AP').getRecordTypeId()) {
            return ('You only can authorize/reject AP Change Requests.');
        }
        else if (!String.isBlank(CRs[0].ITPM_SEL_Change_Subcategory__c)) {
            if (CRs[0].ITPM_SEL_Change_Subcategory__c.contains('Time')) {
                if (CRs[0].ITPM_DT_Project_Planned_Finish_Date__c == null && CRs[0].ITPM_DT_Go_Live_Planned_Date__c == null)
                    return ('You must indicate at least a date to authorize/reject a Time Change Request.');
            }
        }
        
        return ('OK');
    }
    
    /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Método que valida si se puede utilizar el botón de CreateAP
                    con las Change Request pasadas como argumento.
    IN:             selectedIds: Ids de CRs a validar.
    OUT:            'OK' si todo es correcto o el error encontrado.
    History
    <Date>          <Author>            <Description>
    15-09-2016      Javier Franco       Creación del Método
    ----------------------------------------------------------------------------*/
    
    webservice static String validateCreateAP (String selectedIds) {
        
        // Es correcto si toda la selección es de NoAP activos.
        
        obtainSelectedCRs(selectedIds);
        if (CRs == null)
            return ('Unexpected error acquiring CRs information. Please contact your administrator.');
        
        // Valida que todos los CRs son de tipo NoAP y que están activos.
        Id noapRecordTypeId = Schema.SObjectType.ITPM_Change_Request__c.getRecordTypeInfosByName().get('NoAP').getRecordTypeId();
        
        for (ITPM_Change_Request__c cr : CRs) {
            if (!cr.ITPM_SEL_Status__c.equals('Draft'))
                return ('You cannot create an AP from NoAP finished Change Requests.');
            
            if (cr.RecordTypeId <> noapRecordTypeId)
                return ('All Change Requests must be NoAP to create an AP Change Request.');
        }
        
        return ('OK');
    }
    
    /*--------------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Método para cambios de estado de las AP CRs seleccionadas.
    IN:             selectedIds: Ids de CRs a cambiar.
                    finalStatus: Estado a introducir (Rejected / Closed / Approved)
    OUT:            'OK' si todo es correcto o el error encontrado.
    History
    <Date>          <Author>            <Description>
    15-09-2016      Javier Franco       Creación del Método
    13-10-2016      Javier Franco       Si se aprueban tiempos, se pasan al proyecto.
    ----------------------------------------------------------------------------------*/
    
    webservice static String changeStatusCR (String selectedIds, String finalStatus) {
        
        obtainSelectedCRs(selectedIds);
        if (CRs == null)
            return ('Unexpected error acquiring CRs information. Please contact with your administrator.');
        
        for (ITPM_Change_Request__c cr : CRs)
            cr.ITPM_SEL_Status__c = finalStatus;
        
        try {
            update(CRs);
        }
        catch (Exception err) {
            System.debug('@@@ Error changing status of CRs: ' + err.getMessage());
            return('Unexpected error changing status of CRs. Please contact your administrator.');
        }
        
        if (finalStatus.equals('Approved'))
            return (updateProject());
        
        return ('OK');
    }
    
    /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Método para crear una AP desde NoAPs.
    IN:             selectedIds: Ids de CRs a cerrar tras crear la nueva AP.
    OUT:            'OK-' + Id si todo es correcto o el error encontrado.
    History
    <Date>          <Author>            <Description>
    15-09-2016      Javier Franco       Creación del Método
    ----------------------------------------------------------------------------*/
    
    webservice static String createAPCR (String selectedIds) {
        
        obtainSelectedCRs(selectedIds);
        if (CRs == null)
            return ('Unexpected error acquiring CRs information. Please contact your administrator.');
        
        ITPM_Change_Request__c newCR = new ITPM_Change_Request__c();
        
        try {
            Id apRecordTypeId = Schema.SObjectType.ITPM_Change_Request__c.getRecordTypeInfosByName().get('AP').getRecordTypeId();
            
            newCR.RecordTypeId = apRecordTypeId;
            newCR.ITPM_REL_PROJECT__c = CRs[0].ITPM_REL_PROJECT__c;
            newCR.ITPM_SEL_Change_Subcategory__c = 'Time & Cost';
            
            // Validamos que se pueda insertar la nueva AP.
            String testCreacion = ITPM_CRs_Operations.validateInsertCR(newCR);
            if (!testCreacion.equals('OK'))
                return (testCreacion);
            
            insert(newCR);
            for (ITPM_Change_Request__c cr : CRs) {
                cr.ITPM_REL_AP_CR__c = newCR.Id;
                cr.ITPM_SEL_Status__c = 'Closed';
            }
            
            update(CRs);
        }
        catch (Exception err) {
            System.debug('@@@ Error changing status of CRs: ' + err.getMessage());
            return('Unexpected error changing status of CRs. Please contact your administrator.');
        }
        
        return ('OK-' + newCR.Id);
    }
    
    /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Actualiza las fechas de cierre de los proyectos asociados.
    IN:             
    OUT:            Actualiza la lista de proyectos asociados a los CRs.
    History
    <Date>          <Author>            <Description>
    13-10-2016      Javier Franco       Creación del Método
    ----------------------------------------------------------------------------*/
    
    private static String updateProject() {
        
        List <ITPM_Project__c> updateProjects = new List <ITPM_Project__c>();
        ITPM_Project__c project = null;
        
        // Creamos la información que se va a actualizar en los proyectos asociados.
        
        for (ITPM_Change_Request__c cr : CRs)  {
            
            if (!String.isBlank(cr.ITPM_SEL_Change_Subcategory__c)) {
                if (cr.ITPM_SEL_Change_Subcategory__c.contains('Time')) {                    
                    project = new ITPM_Project__c();
                    project.Id = cr.ITPM_REL_PROJECT__c;
                    if(cr.ITPM_DT_Project_Planned_Finish_Date__c!=null)
                        project.ITPM_DT_Planned_Finish_Date__c = cr.ITPM_DT_Project_Planned_Finish_Date__c;
                    if(cr.ITPM_DT_Go_Live_Planned_Date__c!=null)
                       project.ITPM_PROJ_DT_Actual_Go_Live_Date__c= cr.ITPM_DT_Go_Live_Planned_Date__c;
                    updateProjects.add(project);
                }                
                if (cr.ITPM_SEL_Change_Subcategory__c.contains('Cost')) {                    
                    try {
                        List<ITPM_Budget_Item__c> listCI = [select id from ITPM_Budget_Item__c where ITPM_REL2_Project__c =: cr.ITPM_REL_PROJECT__c AND ITPM_CI_SEL_Version__c = 'Plan'];
                        for (ITPM_Budget_Item__c ci : listCI)  {
                            ci.ITPM_BLN_Pendiente_eliminar__c = true;
                        }
                        if(listCI != null && listCI.size()>0) update listCI;
                    }
                    catch (Exception err) {
                        System.debug('@@@ Error changing status of CRs: ' + err.getMessage());
                        return('Unexpected error changing status of CRs. Please contact your administrator.');                       
                    } 
                }
            }
        }
        
        try {
            if(updateProjects!=null && updateProjects.size()>0) update(updateProjects);
        }
        catch (Exception err) {
            System.debug('@@@ Error changing status of CRs: ' + err.getMessage());
            return('Unexpected error changing status of CRs. Please contact your administrator.');
        }
        
        return ('OK');
    }
    
    /*---------------------------------------------------------------------------
    Author:         Javier Franco
    Company:        Indra
    Description:    Convierte la lista de IDs pasada a formato lista.
    IN:             selectedIds: Ids de CRs.
    OUT:            Actualiza la lista de CRs propia de la clase.
    History
    <Date>          <Author>            <Description>
    15-09-2016      Javier Franco       Creación del Método
    ----------------------------------------------------------------------------*/
    
    private static void obtainSelectedCRs (String selectedIds) {
        
        // Procesamos la lista y la convertimos en una lista de IDs.
        List<String> listSelectedIds = selectedIds.split('-');
        
        // Obtenemos la info de los CRs implicados para poder analizarlos.
        CRs = [
            SELECT 
                Id, Name, RecordTypeId, ITPM_SEL_Status__c, ITPM_REL_Project__c, ITPM_SEL_Change_Subcategory__c, 
                ITPM_REL_AP_CR__c, ITPM_DT_Go_Live_Planned_Date__c, ITPM_DT_Project_Planned_Finish_Date__c,
                ITPM_DT_PR_Go_Live_Plan_Date__c, ITPM_DT_PR_Project_Plan_Finish_Date__c
            FROM ITPM_Change_Request__c
            WHERE Id IN :listSelectedIds
            ORDER BY Name
        ];
        
        if (CRs != null) {
            System.debug('tamaño lista ids pasados por parametro: '+listSelectedIds.size());
             System.debug('tamaño lista ids recuperados de BD: '+CRs.size());
            if (listSelectedIds.size() != CRs.size())
                CRs = null;
        }
    }  
}