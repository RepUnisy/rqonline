/**
*	@name: RQO_cls_DocumentTriggerHandler_test
*	@version: 1.0
*	@creation date: 21/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar el funcionamiento del trigger de documentos
*/
@IsTest
public class RQO_cls_DocumentTriggerHandler_test {

    /**
	* @creation date: 23/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createDocument(200);
            RQO_cls_TestDataFactory.createAsset(1);
		}
    }
    
    /**
	* @creation date: 22/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta de actualización en el trigger
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void testAfterUpdate(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            
            RecordType recordTypeAux = [Select Id from Recordtype where DeveloperName = 'RQO_rt_Posicion_de_solicitud'];
            List<Asset> assetList = [SELECT Id, RecordTypeId FROM Asset LIMIT 1];
            for(Asset item : assetList) {
                item.RecordTypeId = recordTypeAux.Id;
            }
            update assetList;
            
            List<RQO_obj_document__c> documentList = [SELECT Id, RQO_fld_documentId__c FROM RQO_obj_document__c LIMIT 1];
            for(RQO_obj_document__c item : documentList) {
                item.RQO_fld_documentId__c = item.RQO_fld_documentId__c+'Prueba';
            }
            update documentList;
            
            List<RQO_obj_notification__c> notificationList = [SELECT Id FROM RQO_obj_notification__c WHERE RQO_fld_messageType__c = 'NT'];
            test.stopTest();
            
            System.assertEquals(1, notificationList.size());
        }
    }
    
    
    
}