/**
*	@name: RQO_cls_EnqueueGetDocumentRP2_test
*	@version: 1.0
*	@creation date: 25/10/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase encolable de RP2
*/
@IsTest
public class RQO_cls_EnqueueGetDocumentRP2_test {
	
    /**
	* @creation date: 25/10/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserDocumentum@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSDocumentumRepository();
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String>{RQO_cls_Constantes.WS_DOCUM_GET, RQO_cls_Constantes.WS_DOCUM_SAP_GET, RQO_cls_Constantes.WS_DOCUM_CREATE});
            REP_cs_activacionTrigger__c actTri = new REP_cs_activacionTrigger__c();
            actTri.REP_fld_triggerActive__c = false;
            actTri.REP_fld_triggerName__c = 'RQO_trg_GradeTrigger';
            actTri.Name = 'RQO_trg_GradeTrigger';
            insert actTri;
            RQO_cls_TestDataFactory.createDocument(200);
		}
    }
    
    /**
	* @creation date: 9/2/2018
	* @author: Juan Elías - Unisys
	* @description:	Método test para probar RQO_cls_EnqueueGetDocumentRP2.
	* @exception: 
	* @throws: 
	*/
    @isTest 
    static void testGetDocumentumRP2(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            List<String> piNameToCreate = new List<String>();
            piNameToCreate.add(RQO_cls_Constantes.WS_DOCUM_RP2_GET);
            List<RQO_obj_logProcesos__c> createLogProcesos = RQO_cls_TestDataFactory.createLogProcesos(1);
            createLogProcesos[0].RQO_fld_processType__c = RQO_cls_Constantes.LOG_TYPE_BATCH;
            createLogProcesos[0].RQO_fld_descripciondeProceso__c = RQO_cls_Constantes.BATCH_DOCUMENTOS_RP2;
            createLogProcesos[0].RQO_fld_errorCode__c = RQO_cls_Constantes.BATCH_LOG_RESULT_OK;
            update createLogProcesos[0];
            List<RQO_obj_Document__c> listaDocumentos = [Select Id from RQO_obj_Document__c LIMIT 200];
            for(Integer i = 0; i<listaDocumentos.size();i++){
                listaDocumentos[i].RQO_fld_type__c = RQO_cls_Constantes.DOC_TYPE_FICHA_SEGURIDAD;
            }
            update listaDocumentos;
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(piNameToCreate);
            test.startTest();
            QueueableContext context;
            Test.setMock(WebServiceMock.class, new RQO_cls_RP2ServiceMockTest(false, RQO_cls_Constantes.MOCK_EXEC_OK, '0'));
            RQO_cls_EnqueueGetDocumentRP2 enqueueGetDocumentRP2 = new RQO_cls_EnqueueGetDocumentRP2();
            enqueueGetDocumentRP2.execute(context);
            List<RQO_obj_document__c> listaDocument = [Select RQO_fld_type__c from RQO_obj_document__c where RQO_fld_type__c = 'Ficha Seguridad'];
            //String strQueryParam = listaDocument[0].RQO_fld_type__c;
            System.assertEquals(200,  listaDocument.size());
            test.stopTest();     
        }
    }
    
    /**
	* @creation date: 25/10/2017
	* @author: Juan Elías - Unisys
	* @description:	Método test para probar RQO_cls_EnqueueGetDocumentRP2.
	* @exception: 
	* @throws: 
	*/
    @isTest 
    static void testFalseGetDocumentumRP2(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserDocumentum@testorg.com');
        System.runAs(u){
            List<String> piNameToCreate = new List<String>();
            piNameToCreate.add(RQO_cls_Constantes.WS_DOCUM_RP2_GET);
            List<RQO_obj_logProcesos__c> createLogProcesos = RQO_cls_TestDataFactory.createLogProcesos(1);
            createLogProcesos[0].RQO_fld_processType__c = RQO_cls_Constantes.LOG_TYPE_BATCH;
            createLogProcesos[0].RQO_fld_descripciondeProceso__c = RQO_cls_Constantes.BATCH_DOCUMENTOS_RP2;
            createLogProcesos[0].RQO_fld_errorCode__c = RQO_cls_Constantes.BATCH_LOG_RESULT_OK;
            update createLogProcesos[0];
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(piNameToCreate);
            test.startTest();
            QueueableContext context;
            Test.setMock(WebServiceMock.class, new RQO_cls_RP2ServiceMockTest(true, RQO_cls_Constantes.MOCK_EXEC_NOK, '0'));
            RQO_cls_EnqueueGetDocumentRP2 enqueueGetDocumentRP2 = new RQO_cls_EnqueueGetDocumentRP2();
            enqueueGetDocumentRP2.execute(context);
            List<RQO_obj_document__c> listaDocument = [Select RQO_fld_type__c from RQO_obj_document__c];
            String strQueryParam = listaDocument[0].RQO_fld_type__c;
            System.assertNotEquals(strQueryParam,null);
            test.stopTest();
        }
    }
}