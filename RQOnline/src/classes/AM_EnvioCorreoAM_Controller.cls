public with sharing class AM_EnvioCorreoAM_Controller{
    public AM_Accion_de_mejora__c am {get;set;}
    public AM_Estacion_de_servicio__c  es {get;set;}
    public string msgc {get;set;}
    public Boolean displayPopup {get;set;}
    
    public string pageRef {get;set;}
    public ApexPages.StandardController x;
    
    public AM_EnvioCorreoAM_Controller(ApexPages.StandardController controller) {
        AM_Accion_de_mejora__c a = (AM_Accion_de_mejora__c)controller.getRecord();
        x = controller;
         try{  
        am = [Select id, name, AM_Accion_Ellegida__c, AM_SEL_Responsable__c, AM_Item__c, AM_Codigo_Estacion_Servicio__c, AM_Otros__c, AM_REF_Estacion_de_servicio__c, AM_TX_Diagnostico__c, AM_REF_Estacion_de_servicio__r.name, AM_REF_Estacion_de_servicio__r.AM_EM_Correo_del_Responsable_Comercial__c from AM_Accion_de_mejora__c where id =: a.id limit 1];
        SYSTEM.DEBUG('hola:  ' + am);
        } catch (Exception e){
        
        }
        /*se deja asi por cambio de ultima hora, no quieren que aparezca el correo del responable*/
/*
         if(am !=null) {
              msgc = am.AM_REF_Estacion_de_servicio__r.AM_EM_Correo_del_Responsable_Comercial__c; 
         } else { msgc= 'vacio';}
  */       
         msgc = '';
         
    }
        
     /*   public String getPageURL() {
           ApexPages.StandardController sc = new ApexPages.StandardController(am);  
        
          PageReference pageRef = sc.edit();
          return pageRef.getUrl();
         
            
        } */
    
    
 //popup  
    public void showPopup()
        {
            displayPopup = true;
        }
    
    public void closePopup() {
        displayPopup = false;
        
    }
    
      public void emailsEs(){
       displayPopup = false;
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
         try{
          //  message.setBccAddresses(new String[]{ 'force@domdigital.pt'});
           // message.setBccSender(true);
            message.setSaveAsActivity(false);
            if (am.AM_Otros__c != null){
                //message.setPlainTextBody( 'Accion de Mejora: '+ URL.getSalesforceBaseUrl().toExternalForm()+ '/'+am.id);        
                message.setPlainTextBody('LE RECORDAMOS QUE DE ACUERDO AL PLAN DE MEJORA de su Estación de servicio debe de:\n' +  
                            '\n Estación de Servicio: ' + am.AM_Codigo_Estacion_Servicio__c + '\n \n' + 
                            '\n Item: ' + am.AM_Item__c + '\n' + 
                            '\n Diagnóstico: ' + am.AM_TX_Diagnostico__c + '\n' + 
                            '\n Acción Sugerida: ' + am.AM_Accion_Ellegida__c + '\n' + 
                            '\n Detalle propuesta: ' + am.AM_Otros__c + '\n' + 
                            '\n Responsable: ' + am.AM_SEL_Responsable__c + 
                            '\n \n Atentamente, \n  Responsable Comercial' +
                            '\n \n Este mail ha sido generado por una herramienta, en forma automática. Por favor no lo responda \n');
            }else{
                message.setPlainTextBody('LE RECORDAMOS QUE DE ACUERDO AL PLAN DE MEJORA de su Estación de servicio debe de:\n' +  
                            '\n Estación de Servicio: ' + am.AM_Codigo_Estacion_Servicio__c + '\n' + 
                            '\n Item: ' + am.AM_Item__c + '\n' + 
                            '\n Diagnóstico: ' + am.AM_TX_Diagnostico__c + '\n' + 
                            '\n Acción Sugerida: ' + am.AM_Accion_Ellegida__c + '\n' + 
                            '\n Detalle propuesta: ' + '\n' + 
                            '\n Responsable: ' + am.AM_SEL_Responsable__c + 
                            '\n \n Atentamente, \n  Responsable Comercial' +
                            '\n \n Este mail ha sido generado por una herramienta, en forma automática. Por favor no lo responda \n');
            }
        
            message.setSubject('E.S. nº '+am.AM_REF_Estacion_de_servicio__r.name +' Ejecución de Acción de Mejora');
          //  message.setToAddresses( new String[]{am.AM_REF_Estacion_de_servicio__r.AM_EM_Correo_del_Responsable_Comercial__c});
           message.setToAddresses( new String[]{msgc});
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
          System.debug('::::::::::::::::::::::'+message);
           
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
             
            if (results[0].isSuccess()) {
               apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'El correo ha sido enviado.');
                ApexPages.addmessage(msg);                   
          } else {
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'El correo no ha sido enviado.' + results[0].getErrors()[0].getMessage());
             ApexPages.addmessage(msg); 
          }

        } catch (Exception e){
        
        }
       PageReference accdetailPage = x.view();

    }
    
       
}