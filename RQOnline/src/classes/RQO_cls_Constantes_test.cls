/**
*	@name: RQO_cls_Constantes_test
*	@version: 1.0
*	@creation date: 05/03/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para la clase de constantes
*/
@IsTest
public class RQO_cls_Constantes_test {
	
    /**
    * @creation date: 05/03/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para las variables estáticas de la clase de constantes
    * @exception: 
    * @throws: 
    */
    @isTest
    static void testStaticVariables(){
        String constante = RQO_cls_Constantes.EQ;
        System.assert(constante.equalsIgnoreCase('='));
    }
}