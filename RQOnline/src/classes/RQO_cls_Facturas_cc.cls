/**
*   @name: RQO_cls_Facturas_cc
*   @version: 1.0
*   @creation date: 30/11/2017
*   @author: Alfonso Constán López - Unisys
*   @description: Controlador para la gestión de facturas
*   @testClass: RQO_cls_Facturas_test
*/
public without sharing class RQO_cls_Facturas_cc {
    
    private static final String CLASS_NAME = 'RQO_cls_Facturas_cc'; 
    
    public class seleccionador{
        string value;
        string label;
        
        public seleccionador(string v, string l){
            this.value = v;
            this.label = l;
        }
    }
    
    /**
    * @creation date: 30/11/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Obtención de facturas
    * @param: 
    * @return: 
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static String getFacturas(String idCliente, String numFactura, String tipoFactura, String estado, String dateFrom, String dateTo, String grado, String numAlbaran, String numPedido, 
                                     String dateEmisionFrom, String dateEmisionTo, Integer ivNumReg, String destinoMercancias, Boolean isHome){
		final String METHOD = 'getFacturas';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
                                         
		Boolean doCallout = true;
		List<String> listMateriales = null;
		String response = '';
                                         
		//Si viene relleno obtenemos el id con el destino de mercancias
		if(String.isNotBlank(destinoMercancias)){
			for (Account cuenta : [Select RQO_fld_idExterno__c from Account where Id = : Id.valueOf(destinoMercancias.trim())]){
                system.debug('destinoMercancias: ' + cuenta);
				destinoMercancias = cuenta.RQO_fld_idExterno__c;
			}
		}
		//Intentamos obtener el código de material a partir de la descripción del grado que introduce el usuario por pantalla
		if(String.isNotBlank(grado)){
			String codigo = '';
			for (RQO_obj_containerJunction__c objContainer : [Select RQO_fld_container__c, RQO_fld_container__r.RQO_fld_codigo__c, 
                                                              	RQO_fld_gradoQp0__r.RQO_fld_descripcionDelGrado__c, RQO_fld_gradoQp0__r.RQO_fld_sku__c			
																from RQO_obj_containerJunction__c where RQO_fld_container__r.RQO_fld_codigo__c <> '' and 
																RQO_fld_gradoQp0__r.RQO_fld_descripcionDelGrado__c = :grado.trim() LIMIT 1000]){
				
				if (listMateriales == null){listMateriales = new List<String>();}
				codigo = (objContainer.RQO_fld_container__r.RQO_fld_codigo__c.length() > 2)
                    			? objContainer.RQO_fld_gradoQp0__r.RQO_fld_sku__c + objContainer.RQO_fld_container__r.RQO_fld_codigo__c.subString(0, 2) 
                    			: objContainer.RQO_fld_gradoQp0__r.RQO_fld_sku__c + objContainer.RQO_fld_container__r.RQO_fld_codigo__c;
				listMateriales.add(codigo);
			}
			//Si llega grado pero no recuperamos datos de materiales no realizamos el callout
			if (listMateriales == null || listMateriales.isEmpty()){doCallout = false;}
		}
             
		if (doCallout){
            List <RQO_cls_FacturaBean> listFacturas = RQO_cls_FacturasGeneral.getFacturasListBySearch(numFactura, idCliente, null, listMateriales, 
                                                                                                      null, destinoMercancias, null, 
                                                                                                      numAlbaran, dateFrom , dateTo, 
                                                                                                      dateEmisionFrom, dateEmisionTo, ivNumReg, estado, tipoFactura, numPedido);
            if (isHome){
                RQO_cls_Constantes.FAC_ORDER_BY_FECHA_EMISION = true;
            }else{
                RQO_cls_Constantes.FAC_ORDER_BY_FECHA_VENCIMIENTO = true;
            }
    
            listFacturas.sort();
            response = JSON.serialize(listFacturas);
		}
                                         
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
                                         
		return response;
    }

    
    @AuraEnabled
    public static String getTipoFacturas(){
               
        List<seleccionador> listTipoFactuas = new List<seleccionador>();
        Schema.DescribeFieldResult fieldResult = RQO_obj_translation__c.RQO_fld_tipoFactura__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple){
            listTipoFactuas.add(new seleccionador( f.getValue(), f.getLabel()));           
        }
        return JSON.serialize(listTipoFactuas);

    }
    
    /**
    * @creation date: 04/12/2017
    * @author: David Igesias - Unisys
    * @description: Obtención del archivo de factura
    * @param: cliente
    * @param: fechaFactura
    * @param: numFactura
    * @param: idArchivado Id de documento en el repositorio de Documentum
    * @return: 
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static List<String> getFileFactura(String cliente, String fechaFactura, String numFactura, String idArchivado){
             
        /**
        * Constantes
        */
        final String METHOD = 'getTipoFacturas';
        
        /**
		* Inicio
		*/
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<String> listResponse = null;
            
        //Llamamos al servicio de SAP para recuperar si es posible mostrar la factura al usuario
        RQO_cls_Documentum docum = new RQO_cls_Documentum();
        docum.esFactura = true;
        if (String.isNotBlank(numFactura)) {
            RQO_cls_Documentum.RQO_cls_documentumResponse dataReturn = docum.getInvoiceDoc(RQO_cls_Constantes.DOC_CS_REPOSITORY, numFactura, fechaFactura);
            listResponse = new List<String>{dataReturn.errorCode, dataReturn.errorMessage, dataReturn.docData, dataReturn.docName, dataReturn.docType};
		}else{
			listResponse =  new List<String>{RQO_cls_Constantes.SOAP_COD_NOK, RQO_cls_Constantes.SOAP_COD_NOK};
		}
            
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        return listResponse;
    }    
    
    
	/**
    * @creation date: 15/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description: Recupera del custom metadata type el rango de años que aplican para el filtro de facturas
    * @return: Integer
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static Integer getDateParametrizedData(){
             
        final String METHOD = 'getDateParametrizedData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        Integer mesesDiferencia = 3;
        
        RQO_cmt_dateFilterData__mdt objDateFilter = RQO_cls_CustomSettingUtil.getParametrizacionDateFilterData(RQO_cls_Constantes.COD_FILTRO_FACTURAS);
        if (objDateFilter != null && objDateFilter.RQO_fld_numeroMeses__c != null){
        	mesesDiferencia = Integer.valueOf(objDateFilter.RQO_fld_numeroMeses__c);
        }

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return mesesDiferencia;
    }
}