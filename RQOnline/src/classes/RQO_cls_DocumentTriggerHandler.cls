/**
 *	@name: RQO_cls_DocumentTriggerHandler
 *	@version: 1.0
 *	@creation date: 19/12/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase controladora del trigger para RQO_obj_document__c
 */
public class RQO_cls_DocumentTriggerHandler {

	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_DocumentTriggerHandler';
    private static RQO_cls_DocumentTriggerHandler handler;

	// ***** CONSTRUCTORES ***** //
	public static RQO_cls_DocumentTriggerHandler getHandler() {
		if (handler == null) {
			handler = new RQO_cls_DocumentTriggerHandler();
		}
		return handler;
	}
    
	// ***** METODOS PUBLICOS ***** //
	      
    /**
	* @creation date: 19/12/2017
	* @author: David Iglesias - Unisys
	* @description:	Metodo encargado de procesar las actualizaciones del objeto RQO_obj_document__c
	* @param:	listNew		Lista de objetos posterior a la inserción
	* @param:	listOld		Lista de objetos anterior a la inserción
	* @param:	mapNew		Mapa de objetos posterior a la inserción
	* @param:	mapOld		Mapa de objetos anterior a la inserción
	* @return: 	
	* @exception: 
	* @throws: 
	*/
    public void afterUpdate(List<RQO_obj_document__c > listNew, List<RQO_obj_document__c > listOld, Map<Id, RQO_obj_document__c > mapNew, Map<Id, RQO_obj_document__c > mapOld) {
		
        /**
         * Constantes
         */
        final String METHOD = 'afterUpdate';
	
		/**
		 * Variables
		 */
		RQO_obj_notification__c notification = null;
		Set<String> accountSet = new Set<String> ();
		List<RQO_obj_notification__c> notificationList = new List<RQO_obj_notification__c> ();
		List<String> gradosList = new List<String> ();
		List<Asset> assetList = null;
		Map<String, List<String>> accountByGradeMap = new Map<String, List<String>> ();
		Map<String, List<String>> contactsByAccountMap = null;
        Map<String, String> gradesbyQp0Map = null;
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		// Tratamiento de los grados a los que hacen referencia los documentos modificados
		for (RQO_obj_document__c itemValueMap : mapNew.values()) {
			if (String.isNotBlank(itemValueMap.RQO_fld_qp0GradeId__c)) {
				gradosList.add(itemValueMap.RQO_fld_qp0GradeId__c);
			}
		}
		System.debug(CLASS_NAME + ' - ' + METHOD + ': gradosList: '+gradosList);
		
		// Obtención de los clientes que han comprado los grados
		getAccountByGrade(accountByGradeMap, accountSet, gradosList);
		System.debug(CLASS_NAME + ' - ' + METHOD + ': accountByGradeMap: '+accountByGradeMap);
		System.debug(CLASS_NAME + ' - ' + METHOD + ': accountSet: '+accountSet);
		
		// Obtención de los contactos asociados a clientes que han comprado los grados
		contactsByAccountMap = getContactsByAccount(accountSet);
		System.debug(CLASS_NAME + ' - ' + METHOD + ': contactsByAccountMap: '+contactsByAccountMap);
        
        // Obtención de los grados asociados a qp0
        gradesbyQp0Map = gradesbyQp0(gradosList);
				
		// Se recorren los documentos actualizados
		for (String itemKeyMap : mapNew.keySet()) {
			
			String idGradeAux = mapNew.get(itemKeyMap).RQO_fld_qp0GradeId__c;
			
			System.debug(CLASS_NAME + ' - ' + METHOD + ': itemKeyMap: '+itemKeyMap);
			// Comprobacion de modificación del documento
			System.debug(CLASS_NAME + ' - ' + METHOD + ': mapNew.get(itemKeyMap): '+mapNew.get(itemKeyMap));
			System.debug(CLASS_NAME + ' - ' + METHOD + ': mapOld.get(itemKeyMap): '+mapOld.get(itemKeyMap));
			System.debug(CLASS_NAME + ' - ' + METHOD + ': mapNew.get(itemKeyMap).RQO_fld_type__c: '+mapNew.get(itemKeyMap).RQO_fld_type__c);
			if (!mapNew.get(itemKeyMap).RQO_fld_documentId__c.equalsIgnoreCase(mapOld.get(itemKeyMap).RQO_fld_documentId__c) &&
					String.isNotEmpty(idGradeAux) && (RQO_cls_Constantes.DOC_TYPE_FICHA_TECNICA.equalsIgnoreCase(mapNew.get(itemKeyMap).RQO_fld_type__c)
						|| RQO_cls_Constantes.DOC_TYPE_FICHA_CERTIFICADO.equalsIgnoreCase(mapNew.get(itemKeyMap).RQO_fld_type__c))) {
				
				// Se recorren los clientes que han comprado el grado afectado
				System.debug('********idGradeAux***: ' + idGradeAux + ' // mapNew.get(itemKeyMap): ' + mapNew.get(itemKeyMap) + ' // mapNew: ' + mapNew );
				System.debug(CLASS_NAME + ' - ' + METHOD + ': accountByGradeMap.get(idGradeAux): '+accountByGradeMap.get(idGradeAux));
				if (idGradeAux.length() == 18) {
					idGradeAux = idGradeAux.substring(0, idGradeAux.length()-3);
				}
				
				if (accountByGradeMap.get(idGradeAux) != null) {
					for(String item : accountByGradeMap.get(idGradeAux)) {
						// Se recorren los contactos asociados a cada cliente
						if (contactsByAccountMap.get(item) != null) {
							for (String itemAux : contactsByAccountMap.get(item)) {
								notification = new RQO_obj_notification__c();
								notification.RQO_fld_contact__c = itemAux;
								notification.RQO_fld_externalId__c = idGradeAux;
								notification.RQO_fld_dynamicField__c = gradesbyQp0Map.get(idGradeAux);
								notification.RQO_fld_objectType__c = RQO_cls_Constantes.COD_SF_NOT_DOCUMENTACION;
								
								// Tipo de notificación a generar
								if (RQO_cls_Constantes.DOC_TYPE_FICHA_TECNICA.equalsIgnoreCase(mapNew.get(itemKeyMap).RQO_fld_type__c)) {
									notification.RQO_fld_messageType__c = RQO_cls_Constantes.COD_PROD_NOTA_TECNICA;
								} else if (RQO_cls_Constantes.DOC_TYPE_FICHA_CERTIFICADO.equalsIgnoreCase(mapNew.get(itemKeyMap).RQO_fld_type__c)) {
									notification.RQO_fld_messageType__c = RQO_cls_Constantes.COD_PROD_CERT_GRADO;
								}
								
								// Lista de notificaciones a insertar
								notificationList.add(notification);
								
							}
						}
					}
				}
			}
		}
		
		// Persistencia en BBDD de las notificaciones
		if (!notificationList.isEmpty()) {
			insert notificationList;
		}

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
	}
	
	// ***** METODO PRIVADOS ***** //
	
    /**
    * @creation date: 23/01/2018
    * @author: David Iglesias - Unisys
    * @description: Método para la obtención de Clientes asociados a grados comprados mediante control por referencia
	* @param: accountByGradeMap			Mapa de clientes por grado comprado
	* @param: accountSet				Set de clientes
	* @param: gradosList				Lista de grados
    * @exception: 
    * @throws: 
    */
	private static void getAccountByGrade (Map<String, List<String>> accountByGradeMap, Set<String> accountSet, List<String> gradosList) {
	
		/**
         * Constantes
         */
        final String METHOD = 'getAccountByGrade';
		
		/**
		 * Variables
		 */
		Id idRTAsset = null;
		List<String> accountList = null;

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		// Obtención del record type Posición de Solicitud
		idRTAsset = Asset.sObjectType.getDescribe().getRecordTypeInfosByName().get(RQO_cls_Constantes.RTID_ASSET_REQUEST_POSITION).getRecordTypeId();
		
		for (Asset itemAsset : [SELECT RQO_fld_qp0Grade__c, AccountId FROM Asset WHERE RQO_fld_qp0Grade__c IN :gradosList AND RecordTypeId = :idRTAsset]) {
		
			String idQp0Grade = itemAsset.RQO_fld_qp0Grade__c;
			Boolean flag = true;
			
			if (idQp0Grade.length() == 18) {
				idQp0Grade = idQp0Grade.substring(0, idQp0Grade.length()-3);
			}
		
			if (accountByGradeMap.get(idQp0Grade) == null) {
				accountList = new List<String> ();
			} else {
				accountList = accountByGradeMap.get(idQp0Grade);
			}
			
			// Recorremos la lista para evitar duplicidad de datos en la respuesta de la query para el mismo grado
			for (String itemAuxFor : accountList) {
				if (itemAuxFor.equalsIgnoreCase(itemAsset.AccountId)) {
					flag = false;
					continue;
				}
			}
			if (flag) {
				accountList.add(itemAsset.AccountId);
				accountByGradeMap.put(idQp0Grade, accountList);
				accountSet.add(itemAsset.AccountId);
			}
			
		}
		System.debug(CLASS_NAME + ' - ' + METHOD + ': accountByGradeMap: '+accountByGradeMap);
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	
	}
	
    /**
    * @creation date: 23/01/2018
    * @author: David Iglesias - Unisys
    * @description: Método para la obtención de Contactos asociados a Clientes
	* @param: accountSet					Set de clientes
	* @return: Map<String, List<String>>	Mapa con el binomio Cliente - Id Contactos asociados
    * @exception: 
    * @throws: 
    */
	private static Map<String, List<String>> getContactsByAccount (Set<String> accountSet) {
	
		/**
         * Constantes
         */
        final String METHOD = 'getContactsByAccount';
		
		/**
		 * Variables
		 */
		Map<String, List<String>> returnMap = new Map<String, List<String>> ();
		List<String> contactList = null;
		RecordType rt = null;

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		for (RecordType item : [SELECT Id FROM RecordType 
                                WHERE DeveloperName = :RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_CONTACT_QUIMICA 
                                	AND SobjectType = :RQO_cls_Constantes.CONTACT]) {
            rt = item;
        }

		
		for (Contact itemContact : [SELECT Id, AccountId FROM Contact 
									WHERE RecordTypeId = :rt.Id AND AccountId IN :accountSet]) {
											
			if (returnMap.get(itemContact.AccountId) == null) {
				contactList = new List<String> ();
			} else {
				contactList = returnMap.get(itemContact.AccountId);
			}
			
			contactList.add(itemContact.Id);											
			returnMap.put(itemContact.AccountId, contactList);
		}
		/*
		for (PermissionSetAssignment itemPermission : [SELECT Id, Assignee.ContactId, Assignee.Contact.AccountId 
														FROM PermissionSetAssignment 
														WHERE PermissionSet.Name = :RQO_cls_Constantes.QUIMICA_PERMISSION_SET AND Assignee.Contact.AccountId IN :accountSet]) {
											
			if (returnMap.get(itemPermission.Assignee.Contact.AccountId) == null) {
				contactList = new List<String> ();
			} else {
				contactList = returnMap.get(itemPermission.Assignee.Contact.AccountId);
			}
			
			contactList.add(itemPermission.Assignee.ContactId);											
			returnMap.put(itemPermission.Assignee.Contact.AccountId, contactList);
		}
		*/
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
		return returnMap;
	
	}
    
    /**
    * @creation date: 02/03/2018
    * @author: David Iglesias - Unisys
    * @description: Método para la obtención de grados asociados a grados qp0
	* @param: gradesList				Lista de grados
    * @exception: 
    * @throws: 
    */
	private static Map<String, String> gradesbyQp0 (List<String> gradesList) {
	
		/**
         * Constantes
         */
        final String METHOD = 'gradesbyQp0';
		
		/**
		 * Variables
		 */
		Map<String, String> returnMap = new Map<String, String> ();
		String qp0;

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		for (RQO_obj_grade__c item : [SELECT Id, RQO_fld_sku__r.Id FROM RQO_obj_grade__c WHERE RQO_fld_sku__r.Id IN :gradesList]) {
			qp0 = item.RQO_fld_sku__r.Id;
			if (qp0.length() == 18) {
				qp0 = qp0.substring(0, qp0.length()-3);
			}
			returnMap.put(qp0, item.Id);
		}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		return returnMap;
        
        
	}
     
}