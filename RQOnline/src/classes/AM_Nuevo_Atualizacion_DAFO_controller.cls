public with sharing class AM_Nuevo_Atualizacion_DAFO_controller{

    public AM_Estacion_de_servicio__c es {get;set;}
    public list<AM_DAFO__c> ldf {get;set;}
   
    public AM_DAFO__c daf {get;set;}
    
    public ApexPages.StandardController stdcontroller;
   
     
    public AM_Nuevo_Atualizacion_DAFO_controller(ApexPages.StandardController controller){
        es = (AM_Estacion_de_servicio__c)controller.getRecord();
        stdcontroller = controller;
     }   

        
        public  pagereference next(){
            ldf = [select id, AM_REL_Estacion_de_servicio__c from AM_DAFO__c where AM_REL_Estacion_de_servicio__c =:es.id ];
            es = [select name FROM AM_Estacion_de_servicio__c WHERE id =: es.id];
            
            System.debug(':::Prueba 1:::');
            System.debug(':::ldf:::'+ldf);
            system.debug(':::fffff..'+es.id);
            PageReference newpage ;
            
            if(!ldf.isempty())
            {
              string str = '/'+ldf[0].id+'/e?';
             newpage = new PageReference ('/'+ldf[0].id+'/e?retURL='+es.id);
             //newpage = new PageReference ('/one/one.app#/sObject/'ldf[0].id+ '/view?';
             newpage.setredirect(true);
               
                 //Pagereference newpage = new Pagereference ('/apex/AM_Nuevo_Atualizacion_DAFO?id='+ldf[0].id+'&action=edit');               
            }else {
                //AM_DAFO__c dafo = new AM_DAFO__c(AM_REL_Estacion_de_servicio__c =es.id);
                //insert dafo;
                //daf = dafo;
                //newpage = new PageReference ('/'+dafo.id+'/e?retURL='+es.id);
                
                Schema.DescribeSObjectResult r = AM_DAFO__c.sObjectType.getDescribe();                
                
                newpage = new PageReference ('/'+r.getKeyPrefix()+'/e?CF'+Label.AM_DAFO_ESTACION_DE_SERVICIO_ID+'='+es.Name+'&retURL='+es.id+'&saveURL='+es.id);
                //newpage = new PageReference ('/'+r.getKeyPrefix()+'/e?CF00N0E000000TN2N='+es.Name+'&retURL='+es.id+'&saveURL='+es.id);

                //newpage = new PageReference ('/one/one.app#/sObject/'+dafo.id+ '/view?');
                //  PageReference newpage = new PageReference (stdcontroller.view().getUrl());
                newpage.setredirect(true);
                // return stdcontroller.save();                
            }
             return newpage;
        }
         public  pagereference save(){
        
             return stdcontroller.save();
         
         }
       
    
}