/*------------------------------------------------------------------------
Author:         Borja Matín Ballesteros
Company:        Indra
Description:    
History
<Date>          <Author>                      <Description>
13-Jul-2016     Borja Matín Ballesteros       Initial version
----------------------------------------------------------------------------*/
public with sharing class ITPM_Milestone_Charts_Gantt_Helper {
  public String myId {get;set;}
  public Boolean allowView {get;set;}
  
  ApexPages.StandardController stdCont;
  
  public ITPM_Milestone_Charts_Gantt_Helper(ApexPages.StandardController stc){
    myId = stc.getId();
    allowView = Apexpages.currentPage().getParameters().get('p') == null;
  }
  
}