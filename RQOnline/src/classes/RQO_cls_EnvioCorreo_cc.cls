public class RQO_cls_EnvioCorreo_cc {
    
    // ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_EnvioCorreo_cc';
	
	/**
	* @creation date: 11/12/2017
	* @author: Unisys
	* @description:	 Método para el registro de la consulta 
	* @exception: 
	* @throws: 
	*/
    @AuraEnabled
    public static void sendRequest(String email, String message, Boolean copyEmail) { 
    	
    	// ***** CONSTANTES ***** //
        final String METHOD = 'sendRequest';
        
        try {
        	Id requestId = RQO_cls_informationRequestService.registerFaq(email, message);
        	RQO_cls_informationRequestService.sendEmail(copyEmail, requestId, null, null);
        }
	    catch (Exception e) {
	        AuraHandledException auraEx = new AuraHandledException(e.getMessage());
	        auraEx.setMessage(e.getMessage());
	        throw auraEx;
	    }
    }

}