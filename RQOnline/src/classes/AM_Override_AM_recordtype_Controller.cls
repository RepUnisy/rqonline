public with sharing class AM_Override_AM_recordtype_Controller {
	public string selected {get;set;}
    public AM_Estacion_de_servicio__c es {get;set;}
    public list<AM_Accion_de_mejora__c> lam {get;set;}
    public AM_Accion_de_mejora__c am {get;set;}
    public list<RecordType> oppRecTypeList {get;set;}
	public id rType {get;set;}

    
    public AM_Override_AM_recordtype_Controller(ApexPages.StandardController controller) {
        if(!test.isRunningTest()) controller.addFields(new List<String>{'AM_Codigo_G__c'});
        es = (AM_Estacion_de_servicio__c)controller.getRecord();

    }
    
    
    public List<SelectOption> getRecordTypes() {
      if(es.AM_Codigo_G__c == 'K' || es.AM_Codigo_G__c == 'U' || es.AM_Codigo_G__c == 'k' || es.AM_Codigo_G__c == 'u'){
    oppRecTypeList = [select id, name from RecordType
                             where sObjecttype = 'AM_Accion_de_mejora__c' 
                      AND  RecordType.Name IN ('Accesibilidad y movilidad' , 'Amabilidad', 'Limpieza', 'Operatividad', 'Otros', 'Rapidez y eficiencia')];
      } else {
            oppRecTypeList = [select id, name from RecordType
                             where sObjecttype = 'AM_Accion_de_mejora__c'
                              AND  RecordType.Name IN ('Accesibilidad y movilidad' , 'Amabilidad', 'Limpieza', 'Operatividad', 'Otros', 'Rapidez y eficiencia','Cartelería y promociones','Fidelización y motivación','Oferta de productos y servicios')];
      }
 
	 List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('','--Elija un Tipo de Accion de Mejora--'));
         for(RecordType r :oppRecTypeList ) {
             options.add(new SelectOption(r.id, r.name));
             
          
         }
       return options;  
    }
    
    public PageReference next(){
      am = new AM_Accion_de_mejora__c();
     
       Schema.DescribeSObjectResult r = AM_Accion_de_mejora__c.sObjectType.getDescribe();
        String keyPrefix = r.getKeyPrefix();
        System.debug('keyPrefix --'+keyPrefix );
        
        //    String prefix = am.SObjectType.getDescribe().getKeyPrefix(); 
        
        if(selected==null || selected == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Debe Elegir un tipo de Accion de Mejora'));
            return null;
        }  
      

  /* solo es disponible para Classic
    * 
    *  string fieldid;
   
	PageReference pr = new PageReference('/' + keyPrefix + '/e?nooverride=1');
            string html;
            Blob pageContent = pr.getContent();
            if(pageContent != null) {
                     html = pageContent.toString();
              }
             Matcher macher = Pattern.compile('<label for="(.*?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
             while(macher.find()) {
                                    String label = macher.group(3);
                                    String fldId = macher.group(1);
             system.debug(label+'############'+fldId);
                 if(label == 'Estación de servicio'){
                     fieldid = fldId;
                 }
                   system.debug(':::::: ES::::::'+fieldid);
        
         }
     pr = new PageReference('/'+keyPrefix+'/e?&'+fieldid+'=5763&'+fieldid+'_lkid='+es.id+'&retURL=%2F'+es.id+'&RecordType='+selected);
     return pr;*/
		return null;
          
        
     }
}