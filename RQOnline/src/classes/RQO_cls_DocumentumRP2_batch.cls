/**
 *	@name: RQO_cls_DocumentumRP2_batch
 *	@version: 1.0
 *	@creation date: 13/11/2017
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase batch para gestionar la inserción /actualización de fichas de seguridad asociadas a grados que nos hemos traido de RP2
 *	@testClass: RQO_cls_DocumentumRP2Batch_test
*/
global class RQO_cls_DocumentumRP2_batch implements Database.Batchable<sObject>, Database.Stateful {
	
    private static final String CLASS_NAME = 'RQO_cls_DocumentumRP2_batch';
    private List<String> listGradeData;
	private Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>> mapServiceData;
    private String docType;
    private RQO_obj_logProcesos__c generalLog;
    
    /**
	* @creation date: 14/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Constructor de la clase RQO_cls_DocumentumRP2_batc
	* @param: String docType
	* @param: List<String> listGradeData
	* @param: List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>> mapServiceData, datos extraidos del webservice de RP2
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
    public RQO_cls_DocumentumRP2_batch(String docType, List<String> listGradeData, Map<String, List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>> mapServiceData){
        this.docType = docType;
        this.listGradeData=listGradeData;
        this.mapServiceData = mapServiceData;
    }
	
    /**
	* @creation date: 14/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método start del batch. Realiza la query que obtiene los datos que queremos actualizar
	* @param: Database.BatchableContext
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        generalLog = RQO_cls_ProcessLog.instanceInitalLogBatch(Label.RQO_lbl_DocumentObject, RQO_cls_Constantes.BATCH_LOG_RESULT_OK, null, null, null, RQO_cls_Constantes.BATCH_DOCUMENTOS_RP2);
        insert generalLog;
        String query = 'Select Id, Name, RQO_fld_codeDocumentum__c, (Select Id, RQO_fld_locale__c, RQO_fld_type__c from Documents__r where RQO_fld_type__c = : docType) ' + 
            				' from RQO_obj_grade__c where RQO_fld_codeDocumentum__c = : listGradeData';
        return Database.getQueryLocator(query);
    }
    
    /**
	* @creation date: 14/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método execute del batch.
	* actualiza los datos que sean necesarios
	* @param: Database.BatchableContext
	* @param: List<sObject>
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
   	global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<RQO_obj_document__c> listaDocumentos = this.getDocList(scope);
        if (listaDocumentos != null && !listaDocumentos.isEmpty()){
            this.createDocumentData(listaDocumentos);
        }
   }

    /**
	* @creation date: 14/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método que se encarga de recuperar la lista de datos de documento que finalmente se actualizarán o insertarán en BBDD
	* comparando la lista de documentos recuperada a través del servicio con los que tenemos en BBDD
	* @param: List<sObject>
	* @return: List<RQO_obj_document__c>	
	* @exception: 
	* @throws: 
	*/
    private List<RQO_obj_document__c> getDocList(List<sObject> scope){
		final String METHOD = 'getDocList';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<RQO_obj_document__c> listaDocumentos = new List<RQO_obj_document__c>();
        RQO_obj_document__c objDocumentAux = null;
        String auxLocale = '';
        Map<String, String> mapaIdioma = RQO_cls_CustomSettingUtil.getMapSAPRP2Language();
        //Recorremos los grados que nos hemos traido con la query. Los grados están filtrados por los ids que nos hemos traido del servicio de RP2
        for(RQO_obj_grade__c gradeObj : (List<RQO_obj_grade__c>) scope) {
            //Verificamos que el grado exista dentro del mapa con los datos del servicio. Debería existir siempre.
            if (mapServiceData!= null && !mapServiceData.isEmpty() && mapServiceData.containsKey(gradeObj.RQO_fld_codeDocumentum__c)){
                //Recorremos la lista de documentos del grado
                for(RQO_cls_GetDocumentumRP2.ZeehsProdrlsa objService : mapServiceData.get(gradeObj.RQO_fld_codeDocumentum__c)){
                    objDocumentAux = null;
                    auxLocale = (objService.Idioma != null && mapaIdioma != null && !mapaIdioma.isEmpty() && mapaIdioma.containsKey(objService.Idioma)) ? mapaIdioma.get(objService.Idioma) : null;
                    //Verificamos si el documento actual de la lista de datos del servicio para el grado actual ya existe en BBDD.
                    //Si existe se actualizará con el nuevo id de documento. Si no existe se registra como nuevo
                    for(RQO_obj_document__c objDoc : gradeObj.Documents__r){
                        if (String.isNotEmpty(objDoc.RQO_fld_locale__c) && objDoc.RQO_fld_locale__c.equalsIgnoreCase(auxLocale)){
                            objDocumentAux = objDoc;
                            objDocumentAux.RQO_fld_documentId__c = objService.Docid;
                            break;
                        }
                    }
                    //El documento no existe. Lo registramos
                    if (objDocumentAux == null){
                        objDocumentAux = new RQO_obj_document__c(RQO_fld_documentId__c = objService.Docid, RQO_fld_grade__c = gradeObj.Id, 
							RQO_fld_locale__c = auxLocale, RQO_fld_type__c = this.docType);
                    }
                    listaDocumentos.add(objDocumentAux);
                }
            }
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
        
        return listaDocumentos;
    }
    
    
	/**
	* @creation date: 14/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Generación de los registros de documentos y gestión del log
	* actualiza los datos que sean necesarios
	* @param: List<RQO_obj_document__c>
	* @return:	
	* @exception: 
	* @throws: 
	*/
    private void createDocumentData(List<RQO_obj_document__c> listaDocumentos){
		final String METHOD = 'createDocumentData';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        Schema.SObjectField f = RQO_obj_document__c.Fields.Id;
        Database.UpsertResult[] upsertResultList = Database.upsert(listaDocumentos, f, false);
        if (upsertResultList != null && !upsertResultList.isEmpty()){
        	this.createRecordLog(upsertResultList);
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    } 
  
    /**
	* @creation date: 14/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Generación de los registros de log
	* actualiza los datos que sean necesarios
	* @param: Database.UpsertResult[]
	* @return:	
	* @exception: 
	* @throws: 
	*/
    private void createRecordLog(Database.UpsertResult[] upsertResultList){
		final String METHOD = 'createRecordLog';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<RQO_obj_logProcesos__c> listProcessLog;
        RQO_obj_logProcesos__c logObject = null;
        //Verificamos que documentos no se han generado correctamente y guardamos un registro de log por cada uno
        for (Database.UpsertResult sr : upsertResultList){
            if (!sr.isSuccess()){
                for(Database.Error err : sr.getErrors()){
                    logObject = RQO_cls_ProcessLog.instanceInitalLogBatch(Label.RQO_lbl_DocumentObject, String.valueOf(err.getStatusCode()), 
                                                                          String.valueOf(err.getFields()), 
                                                                          err.getMessage(), null, RQO_cls_Constantes.BATCH_DOCUMENTOS_RP2);
                    logObject.RQO_fld_parentLog__c = generalLog.Id;
                    if (logObject != null){
                        if(listProcessLog == null){listProcessLog = new List<RQO_obj_logProcesos__c> ();}
                        listProcessLog.add(logObject);
                    }
                }
            }
        }
        if (listProcessLog != null && !listProcessLog.isEmpty()){
			insert listProcessLog;
        }
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
    }
    
    /**
	* @creation date: 14/11/2017
	* @author: Alvaro Alonso - Unisys
	* @description:	Método finish del batch.
	* @param: Database.BatchableContext
	* @return: 	
	* @exception: 
	* @throws: 
	*/
   global void finish(Database.BatchableContext BC){
       List<RQO_obj_logProcesos__c> listGeneralLog = [Select Id from RQO_obj_logProcesos__c where RQO_fld_parentLog__c = : generalLog.Id];
       if (listGeneralLog != null && !listGeneralLog.isEmpty()){
           generalLog.RQO_fld_errorCode__c = RQO_cls_Constantes.BATCH_LOG_RESULT_NOK;
           update generalLog;
       }
   }

}