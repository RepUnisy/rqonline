/**
*   @name: RQO_cls_Application
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona la aplicación
*/
public class RQO_cls_Application  { 
    // Configure and create the UnitOfWorkFactory for this Application
    public static final fflib_Application.UnitOfWorkFactory UnitOfWork = 
        new fflib_Application.UnitOfWorkFactory(   
            new List<SObjectType> {      
                User.SObjectType,
                Contact.SObjectType,
                RQO_obj_grade__c.SObjectType,      
                RQO_obj_category__c.SObjectType,     
                RQO_obj_document__c.SObjectType,
                RQO_obj_informationRequest__c.SObjectType
            }
        );
        
    // Configure and create the DomainFactory for this Application
    public static final fflib_Application.DomainFactory Domain =
        new fflib_Application.DomainFactory (
            RQO_cls_Application.Selector,
            new Map<SObjectType, type> {
                RQO_obj_informationRequest__c.SObjectType => RQO_cls_informationRequests.Constructor.class
            }
        );
        
    // Configure and create the SelectorFactory for this Application
    public static final fflib_Application.SelectorFactory Selector =
        new fflib_Application.SelectorFactory (
            new Map<SObjectType, Type> {
                RQO_obj_informationRequest__c.SObjectType => RQO_cls_InformationRequestSelector.class,
                EmailTemplate.SObjectType => RQO_cls_emailTemplateSelector.class,
                RQO_cmt_informationMails__mdt.SObjectType => RQO_cls_informationMailsSelector.class
            }
        );
}