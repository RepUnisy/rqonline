/**
* @creation date: 27/02/2018
* @author: Alvaro Alonso - Unisys
* @description:	Clase de test para comprobar que el funcionamiento de la clase Trigger RQO_trg_Contact es correcto.
* @exception: 
* @throws:  
*/
@isTest
public class RQO_cls_Contact_test {
    
    /**
    * @creation date: 28/02/2018
    * @author: Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserEvents@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSActivacionTrigger(true);
        }
    }
	    
    /**
    * @creation date: 28/02/2018
    * @author: Unisys
    * @description:	Método de prueba de inserción del objeto
    * @exception: 
    * @throws: 
    */
    private static testMethod void testCreateContact() {
         User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserRunAsInitial@testorg.com');
            System.runAs(usuario){
            	List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
            	RQO_cls_CustomException ex = new RQO_cls_CustomException('testCreateContact');
                System.assertNotEquals(null,
                                	contact, 
                                    'No se ha generado el contacto.');
          }
     }

}