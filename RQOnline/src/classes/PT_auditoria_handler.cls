/*------------------------------------------------------------
Author:        	Rubén Simarro
Company:       	Indra
Description:   	
Clase encargada de realizar los insert al objeto Auditoria__c
Test Class:    	PT_test_trigger_usuarioAuditoria
History
<Date>      	<Author>     	<Change Description>
09-02-2016	    Rubén Simarro 	Initial Version
------------------------------------------------------------*/
public class PT_auditoria_handler {

    @future
    public static void insertaAuditoria(String operacion, String elemento, String nombre, Id idusuario){      
        
        Auditoria__c auditoria = new Auditoria__c();        
        auditoria.Accion__c = operacion;     
        auditoria.Elemento__c = elemento;                     
        auditoria.Nombre__c = nombre;  
        auditoria.Usuario__c = idusuario;
         
        System.debug('@@@ PT_auditoria_handler. Nuevo registro de auditoria. Accion__c: '+auditoria.Accion__c+'. Nombre__c: '
                         +auditoria.Nombre__c+'. Elemento: '+auditoria.Elemento__c+'. Usuario: '+ auditoria.Usuario__c);
        
        if (Test.isRunningTest()) {
            System.runAs(new User(Id = Userinfo.getUserId())) {
                insert auditoria;
            } 
        } 
        else {
            insert auditoria;        
        }
    }
}