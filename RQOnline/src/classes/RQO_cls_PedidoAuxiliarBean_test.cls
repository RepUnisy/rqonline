/**
 * name: RQO_cls_PedidoAuxiliarBean_test
 * @creation date: 18/12/2017
 * @author: Julio Maroto - Unisys
 * @description: Clase que testea los métodos de la clase RQO_cls_PedidoAuxiliarBean__c
 */
@isTest
public class RQO_cls_PedidoAuxiliarBean_test {
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que realiza la inicialización del dataset de ejemplo para la clase de test
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @testSetup
    public static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserDocumentum@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            // Create Request Management
            createRequestManagement();
            // Create Asset
            createAssets();
            // Create Grades
            createGrades();
            // Create Container
            createContainer();
            // Create Translations
            createTranslations();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear RequestManagements
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createRequestManagement() {
        RQO_cls_TestDataFactory.createRequestManagement(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Assets
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createAssets() {
        RQO_cls_TestDataFactory.createAsset(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Grados
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createGrades() {
        RQO_cls_TestDataFactory.createGrade(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Containers (envases)
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createContainer() {
        RQO_cls_TestDataFactory.createContainer(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Accounts
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createAccount() {
        RQO_cls_TestDataFactory.createAccount(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Translations
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createTranslations() {
        RQO_cls_TestDataFactory.createTranslation(200);
    }
    
        /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear el método compareTo de la clase RQO_cls_PedidoAuxiliarBean__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testCompareTo() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            // FIRST OBJECT
            RQO_cls_PedidoAuxiliarBean pedidoAuxiliarBean = new RQO_cls_PedidoAuxiliarBean();
            
            List<RQO_obj_requestManagement__c> requestManagementList = [SELECT Id, Name FROM RQO_obj_requestManagement__c];
            List<Asset> assetList = [SELECT Id FROM Asset];
            List<RQO_obj_grade__c> gradeList = [SELECT Id FROM RQO_obj_grade__c];
            List<RQO_obj_container__c> containerList = [SELECT Id, Name FROM RQO_obj_container__c];
            List<Account> accountList = [SELECT Id, Name FROM Account];
            List<RQO_obj_translation__c> containerTranslationList = [SELECT Id, RQO_fld_translation__c FROM RQO_obj_translation__c WHERE RQO_fld_container__c IN :containerList];
            
            pedidoAuxiliarBean.idSolicitud = requestManagementList[0].Id;
            pedidoAuxiliarBean.idAsset = assetList[0].Id;
            pedidoAuxiliarBean.idGrado = gradeList[0].Id;
            pedidoAuxiliarBean.idEnvase = containerList[0].Id;
            pedidoAuxiliarBean.idDestinoMercancias = accountList[0].Id;
            pedidoAuxiliarBean.nameSolicitud = requestManagementList[0].Name;
            pedidoAuxiliarBean.cantidad = 200;
            pedidoAuxiliarBean.destinoMercanciasNombre = accountList[0].Name;
            pedidoAuxiliarBean.envaseName = containerList[0].Name;
            pedidoAuxiliarBean.envaseTranslate = containerTranslationList[0].RQO_fld_translation__c;
            pedidoAuxiliarBean.numRefCliente = '1234';
            pedidoAuxiliarBean.statusCode = 'CODE_STATUS';
            pedidoAuxiliarBean.statusDescription = 'STATUS_DESCRIPCION';
            pedidoAuxiliarBean.statusCssClass = 'STATUS_CSS_CLASS';
            pedidoAuxiliarBean.qp0gradeName = 'QP0_GRADENAME';
            pedidoAuxiliarBean.numPedido = '90000';
            pedidoAuxiliarBean.fechaSolicitado = '2017-12-02';
            pedidoAuxiliarBean.fechaEntrega = '2018-03-21';
            pedidoAuxiliarBean.fechaEstado = '2018-04-22';
            pedidoAuxiliarBean.fechaCreacion = Date.valueOf('2018-05-23');
            pedidoAuxiliarBean.fechaPreferente = Date.valueOf('2019-06-02');
            pedidoAuxiliarBean.srcFechaPreferente = 'SOURCE';
            pedidoAuxiliarBean.showAlbaranCMR = true;
            pedidoAuxiliarBean.showCertificadoAnalisis = false;
            pedidoAuxiliarBean.cancelable = false;
            pedidoAuxiliarBean.canceladoView = false;
            pedidoAuxiliarBean.cantidadFormateada = 'CANTIDAD FORMATEADA';
            pedidoAuxiliarBean.numEntrega = '40000';
            
            //SECOND OBJECT
            
            RQO_cls_PedidoAuxiliarBean pedidoAuxiliarBeanToCompare = new RQO_cls_PedidoAuxiliarBean();
    
            pedidoAuxiliarBeanToCompare.idSolicitud = requestManagementList[0].Id;
            pedidoAuxiliarBeanToCompare.idAsset = assetList[0].Id;
            pedidoAuxiliarBeanToCompare.idGrado = gradeList[0].Id;
            pedidoAuxiliarBeanToCompare.idEnvase = containerList[0].Id;
            pedidoAuxiliarBeanToCompare.idDestinoMercancias = accountList[0].Id;
            pedidoAuxiliarBeanToCompare.nameSolicitud = requestManagementList[0].Name;
            pedidoAuxiliarBeanToCompare.cantidad = 200;
            pedidoAuxiliarBeanToCompare.destinoMercanciasNombre = accountList[0].Name;
            pedidoAuxiliarBeanToCompare.envaseName = containerList[0].Name;
            pedidoAuxiliarBeanToCompare.envaseTranslate = containerTranslationList[0].RQO_fld_translation__c;
            pedidoAuxiliarBeanToCompare.numRefCliente = '1234';
            pedidoAuxiliarBeanToCompare.statusCode = 'CODE_STATUS';
            pedidoAuxiliarBeanToCompare.statusDescription = 'STATUS_DESCRIPCION';
            pedidoAuxiliarBeanToCompare.statusCssClass = 'STATUS_CSS_CLASS';
            pedidoAuxiliarBeanToCompare.qp0gradeName = 'QP0_GRADENAME';
            pedidoAuxiliarBeanToCompare.numPedido = '100000';
            pedidoAuxiliarBeanToCompare.fechaSolicitado = '2019-12-02';
            pedidoAuxiliarBeanToCompare.fechaEntrega = '2012-03-21';
            pedidoAuxiliarBeanToCompare.fechaEstado = '2012-04-22';
            pedidoAuxiliarBeanToCompare.fechaCreacion = Date.valueOf('2018-05-23');
            pedidoAuxiliarBeanToCompare.fechaPreferente = Date.valueOf('2019-06-02');
            pedidoAuxiliarBeanToCompare.srcFechaPreferente = 'SOURCE';
            pedidoAuxiliarBeanToCompare.showAlbaranCMR = true;
            pedidoAuxiliarBeanToCompare.showCertificadoAnalisis = false;
            pedidoAuxiliarBeanToCompare.cancelable = false;
            pedidoAuxiliarBeanToCompare.canceladoView = false;
            pedidoAuxiliarBeanToCompare.cantidadFormateada = 'CANTIDAD FORMATEADA';
            pedidoAuxiliarBeanToCompare.numEntrega = '40000';
            
            pedidoAuxiliarBean.compareTo(pedidoAuxiliarBeanToCompare);
            pedidoAuxiliarBeanToCompare.compareTo(pedidoAuxiliarBean);
            
            pedidoAuxiliarBean.nameSolicitud = 'NOMBRE_MUY_LARGO_PARA_COMPARAR';
            pedidoAuxiliarBean.compareTo(pedidoAuxiliarBeanToCompare);
            
            System.assert(null != pedidoAuxiliarBean);
            
            Test.stopTest();
        }     
    }
}