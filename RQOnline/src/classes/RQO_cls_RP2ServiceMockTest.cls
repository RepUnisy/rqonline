/**
    *	@name: RQO_cls_SAPServicesMockTest
    *	@version: 1.0
    *	@creation date: 7/2/2018
    *	@author: Juan Elías - Unisys
    *	@description: Clase Mock para testear el método del servicio SOAP de RP2 que recupera las fichas de seguridad que se han modificado en un periodo de tiempo completo
    */

global class RQO_cls_RP2ServiceMockTest implements WebServiceMock {
    private boolean error;
    private String execType;
    private String execErrorNumberCode;
    
        /**
    * @creation date: 7/2/2018
    * @author: Juan Elías - Unisys
    * @description:	Constructor con parámetros de la clase 
    * @param: execType tipo String, tipo de ejecución (OK, NOK)
    * @param: execErrorNumberCode tipo String, tipo de error.
    */
    
    global RQO_cls_RP2ServiceMockTest(boolean error, String execType, String execErrorNumberCode){
        this.error = error;
        this.execType = execType;
        this.execErrorNumberCode = execErrorNumberCode;
    }

		/**
    * @creation date: 7/2/2018
    * @author: Juan Elías - Unisys
    * @description:	Definición del método doInvoke()
    */    
    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
        RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element customResponse = this.getFichasSeguridad();
        response.put('response_x', customResponse);
        
    }
    
    /**
    * @creation date: 7/2/2018
    * @author: Juan Elías - Unisys
    * @description:	Definición del método getFichasSeguridad()
    */    
    private RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element getFichasSeguridad(){
        RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element respuesta = new RQO_cls_GetDocumentumRP2.ZsapGetprodRlesaRqResponse_element();
        if (RQO_cls_Constantes.MOCK_EXEC_NOK.equalsIgnoreCase(this.execType)){
        }else{
            RQO_cls_GetDocumentumRP2.Zelog objItemZelog = new RQO_cls_GetDocumentumRP2.Zelog();
            RQO_cls_GetDocumentumRP2.ZtyteehsProdrlsa objItemZtyteehsProdrlsa = new RQO_cls_GetDocumentumRP2.ZtyteehsProdrlsa();
            List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa> itemZeehsProdrlsa = new List<RQO_cls_GetDocumentumRP2.ZeehsProdrlsa>();
            RQO_cls_GetDocumentumRP2.ZeehsProdrlsa objItemZeehsProdrlsa = new RQO_cls_GetDocumentumRP2.ZeehsProdrlsa();
            
            objItemZelog.Id = '';
            objItemZelog.Num = '';
            objItemZelog.Type_x = RQO_cls_Constantes.REQ_WS_RESPONSE_OK;
            objItemZelog.Msg = '';
            objItemZeehsProdrlsa.Empresa = '';
            objItemZeehsProdrlsa.Area = '';
            objItemZeehsProdrlsa.Seccion = '';
            objItemZeehsProdrlsa.Subid = 'test0';
            objItemZeehsProdrlsa.Tficha = RQO_cls_Constantes.DOC_TYPE_CODE_FICHA_SEGURIDAD;
            objItemZeehsProdrlsa.Idioma = '';
            if (error){
                objItemZeehsProdrlsa.Docid = 'cientouncaracteresparaquefallelainsecioncientouncaracteresparaquefallelainsecioncientouncaracteresparaqu';
            } else {
                objItemZeehsProdrlsa.Docid = '';
            }
            itemZeehsProdrlsa.add(objItemZeehsProdrlsa);
            objItemZtyteehsProdrlsa.item = itemZeehsProdrlsa;
            respuesta.Log = objItemZelog;
            respuesta.Productos = objItemZtyteehsProdrlsa;
        }
        return respuesta;
    }
    
    
}