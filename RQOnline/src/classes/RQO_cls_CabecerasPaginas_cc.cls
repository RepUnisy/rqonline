/**
 *	@name: RQO_cls_CabecerasPaginas_cc
 *	@version: 1.0
 *	@creation date: 23/10/2017
 *	@author: Nicolás García - Unisys
 *	@description: Clase que se utiliza para la obtención de las cabeceras de las páginas de la Community
 *	@testClass: RQO_cls_cabecerasPaginas_Test
*/
public class RQO_cls_CabecerasPaginas_cc {
    /**
	* @creation date: 23/10/2017
	* @author: Nicolás García - Unisys
	* @description:	Método para obtener la información de las cabeceras (imagen y texto) desde cualquier página que no sea "Grados"
	* @param: Nombre del registro de la custom setting que pertenece a la página que queremos obtener
    * @return: Lista de String con el texto a mostrar y la imagen a cargar
	* @exception: 
	* @throws: 
	*/
	@AuraEnabled
    public static List<String> getPageName(String nameCustomSetting){
        
      //  RQO_cs_cabecerasPaginas__c customSetting = new RQO_cs_cabecerasPaginas__c();
      //   customSetting = RQO_cs_cabecerasPaginas__c.getInstance(nameCustomSetting);
      //   
        RQO_cs_cabecerasPaginas__c customSetting = null;
        for(RQO_cs_cabecerasPaginas__c customSettingIter : [select RQO_fld_nombreCabecera__c, RQO_fld_imagenCabecera__c 
                                                        from RQO_cs_cabecerasPaginas__c 
                                                        where Name = : nameCustomSetting limit 1]){
                                                            customSetting = customSettingIter;
                                                        }     
        System.debug('customSetting: ' + nameCustomSetting + ' -- ' + customSetting);
        
        if(customSetting == null){
            nameCustomSetting = Label.RQO_lbl_default_const;
            customSetting = RQO_cs_cabecerasPaginas__c.getInstance(nameCustomSetting);
            System.debug('customSettingDefault: ' + customSetting);
        }
        
        List<String> valorRetorno = new List<String>();
        valorRetorno.add(customSetting.RQO_fld_nombreCabecera__c);
        valorRetorno.add(customSetting.RQO_fld_imagenCabecera__c);
        System.debug('Este es el valor a seguir: ' + valorRetorno);
        return valorRetorno;
    }
     /**
	* @creation date: 24/10/2017
	* @author: Nicolás García - Unisys
	* @description:	Método para obtener la información de las cabeceras (imagen, nombre del grado y categoría) desde la página "Grados"
	* @param: Nombre del registro de la custom setting que pertenece a la página que queremos obtener
	* @param: Id del grado del que queremos obtener la información
    * @return: Lista que contiene el nombre del grado, la categoría y la imagen a mostrar
	* @exception: 
	* @throws: 
	*/
    @AuraEnabled
    public static List<String> getPageNameGrade(String nameCustomSetting, Id grade, String language){
		List<RQO_obj_grade__c> gradoCompleto = [SELECT Id, Name, RQO_fld_product__r.Name FROM RQO_obj_grade__c WHERE Id = :grade LIMIT 1];
        List<RQO_obj_translation__c> listTranslation = [SELECT id, RQO_fld_translation__c, RQO_fld_locale__c from RQO_obj_translation__c where RQO_fld_grade__c in : gradoCompleto AND RQO_fld_locale__c = : language];
        RQO_cs_cabecerasPaginas__c customSetting = new RQO_cs_cabecerasPaginas__c();
        List<String> valorRetorno = new List<String>();
        customSetting = RQO_cs_cabecerasPaginas__c.getInstance(nameCustomSetting);
        System.debug('customSetting: ' + customSetting);
        if(customSetting == null){
            nameCustomSetting = Label.RQO_lbl_default_const;
            customSetting = RQO_cs_cabecerasPaginas__c.getInstance(nameCustomSetting);
            System.debug('customSetting: ' + customSetting);
        }
   //     valorRetorno.add(gradoCompleto.Name);
        valorRetorno.add(listTranslation[0].RQO_fld_translation__c);
        valorRetorno.add(customSetting.RQO_fld_imagenCabecera__c);
        valorRetorno.add(gradoCompleto[0].RQO_fld_product__r.Name);
        
        System.debug('Este es el valor a seguir: ' + valorRetorno);
        
        return valorRetorno;
    }
}