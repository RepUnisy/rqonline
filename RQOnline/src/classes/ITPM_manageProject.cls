/*------------------------------------------------------------------------
Author:         Borja Martín Ballesteros
Company:        Indra
Description:    class
History
<Date>          <Author>                      <Description>
13-Jul-2016     Borja Martín Ballesteros       Initial version
01-Sep-2016     Rubén Simarro Alcaide         Modificaciones en el metodo crearOportunidad para adaptarlo a los nuevos campos
02-Sep-2016     Rubén Simarro Alcaide         Se crea el metodo avanzarEstadoProjectExecution para saltar flujo de subestados
28-Sep-2016     Rubén Simarro Alcaide         Se modifica el metodo crearProyecto para quitar un if-else innecesario
------------------------------------------------------------------------------------*/
global class ITPM_manageProject{
    
    //Avanza los subestados del proyecto
    webservice static string avanzarEstado(string idProject) {
        try {            
            ITPM_Project__c project = [SELECT RecordTypeId, ITPM_Project_Phase__c, ITPM_Project_Status_Date__c, ITPM_REL_Budget_Investment__c 
                                   FROM ITPM_Project__c 
                                   WHERE Id =: idProject
                                   LIMIT 1];

            if(project.ITPM_Project_Phase__c == Label.ITPM_LABEL_PROJECT_EXECUTION || project.ITPM_Project_Phase__c == 'Project Execution with Project'){
               
                 if(project.ITPM_REL_Budget_Investment__c == NULL){
                    return 'You must complete the field Budget Investment. ';
                }
                else if(ComprobarUBC(idProject) != 'OK'){
                    
                    
                    return ComprobarUBC(idProject);
                }
                else{
                    RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name =: Label.ITPM_LABEL_HAND_OVER LIMIT 1];
                    project.ITPM_Project_Status_Date__c = System.Today();
                    project.RecordTypeId = rt.Id;
                }            
            }
            else if(project.ITPM_Project_Phase__c == Label.ITPM_LABEL_HAND_OVER){
            
                if(project.ITPM_REL_Budget_Investment__c == NULL){
                    return 'You must complete the field Budget Investment. ';
                }
                else{ 
                    RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name =: Label.ITPM_LABEL_PROJECT_CLOSURE LIMIT 1]; 
                    project.ITPM_Project_Status_Date__c = System.Today();
                    project.RecordTypeId = rt.Id;
                }
            }
            else if(project.ITPM_Project_Phase__c == Label.ITPM_LABEL_REJECTED){
                project.ITPM_Project_Status_Date__c = System.Today();
                return Label.ITPM_M_E_AVANZAR_ESTADO;
            }
            
            update(project);
            return 'true';
        } 
        catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
    // metodo para comprobar si las UBC estan bien rellenadas
       public static string ComprobarUBC(string idProject){
        String resultado='OK';
        List<ITPM_UBC_Project__c> listUBC = [SELECT Id, Name, ITPM_Percent__c, ITPM_SEL_Delivery_Country__c, ITPM_SEL_Year__c FROM ITPM_UBC_Project__c WHERE ITPM_REL_Project__c =: idProject ORDER BY ITPM_SEL_Delivery_Country__c, ITPM_SEL_Year__c];
        Decimal sumaPorcent = 0;
        boolean anioVacio = false;
        
        for (ITPM_UBC_Project__c ld : listUBC){
            sumaPorcent = ld.ITPM_Percent__c;
            for (ITPM_UBC_Project__c ld2 : listUBC){
                if(ld.ITPM_SEL_Delivery_Country__c == ld2.ITPM_SEL_Delivery_Country__c && ld.ITPM_SEL_Year__c == ld2.ITPM_SEL_Year__c && ld.Id != ld2.Id){
                    sumaPorcent += ld2.ITPM_Percent__c;
                }
                if(ld.ITPM_SEL_Year__c == null || ld.ITPM_SEL_Year__c == ''){
                    anioVacio = true;
                }
            }
            if (sumaPorcent != 100){
                resultado = 'Review the distribution by UBCs. Grouped per Year and Delivery Country, the porcentage must add 100%.';
            }
            if(anioVacio){
                resultado = 'You must report the year corresponding to the cost allocation to the UBC. ';
            }
        }
        
         if (sumaPorcent == 100){ resultado = 'OK';}
         else{
            resultado = 'Review the distribution by UBCs. Grouped per Year and Delivery Country, the porcentage must add 100%.';
        }
        return resultado;
     }
     
    //Metodo que avanza directamente al estado "Project Execution"
    webservice static string avanzarAestadoProjectExecution(string idProject) {
        List<ITPM_UBC_Project__c> listUBC = [SELECT Id, Name, ITPM_Percent__c, ITPM_SEL_Delivery_Country__c, ITPM_SEL_Year__c FROM ITPM_UBC_Project__c WHERE ITPM_REL_Project__c =: idProject ORDER BY ITPM_SEL_Delivery_Country__c, ITPM_SEL_Year__c];
        Decimal sumaPorcent = 0;
        boolean anioVacio = false;
        
        for (ITPM_UBC_Project__c ld : listUBC){
            sumaPorcent = ld.ITPM_Percent__c;
            for (ITPM_UBC_Project__c ld2 : listUBC){
                if(ld.ITPM_SEL_Delivery_Country__c == ld2.ITPM_SEL_Delivery_Country__c && ld.ITPM_SEL_Year__c == ld2.ITPM_SEL_Year__c && ld.Id != ld2.Id){
                    sumaPorcent += ld2.ITPM_Percent__c;
                }
                if(ld.ITPM_SEL_Year__c == null || ld.ITPM_SEL_Year__c == ''){
                    anioVacio = true;
                }
            }
            if (sumaPorcent != 100){
                return 'Review the distribution by UBCs. Grouped per Year and Delivery Country, the porcentage must add 100%.';
            }
            if(anioVacio){
                return 'You must report the year corresponding to the cost allocation to the UBC. ';
            }
        }
        
         if (sumaPorcent == 100){
            try {          
                //si entra al for, entrará una única vez. sinó entra, es porque el proyecto no está en fase "Draft"
                for(ITPM_Project__c project : [SELECT ITPM_Project_Phase__c , ITPM_Project_Status_Date__c
                                               FROM ITPM_Project__c  
                                               WHERE Id =: idProject AND ITPM_Project_Phase__c='Draft' LIMIT 1]){
                   
                                                   RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name =: Label.ITPM_LABEL_PROJECT_EXECUTION LIMIT 1];   
                                                   project.RecordTypeId = rt.Id;
                                                   project.ITPM_Project_Status_Date__c = System.Today();
                                                   update(project);
                                                   return 'true';
                                               } 
                return 'ERROR: Operation Cancelled. Project phase is not "Draft".';
            } 
            catch (Exception e) {         
                system.debug('@@@ '+e.getMessage());
                return 'ERROR: ' + e.getStackTraceString();
        }
        }else{
            return 'Review the distribution by UBCs. Grouped per Year and Delivery Country, the porcentage must add 100%.';
        }
    }
    
    //Cancela el proyecto
    webservice static string cancelarProyecto(string idProject, string reason) {
        try {            
            ITPM_Project__c project = [SELECT RecordTypeId, ITPM_Project_Phase__c, ITPM_Project_Status_Date__c
                                   FROM ITPM_Project__c 
                                   WHERE Id =: idProject
                                   LIMIT 1];
           
            RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Canceled' LIMIT 1]; 
            project.ITPM_Project_SEL_Canceled_Status__c = project.ITPM_Project_Phase__c;
            project.RecordTypeId = rt.Id;
            project.ITPM_Project_TXT_Cancel_reason__c = reason;
            project.ITPM_Project_Status_Date__c = System.Today();
            update(project);
            return 'true';
        } 
        catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }       
    }
    
    
    //Renueva el proyecto
    webservice static string renovarProyecto(string idProject){        
        try{
            ITPM_Project__c project = [SELECT RecordTypeId, ITPM_Project_SEL_Canceled_Status__c, ITPM_Project_Phase__c
                                   FROM ITPM_Project__c 
                                   WHERE Id =: idProject
                                   LIMIT 1];
            //system.debug('renovarProyecto: ' + project);        
            RecordType rt;    
            if(project.ITPM_Project_SEL_Canceled_Status__c == null || project.ITPM_Project_SEL_Canceled_Status__c == ''){
                if(project.ITPM_Project_SEL_Canceled_Status__c == 'Draft')
                    rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Draft' LIMIT 1]; 
                else if(project.ITPM_Project_SEL_Canceled_Status__c == 'Draft with Integration')
                    rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Draft with Integration' LIMIT 1];
            }else{            
                 if([SELECT Count() FROM ITPM_Milestone__c WHERE ITPM_REL_PROJECT__c =: idProject] > 0)
                    rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Project Execution with Project' LIMIT 1]; 
                 else
                    rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = :project.ITPM_Project_SEL_Canceled_Status__c LIMIT 1]; 
            }
                
            project.RecordTypeId = rt.Id;
            update(project);
            return 'true';
        } 
         catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
    
    //Renueva el proyecto rechazado
    webservice static string renovarProyectoDesdeRechazado(string idProject){
        try{
            ITPM_Project__c project = [SELECT RecordTypeId, ITPM_Project_SEL_Canceled_Status__c, ITPM_Project_Phase__c, ITPM_Project_Status_Date__c
                                   FROM ITPM_Project__c 
                                   WHERE Id =: idProject
                                   LIMIT 1];
            RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Draft' LIMIT 1]; 
            project.RecordTypeId = rt.Id;
            project.ITPM_Project_Status_Date__c = System.Today();
            project.ITPM_Project_TXT_Reject_reason__c = '';
            update(project);
            return 'true';
        } 
         catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
    
    //Rechaza el proyecto
    webservice static string rechazarProyecto(string idProject) {
        try {          
            ITPM_Project__c project = [SELECT RecordTypeId, ITPM_Project_Status_Date__c, ITPM_Project_TXT_Reject_reason__c
                                   FROM ITPM_Project__c 
                                   WHERE Id =: idProject
                                   LIMIT 1];
            
            if(project.ITPM_Project_TXT_Reject_reason__c==null)
                return 'You must fill Reject Reason.';
            
            RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Rejected' LIMIT 1];                   
            project.RecordTypeId = rt.Id;
            project.ITPM_Project_Status_Date__c = System.Today();
            update(project);
            return 'true';
        } 
        catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
  
    //Crea el proyecto  
    webservice static string crearProyecto(String idVP){
        List<ITPM_UBC_distribution__c> listDis = [SELECT Id, Name, ITPM_REL_UBC__c, ITPM_PER_Distribution__c, ITPM_SEL_Country__c FROM ITPM_UBC_distribution__c WHERE ITPM_REL_Conceptualization__c =: idVP];
        List<ITPM_Benefit__c> listBenefit = [SELECT Id, Name, ITPM_SEL_Benefit_Type__c, ITPM_SEL_Benefit_Classification__c, ITPM_DT_Start_Date__c, ITPM_rel_opportunity__c, ITPM_REL_Conceptualization__c, ITPM_rel_project__c, ITPM_NU_Benefit_Amount_Year_1__c, ITPM_NU_Benefit_Amount_Year_2__c, ITPM_NU_Benefit_Amount_Year_3__c, ITPM_NU_Benefit_Amount_Year_4__c, ITPM_NU_Benefit_Amount_Year_5__c, CurrencyIsoCode, ITPM_TX_Benefit_Description__c, ITPM_TX_Benefit_Justification__c FROM ITPM_Benefit__c WHERE ITPM_REL_Conceptualization__c =: idVP];
        try {             
            // Se comprueba que los UBCs sumen 100 por país introducido            
            Decimal sumaPorcent = 0;
            for (ITPM_UBC_distribution__c ld : listDis){
                sumaPorcent = ld.ITPM_PER_Distribution__c;
                for (ITPM_UBC_distribution__c ld2 : listDis){
                    if(ld.ITPM_SEL_Country__c == ld2.ITPM_SEL_Country__c && ld.Id != ld2.Id){
                        sumaPorcent += ld2.ITPM_PER_Distribution__c;
                    }
                }
                if (sumaPorcent != 100){
                    return 'The total sum of UBC percentage for every country must be 100.';
                }
            }
            // Si OK, creamos el hijo 
        
        
            RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Draft' LIMIT 1];               
            List<ITPM_Project__c> projReco = new List<ITPM_Project__c>();                
            projReco = [SELECT Id, RecordTypeId, ITPM_Project_REL_Conceptualization__c FROM ITPM_Project__c WHERE ITPM_Project_REL_Conceptualization__c =: idVP];                
            //if (projReco.size() == 0){                 
                ITPM_Conceptualization__c concept = [SELECT Id, ITPM_REL_portfolio__c, ITPM_Conceptu_REL_Program__c, ITPM_Conceptu_SEL_Execution_countries__c, ITPM_Conceptu_MSEL_Business_area__c, ITPM_Conceptu_MSEL_Business_subarea__c, ITPM_Conceptu_MSEL_Business_unit__c, ITPM_Conceptu_Total_budget_not_planned__c, ITPM_Conceptu_TX_Name_conceptualization__c, ITPM_Conceptu_TX_Description__c, ITPM_Conceptu_CUR_Expected_NPV__c, ITPM_Conceptu_REL_Budget_Investment__c,ITPM_Conceptu_REL_Business_Value__c FROM ITPM_Conceptualization__c WHERE Id =: idVP LIMIT 1];

                if(concept.ITPM_Conceptu_REL_Budget_Investment__c == NULL){
                    return 'You must complete the field Budget Investment. ';
                }
                    ITPM_Project__c ITPMPro = new ITPM_Project__c();
                    ITPMPro.RecordTypeId = rt.Id;
                    ITPMPro.ITPM_TX_Project_Name__c = concept.ITPM_Conceptu_TX_Name_conceptualization__c;
                    ITPMPro.ITPM_Project_TX_Description__c = concept.ITPM_Conceptu_TX_Description__c;
                    ITPMPro.ITPM_Project_CUR_Expected_NPV__c = concept.ITPM_Conceptu_CUR_Expected_NPV__c;
                    ITPMPro.ITPM_SEL_project_manager__c = concept.ITPM_Conceptu_REL_Business_Value__c;
                   // ITPMPro.ITPM_Project_BLN_New_service__c = concept.ITPM_Conceptu_BLN_New_service__c;                        
                   
                    ITPMPro.ITPM_Project_Total_budget_not_planned__c = concept.ITPM_Conceptu_Total_budget_not_planned__c;
                    ITPMPro.ITPM_REL_Budget_Investment__c = concept.ITPM_Conceptu_REL_Budget_Investment__c;                        
                    //ITPMPro.ITPM_Project_SEL_Executing_area__c = concept.ITPM_Conceptu_SEL_Executing_area__c;
                    //ITPMPro.ITPM_Project_SEL_Executing_subarea__c = concept.ITPM_Conceptu_SEL_Executing_subarea__c;
                    ITPMPro.ITPM_Project_REL_Conceptualization__c = concept.Id;    
                    ITPMPro.ITPM_Project_MSEL_Business_area__c = concept.ITPM_Conceptu_MSEL_Business_area__c;
                    ITPMPro.ITPM_Project_MSEL_Business_subare__c = concept.ITPM_Conceptu_MSEL_Business_subarea__c;
                    ITPMPro.ITPM_Project_MSEL_Business_unit__c = concept.ITPM_Conceptu_MSEL_Business_unit__c;
                    if(concept.ITPM_REL_Portfolio__c!=null) ITPMPro.ITPM_REL_Portfolio__c = concept.ITPM_REL_portfolio__c;
                    if(concept.ITPM_Conceptu_REL_Program__c!=null) ITPMPro.ITPM_REL_Program__c = concept.ITPM_Conceptu_REL_Program__c;
                    //ITPMPro.ITPM_SEL_Project_Planned_route__c = concept.ITPM_Conceptu_SEL_Planned_path__c;
                
                    insert (ITPMPro);
                    
                    for (ITPM_UBC_distribution__c ld : listDis){
                        ITPM_UBC_Project__c nuevoUBC = new ITPM_UBC_Project__c();
                        nuevoUBC.ITPM_Percent__c = ld.ITPM_PER_Distribution__c;
                        nuevoUBC.ITPM_REL_UBC__c = ld.ITPM_REL_UBC__c;
                        nuevoUBC.ITPM_SEL_Delivery_Country__c = ld.ITPM_SEL_Country__c;
                        nuevoUBC.ITPM_REL_Project__c = ITPMPro.Id;
                        insert(nuevoUBC);
                    }
                    for (ITPM_Benefit__c lb : listBenefit){
                        ITPM_Benefit__c nuevoBenefit = new ITPM_Benefit__c();
                        nuevoBenefit.Name = lb.Name;
                        nuevoBenefit.ITPM_SEL_Benefit_Type__c = lb.ITPM_SEL_Benefit_Type__c;
                        nuevoBenefit.ITPM_SEL_Benefit_Classification__c = lb.ITPM_SEL_Benefit_Classification__c;
                        nuevoBenefit.ITPM_DT_Start_Date__c = lb.ITPM_DT_Start_Date__c;
                        nuevoBenefit.ITPM_rel_project__c = ITPMPro.Id;
                        nuevoBenefit.ITPM_NU_Benefit_Amount_Year_1__c = lb.ITPM_NU_Benefit_Amount_Year_1__c;
                        nuevoBenefit.ITPM_NU_Benefit_Amount_Year_2__c = lb.ITPM_NU_Benefit_Amount_Year_2__c;
                        nuevoBenefit.ITPM_NU_Benefit_Amount_Year_3__c = lb.ITPM_NU_Benefit_Amount_Year_3__c;
                        nuevoBenefit.ITPM_NU_Benefit_Amount_Year_4__c = lb.ITPM_NU_Benefit_Amount_Year_4__c;
                        nuevoBenefit.ITPM_NU_Benefit_Amount_Year_5__c = lb.ITPM_NU_Benefit_Amount_Year_5__c;
                        nuevoBenefit.CurrencyIsoCode = lb.CurrencyIsoCode;
                        nuevoBenefit.ITPM_TX_Benefit_Description__c = lb.ITPM_TX_Benefit_Description__c;
                        nuevoBenefit.ITPM_TX_Benefit_Justification__c = lb.ITPM_TX_Benefit_Justification__c;
                        insert(nuevoBenefit);
                    }   
                return 'true';  
            /*}
            else{
                return Label.ITPM_M_E_PROYECTO_EXISTENTE;
            }*/
        }      
        catch (Exception e) {         
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }       
    }
    
    //Crear una Conceptualización a partir de de una Oportunidad
    webservice static string crearConceptualizacion(string idOpport) {
        try {        
            // Se comprueba que los UBCs sumen 100 por país introducido
            List<ITPM_UBC_distribution__c> listDis = [SELECT Id, Name, ITPM_REL_UBC__c, ITPM_PER_Distribution__c, ITPM_SEL_Country__c FROM ITPM_UBC_distribution__c WHERE ITPM_REL_Opportunity__c =: idOpport ORDER BY  ITPM_SEL_Country__c];
            Decimal sumaPorcent = 0;
            for (ITPM_UBC_distribution__c ld : listDis){
                sumaPorcent = ld.ITPM_PER_Distribution__c;
                for (ITPM_UBC_distribution__c ld2 : listDis){
                    if(ld.ITPM_SEL_Country__c == ld2.ITPM_SEL_Country__c && ld.Id != ld2.Id){
                        sumaPorcent += ld2.ITPM_PER_Distribution__c;
                    }
                }
                if (sumaPorcent != 100){
                    return 'The total sum of UBC percentage for every country must be 100.';
                }
            }
            // Si OK, creamos el hijo       
            ITPM_Conceptualization__c concept = new ITPM_Conceptualization__c();
            ITPM_Opportunity__c opport = [SELECT Id, ITPM_REL_portfolio__c, ITPM_Opportunity_REL_Program__c, ITPM_Opportunity_MSEL_Business_area__c, ITPM_Opportunity_MSEL_Business_subarea__c, ITPM_Opportunity_MSEL_Business_unit__c, ITPM_Opportunity_Total_budget_not_planne__c, ITPM_Opportunity_Investment_item_not_app__c, ITPM_Opportunity_TX_Description__c, ITPM_SEL_source__c, ITPM_Opportunity_Responsible_BV_Subarea__c, ITPM_SEL_Business_Value__c, ITPM_TX_Opportunity_Name__c, ITPM_Opportunity_SEL_Responsible_BV_Area__c, ITPM_Opportunity_TX_Labels__c,  ITPM_Opportunity_SEL_ITCB_line_strategy__c, ITPM_Opportunity_MSEL_Execution_countrie__c, ITPM_Opportunity_CU_Expected_NPV__c, ITPM_Opportunity_SEL_Expected_value__c, ITPM_Opportunity_SEL_Priority__c, ITPM_Opportunity_SEL_Segmentation__c FROM ITPM_Opportunity__c WHERE Id =: idOpport LIMIT 1];
            concept.ITPM_Conceptu_REL_Opportunity__c = opport.Id;
            concept.ITPM_Conceptu_TX_Description__c = opport.ITPM_Opportunity_TX_Description__c;
            concept.ITPM_Conceptu_SEL_Execution_countries__c = opport.ITPM_Opportunity_MSEL_Execution_countrie__c;
            concept.ITPM_Conceptu_SEL_Expected_value__c = opport.ITPM_Opportunity_SEL_Expected_value__c;
            concept.ITPM_Conceptu_CUR_Expected_NPV__c = opport.ITPM_Opportunity_CU_Expected_NPV__c;
            concept.ITPM_Conceptu_SEL_ITCB_line_strategy__c = opport.ITPM_Opportunity_SEL_ITCB_line_strategy__c;
            concept.ITPM_Conceptu_TX_Labels__c = opport.ITPM_Opportunity_TX_Labels__c;
            concept.ITPM_Conceptu_TX_Name_conceptualization__c = opport.ITPM_TX_Opportunity_Name__c;
            concept.ITPM_Conceptu_SEL_Priority__c = opport.ITPM_Opportunity_SEL_Priority__c;
            concept.ITPM_Conceptu_REL_Business_Value__c = opport.ITPM_SEL_Business_Value__c;
            concept.ITPM_Conceptu_SEL_Responsible_BV_Area__c = opport.ITPM_Opportunity_SEL_Responsible_BV_Area__c;
            concept.ITPM_Conceptu_SEL_Responsible_BV_Subarea__c = opport.ITPM_Opportunity_Responsible_BV_Subarea__c;
            concept.ITPM_Conceptu_SEL_Segmentation__c = opport.ITPM_Opportunity_SEL_Segmentation__c;
            concept.ITPM_Conceptu_SEL_Source__c = opport.ITPM_SEL_source__c;                
            
            concept.ITPM_Conceptu_Total_budget_not_planned__c = opport.ITPM_Opportunity_Total_budget_not_planne__c;     
            concept.ITPM_Conceptu_MSEL_Business_area__c = opport.ITPM_Opportunity_MSEL_Business_area__c;
            concept.ITPM_Conceptu_MSEL_Business_subarea__c = opport.ITPM_Opportunity_MSEL_Business_subarea__c;
            concept.ITPM_Conceptu_MSEL_Business_unit__c = opport.ITPM_Opportunity_MSEL_Business_unit__c;            
            if(opport.ITPM_REL_portfolio__c!=null)concept.ITPM_REL_portfolio__c = opport.ITPM_REL_portfolio__c;
            if(opport.ITPM_Opportunity_REL_Program__c!=null)concept.ITPM_Conceptu_REL_Program__c = opport.ITPM_Opportunity_REL_Program__c;
                                   
            insert(concept);
           
            for (ITPM_UBC_distribution__c ld : listDis){
                ITPM_UBC_distribution__c nuevaDist = new ITPM_UBC_distribution__c();
                nuevaDist.ITPM_PER_Distribution__c = ld.ITPM_PER_Distribution__c;
                nuevaDist.ITPM_REL_Conceptualization__c = concept.Id;
                nuevaDist.ITPM_REL_UBC__c = ld.ITPM_REL_UBC__c;
                insert(nuevaDist);
            }
            
            List<ITPM_Investment_Item__c> listII = [SELECT ITPM_CUR_Actual_Costs__c,ITPM_TX_Alternatives__c,ITPM_CUR_April_expense__c,ITPM_CUR_April_investment__c,ITPM_SEL_Area__c,ITPM_CUR_August_expense__c,ITPM_CUR_August_investment__c,ITPM_Invest_REL_BI_that_belongs__c,ITPM_II_REL_Conceptualization__c,ITPM_TX_Cost_Description__c,ITPM_SEL_Country__c,ITPM_Invest_SEL_Customer_country__c,ITPM_CUR_December_expense__c,ITPM_CUR_December_investment__c,ITPM_Invest_TXA_Description__c,ITPM_TX_Dispute_Reason__c,ITPM_Invest_DT_End_date__c,ITPM_DT_Estimate_Date__c,ITPM_CUR_Estimated_Cost__c,ITPM_Invest_SEL_Execution_contry__c,ITPM_Invest_NU_Expense__c,ITPM_CUR_February_expense__c,ITPM_CUR_February_investment__c,ITPM_Invest_NU_Investiment__c,ITPM_Invest_SEL_Investment_Category__c,ITPM_II_TX_Investment_Item_Name__c,ITPM_Invest_SEL_Investment_item_type__c,ITPM_CUR_January_expense__c,ITPM_CUR_January_investment__c,ITPM_CUR_July_expense__c,ITPM_CUR_July_investment__c,ITPM_CUR_June_expense__c,ITPM_CUR_June_investment__c,ITPM_CUR_March_Expense__c,ITPM_CUR_March_investment__c,ITPM_CUR_May_expense__c,ITPM_CUR_May_investment__c,ITPM_CUR_November_expense__c,ITPM_CUR_November_investment__c,ITPM_CUR_October_expense__c,ITPM_CUR_October_investment__c,ITPM_II_REL_Opportunity__c,ITPM_REL2_Project__c,ITPM_Invest_REL_Responsible__c,ITPM_Invest_SEL_Responsible_Area__c,ITPM_Invest_SEL_Responsible_IT_Direction__c,ITPM_Invest_SEL_Responsible_subare__c,ITPM_CUR_September_expense__c,ITPM_CUR_September_investment__c,ITPM_Invest_DT_Start_date__c,ITPM_FOR_Total_expense__c,ITPM_FOR_Total_investment__c,ITPM_Invest_TX_Version__c,ITPM_Invest_SEL_Year_validity__c
            //, ITPM_II_TX_Segmentation__c 
                                                    FROM ITPM_Investment_Item__c WHERE ITPM_II_REL_Opportunity__c =: idOpport];
            List<ITPM_Investment_Item__c> listIIInsertar = new List<ITPM_Investment_Item__c>();                                                     
            for (ITPM_Investment_Item__c ii : listII){
                ITPM_Investment_Item__c nuevoII = new ITPM_Investment_Item__c();                                                       
                //nuevoII.ITPM_II_TX_Segmentation__c = ii.ITPM_II_TX_Segmentation__c;
                nuevoII.ITPM_CUR_Actual_Costs__c = ii.ITPM_CUR_Actual_Costs__c;
                nuevoII.ITPM_TX_Alternatives__c = ii.ITPM_TX_Alternatives__c;
                nuevoII.ITPM_CUR_April_expense__c = ii.ITPM_CUR_April_expense__c;
                nuevoII.ITPM_CUR_April_investment__c = ii.ITPM_CUR_April_investment__c;
                nuevoII.ITPM_SEL_Area__c = ii.ITPM_SEL_Area__c;
                nuevoII.ITPM_CUR_August_expense__c = ii.ITPM_CUR_August_expense__c;
                nuevoII.ITPM_CUR_August_investment__c = ii.ITPM_CUR_August_investment__c;
                nuevoII.ITPM_Invest_REL_BI_that_belongs__c = ii.ITPM_Invest_REL_BI_that_belongs__c;
                nuevoII.ITPM_II_REL_Conceptualization__c = ii.ITPM_II_REL_Conceptualization__c;
                nuevoII.ITPM_TX_Cost_Description__c = ii.ITPM_TX_Cost_Description__c;
                nuevoII.ITPM_SEL_Country__c = ii.ITPM_SEL_Country__c;
                nuevoII.ITPM_Invest_SEL_Customer_country__c = ii.ITPM_Invest_SEL_Customer_country__c;
                nuevoII.ITPM_CUR_December_expense__c = ii.ITPM_CUR_December_expense__c;
                nuevoII.ITPM_CUR_December_investment__c = ii.ITPM_CUR_December_investment__c;
                nuevoII.ITPM_Invest_TXA_Description__c = ii.ITPM_Invest_TXA_Description__c;
                nuevoII.ITPM_TX_Dispute_Reason__c = ii.ITPM_TX_Dispute_Reason__c;
                nuevoII.ITPM_Invest_DT_End_date__c = ii.ITPM_Invest_DT_End_date__c;
                nuevoII.ITPM_DT_Estimate_Date__c = ii.ITPM_DT_Estimate_Date__c;
                nuevoII.ITPM_CUR_Estimated_Cost__c = ii.ITPM_CUR_Estimated_Cost__c;
                nuevoII.ITPM_Invest_SEL_Execution_contry__c = ii.ITPM_Invest_SEL_Execution_contry__c;
                nuevoII.ITPM_Invest_NU_Expense__c = ii.ITPM_Invest_NU_Expense__c;
                nuevoII.ITPM_CUR_February_expense__c = ii.ITPM_CUR_February_expense__c;
                nuevoII.ITPM_CUR_February_investment__c = ii.ITPM_CUR_February_investment__c;
                nuevoII.ITPM_Invest_NU_Investiment__c = ii.ITPM_Invest_NU_Investiment__c;
                nuevoII.ITPM_Invest_SEL_Investment_Category__c = ii.ITPM_Invest_SEL_Investment_Category__c;
                nuevoII.ITPM_II_TX_Investment_Item_Name__c = ii.ITPM_II_TX_Investment_Item_Name__c;
                nuevoII.ITPM_Invest_SEL_Investment_item_type__c = ii.ITPM_Invest_SEL_Investment_item_type__c;
                nuevoII.ITPM_CUR_January_expense__c = ii.ITPM_CUR_January_expense__c;
                nuevoII.ITPM_CUR_January_investment__c = ii.ITPM_CUR_January_investment__c;
                nuevoII.ITPM_CUR_July_expense__c = ii.ITPM_CUR_July_expense__c;
                nuevoII.ITPM_CUR_July_investment__c = ii.ITPM_CUR_July_investment__c;
                nuevoII.ITPM_CUR_June_expense__c = ii.ITPM_CUR_June_expense__c;
                nuevoII.ITPM_CUR_June_investment__c = ii.ITPM_CUR_June_investment__c;
                nuevoII.ITPM_CUR_March_Expense__c = ii.ITPM_CUR_March_Expense__c;
                nuevoII.ITPM_CUR_March_investment__c = ii.ITPM_CUR_March_investment__c;
                nuevoII.ITPM_CUR_May_expense__c = ii.ITPM_CUR_May_expense__c;
                nuevoII.ITPM_CUR_May_investment__c = ii.ITPM_CUR_May_investment__c;
                nuevoII.ITPM_CUR_November_expense__c = ii.ITPM_CUR_November_expense__c;
                nuevoII.ITPM_CUR_November_investment__c = ii.ITPM_CUR_November_investment__c;
                nuevoII.ITPM_CUR_October_expense__c = ii.ITPM_CUR_October_expense__c;
                nuevoII.ITPM_CUR_October_investment__c = ii.ITPM_CUR_October_investment__c;
                nuevoII.ITPM_II_REL_Opportunity__c = ii.ITPM_II_REL_Opportunity__c;
                nuevoII.ITPM_REL2_Project__c = ii.ITPM_REL2_Project__c;
                nuevoII.ITPM_Invest_REL_Responsible__c = ii.ITPM_Invest_REL_Responsible__c;
                nuevoII.ITPM_Invest_SEL_Responsible_Area__c = ii.ITPM_Invest_SEL_Responsible_Area__c;
                nuevoII.ITPM_Invest_SEL_Responsible_IT_Direction__c = ii.ITPM_Invest_SEL_Responsible_IT_Direction__c;
                nuevoII.ITPM_Invest_SEL_Responsible_subare__c = ii.ITPM_Invest_SEL_Responsible_subare__c;
                nuevoII.ITPM_CUR_September_expense__c = ii.ITPM_CUR_September_expense__c;
                nuevoII.ITPM_CUR_September_investment__c = ii.ITPM_CUR_September_investment__c;
                nuevoII.ITPM_Invest_DT_Start_date__c = ii.ITPM_Invest_DT_Start_date__c;
                nuevoII.ITPM_Invest_TX_Version__c = ii.ITPM_Invest_TX_Version__c;
                nuevoII.ITPM_Invest_SEL_Year_validity__c = ii.ITPM_Invest_SEL_Year_validity__c;                    
                listIIInsertar.add(nuevoII);
            }
            if(listIIInsertar!=null && listIIInsertar.size()>0) insert listIIInsertar;
            return 'true';
        }
        catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
    
    //Crea la oportunidad a partir de una Idea
    webservice static string crearOportunidad(String idIdea){
        try {                  
            ITPM_Idea__c ideaOpo = [SELECT Id, Name, ITPM_SEL_Portfolio__c, ITPM_Idea_REL_Program__c, ITPM_TX_Initiative_Name__c, ITPM_SEL_Business_Value__c, ITPM_Idea_SEL_Strategic_Line_ITCB__c, ITPM_Idea_SEL_Responsible_BV_Area__c, ITPM_Idea_SEL_Responsible_BV_SubArea__c, ITPM_Idea_TX_Description__c, ITPM_Idea_TX_Labels__c, ITPM_Idea_MSEL_Business_area__c, ITPM_Idea_MSEL_Business_subarea__c, ITPM_Idea_MSEL_Business_unit__c, ITPM_Idea_SEL_source__c FROM ITPM_Idea__c WHERE Id =: idIdea LIMIT 1];
           
            ITPM_Opportunity__c OporITPM = new ITPM_Opportunity__c();   
            OporITPM.ITPM_Idea__c = ideaOpo.Id; 
            OporITPM.ITPM_TX_Opportunity_Name__c=ideaOpo.ITPM_TX_Initiative_Name__c; 
            OporITPM.ITPM_SEL_Business_Value__c = ideaOpo.ITPM_SEL_Business_Value__c;
            OporITPM.ITPM_Opportunity_SEL_Responsible_BV_Area__c = ideaOpo.ITPM_Idea_SEL_Responsible_BV_Area__c;
            OporITPM.ITPM_Opportunity_Responsible_BV_Subarea__c = ideaOpo.ITPM_Idea_SEL_Responsible_BV_SubArea__c;
            OporITPM.ITPM_Opportunity_TX_Description__c = ideaOpo.ITPM_Idea_TX_Description__c;
            OporITPM.ITPM_Opportunity_TX_Labels__c = ideaOpo.ITPM_Idea_TX_Labels__c;
            OporITPM.ITPM_Opportunity_SEL_ITCB_line_strategy__c = ideaOpo.ITPM_Idea_SEL_Strategic_Line_ITCB__c;
            OporITPM.ITPM_Opportunity_MSEL_Business_area__c = ideaOpo.ITPM_Idea_MSEL_Business_area__c;
            OporITPM.ITPM_Opportunity_MSEL_Business_subarea__c = ideaOpo.ITPM_Idea_MSEL_Business_subarea__c;
            OporITPM.ITPM_Opportunity_MSEL_Business_unit__c = ideaOpo.ITPM_Idea_MSEL_Business_unit__c;
            OporITPM.ITPM_SEL_source__c = ideaOpo.ITPM_Idea_SEL_source__c;
            if(ideaOpo.ITPM_SEL_Portfolio__c!=null)OporITPM.ITPM_REL_portfolio__c = ideaOpo.ITPM_SEL_Portfolio__c;
            if(ideaOpo.ITPM_Idea_REL_Program__c!=null)OporITPM.ITPM_Opportunity_REL_Program__c = ideaOpo.ITPM_Idea_REL_Program__c;
          
            insert (OporITPM);    
           
            return 'true';      
        } 
         catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
    
    //Retrada el subestado
    webservice static string retrasarSubestado(string idProject) {
        try {            
            ITPM_Project__c project = [SELECT RecordTypeId, ITPM_Project_Phase__c
                                   FROM ITPM_Project__c 
                                   WHERE Id =: idProject
                                   LIMIT 1];

            if(project.ITPM_Project_Phase__c == 'Draft'){
                RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Draft' LIMIT 1];              
                project.RecordTypeId = rt.Id;
            }else{ 
                 return Label.ITPM_M_E_RETRASAR_SUBESTADO;
            }
            
            update(project);
            return 'true';
        } 
         catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
    
    //back to Draft status
    webservice static string toProjectProposal(string idProject) {
        try {            
            ITPM_Project__c project = [SELECT RecordTypeId, ITPM_REL_Program__r.Name, ITPM_Project_Phase__c, ITPM_Project_Status_Date__c,
                                        ITPM_REL_Budget_Investment__c 
                                   FROM ITPM_Project__c 
                                   WHERE Id =: idProject
                                   LIMIT 1];

            RecordType rt;
            if(project.ITPM_REL_Program__c!=null && project.ITPM_REL_Program__r.Name == 'Integration')
                rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Draft with Integration' LIMIT 1];              
            else                
                rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Draft' LIMIT 1];              
            
            project.RecordTypeId = rt.Id;
            project.ITPM_Project_Status_Date__c = System.Today();
            update(project);
            return 'true';
        } 
         catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
    
    //back to Project Execution status
    webservice static string toProjectExecution(string idProject) {
        try {            
            ITPM_Project__c project = [SELECT RecordTypeId, ITPM_REL_Program__r.Name, ITPM_Project_Phase__c, ITPM_Project_Status_Date__c,
                                        ITPM_REL_Budget_Investment__c 
                                   FROM ITPM_Project__c 
                                   WHERE Id =: idProject
                                   LIMIT 1];
            
            boolean hayMilestoneCargado = false;
            List<ITPM_Milestone__c> listMilestone = [SELECT Id FROM ITPM_Milestone__c WHERE ITPM_REL_PROJECT__c =: idProject];
            if(listMilestone!=null && listMilestone.size()>0){
                hayMilestoneCargado = true;
            }
                               
            RecordType rt;
            if(hayMilestoneCargado){
                if(project.ITPM_REL_Program__c!=null && project.ITPM_REL_Program__r.Name == 'Integration')
                    rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Project Execution with Project with Integration' LIMIT 1];
                else
                    rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Project Execution with Project' LIMIT 1];
            }else{
                if(project.ITPM_REL_Program__c!=null && project.ITPM_REL_Program__r.Name == 'Integration')               
                    rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Project Execution with Integration' LIMIT 1];              
                else
                    rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Project Execution' LIMIT 1];              
            }
           if(project.ITPM_REL_Budget_Investment__c == NULL){
                    return 'You must complete the field Budget Investment. ';
                }
           else{
           
            project.RecordTypeId = rt.Id;
            project.ITPM_Project_Status_Date__c = System.Today();
            update(project);

            return 'true';
            }
        } 
         catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
        
    //back to Hand Over status
    webservice static string toHandOver(string idProject) {
        try {            
            ITPM_Project__c project = [SELECT RecordTypeId, ITPM_REL_Program__r.Name, ITPM_Project_Phase__c,
                                        ITPM_Project_Status_Date__c, ITPM_REL_Budget_Investment__c 
                                   FROM ITPM_Project__c 
                                   WHERE Id =: idProject
                                   LIMIT 1];

            RecordType rt;
            if(project.ITPM_REL_Program__c!=null && project.ITPM_REL_Program__r.Name == 'Integration')
                rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Hand Over with Integration' LIMIT 1];              
            else                
                rt = [SELECT Id FROM RecordType WHERE SObjectType ='ITPM_Project__c' AND Name = 'Hand Over' LIMIT 1];              
            
            if(project.ITPM_REL_Budget_Investment__c == NULL){
                    return 'You must complete the field Budget Investment. ';
                }
           else{
                project.RecordTypeId = rt.Id;
                project.ITPM_Project_Status_Date__c = System.Today();
                update(project);
                return 'true';
               }
        } 
         catch (Exception e) {
            system.debug('@@@ '+e.getMessage());
            return 'ERROR: ' + e.getStackTraceString();
        }
    }
}