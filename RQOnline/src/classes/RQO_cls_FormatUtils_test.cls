@IsTest
public class RQO_cls_FormatUtils_test {
    
    /**
*	@name: RQO_cls_UploadDocument_test
*	@version: 1.0
*	@creation date: 12/2/2018
*	@author: Juan Elías - Unisys
*	@description: Clase de test para probar los métodos asociados a FormatUtils
*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        
    }
    
     /**
* @creation date: 22/02/2018
* @author: Juan Elías - Unisys
* @description:	Método test para probar el método ObtenerFechaYYYYMMDD
* @exception: 
* @throws: 
*/
    @isTest
    static void testObtenerFechaYYYYMMDD(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            Date esperado;
            String strFecha = '20170101';
            esperado = Date.newInstance(Integer.valueOf(2017), Integer.valueOf(01), Integer.valueOf(01));
            test.startTest();
            Date resultado = RQO_cls_FormatUtils.obtenerFechaYYYYMMDD(strFecha);
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
        /**
* @creation date: 22/02/2018
* @author: Juan Elías - Unisys
* @description:	Método test para probar el método StringToDecimal
* @exception: 
* @throws: 
*/
    @isTest
    static void testStringToDecimal(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            Decimal esperado;
            String num;
            test.startTest();
            Decimal resultado = RQO_cls_FormatUtils.stringToDecimal(num);
            System.assertEquals(esperado, resultado);
            test.stopTest();
        }
    }
    
    /**
* @creation date: 22/02/2018
* @author: Juan Elías - Unisys
* @description:	Método test para probar el método LoadPicklistDocLocale
* @exception: 
* @throws: 
*/
    @isTest
    static void testLoadPicklistDocLocale(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String esperado;
            Decimal rA;
            test.startTest();
            String resultado = RQO_cls_FormatUtils.formatDecimal(rA);
            System.assertEquals(esperado, resultado);            
            test.stopTest();
        }
    }
    
}