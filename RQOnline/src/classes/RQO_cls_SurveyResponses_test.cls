/**
*   @name: RQO_cls_SurveyResponses_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_SurveyResponses.
*/
@isTest
public class RQO_cls_SurveyResponses_test {
	
    /**
    *   @name: RQO_cls_SurveyResponses_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la actualización del idioma de un usuario.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    private static testMethod void whenAddAnonymousResponseThrowsException() {

        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserClienteRunAsJob@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(userRunAs){
            Boolean DidThrowException = false;
            
            try {
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserCliente@testorg.com', 'Customer Community User', null, 'es',
                                                       'es_ES', 'Europe/Berlin', contact[0].Id);
                                                       
                Id surveyQuestionId = fflib_IDGenerator.generate(RQO_obj_surveyQuestion__c.SObjectType);
                Id questionResponseId = fflib_IDGenerator.generate(RQO_obj_questionResponse__c.SObjectType);
                
                List<RQO_obj_surveyResponse__c> surveyResponses = new List<RQO_obj_surveyResponse__c>();
                RQO_obj_surveyResponse__c surveyResponse = new RQO_obj_surveyResponse__c(RQO_fld_user__c = u.Id,
                                                                                         RQO_fld_response__c = questionResponseId,
                                                                                         RQO_fld_question__c = surveyQuestionId);
                surveyResponses.add(surveyResponse);
    
                test.startTest();
                Test.setMock(HttpCalloutMock.class, new RQO_cls_SurveyResponses_test.MockHttpResponseGenerator());
                ((RQO_cls_SurveyResponses) new RQO_cls_SurveyResponses.Constructor().construct(surveyResponses)).addAnonymousResponse();
                test.stopTest();
            }
            catch (Exception e) {
                DidThrowException = true;
            }
            
            System.assertEquals(true,
                                DidThrowException, 
                                'No se ha generado una excepción.');
        }
    }
    
    /**
    *   @name: RQO_cls_SurveyResponses_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método que mockea una llamada HTTP
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	public class MockHttpResponseGenerator implements HttpCalloutMock {
	    // Implement this interface method
	    public HTTPResponse respond(HTTPRequest req) {
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody('{' + 
							'"id":"https://login.salesforce.com/id/00D50000000IZ3ZEAW/00550000001fg5OAAQ",' +
							'"issued_at":"1296509381665",' +
							'"instance_url":"https://na1.salesforce.com",' +
							'"signature":"+Nbl5EOl/DlsvUZ4NbGDno6vn935XsWGVbwoKyXHayo=",' +
							'"access_token":"00D50000000IZ3Z!AQgAQH0Yd9M51BU_rayzAdmZ6NmT3pXZBgzkc3JTwDOGBl8BP2AREOiZzL_A2zg7etH81kTuuQPljJVsX4CPt3naL7qustlb"' +
						'}');
	        res.setStatusCode(200);
	        return res;
	    }
	}
}