/**
 *	@name: RQO_cls_MailNotificacionResumen_batch
 *	@version: 1.0
 *	@creation date: 01/02/2018
 *	@author: David Iglesias - Unisys
 *	@description: Clase para el envio de mails de notificaciones de resumen diario
 */
global class RQO_cls_MailNotificacionResumen_batch extends RQO_cls_ProcesoPlanificado implements Database.Batchable<MailResumenNotificationBean>, Database.Stateful {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_MailNotificacionResumen_batch';
	private static final String JOB_NAME = 'MailNotificacionResumen';
	
	// ***** VARIABLES ***** //
    private List<RQO_obj_notification__c> notificationList = null;
	private List<RQO_obj_logProcesos__c> logProcesosList = null;
	private Map<Id, String> contactIdMap = null;
	private Map<String, String> posPedidoIdMap = null;
	private Map<String, String> entregaIdAuxMap = null;
	
	// ***** CONSTRUCTORES ***** //
    public RQO_cls_MailNotificacionResumen_batch (List<RQO_obj_notification__c> notificationList, Map<Id, String> contactIdMap, Map<String, String> posPedidoIdMap, Map<String, String> entregaIdAuxMap) {
		super(JOB_NAME);
        this.logProcesosList = new List<RQO_obj_logProcesos__c> ();
		this.notificationList = notificationList;
		this.contactIdMap = contactIdMap;
		this.posPedidoIdMap = posPedidoIdMap;
		this.entregaIdAuxMap = entregaIdAuxMap;
    }
	
	// ***** METODO GLOBALES ***** //

	/**
	* @creation date: 01/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Realiza el envio de mails de notificaciones diarias a los contactos
	* @param: Database.BatchableContext
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
    public Iterable<MailResumenNotificationBean> start(Database.BatchableContext BC) {
		
		/**
        * Constantes
        */
        final String METHOD = 'start';
		
		/**
		 * Variables
		 */
		List<RQO_obj_notification__c> notificationAuxList = null;
		List<RQO_obj_notification__c> notificationConfirmadoList = null;
		List<RQO_obj_notification__c> notificationCursoList = null;
		List<MailResumenNotificationBean> mailResumenNotificationBeanList = new List<MailResumenNotificationBean> ();
		Map<String, List<RQO_obj_notification__c>> notificationByType = new Map<String, List<RQO_obj_notification__c>> ();
		MailResumenNotificationBean mailResumenBean = null;
		
		/**
		 * Inicio
		 */
		System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		// Comprobación si se encuentra en dia/horas de lanzamiento
		if (isRunnable()) {			
			// Ordenamiento de las notificaciones por contacto y tipo de notificación
			for (RQO_obj_notification__c item : this.notificationList) {
			
				if (notificationByType.get(item.RQO_fld_contact__c) != null) {
					//notificationAuxList = notificationByType.get(item.RQO_fld_contact__c + item.RQO_fld_messageType__c);
					notificationAuxList = notificationByType.get(item.RQO_fld_contact__c);
				} else {
					notificationAuxList = new List<RQO_obj_notification__c> ();
				}
				notificationAuxList.add(item);
				//notificationByType.put(item.RQO_fld_contact__c + item.RQO_fld_messageType__c, notificationAuxList);
				notificationByType.put(item.RQO_fld_contact__c, notificationAuxList);
			}
			
			// Generación de los objetos con el ordenamiento general ( contacto - tipo de notificación - notificación)
			for (String item : notificationByType.keySet()) {
				
				mailResumenBean = new MailResumenNotificationBean();
				notificationConfirmadoList = new List<RQO_obj_notification__c> ();
				notificationCursoList = new List<RQO_obj_notification__c> ();
				
				// Obtención del Id del contacto a tratar y del coódigo de la comunicación
				// La clave se compone del id del contacto restando las dos ultimas posiciones (código de la comunicación)
				//String idContactAux = item.clave.substring(0, item.clave.length()-2);
				//String notificationCode = item.clave.substring(item.clave.length()-2);
				
				mailResumenBean.clave = item;
				
				for (RQO_obj_notification__c itemAux : notificationByType.get(item)) {
					if (RQO_cls_Constantes.COD_PED_TRATAMIENTO.equalsIgnoreCase(itemAux.RQO_fld_messageType__c)) {
						//mailResumenBean.notificationCursoList = notificationByType.get(item);
						notificationCursoList.add(itemAux);
					} else if (RQO_cls_Constantes.COD_PED_CONFIRMADO.equalsIgnoreCase(itemAux.RQO_fld_messageType__c)) {
						//mailResumenBean.notificationConfirmadoList = notificationByType.get(item);
						notificationConfirmadoList.add(itemAux);
					}
				}
				
				mailResumenBean.notificationCursoList = notificationCursoList;
				mailResumenBean.notificationConfirmadoList = notificationConfirmadoList;
				
				// Añadido a la lista de retorno para el execute
				mailResumenNotificationBeanList.add(mailResumenBean);
			}
			
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN'); 
		
        return mailResumenNotificationBeanList;
		
    }

	/**
	* @creation date: 12/12/2017
	* @author: David Iglesias - Unisys
	* @description:	Ejecucion por tramas del envio de mails de notificaciones diarias
	* @param: Database.BatchableContext
	* @param: List<MailResumenNotificationBean>
	* @return: 	
	* @exception: 
	* @throws: 
	*/
    public void execute(Database.BatchableContext info, List<MailResumenNotificationBean> scope) {
        
		/**
        * Constantes
        */
        final String METHOD = 'execute';
	
		/**
		 * Variables
		 */
        String strBody = '';
		EmailTemplate emailTemplate = null;
		RQO_obj_logProcesos__c logProcesos = null;
		Messaging.SingleEmailMessage mail = null;
        Messaging.SendEmailResult[] resultMail = null;
		List<Messaging.Email> mailList = new List<Messaging.Email> ();
        Set<String> languajeTemplateSet = new Set<String> ();
		List<RQO_obj_notification__c> notificationUpdateList = new List<RQO_obj_notification__c> ();
		Map<String, String> posPedidoDatosMap = null;
		Map<String, String> entregaDatosMap = null;
		Map<String, EmailTemplate> emailTemplateByNameMap = null;

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		try {
		
			// Obtención de Email Templates por idioma
			emailTemplateByNameMap = getTemplateByContact(this.contactIdMap);	
			
			for (MailResumenNotificationBean item : scope) {
				
				String templateName = '';
				
				// Obtención del Id del contacto a tratar y del coódigo de la comunicación
				//String idContactAux = item.clave.substring(0, item.clave.length()-2);
				//String notificationCode = item.clave.substring(item.clave.length()-2);
				
				// Mapeo de códigos del idioma del contacto con el mail template
				String languageTemplate = RQO_cls_Constantes.languageMap.get(this.contactIdMap.get(item.clave));
				
				// Generación del mail custom a enviar
				mail = new Messaging.SingleEmailMessage();
			
				// Email Address
				mail.setToAddresses(new List<String> {item.clave});
				
				/*
				// Obtención del email template específico a tratar
				if (RQO_cls_Constantes.COD_PED_TRATAMIENTO.equalsIgnoreCase(notificationCode)) {
					emailTemplate = emailTemplateByNameMap.get(RQO_cls_Constantes.MAIL_TMP_TRATAMIENTO_RESUMEN + languageTemplate);
					// Obtención de los datos para los pedidos
					posPedidoDatosMap = getPosicionPedidoFields(item.notificationList);
					
				} else if (RQO_cls_Constantes.COD_PED_CONFIRMADO.equalsIgnoreCase(notificationCode)) {
					emailTemplate = emailTemplateByNameMap.get(RQO_cls_Constantes.MAIL_TMP_ESTADO_RESUMEN + languageTemplate);
					// Obtención de los datos para las entregas
					entregaDatosMap = getEntregaFields(item.notificationList);
					
				}
				*/
				
				// Obtención del email template específico a tratar
				emailTemplate = emailTemplateByNameMap.get(RQO_cls_Constantes.MAIL_TMP_RESUMEN_DIARIO + languageTemplate);

				// Obtención de los datos para los pedidos
				posPedidoDatosMap = getPosicionPedidoFields(item.notificationCursoList);
				
				// Obtención de los datos para las entregas
				entregaDatosMap = getEntregaFields(item.notificationConfirmadoList);
				
				// Subject
				mail.setSubject(emailTemplate.Subject);
					
				// Body
				strBody = RQO_cls_MailUtils.fillHtmlBodyResume(emailTemplate.HtmlValue, item.notificationCursoList, item.notificationConfirmadoList, posPedidoDatosMap, entregaDatosMap, languageTemplate);	
				mail.setHtmlBody(strBody);
				
				// Listas con las notificaciones para realizar actualización posterior
				notificationUpdateList = updateNotification(item.notificationCursoList, item.notificationConfirmadoList);
				
			}

			// Send email
			resultMail = Messaging.sendEmail(new Messaging.Email[] { mail });
			
			// Comprobación de resultado del envío de mails
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Comprobación de resultado del envío de mails');
			for (Messaging.SendEmailResult item : resultMail) {
				if (!item.IsSuccess()) {
					Messaging.SendEmailError[] errArr = item.getErrors(); 
					for (Messaging.SendEmailError itemAux : errArr) {
						logProcesos = RQO_cls_ProcessLog.instanceInitalLogBatch(String.valueOf(RQO_obj_notification__c.getSobjectType()), String.valueOf(itemAux.getStatusCode()), 
																					Label.RQO_lbl_msgErrorNotificacionResumeInt+itemAux.getTargetObjectId(), itemAux.getMessage(), 
																					itemAux.getTargetObjectId(), RQO_cls_Constantes.SERVICE_SEND_NOTIFICATION);
						logProcesosList.add(logProcesos);
					}	
				}
			}
			
			// Actualización de registro de notificación
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Actualización de registro de notificación');
			update notificationUpdateList;

		} catch (Exception e) {
			System.debug('Excepción generica: '+e.getTypeName()+' - '+e.getMessage());
			logProcesos = RQO_cls_ProcessLog.instanceInitalLogBatch(String.valueOf(RQO_obj_notification__c.getSobjectType()), e.getTypeName(), 
																		Label.RQO_lbl_mensajeErrorExceptionGenericoIntegracion, e.getMessage(), null, 
																		RQO_cls_Constantes.SERVICE_SEND_NOTIFICATION);
			logProcesosList.add(logProcesos);
		}
		
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN'); 
		
    }

    public void finish(Database.BatchableContext info) {
		
		/**
        * Constantes
        */
        final String METHOD = 'finish';
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		// Inserción del log de errores
		if (!logProcesosList.isEmpty()) {
			insert logProcesosList;
		}
				
		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
		
	}
	
	// ***** METODO PRIVADOS ***** //
	
    /**
    * @creation date: 02/02/2018
    * @author: David Iglesias - Unisys
    * @description: Consultas para obtener el mapa con el binomio Template Name - Template
	* @param: externalIdByTypeObjectMap			Mapa de contactos con con su respectivo idioma
    * @return: Map<String, String> 				Mapa de retorno
    * @exception: 
    * @throws: 
    */
	private static Map<String, EmailTemplate> getTemplateByContact (Map<Id, String> contactMap) {
		
		/**
        * Constantes
        */
        final String METHOD = 'getTemplateByContact';
	
		/**
		 * Variables
		 */
		Map<String, EmailTemplate> emailTemplateByNameMap = new Map<String, EmailTemplate> ();
        Set<String> languajeTemplateSet = new Set<String> ();

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		System.debug('contactMap.values(): ' +contactMap.values());
		// Idioma de los templates a obtener
		for (String item : contactMap.values()) {
			//languajeTemplateSet.add(RQO_cls_Constantes.MAIL_TMP_TRATAMIENTO_RESUMEN + item);
			//languajeTemplateSet.add(RQO_cls_Constantes.MAIL_TMP_ESTADO_RESUMEN + item);
			languajeTemplateSet.add(RQO_cls_Constantes.MAIL_TMP_RESUMEN_DIARIO + RQO_cls_Constantes.languageMap.get(item));
		}
		System.debug('languajeTemplateSet: ' +languajeTemplateSet);
		// Obtención de Email Templates a tratar
		for (EmailTemplate item : [SELECT Name, Subject, HtmlValue FROM EmailTemplate WHERE Name IN :languajeTemplateSet]) {
			emailTemplateByNameMap.put(item.Name, item);
		}

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN'); 
		
		return emailTemplateByNameMap;
	}
	
	/**
    * @creation date: 02/02/2018
    * @author: David Iglesias - Unisys
    * @description: Consultas para obtener el mapa con el binomio Id externo - Objeto
	* @param: notificationAuxList			Lista de notificaciones donde se especifica el id externo del objeto a buscar
    * @return: Map<String, String> 			Mapa de retorno
    * @exception: 
    * @throws: 
    */
	private static Map<String, String> getPosicionPedidoFields (List<RQO_obj_notification__c> notificationAuxList) {
		
		/**
        * Constantes
        */
        final String METHOD = 'getPosicionPedidoFields';
	
		/**
		 * Variables
		 */
		Map<String, String> posPedidoDatosMap = new Map<String, String> ();
		Set<String> idExternoSet = new Set<String> ();

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		for (RQO_obj_notification__c item : notificationAuxList) {
			if (item.RQO_fld_objectType__c.equalsIgnoreCase(RQO_cls_Constantes.COD_SAP_NOT_PEDIDO)) {
				idExternoSet.add(item.RQO_fld_externalId__c);
			}
		}
		
		for (Asset posPedido : [SELECT Id, RQO_fld_idExterno__c, RQO_fld_codigoPedido__c FROM Asset WHERE RQO_fld_idExterno__c IN :idExternoSet]) {
			posPedidoDatosMap.put(posPedido.RQO_fld_idExterno__c, posPedido.RQO_fld_codigoPedido__c);
		}

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN'); 
		
		return posPedidoDatosMap;
	}
	
	/**
    * @creation date: 02/02/2018
    * @author: David Iglesias - Unisys
    * @description: Consultas para obtener el mapa con el binomio Id externo - Objeto
	* @param: notificationAuxList			Lista de notificaciones donde se especifica el id externo del objeto a buscar
    * @return: Map<String, String> 			Mapa de retorno
    * @exception: 
    * @throws: 
    */
	private static Map<String, String> getEntregaFields (List<RQO_obj_notification__c> notificationAuxList) {
		
		/**
        * Constantes
        */
        final String METHOD = 'getEntregaFields';
	
		/**
		 * Variables
		 */
		Map<String, String> entregaDatosMap = new Map<String, String> ();
		Set<String> idExternoSet = new Set<String> ();

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		for (RQO_obj_notification__c item : notificationAuxList) {
			if (item.RQO_fld_objectType__c.equalsIgnoreCase(RQO_cls_Constantes.COD_SF_NOT_ENTREGA)) {
				idExternoSet.add(item.RQO_fld_externalId__c);
			}
		}
		
		/*
		for (RQO_obj_entrega__c entrega : [SELECT Id, RQO_fld_idExterno__c,  FROM RQO_obj_entrega__c WHERE RQO_fld_idExterno__c IN :idExternoSet]) {
			entregaDatosMap.put(entrega.RQO_fld_idExterno__c, entrega.Id);
		}
		*/
		for (RQO_obj_posiciondeEntrega__c poscicionEntrega : [SELECT Id, RQO_fld_idRelacion__r.RQO_fld_idExterno__c, RQO_fld_pedido__c
																FROM RQO_obj_posiciondeEntrega__c 
																WHERE RQO_fld_idRelacion__r.RQO_fld_idExterno__c IN :idExternoSet]) {
			entregaDatosMap.put(poscicionEntrega.RQO_fld_idRelacion__r.RQO_fld_idExterno__c, poscicionEntrega.RQO_fld_pedido__c);
		}

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN'); 
		
		return entregaDatosMap;
	}
	
	/**
    * @creation date: 02/02/2018
    * @author: David Iglesias - Unisys
    * @description: Consultas para obtener el mapa con el binomio Id externo - Objeto
	* @param: notificationCursoList					Lista de notificaciones de pedido en curso
	* @param: notificationConfirmadoList			Lista de notificaciones de pedido confirmado
    * @return: List<RQO_obj_notification__c>		Lista de notificaciones a actualizar
    * @exception: 
    * @throws: 
    */
	private static List<RQO_obj_notification__c> updateNotification (List<RQO_obj_notification__c> notificationCursoList, List<RQO_obj_notification__c> notificationConfirmadoList) {
		
		/**
        * Constantes
        */
        final String METHOD = 'updateNotification';
	
		/**
		 * Variables
		 */
		List<RQO_obj_notification__c> returnList = new List<RQO_obj_notification__c> ();

		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
		
		for (RQO_obj_notification__c item : notificationCursoList) {
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Actualizacion de flag para mail tratado. Notificacion: '+item.Id);
			item.RQO_fld_mailSent__c = true;
			returnList.add(item);
		}
		
		for (RQO_obj_notification__c item : notificationConfirmadoList) {
			System.debug(CLASS_NAME + ' - ' + METHOD + ': Actualizacion de flag para mail confirmado. Notificacion: '+item.Id);
			item.RQO_fld_mailSent__c = true;
			returnList.add(item);
		}

		System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN'); 
		
		return returnList;
	}
	
	// ***** CLASE EMBEBIDA ***** //
	
	public class MailResumenNotificationBean {
		public String clave;
		public List<RQO_obj_notification__c> notificationConfirmadoList;
		public List<RQO_obj_notification__c> notificationCursoList;
	}
		
		
}