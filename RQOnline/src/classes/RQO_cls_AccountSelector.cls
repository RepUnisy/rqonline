public with sharing class RQO_cls_AccountSelector extends RQO_cls_ApplicationSelector {
	
	// ***** CONSTANTES ***** //
	private static final String CLASS_NAME = 'RQO_cls_AccountSelector';
	
	public List<Schema.SObjectField> getSObjectFieldList()
    {
       	return new List<Schema.SObjectField> {
			Account.Name,
            Account.RQO_fld_idExterno__c,
            Account.RQO_fld_calleNumero__c,
            Account.RQO_fld_poblacion__c,
            Account.RQO_fld_pais__c};
    }

    public Schema.SObjectType getSObjectType()
    {
        return Account.sObjectType;
    }
    
    public override String getOrderBy()
	{
		return 'Name';
	}

    public List<Account> selectById(Set<ID> idSet)
    {
         return (List<Account>) selectSObjectsById(idSet);
    }

    public List<Account> selectByCTC(Set<String> ctcSet)
    {
    	// ***** CONSTANTES ***** //
        final String METHOD = 'selectByCTC';
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': ctcSet ' + ctcSet);
        
        string query = newQueryFactory().setCondition('RQO_fld_idExterno__c IN :ctcSet').toSOQL();
 													
 		System.debug(CLASS_NAME + ' - ' + METHOD + ': query ' + query);
 		
 		List<Account> accounts =  (List<Account>) Database.query(query); 

		return accounts;							                
    }
}