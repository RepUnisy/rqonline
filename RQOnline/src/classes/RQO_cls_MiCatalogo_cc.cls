/**
*  @name: RQO_cls_MiCatalogo_cc
*  @version: 1.0
*  @creation date: 02/10/2017
*  @author: Alfonso Constán López - Unisys
*  @description: Gestión de mi catálogo del usuario
*  @testClass: RQO_cls_CatalogueWSTest
*/
public without sharing class RQO_cls_MiCatalogo_cc {
    
    @AuraEnabled
    public static Object getMiCatalogo(Id IdContacto, String language){
        //	Inicializamos las listas
        Map<Id, List<RQO_obj_grade__c>> mapGrados = new Map<Id, List<RQO_obj_grade__c>>();
        
       //	Obtenemos el listado de grados asignados al usuario
        List<RQO_obj_grade__c> listGrados = new List<RQO_obj_grade__c>([SELECT Id, Name, RQO_fld_sku__r.RQO_fld_numContent__c, RQO_fld_product__c, RQO_fld_recommendation__c, RQO_fld_product__r.Name FROM RQO_obj_grade__c WHERE id in (select RQO_fld_grade__c from RQO_obj_myCatalogue__c where RQO_fld_contact__c = :IdContacto) order by RQO_fld_product__c]);

        if (listGrados.size() != 0){   
             //	Obtenemos un mapa de categorias y la lista de grados
            for (RQO_obj_grade__c grade :listGrados){

                if(mapGrados.containsKey(grade.RQO_fld_product__c)){
                    
                    mapGrados.get(grade.RQO_fld_product__c).add(grade);
                }
                else{                    
                    List<RQO_obj_grade__c> listGradoCat = new List<RQO_obj_grade__c>();
                    listGradoCat.add(grade);
                    mapGrados.put(grade.RQO_fld_product__c, listGradoCat);
                }
            }        
                        
            Map<Id, String> mapNombreGrado = new Map<Id, String>();
            //	Obtenemos el nombre de las categorías
            List<RQO_obj_translation__c> transCategory = [select id, RQO_fld_translation__c, RQO_fld_category__c from RQO_obj_translation__c where RQO_fld_category__c in : mapGrados.keySet() ];
            
            for (RQO_obj_translation__c trad : transCategory){
                mapNombreGrado.put(trad.RQO_fld_category__c, trad.RQO_fld_translation__c);
            }
                        
            List<RQO_obj_catSpecification__c> listEspecificaciones = new List<RQO_obj_catSpecification__c>([SELECT Id, RQO_fld_masterSpecification__r.Name, RQO_fld_masterSpecification__r.RQO_fld_tipologiaTexto__c, RQO_fld_category__c FROM RQO_obj_catSpecification__c WHERE RQO_fld_category__c = :mapGrados.keySet() ORDER BY RQO_fld_position__c]);
            Map<Id, List<RQO_obj_catSpecification__c>> mapSpec = new Map<Id, List<RQO_obj_catSpecification__c>>();
            //	Obtenemos un mapa de categorias y la lista de espeficicaciones
            for (RQO_obj_catSpecification__c esp :listEspecificaciones){
                if(mapSpec.containsKey(esp.RQO_fld_category__c)){
                    mapSpec.get(esp.RQO_fld_category__c).add(esp);
                }
                else{
                    List<RQO_obj_catSpecification__c> listEspCat = new List<RQO_obj_catSpecification__c>();                   
                    if (esp != null){
                        listEspCat.add(esp);
                        mapSpec.put(esp.RQO_fld_category__c, listEspCat);
                    }                    
                }
            }
            
            
            List<RQO_obj_specification__c> listaEspecificacionesGrado = new List<RQO_obj_specification__c>([SELECT Id, RQO_fld_grade__r.Name, RQO_fld_grade__r.RQO_fld_product__c, RQO_fld_value__c, RQO_fld_catSpecification__r.RQO_fld_masterSpecification__r.Name FROM RQO_obj_specification__c WHERE RQO_fld_grade__c IN :listGrados ORDER BY RQO_fld_catSpecification__r.RQO_fld_position__c, RQO_fld_catSpecification__r.Name, RQO_fld_value__c]);
            Map<Id, List<RQO_obj_specification__c>> mapEspGrade = new Map<Id, List<RQO_obj_specification__c>>();
            //Obtenemos un mapa de los grados y la lista de espeficicaciones
            for (RQO_obj_specification__c espGrado :listaEspecificacionesGrado){
                if(mapEspGrade.containsKey(espGrado.RQO_fld_grade__r.RQO_fld_product__c)){
                    mapEspGrade.get(espGrado.RQO_fld_grade__r.RQO_fld_product__c).add(espGrado);
                }
                else{
                    List<RQO_obj_specification__c> listespGrade = new List<RQO_obj_specification__c>();
                    listespGrade.add(espGrado);
                    mapEspGrade.put(espGrado.RQO_fld_grade__r.RQO_fld_product__c, listespGrade);
                }
            }
            List<RQO_cls_WrapperTablaObject> listTablas = generarWrapper (language, mapEspGrade, mapGrados, mapNombreGrado, mapSpec);
            return JSON.serialize(listTablas);          
        }
        else{
            return null;
        }               
    }
    
        /**
    * @creation date: 27/10/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Genera el wrapper para la visualización de la tabla obtenida
    * @param: 
    * @return: List<RQO_cls_WrapperTablaObject> listado con elementos del wrapper
    */
    public static List<RQO_cls_WrapperTablaObject> generarWrapper (String language, Map<Id, List<RQO_obj_specification__c>> mapEspGrade, Map<Id, List<RQO_obj_grade__c>> mapGrados, Map<Id, String> mapNombreGrado, Map<Id, List<RQO_obj_catSpecification__c>> mapSpec){
        
        List<RQO_cls_WrapperTablaObject> listTablas = new List<RQO_cls_WrapperTablaObject>();
        for(Id idCategoria : mapGrados.keySet()){
            RQO_cls_WrapperTablaObject wrapperReturn = new RQO_cls_WrapperTablaObject();
            
            List<RQO_obj_catSpecification__c> listEspecificacionWrapper = new List<RQO_obj_catSpecification__c>();
            List<RQO_obj_grade__c> listGradosWrapper = new List<RQO_obj_grade__c>();
            List<RQO_obj_specification__c> listaEspecificacionesGradoWrapper = new List<RQO_obj_specification__c> ();
            
            wrapperReturn.setCategoria(idCategoria);
            listGradosWrapper = mapGrados.get(idCategoria);
            wrapperReturn.setNombreCategoria(mapNombreGrado.get(idCategoria));
            
            listEspecificacionWrapper = mapSpec.get(idCategoria);            
            
            List<Id> listIdsMaster = new List<Id>();
        	for (RQO_obj_catSpecification__c catt : listEspecificacionWrapper) {
            listIdsMaster.add(catt.RQO_fld_masterSpecification__c);
        	}
                     
            List<RQO_obj_translation__c> listTranslation = [select id, RQO_fld_translation__c, RQO_fld_locale__c from RQO_obj_translation__c where RQO_fld_masterSpecification__c in : listIdsMaster AND RQO_fld_locale__c = : language];
			wrapperReturn.setListaTraduccionesEspecifi(listTranslation);

            //	Obtenemos las traducciones de los grados
            wrapperReturn.setTraduccionesGrados(RQO_cls_Global_Class.getGradeTranslate(language, listGradosWrapper));
            
            //	Introducimos en el wrapper las especificaciones
            if (listEspecificacionWrapper != null){
                wrapperReturn.setListaEspecificaciones(listEspecificacionWrapper);
                wrapperReturn.setTipoDatoEspecificaciones();
            }
            
            listaEspecificacionesGradoWrapper = mapEspGrade.get(idCategoria);                    
            
            if (listaEspecificacionesGradoWrapper != null && listaEspecificacionesGradoWrapper.size() != 0){
                for(Integer i = listaEspecificacionesGradoWrapper.size() - 1; i >= 0; i--){
                    wrapperReturn.putElement(listaEspecificacionesGradoWrapper[i].RQO_fld_grade__c, listaEspecificacionesGradoWrapper[i].RQO_fld_catSpecification__c, listaEspecificacionesGradoWrapper[i].RQO_fld_value__c);
                }      
            }                
            wrapperReturn.rellenarHuecos(listGradosWrapper);
            listTablas.add(wrapperReturn);
        }
        return listTablas;
    }
    
    /**
    * @creation date: 27/10/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Comprobación si el grado se encuentra insertado en el catálogo
    * @param: grado: grado que se desea comprobar
    * @param: contacto: contacto perteneciente "mi catálogo"
    * @return: 
    */
    @AuraEnabled
    public static Boolean isMiCatalogoGrado(Id grado, Id contacto){
        //	Buscamos si el grado se encuentra asignado al contacto en el objeto miCatalogo
        List<RQO_obj_myCatalogue__c> listCatalogo = [select id from RQO_obj_myCatalogue__c where RQO_fld_grade__c = :grado AND RQO_fld_contact__c = :contacto];
        System.debug('listCatalogo*************;: ' + listCatalogo);
        if (listCatalogo.size() != 0){
            return true;
        }
        else{
            return false;
        }        
    }
    
    /**
    * @creation date: 27/10/2017
    * @author: Alfonso Constán López - Unisys
    * @description: Inserción de un grado a mi catálogo
    * @param: grado: grado que se desea insertar
    * @param: contacto: contacto perteneciente "mi catálogo"
    * @return: 
    */
    @AuraEnabled
    public static Boolean insertMiCatalogoGrado(Id grado, Id contacto){
        
        RQO_obj_myCatalogue__c objMC = new RQO_obj_myCatalogue__c();
        objMC.RQO_fld_grade__c = grado;
        objMC.RQO_fld_contact__c = contacto;
        insert objMC;
        
        if (objMC.id != null){
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
    * @creation date: 13/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Recupera el catalogo propuesto para un cliente. Se recuperan los datos en función de lo parametrizado en Salesforce para el cliente y
    * de los acuerdos contraidos en SAP
    * @param: idAccoun tipo Id
    * @param: IdContacto tipo Id
    * @return: Object
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static Object volverProponer(Id idAccoun, Id IdContacto, String language){
        
        List<RQO_obj_myCatalogue__c> lisMiCatalogo = new List<RQO_obj_myCatalogue__c>();
        /*AAT - Vamos a por los acuerdos de SAP para añadir al listado propuesto */
        //Recuperamos de un webservice los grados asociados a los acuerdos del contacto.
        RQO_cls_CatalogueWS rqoCatsalogueWS = new RQO_cls_CatalogueWS();
        RQO_cls_CatalogueWS.CatalogueCustomResponse catalogueSapResponse = rqoCatsalogueWS.refreshMyCatalogue(idAccoun, idContacto);               
                        
        //	Eliminamos mi catalogo configurado
        deleteMiCatalogo([select id from RQO_obj_myCatalogue__c where RQO_fld_contact__c = : IdContacto]);
        
        
        date d = Date.Today();
        integer mesesDesde = 12;        
        Date fechaDesde = Date.newInstance(d.year(), (d.month() - mesesDesde), d.day());
        
        //	Insertamos los Assets del cliente
        RQO_obj_myCatalogue__c objMC = null;
        for (AggregateResult ass : [select RQO_fld_grade__c from Asset where AccountId = : idAccoun AND RQO_fld_fechadeConfirmacion__c != null AND RQO_fld_fechadeConfirmacion__c > : fechaDesde group by RQO_fld_grade__c]){
            system.debug('ass ' + ass);
            objMC = new RQO_obj_myCatalogue__c();
            objMC.RQO_fld_grade__c = (id)ass.get('RQO_fld_grade__c');
            objMC.RQO_fld_contact__c = IdContacto;
            lisMiCatalogo.add(objMC);
        }
        
        //Si ha respondido correctamente el servicio y tenemos acuerdoas
        if (RQO_cls_Constantes.SOAP_COD_OK.equalsIgnoreCase(catalogueSapResponse.errorCode) && catalogueSapResponse.sapCatlogue != null 
			&& !catalogueSapResponse.sapCatlogue.isEmpty()){
				//Verificamos que los grados de SAP no existan ya en Salesforces asociados al contacto para no duplicarlo
				List<RQO_obj_myCatalogue__c> finalListSAPCatalogue = verifySAPCatalogue(catalogueSapResponse.sapCatlogue, lisMiCatalogo);
				if (finalListSAPCatalogue != null && !finalListSAPCatalogue.isEmpty()){lisMiCatalogo.addAll(finalListSAPCatalogue);}
				System.debug('Lista: ' + finalListSAPCatalogue);
        }
        
        if (lisMiCatalogo != null && !lisMiCatalogo.isEmpty()){
            insert lisMiCatalogo;
        }
        
        
        return getMiCatalogo(IdContacto, language);
    }
    
    /**
    * @creation date: 13/10/2017
    * @author: Alvaro Alonso - Unisys
    * @description:	Verifica que los grados recuperados de los acuerdos de SAP no estén ya asociados a la lista principal que s eha extraido de Salesforce
    * @param: mapSapGrades tipo Map<Id, RQO_obj_myCatalogue__c>, mapa con el catalogo de SAP para el contacto. 
    * @param: isMiCatalogo tipo Lista de objetos RQO_obj_myCatalogue__c, lista de grados para para el catalogo que s ehan encontrado en Salesforce.
    * @return: Lista de objetos de tipo RQO_obj_myCatalogue__c con los grados de SAP que hay que añadir a la lista principal
    * @exception: 
    * @throws: 
    */
    private static List<RQO_obj_myCatalogue__c> verifySAPCatalogue(Map<Id, RQO_obj_myCatalogue__c> mapSapGrades, List<RQO_obj_myCatalogue__c> lisMiCatalogo){
        List<RQO_obj_myCatalogue__c> listAux = new List<RQO_obj_myCatalogue__c>();
        RQO_obj_myCatalogue__c objCat = null;
        Boolean inList = false;
        for(String key : mapSapGrades.keySet()){
            objCat = mapSapGrades.get(key);
            inList = false;
            for(RQO_obj_myCatalogue__c objCatalogue : lisMiCatalogo){
                if (objCatalogue.RQO_fld_grade__c == objCat.RQO_fld_grade__c){
                    inList = false;
                    break;
                }
            }
            if (!inList){listAux.add(objCat);}
        }
        System.debug('listAux: ' + listAux);
        return listAux;
    }
    
    /**
    * @creation date: 13/10/2017
    * @author: Alfonso Constán López - Unisys
    * @description:	Fundión de eliminación de un grado del catálogo
    * @param: listGrado tipo: List<RQO_obj_myCatalogue__c>, listado de catálogo para eliminar    
    * @return: True al ir todo bien
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static Boolean deleteMiCatalogoGrado(Id grado, Id contacto){
        List<RQO_obj_myCatalogue__c> listCatalogo = [select id from RQO_obj_myCatalogue__c where RQO_fld_grade__c = :grado AND RQO_fld_contact__c = :contacto];        
        return deleteMiCatalogo(listCatalogo);
    }
    
    /**
    * @creation date: 13/10/2017
    * @author: Alfonso Constán López - Unisys
    * @description:	Eliminación de elementos de catálogo 
    * @param: listGrado tipo: List<RQO_obj_myCatalogue__c>, listado de catálogo para eliminar    
    * @return: True al ir todo bien
    * @exception: 
    * @throws: 
    */
    @AuraEnabled
    public static Boolean deleteMiCatalogo(List<RQO_obj_myCatalogue__c> listGrado){        
        delete listGrado;        
        return true;
    }
    
    
}