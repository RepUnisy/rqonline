/**
*	@name: RQO_cls_CustomSettingUtil_Test
*	@version: 1.0
*	@creation date: 21/02/2018
*	@author: David Iglesias - Unisys
*	@description: Clase de test para probar la clase de utilidades de CS
*/
@IsTest
public class RQO_cls_CustomSettingUtil_Test {

    /**
	* @creation date: 23/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
	* @exception: 
	* @throws: 
	*/
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createCSParametrizacionIntegraciones(new List<String> {'integracion'});
            RQO_cls_TestDataFactory.createCSprocesoPlanificado('BorradoHistoricoRegistros');
            RQO_cls_TestDataFactory.createCStiempoBorradoRegistros('RQO');
            RQO_cls_TestDataFactory.createCSParametrizacionObjBulk('RQO_obj_identificador__c', new List<String> {'RQO_fld_tipodeIdentificador__c'});
            RQO_cls_TestDataFactory.createCSRTAccount();
            RQO_cls_TestDataFactory.createCSSapRP2Locale();
            RQO_cls_TestDataFactory.createCSEnvioNotificaciones();
            RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
		}
    }
    
    /**
	* @creation date: 22/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getParametrizacionIntegraciones
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getParametrizacionIntegracionesTest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_obj_parametrizacionIntegraciones__c cs = RQO_cls_CustomSettingUtil.getParametrizacionIntegraciones('integracion');
            test.stopTest();
            
            System.assertEquals('integracion', cs.Name);
        }
    }
	
	/**
	* @creation date: 22/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getProcesoPlanificado
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getProcesoPlanificadoTest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cs_procesoPlanificado__c cs = RQO_cls_CustomSettingUtil.getProcesoPlanificado('BorradoHistoricoRegistros');
            test.stopTest();
            
            System.assertEquals('BorradoHistoricoRegistros', cs.Name);
        }
    }
	
	/**
	* @creation date: 22/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getParametrizacionObjBulk
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getParametrizacionObjBulkTest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            Map<String, RQO_cs_parametrizacionObjBulk__c> csMap = RQO_cls_CustomSettingUtil.getParametrizacionObjBulk();
            test.stopTest();
            
            System.assertEquals('RQO_obj_identificador__c', csMap.get('RQO_obj_identificador__c').Name);
        }
    }
	
	/**
	* @creation date: 22/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getParametrizacionEnvioNotificaciones
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getParametrizacionEnvioNotificacionesTest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cs_envioNotificaciones__c cs = RQO_cls_CustomSettingUtil.getParametrizacionEnvioNotificaciones('PEDIDO');
            test.stopTest();
            
            System.assertEquals(true, cs.RQO_fld_activo__c);
        }
    }
	
	/**
	* @creation date: 22/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getTiempoBorradoRegistros
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getTiempoBorradoRegistrosTest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            RQO_cs_tiempoBorradoRegistros__c cs = RQO_cls_CustomSettingUtil.getTiempoBorradoRegistros('RQO');
            test.stopTest();
            
            System.assertEquals('2', cs.RQO_fld_auxBulkObject__c);
        }
    }
	
	/**
	* @creation date: 23/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getParametrizacionRTAccount
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getParametrizacionRTAccountTest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            Map<String, RQO_cs_parametrizacionRTAccount__c> csMap = RQO_cls_CustomSettingUtil.getParametrizacionRTAccount();
            test.stopTest();
            
            System.assertEquals('Z001', csMap.get('Z001').Name);
        }
    }
	
	/**
	* @creation date: 23/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getParametrizacionSAPRP2Locale
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getParametrizacionSAPRP2LocaleTest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
			RQO_cls_CustomSettingUtil.getParametrizacionDateFilterData('Fac');
            Map<String, RQO_cs_sapRP2Locale__c> csMap = RQO_cls_CustomSettingUtil.getParametrizacionSAPRP2Locale();
            test.stopTest();
            
            System.assertEquals('AR', csMap.get('AR').Name);
        }
    }
	
    	/**
	* @creation date: 23/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getParametrizacionSAPRP2Locale
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getParametrizacionDateFilterData(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
			RQO_cmt_dateFilterData__mdt prueba = RQO_cls_CustomSettingUtil.getParametrizacionDateFilterData();
            test.stopTest();
            
            System.assertNotEquals(null, prueba);
        }
    }
	/**
	* @creation date: 23/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getParametrizacionTrigger
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getParametrizacionTriggerTest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            test.startTest();
            REP_cs_activacionTrigger__c cs = RQO_cls_CustomSettingUtil.getParametrizacionTrigger('RQO_trg_GradeTrigger');
            test.stopTest();
            
            System.assertEquals(false, cs.REP_fld_triggerActive__c);
        }
    }
    
	/**
	* @creation date: 23/02/2018
	* @author: David Iglesias - Unisys
	* @description:	Método test para probar la funcionalidad correcta getParametrizacionDateFilterData
	* @exception: 
	* @throws:  
	*/
    @isTest
    static void getParametrizacionDateFilterDataTest(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
			RQO_cmt_dateFilterData__mdt dateData = null;
       		for(RQO_cmt_dateFilterData__mdt obj : [Select RQO_fld_type__c, RQO_fld_numeroMeses__c from RQO_cmt_dateFilterData__mdt]){
            	dateData = obj;
       		}             
            test.startTest();
            RQO_cmt_dateFilterData__mdt cs = RQO_cls_CustomSettingUtil.getParametrizacionDateFilterData();
            System.assertEquals(dateData, cs);
            test.stopTest();
        }
    }

}