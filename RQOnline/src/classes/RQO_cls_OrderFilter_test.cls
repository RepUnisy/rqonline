/**
*   @name: RQO_cls_OrderFilter_test
*   @version: 1.0
*   @creation date: 27/11/2017
*   @author: Julio Maroto - Unisys
*   @description: Apex controller test for invoice filtering component
*/
@isTest
public class RQO_cls_OrderFilter_test {
    @testSetup
    public static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createAsset(5);
        }
    }
    
    /**
    *   @name: RQO_cls_OrderFilter_test
    *   @version: 1.0
    *   @creation date: 27/11/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Obtener picklist de estados para origen de tipo 'historical' (histórico)
    */
    @isTest
    public static void testGetStatusPicklist() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String origin = RQO_cls_Constantes.COD_FILTRO_PEDIDO_HISTORIAL;
            Map<String, String> verifiedStatusExpected;
            Map<String, String> verifiedStatusActual;
            
            // Comprobar mapa de estados verificados para estados de origen tipo: 'Historical'
            verifiedStatusExpected = getVerifiedStates(origin);       
            verifiedStatusActual = RQO_cls_OrderFilter_cc.getStatusPicklist(origin);
            System.assertEquals(verifiedStatusExpected.size(), verifiedStatusActual.size());
            
            origin = 'Tracking';
            // Comprobar mapa de estados verificados para estados de origen tipo: 'Tracking'
            verifiedStatusExpected = getVerifiedStates(origin);       
            verifiedStatusActual = RQO_cls_OrderFilter_cc.getStatusPicklist(origin);
            System.assertEquals(verifiedStatusExpected.size(), verifiedStatusActual.size());
            
            origin = 'UNKNOWN';
            // Comprobar mapa de estados verificados para estados de un tipo origen no válido: 'UNKNOWN' 
            verifiedStatusExpected = getVerifiedStates(origin);       
            verifiedStatusActual = RQO_cls_OrderFilter_cc.getStatusPicklist(origin);
            System.assertEquals(verifiedStatusExpected.size(), verifiedStatusActual.size());
        }
    }
    
    /**
    *   @name: RQO_cls_OrderFilter_test
    *   @version: 1.0
    *   @creation date: 27/11/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Método auxiliar para obtener estados verificados según el origen
    */
    private static Map<String, String> getVerifiedStates(String origin) {
        Map<String, String> allValidStatus = null;
        Map<String, String> validStatusDataMap = getValidStatusPerOrigin(origin);
        Map<String, String> mapAllStatus = getAllStatusMap();

        
        //Verificamos del listado completo que estados están verificados
        if (mapAllStatus != null && !mapAllStatus.isEmpty()) {

            allValidStatus = new Map<String, String>();
            if (validStatusDataMap == null || validStatusDataMap.isEmpty()) {
                allValidStatus.putAll(mapAllStatus);
            } else {
                String actualValue = '';
                
                for(String dato : mapAllStatus.keySet()) {
                    actualValue = mapAllStatus.get(dato);
                    //Si el mapa está nulo o vacío añadimos 
                    if (validStatusDataMap.containsKey(dato)) {
                        allValidStatus.put(dato, actualValue);
                    }
                }
            }
        }
        
        return allValidStatus;
    }
    
	/**
    *   @name: RQO_cls_OrderFilter_test
    *   @version: 1.0
    *   @creation date: 27/11/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Se peticionan todos los estados de posiciones y de entrega; se meten ambos conjuntos en un mismo mapa
    */ 
    private static Map<String, String> getAllStatusMap() {
        Map <String, String> mapAllStatus = new Map<String, String>();
        
        RQO_cls_GeneralUtilities util = new RQO_cls_GeneralUtilities();
        Map<String, String> positionStatus = util.getAssetStatus();
        
        //Si tengo mapa de estados de posiciones lo añado al mapa de todos los estados
        if (positionStatus != null && !positionStatus.isEmpty()) {
            if (mapAllStatus == null) {
                mapAllStatus = new Map<String, String>();
            }
            
            mapAllStatus.putAll(positionStatus);
        }
        
        //Si tengo mapa de estados de entrega lo añado al mapa de todos los estados
        Map<String, String> deliveryStatus = util.getDeliveryStatus();
        if (deliveryStatus != null && !deliveryStatus.isEmpty()) {
            if (mapAllStatus == null) {
                mapAllStatus = new Map<String, String>();
            }
            
            mapAllStatus.putAll(deliveryStatus);
        }
        
        return mapAllStatus;
    }
    
   	/**
    *   @name: RQO_cls_OrderFilter_test
    *   @version: 1.0
    *   @creation date: 27/11/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Método auxiliar que devuelve un mapa de estados válidos según un origen
    */ 
    private static Map<String, String> getValidStatusPerOrigin(String origin){        
        Map<String, String> validStatusDataMap = null;
        
        for (RQO_cmt_orderFilterStatus__mdt recordData : [SELECT
                                                            RQO_fld_validStatus__c
                                                          FROM
                                                            RQO_cmt_orderFilterStatus__mdt
                                                          WHERE
                                                            RQO_fld_type__c = : origin])
        {
            if (validStatusDataMap == null) {
                validStatusDataMap = new Map<String, String>();
            }
            
            validStatusDataMap.put(recordData.RQO_fld_validStatus__c, recordData.RQO_fld_validStatus__c);
        }
        
        return validStatusDataMap;
    }
    
    /**
    *   @name: RQO_cls_OrderFilter_test
    *   @version: 1.0
    *   @creation date: 27/11/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Método que testea la casuística en la que al obtener todos los grados no se devuelve una lista
    * 	vacía
    */ 
    @isTest
    public static void testGetAllGradesNotEmpty() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            Account account = [SELECT Id FROM Account LIMIT 1];
            String searchKey = 'Descripcion test';

            List<String> assetListWithQp0Grades = RQO_cls_OrderFilter_cc.getAllGrades(account.Id, searchKey);   
            System.assert( false == assetListWithQp0Grades.isEmpty() );
        }
    }
    
    /**
    *   @name: RQO_cls_OrderFilter_test
    *   @version: 1.0
    *   @creation date: 27/11/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Método que testea la casuística en la que se obtienen todos los grados por Qp0AuxDescription
    */ 
    @isTest
    public static void testGetAllGradesQp0AuxDescription() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String searchKey = 'Descripcion test';
            Account account = [SELECT Id FROM Account LIMIT 1];
            List<Asset> assets = [Select Id, RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c from Asset];
            
            for (Asset asset: assets) {
                asset.RQO_fld_qp0Grade__r.RQO_fld_descripcionDelGrado__c = '';
            }
            
            UPDATE assets;
            
            List<String> qp0gradesWithAssetList = RQO_cls_OrderFilter_cc.getAllGrades(account.Id, searchKey);
            
            assets = [SELECT
                        RQO_fld_descripciondeGrado__c
                      FROM
                        Asset
                      WHERE
                        AccountId =: account.Id
                      AND
                        RQO_fld_qp0Grade__c = ''
                      AND
                        RQO_fld_descripciondeGrado__c LIKE :searchKey
                      LIMIT 50000];
            
            Integer counter = 0;
            
            for (Asset asset : assets) {
                String gradeDescription = asset.RQO_fld_descripciondeGrado__c;
                String qp0Description;
                
                if (null != (qp0Description = qp0gradesWithAssetList.get(counter))) {
                    System.assert(gradeDescription.equals(qp0Description));
                }
                
                counter++;
            }
        }
    }
    
    /**
    *   @name: RQO_cls_OrderFilter_test
    *   @version: 1.0
    *   @creation date: 27/11/2017
    *   @author: Julio Maroto - Unisys
    *   @description: Método que testea obtener todos los datos parametrizados
    */ 
    @isTest
    public static void testGetDateParametrizedData() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        System.runAs(u){
            String origin = RQO_cls_Constantes.COD_FILTRO_PEDIDO_HISTORIAL;
            Integer monthsOfDifference = RQO_cls_OrderFilter_cc.getDateParametrizedData(origin);

            System.debug('NUMERO DE MESES: ' + monthsOfDifference);
            System.assertEquals(3, monthsOfDifference);
        }
    }
}