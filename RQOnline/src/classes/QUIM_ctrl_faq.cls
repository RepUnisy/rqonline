global with sharing class QUIM_ctrl_faq{
    	
    /**
     * Conjunto de variables necesarias para mostrar las FAQs
     * */
    public static List<QUIM_Faq__c> faqsList {get;set;}
    public static  List<String> categoriaList {get;set;}
    public static String idioma{get;set;}
    global static String blank{get{return '';}private set;}
        
    /**
     * Constructor de las FAQs
     * 	ENTRADA: ninguna
     * 	SALIDA: 
     * 		- Conjunto de categorías existentes en las FAQs
     * 		- Conjunto de FAQs asociadas a la primera categoría
     * */
    public QUIM_ctrl_faq(){
        System.debug(' ::::: ' + UserInfo.getLanguage()) ;
        idioma=ccrz.cc_CallContext.userLocale;
        
        Integer posicion=1;
        faqsList = new List<QUIM_Faq__c>();
        categoriaList = new List<String>();
        try{            
            Schema.DescribeFieldResult studentStatus = QUIM_Faq__c.Categorizacion__c.getDescribe();
            List<Schema.PicklistEntry> values = studentStatus.getPicklistValues();
            for (Schema.PicklistEntry a : values)
              { 
                 categoriaList.add(a.getValue()); 
              }
           	String categoria=categoriaList.get(0);
            String contactId = ccrz.cc_CallContext.currUser.ContactId;
            
            boolean isPrivate=false;
            if (contactId!=null && contactId!=''){
                isPrivate =true;
            }
            
            if (!idioma.equals('es_ES')  && !idioma.equals('en_US') ){
                if(('es_ES').containsIgnoreCase(idioma)){
                    idioma ='es_ES';
                }else if (('en_US').containsIgnoreCase(idioma)){
                    idioma ='en_US';
                }else {
                    idioma ='en_US';
                }
            }    
            
            
           faqsList = [SELECT Id, Titulo__c, Description__c FROM QUIM_Faq__c WHERE Privado__c =FALSE and Categorizacion__c =: categoria and Locale__c =:idioma];
            System.debug(System.LoggingLevel.INFO, 'print list of faqs: '+faqsList.size());
        }catch(Exception e){
            System.debug(System.LoggingLevel.ERROR, 'ERR'+e);
        }finally{
            System.debug(System.LoggingLevel.INFO, 'M:X fetchFAQs');
        }
    }
    
    /**
* Función changeCategoria
     * 	ENTRADA: string con la categoria
     * 	SALIDA: conjunto de FAQs asociadas a la categoria de entrada
     * */
    @RemoteAction
    global static List<QUIM_Faq__c> changeCategoria(String categoriaSelect, String idiomaPage){
        if (!idiomaPage.equals('es_ES')  && !idiomaPage.equals('en_US') ){
                if(('es_ES').containsIgnoreCase(idiomaPage)){
                    idiomaPage ='es_ES';
                }else if (('en_US').containsIgnoreCase(idiomaPage)){
                    idiomaPage ='en_US';
                }else {
                    idiomaPage ='en_US';
                }
            } 
        System.debug('categoriaSelect= '+categoriaSelect);
        try{
            faqsList = [SELECT Id, Titulo__c, Description__c FROM QUIM_Faq__c WHERE Privado__c = FALSE and Categorizacion__c =: categoriaSelect and Locale__c =:idiomaPage];
			System.debug('faqList= '+faqsList); 
        }catch(Exception e){}
        return faqsList;
    }
    
    /**
     * Función searchResult
     * 	ENTRADA: string con el texto a buscar en la categoria
     * 	SALIDA: conjunto de FAQs asociadas a la categoria de entrada
     * */
    @RemoteAction
    global static List<QUIM_Faq__c> searchResult(String textSearch, String idiomaPage){
        if (!idiomaPage.equals('es_ES')  && !idiomaPage.equals('en_US') ){
                if(('es_ES').containsIgnoreCase(idiomaPage)){
                    idiomaPage ='es_ES';
                }else if (('en_US').containsIgnoreCase(idiomaPage)){
                    idiomaPage ='en_US';
                }else {
                    idiomaPage ='en_US';
                }
            } 
        System.debug('textSearch= '+textSearch); 
        faqsList = new List<QUIM_Faq__c>();
        List<QUIM_Faq__c> faqsListAUX = new List<QUIM_Faq__c>();
        //textSearch= '%'+textSearch+'%';
        System.debug('textSearch= '+textSearch); 
        try{
            faqsListAUX = [SELECT Id, Categorizacion__c, Titulo__c, Description__c FROM QUIM_Faq__c WHERE Privado__c = FALSE and Locale__c =:idiomaPage  ORDER BY Categorizacion__c];
            for (QUIM_Faq__c faq: faqsListAUX){
                //debemos hacerlo con contains ya que Description__c es textAreaLong y no se puede hacer un LIKE sobre dicho campo
                //
                if (faq.Titulo__c.containsIgnoreCase(textSearch) || faq.Description__c.containsIgnoreCase(textSearch)){
                    faqsList.add(faq);
                }
            }   
			System.debug('faqList= '+faqsList); 
        }catch(Exception e){   
        }
        return faqsList;
    }
    
}