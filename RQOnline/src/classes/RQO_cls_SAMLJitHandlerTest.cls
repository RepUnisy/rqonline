/**
*   @name: RQO_cls_SAMLJitHandlerTest
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_SAMLJitHandler.
*/
@isTest(SeeAllData=false)
private class RQO_cls_SAMLJitHandlerTest {
    /**
    *   @name: RQO_cls_emailTemplateSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @testSetup static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsInitial@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        
        System.runAs(u) {
            // Crear email templates RQO_eplt_correoNuevoContactoSAC_ RQO_eplt_correoNuevoContactoMaster_
            RQO_cls_TestDataFactory.createEmailTemplate(new List<String> {'RQO_eplt_correoNuevoContactoSAC_en', 'RQO_eplt_correoNuevoContactoMaster_en'}); 
        }
    }
    
    /**
    *   @name: RQO_cls_emailTemplateSelector
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba el envío de correo de una comunicación creada para el día de hoy.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @isTest static void whenSAMLJitHandlerCreatesUser(){

        Id samlSsoProviderId = '0LE9E0000004CUz'; //@TODO make dynamic
        Id communityId = '0DB9E000000CagW'; //@TODO make dynamic
        Id portalId = null;
        String federationIdentifier = 'sfTestUserProvisioning@testorg.com';
        Map<String, String> attributes = new Map<String, String>();
        attributes.put('Identity', 'sfTestUserProvisioning@testorg.com');
        attributes.put('Contact.Email', 'sfTestUserProvisioning@testorg.com');
        attributes.put('Contact.FirstNam', 'Test First Name');
        attributes.put('Contact.LastName', 'Test Last Name');
        attributes.put('Contact.MobilePhone', '666666666');
        attributes.put('Contact.RQO_fld_commInfConsent__c', 'true');
        attributes.put('Contact.RQO_fld_corpInfConsent__c', 'true');
        attributes.put('Contact.RQO_fld_techInfConsent__c', 'true');
        attributes.put('Contact.RQO_fld_surveyConsent__c', 'true');
        attributes.put('Contact.RQO_fld_oldUsername', '');
        attributes.put('Contact.RQO_fld_pais__c', 'Spain');
        attributes.put('Contact.RQO_fld_idioma__c', 'es');
        attributes.put('Contact.RQO_fld_perfilCliente__c', 'Transformador');
        attributes.put('User.ProfileID', 'Customer Community User');
        attributes.put('User.UserName', 'sfTestUserProvisioning@testorg.com');
        attributes.put('User.Email', 'sfTestUserProvisioning@testorg.com');
        attributes.put('User.FirstName', 'Test First Name');
        attributes.put('User.LastName', 'Test Last Name');
        
        String assertion = 'PHNhbWxwOlJlc3BvbnNlIElEPSJfODJjOWM4MDktNjFkMS00NDIyLWE3NWMtOTAyNWI3MWU1YjE1IiBJblJlc3BvbnNlVG89Il8yQ0FBQUFXSnNRa3hHTUU4d09VVXdNREF3TURBd01EQTJBQUFBMUJ6YlA0MGZxOEFrcGNYcmoxZjhlRGUxLUczZTVQQS1LQUJQQVVmS0tXeUYzbHcwNVFtckRQWHJpdGtiTDlZeTRtc3REX1c2VmlRZjJOVDJQZXlwTkx2em82UlZSVkR2dmlGMkY2R0ZTUFNfNlBYdVFNY0NhYzlMRkNrcWN3c1ZRTll1dHVyY3J5ck5MT05HbmNOSko1cFJGUHhCTFB1cEtVTWc0Sy1lVGdnNmY2S2RqUzlwcGtVdDVvQlJpZkliUEJXcGhfSndvOXlMUG1UeF9Ma0R4Y283RGNiMHZSY3FXcjdnZ3AwbTlwSWhqRnZMeGFFX3dZcGV6cXNfRkRGSi13IiBWZXJzaW9uPSIyLjAiIElzc3VlSW5zdGFudD0iMjAxOC0wMi0yOFQxMDo1ODo0MS41NzZaIiBEZXN0aW5hdGlvbj0iaHR0cHM6Ly9kZXJxb25saW5lLXJlcHNvbC5jczg4LmZvcmNlLmNvbS9jb211bmlkYWRxdWltaWNhL2xvZ2luP3NvPTAwRDlFMDAwMDAwMGhKQSIgeG1sbnM6c2FtbHA9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpwcm90b2NvbCI';

        Boolean DidThrowException = false;
        
        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsEmail@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        
        System.runAs(userRunAs) {
            string messageEx = '';
            test.startTest();
            
            try {
                List<Account> account = RQO_cls_TestDataFactory.createAccount(1);
                                                           
                attributes.put('Account.RQO_fld_idExterno__c', account[0].RQO_fld_idExterno__c);
                
                RQO_cls_SAMLJitHandler handler = new RQO_cls_SAMLJitHandler();
                handler.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
            }
            catch (Exception e) {
                messageEx = e.getMessage();
                System.debug(e.getMessage());
                DidThrowException = true;
            }
            
            System.assertEquals(false,
                                DidThrowException, 
                                'Se ha generado una excepción no esperada. ' + messageEx);
                                
            try {
                List<Contact> contact = RQO_cls_TestDataFactory.createContact(1);
                List<Account> account = RQO_cls_TestDataFactory.createAccount(1);
                User u = RQO_cls_TestDataFactory.userPortalCreation('sfTestUserProvisioning@testorg.com', 'Customer Community User', null, 'es',
                                                           'es_ES', 'Europe/Berlin', contact[0].Id);
                                                           
                attributes.put('Account.RQO_fld_idExterno__c', account[0].RQO_fld_idExterno__c);
                
                RQO_cls_SAMLJitHandler handler = new RQO_cls_SAMLJitHandler();
                handler.updateUser(u.Id, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
    
            }
            catch (Exception e) {
                messageEx = e.getMessage();
                System.debug(e.getMessage());
                DidThrowException = true;
            }
            test.stopTest();
            
            System.assertEquals(false,
                                DidThrowException, 
                                'Se ha generado una excepción no esperada. ' + messageEx);
        }
    }
}