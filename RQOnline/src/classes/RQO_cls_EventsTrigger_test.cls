/**
*	@name: RQO_trg_EventsTrigger_test
*	@version: 1.0
*	@creation date: 28/02/2018
*	@author: Alvaro Alonso - Unisys
*	@description: Clase de test para probar la clase RQO_trg_EventsTrigger
*/
@isTest
public class RQO_cls_EventsTrigger_test {
    /**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método inicial de carga de datos que se comparten entre todos los testMethods
    * @exception: 
    * @throws: 
    */
    @testSetup 
    static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserEvents@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            RQO_cls_TestDataFactory.createContact(1);
        }
    }
    
    
	/**
    * @creation date: 28/02/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método test para comprobar la ejecución del trigger
    * @exception: 
    * @throws:  
    */
    @isTest
    static void triggerExecution(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserEvents@testorg.com');
        System.runAs(u){
			Account cuenta = [Select id from Account LIMIT 1];
            Contact contacto = [Select id from Contact LIMIT 1];
            test.startTest();
            	RQO_obj_event__c evento = new RQO_obj_event__c(RQO_fld_eventActionDefinition__c = '1', RQO_fld_eventActionType__c = '1', 
                                                           RQO_fld_eventClient__c = cuenta.Id,
                                                           RQO_fld_eventCompDesc__c = '1', RQO_fld_eventComponentName__c = '1', 
                                                           RQO_fld_eventContact__c = contacto.Id);
            	insert evento;
            test.stopTest();
            List<RQO_obj_event__c> listaObj = [Select Id from RQO_obj_event__c LIMIT 1];
			System.assert(listaObj != null);
            System.assert(!listaObj.isEmpty());
            System.assert(listaObj.size()>0);
        }
    }
}