/**
 * @creation date: 18/12/2017
 * @author: Julio Maroto - Unisys
 * @description: Clase que testea los métodos de la clase RQO_cls_Notificacion__c
 */
@isTest
public class RQO_cls_Notificacion_test {
    private static List<RQO_obj_notification__c> notificationList;
    private static Map<String, String> translationByGradeMap;
    private static Map<String, RQO_obj_posiciondeEntrega__c> pedidosByEntregaMap;
    private static Map<String, Asset> pedidoByOrderPositionMap;
    private static Map<String, Asset> pedidoByRequestPositionMap;
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que realiza la inicialización del dataset de ejemplo para la clase de test
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @testSetup
    public static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            createTranslations();
            createQp0Grades();
            createNotifications();
            createAssets();
            createPosicionDeEntregaList();
            createCommunicationList();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear traducciones.
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    private static void createTranslations() {
        RQO_cls_TestDataFactory.createTranslation(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Qp0Grades.
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    private static void createQp0Grades() {
        RQO_cls_TestDataFactory.createQp0Grade(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Notifications.
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createNotifications() {
        RQO_cls_TestDataFactory.createNotification(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Assets (posiciones de pedido).
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createAssets() {
        RQO_cls_TestDataFactory.createAsset(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Posiciones de Entrega
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createPosicionDeEntregaList() {
        RQO_cls_TestDataFactory.createPosiciondeEntrega(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Comunicaciones
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createCommunicationList() {
        RQO_cls_TestDataFactory.createCoommunications();
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear el método getNotificationsCategories de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetNotificationsCategories() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            // Se obtiene el mapa constante de tipos de notificaiones
            Map<String, String> mapNotificationTypeExpected = new Map<String, String>();
            Schema.DescribeFieldResult fieldResult = RQO_obj_notification__c.RQO_fld_objectType__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple){
                mapNotificationTypeExpected.put(f.getValue(), f.getLabel());
            }

            Map<String, String> mapNotificationTypeActual = RQO_cls_Notificacion_cc.getNotificationsCategories();

            System.assertEquals(mapNotificationTypeExpected, mapNotificationTypeActual);
            System.assertEquals( mapNotificationTypeExpected.size(), mapNotificationTypeActual.size() );
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear el método getNotificationsCategories de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetTranslationByGrade() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            List<RQO_obj_grade__c> gradeList = [SELECT Id FROM RQO_obj_grade__c];
            List<RQO_obj_qp0Grade__c> qp0GradeList = [SELECT Id FROM RQO_obj_qp0Grade__c];
            
            // Expected Response
            Map<String, String> translationByGradeMapExpected = new Map<String, String> ();

            for (RQO_obj_translation__c item : [SELECT Id, RQO_fld_translation__c, RQO_fld_Grade__c 
                                                    FROM RQO_obj_translation__c 
                                                    WHERE RQO_fld_Grade__c IN :gradeList])
            {
                translationByGradeMapExpected.put(item.RQO_fld_Grade__c, item.RQO_fld_Translation__c);
            }

            for (RQO_obj_qp0Grade__c itemQp0 : [SELECT Id, RQO_fld_descripcionDelGrado__c 
                                                    FROM RQO_obj_qp0Grade__c 
                                                    WHERE Id IN :qp0GradeList])
            {              
                translationByGradeMapExpected.put(itemQp0.Id, itemQp0.RQO_fld_descripcionDelGrado__c);
            }

            // Actual Response      
            List<String> gradeListIds = new List<String>();
            List<String> qp0GradeListIds = new List<String>();
            
            for (RQO_obj_grade__c grade : gradeList) {
                gradeListIds.add(grade.Id);
            }
            
            for (RQO_obj_qp0Grade__c qp0Grade : qp0GradeList) {
                qp0GradeListIds.add(qp0Grade.Id);
            }

            Map<String, String> translationByGradeMapActual = 
                RQO_cls_Notificacion_cc.getTranslationByGrade(gradeListIds, qp0GradeListIds);

            System.assertEquals(translationByGradeMapExpected, translationByGradeMapActual);
            System.assertEquals(translationByGradeMapExpected.size(), translationByGradeMapActual.size());
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear el método getPedidoByRequestPosition de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetPedidoByRequestPosition() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            List<String> requestPositionList = getRequestPositionList();
                
            // Se obtien el mapa de pedidos actual en base a una lista de peticiones de
            // posiciones de pedido
            Map<String, Asset> pedidoByRequestPositionMapActual = 
                RQO_cls_Notificacion_cc.getPedidoByRequestPosition(requestPositionList);
            
            List<Asset> assetList = [SELECT Id, RQO_fld_requestManagement__r.name FROM Asset WHERE RQO_fld_requestManagement__r.name != null];
            
            // Se realizan las comparaciones mediante los assert
            // La key del mapa coincide con el Request Management name establecido y peticionado en la query superior
            String expectedRequestManagamentNameOfAsset = assetList[0].RQO_fld_requestManagement__r.name;
            
            Asset asset = pedidoByRequestPositionMapActual.get(expectedRequestManagamentNameOfAsset);
            
            String actualRequestManagamentNameOfAsset = asset.RQO_fld_requestManagement__r.name;
                
            System.assertEquals(expectedRequestManagamentNameOfAsset, actualRequestManagamentNameOfAsset);
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para obtener peticiones de gestión de posiciones de pedido
    * @param: 
    * @return: List<String> peticiones de gestión de posiciones de pedido
    * @exception: 
    * @throws: 
    */
    
    private static List<String> getRequestPositionList() {                                    
        List<String> requestPositionList = new List<String>();
        List<Asset> assetList = [SELECT Id, RQO_fld_requestManagement__r.name FROM Asset WHERE RQO_fld_requestManagement__r.name != null];
        requestPositionList.add(assetList[0].RQO_fld_requestManagement__r.name);

        return requestPositionList;
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para obtener peticiones de gestión de posiciones de pedido
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetPedidoByOrderPosition() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            List<String> orderPositionList = getOrderPositionList();

            Map<String, Asset> pedidoByOrderPositionMapActual =
                RQO_cls_Notificacion_cc.getPedidoByOrderPosition(orderPositionList);

            List<Asset> assetList = [SELECT Id, RQO_fld_idExterno__c, RQO_fld_idRelacion__c, RQO_fld_idRelacion__r.RQO_fld_idExterno__c, RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c
                           FROM Asset WHERE RQO_fld_idExterno__c != null];
            
            // Se realizan las comparaciones mediante los assert
            String expectedIdExternoOfAsset = assetList[0].RQO_fld_idExterno__c;
            
            Asset asset = pedidoByOrderPositionMapActual.get(expectedIdExternoOfAsset);
            
            String actualIdExternoOfAsset = asset.RQO_fld_idExterno__c;
                
            System.assertEquals(expectedIdExternoOfAsset, actualIdExternoOfAsset);
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para obtener lista de posiciones de órdenes
    * @param: 
    * @return: List<String> lista de posiciones de órdenes
    * @exception: 
    * @throws: 
    */

    private static List<String> getOrderPositionList() {
        List<Asset> assetList = [SELECT Id, RQO_fld_idExterno__c FROM Asset];
        
        Integer counter = 0;
        for (Asset asset : assetList) {
            asset.RQO_fld_idExterno__c = '' + counter;
            counter++;
        }

        UPDATE assetList;
    
        List<String> orderPositionList = new List<String>();
        
        assetList = [SELECT Id, RQO_fld_idExterno__c, RQO_fld_idRelacion__c, RQO_fld_idRelacion__r.RQO_fld_idExterno__c, RQO_fld_idRelacion__r.RQO_fld_fechaPreferentedeEntrega__c FROM Asset WHERE RQO_fld_idExterno__c != null];
        orderPositionList.add(assetList[0].RQO_fld_idExterno__c);

        return orderPositionList;
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear el método getPedidoByEntrega de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetPedidoByEntrega() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            List<String> entregasList = getEntregasList();

            Map<String, RQO_obj_posiciondeEntrega__c> pedidosByEntregaMapActual =
                    RQO_cls_Notificacion_cc.getPedidoByEntrega(entregasList);
            
            List<RQO_obj_posiciondeEntrega__c> posicionDeEntregaList = [SELECT Id, RQO_fld_idRelacion__r.RQO_fld_idExterno__c FROM RQO_obj_posiciondeEntrega__c WHERE RQO_fld_idRelacion__r.RQO_fld_idExterno__c != null];
            
            // Se realizan las comparaciones mediante los assert
            String expectedIdRelacionIdExternoOfPosEntrega = posicionDeEntregaList[0].RQO_fld_idRelacion__r.RQO_fld_idExterno__c;
            
            RQO_obj_posiciondeEntrega__c posicionDeEntrega =
                pedidosByEntregaMapActual.get(expectedIdRelacionIdExternoOfPosEntrega);
            
            String actualIdRelacionIdExternoPosEntrega = posicionDeEntrega.RQO_fld_idRelacion__r.RQO_fld_idExterno__c;
                
            System.assertEquals(expectedIdRelacionIdExternoOfPosEntrega, actualIdRelacionIdExternoPosEntrega);
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para obtenre la lista de posiciones de entrega
    * @param: 
    * @return: List<String> posiciones de entrega
    * @exception: 
    * @throws: 
    */

    private static List<String> getEntregasList() {
        List<String> idRelacionIdExternoList = new List<String>();
        
        List<RQO_obj_posiciondeEntrega__c> posicionEntregaList = [SELECT Id, RQO_fld_idRelacion__r.RQO_fld_idExterno__c FROM RQO_obj_posiciondeEntrega__c WHERE RQO_fld_idRelacion__r.RQO_fld_idExterno__c != null];
        
        idRelacionIdExternoList.add(posicionEntregaList[0].RQO_fld_idRelacion__r.RQO_fld_idExterno__c);

        return idRelacionIdExternoList;
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método de test para probar el método getPosicionEntregaViewData de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testGetPosicionEntregaViewData() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            RQO_cls_NotificacionBean notification = new RQO_cls_NotificacionBean();
            
            List<String> entregasList = getEntregasList();
            
            Map<String, RQO_obj_posiciondeEntrega__c> pedidosByEntregaMapActual =
                RQO_cls_Notificacion_cc.getPedidoByEntrega(entregasList);
            
            RQO_cls_Notificacion_cc.getPosicionEntregaViewData(entregasList[0], notification, pedidosByEntregaMapActual);
            
            System.assert(notification.identificadorRedirect != null);
            System.assert(notification.dateRedirect != null);
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método de test para probar el método mapearLiteral de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testMapearLiteral() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            notificationList = [SELECT Id, RQO_fld_externalId__c FROM RQO_obj_notification__c];
            translationByGradeMap = getTranslationByGradeMap();
            
            List<String> entregasList = getEntregasList();   
            pedidosByEntregaMap = RQO_cls_Notificacion_cc.getPedidoByEntrega(entregasList);
                
            List<String> orderPositionList = getOrderPositionList();    
            pedidoByOrderPositionMap = RQO_cls_Notificacion_cc.getPedidoByOrderPosition(orderPositionList);
                
            List<String> requestPositionList = getRequestPositionList();
            pedidoByRequestPositionMap = RQO_cls_Notificacion_cc.getPedidoByRequestPosition(requestPositionList);

            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_PED_TRATAMIENTO);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_PED_CONFIRMADO);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_PROD_NOTA_TECNICA);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_PROD_CERT_GRADO);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_PROD_RECOM_GRADO);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_PROD_NEW_GRADO);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_PED_TRANSITO);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_PED_ENTREGADO);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_FAC_NEW_VIEW);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_FAC_PROX_VENCIMIENTO_VIEW);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_FAC_VENCIDA_VIEW);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_PED_SOLICITADO);
            mapearLiteralByNotificationMessageType(RQO_cls_Constantes.COD_EQUIPO_COMERCIAL_VIEW);
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar hacer llamadas parametrizadas a mapearLiteral
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void mapearLiteralByNotificationMessageType(String notificationMessageType) {          
        RQO_cls_NotificacionBean notificationBean = new RQO_cls_NotificacionBean();
  
        // mapearLiteral mediante "NOTIFICATION MESSAGE TYPE"
        notificationList[0].RQO_fld_messageType__c = notificationMessageType;
        UPDATE notificationList[0]; 
        RQO_cls_Notificacion_cc.mapearLiteral(notificationList[0], notificationBean, translationByGradeMap, pedidosByEntregaMap, pedidoByOrderPositionMap, pedidoByRequestPositionMap);
        
        System.assert(false == notificationMessageType.equals(''));
    }
    
        /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método auxiliar para obtener traducciones de grados en un mapa
    * @param: 
    * @return: Map<String, String> mapa de traducciones de grados
    * @exception: 
    * @throws: 
    */
    
    private static Map<String, String> getTranslationByGradeMap() {
        List<RQO_obj_grade__c> gradeList = [SELECT Id FROM RQO_obj_grade__c];
        
        Map<String, String> translationByGradeMap = new Map<String, String> ();
        
        for (RQO_obj_translation__c item : [SELECT
                                                Id,
                                                RQO_fld_translation__c,
                                                RQO_fld_Grade__c 
                                            FROM
                                                RQO_obj_translation__c 
                                            WHERE
                                                RQO_fld_Grade__c IN :gradeList])
        {
            translationByGradeMap.put(item.RQO_fld_Grade__c, item.RQO_fld_Translation__c);
        }
        
        return translationByGradeMap;
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear getRecordsNotification de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testGetRecordsNotification() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            List<RQO_obj_notification__c> notificationList = [SELECT Id, RQO_fld_contact__c, RQO_fld_externalId__c, RQO_fld_messageType__c, RQO_fld_objectType__c, RQO_fld_visualized__c, CreatedDate FROM RQO_obj_notification__c];
            
            // COMIENZO DE OBTENCION DATOS NECESARIOS LLAMADA FUNCION //
            Id contactId = notificationList[0].RQO_fld_contact__c;
            String objectType = notificationList[0].RQO_fld_objectType__c;
            Boolean visualized = notificationList[0].RQO_fld_visualized__c;
            
            String startDate = '1996-01-20 00:00:01';
            String endDate = '2020-01-20 00:00:01';
            
            // FIN DE OBTENCION DATOS NECESARIOS LLAMADA FUNCION //
            
            notificationList = RQO_cls_Notificacion_cc.getRecordsNotification(contactId, objectType, startDate, endDate, visualized);
            System.assertEquals(false, notificationList[0].RQO_fld_visualized__c);
            
            visualized = true;
            notificationList = RQO_cls_Notificacion_cc.getRecordsNotification(contactId, objectType, startDate, endDate, visualized);
            System.assertEquals(false, notificationList[0].RQO_fld_visualized__c);
            
            
            Test.stopTest();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear getLastNotifications de la clase RQO_cls_Notificacion__c
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    @isTest
    public static void testGetLastNotifications() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            List<RQO_obj_notification__c> notificationList = [SELECT Id, RQO_fld_contact__c, RQO_fld_externalId__c, RQO_fld_messageType__c, RQO_fld_objectType__c, RQO_fld_visualized__c, CreatedDate FROM RQO_obj_notification__c];
            Id contactId = notificationList[0].RQO_fld_contact__c;
            
            String result = RQO_cls_Notificacion_cc.getLastNotifications(contactId);
            
            System.assert(false == result.equals(''));
        }
    }
    
    /**
    * @creation date: 27/02/2018
    * @author: Alfonso Constán López - Unisys
    * @description:  Método para testear el método de obtención de notificaciones
    * @exception: 
    * @throws:  
    */
    
    @isTest
    public static void getNotificationsTest(){
        
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            
            Test.startTest();
            
            List<RQO_obj_notification__c> notificationList = [SELECT Id, RQO_fld_contact__c, RQO_fld_externalId__c, RQO_fld_messageType__c, RQO_fld_objectType__c, RQO_fld_visualized__c, CreatedDate 
                                                              FROM RQO_obj_notification__c];    
            
            // COMIENZO DE OBTENCION DATOS NECESARIOS LLAMADA FUNCION //
            Id contactId = notificationList[0].RQO_fld_contact__c;
            String objectType = notificationList[0].RQO_fld_objectType__c;
            Boolean visualized = notificationList[0].RQO_fld_visualized__c;
            
            String startDate = '1996-01-20 00:00:01';
            String endDate = '2020-01-20 00:00:01';
            
            String jsonNotification = RQO_cls_Notificacion_cc.getNotifications(contactId, objectType, startDate, endDate, visualized);
            
            System.assertNotEquals('', jsonNotification );
            Test.stopTest();            
        }                
    }
    
}