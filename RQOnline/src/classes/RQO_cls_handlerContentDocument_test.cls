/**
*   @name: RQO_cls_handlerContentDocument_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_handlerBeforeContentDocumentLink asociada al trigger RQO_trg_ContentDocumentLink.
*/
@isTest
public class RQO_cls_handlerContentDocument_test {
	
    /**
    *   @name: whenHandlerBeforeContentDocumentLinkReturnsOk
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la asignación de visibilidad a un ContentDocumentLink.
    */
    private static testMethod void whenHandlerBeforeContentDocumentLinkReturnsOk() {
        User userRunAs = RQO_cls_TestDataFactory.userCreation('sfTestUserClienteRunAsJob@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
		
		System.runAs(userRunAs){
            Boolean DidThrowException = false;
            String exceptionMessage = '';
            
            test.startTest();
    
            ContentDocumentLink cdl = new ContentDocumentLink();
            Id contentDocumentLinkId = fflib_IDGenerator.generate(ContentDocumentLink.SObjectType);
			cdl.ContentDocumentId = contentDocumentLinkId;
            Id communicationId = fflib_IDGenerator.generate(RQO_obj_Communication__c.SObjectType);
			cdl.LinkedEntityId = communicationId;
			cdl.ShareType = 'V';
            
			try {
            	RQO_cls_handlerBeforeContentDocumentLink.handlerBeforeContentDocumentLink(cdl);                   
            }
            catch (Exception e) {
                exceptionMessage = e.getMessage();
                DidThrowException = true;
            }
            
            System.assertEquals(false,
                                DidThrowException, 
                                'Se ha generado una excepción no esperada. ' + exceptionMessage);
    
            test.stopTest();
        }
    }
}