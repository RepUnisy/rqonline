/**
 *	@name: RQO_cls_CancelRequestByDate_batch
 *	@version: 1.0
 *	@creation date: 05/03/2018
 *	@author: Alvaro Alonso - Unisys
 *	@description: Clase batch para gestionar la cancelación de posiciones de solicitud en SAP con la fecha preferente de entrega vencida
 *	@testClass: RQO_cls_CancelRequestByDateBatch_test
*/
global class RQO_cls_CancelRequestByDate_batch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    private static final String CLASS_NAME = 'RQO_cls_CancelRequestByDate_batch';
    
    /**
	* @creation date: 05/03/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método start del batch. Realiza la query que obtiene las posiciones de soliciutd que queremos enviar a SAP
	* @param: Database.BatchableContext
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
    global Database.QueryLocator start(Database.BatchableContext BC){

    	Date fechaActual = Date.today();
        Date fechaInicio = Date.today().addMonths(-1);
    	String status = RQO_cls_Constantes.PED_COD_STATUS_SOLICITADO;
        Recordtype rtType = [Select Id from Recordtype where DeveloperName = : RQO_cls_Constantes.COD_RECORDTYPE_DEVNAME_ASSET_REQUEST_POSITION];
        Id  rtTypeId = rtType.Id;
        
        String query = 'Select Id from Asset where RecordTypeId = : rtTypeId and RQO_fld_positionStatus__c = : status and RQO_fld_childCreateds__c = false ' +
            ' and RQO_fld_cuDeliveryDate__c < : fechaActual and RQO_fld_cuDeliveryDate__c > : fechaInicio ';
        
        return Database.getQueryLocator(query);
    }
    
	/**
	* @creation date: 05/03/2018
	* @author: Alvaro Alonso - Unisys
	* @description:	Método execute del batch.
	* Envía las posiciones de solicitud con el handler ya existente
	* @param: Database.BatchableContext
	* @param: List<sObject>
	* @return: Database.QueryLocator	
	* @exception: 
	* @throws: 
	*/
   	global void execute(Database.BatchableContext BC, List<sObject> scope){
		final String METHOD = 'execute';
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
        List<Id> canceledAssets = new List<Id>();
        for(Asset requestPositions : (List<Asset>) scope) {
			canceledAssets.add(requestPositions.Id);
        }
        
		RQO_cls_SendOrder sendOrderClass = new RQO_cls_SendOrder();
        if (!Test.isRunningTest()){
            RQO_cls_SendOrder.SendOrderCustomResponse returnData = sendOrderClass.sendPositions(null, canceledAssets, RQO_cls_Constantes.REQ_WS_PROCESSTYPE_CANCEL);
        }   
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
   	}
    
    /**
    * @creation date: 05/03/2018
    * @author: Alvaro Alonso - Unisys
    * @description:	Método finish del batch.
    * @param: Database.BatchableContext
    * @return: 	
    * @exception: 
    * @throws: 
    */
    global void finish(Database.BatchableContext BC){}
}