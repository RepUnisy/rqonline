/**
 *	@name: RQO_cls_FacturaBean
 *	@version: 1.0
 *	@creation date: 29/11/2017
 *	@author: David Iglesias - Unisys
 *	@description: Bean de facturas.
 */
public class RQO_cls_FacturaBean implements Comparable{
    
    public String numFactura {get; set;}
    public Date fechaEmision {get; set;}
    public Date fechaVencimiento {get; set;}
    public String importe {get; set;}
    public String importeFormateado {get; set;}
    public String tipoMoneda {get; set;}
    public String tipo {get; set;}
    public String estado {get; set;}
    public Boolean error {get; set;}
    public String mensajeError {get; set;}
    public String idArchivado {get; set;}
    public List <RQO_cls_DetalleFacturaWrapper> detallesList {get; set;}

    /**
     *	@name: RQO_cls_DetalleFacturaWrapper
     *	@version: 1.0
     *	@creation date: 29/11/2017
     *	@author: David Iglesias - Unisys
     *	@description: Bean de detalle de facturas.
     */
    public class RQO_cls_DetalleFacturaWrapper {
        
        public String numRefCliente {get; set;}
        public String numPedido {get; set;}
        public String numAlbaran {get; set;}
        public String grado {get; set;}
        public String cantidad {get; set;}
        public String tipoCantidad {get; set;}
        public String precioTonelada {get; set;}
        public String precioToneladaFormateado {get; set;}
        public String importe {get; set;}
        public String importeNetoFormateado {get; set;}
        public String destino {get; set;}
        
    }
    
	/**
    * @creation date: 12/12/2017
    * @author: Alvaro Alonso Trinidad - Unisys
    * @description: implementación del método compareTo para ordenar la lista de objetos
    * @param: compareTo tipo Object
    * @return: Integer
    * @exception: 
    * @throws: 
    */
	public Integer compareTo(Object compareTo) {
        RQO_cls_FacturaBean compareToEmp = (RQO_cls_FacturaBean)compareTo;
        if (RQO_cls_Constantes.FAC_ORDER_BY_FECHA_EMISION){
			if (fechaEmision == compareToEmp.fechaEmision) return 0;
        	if (fechaEmision > compareToEmp.fechaEmision) return -1;
        }else if(RQO_cls_Constantes.FAC_ORDER_BY_FECHA_VENCIMIENTO){
			if (fechaVencimiento == compareToEmp.fechaVencimiento) return 0;
            if (fechaVencimiento > compareToEmp.fechaVencimiento) return -1;
        }

        return 1;
    }
}