@isTest
public class ScheduledDesactiveUserTest {

    static testMethod void afterInsert() {
            
        Profile pfl = [select id,name from profile where name='Contract Manager'];
        User testUser = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'u1',
            timezonesidkey='America/Los_Angeles', username='acf1@testorg.com' , FederationIdentifier='acf1@testorg.com' );
          
        insert(testUser);    
        
        scheduledDesactiveUser.daysFromEmail = 0;
        scheduledDesactiveUser.daysFromDesactive = 0;
        
        scheduledDesactiveUser.desactiveUsers();
    }
        
}