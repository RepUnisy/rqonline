/*------------------------------------------------------------------------
Author:         Rubén Simarro Alcaide
Company:        Indra
Description:    
History
<Date>          <Author>                      <Description>
28-Oct-2016     Rubén Simarro Alcaide         Version inicial
----------------------------------------------------------------------------*/
global with sharing  class ITPM_Controller_FichasProject_PDF {
          
    private String projectAreaElegida = ApexPages.currentPage().getParameters().get('projectAreaElegida');
    private String projectSubareaElegida = ApexPages.currentPage().getParameters().get('projectSubareaElegida');

    public List<ITPM_ProjectInvestmentItem__c> TotalProjectInvestmentItems;
    public List<ITPM_UBC_Project__c> TotalProjectUBCs;
    //public List<ITPM_Status_Report__c> TotalStatusReport; 
           
    public List<ITPM_ProjectInvestmentItem__c> getTotalProjectInvestmentItems(){  
        return TotalProjectInvestmentItems;
    }   
        
    public List<ITPM_UBC_Project__c> getTotalProjectUBCs(){  
        return TotalProjectUBCs;
    } 
    
    /*public List<ITPM_Status_Report__c> getTotalStatusReport(){  
        return TotalStatusReport;
    } */
    
    public List<ITPM_Project__c> lstProjectsDetails{get; set;} 

    public ITPM_Controller_FichasProject_PDF(){
             
       Apexpages.currentPage().getHeaders().put('content-disposition','filename=ProjectDetails_'+projectAreaElegida+'_'+projectSubareaElegida+'.pdf');
       
       Date myDate = Date.today();
       Date newDate = mydate.addDays(-30);
           
       TotalProjectInvestmentItems = [SELECT ITPM_REL_Investment_Item__r.Name, ITPM_REL_Project__r.Id FROM ITPM_ProjectInvestmentItem__c WHERE ITPM_REL_Project__c != null];  
       TotalProjectUBCs = [SELECT Name, ITPM_REL_Project__c, ITPM_FOR_Business_Country_Name__c, ITPM_SEL_Delivery_Country__c FROM ITPM_UBC_Project__c WHERE ITPM_REL_Project__c != null];
       //TotalStatusReport = [SELECT Name, ITPM_TX_Description__c, ITPM_DT_Report_date__c, ITPM_REL_Project__r.Id FROM ITPM_Status_Report__c WHERE ITPM_REL_Project__c != null AND ITPM_DT_Report_date__c >: newDate];
        
        String SOQL_projectDetail = 'select '+
            ' Id,'+
            ' Name,'+
            ' ITPM_TX_Project_Name__c,'+     
            ' ITPM_Project_Phase__c,'+
            ' ITPM_SEL_project_manager__r.name,'+
            ' ITPM_Project_TX_Description__c,'+
            ' ITPM_Project_TXT_Justif_scope_descrip__c, '+
            ' ITPM_Project_TXT_Objectives__c, '+
            ' ITPM_Project_Status_Date__c, '+             
            ' ITPM_DT_Planned_Start_Date__c, '+  
           
            ' ITPM_DT_Planned_Finish_Date__c, '+      
            ' ITPM_Project_NUM_Time_Deviation__c, '+  
            ' ITPM_Project_NUM_Cost_Deviation__c, '+  
            ' ITPM_Project_SEL_Final_cost__c, '+ 
            ' ITPM_Project_SEL_Executing_area__c, '+  
            ' ITPM_Project_SEL_Executing_subarea__c, '+  
            ' ITPM_Project_SEL_Maintenance_Area__c, '+  
            ' ITPM_Project_SEL_Maintenance_subarea__c, '+  
            ' ITPM_Project_BLN_New_service__c, '+ 
            ' ITPM_Project_TX_Last_Status_Report__c, '+  
            ' ITPM_PROJ_DT_Actual_Finish_Date__c, '+ 
            ' ITPM_PROJ_DT_Actual_Go_Live_Date__c, '+
            ' ITPM_PROJ_DT_Actual_Start_Date__c '+                          
            ' FROM ITPM_project__c ';
        
           try{
         
               boolean hayyawhere = false;                                                        
   
               if ( projectAreaElegida != null && !String.isEmpty(projectAreaElegida)) {                                                          
                   if(hayyawhere){                                                            
                       SOQL_projectDetail = SOQL_projectDetail + 'AND ITPM_Project__c.ITPM_Project_SEL_Executing_area__c ='+'\''+  projectAreaElegida+'\'';                                                       
                   }                                                      
                   else{                                                            
                       SOQL_projectDetail = SOQL_projectDetail + ' where ITPM_Project__c.ITPM_Project_SEL_Executing_area__c ='+'\''+  projectAreaElegida+'\'';                                                          
                       hayyawhere=true;                                                       
                   }                                                          
               }        
               if ( projectSubAreaElegida != null && !String.isEmpty(projectSubAreaElegida)) {                                                          
                   if(hayyawhere){                                                            
                       SOQL_projectDetail = SOQL_projectDetail + 'AND ITPM_Project__c.ITPM_Project_SEL_Executing_subarea__c ='+'\''+  projectSubAreaElegida+'\'';                                                       
                   }                                                      
                   else{                                                            
                       SOQL_projectDetail = SOQL_projectDetail + ' where ITPM_Project__c.ITPM_Project_SEL_Executing_subarea__c ='+'\''+  projectSubAreaElegida+'\'';                                                          
                       hayyawhere=true;                                                       
                   }                                                          
               }       
                    
               lstProjectsDetails = Database.Query(SOQL_projectDetail);

               system.debug('@@@ ITPM_Controller_FichasProject_PDF SOQL: '+SOQL_projectDetail);   

        }
        catch(Exception e){       
            system.debug('@@@ ERROR ITPM_Controller_FichasProject_PDF '+e.getMessage());   
        }    
    }
    
}