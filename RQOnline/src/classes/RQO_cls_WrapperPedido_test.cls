/**
*   @name: RQO_cls_Faqs_cc
*   @version: 1.0
*   @creation date: 28/02/2018
*   @author: Julio Maroto - Unisys
*   @description: Clase que testea WrapperPedido
*/
@isTest
public class RQO_cls_WrapperPedido_test {
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que crea el conjunto inicial de datos necesario para la ejecución del test
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    @testSetup
    public static void initialSetup() {
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
        System.runAs(u){
            // Create Asset
            createAssets();
            // Create Grades
            createGrades();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que crea los assets (posiciones de pedido)
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    private static void createAssets() {
        RQO_cls_TestDataFactory.createAsset(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que crea los grados
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createGrades() {
        RQO_cls_TestDataFactory.createGrade(200);
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método setea un Id de Asset (id de posición de pedido)
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testSetId() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            List<Asset> assetList = [SELECT Id FROM Asset LIMIT 1];

            wrapperPedido.setId(assetList[0].Id);

            System.assertNotEquals(null, assetList);

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método obtien un Id de Asset (id de posición de pedido)
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetId() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            Id actualAssetId = wrapperPedido.getId();

            System.assertNotEquals('', actualAssetId);

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método obtien un Id de Grado
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetIdGrado() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            Id actualGradeId = wrapperPedido.getIdGrado();

            System.assertNotEquals('', actualGradeId);

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método obtien un numero de referencia de cliente
    * @param: 
    * @return:
    * @exception: 

    */

    @isTest
    public static void testGetnumRefCliente() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            String numRefCliente = wrapperPedido.getnumRefCliente();
            System.assertEquals(true, numRefCliente.equals(''));      
            
            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que obtiene una cantidad
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void getCantidad() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();

            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();
            
            wrapperPedido.setCantidad(200);
            decimal cantidad = wrapperPedido.getCantidad();

            System.assertEquals(200, cantidad);

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que setea una cantidad
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testSetCantidad() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            wrapperPedido.setCantidad(200);
            decimal cantidad = wrapperPedido.getCantidad();

            System.assertEquals(200, cantidad);

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que obtiene la cantidad de una unidad
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetCantidadUnidad() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            decimal cantidadUnidad = wrapperPedido.getCantidadUnidad();

            System.assertEquals(0, cantidadUnidad);

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que obtiene un código de estado
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetStatusCode() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            String statusCode = wrapperPedido.getStatusCode();
            System.assertEquals(true, statusCode.equals(''));
            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método obtiene la descripción de un estado
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetStatusDescription() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            String statusDescExpected = 'STATUS_DESC';

            wrapperPedido.setStatusDescription(statusDescExpected);
            String statusDescActual = wrapperPedido.getStatusDescription();

            System.assertEquals(false, statusDescActual.equals(''));
            System.assertEquals(statusDescExpected, statusDescActual);

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método obtiene la descripción de un estado
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testSetStatusDescription() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            String statusDescExpected = 'STATUS_DESC';

            wrapperPedido.setStatusDescription(statusDescExpected);
            String statusDescActual = wrapperPedido.getStatusDescription();

            System.assertEquals(false, statusDescActual.equals(''));
            System.assertEquals(statusDescExpected, statusDescActual);

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método obtiene el estado de una clase css
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetStatusCssClass() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();
            String statusCssResult = wrapperPedido.getStatusCssClass();
            System.assertEquals( true, statusCssResult.equals('') );

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que obtiene el nombre de un Qp0Grade
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetQp0gradeName() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            String qp0GradeNameResult = wrapperPedido.getQp0gradeName();

            System.assertEquals( true, qp0GradeNameResult.equals('') );

            Test.stopTest();
        }
    }

        /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método obtiene el Id de un grado QP0
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetQp0gradeId() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            String qp0GradeIdResult = wrapperPedido.getQp0gradeId();

            System.assertEquals( true, qp0GradeIdResult.equals('') );

            Test.stopTest();
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método obtiene un numero de pedido
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetNumPedido() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            String numPedidoResult = wrapperPedido.getNumPedido();

            System.assertEquals(true, numPedidoResult.equals('') );
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que testea la obtención de una Id de grado como un String 
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetGrado() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            String gradoResult = wrapperPedido.getGrado();

            System.assertEquals( true,gradoResult.equals('') );
        }
    }

    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que obtiene una fecha solicitada
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */

    @isTest
    public static void testGetFechaSolicitado() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Test.startTest();
            
            RQO_cls_WrapperPedido wrapperPedido = new RQO_cls_WrapperPedido();

            String fechaSolicitadoResult = wrapperPedido.getFechaSolicitado();

            System.assertEquals( null, fechaSolicitadoResult );
        }
    }
    
    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método test para testear la fecha de entrega
    * @exception: 
    * @throws: 
    */    
    static testMethod void testgetFechaEntrega(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            
            RQO_cls_WrapperPedido wp = new RQO_cls_WrapperPedido();
            String fechaEntrega = wp.getFechaEntrega();
            System.assertEquals(null, fechaEntrega);
            
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método test para testear la obtencion del nombre del modo de envio
    * @exception: 
    * @throws: 
    */    
    static testMethod void testGetModoEnvioName(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            
            RQO_cls_WrapperPedido wp = new RQO_cls_WrapperPedido();
            String envioName = wp.getModoEnvioName();
            System.assertEquals('', envioName);
            
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método test para testear la fecha preferente
    * @exception: 
    * @throws: 
    */    
    static testMethod void testGetSrcFechaPreferente(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            
            RQO_cls_WrapperPedido wp = new RQO_cls_WrapperPedido();
            String fechaPreferente = wp.getSrcFechaPreferente();
            System.assertEquals(null, fechaPreferente);
            
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método test para testear el numero de entregas
    * @exception: 
    * @throws: 
    */    
    static testMethod void testGetNumEntrega(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            
            RQO_cls_WrapperPedido wp = new RQO_cls_WrapperPedido();
            String numEntrega = wp.getNumEntrega();
            System.assertEquals(null, numEntrega);
            
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método test para testear si se puede mostrar el albaran
    * @exception: 
    * @throws: 
    */    
    static testMethod void testgetShowAlbaranCMR(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            
            RQO_cls_WrapperPedido wp = new RQO_cls_WrapperPedido();
            Boolean showAlbaran = wp.getShowAlbaranCMR();
            System.assertEquals(false, showAlbaran);
            
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método test para testear si se puede mostrar el albaran
    * @exception: 
    * @throws: 
    */    
    static testMethod void testGetShowCertificadoAnalisis(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            
            RQO_cls_WrapperPedido wp = new RQO_cls_WrapperPedido();
            Boolean showCertificado = wp.getShowCertificadoAnalisis();
            System.assertEquals(false, showCertificado);
            
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método test para testear la obtención del nombre de la solicitud
    * @exception: 
    * @throws: 
    */    
    static testMethod void testGetNameSolicitud(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            
            RQO_cls_WrapperPedido wp = new RQO_cls_WrapperPedido();
            String nameSolicitud = wp.getNameSolicitud();
            System.assertEquals('', nameSolicitud);
            
            test.stopTest();
        }
    }
    
        /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método test para testear la obtención del nombre de la solicitud
    * @exception: 
    * @throws: 
    */    
    static testMethod void testGetFechaCreacion(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            
            RQO_cls_WrapperPedido wp = new RQO_cls_WrapperPedido();
            Date nameSolicitud = wp.getFechaCreacion();
            System.assertEquals(null, nameSolicitud);
            
            test.stopTest();
        }
    }
    
    /**
    * @creation date: 23/01/2018
    * @author: Alfonso Constán López - Unisys
    * @description: Método test para testear la obtención del nombre del envase
    * @exception: 
    * @throws: 
    */    
    static testMethod void testGetEnvaseName(){
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserGestionPedido@testorg.com');
        System.runAs(u){
            
            test.startTest();
            
            RQO_cls_WrapperPedido wp = new RQO_cls_WrapperPedido();
            String nameEnvase = wp.getEnvaseName();
            System.assertEquals('', nameEnvase);
            
            test.stopTest();
        }
    }
    
    
}