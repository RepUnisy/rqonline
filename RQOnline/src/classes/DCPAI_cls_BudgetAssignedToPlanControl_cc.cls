/*------------------------------------------------------------------------
 * 
 *   @name:         DCPAI_cls_BudgetAssignedToPlanControl_cc
 *   @version:       1 
 *   @creation date:   30/06/2017
 *   @author:       Alfonso Constán López  -  Unisys
 *   @description:     Custom controller of visualforce page "DCPAI_vf_assignedToPlan"
 * 
----------------------------------------------------------------------------*/
public class DCPAI_cls_BudgetAssignedToPlanControl_cc {
    
    private List<ITPM_Budget_Investment__c> listBudgetInvestment = new List<ITPM_Budget_Investment__c>();
    private List<ITPM_Budget_Investment__c> listBudgetInvestmentWaitting = new List<ITPM_Budget_Investment__c>();
    private List<SObject> listCostItem = new List<ITPM_Budget_Item__c>();
    private Map<String, List<SObject>> mapCostItemDraft = new Map<String, List<SObject>>();
    private List<ITPM_UBC_Project__c> listaUBC_csv = new List<ITPM_UBC_Project__c> ();
    private Map<String, List<ITPM_UBC_Project__c>> mapUBCObtenido = new Map<String, List<ITPM_UBC_Project__c>>();
    private List<ITPM_UBC_Project__c> listUBCAlmacenado = new List<ITPM_UBC_Project__c>();    
    private Integer offset = 0;
    private Integer numberPage = 1;
    private Integer recordPage = 100;
    
    //constructor
    public DCPAI_cls_BudgetAssignedToPlanControl_cc(ApexPages.StandardController controller) {
        initialData();
    }   
    
    private void initialData(){
        offset = recordPage * (numberPage - 1);
        mapCostItemDraft = new Map<String, List<SObject>>();
        
        //  Obtenemos todas las iniciativas assigned
        listBudgetInvestment = [select id, 
                                Name, 
                                ITPM_BI_TX_Budget_Investment_name__c,                             
                                ITPM_BI_SEL_Responsible_IT_Direction__c, 
                                DCPAI_fld_Segmentation__c,
                                DCPAI_fld_Decision_Area__c,
                                ITPM_BIA_SEL_IT_Area__c,
                                ITPM_BIA_SEL_IT_Subarea__c, 
                                ITPM_BIA_TXA_Description__c
                                from ITPM_Budget_Investment__c 
                                where id in (select DCPAI_fld_Budget_Investment__c  from ITPM_Budget_Item__c where DCPAI_fld_Investment_Plan__c = : ApexPages.currentPage().getParameters().get('id') and DCPAI_fld_Assing_Status__c = : Label.DCPAI_CostItem_Status_Assigned and DCPAI_fld_Waiting_to_Assign__c = false )
                                LIMIT :recordPage OFFSET :offset
                               ];
        
        //  Obtenemos todas las iniciativas waitting
        listBudgetInvestmentWaitting = [select id, 
                                        Name, 
                                        ITPM_BI_TX_Budget_Investment_name__c,                             
                                        ITPM_BI_SEL_Responsible_IT_Direction__c, 
                                        DCPAI_fld_Segmentation__c,
                                        DCPAI_fld_Decision_Area__c,
                                        ITPM_BIA_SEL_IT_Area__c,
                                        ITPM_BIA_SEL_IT_Subarea__c, 
                                        ITPM_BIA_TXA_Description__c
                                        from ITPM_Budget_Investment__c 
                                        where id in (select DCPAI_fld_Budget_Investment__c from ITPM_Budget_Item__c where DCPAI_fld_Investment_Plan__c = : ApexPages.currentPage().getParameters().get('id') and DCPAI_fld_Assing_Status__c = : Label.DCPAI_CostItem_Status_Assigned and DCPAI_fld_Waiting_to_Assign__c = true )
                                       ];
        
        //  Obtenemos todos los cost item relaccionados
        listCostItem = [Select ITPM_SEL_Country__c, 
                        DCPAI_fld_Investment_Nature__c, 
                        ITPM_SEL_Year__c,
                        SUM(ITPM_FOR_Total_investment__c) totalInv, 
                        SUM(ITPM_FOR_Total_expense__c) totalExp,
                        DCPAI_fld_Code__c,
                        DCPAI_fld_Budget_Investment__c,
                        DCPAI_fld_Waiting_to_Assign__c 
                        from ITPM_Budget_Item__c where DCPAI_fld_Investment_Plan__c = : ApexPages.currentPage().getParameters().get('id') and DCPAI_fld_Assing_Status__c =: Label.DCPAI_CostItem_Status_Assigned
                        GROUP BY ITPM_SEL_Country__c, DCPAI_fld_Investment_Nature__c, DCPAI_fld_Code__c, DCPAI_fld_Waiting_to_Assign__c, DCPAI_fld_Budget_Investment__c, ITPM_SEL_Year__c
                        ORDER BY DCPAI_fld_Budget_Investment__c];                   

        //  Obtenemos la lista de UBCs
        List<ITPM_UBC_Project__c> listaUBC = [select id,
                                              name, 
                                              ITPM_SEL_Year__c, 
                                              ITPM_SEL_Delivery_Country__c, 
                                              ITPM_Percent__c, 
                                              ITPM_REL_UBC__r.Name, 
                                              ITPM_FOR_Business_Area_Name__c,
                                              ITPM_FOR_Business_Subarea_Name__c,
                                              ITPM_FOR_Business_Unit_Name__c,
                                              ITPM_FOR_Business_Country_Name__c,
                                              DCPAI_fld_Code_of_project__c,
                                              DCPAI_fld_Budget_Investment__c
                                              from ITPM_UBC_Project__c where (DCPAI_fld_Budget_Investment__c in :listBudgetInvestment) OR (DCPAI_fld_Budget_Investment__c in :listBudgetInvestmentWaitting)
                                              order by ITPM_SEL_Year__c, ITPM_SEL_Delivery_Country__c
                                             ];
        
        mapUBCObtenido = DCPAI_cls_GlobalClass.DCPAI_getMapUBCs(listaUBC);        
        
        budgetDetailMap = new Map<String, List<SObject>>();
        budgetDetailMapPending = new Map<String, List<SObject>>();
        
        List<SObject> listBudget = new List<SObject>(); 
        //  Relaccionamos los costes con sus ubcs y calculamos su coste 
        for (SObject bud : listCostItem ){
            String code = String.valueOf(bud.get('DCPAI_fld_Code__c'));
            if (code == 'null' || code == null){
                code = '';
            }
            String key = (String)bud.get('DCPAI_fld_Budget_Investment__c') + (String)bud.get('ITPM_SEL_Country__c') + (String)bud.get('ITPM_SEL_Year__c')+ code;
            List<ITPM_UBC_Project__c> listUBC = mapUBCObtenido.get(key);
            if (listUBC != null){
                
                for (ITPM_UBC_Project__c ubcAgrup : listUBC){
                    listUBCAlmacenado.add(ubcAgrup);
                    listBudget.add(bud);
                    calculate(bud, ubcAgrup, 100);
                } 
            }
            else{
                List<ITPM_UBC_Project__c> listUbcCreada = new List<ITPM_UBC_Project__c>();                
                ITPM_UBC_Project__c ubcVacia = new ITPM_UBC_Project__c();
                listUbcCreada.add(ubcVacia);
                
                mapUBCObtenido.put(key, listUbcCreada);
                listUBCAlmacenado.add(new ITPM_UBC_Project__c());
                listBudget.add(bud);
                calculate(bud, null, 1);
            }
            system.debug('listUBCFinal ' + listUBCFinal);
        }
        
        //  Repartimos los distintos cost item en su matriz correspondiente
        for(SObject detail: listCostItem){
            List<SObject> listaDet = new List<SObject>();
            if(detail.get('DCPAI_fld_Waiting_to_Assign__c') == false){
                if (budgetDetailMap.containsKey(String.valueOf(detail.get('DCPAI_fld_Budget_Investment__c')))){
                    listaDet = budgetDetailMap.get(String.valueOf(detail.get('DCPAI_fld_Budget_Investment__c')));
                    listaDet.add(detail);
                }
                else{
                    listaDet.add(detail);                      
                }    
                mapCostItemDraft.put( String.valueOf(detail.get('DCPAI_fld_Budget_Investment__c')), listaDet);
            }
            else{
                if (budgetDetailMapPending.containsKey(String.valueOf(detail.get('DCPAI_fld_Budget_Investment__c')))){
                    listaDet = budgetDetailMapPending.get(String.valueOf(detail.get('DCPAI_fld_Budget_Investment__c')));
                    listaDet.add(detail);     
                }
                else{
                    listaDet.add(detail);                      
                }    
                budgetDetailMapPending.put(String.valueOf(detail.get('DCPAI_fld_Budget_Investment__c')), listaDet);
            }
        }   
    }
    
    /*
     *   @creation date:     30/06/2017
     *   @author:         Alfonso Constán López  -  Unisys
     *  @description:      Versiona todos los budget, cost item y ubcs relaccionadas con el plan
     *  @param1:        bud: agrupación de costes
     *   @param2:        ubcAgrup: listado de ubc versionado
     *  @param3:        dividendo: controlamos si tiene que calcular la lista de costes
  */  
    private void calculate(SObject bud, ITPM_UBC_Project__c ubcAgrup, Integer dividendo){
        
        Decimal costExpense = (Decimal)bud.get('totalExp');
        Decimal costInvestment = (Decimal) bud.get('totalInv');
        Decimal costPro = costExpense + costInvestment;
        
        if(dividendo != 1){
            costExpense = costExpense * ubcAgrup.ITPM_Percent__c / dividendo;
            costInvestment = costInvestment * ubcAgrup.ITPM_Percent__c / dividendo;
            costPro = (costPro * ubcAgrup.ITPM_Percent__c / dividendo).setScale(2);
            String costePro = costPro.format();
            listCost.put(ubcAgrup.Id, costePro);
        } 
        listExpense.add(costExpense);
        listInvestment.add(costInvestment);
        
    }        
    
    public List<ITPM_Budget_Investment__c> listBudgetUser {
        get {
            return listBudgetInvestment;                        
        }
        set;
    }
    public String title{
        get{
            return Label.DCPAI_Investment_Plan ;
        }
        set;
    }
    public List<String> listaVacia {
        get{
            listaVacia = new List<String>();
            String elemento1 = 'elem1';
            listaVacia.add(elemento1);
            return listaVacia;
        }
        set;
    }
    public List<ITPM_UBC_Project__c> listUBCFinal{
        get{
            return listUBCAlmacenado;
        }
        set;
    }    
    public List<ITPM_Budget_Investment__c> listBudgetUserPending {
        get {
            return listBudgetInvestmentWaitting;            
        }
        set;
    }
    private Map<String, String> listCost = new Map<String, String>();
    private List<Decimal> listExpense = new List<Decimal>();
    private List<Decimal> listInvestment = new List<Decimal>();
    
    public boolean haslistBudgetUserPending {
        get{
            if (listBudgetInvestmentWaitting.size() != 0){
                return true;
            }
            else{
                return false;
            }
        }
        set;
    }
    
    //map of costItem <budget, costItem>
    public Map<String, List<SObject>> budgetDetailMap {
        get {
            return mapCostItemDraft;
        }
        set;
    }
    
    public Map<String, List<SObject>> budgetDetailMapPending {
        get {  
            return budgetDetailMapPending;
        }
        set;
    }
    public Map<String, List<ITPM_UBC_Project__c>> mapUBC{
        get{
            return mapUBCObtenido;
        }
        set;
    }
    
    public String totalBudget{
        get{
            
            List<ITPM_Budget_Investment__c> totalList = [select id, 
                                                         Name, 
                                                         ITPM_BI_TX_Budget_Investment_name__c,                             
                                                         ITPM_BI_SEL_Responsible_IT_Direction__c, 
                                                         DCPAI_fld_Segmentation__c,
                                                         DCPAI_fld_Decision_Area__c,
                                                         ITPM_BIA_SEL_IT_Area__c,
                                                         ITPM_BIA_SEL_IT_Subarea__c, 
                                                         ITPM_BIA_TXA_Description__c
                                                         from ITPM_Budget_Investment__c 
                                                         where id in (select DCPAI_fld_Budget_Investment__c  from ITPM_Budget_Item__c where DCPAI_fld_Investment_Plan__c = : ApexPages.currentPage().getParameters().get('id') and DCPAI_fld_Assing_Status__c = : Label.DCPAI_CostItem_Status_Assigned and DCPAI_fld_Waiting_to_Assign__c = false )                                                      
                                                        ];
            return String.valueOf(totalList.size());
        }
        set;
    }
    public SObject total {
        get {
            if(total != null) {
                return total;
            } else {
                
                total = [Select SUM(ITPM_FOR_Total_investment__c) totalInvest, SUM(ITPM_FOR_Total_expense__c )totalExp
                        from ITPM_Budget_Item__c  where DCPAI_fld_Investment_Plan__c = : ApexPages.currentPage().getParameters().get('id') and DCPAI_fld_Assing_Status__c = : Label.DCPAI_CostItem_Status_Assigned AND DCPAI_fld_Waiting_to_Assign__c = false];
                
                return total;
            }
        }
        set;
    }
    
    public String totalInvestment{
        get {
            totalInvestment = (Decimal.valueOf(String.valueOf(total.get('totalInvest')))).format();
            return totalInvestment;
        }
        set;
    }

    public String totalExpense{
        get {
            totalExpense = (Decimal.valueOf(String.valueOf(total.get('totalExp')))).format();
            return totalExpense;
        }
        set;
    }

    public String totalCost{
        get{
            totalCost = (Decimal.valueOf(String.valueOf(total.get('totalInvest'))) + Decimal.valueOf(String.valueOf(total.get('totalExp')))).format();
            return totalCost;
        }
        set;
    }

     public List<Decimal> listExpenseFinal{
        get{
            return listExpense;
        }
        set;
    }
    public List<Decimal> listInvestmentFinal{
        get{
            return listInvestment;
        }
        set;
    }
    public Map<String, String> listCostFinal{
        get{
            return listCost;
        }
        set;
    }
    
    public Integer totalNumberPage  {   
        get{
            
            Decimal pag = Decimal.valueOf(totalBudget)/recordPage;
            Integer numPag = pag.intValue();          
            if (pag.scale() != 0){
                numPag ++;
            }
            return numPag;
        }
        set;    
    }   
    public Integer actualPag  {
        get{
            return numberPage;
        }
        set{
            
        }
    }
    
    public Pagereference exportAll(){    
        Pagereference pageref = new Pagereference('/apex/DCPAI_vf_exportExcel');
        pageref.getParameters().put('id', ApexPages.currentPage().getParameters().get('id'));
        pageref.getParameters().put('procedencia', 'plan');
        return pageref;        
    }
    
    public PageReference Next() {   
        numberPage++;
        initialData();
        return null;        
    }
    public PageReference First() {   
        numberPage = 1;
        initialData();
        return null;        
    } 
    public PageReference Previous() {   
        numberPage--;
        initialData();
        return null;        
    }   
    public PageReference Last() {   
        numberPage = totalNumberPage;
        initialData();
        return null;        
    }  

   
    public boolean disabledPrevious{
        get{
            if (numberPage == 1){
                return false;
            }
            else{
                return true;
            }
        }
    }
    public boolean disabledNext{
        get{
            if (numberPage == totalNumberPage || totalNumberPage == 0){
                return false;
            }
            else{
                return true;
            }
        }
    }
          
}