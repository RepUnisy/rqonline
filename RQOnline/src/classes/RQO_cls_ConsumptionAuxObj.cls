/**
*	@name: RQO_cls_ConsumptionAuxObj
*	@version: 1.0
*	@creation date: 08/11/2017
*	@author: Alvaro Alonso - Unisys
*	@description: Bean Apex auxiliar accesible desde los componentes lightning que almacena los consumos que se van a mostrar por pantalla
*/
public class RQO_cls_ConsumptionAuxObj {
    // ***** PROPIEDADES ***** //
    public String grade{get;set;}
    
    public String family{get;set;}
    
    public String label{get;set;}
    
    public String translatedFamily{get;set;}
    
    public String familyColor{get;set;}
	
    public List<RQO_cls_ConsumptionAuxObjDate> dataList{get;set;}
    
    public Map<String, RQO_cls_ConsumptionAuxObjDate> dataMap{get;set;}
    	
    public class RQO_cls_ConsumptionAuxObjDate implements Comparable{

        public String yearConsum{get;set;}
        public String monthComsum{get;set;}
        public Integer monthSortData{get;set;}
        public String data{get;set;} //Cantidad formateada
        public Decimal cantidad{get;set;}
        public String cantidadKilos{get;set;}
        public String importe{get;set;}
        public Decimal importeSAP{get;set;}
        public String unidad{get;set;}
        public String moneda{get;set;}
        
		/**
        * @creation date: 18/12/2017
        * @author: Alvaro Alonso Trinidad - Unisys
        * @description: implementación del método compareTo para ordenar la lista de objetos
        * @param: compareTo tipo Object
        * @return: Integer
        * @exception: 
        * @throws: 
        */
        public Integer compareTo(Object compareTo) {
            RQO_cls_ConsumptionAuxObjDate compareToEmp = (RQO_cls_ConsumptionAuxObjDate)compareTo;
            if (monthSortData == compareToEmp.monthSortData) return 0;
            if (monthSortData > compareToEmp.monthSortData) return 1;
            return -1;
        }
    }
}