global with sharing class QUIM_ctrl_formularioContacto {

     public static QUIM_Form__c form {get; set;}
    public static List<String> TemaContacto {get; set;}
    public static List<String> PerfilContacto {get; set;}
    public static List<String> ProductoForm {get; set;}
    public static List<String> listttprueba{get; set;}
    
    global QUIM_ctrl_formularioContacto() {
        listttprueba=new List<String>();
        //Declaración de la lista en el con
        TemaContacto = new List<String>();
        Schema.DescribeFieldResult fieldResult = QUIM_Form__c.TemaContacto__c.getDescribe();
        List<Schema.PicklistEntry> plEntries = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : plEntries ){
            TemaContacto.add(f.getValue());
        }
        
        PerfilContacto = new List<String>();    
        Schema.DescribeFieldResult fieldResults = QUIM_Form__c.Perfil__c.getDescribe();
        List<Schema.PicklistEntry> plEntry = fieldResults.getPicklistValues();
        for( Schema.PicklistEntry a : plEntry )
        {
            PerfilContacto.add(a.getValue());
        }
        
        ProductoForm = new List<String>();    
        Schema.DescribeFieldResult fieldResultados = QUIM_Form__c.Producto__c.getDescribe();
        List<Schema.PicklistEntry> plEntrada = fieldResultados.getPicklistValues();
        for( Schema.PicklistEntry b : plEntrada )
        {
            ProductoForm.add(b.getValue());
        }
    } 
    
    //Funcion encargada de almacenar el formulario
    @RemoteAction
    global static String saveFormulario (String Name, String Descripcion, String Correo, String Asunto, String Telefono, String Tema, String Perfil,String Pais, String empresa, String Producto, Boolean sendCopy, Boolean isFaq, String Idioma){
        System.debug('Llamada método saveObject');
        form = new QUIM_Form__c();

       
        if(('es_ES').containsIgnoreCase(idioma) ){
            idioma ='ES';
        }else if (('en_US').containsIgnoreCase(idioma)){
            idioma ='EN';
        }else {
            idioma ='EN';
        }
        try{
            form.Nombre__c = Name;
            form.Descripcion__c = Descripcion;
            form.Correo__c = Correo;
            form.Asunto__c = Asunto;
            form.Telefono__c = Telefono;
            form.TemaContacto__c= Tema;
            form.Perfil__c= Perfil;
            form.Pais__c = Pais;
            form.Empresa__c = empresa;
            form.Producto__c = Producto;
            form.SendCopia__c =sendCopy;
            form.isFAQ__c = isFaq;
            form.Idioma__c = idioma;
            
            System.Debug('Inserta formulario');
            insert(form);
            System.Debug('Id formulario '+form.Id);
            return form.Id;
        }catch (exception e){
            System.Debug('Excepcion');
            return null;
        }   
  }
    
    @RemoteAction
    global static String doUploadAttachment(String formId, String attachmentBody, String attachmentName, String attachmentId){
            System.Debug('Esta ocurriendo la 2 remote action');
            if(formId != null) {
                if(attachmentBody != null) {
                    Attachment att = getAttachment(attachmentId);
                    String newBody = '';
                    if(att.Body != null) {
                        newBody = EncodingUtil.base64Encode(att.Body);
                    }
                    newBody += attachmentBody;
                    att.Body = EncodingUtil.base64Decode(newBody);
                    if(attachmentId == null) {
                        att.Name = attachmentName;
                        att.parentId = formId;
                    }
                    upsert att;
                    return att.Id;
                } else {
                    return 'Attachment Body was null';
                }               
                                               
            } else {
                return 'Form could not be found';
            }
                
         }

    
    private static Attachment getAttachment(String attId) {
        list<Attachment> attachmentSelect = [SELECT Id, Body FROM Attachment WHERE Id =: attId];
        if(attachmentSelect.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachmentSelect[0];
        }
    }
    
   
    
    
}