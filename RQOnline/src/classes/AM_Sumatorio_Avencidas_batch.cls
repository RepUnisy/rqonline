global class AM_Sumatorio_Avencidas_batch implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        
        String query = 'SELECT Id, Name,N_mero_AMejora_Asociadas__c,N_mero_AMejora__c FROM AM_Estacion_de_servicio__c';
        return Database.getQueryLocator(query);
        
        //lista de las estaciones de servicio
    
        
    }
 
    global void execute(Database.BatchableContext BC, List<AM_Estacion_de_servicio__c> lstEservicio)
    {
        List<AM_Accion_de_mejora__c> lstAMejora = new List <AM_Accion_de_mejora__c>([SELECT Id, Name,AM_REF_Estacion_de_servicio__c,AM_SEL_Estatus__c,AM_Accion_de_mejora_vencida__c,AM_TX_Diagnostico__c  FROM AM_Accion_de_mejora__c WHERE AM_REF_Estacion_de_servicio__c IN :lstEservicio]);
        //lista de las estaciones de servicio
      
        
        Map<Id, List<AM_Accion_de_mejora__c>> mapSortProj = new Map<Id, List<AM_Accion_de_mejora__c>>();
        
        for (AM_Accion_de_mejora__c aux : lstAMejora){
            //no contiene el mapa la estacion de servicio
            if(!mapSortProj.containsKey(aux.AM_REF_Estacion_de_servicio__c)){
            //no lo contiene
             system.debug('acciones de mejora ' + lstAMejora);
                List<AM_Accion_de_mejora__c> listaAuxiliar = new List <AM_Accion_de_mejora__c>();
                listaAuxiliar.add(aux);
                mapSortProj.put(aux.AM_REF_Estacion_de_servicio__c, listaAuxiliar);
            }
            else{
                //lo contiene, obtengo la estacion de servicio 
                List<AM_Accion_de_mejora__c> listaAuxiliar = new List <AM_Accion_de_mejora__c>(mapSortProj.get(aux.AM_REF_Estacion_de_servicio__c));
                listaAuxiliar.add(aux);
                //añado las acciones de mejora
                mapSortProj.put(aux.AM_REF_Estacion_de_servicio__c, listaAuxiliar);
            }   
        }
    //creo lista vacia
    List <AM_Estacion_de_servicio__c> lstEserviciocopia= new List <AM_Estacion_de_servicio__c>();
    //obtengo la lista con las acciones de Mejora y cuento cada una de las  que tiene una estacion
    for(AM_Estacion_de_servicio__c aux2 : lstEservicio){
        aux2.N_mero_AMejora__c=0;
        //obtengo las Acciones de mejora que tiene esa estacion
        //Incompatible key type AM_Estacion_de_servicio__c for Map<Id,List<AM_Accion_de_mejora__c>>
        List<AM_Accion_de_mejora__c> listAux = new List <AM_Accion_de_mejora__c> (mapSortProj.get(aux2.Id));
        //tengo la lista con todas las Acciones de Mejora
        
        for(AM_Accion_de_mejora__c aux3 : listAux){
                        
            if(aux3.AM_Accion_de_mejora_vencida__c==true){
                
                aux2.N_mero_AMejora__c=aux2.N_mero_AMejora__c + 1;
            }
        }
        
        
        
        //inserto el numero de Acciones de mejora asociadas
        //aux2.N_mero_AMejora_Asociadas__c=listAux.size();
        system.debug('estacion '+aux2.name + ' acciones '+ aux2.N_mero_AMejora__c);
        //añado la eservicio con campo N_mero_AMejora_Asociadas__c actualizado
        lstEserviciocopia.add(aux2);   
        
            
    }

update(lstEserviciocopia);


       
    }  
    global void finish(Database.BatchableContext BC)
    {
    }
}