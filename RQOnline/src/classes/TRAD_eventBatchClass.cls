global class TRAD_eventBatchClass implements Database.Batchable <sObject>{
    global final String query;
    global final Set<Id> idSet;
    
    global TRAD_eventBatchClass (String q, Set<Id> s){
        query = q;
        idSet = s;
    }
    
    // Start method
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Event>scope){
        Set<Id> idContacts = new Set<Id>();
        for(Event a:scope){
            if(a.WhoId != null){
                idContacts.add(a.WhoId);
            }
        }
        Map<Id, Contact> contactsMap = new Map<Id, Contact>([SELECT Id, AccountId FROM Contact WHERE Id IN: idContacts]);
        
        for(Event a:scope){
            a.Type = 'Meeting';
            system.debug('Related to: ' + a.WhatId);
            if(a.WhatId != null && a.WhatId.getSObjectType() == Schema.Account.SObjectType ){
                a.Activity_Account__c = a.WhatId;
            }else{
                if(a.WhoId != null){
                    a.Activity_Account__c = contactsMap.get(a.WhoId).AccountId;
                }
            }
        }    
        update scope;
    }
 
    global void finish(Database.BatchableContext BC){
        // Logic to be Executed at finish
    }
}