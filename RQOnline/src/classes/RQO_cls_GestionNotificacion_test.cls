/**
 * @creation date: 18/12/2017
 * @author: Julio Maroto - Unisys
 * @description: Clase que testea los métodos de la clase RQO_cls_Notificacion__c
 */
@isTest
public class RQO_cls_GestionNotificacion_test {
    private static List<Account> accountList = new List<Account>();
    private static List<Contact> contactList = new List<Contact>();
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método que realiza la inicialización del dataset de ejemplo para la clase de test
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
	@testSetup
    public static void initialSetup() {
        
        User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es',
                                             'es_ES','Europe/Berlin');
        System.runAs(u) {
            // Disable Triggers
            disableTriggers();
            // Create Account
            createAccount();
            // Create Contact
            createContact();
            // Create Notifications
            createNotifications();
            // Create Communications
            createCommunicationList();
        }
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear el usuario para el contexto sobre el que se ejecutan los test de la clase.
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createUser() {
        RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es',
                                             'es_ES','Europe/Berlin');
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para desactivar triggers durante la ejecución de las pruebas
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void disableTriggers() {
        RQO_cls_TestDataFactory.createCSActivacionTrigger(false);
        
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Crear Cuentas
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createAccount() {
        
        accountList = RQO_cls_TestDataFactory.createAccount(200);
        
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear contacto
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createContact() {
        
        Account account = accountList[0];
        
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear notificaciones
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createNotifications() {
        RQO_cls_TestDataFactory.createNotification(200);
        
    }
    
	/**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para crear Comunicaciones
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
    
    private static void createCommunicationList() {
        RQO_cls_TestDataFactory.createCoommunications();
    }
    
    /**
    * @creation date: 28/02/2018
    * @author: Julio Maroto - Unisys
    * @description: Método para testear el método countNotificacionesFound
    * @param: 
    * @return:
    * @exception: 
    * @throws: 
    */
	
    // Testing of 'countNotificaciones' method when there are notifications asigned to a user
    // The method call must return a value greater than 0
	@isTest
	public static void testCountNotificacionesFound() {
        User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserAgreement@testorg.com');
        
        System.runAs(u) {
            Contact contact = [
                SELECT
                    Id
                FROM
                    Contact
                LIMIT 1
            ];
            
            String contactIdParsedToString = String.valueOf(contact.Id);
                        
            RQO_obj_notification__c notification = [SELECT Id, RQO_fld_contact__c FROM RQO_obj_notification__c LIMIT 1];
            
            notification.RQO_fld_contact__c = contact.Id;
            update notification;
            
            Integer notificationsFoundResult = RQO_cls_GestionNotificacion_cc.countNotificaciones(contactIdParsedToString);
            
            System.assert(notificationsFoundResult > 0);
    	}
	}
}