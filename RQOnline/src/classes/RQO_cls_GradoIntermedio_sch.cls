/**
 *	@name: RQO_cls_SchedulerGradoIntermedio
 *	@version: 1.0
 *	@creation date: 12/09/2017
 *	@author: David Iglesias - Unisys
 *	@description: Clase para la programación del lanzamiento de RQO_cls_BatchGradoIntermedio
 */
global class RQO_cls_GradoIntermedio_sch extends RQO_cls_ProcesoPlanificado implements Schedulable {
    
    // ***** CONSTANTES ***** //
    private static final String JOB_NAME = 'GradoIntermedio';
    private static final String CLASS_NAME = 'RQO_cls_GradoIntermedio_sch';

	// ***** CONSTRUCTORES ***** //
	
	/**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Constructor por defecto
	* @param:
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public RQO_cls_GradoIntermedio_sch() {
		super(JOB_NAME);
	}

	// ***** METODOS GLOBALES ***** //
	
	/**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Executes the scheduled Apex job. 
	* @param:	sc	Contains the job ID
	* @return: 
	* @exception: 
	* @throws: 
	*/
	global void execute(SchedulableContext sc) {
        
        /**
		 * Constantes
		 */
		final String METHOD = 'execute';
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		if (RQO_cls_AsyncApexJob_Helper.getActiveJobs() < RQO_cls_AsyncApexJob_Helper.MAX_JOBS && isRunnable()) {
			//Lanzamos el proceso batch
			RQO_cls_GradoIntermedio_batch be = new RQO_cls_GradoIntermedio_batch();
			Id batchProcessId = Database.executeBatch(be, 200);
			System.debug('RQO_cls_ProcesoPlanificado -- Start at ' + System.Date.today());
		
			schedule(true);
		} else {
			schedule(false);
		}
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}

	// ***** METODOS PUBLICOS ***** //

	/**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Programar job para la siguiente ejecucion. 
	* @param:	blnForceNewDay	Forzado de inicio en el mismo dia
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public void schedule(Boolean blnForceNewDay) {
		
        /**
		 * Constantes
		 */
		final String METHOD = 'schedule';
        
        /**
		 * Variables 
		 */
		RQO_cls_ProcesoPlanificado.ScheduledInstance instance;
        
		/**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		instance = getSchedule(getNextScheduleDatetime(blnForceNewDay));
		System.schedule(instance.name, instance.cron, new RQO_cls_GradoIntermedio_sch());
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}

	/**
	* @creation date: 12/09/2017
	* @author: David Iglesias - Unisys
	* @description:	Lanzamiento del proceso de forma manual. 
	* @param:	
	* @return: 
	* @exception: 
	* @throws: 
	*/
	public void lanzarProceso() {
        
        /**
		 * Constantes
		 */
		final String METHOD = 'schedule';
        
        /**
		 * Inicio
		 */
        System.debug(CLASS_NAME + ' - ' + METHOD + ': INICIO');
        
		RQO_cls_GradoIntermedio_batch be = new RQO_cls_GradoIntermedio_batch();
		Id batchProcessId = Database.executeBatch(be, 200);
        
        System.debug(CLASS_NAME + ' - ' + METHOD + ': FIN');
	}
}