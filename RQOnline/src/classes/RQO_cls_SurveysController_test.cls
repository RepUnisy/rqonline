/**
*   @name: RQO_cls_SurveysController_test
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase de test para comprobar el funcionamiento de la clase RQO_cls_SurveysController_cc.
*/
@IsTest
public class RQO_cls_SurveysController_test {
	/**
    *   @name: RQO_cls_SurveysController_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método inicial de carga de datos que se comparten entre todos los testMethods.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	@testSetup static void initialSetup() {
	//	User u = RQO_cls_TestDataFactory.userCreation('sfTestUserAgreement@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
     //   System.runAs(u){
            // Crear encuenta con una pregunta y una respuesta para hoy
            RQO_cls_TestDataFactory.createSurveys();
	//	}
	}
	
    /**
    *   @name: RQO_cls_SurveysController_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba que no devuelve encuestas de la home para perfiles incorrectos.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	@isTest
    public static void WhenSelectLastActiveHomeDoesNotReturn() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSurveys@testorg.com');
        System.runAs(u){
            RQO_obj_surveyQuestion__c question = 
                RQO_cls_SurveysController_cc.selectLastActiveHome('WrongClient', 'WrongUser', null);
    	
		System.assertEquals(null,
							question,
    						'Se devuelven datos no esperados.');
        }
    }
 	
    /**
    *   @name: RQO_cls_SurveysController_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba que no devuelve encuesta para un id ficticio.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	@isTest
    public static void WhenSelectSurveyByIdDoesNotReturn() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSurveys@testorg.com');
        System.runAs(u){
            Id surveyId = fflib_IDGenerator.generate(RQO_obj_survey__c.SObjectType);
                
            RQO_obj_survey__c survey = 
                RQO_cls_SurveysController_cc.selectSurveyById(surveyId);
    	
		System.assertEquals(null,
							survey,
    						'Se devuelven datos no esperados.');
        }
 	}
 	
    /**
    *   @name: RQO_cls_SurveysController_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba que no devuelve preguntas para un Id de encuesta ficticio.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	@isTest
    public static void WhenSelectQuestionsDoesNotReturn() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSurveys@testorg.com');
        System.runAs(u){
            Id surveyId = fflib_IDGenerator.generate(RQO_obj_survey__c.SObjectType);
                
            List<RQO_obj_surveyQuestion__c> questions = 
                RQO_cls_SurveysController_cc.selectQuestions(surveyId);
         
		System.assertEquals(null,
							questions,
    						'Se devuelven datos no esperados.');
            }
 	}
 	
    /**
    *   @name: RQO_cls_SurveysController_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la generación de excepción cuando se envía un json de respuestas correctas.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	@isTest
    public static void WhenAddResponseThrowsException() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSurveys@testorg.com');
        System.runAs(u){
            Boolean DidThrowException = false;
            
            try {
                
                Id idSurvey = fflib_IDGenerator.generate(RQO_obj_survey__c.SObjectType);
                Id idQuestion = fflib_IDGenerator.generate(RQO_obj_surveyQuestion__c.SObjectType);
                Id idResponse = fflib_IDGenerator.generate(RQO_obj_surveyResponse__c.SObjectType);
                
                RQO_cls_SurveysController_cc.addResponse('Testing', idSurvey, idQuestion, idResponse);
            }
            catch (Exception e) {
                DidThrowException = true;
            }
	    
		System.assertEquals(DidThrowException,
    						true, 
    						'No se ha generado el error al enviar una cuenta incorrecta.');
            }
    }
    
    /**
    *   @name: RQO_cls_SurveysController_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la generación de respuestas enviadas mediante json.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	@isTest
    public static void WhenAddResponsesDoesNotThrowException() {
		User u = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSurveys@testorg.com');
        System.runAs(u){
            RQO_obj_survey__c survey = [SELECT Id FROM RQO_obj_survey__c limit 1];
                
            List<RQO_obj_surveyQuestion__c> surveyQuestions = 
                new RQO_cls_SurveyQuestionSelector().selectBySurveyId(new Set<Id>{survey.Id});
            
            System.debug(surveyQuestions[0]);
            
            string jsonResponse = '[{"RQO_fld_question__c":"' + surveyQuestions[0].Id + 
                                  '","Id":"' + surveyQuestions[0].Question_Responses__r[0].Id + 
                                  '","Name":"' + surveyQuestions[0].Question_Responses__r[0].Name +
                                  '","RQO_fld_position__c":1,"RQO_fld_selected__c":true}]';
                                  
            System.debug(jsonResponse);
            
            //User u = RQO_cls_TestDataFactory.userCreation('sfTestUserRunAsResponses@testorg.com', 'System Administrator', null,  'es', 'es_ES', 'Europe/Berlin');
            
            List<RQO_obj_surveyResponse__c> responses;
            
            //System.runAs(u){
                test.startTest();
                RQO_cls_SurveysController_cc.addResponses(survey.Id, jsonResponse);
                responses = [SELECT Id FROM RQO_obj_surveyResponse__c WHERE RQO_fld_question__c = :surveyQuestions[0].Id];
                test.stopTest();
            //}
    	
		System.assertNotEquals(0,
						responses.size(), 
						'No se ha creado ningún registro.');
            }
    }
    
    /**
    *   @name: RQO_cls_SurveysController_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la traducción correcta de las encuestas.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
	@isTest
    public static void WhenTranslateSurveyReturnsTranslations() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSurveys@testorg.com');
        System.runAs(usuario){
            RQO_obj_survey__c survey = new RQO_obj_survey__c (Name = 'Test Survey',
                                                              RQO_fld_nameEn__c = 'Test Survey EN',
                                                              RQO_fld_nameFr__c = 'Test Survey FR',
                                                              RQO_fld_nameDe__c = 'Test Survey DE',
                                                              RQO_fld_nameIt__c = 'Test Survey IT',
                                                              RQO_fld_namePt__c = 'Test Survey PT');
                                                              
            User u = new RQO_cls_UserSelector().selectById(new Set<Id>{UserInfo.getUserId()})[0];
            
            u.LanguageLocaleKey='en_US';
            update u;
            survey = RQO_cls_SurveysController_cc.translateSurvey(survey);
            System.assertEquals('Test Survey EN',
                                survey.Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='fr';
            update u;
            survey = RQO_cls_SurveysController_cc.translateSurvey(survey);
            System.assertEquals('Test Survey FR',
                                survey.Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='de';
            update u;
            survey = RQO_cls_SurveysController_cc.translateSurvey(survey);
            System.assertEquals('Test Survey DE',
                                survey.Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='it';
            update u;
            survey = RQO_cls_SurveysController_cc.translateSurvey(survey);
            System.assertEquals('Test Survey IT',
                                survey.Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='pt_BR';
            update u;
            survey = RQO_cls_SurveysController_cc.translateSurvey(survey);
            System.assertEquals('Test Survey PT',
                                survey.Name, 
                                'La encuesta no se ha traducido correctamente.');
                                
            survey = new RQO_obj_survey__c (Name = 'Test Survey', RQO_fld_nameEn__c = 'Test Survey EN');
            
            u.LanguageLocaleKey='fr';
            update u;
            survey = RQO_cls_SurveysController_cc.translateSurvey(survey);
            System.assertEquals('Test Survey EN',
                                survey.Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='de';
            update u;
            survey = RQO_cls_SurveysController_cc.translateSurvey(survey);
            System.assertEquals('Test Survey EN',
                                survey.Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='it';
            update u;
            survey = RQO_cls_SurveysController_cc.translateSurvey(survey);
            System.assertEquals('Test Survey EN',
                                survey.Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='pt_BR';
            update u;
            survey = RQO_cls_SurveysController_cc.translateSurvey(survey);
            System.assertEquals('Test Survey EN',
                                survey.Name, 
                                'La encuesta no se ha traducido correctamente.');
            }
    }
    
    /**
    *   @name: RQO_cls_SurveysController_test
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método de test que comprueba la traducción correcta de las preguntas.
    *   @return: 
    *   @exception: 
    *   @throws: 
    */
    @isTest
    public static void WhenTranslateSurveyQuestionsReturnsTranslations() {
		User usuario = RQO_cls_TestDataFactory.getCreatedUser('sfTestUserSurveys@testorg.com');
        System.runAs(usuario){
            RQO_obj_survey__c survey = [SELECT Id FROM RQO_obj_survey__c limit 1];
            
            List<RQO_obj_surveyQuestion__c> surveyQuestions = 
                new RQO_cls_SurveyQuestionSelector().selectBySurveyId(new Set<Id>{survey.Id});
            
            System.debug(surveyQuestions[0]);
            
            List<RQO_obj_surveyQuestion__c> questions = new List<RQO_obj_surveyQuestion__c>();
            questions.add(surveyQuestions[0]);
        
            User u = new RQO_cls_UserSelector().selectById(new Set<Id>{UserInfo.getUserId()})[0];
            
            u.LanguageLocaleKey='en_US';
            update u;
            questions = RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
            System.assertEquals('Test Survey Question EN',
                                questions[0].Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='fr';
            update u;
            questions = RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
            System.assertEquals('Test Survey Question FR',
                                questions[0].Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='de';
            update u;
            questions = RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
            System.assertEquals('Test Survey Question DE',
                                questions[0].Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='it';
            update u;
            questions = RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
            System.assertEquals('Test Survey Question IT',
                                questions[0].Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='pt_BR';
            update u;
            questions = RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
            System.assertEquals('Test Survey Question PT',
                                questions[0].Name, 
                                'La encuesta no se ha traducido correctamente.');
            
            surveyQuestions[0].RQO_fld_nameFr__c = '';
            surveyQuestions[0].RQO_fld_nameDe__c = '';
            surveyQuestions[0].RQO_fld_nameIt__c = '';
            surveyQuestions[0].RQO_fld_namePt__c = '';
                                               
            update surveyQuestions[0];
            
            questions = new List<RQO_obj_surveyQuestion__c>();
            questions.add(surveyQuestions[0]);
                                
            u.LanguageLocaleKey='fr';
            update u;
            questions = RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
            System.assertEquals('Test Survey Question EN',
                                questions[0].Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='de';
            update u;
            questions = RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
            System.assertEquals('Test Survey Question EN',
                                questions[0].Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='it';
            update u;
            questions = RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
            System.assertEquals('Test Survey Question EN',
                                questions[0].Name, 
                                'La encuesta no se ha traducido correctamente.');
            u.LanguageLocaleKey='pt_BR';
            update u;
            questions = RQO_cls_SurveysController_cc.translateSurveyQuestions(questions);
            System.assertEquals('Test Survey Question EN',
                                questions[0].Name, 
                                'La encuesta no se ha traducido correctamente.');
            }
    }
    
}