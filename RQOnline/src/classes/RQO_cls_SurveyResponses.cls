/**
*   @name: RQO_cls_SurveyResponses
*   @version: 1.0
*   @creation date: 31/10/2017
*   @author: Alfonso Constan López - Unisys
*   @description: Clase que gestiona las RQO_cls_SurveyResponses
*/
public with sharing class RQO_cls_SurveyResponses extends RQO_cls_ApplicationDomain 
{   
    // // ***** CONSTANTES ***** //
    private static final String CLASS_NAME = 'RQO_cls_SurveyResponses';
    
    /**
    *   @name: RQO_cls_SurveyResponses
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método constructor de las RQO_cls_SurveyResponses
    */
    public RQO_cls_SurveyResponses(List<RQO_obj_surveyResponse__c> surveyResponses) 
    {     
        super(surveyResponses);   
    }   
    
    /**
    *   @name: RQO_cls_SurveyResponses
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Clase genérica para gestionar un constructor
    */
    public class Constructor implements fflib_SObjectDomain.IConstructable 
    {     
        public fflib_SObjectDomain construct(List<SObject>sObjectList) 
        {       
            return new RQO_cls_SurveyResponses(sObjectList);     
        }   
    }  
    
    /**
    *   @name: RQO_cls_SurveyResponses
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico para insertar registros desde la clase
    *   padre RQO_cls_ApplicationDomain
    */
    public void addResponse()
    {
         /**
         * Constantes
         */
        final String METHOD = 'addResponse';
        
        insert Records;
    }

    /**
    *   @name: RQO_cls_SurveyResponses
    *   @version: 1.0
    *   @creation date: 31/10/2017
    *   @author: Alfonso Constan López - Unisys
    *   @description: Método genérico para agregar respuestas anónimas
    */
    public void addAnonymousResponse ()
    {
         /**
         * Constantes
         */
        final String METHOD = 'addAnonymousResponse';
        
        for(RQO_obj_surveyResponse__c response : (List<RQO_obj_surveyResponse__c>) Records)
        {
            // Login Salesforce
            string loginUrl = RQO_cls_CustomSettingUtil.getSalesforceLoginUrl();
            System.debug(CLASS_NAME + ' - ' + METHOD + ' loginUrl: ' + loginUrl);
            string body = 'grant_type=password&client_id=' + RQO_cls_CustomSettingUtil.getSalesforceKey() + 
                            '&client_secret=' + RQO_cls_CustomSettingUtil.getSalesforceSecret() + 
                            '&username=' + RQO_cls_CustomSettingUtil.getSalesforceUser() + 
                            '&password=' + RQO_cls_CustomSettingUtil.getSalesforcePassword();
            Map<String, String> headers = new Map<String, String>();
            headers.put('Content-Type', 'application/x-www-form-urlencoded');
            RQO_cls_WSCallout loginCallout = new RQO_cls_WSCallout(loginUrl, RQO_cls_Constantes.REST_METHOD_TYPE_POST, body, headers);
            RQO_cls_WSCalloutResponseBean loginResponse = loginCallout.send();
            
            if (loginResponse != null && loginResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_OK))
            {
                string instance_url;
                string access_token;
                JSONParser parser = JSON.createParser(loginResponse.contenido);
                
                while (parser.nextToken() != null) {
                     if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'instance_url'))
                     {
                        parser.nextToken();
                        instance_url =  parser.getText();
                     }
                     else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token'))
                     {
                        parser.nextToken();
                        access_token = parser.getText();
                     }
                }
                
                if (!String.isBlank(instance_url) && !String.isBlank(access_token))
                {
                    // Add Response
                    string addResponseUrl = instance_url + '/services/data/v41.0/sobjects/RQO_obj_surveyResponse__c';
                    System.debug(CLASS_NAME + ' - ' + METHOD + ' addResponseUrl: ' + addResponseUrl);
                    body = '{"Name" : "' + response.Name + '", ' +
                            '"RQO_fld_question__c" : "' + response.RQO_fld_question__c + '", ' +
                            '"RQO_fld_response__c" : "' + response.RQO_fld_response__c + '"}';
    
                    headers = new Map<String, String>();
                    headers.put('Authorization', 'Bearer ' + access_token);
                    headers.put('Content-Type', 'application/json');
                    RQO_cls_WSCallout addContactCallout = new RQO_cls_WSCallout(addResponseUrl, RQO_cls_Constantes.REST_METHOD_TYPE_POST, body, headers);
                    RQO_cls_WSCalloutResponseBean addContactResponse = addContactCallout.send();
                    
                    if (addContactResponse != null && addContactResponse.estado == String.valueOf(RQO_cls_Constantes.REST_COD_HTTP_CREATED))
                    {
                        System.debug(CLASS_NAME + ' - ' + METHOD + ' Add Response OK: ' + addResponseUrl);
                    }
                    else
                    {
                        throw new CalloutException('Error llamando API Salesforce');
                    }
                }
            }
            else
            {
                throw new CalloutException('Error llamando API Salesforce');
            }
        }
    }
}