/*------------------------------------------------------------------------
Author:         Rubén Simarro
Company:        Indra
Description:    Clase controladora de la visualforce page ITPM_VF_CustomerUnits_Popup
History
<Date>          <Author>        <Description>
26-Oct-2016     Rubén Simarro
----------------------------------------------------------------------------*/
global with sharing class ITPM_Controller_CustomerUnits_Popup {  
    
    public List<ITPM_UBC_distribution__c> lstCustomerUnit {get; set;}
    
     public ITPM_Controller_CustomerUnits_Popup(){
            
           lstCustomerUnit = [select id,    
                           name, 
                           ITPM_PER_Distribution__c,  
                           ITPM_SEL_Country__c,
                           ITPM_REL_UBC__r.name
                           from ITPM_UBC_distribution__c   
                           where ITPM_REL_Investment_Item__c =: ApexPages.currentPage().getParameters().get('Id')];
     }
      
    
}