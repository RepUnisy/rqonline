/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class ccService {
    global static String BASEFIELDSMAP;
    global String entityName {
        get;
        set;
    }
    global static Set<String> FILTEREDFIELDS;
    global static String NAMESPACESET;
    global static String OBJECTFIELDS;
    global String objectName {
        get;
        set;
    }
    global static String QUERYLIMIT;
    global static String QUERYOFFSET;
    global static String QUERYRESULTS;
    global static String QUERYSTRING;
    global static String SEARCHRESULTS;
    global static String SEARCHSTRING;
    global Boolean skipTransform {
        get;
        set;
    }
    global static String SVCDAO;
    global static String TRANSFORMINDEX;
    global ccService() {

    }
    global virtual Map<String,Object> buildQuery(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> buildSearchQuery(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> fetch(Map<String,Object> inputData) {
        return null;
    }
    global static ccrz.ccService findServiceClass(String svccn, Map<Integer,String> vMap, Integer ver) {
        return null;
    }
    global virtual Map<String,Object> getDirectQueryMap(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> getFieldsMap(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> getFilterMap(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> getFindFieldsClauseMap(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> getOrderByMap(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> getReturnClauseMap(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> getSubQueryMap(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> getWhereClauseMap(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> getWriteableFieldsMap(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> handleFieldSizes(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> handleRelSizes(Map<String,Object> inputdata) {
        return null;
    }
    global virtual Map<String,Object> handleSkipTrz(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> initSvcDAO(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> prepReturn(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> revise(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> runQuery(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> runSearchQuery(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> search(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> transformIn(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> transformOut(Map<String,Object> inputData) {
        return null;
    }
}
