/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class ccLogic {
    global String objectName {
        get;
        set;
    }
    global ccLogic() {

    }
    global static ccrz.ccLogic findLogicClass(String svccn, Map<Integer,String> vMap, Integer ver) {
        return null;
    }
    global virtual Map<String,Object> postProcess(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> preProcess(Map<String,Object> inputData) {
        return null;
    }
    global virtual Map<String,Object> process(Map<String,Object> inputData) {
        return null;
    }
}
