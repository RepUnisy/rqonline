/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ccAPIProduct {
    global static String DATEFILTER;
    global static String ENTITYNAME;
    global static String HEADERONLY;
    global static String MEDIAFILTER;
    global static String PARAM_BY_ASC;
    global static String PARAM_BY_NULLS_LAST;
    global static String PARAM_BY_SEQ;
    global static String PARAM_INCLUDE_BLOCKED;
    global static String PARAM_INCLUDE_PRICING;
    global static String PARAM_INCLUDE_SELLERS;
    global static String PRODUCT;
    global static String PRODUCTID;
    global static String PRODUCTIDLIST;
    global static String PRODUCTLIST;
    global static String PRODUCTLOCALE;
    global static String PRODUCTSEARCHMAP;
    global static String PRODUCTSELLER;
    global static String PRODUCTSELLERLIST;
    global static String PRODUCTSKU;
    global static String PRODUCTSKULIST;
    global static String PRODUCTSTOREFRONT;
    global ccAPIProduct() {

    }
    global static Map<String,Object> fetch(Map<String,Object> inputData) {
        return null;
    }
    global static Map<String,Object> search(Map<String,Object> inputData) {
        return null;
    }
}
