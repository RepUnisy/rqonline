/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ccAPICategory {
    global static String CATEGORY;
    global static String CATEGORYID;
    global static String CATEGORYIDLIST;
    global static String CATEGORYLIST;
    global static String CATEGORYLOCALE;
    global static String CATEGORYNAME;
    global static String CATEGORYTREE;
    global static String ENTITYNAME;
    global static String FEATURED;
    global static String HEADERONLY;
    global static String PARENTCATEGORY;
    global static String PRODUCTIDTOSKU;
    global static String PRODUCTSEARCH;
    global static String ROOTCATEGORYID;
    global static String STOREFRONT;
    global ccAPICategory() {

    }
    global static Map<String,Object> fetchCategoryTree(Map<String,Object> inputData) {
        return null;
    }
    global static Map<String,Object> fetch(Map<String,Object> inputData) {
        return null;
    }
}
