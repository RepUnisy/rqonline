/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ccUtil {
    global ccUtil() {

    }
    global static Object defv(Map<String,Object> mp, String k, Object d) {
        return null;
    }
    global static Boolean isEmpty(List<Object> lst) {
        return null;
    }
    global static Boolean isEmpty(Map<String,Object> mp) {
        return null;
    }
    global static Boolean isNotEmpty(List<Object> lst) {
        return null;
    }
    global static Boolean isNotEmpty(Map<String,Object> mp) {
        return null;
    }
    global static Boolean isNotTrue(Object obj) {
        return null;
    }
    global static Boolean isNotTrue(Map<String,Object> mp, String k) {
        return null;
    }
    global static Boolean isTrue(Object obj) {
        return null;
    }
    global static Boolean isTrue(Map<String,Object> mp, String k) {
        return null;
    }
}
