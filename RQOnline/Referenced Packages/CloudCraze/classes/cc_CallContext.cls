/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class cc_CallContext {
    global static Account currAccount {
        get;
    }
    global static ccrz__E_AccountGroup__c currAccountGroup {
        get;
    }
    global static String currAccountId {
        get;
    }
    global static String currCartId {
        get;
    }
    global static Contact currContact {
        get;
    }
    global static String currPageKey {
        get;
    }
    global static String currPageName {
        get;
    }
    global static Map<String,String> currPageParameters {
        get;
    }
    global static String currPageUIKey {
        get;
    }
    global static String currUIKey {
        get;
    }
    global static String currURL {
        get;
    }
    global static User currUser {
        get;
    }
    global static Id currUserId {
        get;
    }
    global static Account effAccount {
        get;
    }
    global static ccrz__E_AccountGroup__c effAccountGroup {
        get;
    }
    global static String effAccountId {
        get;
    }
    global static Boolean isGuest {
        get;
    }
    global static Boolean isOnBehalf {
        get;
    }
    global static Boolean isRemoteCall {
        get;
    }
    global static String pageLabelJSON {
        get;
    }
    global static Map<String,String> pageLabels {
        get;
    }
    global static ccrz__E_PriceGroup__c priceGroup {
        get;
    }
    global static String priceGroupId {
        get;
    }
    global static ccrz.cc_RemoteActionContext remoteContext {
        get;
    }
    global static String storefront {
        get;
    }
    global static Map<String,Object> storeFrontSettings {
        get;
    }
    global static String userCurrency {
        get;
    }
    global static String userLocale {
        get;
    }
    global static String getConfigValue(String conf, String uiKey) {
        return null;
    }
    global static ccrz.cc_RemoteActionResult init(ccrz.cc_RemoteActionContext ctx) {
        return null;
    }
    global static void initContext(ccrz.ccContext ctx) {

    }
    global static void initRemoteContext(ccrz.cc_RemoteActionContext ctx) {

    }
    global static Boolean isConfigEqual(String conf, String val, String uiKey) {
        return null;
    }
    global static Boolean isConfigTrue(String conf, String uiKey) {
        return null;
    }
}
