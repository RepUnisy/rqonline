/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ccApiUser {
    global static String ENTITYNAME;
    global static String USER_EMAIL;
    global static String USER_ID;
    global static String USER_IDLIST;
    global static String USER_NAME;
    global static String USER_NEW;
    global static String USERLIST;
    global ccApiUser() {

    }
    global static Map<String,Object> createUser(Map<String,Object> inputData) {
        return null;
    }
    global static Map<String,Object> fetch(Map<String,Object> inputData) {
        return null;
    }
}
